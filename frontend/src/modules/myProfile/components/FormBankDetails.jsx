import React from "react";
import PropTypes from "prop-types";
import { Button } from "reactstrap";
import isEqual from "lodash/isEqual";
import isEmpty from "lodash/isEmpty";

class FormBankDetails extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      user_id: "",
      bank_account_no: "",
      bank_name: "",
      bank_address: "",
      ifsc: ""
    };
  }
  componentDidMount() {
    this.setState({
      bank_account_no:
        this.props.user_bank_detail &&
        this.props.user_bank_detail.bank_account_no,
      bank_name:
        this.props.user_bank_detail && this.props.user_bank_detail.bank_name,
      bank_address:
        this.props.user_bank_detail && this.props.user_bank_detail.bank_address,
      ifsc: this.props.user_bank_detail && this.props.user_bank_detail.ifsc
    });
  }
  componentWillReceiveProps(props) {
    if (
      isEmpty(props.user_bank_detail) &&
      isEqual(props.user_bank_detail, this.props.user_bank_detail)
    ) {
      this.setState({
        bank_account_no: props.user_bank_detail && props.user_bank_detail.bank_account_no,
        bank_name: props.user_bank_detail && props.user_bank_detail.bank_name,
        bank_address:props.user_bank_detail &&  props.user_bank_detail.bank_address,
        ifsc: props.user_bank_detail && props.user_bank_detail.ifsc
      });
    }
  }
  render() {
    return (
      <div>
        <div className="row no-gutter">
          <div className="col-xs-6 col-sm-12 profile-input form-group">
            <label>Bank Account Number</label>
            <input
              type="text"
              className="form-control"
              onChange={e => this.setState({ bank_account_no: e.target.value })}
              value={this.state.bank_account_no}
            />
          </div>
          <div className="col-xs-6 col-sm-12 profile-input form-group">
            <label>Bank Name</label>
            <input
              type="text"
              className="form-control"
              onChange={e => this.setState({ bank_name: e.target.value })}
              value={this.state.bank_name}
            />
          </div>
        </div>
        <div className="row no-gutter">
          <div className="col-xs-6 col-sm-12 profile-input form-group">
            <label>Bank Address</label>
            <input
              type="text"
              className="form-control"
              onChange={e => this.setState({ bank_address: e.target.value })}
              value={this.state.bank_address}
            />
          </div>
          <div className="col-xs-6 col-sm-12 profile-input form-group">
            <label>IFSC Code</label>
            <input
              type="text"
              className="form-control"
              onChange={e => this.setState({ ifsc: e.target.value })}
              value={this.state.ifsc}
            />
          </div>
        </div>
        <div className="row no-gutter">
          <div className="col-xs-12 profile-input form-group">
            <Button
              className="pull-right"
              color="primary"
              onClick={() => this.props.callUpdateBankDetails(this.state)}
            >
              Update Bank Details
            </Button>
          </div>
        </div>
      </div>
    );
  }
}

FormBankDetails.propTypes = {
  bank_account_no: PropTypes.number,
  bank_name: PropTypes.string,
  bank_address: PropTypes.string,
  ifsc: PropTypes.string
};

export default FormBankDetails;
