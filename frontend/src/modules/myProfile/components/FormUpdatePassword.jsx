import React from 'react';
import {Button} from 'reactstrap'

class FormUpdatePassword extends React.Component {
  constructor (props) {
    super(props);
    this.state = {
      new_password: ''
    };
  }

  componentWillReceiveProps (props) {
    this.state = {
      new_password: ''
    };
  }
  render () {
    return (
      <div>
        <div className="row no-gutter">
          <div className="col-xs-12 profile-input form-group">
            <label>Enter New Password</label>
            <input
              type="password"
              className="form-control"
              onChange={(e) => this.setState({new_password: e.target.value})}
              value={this.state.new_password}
            />
          </div>
          <div className="profile-input form-group col-xs-12">
            <Button className="pull-right" color='primary' onClick={() => this.props.callUpdatePassword(this.state.new_password)}>Update Password</Button>
          </div>
        </div>
      </div>
    );
  }
}

export default FormUpdatePassword;
