import React, { Component } from "react";
import UploadImageComp from "../../uploadImageCompressed/UploadImageComp";
import Dropzone from "react-dropzone";

export default class ProfilePic extends Component {
  render() {
    return (
      <div>
        <Dropzone accept="image/*" onDrop={this.props.handleFileChange}>
          {({ getRootProps, getInputProps }) => {
            return (
              <section className="container m-0 p-0 col-sm-12">
                <div {...getRootProps({ className: "dropzone" })}>
                  <input {...getInputProps()} />
                  <div className="drag_and_drop align-items-center">
                    {this.props.file &&
                      this.props.file.map((file, index) => (
                        <div className="font-weight-bold"> {file.name}</div>
                      ))}
                    {this.props.imageUrl ? (
                      <img
                        src={this.props.imageUrl}
                        className="mx-3 img-fluid profile-doc-preview"
                        alt="Preview"
                      />
                    ) : (
                      <p className="uploading_doc">
                        <i className="fi flaticon-upload" />
                      </p>
                    )}

                    <p className="doc_upload_place">
                      Drop a document here or click to select file to upload
                    </p>
                  </div>
                </div>
              </section>
            );
          }}
        </Dropzone>
        <div>
          <UploadImageComp
            validateDocuments={this.props.validateDocuments}
            fileParams={{
              file: this.props.file,
              document_type: this.props.documentType,
              token: this.props.token,
              type: this.props.type,
              imageUrl: this.props.imageUrl,
              file_upload_action: this.props.documentType
            }}
            url={"generic_upload_url"}
            onSuccessfulFileUpload={this.props.successfulFileUpload}
            handleRomoveImage={this.props.handleRomoveImage}
            isLoading={this.props.loadingProfile}
            isError={this.props.loadingProfileError}
          />
        </div>
      </div>
    );
  }
}
