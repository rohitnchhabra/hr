import React, { useState, useEffect } from "react";
import PropTypes from "prop-types";
import { Button } from "reactstrap";
import {InputTextWithLabel} from "../../../components/generic/InputText/InputText";
import TextAreaWithLabel from "../../../components/generic/TextArea/TextAreaWithLabel";

const FormProfileDetails = (props) => {
  const [state, setState] = useState({
    user_id: "",
    name: "",
    jobtitle: "",
    dateofjoining: "",
    dob: "",
    gender: "",
    marital_status: "Single",
    address1: "",
    address2: "",
    emr_con_1: "",
    emr_con_2: "",
    work_email: "",
    emp_email: "",
    blood_group: "",
    medical_con: "",
  });
  useEffect(() => {
    let {
      name,
      jobtitle,
      dateofjoining,
      dob,
      gender,
      marital_status,
      current_address,
      permanent_address,
      emergency_ph1,
      emergency_ph2,
      work_email,
      other_email,
      blood_group,
      medical_condition,
    } = props.user_profile_detail;
    setState({
      name: name || "",
      jobtitle: jobtitle || "",
      dateofjoining: dateofjoining || "",
      dob: dob || "",
      gender: gender || "",
      marital_status: marital_status || "Single",
      address1: current_address || "",
      address2: permanent_address || "",
      emr_con_1: emergency_ph1 || "",
      emr_con_2: emergency_ph2 || "",
      work_email: work_email || "",
      emp_email: other_email || "",
      blood_group: blood_group || "",
      medical_con: medical_condition || "",
    });
  }, [props.user_profile_detail]);

  return (
    <div>
      <div className="row no-gutter">
        <InputTextWithLabel
          label="Name"
          id="user_name"
          type="text"
          className="form-control"
          value={state.name}
          disabled
        />
      </div>
      <div className="row no-gutter">
        <div className="col-xs-6 col-sm-12 profile-input form-group">
          <label>Marital Status</label>
          <select
            className="form-control"
            disabled={props.user_profile_detail.marital_status}
            onChange={(e) =>
              setState({ ...state, marital_status: e.target.value })
            }
            value={state.marital_status}
          >
            <option value="Single">Single</option>
            <option value="Married">Married</option>
            <option value="Other">Other</option>
          </select>
        </div>
      </div>
      <div className="row no-gutter">
        <div className="col-xs-6 profile-input">
          <TextAreaWithLabel
            label="Current Address"
            disabled={props.user_profile_detail.current_address}
            placeholder="your current address..."
            className="form-control resize-y"
            onChange={(e) => setState({ ...state, address1: e.target.value })}
            value={state.address1}
          />
        </div>
        <div className="col-xs-6 profile-input">
          <div className="form-group">
            <label>Permanent Address</label>
            <textarea
              disabled={props.user_profile_detail.permanent_address}
              placeholder="your permanent address..."
              className="form-control resize-y"
              onChange={(e) => setState({ ...state, address2: e.target.value })}
              value={state.address2}
            ></textarea>
          </div>
        </div>
      </div>
      <div className="row no-gutter">
        <div className="col-xs-6 profile-input">
          <div className="form-group">
            <label>Personal Contact Number</label>
            <input
              disabled={props.user_profile_detail.emergency_ph1}
              id="user_Contact"
              type="text"
              className="form-control"
              onChange={(e) =>
                setState({ ...state, emr_con_1: e.target.value })
              }
              value={state.emr_con_1}
            />
          </div>
        </div>
        <div className="col-xs-6 profile-input">
          <div className="form-group">
            <label>Emergency Contact</label>
            <input
              disabled={props.user_profile_detail.emergency_ph2}
              type="text"
              className="form-control"
              onChange={(e) =>
                setState({ ...state, emr_con_2: e.target.value })
              }
              value={state.emr_con_2}
            />
          </div>
        </div>
      </div>
      <div className="row no-gutter">
        <div className="col-xs-6 col-sm-12 profile-input form-group">
          <label>Personal Email Address</label>
          <input
            type="email"
            disabled={props.user_profile_detail.other_email}
            className="form-control"
            onChange={(e) => setState({ ...state, emp_email: e.target.value })}
            value={state.emp_email}
          />
        </div>
        <div className="col-xs-6 col-sm-12 profile-input form-group">
          <label>Blood Group</label>
          <select
            className="form-control"
            onChange={(e) =>
              setState({ ...state, blood_group: e.target.value })
            }
            value={state.blood_group}
            disabled={props.user_profile_detail.blood_group}
          >
            <option value="">--select your blood group--</option>
            <option value="o+">O+</option>
            <option value="o-">O-</option>
            <option value="a+">A+</option>
            <option value="a-">A-</option>
            <option value="b+">B+</option>
            <option value="b-">B-</option>
            <option value="ab+">AB+</option>
            <option value="ab-">AB-</option>
          </select>
        </div>
      </div>
      <div className="row no-gutter">
        <div className="col-xs-12 profile-input form-group">
          <label>Any Medical Conditions</label>
          <textarea
            disabled={props.user_profile_detail.medical_condition}
            placeholder="your medical conditions..."
            className="form-control resize-y"
            onChange={(e) => setState({ medical_con: e.target.value })}
            value={state.medical_con}
          ></textarea>
        </div>
      </div>
      {(!props.user_profile_detail.medical_con && state.medical_con) ||
      (!props.user_profile_detail.blood_group && state.blood_group) ||
      (!props.user_profile_detail.other_email && state.emp_email) ||
      (!props.user_profile_detail.emergency_ph2 && state.emr_con_2) ||
      (!props.user_profile_detail.emergency_ph1 && state.emr_con_1) ||
      (!props.user_profile_detail.permanent_address && state.address2) ||
      (!props.user_profile_detail.current_address && state.address1) ||
      (!props.user_profile_detail.marital_status && state.marital_status) ? (
        <div id="profile_update" className="row no-gutter">
          <div className="col-xs-12 profile-input form-group">
            <Button
              color="primary"
              className="pull-right"
              onClick={() => props.callUpdateProfileDetails(state)}
            >
              Update Profile Details
            </Button>
          </div>
        </div>
      ) : null}
    </div>
  );
};

FormProfileDetails.propTypes = {
  name: PropTypes.string,
  jobtitle: PropTypes.string,
  dateofjoining: PropTypes.string,
  dob: PropTypes.string,
  gender: PropTypes.string,
  marital_status: PropTypes.string,
  address1: PropTypes.string,
  address2: PropTypes.string,
  emr_con_1: PropTypes.number,
  emr_con_2: PropTypes.number,
  work_email: PropTypes.string,
  emp_email: PropTypes.string,
  blood_group: PropTypes.string,
  medical_con: PropTypes.string,
};
export default FormProfileDetails;
