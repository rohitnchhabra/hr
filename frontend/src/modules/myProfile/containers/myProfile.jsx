import React, { createRef } from "react";
import { connect } from "react-redux";
import { withRouter } from "react-router";
import PropTypes from "prop-types";
import { notify } from "../../../services/notify";
import { isNotUserValid } from "../../../services/generic";
import Header from "../../../components/generic/Header";
import UserHorizontalView from "../../../components/generic/UserHorizontalView";
import FormProfileDetails from "../../../modules/myProfile/components/FormProfileDetails";
import FormBankDetails from "../../../modules/myProfile/components/FormBankDetails";
import FormUpdatePassword from "../../../modules/myProfile/components/FormUpdatePassword";
import * as actionsMyProfile from "../../../redux/myProfile/actions/myProfile";
import * as actions from '../../../redux/actions'
import * as actionsSalary from "../../../redux/salary/actions/viewSalary";
import ImageCompressor from "image-compressor.js";
import { qualityValue } from "../../../helper/helper";
import axios from "axios";
import { CONFIG } from "../../../config";
import { getToken } from "../../../services/generic";
import classnames from "classnames";
import Heading from "../../../components/generic/Heading";
import { Alert} from "reactstrap";
import ProfilePic from "../components/ProfilePic";

class MyProfile extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      status_message: "",
      user_profile_detail: {},
      user_bank_detail: [],
      payslip_history: [],
      user_assign_machine: [],
      picDailog: false,
      activeSecondTab: "tab21",
      file: [],
      imageUrl: "",
      type: "",
      documentType: ""
    };
    this.file = createRef();
    this.callUpdateBankDetails = this.callUpdateBankDetails.bind(this);
    this.callUpdateProfileDetails = this.callUpdateProfileDetails.bind(this);
    this.callUpdateUserDeviceDetails = this.callUpdateUserDeviceDetails.bind(
      this
    );
    this.callUpdatePassword = this.callUpdatePassword.bind(this);
  }
  componentWillMount() {
    this.props.onMyProfileDetails();
    this.props.onSalaryDetails();
    this.props.showHeading("Edit Profile");
  }
  componentWillReceiveProps(props) {
    if (props.myProfile.status_message) {
      notify(props.myProfile.status_message);
    }
    let isNotValid = isNotUserValid(
      this.props.location.pathname,
      props.loggedUser
    );
    if (isNotValid.status) {
      this.props.history.push(isNotValid.redirectTo);
    }
    let s_payslip_history = [];

    if (
      typeof props.salary.payslip_history !== "undefined" &&
      props.salary.payslip_history.length > 0
    ) {
      s_payslip_history = props.salary.payslip_history;
    }

    if (
      JSON.stringify(this.props.myProfile.user_profile_detail) !==
        JSON.stringify(props.myProfile.user_profile_detail) ||
      !Object.keys(this.state.user_profile_detail).length
    ) {
      this.setState({
        user_profile_detail: props.myProfile.user_profile_detail
      });
    }
    this.setState({
      user_assign_machine: props.myProfile.user_assign_machine,
      user_bank_detail: props.myProfile.user_bank_detail,
      payslip_history: s_payslip_history
    });
  }
  toggleSecondTabs = tab => {
    this.setState({
      activeSecondTab: tab
    });
  };
  callUpdateBankDetails(new_bank_details) {
    this.props.onUpdateBankDetails(new_bank_details).then(
      data => {},
      error => {
        notify(error);
      }
    );
  }
  callUpdateUserDeviceDetails(new_device_details) {
    this.props.onUpdateDeviceDetails(new_device_details).then(
      data => {},
      error => {
        notify(error);
      }
    );
  }
  callUpdateProfileDetails(new_profile_details) {
    this.props.onUpdateProfileDetails(new_profile_details).then(
      data => {},
      error => {
        notify(error);
      }
    );
  }
  callUpdatePassword(new_password) {
    new_password = new_password.trim();
    if (new_password === "") {
      notify("Enter Password !!");
    } else {
      this.props.onUpdatePassword(new_password).then(
        data => {
          notify(data);
          this.props.onMyProfileDetails();
        },
        error => {
          notify(error);
        }
      );
    }
  }
  openPicDailog = () => {
    this.setState({ picDailog: true });
  };
  closePicDailog = () => {
    this.setState({ picDailog: false });
  };
  handleFileChange = files => {
    // this.setState({ file: Array.from(e.target.files) });
    this.setState({
      file: files,
      type:
        files[0].name &&
        files[0].name.split(".")[files[0].name.split(".").length - 1],
      documentType: "profile_pic"
    });
    const reader = new FileReader();

    reader.onloadend = () => {
      this.setState({
        imageUrl: reader.result
      });
    };
    if (files) {
      reader.readAsDataURL(files[0]);
      this.setState({
        imageUrl: reader.result
      });
    } else {
      this.setState({
        imageUrl: ""
      });
    }
  };
  handleSubmit = e => {
    e.preventDefault();
    const { user_profile_detail: params } = this.state;
    const file = this.state.file;
    const url = CONFIG.generic_upload_url;

    if (!file) {
      return;
    } else if (file[0] && file[0].type && !file[0].type.includes("image")) {
      const formData = new FormData();
      let isRole = this.props.loggedUser.data.role;
      if (params.user_id) {
        formData.append("user_id", params.user_id);
      }
      formData.append("file", file);
      formData.append("link_1", file[0]);
      formData.append("submit", "Upload");
      formData.append("document_type", "profile_pic");
      formData.append("token", getToken());
      formData.append("file_upload_action", "profile_pic");

      this.props.onUploadFile(formData, url, isRole);
    } else {
      this.setState({ loading: true });
      let quality = qualityValue(file[0]);
      let imageCompressor = new ImageCompressor();
      imageCompressor
        .compress(file[0], {
          quality: quality
        })
        .then(compressedFile => {
          const formData = new FormData();
          if (params.user_id) {
            formData.append("user_id", params.user_id);
          }
          formData.append("file", file);
          formData.append("link_1", compressedFile);
          formData.append("submit", "Upload");
          formData.append("document_type", "profile_pic");
          formData.append("token", getToken());
          formData.append("file_upload_action", "profile_pic");

          // Send the compressed image file to server with XMLHttpRequest.
          // this.props.uploadFile(formData, url);
          axios
            .post(url, formData)
            .then(data => {
              if (data.data && data.data.error) {
                notify("Error", data.data.message, "error");
              } else {
                notify("Success !", `File uploaded successfully`, "success");
                this.setState({ loading: false, file: "", picDailog: false });
                if (this.file.current) {
                  this.file.current.value = "";
                }
              }
            })
            .catch(error => {
              if (error.request.status === 413) {
                notify("Error", "File too large to upload", "error");
              } else {
                notify("Error", error.request.statusText, "error");
              }
              this.setState({ loading: false });
            });
        });
    }
  };

  validateDocuments = e => {
    let stop = false;
    if (!this.state.imageUrl) {
      stop = true;
      notify("Warning!", "Please select a file", "warning");
    }
    if (stop) {
      e.preventDefault();
    }
  };

  uploadingProfilePic = () => {
    this.setState({ profilePicLoading: true });
  };

  uploadingProfilePicError = () => {
    this.setState({ profilePicLoading: false });
  };

  successfulFileUpload = () => {
    this.props.onMyProfileDetails();
    this.setState({
      ...this.state,
      imageUrl: "",
      file: [],
      documentType: "",
      profilePicLoading: false
    });
  };

  handleRomoveImage = () => {
    this.setState({ ...this.state, imageUrl: "", file: [], documentType: "" });
  };

  render() {
    let {
      name,
      jobtitle,
      dateofjoining,
      gender,
      dob,
      work_email,
      mobile_ph,
      user_Id
    } = this.state.user_profile_detail;
    return (
      <div>
        <div id="content" className="app-content box-shadow-z0" role="main">
          <Header pageTitle={"My Profile"} {...this.props} />
          {/* <Heading title={"Edit Profile"} /> */}
          {this.state.activeSecondTab == "tab21" && (
            <Alert fade={false} color={"info"} className="alert-transparent">
              <span className="font-weight-bold">INFO :</span>
              <span> If you want to Edit contact HR.</span>
            </Alert>
          )}
          <div className="app-body" id="view">
            <UserHorizontalView
              profileImage={
                this.props.myProfile.user_profile_detail.profileImage
              }
              userId={user_Id}
              name={name}
              jobtitle={jobtitle}
              dateofjoining={dateofjoining}
              gender={gender}
              dob={dob}
              workEmail={work_email}
              contactNumber={mobile_ph}
              handleSubmit={this.handleSubmit}
              fileRef={this.file}
              file={this.state.file}
              openDailog={this.openPicDailog}
              closeDailog={this.closePicDailog}
              dailogFlag={this.state.picDailog}
              isProfilePicLoading={this.state.profilePicLoading}
            />
            <div className="row no-gutter">
              <div className="col-sm-3">
                <div
                  className={classnames("manage-users-tab", "mb-2", "p-3", {
                    active: this.state.activeSecondTab === "tab21"
                  })}
                  onClick={() => {
                    this.toggleSecondTabs("tab21");
                  }}
                >
                  <span className="text-success pr-1">01</span>
                  <span>Personal Details</span>
                </div>
                {window.screen.availWidth < 576 &&
                  this.state.activeSecondTab == "tab21" && (
                    <div className="col-sm-9 mb-2">
                      <div className="row no-gutter">
                        <div className="col-sm-12 p-a bg-white">
                          <FormProfileDetails
                            user_profile_detail={this.state.user_profile_detail}
                            callUpdateProfileDetails={
                              this.callUpdateProfileDetails
                            }
                          />
                        </div>
                      </div>
                    </div>
                  )}
                <div
                  className={classnames("manage-users-tab", "mb-2", "p-3", {
                    active: this.state.activeSecondTab === "tab22"
                  })}
                  onClick={() => {
                    this.toggleSecondTabs("tab22");
                  }}
                >
                  <span className="text-success pr-1">02</span>
                  <span>Bank Details</span>
                </div>
                {window.screen.availWidth < 576 &&
                  this.state.activeSecondTab == "tab22" && (
                    <div className="col-sm-9 mb-2">
                      <div className="row no-gutter">
                        <div className="col-sm-12 p-a bg-white">
                          <FormBankDetails
                            user_bank_detail={this.state.user_bank_detail}
                            callUpdateBankDetails={this.callUpdateBankDetails}
                          />
                        </div>
                      </div>
                    </div>
                  )}
                <div
                  className={classnames("manage-users-tab", "mb-2", "p-3", {
                    active: this.state.activeSecondTab === "tab23"
                  })}
                  onClick={() => {
                    this.toggleSecondTabs("tab23");
                  }}
                >
                  <span className="text-success pr-1">03</span>
                  <span>Update Password</span>
                </div>
                <div
                  className={classnames("manage-users-tab", "mb-2", "p-3", {
                    active: this.state.activeSecondTab === "tab24"
                  })}
                  onClick={() => {
                    this.toggleSecondTabs("tab24");
                  }}
                >
                  <span className="text-success pr-1">01</span>
                  <span>Profile Picture</span>
                </div>
                {window.screen.availWidth < 576 &&
                  this.state.activeSecondTab == "tab24" && (
                    <div className="col-sm-9 mb-2">
                      <div className="row no-gutter">
                        <div className="col-sm-12 p-a bg-white">
                          <ProfilePic
                            file={this.state.file}
                            imageUrl={this.state.imageUrl}
                            documentType={this.state.documentType}
                            token={getToken()}
                            type={this.state.type}
                            handleRomoveImage={this.handleRomoveImage}
                            validateDocuments={this.validateDocuments}
                            successfulFileUpload={this.successfulFileUpload}
                            handleFileChange={this.handleFileChange}
                            loadingProfile={this.uploadingProfilePic}
                            loadingProfileError={this.uploadingProfilePicError}
                          />
                        </div>
                      </div>
                    </div>
                  )}
              </div>
              {window.screen.availWidth < 576 &&
                this.state.activeSecondTab == "tab23" && (
                  <div className="col-sm-9 mb-2">
                    <div className="row no-gutter">
                      <div className="col-sm-12 p-a bg-white">
                        <FormUpdatePassword
                          callUpdatePassword={this.callUpdatePassword}
                        />
                      </div>
                    </div>
                  </div>
                )}
              {window.screen.availWidth >= 576 && (
                <div className="col-sm-9 mb-5">
                  <div className="row pl-3 no-gutter">
                    {this.state.activeSecondTab == "tab21" && (
                      <div className="col-sm-12 p-a bg-white">
                        <FormProfileDetails
                          user_profile_detail={this.state.user_profile_detail}
                          callUpdateProfileDetails={
                            this.callUpdateProfileDetails
                          }
                        />
                      </div>
                    )}
                    {this.state.activeSecondTab == "tab22" && (
                      <div className="col-sm-12 p-a bg-white">
                        <FormBankDetails
                          user_bank_detail={this.state.user_bank_detail}
                          callUpdateBankDetails={this.callUpdateBankDetails}
                        />
                      </div>
                    )}
                    {this.state.activeSecondTab == "tab23" && (
                      <div className="col-sm-12 p-a bg-white">
                        <FormUpdatePassword
                          callUpdatePassword={this.callUpdatePassword}
                        />
                      </div>
                    )}
                    {this.state.activeSecondTab == "tab24" && (
                      <div className="col-sm-12 p-a bg-white">
                        <ProfilePic
                          file={this.state.file}
                          imageUrl={this.state.imageUrl}
                          documentType={this.state.documentType}
                          token={getToken()}
                          type={this.state.type}
                          handleRomoveImage={this.handleRomoveImage}
                          validateDocuments={this.validateDocuments}
                          successfulFileUpload={this.successfulFileUpload}
                          handleFileChange={this.handleFileChange}
                          loadingProfile={this.uploadingProfilePic}
                          loadingProfileError={this.uploadingProfilePicError}
                        />
                      </div>
                    )}
                  </div>
                </div>
              )}
            </div>
          </div>
        </div>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    frontend: state.frontend.toJS(),
    loggedUser: state.logged_user.userLogin,
    myProfile: state.myProfile.toJS(),
    salary: state.salary.toJS()
  };
}
const mapDispatchToProps = dispatch => {
  return {
    onMyProfileDetails: () => {
      return dispatch(actionsMyProfile.getMyProfileDetails());
    },
    onUpdateBankDetails: new_bank_details => {
      return dispatch(actionsMyProfile.updateBankDetails(new_bank_details));
    },
    onUpdateProfileDetails: new_profile_details => {
      return dispatch(
        actionsMyProfile.updateProfileDetails(new_profile_details)
      );
    },
    onUpdateDeviceDetails: new_device_details => {
      // return dispatch(actionsMyProfile.updateUserDeviceDetails(new_device_details));
    },
    onUpdatePassword: new_password => {
      return dispatch(actionsMyProfile.updatePassword(new_password));
    },
    onSalaryDetails: () => {
      return dispatch(actionsSalary.getSalaryDetails());
    },
    showHeading:(data)=>{
      return dispatch(actions.showHeading(data))
    }
  };
};
MyProfile.propTypes = {
  user_bank_detail: PropTypes.array,
  user_profile_detail: PropTypes.object,
  payslip_history: PropTypes.array,
  user_assign_machine: PropTypes.array
};

export default withRouter(
  connect(mapStateToProps, mapDispatchToProps)(MyProfile)
);
