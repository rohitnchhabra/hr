import React from "react";
import { CONFIG } from "../../../config/index";
import "timepicker/jquery.timepicker.css";
import "timepicker/jquery.timepicker.min.js";
import $ from "jquery";
import Dialog from "material-ui/Dialog";
import { getTimePicker } from "../../../services/generic";

class UserDaySummary extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      current_userid: "",
      current_date: "",
      form_entry_time: "",
      form_exit_time: "",
      form_reason: "",
      formInfo: "",
      inputAccess: "",
      buttonAccess: "show",
      year: "",
      month: "",
      doEditEntryTime:false,
      doEditExitTime:false
    };
    this.doUpdateDaySummary = this.doUpdateDaySummary.bind(this);
    this.timeArray = getTimePicker();
  }
  componentDidMount() {
    $(".timepickerInputTime").timepicker({
      minTime: "09:00 AM",
      maxTime: "09:00 PM",
      timeFormat: "h:i A",
      step: 5
    });
  }
  componentWillReceiveProps(props) {
    if (this.props.loggedUser.data.role === CONFIG.EMPLOYEE) {
      this.setState({ formInfo: "show" });
    } else {
      this.setState({ formInfo: "hidden" });
    }
    if (
      this.props.loggedUser.data.role === CONFIG.EMPLOYEE &&
      props.userDaySummary.entry_time !== "" &&
      props.userDaySummary.exit_time !== ""
    ) {
      this.setState({ inputAccess: "true", buttonAccess: "hidden" });
    } else {
      this.setState({ inputAccess: "" });
    }
    this.setState({
      year: props.year,
      month: props.month,
      current_userid:props.userid.userid,
      current_date: props.userid.full_date,
    });
    if(this.props.userid !==props.userid){
      this.setState({
      form_entry_time:props.userid.in_time && props.userid.in_time.replace(/ /g, ""),
      form_exit_time:props.userid.out_time && props.userid.out_time.replace(/ /g, ""),
      doEditEntryTime:false,
      doEditExitTime:false
      })
    }
  }
  doUpdateDaySummary(evt) {
    evt.preventDefault();
    let {
      current_userid,
      current_date,
      form_entry_time,
      form_exit_time,
      form_reason,
      year,
      month
    } = this.state;
    this.props.requestUpdateUserDaySummary({
      userid:current_userid,
      date: current_date,
      entryTime: form_entry_time,
      exitTime: form_exit_time,
      reason: form_reason,
      year,
      month
    });
    this.setState({
      form_entry_time: "",
      form_exit_time: "",
      form_reason: ""
    })
    this.props.dialog_Close();
    $("#modalUserDaySummary").modal("hide");
  }

  render() {
    const value =
      "Edit Attendance Manually -" +
      this.props.userid.userName +
      " - " +
      this.props.userid.full_date;
    return (
      <div className="employeeAttpage">
        <Dialog
          title={value}
          titleStyle={{ opacity: "0.56" }}
          modal={false}
          open={this.props.dialog_open}
          onRequestClose={this.props.dialog_Close}
          contentStyle={{ width: "70%", maxWidth: "none" , padding:"0px" }}
          autoScrollBodyContent
          id={"modalUserDaySummary"}
        >
              <div className="modal-body p-lg">
                {/* <i className="pl-2">*Entry / Exit time must be like - e.g 10:30 AM, 07:30 PM</i> */}
                <i className={this.state.formInfo}>
                  {
                    "20 min will be added/deducted from your entry/exit time as compensation in case you forgot to push in/out.If there is some other reason for your using this form contact HR"
                  }
                </i>
                <br />
                <br />
                <form
                  className="manual-entry-form"
                  onSubmit={evt => {
                    this.doUpdateDaySummary(evt);
                  }}
                >
                  <div className="form-group row mx-0">
                    <label className="col-sm-3 form-control-label align-self-md-center mb-0">
                      Entry Time
                    </label>
                    <div className="value_input col-sm-9">
                     {!this.props.userid.in_time || this.state.doEditEntryTime? <select 
                        className="form-control"
                        disabled={this.state.inputAccess}
                        value={this.state.form_entry_time}
                        ref="entry_time"
                        onChange={(e) => {
                          this.setState({
                            form_entry_time: e.target.value
                          });
                        }}
                        required
                      >
                        <>
                          <option value={""}>Select Entry Time</option>
                          {this.timeArray &&
                            this.timeArray.map((data, index) => (
                              <option key={index} value={data} className=''>
                                {data}
                              </option>
                            ))}
                        </>
                      </select>:<div className="d-flex justify-content-between align-items-baseline">
                          <div className="text-light">{this.state.form_entry_time}</div>
                          <div><small className="py-1 editLink" onClick={()=>{this.setState({doEditEntryTime:true})}}>(Edit)</small></div>
                        </div>}
                     
                    </div>
                  </div>

                  <div className="form-group row mx-0">
                    <label className="col-sm-3 form-control-label align-self-md-center mb-0">
                      Exit Time
                    </label>
                    <div className="col-sm-9">
                    {!this.props.userid.out_time || this.state.doEditExitTime ?  <select
                          className="form-control"
                          disabled={this.state.inputAccess}
                          value={this.state.form_exit_time}
                          ref="exit_time"
                          onChange={(e) => {
                            this.setState({
                              form_exit_time: e.target.value
                            });
                          }}
                          required
                        >
                          <>
                            <option value={""}>Select Exit Time</option>
                            {this.timeArray &&
                              this.timeArray.map((data, index) => (
                                <option key={index} value={data} className='border py-2'>
                                  {data}
                                </option>
                              ))}
                          </>
                        </select>  :
                        <div className="d-flex justify-content-between align-items-baseline">
                          <div className="text-light">{this.state.form_exit_time}</div>
                          <div><small className="py-1 editLink" onClick={()=>{this.setState({doEditExitTime:true})}}>(Edit)</small></div>
                        </div>
                     
                    }
                      
                    </div>
                  </div>

                  {!this.props.userid.in_time || !this.props.userid.out_time || this.state.doEditEntryTime || this.state.doEditExitTime ?<> <div className="form-group row mx-0">
                    <label className="col-sm-3 form-control-label align-self-md-center mb-0">
                      Reason
                    </label>
                    <div className="col-sm-9">
                      <input
                        type="text"
                        name="reason"
                        className="form-control"
                        ref="reason"
                        disabled={this.state.inputAccess}
                        value={this.state.form_reason}
                        onChange={() =>
                          this.setState({ form_reason: this.refs.reason.value })
                        }
                        required
                      />
                    </div>
                  </div>
                  <div className="form-group row mx-0">
                    <div className="col-sm-12">
                      <div className={this.state.buttonAccess}>
                        <button type="submit" id="submit" class="width-100 mb-xs mr-xs btn btn-success">UPDATE</button>
                      </div>
                    </div>
                  </div></>:null}
                </form>
              </div>
        </Dialog>
      </div>
    );
  }
}

export default UserDaySummary;
