import React from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { withRouter } from "react-router";
import { notify } from "../../../services/notify";
import Header from "../../../components/generic/Header";
import UserMonthlyAttendance from "../../../components/attendance/UserMonthlyAttendance";
import UserDaySummary from "../../../modules/attendance/components/UserDaySummary";
// import * as actionsUsersList from '../../../redux/generic/actions/usersList';
// import * as actionsMonthlyAttendance from '../../../redux/attendance/actions/monthlyAttendance';
// import * as actionsUserDaySummary from '../../../redux/attendance/actions/userDaySummary';
import * as actions from "../../../redux/actions";
import Heading from "../../../components/generic/Heading";

class Home extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      defaultUserDisplay: "",
      daysummary_userid: "",
      daysummary_date: "",
      year: "",
      month: "",
      buttonClicked: "",
      dialog_open:false
    };
    this.onUserClick = this.onUserClick.bind(this);
    this.onShowDaySummary = this.onShowDaySummary.bind(this);
    this.monthToggle = this.monthToggle.bind(this);
  }
  componentDidMount() {
    this.props.requestUsersList();
    let d = new Date();
    let year = d.getFullYear();
    let month = d.getMonth() + 1; // +1 since getMonth starts from 0
    this.setState({ year: year, month: month });
    this.props.showHeading("Employee Attendance")
  }
  componentWillReceiveProps(props) {
    if (props.userDaySummary.status_message !== "") {
      notify(props.userDaySummary.status_message);
    }

    if (this.state.defaultUserDisplay === "") {
      if (props.usersList.users && props.usersList.users.length > 0) {
        let firstUser = props.usersList.users[0];
        let defaultUserId = firstUser.user_Id;
        this.onUserClick(defaultUserId);
      }
    }
  }
  onUserClick(userid) {
    this.setState({ defaultUserDisplay: userid });
    this.props.selectedUser(userid);
    this.props.requestUserAttendance({
      userid,
      year: this.state.year,
      month: this.state.month
    });
    this.dialog_Close();
  }
  monthToggle(u, y, m, check) {
    this.setState({ year: y, month: m, buttonClicked: check });
    this.props.requestUserAttendance({ userid: u, year: y, month: m });
  }
  onShowDaySummary(userid, date) {
    this.setState({ daysummary_userid: userid, daysummary_date: date });
    this.props.requestUserDaySummary({ userid, date });
    this.setState({dialog_open:true});
  }
  dialog_Close =()=> {
    this.setState({dialog_open:false});
  }
  render() {    
    return (
      <div className="emp-attendance-wrapper">
        <UserDaySummary
          userid={this.state.daysummary_userid}
          date={this.state.daysummary_date}
          year={this.state.year}
          month={this.state.month}
          dialog_open={this.state.dialog_open}
          dialog_Close ={this.dialog_Close}
          {...this.props}
        />
        <div id="content" className="app-content box-shadow-z0" role="main">
          <Header
            pageTitle={"Users"}
            showLoading={this.props.frontend.show_loading}
          />
          <div className="app-body" id="view">
              <div className="row mr-0 ml-0">
                <div className="col-md-12 pl-0 pr-0">
                  <UserMonthlyAttendance
                    monthlyAttendance={this.props.monthlyAttendance}
                    monthToggle={this.monthToggle}
                    onShowDaySummary={this.onShowDaySummary}
                    buttonClicked = {this.state.buttonClicked}
                  />
                </div>
              </div>
          </div>
        </div>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    frontend: state.frontend.toJS(),
    loggedUser: state.logged_user.userLogin,
    usersList: state.usersList.toJS(),
    monthlyAttendance: state.monthlyAttendance.toJS(),
    userDaySummary: state.userDaySummary.toJS()
  };
}
const mapDispatchToProps = dispatch => {
  return bindActionCreators(actions, dispatch);
};

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(Home)
);
