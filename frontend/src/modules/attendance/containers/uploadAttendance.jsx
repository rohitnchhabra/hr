import React from "react";
import { connect } from "react-redux";
import { withRouter } from "react-router";
import { CONFIG } from "../../../config/index";
import { isNotUserValid } from "../../../services/generic";
import AlertNotification from "../../../components/generic/AlertNotification";
import Header from "../../../components/generic/Header";
import * as actions from "../../../redux/actions";
import * as actionsUsersList from "../../../redux/generic/actions/usersList";
import * as actionsManageUsers from "../../../redux/manageUsers/actions/manageUsers";
import * as actionsManagePayslips from "../../../redux/salary/actions/managePayslips";
import { Button, Spinner, Label, Alert } from "reactstrap";
import Heading from "../../../components/generic/Heading";
import Dialog from "material-ui/Dialog";
import intersection from "lodash/intersection";
import { notify } from "../../../services/notify";
import groupBy from "lodash/groupBy";
import findIndex from "lodash/findIndex";
import min from "lodash/min";
import max from "lodash/max";
import moment from "moment";
import GenericTable from "../../../components/generic/table/Table";
import Dropzone from "react-dropzone";

const uploadAttendanceList = (
  listItem,
  onChange,
  rowStyle,
  index,
  extraProps
) => {
  let user = extraProps.users.filter(
    (val, index) => val.user_Id == listItem.user_id
  );
  let datesArray = Object.keys(listItem.time);
  let sortedDatesArray = datesArray.sort(function(a, b) {
    return new Date(b) - new Date(a);
  });
  let newArray = sortedDatesArray.slice(0, 3);

  function secondsToHms(d) {
    d = Number(d);
    var h = Math.floor(d / 3600);
    var m = Math.floor((d % 3600) / 60);
    var s = Math.floor((d % 3600) % 60);
    var hDisplay = h > 0 ? h + (h == 1 ? " hr " : " hr ") : "0hr ";
    var mDisplay = m > 0 ? m + (m == 1 ? " min " : " min ") : "0min ";
    var sDisplay = s > 0 ? s + (s == 1 ? " sec" : " sec") : "0sec";
    return hDisplay + mDisplay + sDisplay;
  }

  return (
    <>
      <td>{listItem.user_id}</td>
      <td>{extraProps.users ? (user.length > 0 ? user[0].name : "") : null}</td>
      <td>
        {extraProps.users ? (user.length > 0 ? user[0].work_email : "") : null}
      </td>
      <td>
        {newArray.map((val, i) => {
          return (
            <div>
              <span className="font-weight-bold">{val + ":"}</span>
              {listItem.time[val].entry_time
                ? moment(listItem.time[val].entry_time * 1000).format(
                    "hh:mm:ss A"
                  ) + "/"
                : "null/"}
              {listItem.time[val].exit_time
                ? moment(listItem.time[val].exit_time * 1000).format(
                    "hh:mm:ss A"
                  )
                : "null"}
            </div>
          );
        })}
      </td>

      <td>
        {newArray.map((val, i) => {
          return (
            <div>
              {listItem.time[val].exit_time && listItem.time[val].entry_time
                ? secondsToHms(
                    moment
                      .duration(
                        moment(listItem.time[val].exit_time * 1000).diff(
                          listItem.time[val].entry_time * 1000
                        )
                      )
                      .asSeconds()
                  )
                : "0hr 0min 0sec"}
            </div>
          );
        })}
      </td>
    </>
  );
};

class UploadAttendance extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      dialog: false,
      askForIDField: false,
      askForTimeField: false,
      fields: [],
      userKey: "",
      dateTimeKey: "",
      data: [],
      csv: true,
      newData: [],
      file: ""
    };
  }
  componentWillMount() {
    this.props.getGenericConfig();
    this.props.onUsersList();
    this.props.showHeading("Upload Attendance")
  }
  componentWillReceiveProps(props) {
    window.scrollTo(0, 0);
    let isNotValid = isNotUserValid(
      this.props.location.pathname,
      props.loggedUser
    );
    if (isNotValid.status) {
      this.props.history.push(isNotValid.redirectTo);
    }
  }
  componentDidUpdate(prevProps) {
    if (
      this.props.attendanceUploadSetting.isSuccess &&
      this.props.attendanceUploadSetting.isSuccess !==
        prevProps.attendanceUploadSetting.isSuccess &&
      this.state.data.length
    ) {
      notify("success", "settings updated, Please upload file again");
      this.setState({
        askForIDField: false,
        askForTimeField: false
      });
    }
    if (
      this.props.attendance_status.isSuccess &&
      this.props.attendance_status.isSuccess !==
        prevProps.attendance_status.isSuccess
    ) {
      notify("success", "Attendance Uploaded Successfully.");
      this.setState({
        dialog: false,
        file: ""
      });
    }
  }

  handleSubmit = () => {
    window.open(CONFIG.upload_attendance_url);
  };

  handleDialogClose = () => {
    this.setState({ dialog: false });
  };

  handleIDFieldChange = e => {
    this.setState({ userKey: e.target.value });
  };

  handleTimeFieldChange = e => {
    this.setState({ dateTimeKey: e.target.value });
  };

  handleAddClick = params => {
    this.props.requestAddAttendanceUploadSetting(params);
  };

  newData = () => {
    if (this.state.data.length > 1) {
      const { csv, data: attendance } = this.state;
      const attendance_fields = csv
        ? attendance[0].replace('"', "").split(/","|,/)
        : attendance[0].split(/\s+/);
      const { userIDFields, timeFields } = this.state;
      const userIdIndex = attendance_fields.indexOf(userIDFields[0]),
        timeIndex = attendance_fields.indexOf(timeFields[0]);
      const newData = [];
      attendance.slice(1, attendance.length).forEach((v, i) => {
        const attendance_formatted = csv
          ? v.replace('"', "").split(/","|,/)
          : v.split(/[\t]/);
        const found = findIndex(
          newData,
          item => attendance_formatted[userIdIndex] == item.user_id
        );
        if (
          [attendance_formatted[timeIndex]] &&
          moment(attendance_formatted[timeIndex], "MM-DD-YYYY hh:mm:ssa")
            .isValid
        ) {
          if (found > -1) {
            newData[found].time.push(
              csv
                ? attendance_formatted[timeIndex]
                : moment(
                    attendance_formatted[timeIndex],
                    "MM-DD-YYYY hh:mm:ssa"
                  ).unix()
            );
          } else {
            if (attendance_formatted[userIdIndex]) {
              newData.push({
                user_id: attendance_formatted[userIdIndex],
                time: csv
                  ? [attendance_formatted[timeIndex]]
                  : [
                      moment(
                        attendance_formatted[timeIndex],
                        "MM-DD-YYYY hh:mm:ssa"
                      ).unix()
                    ]
              });
            }
          }
        }
      });
      this.setState({
        newData: newData
      });
      newData.forEach((v, i) => {
        newData[i].time = groupBy(v.time, time =>
          moment(time * 1000).format("YYYY-MM-DD")
        );
      });
      newData.forEach((v, i) => {
        for (let date in v.time) {
          newData[i].time[date] = {
            entry_time: min(v.time[date]),
            exit_time: v.time[date].length > 1 ? max(v.time[date]) : null
          };
        }
      });
    }
  };

  showData = () => {
    const { csv, data: attendance } = this.state;
    const attendance_fields = csv
      ? attendance[0].replace('"', "").split(/","|,/)
      : attendance[0].split(/\s+/);
    const { userIDFields, timeFields } = this.state;
    const userIdIndex = attendance_fields.indexOf(userIDFields[0]),
      timeIndex = attendance_fields.indexOf(timeFields[0]);
    let attendance_formatted = attendance
      .slice(1, attendance.length)
      .map((v, i) => {
        return csv ? v.replace('"', "").split(/","|,/) : v.split(/[\t]/);
      });
    return (
      // <table className="table">
      //   <thead>
      //     <tr>
      //       <th>{userIDFields[0]}</th>
      //       <th>{timeFields[0]}</th>
      //     </tr>
      //   </thead>
      //   <tbody>
      //     {attendance_formatted.map((v, i) => (
      //       <tr key={i}>
      //         <td>{v[userIdIndex]}</td>
      //         <td>{v[timeIndex]}</td>
      //       </tr>
      //     ))}
      //   </tbody>
      // </table>
      <>
        <GenericTable
          list={uploadAttendanceList}
          headerData={["user id", "name", "email", "timing", "duration"]}
          tableListData={this.state.newData}
          users={this.props.usersList.users}
        />
      </>
    );
  };

  parseFile = e => {
    // var regex = /.+(\.csv|.txt)$/;
    if (
      e[0].name.split(".")[e[0].name.split(".").length - 1] == "csv" ||
      "txt"
    ) {
      let csv = true;
      if (e[0].type === "text/plain") {
        this.setState({ csv: false });
        csv = false;
      }
      const reader = new FileReader();
      reader.onload = event => {
        const dat = event.target.result.split(/\r?\n/);
        const data = this.props.attendanceUploadSetting;
        let userIDFields, timeFields;

        const fields = csv
          ? dat[0].replace('"', "").split(/","|,/)
          : dat[0].split(/[\t]/);
        userIDFields = intersection(fields, data.user_id);
        timeFields = intersection(fields, data.time);
        if (userIDFields.length && timeFields.length) {
          this.setState({
            fields,
            userIDFields,
            timeFields,
            data: dat,
            askForIDField: false,
            askForTimeField: false,
            file: e
          });
        } else {
          this.setState({ fields });
        }
        if (
          data &&
          dat[0] &&
          data.time &&
          data.time.length &&
          data.user_id &&
          data.user_id.length
        ) {
          if (!userIDFields.length) {
            this.setState({ askForIDField: true });
          }
          if (!timeFields.length) {
            this.setState({ askForTimeField: true });
          }
        }
        if (!data.time.length) {
          this.setState({ askForTimeField: true });
        }
        if (!data.user_id.length) {
          this.setState({ askForIDField: true });
        }
        this.setState({ dialog: true }, this.newData());
      }; // desired file content
      reader.onerror = error => {
        notify("error", "Something went wrong!", "error");
      };
      reader.readAsText(e[0]);
    } else {
      notify("error", "file not CSV");
    }
    // e.target.value = "";
  };

  render() {
    return (
      <div className="uploadAttendance">
        <AlertNotification message={this.props.manageUsers.status_message} />
        <Header
          pageTitle={"UPLOAD ATTENDANCE SHEET"}
          showLoading={this.props.frontend.show_loading}
        />

        {/* <Dialog
          title="Upload Attendance Summary"
          classes="pt-5"
          modal={false}
          open={this.state.dialog || this.props.attendance_status.isLoading}
          onRequestClose={this.handleDialogClose}
          autoScrollBodyContent
        ></Dialog> */}
        <div className="row no-gutters">
          {/* <h6 className="px-2">
              UPLOAD<span className="font-weight-bold">ATTENDANCE</span>
            </h6> */}
          <Dropzone onDrop={this.parseFile}>
            {({ getRootProps, getInputProps }) => {
              return (
                <section
                  className={
                    this.state.dialog ? "col-sm-10 col-12" : "col-sm-12 col-12"
                  }
                >
                  <div {...getRootProps({ className: "dropzone" })}>
                    <input {...getInputProps()} />
                    <div className="drag_and_drop">
                      {this.state.file &&
                        this.state.file.map((file, index) => (
                          <div className="font-weight-bold text-center"> {file.name}</div>
                        ))}
                      {/* <p className="uploading_doc text-center">
                        <i className="fi flaticon-upload text-center" />
                      </p> */}

                      <p className="doc_upload_place text-center mb-1 p-0">
                        Drop a document here or click to select file to upload
                      </p>
                    </div>
                  </div>
                </section>
              );
            }}
          </Dropzone>
          {/* <div className="text-center">
              <Button
                onClick={() => {
                  document.getElementById("attendance-file").click();
                }}
                disabled={this.props.frontend.show_loading}
                color="primary"
                outline
              >
                Browse Files
              </Button>
              <input
                type="file"
                className="hidden"
                id="attendance-file"
                onChange={this.parseFile}
              />
            </div> */}
          {this.state.dialog || this.props.attendance_status.isLoading ? (
            <div className="col-sm-2 col-12 pl-sm-2 mt-2 mt-sm-0">
              {" "}
              {!this.state.askForIDField &&
              !this.state.askForTimeField &&
              this.state.data.length ? (
                <>
                  {this.props.attendance_status.isLoading ? (
                    <Spinner color="black" />
                  ) : (
                    <>
                      <Button
                        className="w-100"
                        color="primary"
                        onClick={() =>
                          this.props.uploadAttendance(this.state.newData)
                        }
                      >
                        Submit
                      </Button>
                    </>
                  )}
                </>
              ) : null}
            </div>
          ) : null}
        </div>
        <div className="row no-gutters mt-3">
          <div className="col-md-12 col-12 px-0">
            {this.state.dialog || this.props.attendance_status.isLoading ? (
              <div>
                {this.state.askForIDField ? (
                  <div className="form-group mb-0 bg-white p-2">
                    <Label for="select field">
                      {" "}
                      Select a field for User ID
                    </Label>
                    <select
                      className="form-control"
                      onChange={this.handleIDFieldChange}
                      // value={this.state.gender}
                    >
                      <option value="">Select a field</option>
                      {this.state.fields.map(item => (
                        <option value={item}>{item}</option>
                      ))}
                    </select>
                  </div>
                ) : null}
                {this.state.askForTimeField ? (
                  <div className="form-group bg-white p-2 mb-0">
                    <Label for="select field"> Select a field for Time</Label>
                    <select
                      className="form-control"
                      onChange={this.handleTimeFieldChange}
                      // value={this.state.gender}
                    >
                      <option value="">Select a field</option>
                      {this.state.fields.map(item => (
                        <option value={item}>{item}</option>
                      ))}
                    </select>
                  </div>
                ) : null}
                {!this.state.askForIDField &&
                !this.state.askForTimeField &&
                this.state.data.length ? (
                  <div>
                    {/* {this.props.attendance_status.isLoading ? (
                      <Spinner color="black" />
                    ) : (
                      <div className="col-md-12 px-0 py-2">
                        <Button
                          color="primary"
                          onClick={() =>
                            this.props.uploadAttendance(this.state.newData)
                          }
                        >
                          Submit
                        </Button>
                      </div>
                    )} */}

                    {this.props.usersList.users && this.showData()}
                  </div>
                ) : (
                  <div className="bg-white p-2">
                    <Button
                      color="primary"
                      onClick={() => {
                        this.handleAddClick({
                          userid_key: this.state.userKey,
                          timing_key: this.state.dateTimeKey
                        });
                        this.setState({ dialog: false });
                      }}
                    >
                      Submit
                    </Button>
                  </div>
                )}
              </div>
            ) : null}
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  frontend: state.frontend.toJS(),
  managePayslips: state.managePayslips.toJS(),
  loggedUser: state.logged_user.userLogin,
  usersList: state.usersList.toJS(),
  manageUsers: state.manageUsers.toJS(),
  attendanceUploadSetting: state.settings.attendanceUploadSetting,
  attendance_status: state.settings.uploadAttendance,
  attendance: state.attendance
});

const mapDispatchToProps = dispatch => ({
  onUsersList: () => dispatch(actionsUsersList.get_users_list()),
  onUserProfileDetails: (userid, username) =>
    dispatch(actionsManageUsers.getUserProfileDetails(userid, username)),
  onUpdateUserBankDetails: newBankDetails => {
    // return dispatch(actionsManageUsers.updateUserBankDetails(newBankDetails));
  },
  onUpdateUserProfileDetails: newProfileDetails =>
    dispatch(actionsManageUsers.updateUserProfileDetails(newProfileDetails)),
  onAddNewEmployee: newEmployeeDetails =>
    dispatch(actionsManageUsers.addNewEmployee(newEmployeeDetails)),
  onUpdatedocuments: documentLink => {
    // return dispatch(actionsManageUsers.updateDocument(documentLink));
  },
  onChangeEmployeeStatus: (userid, status) =>
    dispatch(actionsManageUsers.changeEmployeeStatus(userid, status)),
  onGetUserDocument: userid =>
    dispatch(actionsManageUsers.getUserDocument(userid)),
  onDeleteDocument: docId => dispatch(actionsManageUsers.deleteDocument(docId)),
  onUserManagePayslipsData: userid =>
    dispatch(actionsManagePayslips.get_user_manage_payslips_data(userid)),
  // getAttendanceUploadSettings: () =>
  //   dispatch(actions.requestGetAttendanceUploadSetting()),
  requestAddAttendanceUploadSetting: params =>
    dispatch(actions.requestAddAttendanceUploadSetting(params)),
  uploadAttendance: params => dispatch(actions.uploadAttendance(params)),
  getGenericConfig: () => dispatch(actions.getGenricLoginConfig()),
  showHeading: data => {
    return dispatch(actions.showHeading(data));
  }
});

const VisibleUploadAttendance = connect(
  mapStateToProps,
  mapDispatchToProps
)(UploadAttendance);

const RouterVisibleUploadAttendance = withRouter(VisibleUploadAttendance);

export default RouterVisibleUploadAttendance;
