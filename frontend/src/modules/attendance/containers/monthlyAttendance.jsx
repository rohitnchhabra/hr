import React from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { withRouter } from "react-router";
import { isNotUserValid } from "../../../services/generic";
import Header from "../../../components/generic/Header";
import EmpDaySummary from "../../../modules/attendance/components/empDaySummary";
import UserMonthlyAttendance from "../../../components/attendance/UserMonthlyAttendance";
// import * as actionsUserDaySummary from '../../../redux/attendance/actions/userDaySummary';
import * as actions from "../../../redux/actions";
import * as actionsManageUsers from "../../../redux/manageUsers/actions/manageUsers";
import isEqual from "lodash/isEqual";
import Heading from "../../../components/generic/Heading";

class MonthlyAttendance extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      defaultUserDisplay: "",
      daysummary_userid: "",
      daysummary_date: "",
      year: "",
      month: "",
      buttonClicked: "",
      in_time: "", 
      out_time: ""
    };
    this.onUserClick = this.onUserClick.bind(this);
    this.onShowDaySummary = this.onShowDaySummary.bind(this);
    this.monthToggle = this.monthToggle.bind(this);
  }
  componentWillMount() {
    const { id } = this.props.loggedUser.data;
    this.setState({ defaultUserDisplay: id });
    let d = new Date();
    let year = d.getFullYear();
    let month = d.getMonth() + 1; // +1 since getMonth starts from 0
    this.setState({ year: year, month: month });
    this.props.requestUserAttendance({
      userid: id,
      year: year,
      month: month
    });
    this.props.showHeading("My Attendance")
  }
  componentWillReceiveProps(props) {
    const { id, username } = this.props.loggedUser.data.id;
    // const userId = props.loggedUser.data.id;
    let isNotValid = isNotUserValid(
      this.props.location.pathname,
      props.loggedUser
    );
    if (isNotValid.status) {
      this.props.history.push(isNotValid.redirectTo);
    }
    if (
      id &&
      !isEqual(props.loggedUser.data.id, this.props.loggedUser.data.id)
    ) {
      this.props.onUserProfileDetails(id, username);
      this.props.requestUserAttendance({
        userid: id,
        year: this.state.year,
        month: this.state.month
      });
    }
  }
  onShowDaySummary(data) {
    this.setState({ daysummary_userid: data.userid, daysummary_date: data.full_date, in_time:data.in_time, out_time:data.out_time });
    this.props.requestUserDaySummary({ userid:data.userid, date:data.full_date });
  }
  onUserClick(userid) {
    this.setState({ defaultUserDisplay: userid });
    this.props.requestUserAttendance({
      userid,
      year: this.state.year,
      month: this.state.month
    });
  }
  monthToggle(u, y, m, check) {
    this.setState({ year: y, month: m, buttonClicked: check });
    this.props.requestUserAttendance({ userid: u, year: y, month: m });
  }
  render() {
    let doj = this.props.manageUsers.user_profile_detail.dateofjoining

    return (
      <div className="emp-attandance-wrapper">
        <EmpDaySummary
          userid={this.state.daysummary_userid}
          date={this.state.daysummary_date}
          year={this.state.year}
          month={this.state.month}
          in_time={this.state.in_time}
          out_time={this.state.out_time}
          {...this.props}
        />
        <Header
          pageTitle={"My Attendance"}
          showLoading={this.props.frontend.show_loading}
        />
        <UserMonthlyAttendance
          monthlyAttendance={this.props.monthlyAttendance}
          monthToggle={this.monthToggle}
          onShowDaySummary={this.onShowDaySummary}
          buttonClicked={this.state.buttonClicked}
          doj={doj}
        />
      </div>
    );
  }
}

MonthlyAttendance.styles = {
  height100per: {
    minHeight: "150px"
  }
};

function mapStateToProps(state) {
  return {
    frontend: state.frontend.toJS(),
    userDaySummary: state.userDaySummary.toJS(),
    empDaySummary: state.empDaySummary,
    loggedUser: state.logged_user.userLogin,
    monthlyAttendance: state.monthlyAttendance.toJS(),
    manageUsers: state.manageUsers.toJS(),
    usersList: state.usersList.toJS()
  };
}
const mapDispatchToProps = dispatch => {
  return {
    ...bindActionCreators(actions, dispatch),
    onUserProfileDetails: (userid, username) => {
      return dispatch(
        actionsManageUsers.getUserProfileDetails(userid, username)
      );
    },
    showHeading: data => {
      return dispatch(actions.showHeading(data));
    },
  };
};

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(MonthlyAttendance)
);
