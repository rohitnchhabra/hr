import React from "react";
import Header from "../../../components/generic/Header";
import { connect } from "react-redux";
import { withRouter } from "react-router";
import DeviceCounterTab from "../../../components/inventory/DeviceCounterTab";
import * as actionsManageDevice from "../../../redux/inventory/actions/inventory";
import * as actions from '../../../redux/actions';
import { notify } from "../../../services/notify";
import { compose } from "redux";
import isMobile from "../../../components/hoc/WindowResize";
import { Alert } from "reactstrap";

class InventoryOverviewContainer extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  componentWillMount() {
    this.props.onFetchDeviceStatus().then(() => {
      this.props.onFetchDeviceCount();
      this.props.onFetchDeviceType();
    });
    this.props.showHeading("Inventory")
  }
  callAddDevice = deviceType => {
    this.props.onCallDeviceType(deviceType).then(
      message => {
        this.setState({
          status_message: message
        });
        this.props.onFetchDeviceType();
      },
      error => {
        notify("Error !", error, "error");
      }
    );
  };
  render() {
    const { deleteStatus } = this.props.manageDevice;
    
    return (
      <div className="inventry-overview-container">
        <div id="content" className="app-content box-shadow-z0 ." role="main">
          <Header pageTitle={"Inventory Overview"} />
          {deleteStatus.error === 1 && (
            <Alert color="danger">Error Occurs While Deleting Sold Item</Alert>
          )}
          <div>
            <div className={this.state.viewUserNew}>
              <DeviceCounterTab
                statusList={this.props.manageDevice.statusList}
                deviceCountList={this.props.manageDevice.deviceCountList}
                history={this.props.history}
                callAddDevice={this.callAddDevice}
                deviceTypeList={this.props.manageDevice.deviceList}
                onCallDeviceType={this.props.onCallDeviceType}
                isMobile={this.props.isMobile}
                handleDeleteSoldDevice={this.handleDeleteSoldDevice}
              />
            </div>
          </div>
        </div>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    loggedUser: state.logged_user.userLogin,
    manageDevice: state.manageDevice.toJS()
  };
}
const mapDispatchToProps = dispatch => {
  return {
    onFetchDeviceStatus: () => {
      return dispatch(actionsManageDevice.getDeviceStatus());
    },
    onFetchDeviceCount: () => {
      return dispatch(actionsManageDevice.deviceCount());
    },
    onFetchDeviceType: () => {
      return dispatch(actionsManageDevice.getDeviceType());
    },
    onCallDeviceType: deviceList => {
      return dispatch(actionsManageDevice.assignDeviceType(deviceList));
    },
    showHeading:(data)=>{
      return dispatch(actions.showHeading(data))
    }
  };
};

export default compose(
  connect(
    mapStateToProps,
    mapDispatchToProps
  ),
  isMobile,
  withRouter
)(InventoryOverviewContainer);

// const InventoryOverviewContainerData = connect(
//   mapStateToProps,
//   mapDispatchToProps
// )(InventoryOverviewContainer);
