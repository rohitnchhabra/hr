import React, { Component } from "react";
import CalendarWeek from "../../components/generic/employeeMonthGrid/CalendarWeek";
import TimeSheetData from "../../components/userTimeSheet/TimeSheetData";
import * as actions from "../../redux/actions";
import { Badge, Button, Spinner, Alert } from "reactstrap";
import Toolbar from "../../components/generic/Toolbar";
import { connect } from "react-redux";
import AlertFader from "../../components/generic/AlertFader";
import { confirm } from "../../services/notify";
import {
  getFromDate,
  getMonthName,
  getMonday,
  getNextMonady
} from "../../services/generic";

class timeSheetcalendar extends Component {
  state = {
    from_date: new Date(),
    full_date: "",
    user_id: "",
    buttonClicked: ""
  };

  componentDidMount() {
    let user_id = localStorage.getItem("userid");
    this.setState({
      user_id: user_id
    });
    let todaysDate = new Date();
    let todaysDay = todaysDate.getDay();
    if (todaysDay == 1) {
      var from_date = getFromDate(new Date());
      this.setState({
        from_date: todaysDate
      });
    } else {
      let mondayDate = getMonday(new Date());
      var from_date = getFromDate(mondayDate);
      this.setState({
        from_date: mondayDate
      });
    }
    this.props.userTimeSheetRequest({ from_date, user_id: user_id });
    this.props.showHeading("Weekly Timesheet");
  }
  componentDidUpdate(prevProps) {
    if (
      this.props.submitWeeklyTimeSheetData.data !==
      prevProps.submitWeeklyTimeSheetData.data
    ) {
      this.setState(
        {
          status: this.props.submitWeeklyTimeSheetData.data
        },
        () => {
          this.setState({
            status: ""
          });
        }
      );
    }
    if (
      this.props.userTimeSheetSentData.data !==
      prevProps.userTimeSheetSentData.data
    ) {
      this.setState(
        {
          status: this.props.userTimeSheetSentData.data
        },
        () => {
          this.setState({
            status: ""
          });
        }
      );
    }
  }

  onShowDaySummary = data => {
    if (data.status !== "Pending" && data.status !== "Approved" && new Date(data.full_date) <= new Date()) {
      this.setState({
        user_id: data.userid,
        full_date: data.full_date
      });
      this.props.getUserTmsReport({
        date: data.full_date,
        username: data.userName
      });
    }
  };
  onSendUserTimeSheetdata = data => {
    this.props.onSendUserTimeSheetdata(data);
  };

  _onChangeWeek = data => {
    if (data == "previous") {
      let previousDate = getMonday(this.state.from_date);
      var from_date = getFromDate(previousDate);
      this.setState(
        { buttonClicked: "previous", from_date: previousDate },
        () => {
          this.props.userTimeSheetRequest({
            from_date: from_date,
            user_id: this.state.user_id
          });
        }
      );
    } else {
      let nextDate = getNextMonady(this.state.from_date);
      var from_date = getFromDate(nextDate);
      this.setState({ buttonClicked: "next", from_date: nextDate }, () => {
        this.props.userTimeSheetRequest({
          from_date: from_date,
          user_id: this.state.user_id
        });
      });
    }
  };
  submitWeeklyTimeSheet = () => {
    confirm(
      "Are you sure you want to submit, as timesheet cannot be changed once you submit"
    ).then(res => {
      if (res) {
        var from_date = getFromDate(this.state.from_date);
        this.props.doSubmitWeeklyTimeSheet({
          from_date,
          user_id: this.state.user_id
        });
      }
    });
  };
  checkStatus = () => {
    const { userTimeSheetGetData } = this.props;
    if (userTimeSheetGetData.data) {
      if (userTimeSheetGetData.data.data) {
        let savedStatus = userTimeSheetGetData.data.data.filter((v, i) => {
          return v.status == "Saved";
        });
        if (savedStatus.length) {
          return "saved";
        } else {
          let rejectedStatus = userTimeSheetGetData.data.data.filter((v, i) => {
            return v.status === "Rejected";
          });
          if (rejectedStatus.length) {
            return "rejected";
          } else {
            let pendingStatus = userTimeSheetGetData.data.data.filter(
              (v, i) => {
                return v.status === "Pending";
              }
            );
            if (pendingStatus.length) {
              return "pending";
            } else {
              return "approved";
              // let approvedStatus = userTimeSheetGetData.data.data.filter(
              //   (v, i) => {
              //     return v.status === "Approved";
              //   }
              // );
              // if (approvedStatus.length) {
              //   return "approved";
              // }
            }
          }
        }
      }
    }
  };
  render() {
    const { userTimeSheetGetData, submitWeeklyTimeSheetData } = this.props;
    const { buttonClicked } = this.state;
    const checkRejectedDates =
      userTimeSheetGetData.data &&
      userTimeSheetGetData.data.data.filter((v, i) => v.status == "Rejected");
    const checkPendingDates =
      userTimeSheetGetData.data &&
      userTimeSheetGetData.data.data.filter((v, i) => v.status == "Pending");
    return (
      <div>
        <AlertFader>
          {this.state.status ? (
            <Alert
              className="alert-transparent rem-fade rh-section rh-alert"
              color={this.state.status.error ? "danger" : "success"}
            >
              {this.state.status.message}
            </Alert>
          ) : null}
        </AlertFader>
        <Toolbar
          isLoading={userTimeSheetGetData.isLoading}
          buttonClicked={buttonClicked}
          _onChangeWeek={this._onChangeWeek}
          date={this.state.from_date}
          timeSheet={true}
          showWeek={true}
        />
        {this.checkStatus() !== "approved" &&
          userTimeSheetGetData.data &&
          userTimeSheetGetData.data.data && (
            <div className="pt-3 d-sm-flex justify-content-end">
              <Button
                color="success"
                onClick={this.submitWeeklyTimeSheet}
                disabled={
                  this.checkStatus() == "rejected" ||
                  this.checkStatus() == "pending"
                }
              >
                {submitWeeklyTimeSheetData.isLoading ? (
                  <Spinner />
                ) : this.checkStatus() === "pending" ? (
                  "Pending for Approval"
                ) : (
                  "Submit Weekly Time Sheet"
                )}
              </Button>
            </div>
          )}
        <TimeSheetData
          {...this.state}
          onSendUserTimeSheetdata={this.onSendUserTimeSheetdata}
          userTimeSheetSentData={this.props.userTimeSheetSentData}
          tmsReportData={this.props.tmsReportData}
        />
        <div className="timeSheet-calendar-structure pt-3">
          {userTimeSheetGetData.data &&
            userTimeSheetGetData.data.data.map((dayData, i) => (
              <div>
                <div className="timeSheet-weekdays">{dayData.day}</div>
                <CalendarWeek
                  userId={this.state.user_id}
                  dayData={{
                    ...dayData,
                    admin_alert: 0
                  }}
                  userName={dayData.username}
                  timeSheet={true}
                  onShowDaySummary={this.onShowDaySummary}
                  timeSheet={true}
                  className={"timesheet-calendar"}
                  id={"timesheet-calendar"}
                  isDisable={
                    checkRejectedDates.length
                      ? false
                      : checkPendingDates.length
                      ? true
                      : false
                  }
                />
                <div className="timesheetImage">
                  {dayData.status ? (
                    <div className="py-2 ">
                      <Badge
                        className="d-block"
                        color={
                          dayData.status == "Approved"
                            ? "success"
                            : dayData.status == "Rejected"
                            ? "danger"
                            : "secondary"
                        }
                      >
                        {" "}
                        Status: {dayData.status}
                      </Badge>
                    </div>
                  ) : null}
                  {dayData.file ? (
                    <div>
                      <img className="w-100" src={dayData.file} />
                    </div>
                  ) : null}

                  {dayData.comments && (
                    <div className="py-2">
                      <pre>{dayData.comments}</pre>
                    </div>
                  )}
                </div>
              </div>
            ))}
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    userTimeSheetGetData: state.timeSheet.userTimeSheetGetData,
    userTimeSheetSentData: state.timeSheet.userTimeSheetSentData,
    submitWeeklyTimeSheetData: state.timeSheet.submitWeeklyTimeSheet,
    tmsReportData: state.timeSheet.tmsReportData
  };
};
const mapDispatchToProps = dispatch => {
  return {
    userTimeSheetRequest: data => {
      dispatch(actions.userTimeSheetRequest(data));
    },
    onSendUserTimeSheetdata: data => {
      dispatch(actions.sendTimeSheetDataRequest(data));
    },
    showHeading: data => {
      dispatch(actions.showHeading(data));
    },
    doSubmitWeeklyTimeSheet: data => {
      dispatch(actions.submitWeeklyTimeSheetRequest(data));
    },
    getUserTmsReport: data => {
      dispatch(actions.requestGetUserTmsReport(data));
    }
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(timeSheetcalendar);
