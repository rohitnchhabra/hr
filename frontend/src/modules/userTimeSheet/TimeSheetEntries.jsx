import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import {
  Button,
  ListGroup,
  ListGroupItem,
  ListGroupItemHeading,
  ListGroupItemText,
  Card,
  Badge,
  Alert,
} from "reactstrap";
import * as actions from "../../redux/actions";
import Toolbar from "../../components/generic/Toolbar";
import { getFromDate, getMonday, getNextMonady } from "../../services/generic";

export default function TimeSheetEntries(props) {
  const dispatch = useDispatch();
  const selected_date = new Date();
  const [date, setDate] = useState(
    props.location.date ? new Date(props.location.date) : new Date()
  );
  const userSelected = useSelector((state) => state.userListSidebar);
  const [alertMsg, setAlertMsg] = useState("");
  const [buttonClicked, onButtonClick] = useState("");
  const approveRejectTimeSheet = useSelector(
    (state) => state.timeSheet.approveRejectTimeSheet
  );
  const usersList = useSelector((state) => state.usersList.toJS());
  const submittedTimeSheetData = useSelector(
    (state) => state.timeSheet.submittedTimeSheetData
  );
  useEffect(() => {
    dispatch(actions.requestUsersList());
    dispatch(actions.showHeading("Timesheet Entries"));
  }, []);
  useEffect(() => {
    if (userSelected.userId) {
      let todaysDate = date;
      let todaysDay = todaysDate.getDay();
      if (todaysDay == 1) {
        var from_date = getFromDate(date);

        setDate(todaysDate);
      } else {
        let mondayDate = getMonday(date);
        var from_date = getFromDate(mondayDate);
        setDate(mondayDate);
      }
      dispatch(
        actions.getSubmittedTimeSheetRequest({
          user_id: userSelected.userId,
          from_date,
        })
      );
    }
  }, [userSelected.userId]);

  useEffect(() => {
    if (approveRejectTimeSheet.isSuccess && date) {
      var from_date = getFromDate(date);
      dispatch(
        actions.getSubmittedTimeSheetRequest({
          user_id: userSelected.userId,
          from_date,
        })
      );
    }
  }, [approveRejectTimeSheet.isSuccess]);

  useEffect(() => {
    if (submittedTimeSheetData.isError) {
      setAlertMsg(submittedTimeSheetData.data);
    }
    if (submittedTimeSheetData.isSuccess) {
      setAlertMsg("");
    }
  }, [submittedTimeSheetData.isError, submittedTimeSheetData.isSuccess]);

  const approveRejectTime = (item, status) => {
    dispatch(
      actions.approveRejectTimeSheetRequest({
        user_id: userSelected.userId,
        date: item.full_date,
        status,
      })
    );
  };

  const _onChangeWeek = (data) => {
    if (data == "previous") {
      let previousDate = getMonday(date);
      var from_date = getFromDate(previousDate);
      onButtonClick("previous");
      setDate(previousDate);
      dispatch(
        actions.getSubmittedTimeSheetRequest({
          user_id: userSelected.userId,
          from_date,
        })
      );
    } else {
      let nextDate = getNextMonady(date);
      var from_date = getFromDate(nextDate);
      onButtonClick("next");
      setDate(nextDate);
      dispatch(
        actions.getSubmittedTimeSheetRequest({
          user_id: userSelected.userId,
          from_date,
        })
      );
    }
  };

  const user = usersList.users
    ? usersList.users.find((v) => v.user_Id === userSelected.userId)
    : {};
  const timeSheetEntryData = submittedTimeSheetData &&
  submittedTimeSheetData.isSuccess &&
  submittedTimeSheetData.data ?
  submittedTimeSheetData.data.filter((v,i)=>
    v.status
  ):[]

  const approveRejectFullTimeSheet=(status)=>{
    dispatch(
      actions.approveRejectFullTimeSheetRequest({
        user_id: userSelected.userId,
        from_date: getFromDate(date),
        status,
      })
    );
  }

  const show_ApproveRejectWeeklyTimeSheetButton = timeSheetEntryData.length ? timeSheetEntryData.find((v,i)=>
    v.status=="Pending"
  ):null

  return (
    <div>
      {user ? (
        <Card className="p-4 mb-2">
          <div>
            {user.image ? (
              <span className="thumb-sm">
                <img className="rounded-circle" src={user.image} alt="" />
              </span>
            ) : null}
            <span className="ml-3">{user.name}</span>
          </div>
        </Card>
      ) : null}
      {alertMsg.error && (
        <Alert className="mt-1" color="danger">
          {alertMsg.message}
        </Alert>
      )}
     
      <Toolbar
        isLoading={submittedTimeSheetData.isLoading}
        buttonClicked={buttonClicked}
        _onChangeWeek={_onChangeWeek}
        date={date}
        showWeek={true}
      />
       <div className="d-flex my-3 justify-content-end">
        {show_ApproveRejectWeeklyTimeSheetButton ?<><Button
          onClick={() => approveRejectFullTimeSheet("Approved")}
          color="primary"
        >
          Approve Weekly Timesheet
        </Button>
        <Button
          className="ml-2"
          onClick={() => approveRejectFullTimeSheet("Rejected")}
          color="danger"
        >
          Reject Weekly Timesheet
        </Button></>:null}
        
      </div>
      <ListGroup>
        {timeSheetEntryData.length ? timeSheetEntryData.map(v => (
            <ListGroupItem color="info">
              <div className="d-sm-flex justify-content-between">
                <div className="mb-2">
                  <ListGroupItemHeading>
                    {v.full_date} ({v.day})
                    {v.status ? <Badge> Status: {v.status}</Badge> : null}
                  </ListGroupItemHeading>
                  <ListGroupItemText>
                    {v.total_hours ? v.total_hours + " hours" : null}
                  </ListGroupItemText>
                </div>
                <div className="pb-2 pb-sm-0">
                  {v.status === "Pending" ? (
                    <>
                      <Button
                        onClick={() => approveRejectTime(v, "Approved")}
                        color="primary"
                      >
                        Approve
                      </Button>
                      <Button
                        className="ml-2"
                        onClick={() => approveRejectTime(v, "Rejected")}
                        color="danger"
                      >
                        Reject
                      </Button>
                    </>
                  ) : null}
                </div>
              </div>
              <div className="d-flex flex-column">
                {v.file ? (
                  <a className="mb-2" href={v.file} target="_blank">
                    <img className="timesheetEntryImage w-100" src={v.file} alt="" />
                  </a>
                ) : null}
                {v.comments ? <pre>{v.comments}</pre> : null}
              </div>
            </ListGroupItem>
          )):null}
      </ListGroup>
    </div>
  );
}
