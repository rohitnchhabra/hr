import React from "react";
import {
  Table,
  Card,
  CardBody,
  CardTitle,
  Spinner,
  Button,
  Alert,
} from "reactstrap";
import Toolbar from "../../components/generic/Toolbar";
import {
  getFromDate,
  getMonday,
  getNextMonady,
  getFormattedMonth,
} from "../../services/generic";
import * as actions from "../../redux/actions";
import { useDispatch, useSelector } from "react-redux";
import * as avatar from "../../static/avatar.png";

export default function TimeSheetPerWeek(props) {
  const dispatch = useDispatch();
  const [month, setMonth] = React.useState(new Date().getMonth());
  const [year, setYear] = React.useState(new Date().getFullYear());
  const [buttonClicked, onButtonClick] = React.useState("");
  const timeSheetPerWeekData = useSelector(
    (state) => state.timeSheet.timeSheetPerWeekData
  );
  const _onChangeWeek = (check) => {
    if (check === "previous") {
      if (month === 0) {
        setMonth(11);
        setYear(year - 1);
      } else {
        setMonth(month - 1);
      }
      onButtonClick("previous");
    } else if (check === "next") {
      if (month === 11) {
        setMonth(0);
        setYear(year + 1);
      } else {
        setMonth(month + 1);
      }
      onButtonClick("next");
    }
  };

  React.useEffect(() => {
    dispatch(actions.showHeading("User Timesheets"));
  }, []);

  React.useEffect(() => {
    dispatch(
      actions.getTimeSheetPerWeekRequest({
        month: getFormattedMonth(month + 1),
        year: year.toString(),
      })
    );
  }, [month, year]);

  // if() {
  //   return <Spinner color="black" />
  // }

  return (
    <div>
      <Toolbar
        isLoading={timeSheetPerWeekData.isLoading}
        buttonClicked={buttonClicked}
        _onChangeWeek={_onChangeWeek}
        date={new Date()}
        month={month}
        year={year}
        showMonth={true}
      />
      {timeSheetPerWeekData.isLoading ? (
        <div className="attendance-cards-spinner">
          <Spinner color="black" />
        </div>
      ) : timeSheetPerWeekData.data &&
        timeSheetPerWeekData.data.data &&
        Array.isArray(timeSheetPerWeekData.data.data) &&
        timeSheetPerWeekData.data.data.length ? (
        timeSheetPerWeekData.data.data.map((user, i) => (
          <Card className="mt-3" key={i}>
            <CardBody>
              <CardTitle className="d-flex">
                <span className="thumb-sm mb-3">
                  {user.image ? (
                    <img
                      className={"rounded-circle"}
                      src={user.image ? user.image : avatar}
                      alt="..."
                    />
                  ) : null}
                </span>
                <span className="ml-3">{user.username}</span>
              </CardTitle>

              <Table responsive>
                <tbody>
                  {user.timesheet.map(
                    (
                      {
                        full_date,
                        total_hours,
                        file,
                        day_type,
                        status,
                        fileId,
                      },
                      i
                    ) => (
                      <>
                        {status ? (
                          <tr
                            key={i}
                            onClick={async () => {
                              await dispatch(
                                actions.selectedUser({ userid: user.user_Id })
                              );
                              props.history.push({
                                pathname: "/app/all_user_timesheet",
                                date: full_date,
                              });
                            }}
                          >
                            <td>{full_date}</td>
                            <td>{total_hours} Hours</td>
                            <td className="text-right">{status}</td>
                          </tr>
                        ) : null}
                      </>
                    )
                  )}
                </tbody>
              </Table>
            </CardBody>
          </Card>
        ))
      ) : (
        <Alert color="danger" className="mt-3">
          No timesheet submitted this month.
        </Alert>
      )}
    </div>
  );
}
