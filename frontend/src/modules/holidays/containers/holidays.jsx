import React from "react";
import { connect } from "react-redux";
import { withRouter } from "react-router";
import { bindActionCreators } from "redux";
import * as actions from "../../../redux/actions";
import { isNotUserValid } from "../../../services/generic";
import HolidaysList from "../../../components/holidays/HolidaysList";
import { getYearArrayHoliday } from "../../../services/generic";
import { compose } from "redux";
import isMobile from "../../../components/hoc/WindowResize";
import Heading from "../../../components/generic/Heading";

class Holidays extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      date: new Date(),
      holidayName: "",
      type: "",
      year: "",
      yearSelected: ""
    };
    this.year = [];
  }

  componentWillMount() {
    this.props.getGenricLoginConfig();
    this.props.requestHolidayList({ year: new Date().getYear() + 1900 });
    // this.props.resetAddHolidayReducer();
    this.year = getYearArrayHoliday();
    this.setState({ yearSelected: `${this.year[3]}` });
    this.props.showHeading("Holiday List")
  }

  componentWillReceiveProps(props) {
    let { location, history, loggedUser } = props;
    let isNotValid = isNotUserValid(location.pathname, loggedUser);
    if (isNotValid.status) {
      history.push(isNotValid.redirectTo);
    }
  }

  componentWillUnmount() {
    window.clearTimeout();
  }

  handleYearChange = e => {
    this.setState({ yearSelected: e.target.value });
    this.props.requestHolidayList({ year: e.target.value });
  };

  render() {
    let { data} = this.props.holidaysList;
    return (
      <div className="holidays-wrapper">
        <div
          id="content"
          className="app-content box-shadow-z0 holiday-wrapper"
          role="main"
        >
          <div className="app-body" id="view">
            <div className="row addHoliday mx-0">
              <div className="col-md-3 px-0">
                <div className="row mx-0">
                  <div className="col-12 col-md-6 holiday-action px-0">
                    <select
                      className="form-control ml-0"
                      ref="year_holidays"
                      onChange={e => {
                        this.handleYearChange(e);
                      }}
                      value={this.state.yearSelected}
                      style={{ minHeight: "0" }}
                    >
                      {this.year &&
                        this.year.map((data, index) => (
                          <option key={index} value={data}>
                            {data}
                          </option>
                        ))}
                    </select>
                  </div>
                </div>
              </div>
            </div>
            <div className="row mx-0">
              <div className="col-md-12 px-0">
                <HolidaysList
                  holidays={data.holidays ? data.holidays : []}
                  state={this.state}
                />
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    frontend: state.frontend.toJS(),
    loggedUser: state.logged_user.userLogin,
    holidaysList: state.holidaysList.holidaysList
  };
}
const mapDispatchToProps = dispatch => {
  return bindActionCreators(actions, dispatch);
};

export default compose(
  connect(
    mapStateToProps,
    mapDispatchToProps
  ),
  isMobile,
  withRouter
)(Holidays);
