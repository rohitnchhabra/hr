import React from "react";
import { connect } from "react-redux";
import { withRouter } from "react-router";
import { bindActionCreators } from "redux";
import * as actions from "../../../redux/actions";
import { isNotUserValid } from "../../../services/generic";
import HolidaysList from "../../../components/holidays/HolidaysList";
import { getToken, getYearArrayHoliday } from "../../../services/generic";
import { compose } from "redux";
import isMobile from "../../../components/hoc/WindowResize";
import Heading from "../../../components/generic/Heading";
import AlertFader from "../../../components/generic/AlertFader";
import { Alert, Button, Spinner } from "reactstrap";
import InputText from "../../../components/generic/input/InputText";
import GenericDatePicker from "../../../components/generic/GenericDatePicker";
var moment = require("moment");

class Holidays extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      date: new Date(),
      holidayName: "",
      type: "",
      year: "",
      yearSelected: ""
    };
    this.year = [];
  }

  componentWillMount() {
    this.props.getGenricLoginConfig();
    this.props.requestHolidayList({ year: new Date().getYear() + 1900 });
    this.props.resetAddHolidayReducer();
    this.props.requestHolidayType({ token: getToken() });
    this.year = getYearArrayHoliday();
    this.setState({ yearSelected: `${this.year[3]}` });
    this.props.showHeading("Holidays List")
  }

  componentWillReceiveProps(props) {
    let { addHoliday, holidayType, deleteHoliday } = props;
    // if (holidayType && holidayType.data && holidayType.data.holiday_type_list) {
    //   this.setState({ type: `${holidayType.data.holiday_type_list[0].type}` });
    // }
    if (addHoliday.isError != this.props.addHoliday.isError) {
      this.props.resetAddHolidayReducer();
    }
    if (addHoliday.isSuccess != this.props.addHoliday.isSuccess) {
      this.setState({
        date: new Date(),
        holidayName: "",
        yearSelected: this.state.yearSelected
      });
      this.props.resetAddHolidayReducer();
      this.props.requestHolidayList({ year: this.state.yearSelected });
    }
    if (deleteHoliday.isSuccess != this.props.deleteHoliday.isSuccess) {
      this.props.requestHolidayList({ year: this.state.yearSelected });
      this.props.resetAddHolidayReducer();
    }

    let {
      location,
      history,
      loggedUser
    } = props;
    let isNotValid = isNotUserValid(location.pathname, loggedUser);
    if (isNotValid.status) {
      history.push(isNotValid.redirectTo);
    }
    if (
      holidayType.isSuccess != this.props.holidayType.isSuccess &&
      holidayType.data &&
      holidayType.data.holiday_type_list
    ) {
      this.setState({
        type: `${holidayType.data.holiday_type_list[0].type}`
      });
    }
  }

  componentWillUnmount() {
    window.clearTimeout();
  }

  handleDateChnage = date => {
    if (date) {
      this.setState({ date: date, year: moment(date).year() });
    }
  };

  handleHolidayNameChnage = e => {
    this.setState({ holidayName: e.target.value });
  };

  handleTypeChnage = e => {
    this.setState({ type: e.target.value });
  };

  handleYearChange = e => {
    this.setState({ yearSelected: e.target.value });
    this.props.requestHolidayList({ year: e.target.value });
  };

  addHoliday = () => {
    this.props.requestAddHoliday({ data: this.state, token: getToken() });
  };

  deleteHoliday = (index, id) => {
    this.setState({ deleteHolidayIndex: index });
    this.props.requestDeleteHoliday({ id: id, token: getToken() });
  };

  handleDateChange = date => {
    this.setState({ date });
  };

  render() {
    let { isLoading, data} = this.props.holidaysList;
    let isAddLoading = this.props.addHoliday.isLoading;
    let isAddSuccess = this.props.addHoliday.isSuccess;
    let isAddError = this.props.addHoliday.isError;
    let addHolidayResponse = this.props.addHoliday.message;
    let pageHeading =
      this.props.page_headings &&
      this.props.page_headings.find(
        o => o.reference.toLowerCase() == "holiday"
      );
    return (
      <div className="holidays-wrapper">
        <div
          id="content"
          className="app-content box-shadow-z0 holiday-wrapper"
          role="main"
        >
          <div className="app-body" id="view">
            <div className="row no-gutters mb-4">
              <div className="col-sm-2 pr-sm-2 mb-sm-0 mb-2">
                <div className="row mx-0">
                  <div className="col-12 holiday-action px-0">
                    <select
                      className="form-control ml-0"
                      ref="year_holidays"
                      onChange={e => {
                        this.handleYearChange(e);
                      }}
                      value={this.state.yearSelected}
                      style={{ minHeight: "0" }}
                    >
                      {this.year &&
                        this.year.map((data, index) => (
                          <option key={index} value={data}>
                            {data}
                          </option>
                        ))}
                    </select>
                  </div>
                </div>
              </div>
              <div className="col-12 col-sm-2 holiday-action pr-sm-2 mb-sm-0 mb-2">
                <GenericDatePicker
                  date={this.state.date}
                  handleDateChange={date => this.handleDateChange(date)}
                  type={this.props.isMobile < 768 ? "absolute" : "inline"}
                />
              </div>
              <div className="col-12 col-sm-2 holiday-action pr-sm-2 mb-sm-0 mb-2">
                <InputText
                  onchange={e => {
                    this.handleHolidayNameChnage(e);
                  }}
                  value={this.state.holidayName}
                  placeHolder="Holiday Name"
                  style={{ minHeight: "0" }}
                />
              </div>
              <div className="col-12 col-sm-2 holiday-action mb-sm-0 mb-2">
                <select
                  className="form-control"
                  ref="holiday_type"
                  onChange={e => {
                    this.handleTypeChnage(e);
                  }}
                  value={this.state.type}
                  style={{ minHeight: "0" }}
                >
                  {this.props.holidayType.data.holiday_type_list &&
                    this.props.holidayType.data.holiday_type_list.map(
                      (data, index) => (
                        <option key={index} value={data.type}>
                          {data.text}
                        </option>
                      )
                    )}
                </select>
              </div>

              <div className="col-12 col-sm-2 holiday-action">
                <Button
                  color={isAddSuccess ? "success" : "primary"}
                  outline={isAddSuccess ? true : false}
                  size="md"
                  className="add-holiday-btn pull-right"
                  onClick={() => this.addHoliday()}
                  disabled={
                    this.state.date === "" ||
                    this.state.holidayName === "" ||
                    this.state.type === "" ||
                    isAddLoading
                  }
                >
                  <>
                    <i
                      className={`${
                        isAddSuccess
                          ? "show-tick-icon tick_list glyphicon glyphicon-ok"
                          : "hide-tick-icon"
                      }`}
                      style={{ fontSize: "22px !important" }}
                    ></i>
                    {isAddLoading ? (
                      <Spinner color="light" />
                    ) : (
                      <span
                        className={
                          isAddSuccess ? "hide-holiday" : "show-holiday"
                        }
                      >
                        Add Holiday
                      </span>
                    )}
                  </>
                </Button>
              </div>
            </div>
            {pageHeading && pageHeading.value && pageHeading.value != "" ? (
              <Alert
                className="alert-transparent rem-fade rh-section rh-alert"
                color="info"
              >
                <span className="font-weight-bold">RH:</span>
                <span> {pageHeading.value}</span>
              </Alert>
            ) : null}
            <AlertFader>
              {addHolidayResponse ? (
                <Alert
                  className="alert-transparent rem-fade rh-section rh-alert"
                  color={isAddSuccess ? "success" : isAddError ? "danger" : ""}
                >
                  {addHolidayResponse}
                </Alert>
              ) : null}
            </AlertFader>

            <div className="row mx-0">
              <div className="col-md-12 px-0">
                <HolidaysList
                  holidays={data.holidays ? data.holidays : []}
                  deleteHoliday={this.deleteHoliday}
                  deleteHolidayIsLoading={this.props.deleteHoliday.isLoading}
                  state={this.state}
                  isLoading={isLoading}
                  deleteHolidayIndex={this.state.deleteHolidayIndex}
                  showDelete={true}
                />
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    frontend: state.frontend.toJS(),
    loggedUser: state.logged_user.userLogin,
    holidaysList: state.holidaysList.holidaysList,
    addHoliday: state.holidaysList.addHolidays,
    deleteHoliday: state.holidaysList.deleteHolidays,
    holidayType: state.holidaysList.holidayType,
    page_headings: state.settings.page_headings
  };
}
const mapDispatchToProps = dispatch => {
  return bindActionCreators(actions, dispatch);
};

export default compose(
  connect(
    mapStateToProps,
    mapDispatchToProps
  ),
  isMobile,
  withRouter
)(Holidays);
