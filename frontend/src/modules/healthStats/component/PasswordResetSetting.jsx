import React, { Component } from "react";
import Select from "react-select";
import * as _ from "lodash";
import Widget from "../../../components/generic/Widget/Widget";
import {Input,FormGroup, Label} from "reactstrap";
const durationDays = () => {
  let options = [];
  let i = 1;
  while (i <= 100) {
    options.push({ value: i, label: i + " Days" });
    i++;
  }
  return options;
};

export default class PasswordResetSetting extends Component {
  state = { toggleActive: false, selectedOption: null };

  onToggle = () => {
    this.setState({ toggleActive: !this.state.toggleActive });
  };

  handleChange = selectedOption => {
    this.setState({ selectedOption, isDisabled: "" });
  };

  onSaveChangesClick = () => {
    this.props.resetPasswordData(this.state);
  };
  componentWillReceiveProps(props) {
    const value = props.resetPasswordStatusData;
    if (
      !_.isEqual(
        this.props.resetPasswordStatusData,
        props.resetPasswordStatusData
      )
    ) {
      this.setState({
        toggleActive: value && value.status,
        selectedOption: value
          ? {
              value: parseInt(value.days),
              label: parseInt(value.days) + "Days"
            }
          : null
      });
    }
  }

  componentWillUnmount() {
    this.props.clearData();
  }
  render() {
    let isDisabled = this.state.selectedOption === null ? "disabled" : "";
    return (
          <Widget className="attendance-upload-widget">
            <div className="attendance-upload-setting">
              <div className="reset-password-save">
                <div className="col-sm-6 col-xs-6 stong-text">
                <h6 className="pd-left-h"><strong>Password Reset Setting</strong></h6>
                </div>
              </div>
              <table className="table table-responsive secret-key-table display-block-none">
                <tbody>
                  <tr>
                    <td><span className="font-color-controller">Reset Password</span></td>
                    <td>
                      <div className="app-name-input-div">
                        <FormGroup  className="display-inline-block checkbox-ios">
                      <Label for="checkbox-ios2" className="switch">
                        <Input
                          type="checkbox" className="ios"
                          id="checkbox-ios2"
                          checked={this.state.toggleActive}
                        /><i onClick={()=> {
                          this.onToggle()
                        }}/>
                      </Label>
                    </FormGroup>
                        {" "}
                      </div>
                    </td>
                  </tr>
                  <tr>
                    <td><span className="font-color-controller">Reset Password Duration</span></td>
                    <td>
                      <Select
                        value={this.state.selectedOption}
                        onChange={this.handleChange}
                        options={durationDays()}
                        placeholder="Select Duration"
                      />
                    </td>
                  </tr>
                </tbody>
              </table>
              <div className="clearfix">
                  <div className="btn-toolbar float-right">
                    <button 
                        onClick={this.onSaveChangesClick}
                        disabled={isDisabled}
                        type="button"
                        className={`btn btn-success setting-page ${isDisabled}`}>
                        &nbsp;Save Changes&nbsp;
                    </button>
                  </div>
              </div>
            </div>
        </Widget>
    );
  }
}
