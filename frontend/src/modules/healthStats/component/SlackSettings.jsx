import React, { Component } from "react";
import ToggleButton from "react-toggle-button";
import isEqual from "lodash/isEqual";
import isEmpty from "lodash/isEmpty";
import Dialog from "material-ui/Dialog";
import InputText from "../../../components/generic/input/InputText";
import Widget from "../../../components/generic/Widget/Widget";
import FlatButton from "material-ui/FlatButton";
import RaisedButton from "material-ui/RaisedButton";
import { Link } from "react-router-dom";
import { Alert} from "reactstrap";
import AlertFader from "../../../components/generic/AlertFader";
import screenshot1 from "../../../static/slack_token _generation/screenshot1.png";
import screenshot2 from "../../../static/slack_token _generation/screenshot2.png";
import screenshot3 from "../../../static/slack_token _generation/screenshot3.png";
import screenshot4 from "../../../static/slack_token _generation/screenshot4.png";
import screenshot5 from "../../../static/slack_token _generation/screenshot5.png";
import screenshot6 from "../../../static/slack_token _generation/screenshot6.png";
import screenshot7 from "../../../static/slack_token _generation/screenshot7.png";
import screenshot8 from "../../../static/slack_token _generation/screenshot8.png";
import screenshot9 from "../../../static/slack_token _generation/screenshot9.png";

let screenshots = [
  screenshot1,
  screenshot2,
  screenshot3,
  screenshot4,
  screenshot5,
  screenshot6,
  screenshot7,
  screenshot8,
  screenshot9
];

export default class SlackSettings extends Component {
  constructor(props) {
    super();
    this.state = {
      slack_token: "",
      showSlackSettingSteps: false,
      slackChannel: "",
      slack_message:""
    };
  }

  componentDidUpdate(prevProps, prevState) {
    if (prevProps.slackSettings !== this.props.slackSettings) {
      this.setState({
        slack_token: this.props.slackSettings.slack_token
      });
      if(prevProps.slackSettings && this.props.slackSettings && 
        prevProps.slackSettings.slack_message!==this.props.slackSettings.slack_message){
          this.setState({
            slack_message:this.props.slackSettings.slack_message
          },()=>this.setState({
            slack_message:""
          }))
        }
    }
  }
  onSaveChangesClick = () => {
    this.props.onToggle({
      slack_token: this.state.slack_token
    });
  };

  onTestSlackMsg = () => {
    this.props.testSlackMsg({
      email:
        this.props.loggedUserData && this.props.loggedUserData.work_email.trim()
    });
  };

  render() {
    const actions = [
      <FlatButton
        label="Cancel"
        primary
        onClick={() =>this.props.hideSlackChannel(false)}
        style={{ marginRight: 5 }}
      />,
      <RaisedButton
        label="Submit"
        primary
        onClick={()=>{
          this.props.addChannel({channel : this.state.slackChannel}); 
        }}
        disabled={!this.state.slackChannel}
      />
    ];
    return (
      <Widget className="attendance-upload-widget">
        <div className="attendance-upload-setting">
            <div className="col-sm-12  px-0">
              <h6>
                <strong>Slack Setting</strong>
              </h6>
              <AlertFader>
                {this.state.slack_message && (
                  <>
                    <Alert
                      fade={false}
                      className="alert-transparent"
                      color={
                        this.state.slack_message.status == true ? "success" : "danger"
                      }
                    >
                      {this.state.slack_message.message}
                    </Alert>
                  </>
                )}
              </AlertFader>
            </div>
            <div>
            </div>
          <table className="table table-responsive secret-key-table display-block-none">
            <tbody>
              <tr>
                <td>
                  <span className="font-color-controller">Slack Token</span>
                </td>
                <td>
                  <div className="app-name-input-div">
                    <div className="form-group">
                      <InputText
                        value={this.state.slack_token}
                        className="app-name-input"
                        name="slackId"
                        placeHolder="Slack ID"
                        onchange={e => {
                          this.setState({
                            slack_token: e.target.value
                          });
                        }}
                      />
                    </div>
                  </div>
                </td>
              </tr>
              <tr>
                <td></td>
                <td className="text-right">
                  <span
                    className="font-color-controller"
                    style={{ fontSize: 12, cursor: "pointer" }}
                    onClick={() =>
                      this.setState({ showSlackSettingSteps: true })
                    }
                  >
                    How to generate Slack Token?
                  </span>
                  <Dialog
                    modal={false}
                    open={this.state.showSlackSettingSteps}
                    onRequestClose={() =>
                      this.setState({ showSlackSettingSteps: false })
                    }
                    autoScrollBodyContent={true}
                  >
                    <h5>How to generate Slack Token?</h5>
                    <div style={{ fontSize: 12 }}>
                      <div className="thumbnail">
                        1. Login with slack of admin.
                        <br />
                        2. Go to{" "}
                        <a target="_BLANK" href="https://api.slack.com/">
                          https://api.slack.com/
                        </a>
                        <br />
                        3. Go to Your Apps.
                        <br />
                        4. Select Create New App.
                        <br />
                        5. Select app name and workspace
                        <br />
                        6. Go to Oauth and permissions.
                        <br />
                        7. Select scopes and click on Add OAuth Scope.
                        <br />
                        8. Add all the scopes provided in the list only.
                        <br />
                        9. Select Install app to workspace.
                        <br />
                        10. Allow for permissions.
                        <br />
                        11. Token will be generated copy that and paste in the
                        location.
                        <br />
                        12. If you change scopes then after adding scopes click
                        on reinstall App and select channel.
                      </div>
                      <div className="row no-gutters">
                        {screenshots.map((screenshot, i) => (
                          <div className="col-md-12 mb-4">
                            <img src={screenshot} className="w-100"></img>
                          </div>
                        ))}
                      </div>
                    </div>
                  </Dialog>
                  <Dialog
                    modal={false}
                    open={this.props.slack_channel.show_slack_channel}
                    actions={actions}
                  >
                    <h5>Select the Channel</h5>
                    <div className="row">
                      <div className="col-md-12">
                        <select
                          onChange={e => {
                            this.setState({
                              slackChannel: e.target.value
                            });
                          }}
                          value={this.state.slackChannel}
                          className="form-control mb-1 mt-1"
                        >
                          <option value="">Select Channel</option>
                          {Object.keys(
                            this.props.slack_channel.slack_channels
                          ).map((data, i) => {
                            return this.props.slack_channel.slack_channels[
                              data
                            ].map((v, index) => (
                              <option value={v.value}>{v.text}</option>
                            ));
                          })}
                        </select>
                      </div>
                    </div>
                  </Dialog>
                </td>
              </tr>
            </tbody>
          </table>
          <div className="clearfix">
            <div className="btn-toolbar float-right">
              <button
                onClick={this.onSaveChangesClick}
                type="button"
                className={`btn btn-success setting-page`}
              >
                &nbsp;Save Changes&nbsp;
              </button>

              {this.state.slack_token && (
                <button
                  onClick={this.onTestSlackMsg}
                  type="button"
                  className={`btn btn-success setting-page`}
                >
                  &nbsp;Test Slack Message&nbsp;
                </button>
              )}
            </div>
          </div>
        </div>
      </Widget>
    );
  }
}
