import React, { Component } from "react";
import Widget from "../../../components/generic/Widget/Widget";
import { Input, FormGroup, Label, Alert } from "reactstrap";
import AlertFader from "../../../components/generic/AlertFader";
import Select from "react-select";
const durationDays = () => {
  let options = [];
  let i = 1;
  while (i <= 100) {
    options.push({ value: i, label: i });
    i++;
  }
  return options;
};

export default class Rh_Config_setting extends Component {
  constructor(props) {
    super();
    this.state = {
      toggleVal: false,
      rh_per_quater: null,
      rh_extra: null
    };
  }

  componentDidUpdate(prevProps, prevState) {
    if (prevProps.rh_config && this.props.rh_config) {
      if (prevProps.rh_config.rh_config !== this.props.rh_config.rh_config) {
        this.setState({
          toggleVal: this.props.rh_config.rh_config.rh_rejection_setting,
          rh_per_quater: {
            value: this.props.rh_config.rh_config.rh_per_quater,
            label: this.props.rh_config.rh_config.rh_per_quater
          },
          rh_extra: {
            value: this.props.rh_config.rh_config.rh_extra,
            label: this.props.rh_config.rh_config.rh_extra
          }
        });
      }
      if (prevProps.rh_config.status !== this.props.rh_config.status) {
        this.setState(
          {
            status: this.props.rh_config.status
          },
          () => {
            this.setState({
              status: ""
            });
          }
        );
      }
    }
  }

  onSaveChangesClick = () => {
    this.props.updateData({
      rh_per_quater: this.state.rh_per_quater.value,
      rh_extra: this.state.rh_extra.value,
      rh_rejection_setting: this.state.toggleVal
    });
  };

  handleChange = (selectedOption, name) => {
    this.setState({ [name.name]: selectedOption });
  };

  render() {
    return (
      <Widget className="attendance-upload-widget">
        <div className="attendance-upload-setting">
          <div>
            <h6>
              <strong>RH Config Setting</strong>
            </h6>
              {/* <tr> */}
            <AlertFader>
              {this.state.status && (
                <Alert
                  fade={false}
                  className="alert-transparent"
                  color={
                    this.state.status.status
                      ? this.state.status.message === "Values already exist"
                        ? "info"
                        : "success"
                      : "danger"
                  }
                >
                  {this.state.status.message}
                </Alert>
              )}
            </AlertFader>
          </div>
          <div className="rh-content-wrapper mb-4">
            <div className="rh-content">
                <span className="font-color-controller">RH Per Quater</span>
                <Select
                  value={this.state.rh_per_quater}
                  name="rh_per_quater"
                  onChange={this.handleChange}
                  options={durationDays()}
                  placeholder="Select RH Count"
                  className="rh-dropdown-item"
                />
              </div>
              <Alert className="alert-transparent rem-fade mt-2" color="info">
                      This is the maximum amount of RH that employee can take in a single QTR
              </Alert>
          </div>
          <div className="rh-content-wrapper mb-4">
            <div className="rh-content">
              <span className="font-color-controller">RH Extra</span>
              <div className="rh-extra-option">
                <Select
                  value={this.state.rh_extra}
                  name="rh_extra"
                  onChange={this.handleChange}
                  options={durationDays()}
                  placeholder="Select RH Count"
                />
              </div>
            </div>
            <Alert className="alert-transparent rem-fade mt-2" color="info">
              This is the extra RH which employee can take on any qty. This is on the full year basis not per qty basis.
            </Alert>
          </div>
          <div className="rh-content-wrapper mb-4">
            <div className="rh-content">
              <span className="font-color-controller">Rh Rejection</span>
              <div className="app-name-input-div">
                <FormGroup className="display-inline-block checkbox-ios">
                  <Label for="checkbox-ios2" className="switch">
                    <Input
                      type="checkbox"
                      className="ios"
                      id="checkbox-ios2"
                      checked={this.state.toggleVal}
                    />
                    <i
                      onClick={e => {
                        this.setState({
                          toggleVal: !this.state.toggleVal
                        });
                      }}
                    />
                  </Label>
                </FormGroup>{" "}
              </div>
            </div>
            <Alert className="alert-transparent rem-fade" color="info">
              {
                this.state.toggleVal ? 
                "This means employee's cannot take RH any day of the year. Employee can take RH only on the daily when RH is setup on Holiday Calander. If employee wants to take RH on any day of the year, he needs to apply RH via a special leave type called 'RH Compensation' and this option will show only if employee has a previously rejected RH. If there are no previous rejected RH, employee cannot take RH on any of day of the year.":
                "This means that employee can apply RH on any day of the year based on their RH availability and availability."
              }
            </Alert>
          </div>
          <div className="clearfix">
            <div className="btn-toolbar float-right">
              <button
                className={`btn btn-primary `}
                onClick={this.onSaveChangesClick}
                // disabled={isDisabled}
                type="button"
                className={`btn btn-success setting-page`}
              >
                &nbsp;Save Changes&nbsp;
              </button>
            </div>
          </div>
        </div>
      </Widget>
    );
  }
}
