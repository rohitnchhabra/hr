import React, { Component } from "react";
import ToggleButton from "react-toggle-button";
import isEqual from "lodash/isEqual";
import isEmpty from "lodash/isEmpty";
import Dialog from "material-ui/Dialog";
import InputText from '../../../components/generic/input/InputText';
import Widget from "../../../components/generic/Widget/Widget";
import {Input,FormGroup, Label} from "reactstrap";

export default class LoginTypesSettings extends Component {
  constructor(props) {
    super();
    this.state = {
      google_login: false,
      normal_login: false,
      google_auth_client_id: "",
      showClientIdHint: false
    }
  }

  componentWillReceiveProps(props) {
    if (props.loginTypes.isLoading != this.props.loginTypes.isLoading) {
      if (!isEmpty(props.loginTypes.data)) {
        const {data} = props.loginTypes
        this.setState({ 
          google_login: data.google_login || false,
          normal_login: data.normal_login || false,
          google_auth_client_id: data.google_auth_client_id || ""
        });
      }
    }
  }
  onSaveChangesClick = () => {
    this.props.onToggle(this.state);
  }
  render() {    
    return (
          <Widget className="attendance-upload-widget">
          <div className="attendance-upload-setting">
            <div className="reset-password-save">
              <div className="col-sm-6 col-xs-6 stong-text">
              <h6 className="pd-left-h"><strong>Login Types Setting</strong></h6>
              </div>
            </div>
            <table className="table table-responsive secret-key-table display-block-none">
              <tbody>
                <tr>
                  <td><span className="font-color-controller"> Normal Login</span></td>
                  <td>
                    <div className="app-name-input-div">
                    <FormGroup 
                    className="display-inline-block checkbox-ios">
                      <Label
                        for="checkbox-ios1" className="switch">
                        <Input
                          checked={this.state.normal_login}
                          type="checkbox" className="ios" 
                          id="checkbox-ios1"
                        /><i onClick={() => {
                          this.setState({
                            normal_login: !this.state.normal_login
                          });
                        }}/>
                      </Label>
                    </FormGroup>
                      {" "}
                    </div>
                  </td>
                </tr>
                <tr>
                  <td><span className="font-color-controller">Google Login</span></td>
                  <td>
                    <div className="app-name-input-div">
                      <FormGroup  className="display-inline-block checkbox-ios">
                      <Label for="checkbox-ios2" className="switch">
                        <Input
                          type="checkbox" className="ios"
                          id="checkbox-ios2"
                          checked={this.state.google_login}
                        /><i onClick={() => {
                          this.setState({
                            google_login: !this.state.google_login,
                          });
                        }}/>
                      </Label>
                    </FormGroup>
                      {" "}
                    </div>
                  </td>
                </tr>
                <tr>
                  <td><span className="font-color-controller">Google Auth Client ID</span></td>
                  <td>
                      <div className="app-name-input-div">
                      <div className="form-group">
                          <InputText
                            disabled={this.state.google_login ? false : true }
                            value={this.state.google_auth_client_id}
                            className="app-name-input" 
                            name="googleAuthClientId" 
                            placeHolder="Google Auth Client ID"
                            onchange={(e) => {
                              this.setState({
                                google_auth_client_id: e.target.value
                              });
                            }} 
                          />
                          </div>
                      </div>
                  </td>
                </tr>  
                <tr>
                  <td></td>
                  <td className="text-right">
                      <span className="font-color-controller" style={{fontSize:12, cursor:"pointer"}} onClick={() =>  this.setState({showClientIdHint:true})}>How to generate Google Auth Client ID?</span>
                      <Dialog
                        // actions={actions}
                        modal={false}
                        open={this.state.showClientIdHint}
                        onRequestClose={() =>  this.setState({showClientIdHint:false})}
                        autoScrollBodyContent={true}
                      >
                        <h5>How to generate Google Auth Client ID?</h5>
                        <div style={{fontSize:12}}>
                          <div className="thumbnail">
                            1. Go to the API console - <a target="_BLANK" href="https://console.developers.google.com">https://console.developers.google.com</a>
                            <br/>
                            2. From the projects list, select a project or create a new one.
                            <br/>
                            3. If the APIs & services page isn't already open, open the console left side menu and select APIs & services.
                            <br/>
                            4. On the left, click Credentials.
                            <br/>
                            5. Click New Credentials, then select OAuth client ID.
                            <br/>
                            5A. New page will open. If prompted : "To create an OAuth client ID, you must first set a product name on the consent screen". Click on configure consent screen button.
                            <br/>
                            5B. Fill application name e.g EHR2019 ( this will appera in pop modal while login ) and click save. It will take you back to Page on step 5A.  Choose Web application in Application type.
                            <br/>
                            6. Enter name. eg. EHR2019-OAUTH 
                            <br/>
                            6A. Enter your domain name in Authorized JavaScript origins
                            <br/>
                            6B. Enter your domain name in Authorized redirect URIs
                            <br/>
                            7. Click Save button. 
                            <br/>
                            8. A modal will open mentioning Client ID and Client Secret. Copy client ID and paste it in Google Auth Client ID under Login Types Setting.
                          </div>
                        </div>
                      </Dialog>
                  </td>
                </tr>           
              </tbody>
            </table>
            <div className="clearfix">
                <div className="btn-toolbar float-right">
                  <button
                      onClick={this.onSaveChangesClick}
                      type="button"
                      className={`btn btn-success setting-page`}>
                      &nbsp;Save Changes&nbsp;
                  </button>
                </div>
            </div>
          </div>
      </Widget>
    );
  }
}
