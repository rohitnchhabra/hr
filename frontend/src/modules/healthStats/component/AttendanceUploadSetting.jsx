import React, { Component } from "react";
import InputText from '../../../components/generic/input/InputText';
import {notify} from '../../../services/notify';
import Widget from "../../../components/generic/Widget/Widget";
import moment from 'moment';


export default class AttendanceUploadSetting extends React.Component {
    constructor() {
        super();
        this.state={
            userKey: '',
            dateTimeKey: ''
        };
    }
    componentWillReceiveProps(props) {  
        const {attendanceUploadSetting} = props;
        const {userKey, dateTimeKey} = this.state;
        if (attendanceUploadSetting.isError) {    
            notify('Error !', attendanceUploadSetting.message, 'error');
        }
        if (attendanceUploadSetting.isSuccess && (userKey !== '' || dateTimeKey !== '')) {    
            this.setState({
                userKey: '',
                dateTimeKey: ''
            });
        }
    }
    handleChange = (e) => {
        this.setState({ [e.target.name]: e.target.value });
    }
    handleAddClick = (params) => {
        this.props.requestAddAttendanceUploadSetting(params);
    }
    handleRemoveKey = (params) => {
        this.props.requestDeleteAttendanceUploadSetting(params);
    }
    render() {
        let config = this.props.attendanceUploadSetting;
        let userKeys = [], dateTimeKeys = [];    
        config.user_id && config.user_id.map((keyText, i)=>{
            userKeys.push(<span key={i} className="key-chips">
            <span className="text">{keyText}</span>
            <span className="delete" onClick={()=>{this.handleRemoveKey({field_name:'user_id', key_text: keyText})}} >x</span>
        </span>)
        })
        config.time && config.time.map((keyText, i)=>{
            dateTimeKeys.push(<span key={i} className="key-chips">
            <span className="text">{keyText}</span>
            <span className="delete" onClick={()=>{this.handleRemoveKey({field_name:'time', key_text: keyText})}}>x</span>
        </span>)
        })
        return (
            <Widget className="attendance-upload-widget">
                <div className="attendance-upload-setting">
                    <h6 className="pd-left-h"><strong>Attendance Upload Setting</strong></h6>
                    <table className="table table-responsive secret-key-table mrl-0">
                        <thead>
                            <tr className="text-align-left-controller">
                                <th className="field-column"><h6 className="pd-left-h pb-2">Field</h6></th>
                                <th className="allowed-keys-column"><h6 className=" pb-2">Allowed Keys</h6></th>
                                <th className="add-new-key-column" ><h6 className=" pb-2">Add New Key</h6></th>
                            </tr>
                        </thead>
                        <tbody>
                        <tr>
                        <span className="font-color-controller pd-left-h" >User</span>
                            <td><div className="keys-wrapper">{userKeys}</div></td>
                            <td className="input-field-attendance">
                                <div className="app-name-input-div">
                                <div className="form-group">
                                    <InputText className="data-time-key-input" name="userKey" placeHolder="Date Time Key" value={this.state.userKey} onchange={this.handleChange} />
                                </div>
                                </div>
                                <div className="stats-add-icon-div">
                                    <i className="material-icons stats-add-icon" onClick={() => this.handleAddClick({userid_key:this.state.userKey})}>add_box</i>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td><span className="font-color-controller pd-left-hh">DateTime</span></td>
                            <td><div className="keys-wrapper">{dateTimeKeys}</div></td>
                            <td className="input-field-attendance">
                                <div className="app-name-input-div">
                                <div className="form-group">
                                    <InputText className="data-time-key-input" name="dateTimeKey" placeHolder="Date Time Key" value={this.state.dateTimeKey} onchange={this.handleChange} />
                                </div>
                                </div>
                                <div className="stats-add-icon-div">
                                    <i className="material-icons stats-add-icon" onClick={() => this.handleAddClick({timing_key: this.state.dateTimeKey})}>add_box</i>
                                </div>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </Widget>
        )
    }
}
