import React, { Component } from "react";
import { confirm } from "../../../services/notify";

export default class HealthStats extends React.Component {
  render() {
    let data;
    if (this.props.healthData.attendance_rows) {
      data = this.props.healthData.attendance_rows.map((item, index) => {
        return (
          <tr key={index}>
            <td>{item.year}</td>
            <td>{item.count}</td>
            <td>
              <i
                className="material-icons"
                title="Delete"
                onClick={e => {
                  confirm(
                    "Are you sure ?",
                    "Do you want to delete this record ?",
                    "warning"
                  ).then(res => {
                    if (res) {
                      this.props.deleteHealthStats(item.year);
                    }
                  });
                }}
                aria-hidden="true"
              >
                delete
              </i>
            </td>
          </tr>
        );
      });
    }
    return (
      <div>
        <h6><strong>DB Health - Attendance Table</strong></h6>
          <table className="table attendance-stats">
            <thead>
              <tr className="config-table-font">
                <th><h6 className="text-center pb-2">Year</h6></th>
                <th><h6 className="text-center pb-2">Count</h6></th>
                <th><h6 className="text-center pb-2">Action</h6></th>
              </tr>
            </thead>
            <tbody>{data}</tbody>
          </table>
      </div>
    );
  }
}
