import React, { Component } from "react";
import Dialog from "material-ui/Dialog";
import InputText from "../../../components/generic/input/InputText";
import Widget from "../../../components/generic/Widget/Widget";
import { Input, FormGroup, Label, Alert } from "reactstrap";
import AlertFader from "../../../components/generic/AlertFader";

export default class SmtpSettings extends Component {
  constructor(props) {
    super();
    this.state = {
      mailServer: "",
      email: "",
      mailPassword: "",
      mailPort: "",
      mail_use_tls: false,
      google_smtp: true,
      mail_from: null,
      message: ""
    };
  }

  componentDidUpdate(prevProps, prevState) {
    if (
      prevProps.smptpSettings.smtp_settings !==
      this.props.smptpSettings.smtp_settings
    ) {
      this.setState({
        mailServer:
          this.props.smptpSettings &&
          this.props.smptpSettings.smtp_settings[0] &&
          this.props.smptpSettings.smtp_settings[0].mail_server,
        email:
          this.props.smptpSettings &&
          this.props.smptpSettings.smtp_settings[0] &&
          this.props.smptpSettings.smtp_settings[0].mail_username,
        mailPort:
          this.props.smptpSettings &&
          this.props.smptpSettings.smtp_settings[0] &&
          this.props.smptpSettings.smtp_settings[0].mail_port,
        mail_use_tls:
          this.props.smptpSettings &&
          this.props.smptpSettings.smtp_settings[0] &&
          this.props.smptpSettings.smtp_settings[0].mail_use_tls,
        mail_from:
          this.props.smptpSettings &&
          this.props.smptpSettings.smtp_settings[0] &&
          this.props.smptpSettings.smtp_settings[0].mail_from
      });
    }
    if (
      prevProps.smptpSettings.smtp_message !==
      this.props.smptpSettings.smtp_message
    ) {
      this.setState(
        {
          message: this.props.smptpSettings.smtp_message
        },
        () =>
          this.setState({
            message: ""
          })
      );
    }
  }
  onSaveChangesClick = () => {
    let apiData={}
    if(this.state.google_smtp){
      apiData={
        mail_username: this.state.email,
        mail_password: this.state.mailPassword,
      }
    }
    else{
      apiData={
          mail_username: this.state.email,
          mail_password: this.state.mailPassword,
          mail_port:this.state.mailPort,
          mail_server: this.state.mailServer,
          mail_use_tls:this.state.mail_use_tls,
          mail_from:this.state.mail_from ? this.state.mail_from : null,
        }
    }
    
    this.props.onSaveSmtpSettings(apiData)
  };
  handleChange = (e, data) => {
    this.setState({
      [data]: e.target.value
    });
  };

  testSmtp = () => {
    this.props.onTestSmtp({ email: this.props.loggedUserData.work_email });
  };
  render() {
    return (
      <Widget className="attendance-upload-widget">
        <div className="attendance-upload-setting">
          <div className="col-sm-12 px-0">
            <h6>
              <strong>SMTP Settings</strong>
            </h6>

            <div>
              <AlertFader>
                {this.state.message && (
                  <>
                    <Alert
                      fade={false}
                      className="alert-transparent"
                      color={
                        this.state.message.status == true ? "success" : "danger"
                      }
                    >
                      {this.state.message.message}
                    </Alert>
                  </>
                )}
              </AlertFader>
            </div>
          </div>

          <table className="table table-responsive secret-key-table display-block-none">
            <tbody>
              <tr>
                <td>
                  <span className="font-color-controller">Google Smtp</span>
                </td>
                <td>
                  <div className="app-name-input-div">
                    <FormGroup className="display-inline-block checkbox-ios">
                      <Label for="checkbox-ios1" className="switch">
                        <Input
                          checked={this.state.google_smtp}
                          type="checkbox"
                          className="ios"
                          id="checkbox-ios1"
                          value={this.state.google_smtp}
                        />
                        <i
                          onClick={() => {
                            this.setState({
                              google_smtp: !this.state.google_smtp
                            });
                          }}
                        />
                      </Label>
                    </FormGroup>{" "}
                  </div>
                </td>
              </tr>
              <tr>
                <td>
                  <span className="font-color-controller">{!this.state.google_smtp ? "Username" : "Email"}</span>
                </td>
                <td>
                  <div className="app-name-input-div">
                    <div className="form-group">
                      <InputText
                        name="email"
                        placeHolder="Email"
                        onchange={e => this.handleChange(e, "email")}
                        value={this.state.email}
                      />
                    </div>
                  </div>
                </td>
              </tr>
              <tr>
                <td>
                  <span className="font-color-controller">Password</span>
                </td>
                <td>
                  <div className="app-name-input-div">
                    <div className="form-group">
                      <InputText
                        name="MailPassword"
                        placeHolder=" Password"
                        value={this.state.mailPassword}
                        onchange={e => this.handleChange(e, "mailPassword")}
                      />
                    </div>
                  </div>
                </td>
              </tr>
              {!this.state.google_smtp && (
                <tr>
                  <td>
                    <span className="font-color-controller">Mail Port</span>
                  </td>
                  <td>
                    <div className="app-name-input-div">
                      <div className="form-group">
                        <InputText
                          name=" MailPort"
                          placeHolder=" Mail Port"
                          onchange={e => this.handleChange(e, "mailPort")}
                          value={this.state.mailPort}
                        />
                      </div>
                    </div>
                  </td>
                </tr>
              )}
              {!this.state.google_smtp && (
                <tr>
                  <td>
                    <span className="font-color-controller">Mail Server</span>
                  </td>
                  <td>
                    <div className="app-name-input-div">
                      <div className="form-group">
                        <InputText
                          name="MailServer"
                          placeHolder="Mail Server"
                          onchange={e => this.handleChange(e, "mailServer")}
                          value={this.state.mailServer}
                        />
                      </div>
                    </div>
                  </td>
                </tr>
              )}
              {!this.state.google_smtp && (
              <tr>
                <td>
                  <span className="font-color-controller">Mail From</span>
                </td>
                <td>
                  <div className="app-name-input-div">
                    <div className="form-group">
                      <InputText
                        name="mail_from"
                        placeHolder="Mail From"
                        value={this.state.mail_from}
                        onchange={e => this.handleChange(e, "mail_from")}
                      />
                    </div>
                  </div>
                </td>
              </tr>
              )}
              {!this.state.google_smtp && (
                <tr>
                  <td>
                    <span className="font-color-controller"> Mail Use TLS</span>
                  </td>
                  <td>
                    <div className="app-name-input-div">
                      <FormGroup className="display-inline-block checkbox-ios">
                        <Label for="checkbox-ios1" className="switch">
                          <Input
                            checked={this.state.mail_use_tls}
                            type="checkbox"
                            className="ios"
                            id="checkbox-ios1"
                            value={this.state.mail_use_tls}
                          />
                          <i
                            onClick={() => {
                              this.setState({
                                mail_use_tls: !this.state.mail_use_tls
                              });
                            }}
                          />
                        </Label>
                      </FormGroup>{" "}
                    </div>
                  </td>
                </tr>
              )}
              {/* <tr>
                <td></td>
                <td className="text-right">
                  <span
                    className="font-color-controller"
                    // style={{ fontSize: 12, cursor: "pointer" }}
                    // onClick={() => this.setState({ showClientIdHint: true })}
                  >
                    How to generate Google Auth Client ID?
                  </span>
                  <Dialog
                        // actions={actions}
                        modal={false}
                        open={this.state.showClientIdHint}
                        onRequestClose={() =>  this.setState({showClientIdHint:false})}
                        autoScrollBodyContent={true}
                      >
                        <h5>How to generate Google Auth Client ID?</h5>
                        <div style={{fontSize:12}}>
                          <div className="thumbnail">
                          </div>
                        </div>
                      </Dialog> 
                </td>
              </tr> */}
            </tbody>
          </table>
          <div className="clearfix">
            <div className="btn-toolbar float-right">
              <button
                onClick={this.onSaveChangesClick}
                type="button"
                className={`btn btn-success setting-page`}
              >
                &nbsp;Save Changes&nbsp;
              </button>
            </div>
            {this.props.smptpSettings &&
              this.props.smptpSettings.smtp_settings &&
              this.props.smptpSettings.smtp_settings[0] &&
              Object.keys(this.props.smptpSettings.smtp_settings[0]).length >
                0 && (
                <div className="btn-toolbar float-right">
                  <button
                    onClick={this.testSmtp}
                    type="button"
                    className={`btn btn-success setting-page`}
                  >
                    &nbsp;Test Smtp&nbsp;
                  </button>
                </div>
              )}
          </div>
        </div>
      </Widget>
    );
  }
}
