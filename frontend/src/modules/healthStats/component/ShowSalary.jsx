import React, { Component } from "react";
import ToggleButton from "react-toggle-button";
import isEqual from "lodash/isEqual";
import isEmpty from "lodash/isEmpty";
import Widget from "../../../components/generic/Widget/Widget";
import {Input,FormGroup, Label} from "reactstrap";

export default class ShowSalary extends Component {
  constructor(props) {
    super();
    this.state = {
      toggleVal: false
    };
  }

  componentWillReceiveProps(props) {
    if (props.showSalary.webShowSalary != this.props.showSalary.webShowSalary) {
      if (props.showSalary.webShowSalary != "") {
        let toggle;
        if (Number(props.showSalary.webShowSalary) == 0) {
          toggle = false;
        } else if (Number(props.showSalary.webShowSalary) == 1) {
          toggle = true;
        }
        this.setState({ toggleVal: toggle });
      }
    }
  }
  render() {
    return (
      <Widget className="attendance-upload-widget">
        <div className="attendance-upload-setting">
          <div className="reset-password-save">
            <div className="col-sm-6 col-xs-6 stong-text">
            <h6 className="pd-left-h"><strong>Show Salary Setting</strong></h6>
            </div>
          </div>
          <table className="table table-responsive secret-key-table display-block-none">
            <tbody>
              <tr>
                <td><span className="font-color-controller">Show Salary On Web</span></td>
                <td>
                  <div className="app-name-input-div">
                    <FormGroup  className="display-inline-block checkbox-ios">
                      <Label for="checkbox-ios2" className="switch">
                        <Input
                          type="checkbox" className="ios"
                          id="checkbox-ios2"
                          checked={this.state.toggleVal}
                        /><i onClick={e => {
                          this.setState(
                            {
                              toggleVal: !this.state.toggleVal
                            },
                            () => {
                              this.props.onToggle({
                                status: this.state.toggleVal ? 1 : 0
                              });
                            }
                          );
                        }}/>
                      </Label>
                    </FormGroup>
                    {" "}
                  </div>
                </td>
              </tr>
            </tbody>
          </table>
        </div>
      </Widget>
    );
  }
}
