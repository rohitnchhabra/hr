import React, { Component } from "react";
import InputText from '../../../components/generic/input/InputText';
import { confirm } from "../../../services/notify";
import moment from 'moment';

export default class HealthStatsSecretKey extends React.Component {
    constructor() {
        super();
        this.state = { appName: "" };
    }
    handleChange = (e) => {
        this.setState({ appName: e.target.value });
    }
    handleAddClick = () => {
        this.props.healthStatsAddKeyRequest(this.state.appName);
        this.setState({ appName: "" });
    }
    render() {
        let data;
        if (this.props.healthKeyData.app_info) {
            data = this.props.healthKeyData.app_info.map((item, index) => {
                return (
                    <tr key={index}>
                        <td><span className="font-color-controller">{index + 1}</span></td>
                        <td><span className="font-color-controller">{item.app_name}</span></td>
                        <td><span className="font-color-controller">{item.secret_key}</span></td>
                        <td><span className="font-color-controller">
                        <b className="b">Added on:</b> {moment(item.added_on).format('MMM Do YYYY, h:mm a')} <br></br> 
                        {item.last_request ? <span><b className="b">Last Requested at:</b> {moment(item.last_request).format('MMM Do YYYY, h:mm a')}</span> : null}
                        </span>
                        </td>
                        <td>
                            <i
                                className="material-icons stats-delete-icon"
                                onClick={e => {
                                    confirm(
                                        "Are you sure ?",
                                        "Do you want to delete this key ?",
                                        "warning"
                                    ).then(res => {
                                        if (res) {
                                            this.props.healthStatsDeleteKeyRequest(item.id);
                                        }
                                    });
                                }}
                                aria-hidden="true"
                            >
                                delete
                            </i>
                        </td>
                        <td>
                            <i
                                className="material-icons stats-refresh-icon"
                                onClick={e => {
                                    confirm(
                                        "Are you sure ?",
                                        "Do you want to Regenerate the key ?",
                                        "warning"
                                    ).then(res => {
                                        if (res) {
                                            this.props.healthStatsRegenerateKeyRequest(item.id);
                                        }
                                    });
                                }}
                                aria-hidden="true"
                            >
                                autorenew
                            </i>
                        </td>
                    </tr>
                );
            });
        }
        return (
            <div>
                <h6><strong>Third Party Api Key</strong></h6>
                <div className="app-name-input-div">
                    <div className="form-group remove-margin">
                        <InputText placeHolder="App Name" value={this.state.appName} onchange={this.handleChange} />
                        <div className="stats-add-icon-div">
                    <i
                        className="material-icons stats-add-icon"
                        onClick={() => this.handleAddClick()}
                    >
                        add_box
                        </i>
                </div>
                    </div>
                    
                </div>
                <table className="table table-responsive secret-key-table">
                    <thead>
                        <tr>
                            <th><h6 className="text-center pb-2">#</h6></th>
                            <th><h6 className="text-center pb-2">App</h6></th>
                            <th><h6 className="text-center pb-2">Secret Key</h6></th>
                            <th><h6 className="text-center pb-2">Details</h6></th>
                        </tr>
                    </thead>
                    <tbody>
                        {data}
                    </tbody>
                </table>
            </div>
        )
    }
}
