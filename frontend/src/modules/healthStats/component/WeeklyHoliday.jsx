import React, { useState } from "react";
import ToggleButton from "react-toggle-button";
import map from "lodash/map";
import forEach from "lodash/forEach";
import { dateFormatter } from "../../../helper/helper";
import moment from "moment";
import { confirm } from "../../../services/notify";
import Widget from "../../../components/generic/Widget/Widget";
import {Input,FormGroup,Col, Label,UncontrolledButtonDropdown, ButtonDropdown, DropdownToggle,DropdownMenu, DropdownItem} from "reactstrap";

export default function WeeklyHoliday({ alternative_saturdays, onSave }) {
  const [toggleVal, setToggleVal] = useState(
    alternative_saturdays.length && alternative_saturdays[0]
      ? alternative_saturdays[0].data
      : {}
  );
  const [monthSelected, setMonthSelected] = useState(new Date().getMonth() + 1);
  const numbers = ["First", "Second", "Third", "Forth", "Fifth"];

  const [showHistory, toggleShowHistory] = useState(false);
  return (
    <Widget className="attendance-upload-widget-holiday">
      <div className="attendance-upload-setting">
      <h6 className="pd-left-h"><strong>Manage Saturdays to be working/non-working</strong></h6>
        <div className="d-flex save-btn justify-content-between">
        </div>
        <div className="dropdown-controller-div">
          <span className="font-color-controller"> Starting from first of:</span>
          <FormGroup  row>
                  <Col md="8">
                    <UncontrolledButtonDropdown>
                      <DropdownToggle
                        caret color="default"
                        className="dropdown-toggle-split mr-xs weeklyhours"
                      >
                        {moment().month(monthSelected-1).format("MMMM")}
                      </DropdownToggle>
                      <DropdownMenu>
                      {dateFormatter().fullMonths.map((month, i) => {
                        return(
                      <DropdownItem  onClick={(e)=>setMonthSelected(i+1)}>
                      {month}
                    </DropdownItem>
                    )})}
                      </DropdownMenu>
                    </UncontrolledButtonDropdown>
                  </Col>
                </FormGroup>
        </div>
        <table className="table table-responsive secret-key-table display-block-none">
          <tbody>
            {map(numbers, (number, i) => (
              <tr key={i}>
                <td><span className="font-color-controller">{number} Saturday</span></td>
                <td>
                  <div className="app-name-input-div">
                    <FormGroup  className="display-inline-block checkbox-ios">
                      <Label for="checkbox-ios2" className="switch">
                        <Input
                          type="checkbox" className="ios"
                          id="checkbox-ios2"
                          checked={toggleVal[i + 1]}
                        /><i onClick={e => {
                          setToggleVal({
                            ...toggleVal,
                            [i + 1]: !toggleVal[i + 1]
                          });
                        }}/>
                      </Label>
                    </FormGroup>
                    {" "}
                  </div>
                </td>
              </tr>
            ))}
          </tbody>
        </table>
        {alternative_saturdays.length ? (
          <div 
            className="font-weight-bold mb-2" 
            style={{cursor:"pointer"}}
            onClick={() => toggleShowHistory(!showHistory)}
          >
            {
              showHistory ? 'Hide History' : 'Show History'
            }
          </div>
        ) : null}
        {showHistory && alternative_saturdays.length
          ? alternative_saturdays.map((alternative_saturday, i) => (
              <div key={i}>
                <div className="d-flex bg-dark text-white p-1">
                  <div className="mr-3">
                    Start date:{" "}
                    {moment(alternative_saturday.updated_date).format("DD/MM/YY")}
                  </div>
                  <div>
                    Updated date:
                    {moment(alternative_saturday.start_date).format("DD/MM/YY")}
                  </div>
                </div>
                <div className="pb-2">
                  {map(numbers, (number, i) => (
                    <div key={i}>
                      {number} Saturday:{" "}
                      {alternative_saturday.data[i + 1] &&
                      JSON.parse(alternative_saturday.data[i + 1])
                        ? "On"
                        : "Off"}
                    </div>
                  ))}
                </div>
              </div>
            ))
          : null}
                  <div className="clearfix">
              <div className="btn-toolbar float-right">
                <button className={`btn btn-primary `}
                    onClick={() => {
                      let message = [];
                      let x = forEach(toggleVal, (v, i) => {
                        if (v) {
                          message.push(
                            "<span class='font-weight-bold'>" + numbers[i - 1] + "</span>"
                          );
                        }
                      });
                      if (!message.length) {
                        message = "<span class='font-weight-bold'>all</span>";
                      } else if (message.length === 1) {
                        message = message[0];
                      } else if (message.length === 2) {
                        message = message.join(" and ");
                      } else {
                        message = message.join(", ");
                      }
                      message += " saturday as working";
                      confirm(
                        "Are you sure ?",
                        `Do you want to set ${message} from <span class='font-weight-bold'>${
                          dateFormatter().fullMonths[monthSelected - 1]
                        }</span>.`,
                        "warning"
                      ).then(res => {
                        if (res) {
                          onSave({ value: toggleVal, start_month: monthSelected });
                        }
                      });
                    }}
                    type="button"
                    className={`btn btn-success setting-page`}>
                    &nbsp;Save Changes&nbsp;
                </button>
              </div>
          </div>
      </div>
    </Widget>
  );
}
