import React, { Component } from "react";
import ToggleButton from "react-toggle-button";
import {
  Button,
  Popover,
  PopoverHeader,
  PopoverBody,
  UncontrolledPopover
} from "reactstrap";
import Widget from "../../../components/generic/Widget/Widget";
import InputText from "../../../components/generic/input/InputText";

export default class ConfigPageHeadings extends Component {
  constructor(props) {
    super();
    this.state = {
      page_headings: props.config_page_headings || [],
      selectedPage: false,
      selectedPageIndex: false,
      popoverOpen: false,
      textbox_heading: ""
    };
  }
  componentWillUpdate(nextProps) {
    if (nextProps.page_headings != this.props.page_headings) {
      this.setState({
        page_headings: nextProps.page_headings,
        selectedPage: nextProps && nextProps.page_headings && nextProps.page_headings[0],
        selectedPageIndex: 0
      });
    }
  }
  _addNewKeyVal = () => {
    let { page_headings } = this.state;
    page_headings.push({
      value: "new",
      reference: this.state.textbox_heading
    });
    this.setState({
      page_headings: page_headings,
      selectedPage: page_headings[page_headings.length - 1],
      selectedPageIndex: page_headings.length - 1
    });
  };

  _removeHeading = key => {
    let { page_headings } = this.state;
    page_headings.splice(key, 1);
    this.setState({
      page_headings: page_headings,
      selectedPage: page_headings[0],
      selectedPageIndex: 0
    });
  };

  _updateHeading = (type, key, newVal) => {
    let { page_headings } = this.state;
    page_headings[key][type] = newVal;
    this.setState({
      page_headings: page_headings
    });
  };

  _saveConfigPageHeadings = () => {
    if (this.state.page_headings.length > 0) {
      this.props.updateConfigPageHeadingsRequest(this.state.page_headings);
    }
  };

  _onPageSelection = e => {
    this.setState({
      selectedPageIndex: e.target.value,
      selectedPage: this.state.page_headings[e.target.value]
    });
  };

  handleHeadingChange = e => {
    this.setState({
      textbox_heading: e.target.value
    });
  };

  _renderAddNewBody = () => {
    return (
      <div>
        <UncontrolledPopover
          className="pop-up"
          trigger="legacy"
          placement="right"
          target="PopoverAddNewPage"
        >
          <PopoverHeader>Add Page Name</PopoverHeader>
          <PopoverBody className="d-flex">
            {" "}
            <InputText
              onchange={e => {
                this.handleHeadingChange(e);
              }}
              value={this.state.textbox_heading}
              placeHolder="Page Name"
            />
            <div
              className="stats-add-icon-div pl-1 pt-1"
            >
              <i
                onClick={this._addNewKeyVal}
                className="material-icons stats-add-icon"
              >
                add_box
              </i>
            </div>
          </PopoverBody>
        </UncontrolledPopover>
      </div>
    );
  };

  render() {
    const { page_headings, selectedPageIndex, selectedPage } = this.state;
    return (
        <Widget className="attendance-upload-widget-config">
      <div id="config-page-heading">
        <div className="row">
          
        <div className="col-sm-6 col-xs-6 stong-text">
            <h6><strong>Manage Page Headings</strong></h6>
            <span className="stats-add-icon-div" id="PopoverAddNewPage">
                <i className="material-icons stats-add-icon" >add_box</i>
            </span>
        </div>
          <div className="col-sm-2 col-xs-2 save-btn">
            
          </div>
        </div>

        <div className="row">
          <div className="block col-sm-12 col-xs-12">
            {this._renderAddNewBody()}
          </div>
        </div>

        {page_headings && page_headings.length > 0 ?
            <div>
            <div className="row">
              <div className="col-sm-2 col-xs-2">Page</div>
              <div className="col-sm-5 col-xs-5">
                <select
                  className="form-control"
                  onChange={this._onPageSelection}
                  value={(selectedPage && selectedPageIndex) || 0}
                >
                  {page_headings.map((val, key) => {
                    let reference = val["reference"];
                    let headingValue = val["value"];
                    return (
                      <option key={reference+key} value={key}>
                        {reference}
                      </option>
                    );
                  })}
                </select>
              </div>
              <div className="col-sm-1 col-xs-1">
                <i
                  className="material-icons delete-button"
                  onClick={e => {
                    this._removeHeading(selectedPageIndex);
                  }}
                  aria-hidden="true"
                >
                  delete
                </i>
              </div>
            </div>


            <div className="row">
              <div className="col-sm-2 col-xs-2">Heading</div>
              <div className="col-sm-10 col-xs-10">
                <div className="">
                  <InputText
                    value={(selectedPage && selectedPage.value) || ""}
                    // className="app-name-input"
                    onchange={e =>
                      this._updateHeading(
                        "value",
                        selectedPageIndex,
                        e.target.value
                      )
                    }
                    placeHolder=""
                  />
                </div>
              </div>
            </div>
           

        

        <div className="row">
          
            <div className="col-sm-10 col-xs-10 stong-text">
                
            </div>
          
          <div className="col-sm-2 col-xs-2 save-btn">
            <button
              type="button"
              className={`btn btn-success setting-page`}
              onClick={() => {
                this._saveConfigPageHeadings();
              }}
            >
              Save Changes
            </button>
          </div>
        </div>

         </div>

        : null }

      </div>
      </Widget>
    );
  }
}