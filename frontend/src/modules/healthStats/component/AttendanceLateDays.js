import React, { Component } from "react";
import Select from "react-select";
import * as _ from "lodash";
import Widget from "../../../components/generic/Widget/Widget";
import {Alert} from "reactstrap";

const durationDays = () => {
  let options = [];
  let i = 0;
  while (i <= 10) {
    options.push({ value: i, label: i + " Days" });
    i++;
  }
  return options;
};

export default class AttendanceLateDays extends Component {
  state = { toggleActive: false, selectedOption: null };

  onToggle = () => {
    this.setState({ toggleActive: !this.state.toggleActive });
  };

  handleChange = selectedOption => {
    this.setState({ selectedOption, isDisabled: "" });
  };

  onSaveChangesClick = () => {
    this.props.updateAttendanceLateDays(this.state);
  };
  componentWillReceiveProps(props) {
    if (
      !_.isEqual(
        this.props.attendance_late_days,
        props.attendance_late_days
      )
    ) {
      const value = props.attendance_late_days
      this.setState({
        selectedOption:{
          value: parseInt(value),
          label: `${parseInt(value)} Days`
        }
      });
    }
  }

  render() {
    let isDisabled = this.state.selectedOption === null ? "disabled" : "";
    return (
          <Widget className="attendance-upload-widget">
            <div className="attendance-upload-setting">
              <div className="reset-password-save">
                <div className="col-sm-6 col-xs-6 stong-text">
                <h6 className="pd-left-h"><strong>Set number of late days limit</strong></h6>
                </div>
              </div>
              <Alert className="alert-transparent rem-fade mb-4" color="info">
                  Note! You can set here the number of days per an employee is can be late to office.
                  After these many days, we will start sending warning notification to employee.
              </Alert>
              <table className="table table-responsive secret-key-table display-block-none">
                <tbody>
                  <tr>
                    <td><span className="font-color-controller">Late days limit</span></td>
                    <td>
                      <Select
                        value={this.state.selectedOption}
                        onChange={this.handleChange}
                        options={durationDays()}
                        placeholder="Select Duration"
                      />
                    </td>
                  </tr>
                </tbody>
              </table>
              <div className="clearfix">
                  <div className="btn-toolbar float-right">
                    <button
                        onClick={this.onSaveChangesClick}
                        disabled={isDisabled}
                        type="button"
                        className={`btn btn-success setting-page ${isDisabled}`}>
                        &nbsp;Save Changes&nbsp;
                    </button>
                  </div>
              </div>
            </div>
        </Widget>
    );
  }
}
