import React, { Component } from "react";
import ToggleButton from "react-toggle-button";
import isEqual from "lodash/isEqual";
import isEmpty from "lodash/isEmpty";
import Widget from "../../../components/generic/Widget/Widget";
import {Input,FormGroup, Label} from "reactstrap";

export default class PayslipSetting extends Component {
  constructor(props) {
    super();
    this.state = {
      toggleVal: false
    };
  }

  componentDidUpdate(prevProps){
      if(this.props.payslip_settings!==prevProps.payslip_settings){
          this.setState({
              toggleVal:this.props.payslip_settings && this.props.payslip_settings.pdf
          })
      }
  }

  render() {
    return (
      <Widget className="attendance-upload-widget">
        <div className="attendance-upload-setting">
          <div className="reset-password-save">
            <div className="col-sm-6 col-xs-6 stong-text">
            <h6 className="pd-left-h"><strong>Payslip Setting</strong></h6>
            </div>
          </div>
          <table className="table table-responsive secret-key-table display-block-none">
            <tbody>
              <tr>
                <td><span className="font-color-controller">Send PDF Payslip</span></td>
                <td>
                  <div className="app-name-input-div">
                    <FormGroup  className="display-inline-block checkbox-ios">
                      <Label for="checkbox-ios2" className="switch">
                        <Input
                          type="checkbox" className="ios"
                          id="checkbox-ios2"
                          checked={this.state.toggleVal}
                        /><i onClick={e => {
                          this.setState(
                            {
                              toggleVal: !this.state.toggleVal
                            },
                            () => {
                              this.props.onToggle({
                                pdf: this.state.toggleVal
                              });
                            }
                          );
                        }}/>
                      </Label>
                    </FormGroup>
                    {" "}
                  </div>
                </td>
              </tr>
            </tbody>
          </table>
        </div>
      </Widget>
    );
  }
}
