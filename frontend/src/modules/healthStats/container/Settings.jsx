import React from "react";
import Header from "../../../components/generic/Header";
import { connect } from "react-redux";
import * as actions from "../../../redux/actions";
import AttendanceUploadSetting from "../../../modules/healthStats/component/AttendanceUploadSetting";
import PasswordResetSetting from "../../../modules/healthStats/component/PasswordResetSetting";
import LoginTypesSettings from "../component/LoginTypesSettings";
import AttendanceLateDays from "../component/AttendanceLateDays";
import ShowSalary from "../component/ShowSalary";
import SlackSettings from "../component/SlackSettings"
import SmtpSettings from "../component/SmtpSettings"
import WeeklyHoliday from "../component/WeeklyHoliday";
import Heading from "../../../components/generic/Heading";
import ConfigPageHeadings from "../component/ConfigPageHeadings";
import PayslipSetting from '../component/PayslipSetting';
import { notify } from "../../../services/notify";
import Dropzone from "react-dropzone";
import { CONFIG } from "../../../config/index";
import { Alert } from "reactstrap";
import UploadImageComp from "../../uploadImageCompressed/UploadImageComp";
import * as actionsUsersList from "../../../redux/generic/actions/usersList";
import { getToken } from "../../../services/generic";
import Rh_Config_setting from '../component/RhConfigSetting';

class ContainerHealthStats extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      document_type: "",
      token: "",
      file: [],
      imageUrl: "",
      error:""
    };
  }
  componentWillMount() {
    this.props.onUsersList();
    this.props.getGenricLineConfig();
    this.props.getSlackSettings();
    this.props.getSmptpSettings();
    this.props.onGetPayslipSettings();
    this.props.showHeading("Settings")
  }
  componentWillReceiveProps(props) {
    this.setState({
      token: getToken()
    });
  }
  onDrop = files => {
    this.setState({
      // document_type: "",
      file: files,
      type:
        files[0].name &&
        files[0].name.split(".")[files[0].name.split(".").length - 1]
    });
    const reader = new FileReader();

    reader.onloadend = () => {
      this.setState({
        imageUrl: reader.result
      });
    };
    if (files) {
      reader.readAsDataURL(files[0]);
      this.setState({
        imageUrl: reader.result
      });
    } else {
      this.setState({
        imageUrl: ""
      });
    }
  };
  successfulFileUpload = () => {
    this.setState({
      imageUrl: "",
      file: [],
      document_type: ""
    });
    this.refs.declear.checked = false;
  };

  onSuccessfulFileUpload = () => {
    this.successfulFileUpload();
    if (this.props.loggedUser.data.role === "Admin") {
      this.props.onFetchDevice();
      this.props.onFetchUnapprovedUser();
      this.props.onGetMyDocuments();
    } else if (this.props.loggedUser.data.role === "HR") {
      return;
      // this.props.onFetchDevice();
    } else {
      this.props.onGetMyDocuments();
    }
  };

  callUpdateDocuments = e => {
    let type = this.state.document_type;
    let stop = false;
    if (type === "") {
      stop = true;
      notify("Warning!", "Please select document type.", "warning");
    } else if (!this.state.imageUrl) {
      stop = true;
      notify("Warning!", "Please select a file", "warning");
    } else if (this.refs.declear.checked !== true) {
      stop = true;
      notify("Warning!", "Mark declaration before submit", "warning");
    }

    if (stop) {
      e.preventDefault();
    }
  };

  handleRomoveImage=()=>{
    this.setState({ imageUrl: "",file:[]})
  }

  render() {
    let loggedUserData=this.props.usersList && this.props.usersList.users && this.props.usersList.users.filter((user,i)=>{
      return user.user_Id==this.props.loggedUser.data.id
    })
    const { loginTypes } = this.props;
    return (
      <div>
        <div
          id="content"
          className="app-content box-shadow-z0 content-health-stats"
          role="main"
        >
          <div className="row no-gutters">
            <div className="col-md-12">
            {this.state.error && 
              <Alert
                className="alert-transparent rem-fade rh-section rh-alert"
                color="danger"
              > 
                <span className="bold-text">ALERT:</span>{" "}
                <span>{this.state.error}</span>
              </Alert>}
            </div>
          </div>
          <div className="app-body health-stats-setting" id="view">
            <div className="row m-10 mr-0 ml-0">
              <div className="col-sm-6 setting-section1">
                <div className="">
                  <AttendanceUploadSetting {...this.props} />
                </div>
              </div>
              <div className="col-sm-6 setting-section2">
                <div className="">
                  <PasswordResetSetting
                    resetPasswordData={this.props.requestResetPasswordSetting}
                    resetPasswordStatusData={this.props.resetPasswordStatusData}
                    clearData={this.props.clearData}
                  />
                </div>
              </div>
            </div>
            <div className="row m-10 mr-0 ml-0">
              <div className="col-sm-6 setting-section1">
                <div className="">
                  {this.props.loginTypes && this.props.loginTypes.data && (
                    <LoginTypesSettings
                      loginTypes={this.props.loginTypes}
                      onToggle={data => this.props.changeConfig(data)}
                    />
                  )}
                </div>
              </div>
              <div className="col-sm-6 setting-section2">
                <div>
                  <ShowSalary
                    showSalary={this.props.showSalary}
                    onToggle={data => this.props.changeViewSalary(data)}
                  />
                </div>
              </div>
            </div>
            <div className="row m-10 mr-0 ml-0">
              <div className="col-sm-6 setting-section1">
                <div>
                  <Rh_Config_setting 
                    rh_config={this.props.rh_config}
                    updateData={(data)=>{
                      this.props.onChangeRhConfigSetting(data)
                    }}
                  />
                </div>
              </div>
            </div>
           
            <div className="row m-10 mr-0 ml-0">
              <div className="col-sm-6 setting-section1">
                  <div>
                      <SlackSettings
                       onToggle={data =>data.slack_token? this.props.changeSlackSettings(data):this.setState({
                         error:"Slack Token is Empty"
                       })}
                       slackSettings={this.props.slackSettings}
                       testSlackMsg={data=>this.props.testSlackMsg(data)}
                       loggedUserData={loggedUserData[0]}
                       slack_channel={this.props.slack_channel}
                       addChannel={this.props.addChannel}
                       hideSlackChannel={this.props.hideSlackChannel}
                      />
                  </div>
              </div>
              <div className="col-sm-6 setting-section1">
                  <div>
                      <PayslipSetting
                       onToggle={data =>this.props.onChangePayslipSettings(data)}
                       payslip_settings={this.props.payslip_settings}
                      />
                  </div>
              </div>
            </div>
            
              <div className="row m-10 mr-0 ml-0">
              {loginTypes.isSuccess ? (<div className="col-sm-6 setting-section1">
                  <div className="">
                    <WeeklyHoliday
                      alternative_saturdays={this.props.alternative_saturdays}
                      onSave={data => this.props.setWeeklyHoliday(data)}
                    />
                  </div>
                </div>) : null}
                <div className="col-sm-6">
                  <SmtpSettings
                  smptpSettings={this.props.SmtpSettings}
                  onSaveSmtpSettings={this.props.onSaveSmtpSettings}
                  loggedUserData={loggedUserData[0]}
                  onTestSmtp={this.props.onTestSmtp}
                  />
                </div>
              </div>
            
            <div className="col-sm-6 setting-section2">
                    <div className="">
                    <AttendanceLateDays
                      updateAttendanceLateDays={this.props.attendanceLateLimit}
                      attendance_late_days={ this.props.attendance_late_days}
                    />
                    </div>
                  </div>
                </div>
            {/* confg page heading */}
            <div className="row m-10 mr-0 ml-0">
              <div className="col-sm-12 setting-section1">
                <div className="">
                  <ConfigPageHeadings
                    page_headings={this.props.page_headings}
                    updateConfigPageHeadingsRequest={
                      this.props.updateConfigPageHeadingsRequest
                    }
                    // loginTypes={this.props.loginTypes}
                    // onToggle={data => this.props.changeConfig(data)}
                  />
                </div>
              </div>
            </div>
            {/* Upload Picture Form */}
            <div className="row mx-0">
              <div className="col-sm-8 px-0">
                <div
                  className="box p-a-md m-b-lg mr-3 form-my-doc"
                  id="uploadDoc"
                >
                  <Alert color="info" fade={false}>
                    You can upload multiple documents together{" "}
                  </Alert>
                  <form
                    action={CONFIG.upload_url}
                    method="POST"
                    encType="multipart/form-data"
                  >
                    <div className="form-group  d-flex form_mobile_document">
                      <label className="col-sm-3 form_document_label">
                        Document Type
                      </label>
                      <select
                        className="upload_doc_list form-control col-sm-9 pl-2"
                        ref="document_type"
                        onChange={() => {
                          this.setState({
                            document_type: this.refs.document_type.value,
                            file: [],
                            imageUrl: ""
                          });
                        }}
                        value={this.state.document_type}
                      >
                        <option value="">--- Select Doc Type ---</option>
                        <option value="logo">Logo</option>
                      </select>
                    </div>
                    <input
                      type="hidden"
                      name="document_type"
                      value={this.state.document_type}
                    />
                    <div className="form-group d-flex form_mobile_document">
                      <label className="col-sm-3 form_document_label_type">
                        Upload Document{" "}
                      </label>
                      <Dropzone onDrop={this.onDrop}>
                        {({ getRootProps, getInputProps }) => {
                          return (
                            <section className="container m-0 p-0 col-sm-9">
                              <div {...getRootProps({ className: "dropzone" })}>
                                <input {...getInputProps()} />

                                <div className="drag_and_drop">
                                  {this.state.file &&
                                    this.state.file.map((file, index) => (
                                      <div className="font-weight-bold">
                                        {" "}
                                        {file.name}
                                      </div>
                                    ))}
                                  {this.state.imageUrl ? (
                                    <img
                                      src={this.state.imageUrl}
                                      className="mx-3 img-fluid"
                                      alt="Preview"
                                    />
                                  ) : (
                                    <p className="uploading_doc">
                                      <i className="fi flaticon-upload" />
                                    </p>
                                  )}

                                  <p className="doc_upload_place">
                                    Drop a document here or click to select file
                                    to upload
                                  </p>
                                </div>
                              </div>
                            </section>
                          );
                        }}
                      </Dropzone>
                    </div>
                    <div className="form-group offset-sm-3 ">
                      <input
                        type="checkbox"
                        ref="declear"
                        className="vertical-middle t-scale-1"
                      />
                      <span className="declaration text-danger font-weight-bold">
                        <small className="declare_statement">
                          By uploading this document you certify that these
                          document are true and all information is correct
                        </small>
                      </span>
                    </div>
                    <div className="form-group offset-sm-3">
                      <UploadImageComp
                        validateDocuments={this.callUpdateDocuments}
                        fileParams={{
                          ...this.state,
                          user_id: this.props.user_id,
                          file_upload_action: "logo"
                        }}
                        onSuccessfulFileUpload={this.onSuccessfulFileUpload}
                        url={"generic_upload_url"}
                        showSuccessMessage={this.showSuccessMessage}
                        handleRomoveImage={this.handleRomoveImage}
                      />
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
    );
  }
}
const mapStateToProps = state => {  
  return {
    frontend: state.frontend.toJS(),
    loggedUser: state.logged_user.userLogin,
    attendanceUploadSetting: state.settings.attendanceUploadSetting,
    resetPasswordStatusData: state.settings.reset_password,
    loginTypes: state.settings.loginTypes,
    showSalary: state.settings.showSalary,
    alternative_saturdays: state.settings.alternative_saturdays,
    page_headings: state.settings.page_headings,
    attendance_late_days:state.settings.attendance_late_days,
    slackSettings:state.settings.slackSettings,
    slack_channel:state.settings.slack_channel,
    usersList: state.usersList.toJS(),
    SmtpSettings:state.settings.smtp_settings,
    payslip_settings:state.settings.payslip_settings,
    rh_config:state.settings.rh_config
    
  };
};
const mapDispatchToProps = dispatch => ({
  onChangeRhConfigSetting:(data)=>{
    return dispatch(actions.changeRhConfigSetting(data))
  },
  onUsersList: () => {
    return dispatch(actionsUsersList.get_users_list());
  },
  getSmptpSettings:()=>{
    return dispatch(actions.requestGetSmtpSettings())
  },
  onSaveSmtpSettings:(data)=>{
    return dispatch(actions.saveSmtpSettings(data));
  },
  onTestSmtp:(data)=>{
    return dispatch(actions.testSmtp(data));
  },
  onChangePayslipSettings:(data)=>{
    return dispatch(actions.changePayslipSettings(data))
  },
  onGetPayslipSettings:()=>{
    return dispatch(actions.getPayslipSettings())
  },
  hideSlackChannel: data =>
    dispatch(actions.requestHideSlackChannel(data)),
  addChannel: data =>
    dispatch(actions.requestAddChannel(data)),
  requestAddAttendanceUploadSetting: params =>
    dispatch(actions.requestAddAttendanceUploadSetting(params)),
  requestDeleteAttendanceUploadSetting: params =>
    dispatch(actions.requestDeleteAttendanceUploadSetting(params)),
  requestResetPasswordSetting: params =>
    dispatch(actions.requestResetPasswordSetting(params)),
  clearData: () => dispatch(actions.requestClearResetPasswordData()),
  changeConfig: data => dispatch(actions.setGenricLoginConfig(data)),
  changeViewSalary: data => dispatch(actions.setWebSalaryView(data)),
  changeSlackSettings:data=>dispatch(actions.setSlackSettings(data)),
  getGenricLineConfig: () => dispatch(actions.getGenricLoginConfig()),
  getSlackSettings:  ()=>  dispatch(actions.getSlackSettings()),
  testSlackMsg: (data)=>dispatch(actions.testSlackMsg(data)),
  setWeeklyHoliday: data => dispatch(actions.setWeeklyHoliday(data)),
  attendanceLateLimit:data=>dispatch(actions.setAttendanceLimit(data)),
  updateConfigPageHeadingsRequest: data =>
    dispatch(actions.updateConfigPageHeadingsRequest(data)),
  showHeading: data => {
      return dispatch(actions.showHeading(data));
    },
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ContainerHealthStats);
