import React from "react";
import PropTypes from "prop-types";
import {Button} from 'reactstrap'
import InputText from "../../../components/generic/input/InputText";

class UpdatePassword extends React.Component {
  state = {
      new_password:""
  };
  render() {
    return (
      <div>
        <div className="row no-gutter">
          <div className="col-xs-12 profile-input form-group">
            <label>Enter New Password</label>
            <input
              type="text"
              className="form-control"
              onChange={e => this.setState({ new_password: e.target.value })}
              value={this.state.new_password}
            />
          </div>
          <div className="profile-input form-group col-xs-12">
            <Button
              className="pull-left"
              color="primary"
              onClick={() =>
                this.props.callUpdateUserPassword(this.state.new_password)
              }
            >
              Update Password
            </Button>
          </div>
        </div>
      </div>
    );
  }
}

export default UpdatePassword;
