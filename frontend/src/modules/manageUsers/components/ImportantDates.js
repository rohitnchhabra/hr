import React from "react";
import PropTypes from "prop-types";
import { Label, Button, Spinner } from "reactstrap";
import isEqual from "lodash/isEqual";
import { compose } from "redux";
import isMobile from "../../../components/hoc/WindowResize";
import GenericDatePicker from "../../../components/generic/GenericDatePicker";
import Checkbox from "../../../components/generic/input/Checkbox";

var moment = require("moment");

class ImportantDates extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      username: "",
      user_id: "",
      name: "",
      jobtitle: "",
      dateofjoining: "",
      dob: "",
      gender: "",
      training_month: "",
      marital_status: "",
      team: "",
      current_address: "",
      permanent_address: "",
      emergency_ph1: "",
      emergency_ph2: "",
      blood_group: "",
      medical_condition: "",
      work_email: "",
      other_email: "",
      holding_comments: "",
      send_slack_msg: false,
      slack_msg: "",
      training_completion_date: "",
      termination_date: "",
      notifyEmpConfirmation: false,
      notifyEmpConfirmationPolicies: false,
      notifyEmpTermination: false,
      notifyEmpTerminationPolicies: false,
      showConfirmation: false,
      showTermination: false,
    };
    // this.handleTest = this.handleTest.bind(this);
  }
  componentWillReceiveProps(props) {
    if (!isEqual(props.user_profile_detail, this.props.user_profile_detail)) {
      let username = "";
      let user_id = "";
      let name = "";
      let jobtitle = "";
      let team = "";
      let dateofjoining = "";
      let dob = "";
      let gender = " ";
      let training_month = "";
      let marital_status = " ";
      let current_address = " ";
      let permanent_address = " ";
      let emergency_ph1 = " ";
      let emergency_ph2 = " ";
      let blood_group = " ";
      let medical_condition = " ";
      let work_email = " ";
      let other_email = "";
      let training_completion_date = "";
      let termination_date = "";
      let holding_comments = "";
      let slack_msg = "";
      let userProfileDetail = props.user_profile_detail;

      if (props.username) {
        username = props.username;
      }

      if (userProfileDetail.dateofjoining) {
        dateofjoining = userProfileDetail.dateofjoining;
      }
      if (userProfileDetail.other_email) {
        other_email = props.user_profile_detail.other_email;
        this.setState({
          other_email: other_email
        });
      } else if (userProfileDetail.other_email == null) {
        this.setState({
          other_email: ""
        });
      }
      if (userProfileDetail.user_Id) {
        user_id = userProfileDetail.user_Id;
      }
      if (userProfileDetail.name) {
        name = userProfileDetail.name;
      }
      if (userProfileDetail.jobtitle) {
        jobtitle = userProfileDetail.jobtitle;
      }
      if (userProfileDetail.training_month) {
        training_month = userProfileDetail.training_month;
        this.setState({
          training_month: training_month
        });
      }

      if (userProfileDetail.dob && userProfileDetail.dob !== "0000-00-00") {
        var mydate = new Date(userProfileDetail.dob);
        if (mydate !== "Invalid Date") {
          dob = moment(mydate).format("YYYY-MM-DD");
        }
      }
      if (userProfileDetail.gender) {
        gender = userProfileDetail.gender;
      }
      if (userProfileDetail.marital_status) {
        marital_status = userProfileDetail.marital_status;
        this.setState({
          marital_status: marital_status
        });
      } else if (userProfileDetail.marital_status == null) {
        this.setState({
          marital_status: ""
        });
      }
      if (userProfileDetail.team) {
        team = userProfileDetail.team;
      }
      if (userProfileDetail.current_address) {
        current_address = userProfileDetail.current_address;
        this.setState({
          current_address: current_address
        });
      } else if (userProfileDetail.current_address == null) {
        this.setState({
          current_address: ""
        });
      }
      if (userProfileDetail.permanent_address) {
        permanent_address = userProfileDetail.permanent_address;
      }
      if (userProfileDetail.emergency_ph1) {
        emergency_ph1 = userProfileDetail.emergency_ph1;
      }
      if (userProfileDetail.emergency_ph2) {
        emergency_ph2 = userProfileDetail.emergency_ph2;
      }
      if (userProfileDetail.blood_group) {
        blood_group = userProfileDetail.blood_group;
      }
      if (userProfileDetail.medical_condition) {
        medical_condition = userProfileDetail.medical_condition;
      }
      if (userProfileDetail.work_email) {
        work_email = userProfileDetail.work_email;
      }
      if (
        userProfileDetail.training_completion_date &&
        userProfileDetail.training_completion_date !== "000-00-00"
      ) {
        var mydate = new Date(userProfileDetail.training_completion_date);
        if (mydate !== "Invalid Date") {
          training_completion_date = moment(mydate).format("YYYY-MM-DD");
          this.setState({
            training_completion_date: training_completion_date
          });
        }
      } else if (userProfileDetail.training_completion_date === null) {
        this.setState({
          training_completion_date: null
        });
      } else if (userProfileDetail.training_completion_date == "000-00-00") {
        this.setState({
          training_completion_date: "000-00-00"
        });
      }
      if (
        userProfileDetail.termination_date &&
        userProfileDetail.termination_date !== "0000-00-00"
      ) {
        var mydate = new Date(userProfileDetail.termination_date);
        if (mydate !== "Invalid Date") {
          termination_date = moment(mydate).format("YYYY-MM-DD");
          this.setState({
            termination_date: termination_date
          });
        }
      } else if (userProfileDetail.termination_date === null) {
        this.setState({
          termination_date: null
        });
      } else if (userProfileDetail.termination_date === "0000-00-00") {
        this.setState({
          termination_date: userProfileDetail.termination_date
        });
      }

      if (userProfileDetail.holding_comments) {
        holding_comments = userProfileDetail.holding_comments;
      }
      if (userProfileDetail.slack_msg) {
        slack_msg = userProfileDetail.slack_msg;
      }
      this.setState({
        username: username,
        user_id: user_id,
        name: name,
        jobtitle: jobtitle,
        team: team,
        dateofjoining: dateofjoining,
        dob: dob,
        gender: gender,
        permanent_address: permanent_address,
        emergency_ph1: emergency_ph1,
        emergency_ph2: emergency_ph2,
        blood_group: blood_group,
        medical_condition: medical_condition,
        work_email: work_email,
        holding_comments: holding_comments,
        slack_msg: slack_msg
      });
    }
  }

  handleUpdateClick = () => {
    this.props.callUpdateUserProfileDetails({
      ...this.state, 
      termination_date: this.state.termination_date?moment(this.state.termination_date).format("YYYY-MM-DD"):null,
      training_completion_date:this.state.training_completion_date? moment(this.state.training_completion_date).format("YYYY-MM-DD"):null,
      dateofjoining:this.state.dateofjoining?moment(this.state.dateofjoining).format("YYYY-MM-DD"):null

    },["dateofjoining","termination_date","training_completion_date","dob"]);
  };

  handleCheckboxes = name => {
    this.setState({ [name]: !this.state[name] });
  };

  onSendNotification = name => {
    if (!this.state.send_slack_msg) {
      this.setState({ [name]: "1" });
    } else {
      this.setState({ [name]: "" });
    }
  };

  editTemplate=()=>{
    this.props.history.push("/app/mail_templates")
  }

  render() {
    const { updateProfileLoader } = this.props;
    return (
      <div>
        <div className="row no-gutter">
          <div className="col-sm-12 col-lg-6 profile-input">
            <div className="form-group">
              <Label for="Date Of Birth">Date Of Birth ( eg. 27/1/1988 )</Label>
              <GenericDatePicker
                className="ml-0"
                handleDateChange={dob => this.setState({ dob })}
                type={this.props.isMobile < 768 ? "absolute" : "inline"}
                date={this.state.dob}
                dob={true}
              />
            </div>
          </div>
          <div className="col-sm-12 col-lg-6 profile-input">
            <div className="form-group">
              <Label for="Date Of Joining">
                Date Of Joining ( eg. 2016-12-30 )
              </Label>
              <GenericDatePicker
                className="ml-0"
                handleDateChange={dateofjoining =>
                  this.setState({ dateofjoining})
                }
                type={this.props.isMobile < 768 ? "absolute" : "inline"}
                date={this.state.dateofjoining}
              />
            </div>
          </div>
        </div>
        <div className="row no-gutter">
          <div className="col-sm-12 col-lg-6 profile-input">
            <div className="form-group">
              <Label for="Training Completion Date">
                Training Completion Date
              </Label>
              <GenericDatePicker
                className="ml-0"
                handleDateChange={date =>
                  this.setState(
                    {
                      training_completion_date: date
                    },
                    () => {
                      if (!this.state.showConfirmation) {
                        this.setState({ showConfirmation: true });
                      }
                    }
                  )
                }
                type={this.props.isMobile < 768 ? "absolute" : "inline"}
                date={
                  this.state.training_completion_date == "0000-00-00"
                    ? null
                    : this.state.training_completion_date
                }
              />
            </div>
          </div>
          <div className="col-sm-12 col-lg-6 profile-input">
            <div className="form-group">
              <Label for="Date Of Termination">
                Date Of Termination ( eg. 2016-12-30 )
              </Label>
              <GenericDatePicker
                className="ml-0"
                handleDateChange={date =>
                  this.setState(
                    {
                      termination_date: date
                    },
                    () => {
                      if (!this.state.showTermination) {
                        this.setState({ showTermination: true });
                      }
                    }
                  )
                }
                type={this.props.isMobile < 768 ? "absolute" : "inline"}
                date={
                  this.state.termination_date == "0000-00-00"
                    ? null
                    : this.state.termination_date
                }
              />
            </div>
          </div>
        </div>
        <div className="mt-2 mb-2">
          {this.state.showConfirmation && (
            <>
              <div className="form-group px-2 d-flex  align-items-center">
                <Checkbox
                  id="notifyEmpConfirmation"
                  name="notifyEmpConfirmation"
                  checked={this.state.notifyEmpConfirmation}
                  onClick={this.handleCheckboxes}
                />
                <Label for="notifyEmpConfirmation" className="mb-0 col-sm-7 px-0">
                  Notify Employee about Confirmation
                </Label>
                <small className="py-1 editLink" onClick={this.editTemplate}>(Edit)</small>
              </div>
              <div className="form-group  px-2 d-flex  align-items-center">
                <Checkbox
                  id="notifyEmpConfirmationPolicies"
                  name="notifyEmpConfirmationPolicies"
                  checked={this.state.notifyEmpConfirmationPolicies}
                  onClick={this.handleCheckboxes}
                />
                <Label
                  for="notifyEmpConfirmationPolicies"
                  className="mb-0 col-sm-7 px-0"
                >
                  Notify Employee about Confirmation Policies
                </Label>
                <small className="py-1 editLink" onClick={this.editTemplate}>(Edit)</small>
              </div>
            </>
          )}
          {this.state.showTermination && (
            <>
              <div className="form-group px-2 d-flex  align-items-center">
                <Checkbox
                  id="notifyEmpTermination"
                  name="notifyEmpTermination"
                  checked={this.state.notifyEmpTermination}
                  onClick={this.handleCheckboxes}
                />
                <Label for="notifyEmpTermination" className="mb-0 col-sm-7 px-0">
                  Notify Employee about Termination
                </Label>
                <small className="py-1 editLink" onClick={this.editTemplate}>(Edit)</small>
              </div>
              <div className="form-group px-2 d-flex  align-items-center">
                <Checkbox
                  id="notifyEmpTerminationPolicies"
                  name="notifyEmpTerminationPolicies"
                  checked={this.state.notifyEmpTerminationPolicies}
                  onClick={this.handleCheckboxes}
                />
                <Label for="notifyEmpTerminationPolicies" className="mb-0 col-sm-7 px-0">
                  Notify Employee about Termination Policies
                </Label>
                <small className="py-1 editLink" onClick={this.editTemplate}>(Edit)</small>
              </div>
            </>
          )}
        </div>
        <div className="d-flex px-1">
          <Button
            color="primary"
            className="pull-right"
            onClick={this.handleUpdateClick}
            disabled={updateProfileLoader}
          >
            {updateProfileLoader ? <Spinner color="white" /> : "Save"}
          </Button>
          <div className="col px-2 d-flex  align-items-center">
            <i
              className="fa fa-slack align-self-center hash-icon"
              aria-hidden="true"
            />
            <Label for="send_slack_msg" className="mb-0 px-3">
              {" "}
              Don't send notification for current update
            </Label>
            <Checkbox
              id="send_slack_msg"
              name="send_slack_msg"
              checked={this.state.send_slack_msg}
              onClick={this.onSendNotification}
            />
          </div>
        </div>
      </div>
    );
  }
}

export default compose(isMobile)(ImportantDates);

ImportantDates.propTypes = {
  user_profile_detail: PropTypes.object.isRequired,
  callUpdateUserProfileDetails: PropTypes.func.isRequired,
  teamList: PropTypes.object.isRequired,
  loggedUser: PropTypes.object.isRequired,
  updateProfileLoader: PropTypes.bool
};
