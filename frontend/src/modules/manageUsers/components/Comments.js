import React from "react";
import PropTypes from "prop-types";
import { CONFIG } from "../../../config/index";
import Textarea from "../../../components/generic/input/TextArea";
import { Label, Button } from "reactstrap";
import { confirm } from "../../../services/notify";
import isEqual from "lodash/isEqual";
import cloneDeep from "lodash/cloneDeep";
import { Spinner } from "reactstrap";
import Checkbox from "../../../components/generic/input/Checkbox";
var moment = require("moment");

class Comments extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      username: "",
      user_id: "",
      name: "",
      jobtitle: "",
      dateofjoining: "",
      dob: "",
      gender: "",
      training_month: "",
      marital_status: "",
      team: "",
      current_address: "",
      permanent_address: "",
      emergency_ph1: "",
      emergency_ph2: "",
      blood_group: "",
      medical_condition: "",
      work_email: "",
      other_email: "",
      training_completion_date: "",
      termination_date: "",
      holding_comments: "",
      send_slack_msg: false,
      slack_msg: ""
    };
  }
  componentWillReceiveProps(props) {
    if (!isEqual(props.user_profile_detail, this.props.user_profile_detail)) {
      let username = "";
      let user_id = "";
      let name = "";
      let jobtitle = "";
      let team = "";
      let dateofjoining = "";
      let dob = "";
      let gender = " ";
      let training_month = "";
      let marital_status = " ";
      let current_address = " ";
      let permanent_address = " ";
      let emergency_ph1 = " ";
      let emergency_ph2 = " ";
      let blood_group = " ";
      let medical_condition = " ";
      let work_email = " ";
      let other_email = "";
      let training_completion_date = "";
      let termination_date = "";
      let holding_comments = "";
      let slack_msg = "";
      let userProfileDetail = props.user_profile_detail;
      if (typeof props.username !== "undefined" && props.username != null) {
        username = props.username;
      }
      if (
        typeof props.user_profile_detail.other_email !== "undefined" &&
        props.user_profile_detail.other_email != null
      ) {
        other_email = props.user_profile_detail.other_email;
        this.setState({
          other_email: other_email
        });
      }
      else if(userProfileDetail.other_email==null){
        this.setState({
          other_email:''
        })
      }
      if (
        typeof userProfileDetail.user_Id !== "undefined" &&
        userProfileDetail.user_Id != null
      ) {
        user_id = userProfileDetail.user_Id;
      }
      if (
        typeof userProfileDetail.name !== "undefined" &&
        userProfileDetail.name != null
      ) {
        name = userProfileDetail.name;
      }
      if (
        typeof userProfileDetail.jobtitle !== "undefined" &&
        userProfileDetail.jobtitle != null
      ) {
        jobtitle = userProfileDetail.jobtitle;
      }
      if (
        typeof userProfileDetail.training_month !== "undefined" &&
        userProfileDetail.training_month !== null
      ) {
        training_month = userProfileDetail.training_month;
      }
      if (
        typeof userProfileDetail.dateofjoining !== "undefined" &&
        userProfileDetail.dateofjoining != null &&
        userProfileDetail.dateofjoining !== "0000-00-00"
      ) {
        dateofjoining = userProfileDetail.dateofjoining;
      }
      if (
        typeof userProfileDetail.dob !== "undefined" &&
        userProfileDetail.dob !== null &&
        userProfileDetail.dob !== "0000-00-00"
      ) {
        var mydate = new Date(userProfileDetail.dob);
        if (mydate !== "Invalid Date") {
          dob = moment(mydate).format("YYYY-MM-DD");
        }
      }
      if (
        typeof userProfileDetail.training_month !== "undefined" &&
        userProfileDetail.training_month != null
      ) {
        training_month = userProfileDetail.training_month;
        this.setState({
          training_month: training_month
        });
      }
      if (
        typeof userProfileDetail.gender !== "undefined" &&
        userProfileDetail.gender != null
      ) {
        gender = userProfileDetail.gender;
      }
      if (
        typeof userProfileDetail.marital_status !== "undefined" &&
        userProfileDetail.marital_status != null
      ) {
        marital_status = userProfileDetail.marital_status;
        this.setState({
          marital_status: marital_status
        });
      }
      else if(userProfileDetail.marital_status==null){
        this.setState({
          marital_status:''
        })
      }
      if (
        typeof userProfileDetail.team !== "undefined" &&
        userProfileDetail.team != null
      ) {
        team = userProfileDetail.team;
      }
      if (
        typeof userProfileDetail.current_address !== "undefined" &&
        userProfileDetail.current_address != null
      ) {
        current_address = userProfileDetail.current_address;
        this.setState({
          current_address: current_address
        });
      }
      else if(userProfileDetail.current_address==null){
        this.setState({
          current_address:''
        })
      }
      if (
        typeof userProfileDetail.permanent_address !== "undefined" &&
        userProfileDetail.permanent_address != null
      ) {
        permanent_address = userProfileDetail.permanent_address;
      }
      if (
        typeof userProfileDetail.emergency_ph1 !== "undefined" &&
        userProfileDetail.emergency_ph1 != null
      ) {
        emergency_ph1 = userProfileDetail.emergency_ph1;
      }
      if (
        typeof userProfileDetail.emergency_ph2 !== "undefined" &&
        userProfileDetail.emergency_ph2 != null
      ) {
        emergency_ph2 = userProfileDetail.emergency_ph2;
      }
      if (
        typeof userProfileDetail.blood_group !== "undefined" &&
        userProfileDetail.blood_group != null
      ) {
        blood_group = userProfileDetail.blood_group;
      }
      if (
        typeof userProfileDetail.medical_condition !== "undefined" &&
        userProfileDetail.medical_condition != null
      ) {
        medical_condition = userProfileDetail.medical_condition;
      }
      if (
        typeof userProfileDetail.work_email !== "undefined" &&
        userProfileDetail.work_email != null
      ) {
        work_email = userProfileDetail.work_email;
      }
      if (
        typeof userProfileDetail.training_completion_date !== "undefined" &&
        userProfileDetail.training_completion_date !== null &&
        userProfileDetail.training_completion_date !== "0000-00-00"
      ) {
        var mydate = new Date(userProfileDetail.training_completion_date);
        if (mydate !== "Invalid Date") {
          training_completion_date = mydate;
          this.setState({
            training_completion_date: training_completion_date
          });
        }
      } else if (userProfileDetail.training_completion_date === null) {
        this.setState({
          training_completion_date: null
        });
      }
      else if (userProfileDetail.training_completion_date === "0000-00-00") {
        this.setState({
          training_completion_date: userProfileDetail.training_completion_date
        });
      }

      if (
        typeof userProfileDetail.termination_date !== "undefined" &&
        userProfileDetail.termination_date !== null &&
        userProfileDetail.termination_date !== "0000-00-00"
      ) {
        var mydate = new Date(userProfileDetail.termination_date);
        if (mydate !== "Invalid Date") {
          termination_date = mydate;
          this.setState({
            termination_date: termination_date
          });
        }
      } else if (userProfileDetail.termination_date === null) {
        this.setState({
          termination_date: null
        });
      }
      else if (userProfileDetail.termination_date === "0000-00-00") {
        this.setState({
          termination_date: userProfileDetail.termination_date
        });
      }

      if (
        typeof userProfileDetail.holding_comments !== "undefined" &&
        userProfileDetail.holding_comments != null
      ) {
        holding_comments = userProfileDetail.holding_comments;
      }
      if (
        typeof userProfileDetail.slack_msg !== "undefined" &&
        userProfileDetail.slack_msg != null
      ) {
        slack_msg = userProfileDetail.slack_msg;
      }
      this.setState({
        username: username,
        user_id: user_id,
        name: name,
        jobtitle: jobtitle,
        team: team,
        dateofjoining: dateofjoining,
        dob: dob,
        gender: gender,
        permanent_address: permanent_address,
        emergency_ph1: emergency_ph1,
        emergency_ph2: emergency_ph2,
        blood_group: blood_group,
        medical_condition: medical_condition,
        work_email: work_email,
        holding_comments: holding_comments,
        slack_msg: slack_msg
      });
    }
  }

  handleUpdateClick = () => {
      this.props.callUpdateUserProfileDetails({
        ...this.state
      },["medical_condition","holding_comments"]);
  };

  onSendNotification = name => {
    if (!this.state.send_slack_msg) {
      this.setState({ [name]: "1" });
    } else {
      this.setState({ [name]: "" });
    }
  };

  render() {
    const { updateProfileLoader } = this.props;

    return (
      <div>
        <div className="row no-gutter">
          <div
            className="col-lg-6 col-sm-12 profile-input"
          >
            <div className="form-group">
              <Label for="Employee Holding Comment">
                General Employee Comment
              </Label>
              <Textarea
                onchange={e =>
                  this.setState({ holding_comments: e.target.value })
                }
                value={this.state.holding_comments}
              />
            </div>
          </div>
          <div
            className="col-lg-6 col-sm-12 profile-input"
          >
            <div className="form-group">
              <Label for="Any Medical Conditions">Any Medical Conditions</Label>
              <Textarea
                onchange={e =>
                  this.setState({ medical_condition: e.target.value })
                }
                value={this.state.medical_condition}
              />
            </div>
          </div>
        </div>

        <div className="d-flex px-1">
          <Button
            color="primary"
            className="pull-right"
            onClick={this.handleUpdateClick}
            disabled={updateProfileLoader}
          >
            {updateProfileLoader ? <Spinner color="white" /> : "Save"}
          </Button>
          <div className="col px-2 d-flex align-items-center">
            <i
              className="fa fa-slack align-self-center hash-icon"
              aria-hidden="true"
            />
            <Label for="send_slack_msg" className="mb-0 px-3">
              {" "}
              Don't send notification for current update
            </Label>
            <Checkbox
              id="send_slack_msg"
              name="send_slack_msg"
              checked={this.state.send_slack_msg}
              onClick={this.onSendNotification}
            />
          </div>
        </div>
      </div>
    );
  }
}

export default Comments;

Comments.propTypes = {
  user_profile_detail: PropTypes.object.isRequired,
  callUpdateUserProfileDetails: PropTypes.func.isRequired,
  teamList: PropTypes.object.isRequired,
  loggedUser: PropTypes.object.isRequired,
  updateProfileLoader: PropTypes.bool
};
