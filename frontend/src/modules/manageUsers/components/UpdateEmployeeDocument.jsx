import React from "react";
import * as _ from "lodash";
import { notify } from "../../../services/notify";
import { getToken } from "../../../services/generic";
import {  Button, Alert} from "reactstrap";
class UpdateEmployeeDocument extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      user_id: "",
      open: false,
      doc_type: "",
      user_token: ""
    };
    this.deleteDocument = this.deleteDocument.bind(this);
    this.handleOpen = this.handleOpen.bind(this);
    this.handleClose = this.handleClose.bind(this);
    this.callUpdateDocuments = this.callUpdateDocuments.bind(this);
  }
  componentWillReceiveProps(props) {
    this.setState({ user_token: getToken() });
    if (typeof props.user_id !== "undefined" && props.user_id !== null) {
      this.setState({ user_id: props.user_id });
    }
  }
  handleOpen() {
    this.setState({ open: true });
  }
  handleClose() {
    this.setState({ open: false });
  }
  callUpdateDocuments(e) {
    let type = this.state.doc_type;
    let link1 = this.refs.file.value;
    let stop = false;
    if (this.state.user_id === "") {
      stop = true;
      notify("User not selected");
    } else if (type === "") {
      stop = true;
      notify("Please select document type");
    } else if (link1 === "") {
      stop = true;
      notify("Please select a file");
    } else if (this.refs.declear.checked !== true) {
      stop = true;
      notify("Mark declaration before submit");
    }
    if (stop) {
      e.preventDefault();
    }
  }
  deleteDocument(docId) {
    this.props
      .onDeleteDocument(docId)
      .then(msg => {
        this.props.onGetUserDocument(this.state.user_id);
        notify(msg.toString());
      })
      .catch(err => {
        notify(err.toString());
      });
  }
  render() {
    let userDoc = [];
    _.map(this.props.user_documents, (doc, key) => {
      userDoc.push(
        <div key={key} className="col-md-4 p-1 align-self-end">
            <div className="_500 block">
              {doc.document_type}
            </div>
            {typeof doc.link_1 === "undefined" ? (
              ""
            ) : (
              <span className="text-muted">
                <div className="doc-frame" dangerouslySetInnerHTML={{ __html: doc.link_1 }} />
              </span>
            )}
        </div>
      );
    });
    return (
      <div>
        <h6 className="text-center">Uploaded Documents</h6>
        <hr />
        <div>
          <div className="m-b row no-gutters">
            {userDoc.length === 0 ? (
               <div className="col-md-12">
                 <Alert
                className="alert-transparent rem-fade rh-section rh-alert"
                color="danger"
                >
                <span>No document uploaded</span>
                </Alert>
               </div>
            ) : (
              userDoc
            )}
          </div>
        </div>
        <div className="text-center">
          {this.props.disabled ? (
            ""
          ) : (
            <Button
            color="primary"
            className="pull-right"
            onClick={() =>
                this.props.history.push(`/app/add_documents/${this.state.user_id}`)
              }
            >
              Upload New Documents 
            </Button>
          )}
        </div>
      </div>
    );
  }
}

export default UpdateEmployeeDocument;
