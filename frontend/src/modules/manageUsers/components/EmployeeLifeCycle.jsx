import React from "react";
import _ from "lodash";
import PropTypes from "prop-types";
import ToggleButton from "react-toggle-button";
import $ from "jquery";
import { Button } from "reactstrap";
import TaskContainer from "../../../components/TaskContainer/TaskContainer";

const EmployeeLifeCycle = ({
  employee_life_cycle,
  handleChangeSteps,
  user_profile_detail,
  handleConfirm,
  handleTerminate,
  collapseHandler,
  collapseId
}) => {
  $(document).ready(function() {
    $("#empLifeCycle5501").on("show.bs.collapse", function() {
      $(".emp-life-cycle .expand-icon").text("expand_less");
    });
    $("#empLifeCycle5501").on("hide.bs.collapse", function() {
      $(".emp-life-cycle .expand-icon").text("expand_more");
    });
    $("#empLifeCycle5502").on("show.bs.collapse", function() {
      $(".emp-life-cycle .expand-icon").text("expand_less");
    });
    $("#empLifeCycle5502").on("hide.bs.collapse", function() {
      $(".emp-life-cycle .expand-icon").text("expand_more");
    });
    $("#empLifeCycle5503").on("show.bs.collapse", function() {
      $(".emp-life-cycle .expand-icon").text("expand_less");
    });
    $("#empLifeCycle5503").on("hide.bs.collapse", function() {
      $(".emp-life-cycle .expand-icon").text("expand_more");
    });
  });
  let stages = [];
  let stage1 = { steps: [] };
  let stage2 = { steps: [] };
  let stage3 = { steps: [] };

  let employee_action = "";
  if (employee_life_cycle && Object.keys(employee_life_cycle).length) {
    if (employee_life_cycle) {
      let arr = Object.keys(employee_life_cycle);

      let newArr = arr.filter((val, index) => {
        return employee_life_cycle[val].text === "Termination";
      });
      

      stage3 = employee_life_cycle[newArr[0]];
    
    }
    if (employee_life_cycle) {
      let arr = Object.keys(employee_life_cycle);

      let newArr = arr.filter((val, index) => {
        return employee_life_cycle[val].text === "Employment Confirmation";
      });
      stage2 = employee_life_cycle[newArr[0]];
    }
    // employee_action = (
    //   <Button color="danger" onClick={handleTerminate}>
    //     Termination
    //   </Button>
    // );
    // } else {
    if (employee_life_cycle) {
      let arr = Object.keys(employee_life_cycle);

      let newArr = arr.filter((val, index) => {
        return employee_life_cycle[val].text === "New Employee Onboarding";
      });
      stage1 = employee_life_cycle[newArr[0]];
    }

    // if (
    //   user_profile_detail.termination_date === null &&
    //   user_profile_detail.training_completion_date === null
    // ) {
    //   employee_action = (
    //     <div className="mb-2u">
    //       <Button className="my-2" color="primary" onClick={handleConfirm}>
    //         Employee Confirmation
    //       </Button>
    //       <Button className="ml-3" color="danger" onClick={handleTerminate}>
    //         Termination
    //       </Button>
    //     </div>
    //   );
    // }
    stages = stages.concat(stage1, stage2, stage3);
  }
  return (
    <div className="col-xs-12  employee-life-cycle pr-0 pl-0 ">
      <div className="card-design">
        {stages.map((val, index) => {
          return (
            <TaskContainer
              data={val.steps}
              stage={val.text}
              stageId={val.stage_id}
              handleChangeSteps={handleChangeSteps}
              collapseHandler={(id)=>collapseHandler(id)}
              collapseId={collapseId}
            />
          );
        })}
      </div>
    </div>
  );
};

export default EmployeeLifeCycle;

EmployeeLifeCycle.propTypes = {
  data: PropTypes.shape({
    employee_life_cycle: PropTypes.object.isRequired
  }).isRequired,
  handleChangeSteps: PropTypes.func.isRequired
};
