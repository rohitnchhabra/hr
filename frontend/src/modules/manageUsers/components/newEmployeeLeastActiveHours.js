import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { withRouter } from 'react-router';
import CircularProgress from "material-ui/CircularProgress";
import * as actions from '../../../redux/actions';
import GenericDatePicker from "../../../components/generic/GenericDatePicker"
import {compose} from "redux";
import isMobile from "../../../components/hoc/WindowResize";

import _ from 'lodash';
import moment from 'moment';

class NewEmployeeLeastActiveHours extends Component {
    state={}
    handleChange=(date,type)=>{
        this.setState({
            [type]:date
        })
        this.props.handleStartDate(date)
    }
    render() {
        let leastaActiveList;        
        leastaActiveList = _.map(this.props.empActiveHoursList.data, (val,i) => {
            return(
                <tr key={i}>
                    <td>{val.name}</td>
                        <td>{val.average_inside_hours}  <span>({val.totalPresentDays} day present)</span>
                    </td>
                </tr>
            )
        })
        return (
            <div className="col-xs-12 employee-life-cycle">
                <div className="text-center emp-life-cycle">
                    <div>
                        <h4 style={{ fontSize: "16px", fontWeight: 'bold' }}>LEAST ACTIVE EMPLOYEES</h4>
                        <div className="row ">
                            <div className="col-sm-6">
                                <h4 style={{ fontSize: "16px", fontWeight: 'bold', display: "inline-block", marginRight: "20px" }}>Select Start Date</h4>
                                <div style={{ display: "inline-block" }}>
                                    
                                     <GenericDatePicker
                                        handleDateChange={(date)=>this.handleChange(date,'start') }
                                        type={this.props.isMobile < 768 ? "absolute" : "inline"}
                                        date={ this.state.start||null}
                                        emptyLabel="Select Start Date"
                                      />
                                </div>
                            </div>
                            <div className="col-sm-6">
                                <h4 style={{ fontSize: "16px", fontWeight: 'bold', display: "inline-block",marginRight:"20px" }}>Select End Date</h4>
                                <div style={{ display: "inline-block" }}>
                                <GenericDatePicker
                                 handleDateChange={date => this.handleChange(date,'end') }
                                 type={this.props.isMobile < 768 ? "absolute" : "inline"}
                                 date={ this.state.start||null}
                                 emptyLabel="Select End Date"
                                />
                                </div>
                            </div>
                        </div>
                        <div className="table-responsive">
                            <div className="container-fluid">
                            </div>  
                            <table className="table table-striped table-hover">
                                <thead style={{ textAlign: "center" }}>
                                    <tr>
                                        <th style={{ textAlign: "center" }}>Employee Name</th>
                                        <th style={{ textAlign: "center" }}>Average Inside Hours</th>
                                    </tr>
                                </thead>
                                {this.props.empActiveHoursList.isLoading ?
                                    <tbody>
                                        <tr>
                                            <td colSpan="2" className="text-center">
                                                <CircularProgress
                                                    size={30}
                                                    thickness={3}
                                                />
                                            </td>
                                        </tr>
                                    </tbody>
                                    : this.props.empActiveHoursList.data.length == 0 ?
                                        <tr>
                                            <td colSpan="2" className="text-center">No Data Found </td>
                                        </tr>
                                        :
                                       <tbody>
                                            {leastaActiveList}
                                        </tbody>}
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default compose(isMobile)(NewEmployeeLeastActiveHours)