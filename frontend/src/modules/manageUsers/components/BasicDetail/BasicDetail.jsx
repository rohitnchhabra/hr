import React, { useState, useEffect } from "react";
import PropTypes from "prop-types";
import { InputTextWithLabel } from "../../../../components/generic/InputText/InputText";
import Spinner from "../../../../components/generic/Spinner/GenericSpinner";
import { ButtonPrimary } from "../../../../components/generic/buttons/Button";
import { Form, TabContent, TabPane } from "reactstrap";
import ToggleButton from "../../../../components/generic/toggleButton/ToggleButton";
import Checkbox from "../../../../components/generic/input/Checkbox";
import { SelectWithLabel } from "../../../../components/generic/select/Select";

const BasicDetail = (props) => {
  const { updateProfileLoader } = props;
  const [state, setState] = useState({});
  useEffect(() => {
    setState({
      ...props.user_profile_detail,
      user_id: props.user_profile_detail && props.user_profile_detail.user_Id,
      send_slack_msg: false,
    });
  }, [props.user_profile_detail]);
  const handleUpdateClick = (e) => {
    e.preventDefault();
    props.callUpdateUserProfileDetails(state, [
      "name",
      "jobtitle",
      "team",
      "work_email",
      "training_month",
      "other_email",
      "blood_group",
      "gender",
      "marital_status",
    ]);
  };
  const onSendNotification = (e) => {
    setState({ ...state, send_slack_msg: !state.send_slack_msg });
  };
  return (
    // <TabContent activeTab={props.activeTab}>
    //   <TabPane tabId="tab21" className="manage-users-tabPane pb-4">
    <Form onSubmit={(e) => handleUpdateClick(e)} method="post">
      <div className="row no-gutter">
        <div className="col-sm-12 col-lg-6 profile-input">
          <div className="form-group">
            <InputTextWithLabel
              onChange={(e) =>
                setState({
                  ...state,
                  name: e.target.value,
                })
              }
              label={"Name"}
              value={state.name}
              id="name"
            />
          </div>
        </div>
        <div className="col-sm-12 col-lg-6 profile-input">
          <div className="form-group">
            <InputTextWithLabel
              onChange={(e) =>
                setState({
                  ...state,
                  jobtitle: e.target.value,
                })
              }
              label={"Job Title"}
              value={state.jobtitle}
              id="jobTitle"
            />
          </div>
        </div>
      </div>
      <div className="row no-gutter">
        <div className="col-sm-12 col-lg-6 profile-input">
          <SelectWithLabel
            label={"Training Month ( eg. 0-4 month )"}
            onChange={(e) =>
              setState({ ...state, training_month: e.target.value })
            }
            value={state.training_month}
          >
            <option disabled>--select month--</option>
            <option value="0">0 month </option>
            <option value="1">1 month</option>
            <option value="2">2 month</option>
            <option value="3">3 month</option>
            <option value="4">4 month</option>
          </SelectWithLabel>
        </div>
        <div className="col-md-12 col-lg-6 profile-input">
          <InputTextWithLabel
            type="email"
            pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2, 3}$"
            className="form-control"
            onChange={(e) =>
              setState({ ...state, other_email: e.target.value })
            }
            label={"Personal Email"}
            value={state.other_email}
            id={"Persoanl Email"}
          />
        </div>
      </div>
      <div className="row no-gutter">
        <div className={"col-md-12 profile-input"}>
          <InputTextWithLabel
            type="email"
            pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2, 3}$"
            onChange={(e) => setState({ ...state, work_email: e.target.value })}
            label={"Work Email"}
            value={state.work_email}
            id="Work Email"
          />
        </div>
      </div>
      <div className="row no-gutter">
        <div className={"col-md-6 profile-input"}>
          <SelectWithLabel
            label={"Blood Group"}
            onChange={(e) =>
              setState({ ...state, blood_group: e.target.value })
            }
            value={state.blood_group}
          >
            <option value="">--Select your Blood Group--</option>
            <option value="0">O-</option>
            <option value="1">O+</option>
            <option value="2">A+</option>
            <option value="3">A-</option>
            <option value="4">B+</option>
            <option value="5">B-</option>
            <option value="6">AB+</option>
            <option value="7">AB-</option>
          </SelectWithLabel>
        </div>
        <div className="col-md-6 col-12 profile-input">
          <SelectWithLabel
            label={"Team"}
            onChange={(e) => setState({ ...state, team: e.target.value })}
            value={state.team}
          >
            <option value="">--Select Team--</option>
            {props.teamList &&
              props.teamList.map((val, i) => (
                <option value={val} key={i}>
                  {val}
                </option>
              ))}
          </SelectWithLabel>
        </div>
      </div>
      <div className="row no-gutter">
        <div className="col-sm-12 col-lg-6 profile-input">
          <SelectWithLabel
            label="Gender"
            onChange={(e) => setState({ ...state, gender: e.target.value })}
            value={state.gender}
          >
            <option value="">--Select gender--</option>
            <option value="Female">Female</option>
            <option value="Male">Male</option>
            <option value="Other">Other</option>
          </SelectWithLabel>
        </div>
        <div className="col-sm-12 col-lg-6 profile-input">
          <SelectWithLabel
            label={"Marital Status"}
            onChange={(e) =>
              setState({ ...state, marital_status: e.target.value })
            }
            value={state.marital_status}
          >
            <option value="">--Select marital status--</option>
            <option value="Single">Single</option>
            <option value="Married">Married</option>
            <option value="Other">Other</option>
          </SelectWithLabel>
        </div>
      </div>
      <div className="px-1 py-2">
        <div className="d-flex">
          <label className="slack-label">
            <i className="fa fa-bell-slash fa-lg" aria-hidden="true" />{" "}
            <span className="pl-1">
              {" "}
              Permanently Disabled Notification For This User
            </span>
          </label>
          <span className="slack-toggle">
            <ToggleButton
              value={Number(state.slack_msg) ? Number(state.slack_msg) : 0}
              onToggle={props.onToggle}
            />
          </span>
        </div>

        <div className="d-flex">
          <ButtonPrimary
            type="submit"
            disabled={updateProfileLoader}
            id="sunmit_employee_data"
          >
            {updateProfileLoader ? <Spinner color="white" /> : "Save"}
          </ButtonPrimary>
          <div className="col px-2 d-flex  align-items-center">
            <i
              className="fa fa-slack align-self-center hash-icon"
              aria-hidden="true"
            />
            <label for="send_slack_msg" className="mb-0 px-3">
              {" "}
              Don't send notification for current update
            </label>
            <Checkbox
              id="send_slack_msg"
              name="send_slack_msg"
              checked={state.send_slack_msg ? state.send_slack_msg : false}
              onClick={onSendNotification}
            />
          </div>
        </div>
      </div>
    </Form>
    //   </TabPane>
    // </TabContent>
  );
};
BasicDetail.propTypes = {
  user_profile_detail: PropTypes.object.isRequired,
  callUpdateUserProfileDetails: PropTypes.func.isRequired,
  teamList: PropTypes.array.isRequired,
  updateProfileLoader: PropTypes.bool,
  onToggle: PropTypes.func,
};

export default BasicDetail;
