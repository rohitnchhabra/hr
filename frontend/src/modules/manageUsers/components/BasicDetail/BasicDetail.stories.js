import React from "react";
import BasicDetail from "./BasicDetail";
import { withKnobs,text,boolean } from "@storybook/addon-knobs";
import { action } from "@storybook/addon-actions";

export default {
  title: "BasicDetail",
  decorators: [withKnobs],
};
const teamList = ["react", "php", "node js"];
const user_profile_detail = {
  name: text("name","name"),
  other_email: text("OtherEmail","test@gmail.com"),
  marital_status: text("marital_status","Married"),
  gender: "Male",
  jobtitle:text("JobTitle","Developer"),
  blood_group:"1",
  work_email:text("WorkEmail","test@gmail.com"),
  send_slack_msg:boolean("send_slack_msg",false)
};

export const basicDetail = () => (
  <BasicDetail
    activeTab={"tab21"}
    teamList={teamList}
    callUpdateUserProfileDetails={action("clicked")}
    user_profile_detail={user_profile_detail}
    updateProfileLoader={boolean("loader",false)}
  />
);
