import React from "react";
import Dialog from "material-ui/Dialog";
import TextField from "material-ui/TextField";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import Header from "../../../components/generic/Header";
import { CONFIG } from "../../../config/index";
import { withRouter } from "react-router";
import CircularProgress from "material-ui/CircularProgress";
import * as actions from "../../../redux/actions";
import * as actionsUsersList from "../../../redux/generic/actions/usersList";
import * as actionsManageUsers from "../../../redux/manageUsers/actions/manageUsers";
import { Button, Spinner, Alert, Input } from "reactstrap";
import { compose } from "redux";
import isMobile from "../../../components/hoc/WindowResize";
import GenericDatePicker from "../../../components/generic/GenericDatePicker";
import Checkbox from "../../../components/generic/input/Checkbox";

class FormAddNewEmployee extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      dateofjoining: new Date(),
      name: "",
      user_id: "",
      jobtitle: "",
      gender: "",
      dob: null,
      username: "",
      training_month: "",
      workemail: "",
      personalEmail: "",
      notifyNewEmpHrms: false,
      notifyNewEmpPolicies: false,
      notifyNewEmpImportantInformation: false,
      notifyNewEmpGeneralIntro: false
    };
  }
  handleCheckboxes = name => {
    this.setState({ [name]: !this.state[name] });
  };

  editTemplate=()=>{
    this.props.history.push("/app/mail_templates")
  }
  render() {
    return (
      <div>
        <div id="content" className="app-content box-shadow-z0" role="main">
          <Header
            pageTitle={"Add New Employee Details"}
            showLoading={this.props.frontend.show_loading}
          />
          <div className="app-body" id="view" className="formAddNewEmployee">
            <div className="">
              <div className="row">
                <div className="col-md-12">
                  <div className="col-sm-12 basicInfo pt-3 pl-3">
                    {this.props.message2 && (
                      <div>
                        <Alert
                          className="alert-transparent rem-fade rh-section rh-alert"
                          color="danger"
                        >
                          <span className="alert-bold">ALERT</span>:{" "}
                          {this.props.message2}
                        </Alert>
                      </div>
                    )}
                  </div>
                  <div className="col-xs-12 basicInfo p-0 pb-2">
                    <div className="col-sm-12 col-lg-6 input-wrapper">
                      <label className="addEmployee-label" for="dateOfJoining">
                        Date of Joining
                      </label>
                      <div className="datepicker">
                        <GenericDatePicker
                          handleDateChange={dateofjoining =>
                            this.setState({ dateofjoining })
                          }
                          type={
                            this.props.isMobile < 768 ? "absolute" : "inline"
                          }
                          date={this.state.dateofjoining}
                        />
                      </div>
                    </div>
                    <div className="col-sm-12 col-lg-6 input-wrapper">
                      <label className="addEmployee-label">
                        {" "}
                        Date of Birth
                      </label>
                      <div className="datepicker">
                        <GenericDatePicker
                          handleDateChange={dob => this.setState({ dob })}
                          type={
                            this.props.isMobile < 768 ? "absolute" : "inline"
                          }
                          date={this.state.dob}
                          dob={true}
                        />
                      </div>
                    </div>
                  </div>
                  <div className="col-xs-12 basicInfo p-0">
                    <div className="col-sm-12 col-lg-6 input-wrapper">
                      <label className="addEmployee-label">
                        Training Month
                      </label>
                      <select
                        className="form-control"
                        ref="training_month"
                        onChange={evt => {
                          this.setState({
                            training_month: evt.target.value
                          });
                        }}
                      >
                        <option>--Select month--</option>
                        <option value="0">0 month </option>
                        <option value="1">1 month</option>
                        <option value="2">2 month</option>
                        <option value="3">3 month</option>
                        <option value="4">4 month</option>
                      </select>
                    </div>
                    <div className="col-sm-12 col-lg-6 input-wrapper ">
                      <label className="addEmployee-label"> Gender</label>
                      <select
                        className="form-control"
                        ref="gender"
                        onChange={e =>
                          this.setState({ gender: e.target.value })
                        }
                        value={this.state.gender}
                      >
                        <option value="">--Select gender--</option>
                        <option value="Female">Female</option>
                        <option value="Male">Male</option>
                        <option value="Other">Other</option>
                      </select>
                    </div>
                  </div>
                  <div className="col-xs-12 basicInfo p-0 mt-2">
                    <div className="col-sm-12 col-lg-6 input-wrapper">
                      <label className="addEmployee-label">Name</label>
                      <Input
                        fullWidth
                        type="text"
                        placeholder="Name"
                        onChange={e => this.setState({ name: e.target.value })}
                        value={this.state.name}
                      />
                    </div>

                    <div className="col-lg-6 col-sm-12 input-wrapper">
                      <label className="addEmployee-label">Job Title</label>
                      <Input
                        placeholder="Job Title"
                        type="text"
                        fullWidth
                        onChange={e =>
                          this.setState({ jobtitle: e.target.value })
                        }
                        value={this.state.jobtitle}
                      />
                    </div>
                  </div>
                  <div className="col-xs-12 basicInfo p-0 mt-2">
                    <div className="col-sm-12 col-lg-6 input-wrapper">
                      <label className="addEmployee-label">UserName</label>
                      <Input
                        placeholder="User Name"
                        fullWidth
                        onChange={e =>
                          this.setState({
                            username: e.target.value.toLowerCase()
                          })
                        }
                        value={this.state.username}
                      />
                    </div>
                    <div className="col-sm-12 col-lg-6  input-wrapper">
                      <label className="addEmployee-label">Work Email</label>
                      <Input
                        type="email"
                        pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2, 3}$"
                        placeholder="Email"
                        fullWidth
                        onChange={e =>
                          this.setState({ workemail: e.target.value })
                        }
                        value={this.state.workemail}
                      />
                    </div>
                  </div>
                  <div className="col-xs-12 basicInfo p-0 mt-2">
                    <div className="col-sm-12 col-lg-12 input-wrapper">
                      <label className="addEmployee-label">
                        Personal Email
                      </label>
                      <Input
                        placeholder="Personal Email"
                        fullWidth
                        onChange={e =>
                          this.setState({
                            personalEmail: e.target.value.toLowerCase()
                          })
                        }
                        value={this.state.personalEmail}
                      />
                    </div>
                  </div>
                  {/* <div className="col-xs-12 basicInfo mt-2 "> */}
                  <div className="col-xs-12 basicInfo mt-4 d-flex align-items-center">
                    <Checkbox
                      id="notifyNewEmpHrms"
                      name="notifyNewEmpHrms"
                      type="checkbox"
                      checked={this.state.notifyNewEmpHrms}
                      onClick={this.handleCheckboxes}
                    />
                    <label
                      htmlFor="notifyNewEmpHrms"
                      className="addEmployee-label col-sm-7 px-0"
                    >
                      Send Welcome Employee Email{" "}
                    </label>
                    <small className="py-1 editLink" onClick={this.editTemplate}>(Edit)</small>
                    {/* <Input
                      id="notifyNewEmpHrms"
                      name="notifyNewEmpHrms"
                      type="checkbox"
                      checked={this.state.notifyNewEmpHrms}
                      onChange={this.handleCheckboxes}
                      value={this.state.notifyNewEmpHrms}
                    /> */}
                  </div>
                  <div className="col-xs-12 basicInfo mt-2 d-flex align-items-center">
                    <Checkbox
                      id="notifyNewEmpPolicies"
                      name="notifyNewEmpPolicies"
                      checked={this.state.notifyNewEmpPolicies}
                      onClick={this.handleCheckboxes}
                    />
                    <label
                      htmlFor="notifyNewEmpPolicies"
                      className="addEmployee-label col-sm-7 px-0"
                    >
                      Notify about Important HR guidelines and Company Policies{" "}
                    </label>
                    <small className="py-1 editLink" onClick={this.editTemplate}>(Edit)</small>
                  </div>
                  {/* </div> */}
                  {/* <div className="col-xs-12 basicInfo mt-2"> */}
                  <div className="col-xs-12 basicInfo mt-2 d-flex align-items-center">
                    <Checkbox
                      id="notifyNewEmpImportantInformation"
                      name="notifyNewEmpImportantInformation"
                      checked={this.state.notifyNewEmpImportantInformation}
                      onClick={this.handleCheckboxes}
                    />
                    <label
                      htmlFor="notifyNewEmpImportantInformation"
                      className="addEmployee-label col-sm-7 px-0"
                    >
                      Notify about New Employee Important Information{" "}
                    </label>
                    <small className="py-1 editLink" onClick={this.editTemplate}>(Edit)</small>
                  </div>
                  <div className="col-xs-12 basicInfo mt-2 d-flex align-items-center">
                    <Checkbox
                      id="notifyNewEmpGeneralIntro"
                      name="notifyNewEmpGeneralIntro"
                      checked={this.state.notifyNewEmpGeneralIntro}
                      onClick={this.handleCheckboxes}
                    />
                    <label
                      htmlFor="notifyNewEmpGeneralIntro"
                      className="addEmployee-label col-sm-7 px-0"
                    >
                      Notify about New Employee General Introduction{" "}
                    </label>
                    <small className="py-1 editLink" onClick={this.editTemplate}>(Edit)</small>
                  </div>
                  {/* </div>/ */}
                  <div className="col-xs-12 basicInfo p-4">
                    {!this.props.frontend.show_loading ? (
                      <Button
                        color="primary"
                        className="pull-right"
                        onClick={() =>
                          this.props.callAddNewEmployee(this.state)
                        }
                        style={{ minWidth: "67px" }}
                      >
                        Next
                      </Button>
                    ) : (
                      <Button
                        color="primary"
                        className="pull-right"
                        style={{ minWidth: "67px" }}
                      >
                        <Spinner color="light" size="sm" />
                      </Button>
                    )}
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    frontend: state.frontend.toJS(),
    loggedUser: state.logged_user.userLogin,
    manageUsers: state.manageUsers.toJS(),
    usersList: state.usersList.toJS()
  };
}

const mapDispatchToProps = dispatch => {
  return {
    onAddNewEmployee: newEmployeeDetails => {
      return dispatch(actionsManageUsers.addNewEmployee(newEmployeeDetails));
    },
    onUsersList: () => {
      return dispatch(actionsUsersList.get_users_list());
    }
  };
};

const AddNewEmployee = compose(
  connect(
    mapStateToProps,
    mapDispatchToProps
  ),
  isMobile,
  withRouter
)(FormAddNewEmployee);

export default AddNewEmployee;

AddNewEmployee.propTypes = {
  onIsAlreadyLogin: PropTypes.func.isRequired,
  usersList: PropTypes.object.isRequired,
  onAddNewEmployee: PropTypes.func.isRequired,
  router: PropTypes.object.isRequired
};
