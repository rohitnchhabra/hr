import React, {Component} from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {withRouter} from 'react-router';
import _ from 'lodash';
import * as actions from '../../../redux/actions';
import Widget from '../../../components/Widget';
import $ from 'jquery';
import { Row, Col } from 'reactstrap';
import 'flot/jquery.flot';
import 'flot/jquery.flot.stack'
var moment = require('moment');

class PageEmployeePerformance extends Component {
  constructor (props) {
    super(props);
    this.state = {
      search:              '',
      start_year:          '',
      end_year:            '',
      pendingData:         '',
      year:                '',
      month:               '',
      userId:              '',
      employeePerformance: ''
    };
  }
  componentDidUpdate() {
    this.initActiveChart();
    this.initEventListeners();
    this.initTotalChart();   
    this.initTotalEventListeners(); 
  }
  
  componentDidMount(){
    window.addEventListener('resize', this.initActiveChart);
    window.addEventListener('resize', this.initTotalChart);
  }
  
  componentWillUnmount() {
    window.removeEventListener('resize', this.initActiveChart)
    window.removeEventListener('resize', this.initTotalChart);

  }

  componentWillMount () {
    this.setState({
      year:this.props.currentYear,
      month:this.props.currentMonth,
      employeePerformance: this.props.employeePerformance
    });
  }

  componentWillReceiveProps (props) {
    this.setState({
      employeePerformance: props.employeePerformance
    });
  }

  handleMonth=(data)=>{
    this.setState({month:data});
    this.getByData({month:data, change: 'month' });
  }

  handleYear=(data)=>{
    this.setState({ year: data});
    this.getByData({ year: data, change: 'year' });
  }

  getByData=(data)=>{
    const userId = localStorage.getItem('userid');
    let year = this.state.year !== '' ? this.state.year : this.props.currentYear;
    let month = this.state.month !== '' ? this.state.month : this.props.currentMonth;
    if(data.change === 'month'){
      month=data.month;
    }else if (data.change === 'year') {
      year=data.year;
    }
    this.props.requestEmployeePerformance({
      'month': month,
      'year':  year
    });
  }

  getActiveChartData=()=>{ // eslint-disable-line
    let EmpPerformance = this.state.employeePerformance;
    const activeHours = [];
    if (EmpPerformance !== undefined && EmpPerformance.isSuccess) {
      EmpPerformance.data.map((dat)=>activeHours.push([Number(dat.day.split(" ")[0]),Number(dat.top_active_hrs.hours),{'name':dat.top_active_hrs.username, 'month':dat.day.split(" ")[1]}]))
    }
    return [activeHours];
  }
  
  getTotalChartData=()=>{ // eslint-disable-line
    let EmpPerformance = this.state.employeePerformance;
    const totalHours = [];
    if (EmpPerformance !== undefined && EmpPerformance.isSuccess) {
      EmpPerformance.data.map((dat)=>totalHours.push([Number(dat.day.split(" ")[0]),Number(dat.top_total_hrs.hours),{'name':dat.top_total_hrs.username, 'month':dat.day.split(" ")[1]}]))
    }
    return [totalHours];
  }

  getDates=()=>{
    let EmpPerformance = this.state.employeePerformance;
    let dates = []
    if(EmpPerformance !== undefined && EmpPerformance.isSuccess){
      EmpPerformance.data.map((dat)=>dates.push(dat.day))
    }else{
      dates.push("Month")
    }
    return dates
  }

  onDrawHook=()=>{
    this.$chartLegend
      .find('.legendColorBox > div')
      .css({
        border: 0,
        borderRadius: 0,
        paddingTop: 5
      })
      .children('div')
      .css({
        borderWidth: 1,
        borderRadius: 0,
        width: '100%',
      });

    this.$chartLegend.find('tbody td').css({
      paddingLeft: 10,
      paddingRight: 10,
      textAlign: 'center'
    });

    let labels = this.$chartLegend.find('.legendLabel').detach();
    this.$chartLegend.find('tbody').prepend('<tr></tr>');
    this.$chartLegend.find('tbody tr:eq(0)').append(labels);
  }

  onTotalDrawHook=()=>{
    this.$totalChartLegend
      .find('.legendColorBox > div')
      .css({
        border: 0,
        borderRadius: 0,
        paddingTop: 5
      })
      .children('div')
      .css({
        borderWidth: 1,
        borderRadius: 0,
        width: '100%',
      });

    this.$totalChartLegend.find('tbody td').css({
      paddingLeft: 10,
      paddingRight: 10,
      textAlign: 'center'
    });

    let labels = this.$totalChartLegend.find('.legendLabel').detach();
    this.$totalChartLegend.find('tbody').prepend('<tr></tr>');
    this.$totalChartLegend.find('tbody tr:eq(0)').append(labels);
  }

  initActiveChart=()=>{
    const data = this.getActiveChartData();
    const ticks =  this.getDates();
    // check the screen size and either show tick for every 4th tick on large screens, or
    // every 8th tick on mobiles
    const tickInterval = window.screen.width < 500 ? 10 : 3;
    let counter = 0;

    if (this.$chartContainer.length > 0) {
      return window.jQuery.plot(this.$chartContainer, [{
        label: 'Active Hours',
        data: data[0],
        lines: {
          show: true,
          lineWidth: 1.5,
          dashLength: [5, 2],
        },
        points: {
          fillColor: '#3abf94',
        },
        shadowSize: 0,
      }], {
        xaxis: {
          tickColor: '#f8f9fa',
          tickSize: tickInterval,
          tickFormatter: (i) => ticks[ticks.length == 1 ? i : i-1],
          font: {
            lineHeight: 11,
            weight: 400,
          },
        },
        yaxis: {
          tickColor: '#f8f9fa',
          max: 14,
          font: {
            lineHeight: 11,
            weight: 400,
            labelFontColor: 'red'
          },
        },
        points: {
          show: true,
          fill: true,
          lineWidth: 1,
          radius: 2,
        },
        grid: {
          backgroundColor: { colors: ['#ffffff', '#ffffff'] },
          borderWidth: 1,
          borderColor: '#ffffff',
          margin: 0,
          minBorderMargin: 0,
          labelMargin: 20,
          hoverable: true,
          clickable: true,
          mouseActiveRadius: 6,
        },
        legend: {
          noColumns: 1,
          container: this.$chartLegend,
        },
        colors: ['#3abf94'],
        hooks: {
          draw: [this.onDrawHook],
        },
      });
    }
  }

  initTotalChart=()=>{
    const data = this.getTotalChartData();
    const ticks =  this.getDates();
    // check the screen size and either show tick for every 4th tick on large screens, or
    // every 8th tick on mobiles
    const tickInterval = window.screen.width < 500 ? 10 : 3;
    let counter = 0;

    if (this.$totalChartContainer.length > 0) {
      return window.jQuery.plot(this.$totalChartContainer, [{
        label: 'Total Hours',
        data: data[0],
        lines: {
          show: true,
          lineWidth: 1.5,
          dashLength: [5, 2],
        },
        points: {
          fillColor: '#ffc247',
        },
        shadowSize: 0,
      }], {
        xaxis: {
          tickColor: '#f8f9fa',
          tickSize: tickInterval,
          tickFormatter: (i) => ticks[ticks.length == 1 ? i : i-1],
          font: {
            lineHeight: 11,
            weight: 400,
          },
        },
        yaxis: {
          tickColor: '#f8f9fa',
          max: 14,
          font: {
            lineHeight: 11,
            weight: 400,
            labelFontColor: 'red'
          },
        },
        points: {
          show: true,
          fill: true,
          lineWidth: 1,
          radius: 2,
        },
        grid: {
          backgroundColor: { colors: ['#ffffff', '#ffffff'] },
          borderWidth: 1,
          borderColor: '#ffffff',
          margin: 0,
          minBorderMargin: 0,
          labelMargin: 20,
          hoverable: true,
          clickable: true,
          mouseActiveRadius: 6,
        },
        legend: {
          noColumns: 1,
          container: this.$totalChartLegend,
        },
        colors: ['#ffc247'],
        hooks: {
          draw: [this.onTotalDrawHook],
        },
      });
    }
  }

  initEventListeners=()=>{
    const self = this;

    window.jQuery("#chartContai").on('plothover', (event, pos, item) => {
      if (item) {
        const x = item.datapoint[0].toFixed(2);
        const y = item.datapoint[1].toFixed(2);

        window.jQuery("#chartTool").html(`<i><b>${item.series.data[item.dataIndex][2].name}</b></i> on <b>${x.split(".")[0]} ${item.series.data[item.dataIndex][2].month}</b> for <b>${y} hrs</b>`)
          .css({
            top: (item.pageY + 5) - window.scrollY,
            left: (item.pageX + 5) - window.scrollX,
          })
          .fadeIn(200);
      } else {
        window.jQuery("#chartTool").hide();
      }
    });
  }

  initTotalEventListeners=()=>{
    const self = this;

    window.jQuery("#totalChartContai").on('plothover', (event, pos, item) => {
      if (item) {
        const x = item.datapoint[0].toFixed(2);
        const y = item.datapoint[1].toFixed(2);

        window.jQuery("#totalChartTool").html(`<i><b>${item.series.data[item.dataIndex][2].name}</b></i> on <b>${x.split(".")[0]} ${item.series.data[item.dataIndex][2].month}</b> for <b>${y} hrs</b>`)
          .css({
            top: (item.pageY + 5) - window.scrollY,
            left: (item.pageX + 5) - window.scrollX,
          })
          .fadeIn(200);
      } else {
        window.jQuery("#totalChartTool").hide();
      }
    });
  }

  render () {
    let monthOptions = [];
    let yearOptions = [];
    let monthOption = _.map(this.props.months, (monthData, i) => {
      monthOptions.push(<option key={i} value={monthData}>{monthData}</option>);
    });
    let yearOption = _.map(this.props.year, (data, i) => {
      return (
      yearOptions.push(<option key={i} value={data}>{data}</option>)
      );
    });
    return (
      <div>
        <Widget
        bodyClass="mt"
        className="mb-xlg well p-a"
        title={
          <Row>
            <Col xs={12} sm={5}>
              <h5>
                Employee <span className="fw-semi-bold">Active Hours</span>
              </h5>
            </Col>
            <Col xs={12} sm={7}>
              <div className="d-flex justify-content-end">
                <div ref={(r) => { this.$chartLegend = $(r); }} />
              </div>
            </Col>
          </Row>
        }
      >
        <div ref={(r) => { this.$chartContainer = $(r); }} id="chartContai" style={{ width: '100%', height: '250px' }} />
        <div className="chart-tooltip" ref={(r) => { this.$chartTooltip = $(r); }} id="chartTool" />
      </Widget>
      <Widget
        bodyClass="mt"
        className="mb-xlg well p-a"
        title={
          <Row>
            <Col xs={12} sm={5}>
              <h5>
                Employee <span className="fw-semi-bold">Total Hours</span>
              </h5>
            </Col>
            <Col xs={12} sm={7}>
              <div className="d-flex justify-content-end">
                <div ref={(r) => { this.$totalChartLegend = $(r); }} />
              </div>
            </Col>
          </Row>
        }
      >
        <div ref={(r) => { this.$totalChartContainer = $(r); }} id="totalChartContai" style={{ width: '100%', height: '250px' }} />
        <div className="chart-tooltip" ref={(r) => { this.$chartTooltip = $(r); }} id="totalChartTool" />
      </Widget>
        <div className="container p-t ">
          <div className="row">
            <div className="form-group col-xs-6 profile-input p-a">
              <label htmlFor="sel1">Select Month:</label>
              <select className="form-control" id="sel1" defaultValue={this.props.currentMonth}
                onChange={(evt) => { this.handleMonth(evt.target.value); }}>
                {monthOptions}
              </select>
            </div>
            <div className="form-group col-xs-6 profile-input p-a">
              <label htmlFor="sel1">Select Year:</label>
              <select className="form-control" id="sel12" defaultValue={this.props.currentYear}
                onChange={(evt) => { this.handleYear(evt.target.value); }}>
                {yearOptions}
              </select>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

function mapStateToProps (state) {
  return {
    frontend:       state.frontend.toJS(),
    loggedUser:     state.logged_user.userLogin,
    usersList:      state.usersList.toJS(),
    teamStats:      state.teamStats,
    empLifeCycle:   state.teamStats.empLifeCycle,
    empHours:       state.teamStats.empHours,
    monthlyHours:   state.teamStats.monthlyHours,
    empPerformance: state.teamStats.employeePerformance
  };
}
const mapDispatchToProps = (dispatch) => {
  return bindActionCreators(actions, dispatch);
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(PageEmployeePerformance));
