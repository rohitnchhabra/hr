import React,{useState,useEffect} from 'react';
import PropTypes from "prop-types";
import { ButtonPrimary } from "../../../../components/generic/buttons/Button";
import Spinner from "../../../../components/generic/Spinner/GenericSpinner";
import Checkbox from "../../../../components/generic/Checkbox/Checkbox";
import { compose } from "redux";
import isMobile from "../../../../components/hoc/WindowResize";
import GenericDatePicker from "../../../../components/generic/GenericDatePicker/GenericDatePicker";
import {Label} from 'reactstrap';
import moment from 'moment';

function ImportantDates(props) {
    const {updateProfileLoader}=props
    const [state, setState] = useState({})
    useEffect(() => {
        setState({
          ...props.user_profile_detail,
          user_id: props.user_profile_detail && props.user_profile_detail.user_Id,
          send_slack_msg: false,
        });
      }, [props.user_profile_detail]);

    const handleUpdateClick = () => {
        props.callUpdateUserProfileDetails({
          ...state, 
          termination_date: state.termination_date?moment(state.termination_date).format("YYYY-MM-DD"):null,
          training_completion_date:state.training_completion_date? moment(state.training_completion_date).format("YYYY-MM-DD"):null,
          dateofjoining:state.dateofjoining?moment(state.dateofjoining).format("YYYY-MM-DD"):null
    
        },["dateofjoining","termination_date","training_completion_date","dob"]);
      };
    
      const handleCheckboxes = name => {
        setState({ ...state,[name]: !this.state[name] });
      };
    
      const onSendNotification = (e) => {
        setState({ ...state, send_slack_msg: !state.send_slack_msg });
      };
    
      const editTemplate=()=>{
        props.history.push("/app/mail_templates")
      }
    return (
        <div>
        <div className="row no-gutter">
          <div className="col-sm-12 col-lg-6 profile-input">
            <div className="form-group">
              <Label for="Date Of Birth">Date Of Birth ( eg. 27/1/1988 )</Label>
              <GenericDatePicker
                className="ml-0"
                handleDateChange={dob => setState({...state, dob })}
                type={props.isMobile < 768 ? "absolute" : "inline"}
                date={state.dob}
                dob={true}
              />
            </div>
          </div>
          <div className="col-sm-12 col-lg-6 profile-input">
            <div className="form-group">
              <Label for="Date Of Joining">
                Date Of Joining ( eg. 2016-12-30 )
              </Label>
              <GenericDatePicker
                className="ml-0"
                handleDateChange={dateofjoining =>
                  setState({ ...state,dateofjoining})
                }
                type={props.isMobile < 768 ? "absolute" : "inline"}
                date={state.dateofjoining}
              />
            </div>
          </div>
        </div>
        <div className="row no-gutter">
          <div className="col-sm-12 col-lg-6 profile-input">
            <div className="form-group">
              <Label for="Training Completion Date">
                Training Completion Date
              </Label>
              <GenericDatePicker
                className="ml-0"
                handleDateChange={date =>
                  setState(
                    {...state,
                      training_completion_date: date
                    },
                    () => {
                      if (!state.showConfirmation) {
                        setState({ ...state,showConfirmation: true });
                      }
                    }
                  )
                }
                type={props.isMobile < 768 ? "absolute" : "inline"}
                date={
                  state.training_completion_date && state.training_completion_date !== "0000-00-00"
                    ? state.training_completion_date
                    : null
                }
              />
            </div>
          </div>
          <div className="col-sm-12 col-lg-6 profile-input">
            <div className="form-group">
              <Label for="Date Of Termination">
                Date Of Termination ( eg. 2016-12-30 )
              </Label>
              <GenericDatePicker
                className="ml-0"
                handleDateChange={date =>
                  setState(
                    {...state,
                      termination_date: date
                    },
                    () => {
                      if (!state.showTermination) {
                        setState({ ...state,showTermination: true });
                      }
                    }
                  )
                }
                type={props.isMobile < 768 ? "absolute" : "inline"}
                date={
                  state.termination_date && state.termination_date !== "0000-00-00"
                    ? state.termination_date
                    : null
                }
              />
            </div>
          </div>
        </div>
        <div className="mt-2 mb-2">
          {state.showConfirmation && (
            <>
              <div className="form-group px-2 d-flex  align-items-center">
                <Checkbox
                  id="notifyEmpConfirmation"
                  name="notifyEmpConfirmation"
                  checked={state.notifyEmpConfirmation}
                  onClick={handleCheckboxes}
                />
                <Label for="notifyEmpConfirmation" className="mb-0 col-sm-7 px-0">
                  Notify Employee about Confirmation
                </Label>
                <small className="py-1 editLink" onClick={editTemplate}>(Edit)</small>
              </div>
              <div className="form-group  px-2 d-flex  align-items-center">
                <Checkbox
                  id="notifyEmpConfirmationPolicies"
                  name="notifyEmpConfirmationPolicies"
                  checked={state.notifyEmpConfirmationPolicies}
                  onClick={handleCheckboxes}
                />
                <Label
                  for="notifyEmpConfirmationPolicies"
                  className="mb-0 col-sm-7 px-0"
                >
                  Notify Employee about Confirmation Policies
                </Label>
                <small className="py-1 editLink" onClick={editTemplate}>(Edit)</small>
              </div>
            </>
          )}
          {state.showTermination && (
            <>
              <div className="form-group px-2 d-flex  align-items-center">
                <Checkbox
                  id="notifyEmpTermination"
                  name="notifyEmpTermination"
                  checked={state.notifyEmpTermination}
                  onClick={handleCheckboxes}
                />
                <Label for="notifyEmpTermination" className="mb-0 col-sm-7 px-0">
                  Notify Employee about Termination
                </Label>
                <small className="py-1 editLink" onClick={editTemplate}>(Edit)</small>
              </div>
              <div className="form-group px-2 d-flex  align-items-center">
                <Checkbox
                  id="notifyEmpTerminationPolicies"
                  name="notifyEmpTerminationPolicies"
                  checked={state.notifyEmpTerminationPolicies}
                  onClick={handleCheckboxes}
                />
                <Label for="notifyEmpTerminationPolicies" className="mb-0 col-sm-7 px-0">
                  Notify Employee about Termination Policies
                </Label>
                <small className="py-1 editLink" onClick={editTemplate}>(Edit)</small>
              </div>
            </>
          )}
        </div>
        <div className="d-flex px-1">
          <ButtonPrimary
            id="save-btn"
            className="pull-right"
            onClick={handleUpdateClick}
            disabled={updateProfileLoader}
          >
            {updateProfileLoader ? <Spinner color="white" /> : "Save"}
          </ButtonPrimary>
          <div className="col px-2 d-flex  align-items-center">
            <i
              className="fa fa-slack align-self-center hash-icon"
              aria-hidden="true"
            />
            <Label for="send_slack_msg" className="mb-0 px-3">
              {" "}
              Don't send notification for current update
            </Label>
            <Checkbox
              id="send_slack_msg"
              name="send_slack_msg"
              checked={state.send_slack_msg}
              onClick={onSendNotification}
            />
          </div>
        </div>
      </div>
    )
}

export default compose(isMobile)(ImportantDates);

ImportantDates.propTypes = {
  user_profile_detail: PropTypes.object.isRequired,
  callUpdateUserProfileDetails: PropTypes.func.isRequired,
  updateProfileLoader: PropTypes.bool
};

