import React from "react";
import { withKnobs,boolean } from "@storybook/addon-knobs";
import { action } from "@storybook/addon-actions";
import ImportantDates from "./ImportantDates";

export default {
  title: "Important Dates",
  decorators: [withKnobs],
};
const user_profile_detail = {
  send_slack_msg:boolean("send_slack_msg",false)
};

export const importantDates = () => (
  <ImportantDates
    callUpdateUserProfileDetails={action("clicked")}
    user_profile_detail={user_profile_detail}
    updateProfileLoader={boolean("loader",false)}
  />
);
