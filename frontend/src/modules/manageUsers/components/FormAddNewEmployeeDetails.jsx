import React from "react";
import Dialog from "material-ui/Dialog";
import TextField from "material-ui/TextField";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { notify } from "../../../services/notify";
import ToggleButton from "react-toggle-button";
import { CONFIG } from "../../../config/index";
import { isNotUserValid } from "../../../services/generic";
import Header from "../../../components/generic/Header";
import { withRouter } from "react-router";
import { Button, Spinner, Input, Alert } from "reactstrap";
import SignaturePad from "signature_pad";
import * as actions from "../../../redux/actions";
import * as actionsUsersList from "../../../redux/generic/actions/usersList";
import * as actionsManageUsers from "../../../redux/manageUsers/actions/manageUsers";
import CircularProgress from "material-ui/CircularProgress";
import Textarea from "../../../components/generic/input/TextArea";

let signaturePad;
class FormAddNewEmployeeDetails extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      user_id: props.user_id,
      permanent_address: "",
      emergency_ph1: "",
      emergency_ph2: "",
      blood_group: "",
      medical_condition: "",
      holding_comments: "",
      signature: "",
      messageError: ""
    };
  }

  handleCanvas = () => {
    signaturePad = new SignaturePad(document.getElementById("signature-pad"), {
      backgroundColor: "rgba(255, 255, 255, 0)",
      penColor: "rgb(0, 0, 0)",
      onEnd: () => {
        localStorage.setItem("signature", signaturePad.toDataURL());
        this.setState({
          signature: localStorage.signature
        });
      }
    });
  };
  componentWillMount() {
    this.props.onUsersList();
  }
  handleCancelButton() {
    signaturePad.clear();
  }
  callAddNewEmployeeDetails = new_profile_details => {
    this.props.onAddNewUserDetails(new_profile_details).then(
      data => {
        this.props.history.push({
          pathname: `manage_users/${this.props.user_id}`,
          state: {message:data.message,password:this.props.password_generated}
        });
      },
      error => {
        this.setState(
          {
            messageError: error
          },
          () => {
            if (this.state.messageError && window.screen.availWidth < 576) {
              window.scrollTo(0, 0);
            }
          }
        );
      }
    );
  };
  render() {
    return (
      <div>
        <div
          id="content"
          className="app-content box-shadow-z0 personalInfo"
          role="main"
        >
          <Header
            pageTitle={"Add New Employee Details"}
            showLoading={this.props.frontend.show_loading}
          />
          <div
            className="app-body"
            id="view"
          >
            <div className='row no-gutters py-3'>
              <div className="col-sm-12">
                {this.props.message1 && (
                  <Alert
                    className="alert-transparent rem-fade rh-section rh-alert"
                    color="success"
                  >
                    <span className="alert-bold">INFO</span>:{" "}
                    {this.props.message1}
                  </Alert>
                )}
              </div>
              <div className="col-sm-12">
                {this.state.messageError && (
                  <Alert
                    className="alert-transparent rem-fade rh-section rh-alert"
                    color="danger"
                  >
                    <span className="alert-bold">ALERT</span>:{" "}
                    {this.state.messageError}
                  </Alert>
                )}
              </div>
              <div className="col-sm-12 ">
                <div className="col-md-12 pb-3 input-wrapper">
                  <label className="addEmployee-label">
                    {" "}
                    Permanent Address
                  </label>
                  <Textarea
                    placeHolder={"Address"}
                    onchange={e =>
                      this.setState({ permanent_address: e.target.value })
                    }
                    value={this.state.permanent_address}
                  />
                </div>
              </div>
              <div className="col-sm-12 ">
                <div className="col-md-6 pb-3 col-sm-12 input-wrapper">
                  <label for="phone" className="addEmployee-label">
                    Phone No
                  </label>
                  <Input
                    placeholder="Phone No"
                    type="number"
                    id="phone"
                    name="phone"
                    required
                    fullWidth
                    onChange={e =>
                      this.setState({ emergency_ph1: e.target.value })
                    }
                    value={this.state.emergency_ph1}
                  />
                </div>
                <div className="col-md-6 pb-3 col-sm-12 input-wrapper">
                  <label className="addEmployee-label">
                    {" "}
                    Emergency Contact No
                  </label>
                  <Input
                    placeholder="Contact No"
                    type="number"
                    fullWidth
                    onChange={e =>
                      this.setState({ emergency_ph2: e.target.value })
                    }
                    value={this.state.emergency_ph2}
                    onKeyPress={this.handlePhoneValidation}
                  />
                </div>
              </div>
              <div className="col-sm-12 d-flex flex-wrap align-items-baseline">
                <div className="col-md-6 pb-3 col-sm-12 input-wrapper">
                  <label className="addEmployee-label">Blood Group</label>
                  <select
                    className="form-control"
                    ref="blood_group"
                    onChange={e =>
                      this.setState({ blood_group: e.target.value })
                    }
                    value={this.state.blood_group}
                  >
                    <option value="">--select blood group--</option>
                    <option value="0">B+ </option>
                    <option value="1">O+</option>
                    <option value="2">AB+</option>
                    <option value="3">A+</option>
                    <option value="4">B- </option>
                    <option value="5">O-</option>
                    <option value="6">AB-</option>
                    <option value="7">A-</option>
                  </select>
                </div>
                <div className="col-md-6 pb-3 col-sm-12 input-wrapper">
                  <label className="addEmployee-label">
                    Any Medical issues in past/present
                  </label>
                  <Input
                    placeholder="Medical Issues"
                    fullWidth
                    onChange={e =>
                      this.setState({ medical_condition: e.target.value })
                    }
                    value={this.state.medical_condition}
                  />
                </div>
              </div>
              <div className='col-sm-12'>
                <div className="col-md-12 personalInfoComment input-wrapper pb-3">
                  <label className="addEmployee-label">Comments</label>
                  <Textarea
                    placeHolder={"Comment"}
                    onchange={e =>
                      this.setState({
                        holding_comments: e.target.value
                      })
                    }
                    value={this.state.holding_comments}
                  />
                </div>
              </div>
              <div className="col-md-12 pb-3 input-wrapper">
                {/* <label>Employee Signature</label>
                <p>(Double-tap and hold then start drawing your signature)</p>
                <canvas
                  id="signature-pad"
                  onClick={this.handleCanvas}
                  className="signature-pad"
                  style={{
                    width: "100%",
                    height: 200,
                    border: "1px solid rgba(80,80,80,0.6)"
                  }}
                /> */}
                <div className="col-sm-12">
                  {/* <Button
                    outline
                    color="secondary"
                    onClick={this.handleCancelButton}
                    id="clear"
                  >
                    Clear
                  </Button> */}
                  {!this.props.frontend.show_loading ? (
                    <Button
                      color="primary"
                      className="pull-right"
                      onClick={() => this.callAddNewEmployeeDetails(this.state)}
                    >
                      Submit
                    </Button>
                  ) : (
                    <Button
                      color="primary"
                      className="pull-right"
                      style={{ minWidth: "67px" }}
                    >
                      <Spinner color="light" size="sm" />
                    </Button>
                  )}
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    frontend: state.frontend.toJS(),
    loggedUser: state.logged_user.userLogin,
    manageUsers: state.manageUsers.toJS()
  };
}

const mapDispatchToProps = dispatch => {
  return {
    onUsersList: () => {
      return dispatch(actionsUsersList.get_users_list());
    },
    onAddNewUserDetails: new_profile_details => {
      return dispatch(
        actionsManageUsers.addNewUserDetails(new_profile_details)
      );
    }
  };
};
const AddNewEmployeeDetails = withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(FormAddNewEmployeeDetails)
);

export default AddNewEmployeeDetails;

AddNewEmployeeDetails.propTypes = {
  onIsAlreadyLogin: PropTypes.func.isRequired,
  usersList: PropTypes.object.isRequired,
  onAddNewUserDetails: PropTypes.func.isRequired,
  router: PropTypes.object.isRequired
};
