import React, { useState, useEffect } from "react";
import PropTypes from "prop-types";
import {
  InputTextWithLabel,
} from "../../../../components/generic/InputText/InputText";
import TextAreaWithLabel from "../../../../components/generic/TextArea/TextArea";
import { Form } from "reactstrap";
import { ButtonPrimary } from "../../../../components/generic/buttons/Button";
import Spinner from "../../../../components/generic/Spinner/GenericSpinner";
import Checkbox from "../../../../components/generic/Checkbox/Checkbox";

const ContactInfo = (props) => {
  const [state, setState] = useState({});

  useEffect(() => {
    setState({
      ...props.user_profile_detail,
      user_id: props.user_profile_detail && props.user_profile_detail.user_Id,
      send_slack_msg: false,
    });
  }, [props.user_profile_detail]);
  const handleUpdateClick = (e) => {
    e.preventDefault();
    props.callUpdateUserProfileDetails(state, [
      "emergency_ph1",
      "emergency_ph2",
      "permanent_address",
      "current_address",
    ]);
  };
  const onSendNotification = (e) => {
    setState({ ...state, send_slack_msg: !state.send_slack_msg });
  };
  const { updateProfileLoader } = props;
  return (
    <div>
      <Form onSubmit={handleUpdateClick}>
        <div className="row no-gutter">
          <div className="col-lg-6 col-sm-12 profile-input">
            <div className="form-group">
              <TextAreaWithLabel
                id="current Address"
                label="Current Address"
                placeHolder="your current address..."
                onChange={(e) =>
                  setState({ ...state, current_address: e.target.value })
                }
                value={state.current_address}
              />
            </div>
          </div>
          <div className="col-lg-6 col-sm-12 profile-input">
            <div className="form-group">
              <TextAreaWithLabel
                id="Permanent Address"
                label="Permanent Address"
                placeHolder="your permanent address..."
                onChange={(e) =>
                  setState({ ...state, permanent_address: e.target.value })
                }
                value={state.permanent_address}
              />
            </div>
          </div>
        </div>
        <div className="row no-gutter">
          <div className="col-lg-6 col-sm-12 profile-input">
            <InputTextWithLabel
              id="Personal Contact No."
              required={true}
              onChange={(e) =>
                setState({
                  ...state,
                  emergency_ph1: e.target.value,
                })
              }
              label=" Personal Contact No."
              value={state.emergency_ph1}
            />
          </div>
          <div className="col-lg-6 col-sm-12 profile-input">
            <InputTextWithLabel
              id="emergency_ph2"
              label=" Emergency Contact No."
              onChange={(e) =>
                setState({
                  ...state,
                  emergency_ph2: e.target.value,
                })
              }
              value={state.emergency_ph2}
            />
          </div>
        </div>

        <div className="d-flex px-1">
          <ButtonPrimary type="submit" disabled={updateProfileLoader} id="save-btn">
            {updateProfileLoader ? <Spinner color="white" /> : "Save"}
          </ButtonPrimary>
          <div className="col px-2 d-flex  align-items-center">
            <i
              className="fa fa-slack align-self-center hash-icon"
              aria-hidden="true"
            />
            <span className="mb-0 px-3">
              {" "}
              Don't send notification for current update
            </span>
            <Checkbox
              id="send_slack_msg"
              name="send_slack_msg"
              checked={state.send_slack_msg}
              onClick={onSendNotification}
            />
          </div>
        </div>
      </Form>
    </div>
  );
};

export default ContactInfo;

ContactInfo.propTypes = {
  user_profile_detail: PropTypes.object.isRequired,
  callUpdateUserProfileDetails: PropTypes.func.isRequired,
  updateProfileLoader: PropTypes.bool,
};
