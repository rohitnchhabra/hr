import React from "react";
import { withKnobs,boolean } from "@storybook/addon-knobs";
import { action } from "@storybook/addon-actions";
import ContactInfo from "./ContactInfo";

export default {
  title: "Contact Info",
  decorators: [withKnobs],
};
const user_profile_detail = {
  send_slack_msg:boolean("send_slack_msg",false)

};

export const contactInformation = () => (
  <ContactInfo
    callUpdateUserProfileDetails={action("clicked")}
    user_profile_detail={user_profile_detail}
    updateProfileLoader={boolean("loader",false)}
  />
);
