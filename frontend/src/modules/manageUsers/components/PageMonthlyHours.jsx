import React, {Component} from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {withRouter} from 'react-router';
import {notify} from '../../../services/notify';
import map from 'lodash/map';
import * as actions from '../../../redux/actions';
import DialogInOut from './DialogInOut';
import Widget from '../../../components/Widget';
import $ from 'jquery';
import { Row, Col } from 'reactstrap';
import 'flot/jquery.flot';
import 'flot/jquery.flot.stack'
var moment = require('moment');

class PageMonthlyHours extends Component {
  constructor (props) {
    super(props);
    this.state = {
      search:       '',
      date:   '',
      end_year:     '',
      userId:       '',
      pendingData:  '',
      year:         '',
      month:        '',
      monthlyHours: '',
      open:         false
    };
  }
  componentWillMount () {
    this.setState({
      month:this.props.currentMonth,
      year:this.props.currentYear,
      monthlyHours: this.props.monthlyHours
    });
  }
  componentWillReceiveProps (props) {
    this.setState({
      monthlyHours: props.monthlyHours.data
    });
  }

  componentDidUpdate() {
    this.initChart();
    this.initEventListeners();
    this.initClickEventListners();
  }

  componentDidMount(){
    window.addEventListener('resize', this.initChart);
  }
  
  componentWillUnmount() {
    window.removeEventListener('resize', this.initChart);

  }


  handleMonth=(data)=>{
    this.setState({month:data});
    this.getByData({month:data, change: 'month' });
  }

  handleYear=(data)=>{
    this.setState({ year: data});
    this.getByData({ year: data, change: 'year' });
  }

  handleUserId=(data)=>{
    this.setState({ userId: data});
    this.getByData({ userId: data, change: 'user_id' });
  }
  handleClick=(date)=>{
    let dateData = date.toString().length > 1 ? date : '0'+date;
    this.setState({ open : true, date : dateData });
  }
  handleClose=()=>{
    this.setState({ open: false});
  }

  getByData=(data)=>{
    let userId = this.state.userId;
    let year = this.state.year !== '' ? this.state.year : this.props.currentYear;
    let month = this.state.month !== '' ? this.state.month : this.props.currentMonth;
    if(data.change === 'month'){
      month=data.month;
    }else if (data.change === 'year') {
      year=data.year;
    }else if (data.change === 'user_id') {
      userId=data.userId;
    }
    if (userId === '') {
      notify('Error!', 'Please Select user', 'error');
    }else{
      this.props.requestEmployeeMonthlyHours({
        'id':    userId,
        'month': month,
        'year':  year
      });
    }
  }

  getMainChartData=()=>{ // eslint-disable-line
    const EmpMonthltHours = this.state.monthlyHours;
    const activeHour=[];
    const totalHour=[];
    if (EmpMonthltHours[0] != undefined) {
      EmpMonthltHours[0].day_wise_detail.map((dat)=>activeHour.push([Number(dat.day.split(" ")[0]),Number(dat.active_hours.total_time),{'day':dat.day}]))
      EmpMonthltHours[0].day_wise_detail.map((dat)=>totalHour.push([Number(dat.day.split(" ")[0]),Number(dat.total_hours.total_time),{'day':dat.day}]))
    }
    return [activeHour, totalHour];
  }

  getDates=()=>{
    const EmpMonthltHours = this.state.monthlyHours;
    const dates = []
    if (EmpMonthltHours[0] !== null && EmpMonthltHours[0] !== undefined) {
      EmpMonthltHours[0].day_wise_detail.map((dat)=>dates.push(dat.day))
    }else{
      dates.push("Month")
    }
    return dates
     
  }

  onDrawHook=()=>{
    this.$chartMonthlyLegend
      .find('.legendColorBox > div')
      .css({
        border: 0,
        borderRadius: 0,
        paddingTop: 5
      })
      .children('div')
      .css({
        borderWidth: 1,
        borderRadius: 0,
        width: '100%',
      });

    this.$chartMonthlyLegend.find('tbody td').css({
      paddingLeft: 10,
      paddingRight: 10,
      textAlign: 'center'
    });

    let labels = this.$chartMonthlyLegend.find('.legendLabel').detach();
    this.$chartMonthlyLegend.find('tbody').prepend('<tr></tr>');
    this.$chartMonthlyLegend.find('tbody tr:eq(0)').append(labels);
  }

  initChart=()=>{
    const data = this.getMainChartData();
    const ticks =  this.getDates();
    // check the screen size and either show tick for every 4th tick on large screens, or
    // every 8th tick on mobiles
    const tickInterval = window.screen.width < 500 ? 10 : 3;
    let counter = 0;

    if (this.$chartMonthlyContainer.length > 0) {
      return window.jQuery.plot(this.$chartMonthlyContainer, [{
        label: 'Active Hours',
        data: data[0],
        lines: {
          show: true,
          lineWidth: 1.5,
          dashLength: [5, 2],
        },
        points: {
          fillColor: '#3abf94',
        },
        shadowSize: 0,
      }, {
        label: 'Total Hours',
        data: data[1],
        lines: {
          show: true,
          lineWidth: 1.5,
        },
        points: {
          fillColor: '#f55d5d',

        },
        shadowSize: 0,
      }], {
        xaxis: {
          tickColor: '#f8f9fa',
          tickSize: tickInterval,
          tickFormatter: (i) => ticks[ticks.length == 1 ? i : i-1],
          font: {
            lineHeight: 11,
            weight: 400,
          },
        },
        yaxis: {
          tickColor: '#f8f9fa',
          max: 14,
          font: {
            lineHeight: 11,
            weight: 400,
          },
        },
        points: {
          show: true,
          fill: true,
          lineWidth: 1,
          radius: 2,
        },
        grid: {
          backgroundColor: { colors: ['#ffffff', '#ffffff'] },
          borderWidth: 1,
          borderColor: '#ffffff',
          margin: 0,
          minBorderMargin: 0,
          labelMargin: 20,
          hoverable: true,
          clickable: true,
          mouseActiveRadius: 6,
        },
        legend: {
          noColumns: 2,
          container: this.$chartLegend,
        },
        colors: ['#3abf94', '#ffc247'],
        hooks: {
          draw: [this.onDrawHook],
        },
      });
    }
  }
  initEventListeners=()=>{
    const self = this;
    window.jQuery("#monthlyChartContainer").on('plothover', (event, pos, item) => {
      if (item) {
        const x = item.datapoint[0].toFixed(2);
        const y = item.datapoint[1].toFixed(2);
        window.jQuery("#monthlyChartTool").html(`<b>${item.series.data[item.dataIndex][2].day}</b> ${item.series.label}: <b>${y}</b>`)
          .css({
            top: (item.pageY + 5) - window.scrollY,
            left: (item.pageX + 5) - window.scrollX,
          })
          .fadeIn(200);
      } else {
        window.jQuery("#monthlyChartTool").hide();
      }
    });
  }

  initClickEventListners=()=>{
    window.jQuery("#monthlyChartContainer").on('plotclick', (event, pos, item) => {
      if (item) {
        this.handleClick(item.datapoint[0])
      }
    });
  }

  render () {
    let EmpMonthltHours = this.state.monthlyHours;
    var noOfDays = [];
    var activeHour = [];
    var totalHour = [];
    let monthOptions = [];
    let yearOptions = [];
    let userIdOptions = [];
    let monthOption = map(this.props.months, (monthData, i) => {
      monthOptions.push(<option key={i} value={monthData}>{monthData}</option>);
    });
    let yearOption = map(this.props.year, (data, i) => {
      return (
      yearOptions.push(<option key={i} value={data}>{data}</option>)
      );
    });
    let userIdOption = map(this.props.employeeList.data, (userList, k) => {
      userIdOptions.push(<option key={k} value={userList.user_Id}>  {userList.name}</option>);
    });
    return (
      <div>
        <DialogInOut open={this.state.open} handleClose={this.handleClose} date={this.state.date} userId={this.state.userId} selectedYear={this.state.year} selectedMonth={this.state.month} {...this.props} />
        <div className="team">
          <Widget
              bodyClass="mt"
              className="mb-xlg well p-a"
              style={{width:"100%"}}
              title={
                <Row>
                  <Col xs={12} sm={5}>
                    <h5>
                      Employee <span className="fw-semi-bold">Monthly Hours</span>
                    </h5>
                  </Col>
                  <Col xs={12} sm={7}>
                    <div className="d-flex justify-content-end">
                      <div ref={(r) => { this.$chartMonthlyLegend = $(r); }} />
                    </div>
                  </Col>
                </Row>
              }
          >
          <div ref={(r) => { this.$chartMonthlyContainer = $(r); }} id="monthlyChartContainer" style={{ width: '100%', height: '250px' }} />
          <div className="chart-tooltip" ref={(r) => { this.$chartMonthlyTooltip = $(r); }} id="monthlyChartTool" />
          </Widget>
        </div>
        <div className="container p-t">
          <div className="form-group col-sm-4 col-xs-12 p-a">
            <label htmlFor="sel1">Select User</label>
            <select className="form-control" id="user"
              onChange={(evt) => {  this.handleUserId(evt.target.value); }}>
              <option>Select User</option>
              {userIdOptions}
            </select>
          </div>
          <div className="form-group col-sm-4 col-xs-6 p-a">
            <label htmlFor="sel1">Select Month:</label>
            <select className="form-control" id="sel1" defaultValue={this.props.currentMonth}
              onChange={(evt) => { this.handleMonth(evt.target.value);  }}>
              {monthOptions}
            </select>
          </div>
          <div className="form-group col-sm-4  col-xs-6 p-a">
            <label htmlFor="sel1">Select Year:</label>
            <select className="form-control" id="sel12" defaultValue={this.props.currentYear}
              onChange={(evt) => { this.handleYear(evt.target.value); }}>
              {yearOptions}
            </select>
          </div>
          {/* <div className="form-group col-md-3">
            <button type="button" onClick={(evt) => this.getByData()} className="btn btn-primary form-group m-t-md">Get Details</button>
          </div> */}
        </div>
      </div>
    );
  }
}

function mapStateToProps (state) {
  return {
    frontend:     state.frontend.toJS(),
    loggedUser:   state.logged_user.userLogin,
    usersList:    state.usersList.toJS(),
    teamStats:    state.teamStats,
    empLifeCycle: state.teamStats.empLifeCycle,
    empHours:     state.teamStats.empHours,
    monthlyHours: state.teamStats.monthlyHours
  };
}
const mapDispatchToProps = (dispatch) => {
  return bindActionCreators(actions, dispatch);
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(PageMonthlyHours));
