import React from "react";
import { connect } from "react-redux";
import { withRouter } from "react-router";
import * as _ from "lodash";
import PropTypes from "prop-types";
import { notify, } from "../../../services/notify";
import Header from "../../../components/generic/Header";
import DisplayUserBankDetails from "../../../components/manageUser/DisplayUserBankDetails";
import UserPayslipsHistory from "../../../components/salary/managePayslips/UserPayslipsHistory";
import EmployeeLifeCycle from "../../../modules/manageUsers/components/EmployeeLifeCycle";
import FormAddDocuments from "../../../modules/addDocuments/components/FormAddDocuments";
import UpdatePassWord from "../components/UpdatePassword";
import * as actions from "../../../redux/actions";
import * as actionsUsersList from "../../../redux/generic/actions/usersList";
import * as actionsManageUsers from "../../../redux/manageUsers/actions/manageUsers";
import * as actionsManagePayslips from "../../../redux/salary/actions/managePayslips";
import * as actions_manageSalary from "../../../redux/salary/actions/manageSalary";
import moment from "moment";
import isEqual from "lodash/isEqual";
import BasicDetail from "../components/BasicDetail/BasicDetail";
import ContactInfo from "../components/ContactInfo/ContactInfo";
import ImportantDates from "../components/ImportantDates/ImportantDates";
import Comments from "../components/Comments";
import Heading from "../../../components/generic/Heading";
import UserPicHeadTitle from "../../../components/UserPicHeadTitle/UserPicHeadTitle";
import { Alert, Col, Button, TabContent, TabPane } from "reactstrap";
import classnames from "classnames";
import profilePlaceholder from "../../../static/profilePlaceholder.png";
import AlertFader from "../../../components/generic/AlertFader";

class ManageUsers extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      status_message: "",
      defaultUserDisplay: "",
      user_profile_detail: {},
      user_bank_detail: [],
      user_assign_machine: [],
      user_payslip_history: [],
      employee_life_cycle: {},
      openIframe: false,
      username: "",
      open: false,
      isTerminatingEmployee: false,
      activeSecondTab: "tab21",
      previousPageMessage: "",
      collapseId: null,
      stepid: [],
      firstClick: false,
      password_status_message: "",
      passErr: "",
      updateProfileErr: "",
      warning: ""
    };
  }
  componentDidMount() {
    this.props.onUsersList();
    this.props.onFetchTeams();
    if (this.props.location.state) {
      this.setState({
        previousPageMessage: this.props.location.state.message,
        password: this.props.location.state.password
      });
    }
    this.props.showHeading("My Profile")
  }

  componentWillReceiveProps(props) {
    let {
      managePayslips: { user_payslip_history },
      manageUsers: {
        username,
        user_profile_detail,
        user_bank_detail,
        user_assign_machine,
        stages,
        status_message
      }
    } = props;

    // let isNotValid = isNotUserValid(location.pathname, loggedUser);
    // console.log(isNotValid,'sa')
    // if (isNotValid.status) {
    //   this.props.history.push(isNotValid.redirectTo);
    // }
    if (
      !isEqual(
        props.manageUsers.user_profile_detail,
        this.props.manageUsers.user_profile_detail
      )
    ) {
      this.setState({
        username: username,
        user_profile_detail: user_profile_detail,
        user_bank_detail: user_bank_detail,
        user_assign_machine: user_assign_machine
      });
    }
    if (!isEqual(props.managePayslips, this.props.managePayslips)) {
      this.setState({
        user_payslip_history: user_payslip_history
      });
    }
    if (this.state.employee_life_cycle_stages !== stages) {
      this.setState({
        employee_life_cycle: stages.employee_life_cycle
      });
    }
    if (
      !isEqual(
        props.manageUsers.status_message,
        this.props.manageUsers.status_message
      )
    ) {
      if (props.manageUsers.status_message !== "") {
        this.setState(
          {
            status_message: status_message
          },
          () => {
            this.setState({
              status_message: ""
            });
          }
        );
      }
    }
    if (
      !isEqual(
        props.manageUsers.password_status_message,
        this.props.manageUsers.password_status_message
      )
    ) {
      this.setState(
        {
          password_status_message: props.manageUsers.password_status_message
        },
        () => {
          this.setState({
            password_status_message: ""
          });
        }
      );
    }
  }
  componentDidUpdate(prevProps, prevState) {
    if (this.state.defaultUserDisplay === "") {
      if (this.props.usersList.users.length > 0) {
        const userId = this.props.match.params.id;
        let firstUser;
        if (userId) {
          const findUser = this.props.usersList.users.filter(
            user => user.user_Id === userId
          );
          if (findUser.length) {
            firstUser = findUser[0];
          } else {
            firstUser = this.props.usersList.users[0];
          }
        } else {
          firstUser = this.props.usersList.users[0];
        }
        let defaultUserId = firstUser.user_Id;
        let defaultUserName = firstUser.username;
        this.onUserClick(defaultUserId, defaultUserName);
      }
    }

    if (
      prevState.firstClick !== this.state.firstClick &&
      this.state.firstClick
    ) {
      setTimeout(this.callHandleChangeSteps, 5000);
    }
  }
  onUserClick = (userid, username) => {
    let selectedUserName = "";
    let selectedUserImage = "";
    let selectedUserJobtitle = "";
    let selectedUserId = "";
    if (this.props.usersList.users.length > 0) {
      let userDetails = _.find(this.props.usersList.users, { user_Id: userid });
      if (typeof userDetails !== "undefined") {
        selectedUserName = userDetails.name;
        selectedUserImage = userDetails.slack_profile.image_192;
        selectedUserJobtitle = userDetails.jobtitle;
        selectedUserId = userDetails.user_Id;
      }
    }
    this.setState({
      defaultUserDisplay: userid,
      selected_user_name: selectedUserName,
      selected_user_image: selectedUserImage,
      selected_user_jobtitle: selectedUserJobtitle,
      selected_user_id: selectedUserId
    });
    this.props.onUserProfileDetails(userid, username);
    this.props.onGetUserDocument(userid);
    this.props.onUserManagePayslipsData(userid);
    this.props.onGetStages(userid);
  };
  callUpdateUserBankDetails = newBankDetails => {
    this.props.onUpdateUserBankDetails(newBankDetails).then(
      data => {},
      error => {
        notify(error);
      }
    );
  };
  callUpdateUserDeviceDetails = newDeviceDetails => {
    this.props.onUpdateUserDeviceDetails(newDeviceDetails).then(
      data => {},
      error => {
        notify(error);
      }
    );
  };
  callUpdateUserProfileDetails = (newProfileDetails,check_empty_value) => {
    this.setState({
      previousPageMessage: "",
      password: ""
    });
    if (
      newProfileDetails.training_completion_date !==
        this.state.user_profile_detail.training_completion_date &&
      newProfileDetails.termination_date !==
        this.state.user_profile_detail.termination_date
    ) {
      this.handleConfirmAndTerminate(newProfileDetails,check_empty_value);
    } else if (
      newProfileDetails.training_completion_date !==
      this.state.user_profile_detail.training_completion_date
    ) {
      this.handleConfirm(newProfileDetails,check_empty_value);
    } else if (
      newProfileDetails.termination_date !==
      this.state.user_profile_detail.termination_date
    ) {
      this.handleTerminate(newProfileDetails,check_empty_value);
    } else {
      this.props.onUpdateUserProfileDetails(newProfileDetails,"",check_empty_value).then(
        data => {},
        error => {
          this.setState(
            {
              open: true,
              updateProfileErr: error
            },
            () =>
              this.setState({
                updateProfileErr: ""
              })
          );
        }
      )
    }
  };

  changeEmployeeStatus = (userid, status) => {
    this.props
      .onChangeEmployeeStatus(userid, status)
      .then(msg => {
        notify("Success!", msg, "success");
        this.props.onUsersList().then(() => {
          if (this.props.usersList.users.length > 0) {
            let firstUser = this.props.usersList.users[0];
            let defaultUserId = firstUser.user_Id;
            this.onUserClick(defaultUserId);
          }
        });
      })
      .catch(err => {
        notify("Error!", err, "error");
      });
  };
  handleChangeSteps = (stepid, userid) => {
    if (!this.state.firstClick) {
      this.setState({ firstClick: true });
    }
    let { employee_life_cycle } = this.state;
    _.map(employee_life_cycle, (stage, key) => {
      _.map(stage.steps, (step, k) => {
        if (step.id === stepid) {
          step.status = !step.status;
        }
      });
    });
    this.setState({
      employee_life_cycle
    });

    if (this.state.stepid.includes(stepid)) {
      this.setState({
        stepid: this.state.stepid.filter(step => step !== stepid)
      });
    } else {
      this.setState({ stepid: [...this.state.stepid, stepid] });
    }
  };
  callHandleChangeSteps = () => {
    const userid = this.state.user_profile_detail.user_Id;
    this.props.onHandleChangeSteps(userid, this.state.stepid);
    this.setState({ firstClick: false, stepid: [] });
  };
  callAddUserSalary = new_salary_details => {
    new_salary_details.first_update = true;
    this.props
      .onAddNewSalary({
        ...new_salary_details,
        user_id: this.state.user_profile_detail.user_Id,
        do_force_confirmation: 1
      })
      .then(
        data => {
          this.setState({
            open: false
          });
        },
        error => {
          notify(error);
        }
      );
  };
  openTerminate = () => {
    this.setState({ open: true, isTerminatingEmployee: true });
  };

  handleTerminate = (newProfileDetails,check_empty_value) => {
    const { employee_life_cycle } = this.state;
    if (employee_life_cycle && Object.keys(employee_life_cycle).length) {
      let termination_error = false;
      let indexes = Object.keys(employee_life_cycle);

      let selectedIndex = indexes.filter((val, index) => {
        return (
          employee_life_cycle[val].text === "New Employee Onboarding" ||
          employee_life_cycle[val].text === "Employment Confirmation" ||
          employee_life_cycle[val].text === "Termination"
        );
      });
      selectedIndex.map((val, i) => {
        employee_life_cycle[val].steps.forEach(step => {
          if (!step.status) {
            termination_error = true;
          }
        });
      });
      if (termination_error) {
        notify(
          "ELC till employment termination need to be complete before terminating a user!!",
          "",
          "error"
        );
      } else {
        this.setState({ open: false, isTerminatingEmployee: false });
        if (newProfileDetails) {
          this.props.onUpdateUserProfileDetails(newProfileDetails, true,check_empty_value).then(
            data => {},
            error => {
              this.setState(
                {
                  open: true,
                  updateProfileErr: error
                },
                () =>
                  this.setState({
                    updateProfileErr: ""
                  })
              );
            }
          );
        } else {
          this.props
            .onUpdateUserProfileDetails(
              {
                ...this.state.user_profile_detail,
                user_id: this.state.user_profile_detail.user_Id,
                termination_date: moment().format("YYYY-MM-DD")
              },
              true,check_empty_value
            )
            .then(
              data => {},
              error => {
                this.setState(
                  {
                    open: true,
                    updateProfileErr: error
                  },
                  () =>
                    this.setState({
                      updateProfileErr: ""
                    })
                );
              }
            );
        }
      }
    }
  };
  handleConfirm = (newProfileDetails,check_empty_value) => {
    const { employee_life_cycle } = this.state;
    if (employee_life_cycle && Object.keys(employee_life_cycle).length) {
      let confirmation_error = false;
      let indexes = Object.keys(employee_life_cycle);
      let selectedIndex = indexes.filter((val, index) => {
        return (
          employee_life_cycle[val].text === "New Employee Onboarding" ||
          employee_life_cycle[val].text === "Employment Confirmation"
        );
      });
      selectedIndex.map((val, i) => {
        this.state.employee_life_cycle[val].steps.forEach(step => {
          if (!step.status) {
            confirmation_error = true;
          }
        });
      });

      if (confirmation_error) {
        notify(
          "error",
          "ELC till Confirmation need to be complete before confirming a user!!",
          "error"
        );
      } else {
        this.setState({ open: false, isTerminatingEmployee: false });
        if (newProfileDetails) {
          this.props.onUpdateUserProfileDetails(newProfileDetails, true,check_empty_value).then(
            data => {
              this.setState(
                {
                  warning: data.message_warning
                },
                () => {
                  this.setState({
                    warning: ""
                  });
                }
              );
            },
            error => {
              this.setState(
                {
                  open: true,
                  updateProfileErr: error
                },
                () =>
                  this.setState({
                    updateProfileErr: ""
                  })
              );
            }
          );
        } else {
          this.props
            .onUpdateUserProfileDetails(
              {
                ...this.state.user_profile_detail,
                user_id: this.state.user_profile_detail.user_Id,
                training_completion_date: moment().format("YYYY-MM-DD"),
                notifyEmpConfirmation: true,
                notifyEmpConfirmationPolicies: true
              },
              true,check_empty_value
            )
            .then(
              data => {
                this.setState(
                  {
                    warning: data.message_warning
                  },
                  () => {
                    this.setState({
                      warning: ""
                    });
                  }
                );
              },
              error => {
                this.setState(
                  {
                    open: true,
                    updateProfileErr: error
                  },
                  () =>
                    this.setState({
                      updateProfileErr: ""
                    })
                );
              }
            );
        }
      }
    }
  };

  handleConfirmAndTerminate = (newProfileDetails,check_empty_value) => {
    const { employee_life_cycle } = this.state;
    if (employee_life_cycle && Object.keys(employee_life_cycle).length) {
      let confirmation_error = false;
      let indexes = Object.keys(employee_life_cycle);

      let selectedIndex = indexes.filter((val, index) => {
        return employee_life_cycle[val].text === "New Employee Onboarding";
      });
      this.state.employee_life_cycle[selectedIndex[0]].steps.forEach(step => {
        if (!step.status) {
          confirmation_error = true;
        }
      });
      if (confirmation_error) {
        notify(
          "error",
          "ELC for on-board need to be complete before confirming an user!!",
          "error"
        );
      } else {
        let termination_error = false;
        let indexes = Object.keys(employee_life_cycle);

        let selectedIndex = indexes.filter((val, index) => {
          return employee_life_cycle[val].text === "Employment Confirmation";
        });
        employee_life_cycle[selectedIndex[0]].steps.forEach(step => {
          if (!step.status) {
            termination_error = true;
          }
        });
        if (termination_error) {
          notify(
            "ELC till employment confirmation need to be complete before terminating a user!!",
            "",
            "error"
          );
        } else {
          this.props.onUpdateUserProfileDetails(newProfileDetails, true,check_empty_value).then(
            data => {
              this.setState(
                {
                  warning: data.message_warning
                },
                () => {
                  this.setState({
                    warning: ""
                  });
                }
              );
            },
            error => {
              this.setState(
                {
                  open: true,
                  updateProfileErr: error
                },
                () =>
                  this.setState({
                    updateProfileErr: ""
                  })
              );
            }
          );
        }
      }
    }
  };
  toggleSecondTabs = tab => {
    if (this.state.activeSecondTab !== tab) {
      this.setState({
        activeSecondTab: tab
      });
    }
  };
  parentUpdate = data => {
    this.setState({
      user_profile_detail: {
        ...this.state.user_profile_detail,
        ...data
      }
    });
  };

  onToggle = e => {
    const vals = Number(this.state.user_profile_detail.slack_msg) == 0 ? 1 : 0;
    this.setState({
      user_profile_detail: {
        ...this.state.user_profile_detail,
        slack_msg: vals
      }
    });
  };

  collapseHandler = collapseId => {
    if (collapseId == this.state.collapseId) {
      this.setState({ collapseId: "null" });
    } else {
      this.setState({ collapseId });
    }
  };

  onDeleteDocument = (docId, userid) => {
    this.props
      .onDeleteDocument(docId)
      .then(msg => {
        this.props.onGetUserDocument(userid);
        notify("Success!", msg.toString(), "success");
      })
      .catch(err => {
        notify("Error!", err.toString(), "error");
      });
  };
  callUpdateUserPassword = password => {
    this.props
      .onUpdateUserPassword({
        password: password,
        user_id: this.props.manageUsers.user_profile_detail.user_Id
      })
      .then(
        data => {},
        err => {
          this.setState(
            {
              passErr: err
            },
            () => {
              this.setState({
                passErr: ""
              });
            }
          );
        }
      );
  };

  render() {
    let role = this.props.usersList.users.filter((val, i) => {
      return val.user_Id == this.props.manageUsers.user_profile_detail.user_Id;
    });
    let secondCol = () => {
      return (
        <div className="disable-user">
          <div className="pb-2">
            <Button
              color="danger"
              onClick={() =>
                this.changeEmployeeStatus(
                  this.props.manageUsers &&
                    this.props.manageUsers.user_profile_detail.user_Id,
                  "Disabled"
                )
              }
              className="btn btn-primary w-100"
              disabled={
                this.props.user_profile_detail &&
                Object.keys(this.props.user_profile_detail).length
                  ? !this.props.user_profile_detail.employee_life_cycle
                  : false
              }
            >
              Terminate Employee
            </Button>
          </div>
          {!this.props.manageUsers.user_profile_detail
            .training_completion_date && (
            <div>
              {" "}
              <Button
                color="primary"
                onClick={() => {
                  this.toggleSecondTabs("tab23");
                }}
                className="btn btn-primary w-100"
                disabled={
                  this.props.user_profile_detail &&
                  Object.keys(this.props.user_profile_detail).length
                    ? !this.props.user_profile_detail.employee_life_cycle
                    : false
                }
              >
                Confirm Employee
              </Button>
            </div>
          )}
        </div>
      );
    };
    return (
      <div className="manageUsersSection">
        <div className="pl-0 pr-0">
          <AlertFader>
            {this.state.updateProfileErr && (
              <Alert
                className="alert-transparent rem-fade rh-section rh-alert"
                color="danger"
              >
                <span className="alert-bold">ALERT:</span>
                <span>{this.state.updateProfileErr}</span>
              </Alert>
            )}
            {this.state.warning && (
              <Alert
                className="alert-transparent rem-fade rh-section rh-alert"
                color="info"
              >
                <span className="alert-bold">INFO:</span>
                <span>{this.state.warning}</span>
              </Alert>
            )}
          </AlertFader>
          {this.state.previousPageMessage && (
            <Alert
              className="alert-transparent rem-fade rh-section rh-alert"
              color="success"
            >
              <span className="alert-bold">INFO</span>:{" "}
              <span>{this.state.previousPageMessage + ". "}</span>
              <span>{"Password:"}</span>
              <span className="alert-bold">{this.state.password}</span>
            </Alert>
          )}
          <AlertFader>
            {this.state.password_status_message &&
              Object.keys(this.state.password_status_message).length > 0 && (
                <Alert
                  className="alert-transparent rem-fade rh-section rh-alert"
                  color={
                    this.state.password_status_message.error == 0
                      ? "success"
                      : "danger"
                  }
                >
                  <span className="alert-bold">
                    {this.state.password_status_message.error == 0
                      ? "INFO"
                      : "ALERT"}
                  </span>
                  : <span>{this.state.password_status_message.message}</span>
                </Alert>
              )}
          </AlertFader>
          <AlertFader>
            {this.state.passErr && (
              <Alert
                className="alert-transparent rem-fade rh-section rh-alert"
                color="danger"
              >
                <span className="alert-bold">ALERT</span>:{" "}
                <span>{this.state.passErr}</span>
              </Alert>
            )}
          </AlertFader>
        </div>
        {/* <Dialog
          title={
            this.state.isTerminatingEmployee
              ? "Reason for Termination"
              : "Add Salary"
          }
          modal={false}
          open={this.state.open}
          onRequestClose={() =>
            this.setState({ open: false, isTerminatingEmployee: false })
          }
          contentClassName="dialog-class"
          autoScrollBodyContent
        >
          {this.state.isTerminatingEmployee ? (
            <div>
              <input
                type="text"
                className="form-control"
                value={this.state.user_profile_detail.medical_condition}
                onChange={e =>
                  this.setState({
                    ...this.state.user_profile_detail,
                    medical_condition: e.target.value
                  })
                }
              />
              <Button type="danger" onClick={this.handleTerminate}>
                Terminate
              </Button>
            </div>
          ) : (
            <div className="content-salary">
              <AddSalaryForm
                {...this.props}
                userid={this.state.selected_user_id}
                callAddUserSalary={this.callAddUserSalary}
              />
            </div>
          )}
        </Dialog> */}
        <AlertFader>
          {this.state.status_message == "No fields updated into table" && (
            <Alert
              className="alert-transparent rem-fade rh-section rh-alert"
              color="info"
            >
              <span className="bold-text">Info :</span>
              <span> {this.state.status_message}</span>
            </Alert>
          )}
          {this.state.status_message ==
            "Employee details updated successfully!!" && (
            <Alert
              className="alert-transparent rem-fade rh-section rh-alert"
              color="success"
            >
              {" "}
              <span className="bold-text">Info:</span>
              <span>{this.state.status_message}</span>
            </Alert>
          )}
        </AlertFader>
        <div id="content" className="app-content box-shadow-z0" role="main">
          <Header
            pageTitle={"Manage Employees Profile"}
            showLoading={this.props.frontend.show_loading}
            userListHeader
          />
          {/* <UsersListHeader
            users={this.props.usersList.users}
            selectedUserId={this.state.selected_user_id}
            onUserClick={this.onUserClick}
          /> */}
          {/* <div className="app-body" id="view">
            <div>
              <div id="manage-user">
                <div>
                  <div className="profile-thing card-detail-info d-flex my-2 p-3 pl-0">
                    <div className="d-flex  p-4">
                      <div className="image-place">
                        <img src={profilePlaceholder} />
                      </div>
                      <div className="d-flex flex-column mr-2">
                        <div className="profile-user-name cell-item  ">
                          <h5>{this.state.user_profile_detail.name}</h5>
                        </div>
                        <div className="peofile-detail-view d-block">
                          <p className="profile-job_detail m-0 p-0">
                            {this.state.user_profile_detail.jobtitle}
                          </p>
                        </div>
                      </div>
                    </div>
                    <div className="disable-user">
                      <Button
                        color="danger"
                        onClick={() =>
                          this.changeEmployeeStatus(
                            this.props.manageUsers &&
                              this.props.manageUsers.user_profile_detail
                                .user_Id,
                            "Disabled"
                          )
                        }
                        className="btn"
                        disabled={
                          this.props.user_profile_detail &&
                          Object.keys(this.props.user_profile_detail).length
                            ? !this.props.user_profile_detail
                                .employee_life_cycle
                            : false
                        }
                      >
                        Disable User
                      </Button>
                      <Button
                        color="primary"
                        onClick={() => this.handleConfirm()}
                        className="btn btn-primary"
                        disabled={
                          this.props.user_profile_detail &&
                          Object.keys(this.props.user_profile_detail).length
                            ? !this.props.user_profile_detail
                                .employee_life_cycle
                            : false
                        }
                      >
                        Confirm Employee
                      </Button>
                    <  /div>
                  </div>
                </div>
              </div>
            </div>
          </div  > */}
          <UserPicHeadTitle
            profileImage={
              this.state.user_profile_detail.profileImage || profilePlaceholder
            }
            userName={this.state.user_profile_detail.username}
            jobTitle={this.state.user_profile_detail.jobtitle}
            name={this.state.user_profile_detail.name}
            userId={this.state.user_profile_detail.user_Id}
            secondCol={
              Object.keys(this.props.manageUsers.user_profile_detail).length >
                0 && secondCol
            }
          />
        </div>

        <div className="table-responsive-thing d-sm-flex">
          <div className="col-sm-12 col col-md-2 pr-0 pl-0">
            <div
              className={classnames("manage-users-tab", "mb-2", "p-3", {
                active: this.state.activeSecondTab === "tab21"
              })}
              onClick={() => {
                this.toggleSecondTabs("tab21");
              }}
            >
              <span className="text-success pr-1">01</span>
              <span>Basic Details</span>
            </div>
            {window.screen.availWidth < 768 && (
              <TabContent
                className="mb-lg pl-0"
                activeTab={this.state.activeSecondTab}
              >
                <TabPane tabId="tab21" className="manage-users-tabPane pb-4">
                  <BasicDetail
                    user_profile_detail={this.state.user_profile_detail}
                    callUpdateUserProfileDetails={
                      this.callUpdateUserProfileDetails
                    }
                    toggleSecondTabs={this.toggleSecondTabs}
                    username={this.state.username}
                    parentUpdate={this.parentUpdate}
                    {...this.props}
                    onToggle={this.onToggle}
                    updateProfileLoader={
                      this.props.manageUsers.updateProfileLoader
                    }
                    teamList={this.props.teamList.data}
                  />
                </TabPane>
              </TabContent>
            )}
            <div
              className={classnames("manage-users-tab", "mb-2", "p-3", {
                active: this.state.activeSecondTab === "tab22"
              })}
              onClick={() => {
                this.toggleSecondTabs("tab22");
              }}
            >
              <span className="text-success pr-1">02</span>
              <span>Contact Info</span>
            </div>
            {window.screen.availWidth < 768 && (
              <TabContent
                className="mb-lg pl-0"
                activeTab={this.state.activeSecondTab}
              >
                <TabPane tabId="tab22" className="manage-users-tabPane pb-5">
                  <ContactInfo
                    user_profile_detail={this.state.user_profile_detail}
                    callUpdateUserProfileDetails={
                      this.callUpdateUserProfileDetails
                    }
                    toggleSecondTabs={this.toggleSecondTabs}
                    username={this.state.username}
                    {...this.props}
                    parentUpdate={this.parentUpdate}
                    updateProfileLoader={
                      this.props.manageUsers.updateProfileLoader
                    }
                  />
                </TabPane>
              </TabContent>
            )}
            <div
              className={classnames("manage-users-tab", "mb-2", "p-3", {
                active: this.state.activeSecondTab === "tab23"
              })}
              onClick={() => {
                this.toggleSecondTabs("tab23");
              }}
            >
              <span className="text-success pr-1">03</span>
              <span>Important Dates</span>
            </div>
            {window.screen.availWidth < 768 && (
              <TabContent
                className="mb-lg pl-0"
                activeTab={this.state.activeSecondTab}
              >
                <TabPane tabId="tab23" className="manage-users-tabPane pb-5">
                  <ImportantDates
                    user_profile_detail={this.state.user_profile_detail}
                    callUpdateUserProfileDetails={
                      this.callUpdateUserProfileDetails
                    }
                    toggleSecondTabs={this.toggleSecondTabs}
                    username={this.state.username}
                    {...this.props}
                    parentUpdate={this.parentUpdate}
                    updateProfileLoader={
                      this.props.manageUsers.updateProfileLoader
                    }
                    handleConfirm={this.handleConfirm}
                  />
                </TabPane>
              </TabContent>
            )}

            <div
              className={classnames("manage-users-tab", "mb-2", "p-3", {
                active: this.state.activeSecondTab === "tab24"
              })}
              onClick={() => {
                this.toggleSecondTabs("tab24");
              }}
            >
              <span className="text-success pr-1">04</span>
              <span>Comments</span>
            </div>
            {window.screen.availWidth < 768 && (
              <TabContent
                className="mb-lg pl-0"
                activeTab={this.state.activeSecondTab}
              >
                <TabPane tabId="tab24" className="manage-users-tabPane pb-5">
                  <Comments
                    user_profile_detail={this.state.user_profile_detail}
                    callUpdateUserProfileDetails={
                      this.callUpdateUserProfileDetails
                    }
                    toggleSecondTabs={this.toggleSecondTabs}
                    username={this.state.username}
                    {...this.props}
                    parentUpdate={this.parentUpdate}
                    updateProfileLoader={
                      this.props.manageUsers.updateProfileLoader
                    }
                  />
                </TabPane>
              </TabContent>
            )}

            <div
              className={classnames("manage-users-tab", "mb-2", "p-3", {
                active: this.state.activeSecondTab === "tab25"
              })}
              onClick={() => {
                this.toggleSecondTabs("tab25");
              }}
            >
              <span className="text-success pr-1">05</span>
              <span>Bank Details</span>
            </div>
            {window.screen.availWidth < 768 && (
              <TabContent
                className="mb-lg pl-0"
                activeTab={this.state.activeSecondTab}
              >
                <TabPane tabId="tab25" className="manage-users-tabPane ">
                  <DisplayUserBankDetails
                    userBankDetails={this.state.user_bank_detail}
                  />
                </TabPane>
              </TabContent>
            )}
            <div
              className={classnames("manage-users-tab", "mb-2", "p-3", {
                active: this.state.activeSecondTab === "tab27"
              })}
              onClick={() => {
                this.toggleSecondTabs("tab27");
              }}
            >
              <span className="text-success pr-1">06</span>
              <span>Document</span>
            </div>
            {window.screen.availWidth < 768 && (
              <TabContent
                className="mb-lg pl-0"
                activeTab={this.state.activeSecondTab}
              >
                <TabPane tabId="tab27" className="manage-users-tabPane">
                  <FormAddDocuments
                    user_profile_detail={this.state.user_profile_detail}
                    user_documents={this.props.manageUsers.user_documents || []}
                    deleteDocument={this.onDeleteDocument}
                    onGetUserDocument={this.props.onGetUserDocument}
                  />
                </TabPane>
              </TabContent>
            )}

            <div
              className={classnames("manage-users-tab", "mb-2", "p-3", {
                active: this.state.activeSecondTab === "tab28"
              })}
              onClick={() => {
                this.toggleSecondTabs("tab28");
              }}
            >
              <span className="text-success pr-1">07</span>
              <span>Payslip</span>
            </div>
            {window.screen.availWidth < 768 && (
              <TabContent
                className="mb-lg pl-0"
                activeTab={this.state.activeSecondTab}
              >
                <TabPane tabId="tab28" className="manage-users-tabPane">
                  <UserPayslipsHistory
                    user_payslip_history={this.state.user_payslip_history}
                  />
                </TabPane>
              </TabContent>
            )}
            {(this.props.manageUsers.user_profile_detail.type !== "Admin" &&
              role.length > 0 &&
              role[0].role_name !== "HR Payroll Manager" &&
              this.props.loggedUser.data.role == "HR") ||
            this.props.loggedUser.data.role == "Admin" ? (
              <>
                <div
                  className={classnames("manage-users-tab", "mb-2", "p-3", {
                    active: this.state.activeSecondTab === "tab29"
                  })}
                  onClick={() => {
                    this.toggleSecondTabs("tab29");
                  }}
                >
                  <span className="text-success pr-1">01</span>
                  <span>Update Password</span>
                </div>
                {window.screen.availWidth < 768 && (
                  <TabContent
                    className="mb-lg pl-0"
                    activeTab={this.state.activeSecondTab}
                  >
                    <TabPane
                      tabId="tab29"
                      className="manage-users-tabPane pb-4"
                    >
                      <UpdatePassWord />
                    </TabPane>
                  </TabContent>
                )}
              </>
            ) : null}
          </div>

          {window.screen.availWidth >= 768 && (
            <div className="col-sm-12 col col-md-7  pr-0 pl-3">
              <Col xs="12" className="mb-5 users-tabs-wrapper ml-0 pl-0 pr-3">
                <TabContent
                  className="mb-lg pl-0"
                  activeTab={this.state.activeSecondTab}
                >
                  <TabPane tabId="tab21" className="manage-users-tabPane pb-4">
                    <BasicDetail
                      user_profile_detail={this.state.user_profile_detail}
                      callUpdateUserProfileDetails={
                        this.callUpdateUserProfileDetails
                      }
                      toggleSecondTabs={this.toggleSecondTabs}
                      username={this.state.username}
                      parentUpdate={this.parentUpdate}
                      {...this.props}
                      onToggle={this.onToggle}
                      updateProfileLoader={
                        this.props.manageUsers.updateProfileLoader
                      }
                      teamList={this.props.teamList.data}
                    />
                  </TabPane>
                  <TabPane tabId="tab22" className="manage-users-tabPane pb-5">
                    <ContactInfo
                      user_profile_detail={this.state.user_profile_detail}
                      callUpdateUserProfileDetails={
                        this.callUpdateUserProfileDetails
                      }
                      toggleSecondTabs={this.toggleSecondTabs}
                      username={this.state.username}
                      {...this.props}
                      parentUpdate={this.parentUpdate}
                      updateProfileLoader={
                        this.props.manageUsers.updateProfileLoader
                      }
                    />
                  </TabPane>
                  <TabPane tabId="tab23" className="manage-users-tabPane pb-5">
                    <ImportantDates
                      user_profile_detail={this.state.user_profile_detail}
                      callUpdateUserProfileDetails={
                        this.callUpdateUserProfileDetails
                      }
                      toggleSecondTabs={this.toggleSecondTabs}
                      username={this.state.username}
                      {...this.props}
                      parentUpdate={this.parentUpdate}
                      updateProfileLoader={
                        this.props.manageUsers.updateProfileLoader
                      }
                      handleConfirm={this.handleConfirm}
                    />
                  </TabPane>
                  <TabPane tabId="tab24" className="manage-users-tabPane pb-5">
                    <Comments
                      user_profile_detail={this.state.user_profile_detail}
                      callUpdateUserProfileDetails={
                        this.callUpdateUserProfileDetails
                      }
                      toggleSecondTabs={this.toggleSecondTabs}
                      username={this.state.username}
                      {...this.props}
                      parentUpdate={this.parentUpdate}
                      updateProfileLoader={
                        this.props.manageUsers.updateProfileLoader
                      }
                    />
                  </TabPane>
                  <TabPane tabId="tab25" className="manage-users-tabPane ">
                    <DisplayUserBankDetails
                      userBankDetails={this.state.user_bank_detail}
                    />
                  </TabPane>
                  <TabPane tabId="tab27" className="manage-users-tabPane">
                    <FormAddDocuments
                      user_profile_detail={this.state.user_profile_detail}
                      user_documents={
                        this.props.manageUsers.user_documents || []
                      }
                      deleteDocument={this.onDeleteDocument}
                      onGetUserDocument={this.props.onGetUserDocument}
                    />
                  </TabPane>
                  <TabPane tabId="tab28" className="manage-users-tabPane">
                    <UserPayslipsHistory
                      user_payslip_history={this.state.user_payslip_history}
                    />
                  </TabPane>
                  <TabPane tabId="tab29" className="manage-users-tabPane pb-5">
                    <UpdatePassWord
                      callUpdateUserPassword={this.callUpdateUserPassword}
                    />
                  </TabPane>
                </TabContent>
              </Col>
            </div>
          )}
          <div className="col-sm-12 col col-md-3 p-0">
            <EmployeeLifeCycle
              collapseHandler={this.collapseHandler}
              collapseId={this.state.collapseId}
              employee_life_cycle={this.state.employee_life_cycle}
              user_profile_detail={this.state.user_profile_detail}
              handleConfirm={this.handleConfirm}
              handleTerminate={this.openTerminate}
              handleChangeSteps={stepid =>
                this.handleChangeSteps(
                  stepid,
                  this.state.user_profile_detail.user_Id
                )
              }
            />
          </div>
        </div>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    frontend: state.frontend.toJS(),
    managePayslips: state.managePayslips.toJS(),
    loggedUser: state.logged_user.userLogin,
    usersList: state.usersList.toJS(),
    manageUsers: state.manageUsers.toJS(),
    teamList: state.teamList.teamList
  };
}

const mapDispatchToProps = dispatch => {
  return {
    onUpdateUserPassword: passwordDetails => {
      return dispatch(actionsManageUsers.updateUserPassword(passwordDetails));
    },
    onUsersList: () => {
      return dispatch(actionsUsersList.get_users_list());
    },
    onUserProfileDetails: (userid, username) => {
      return dispatch(
        actionsManageUsers.getUserProfileDetails(userid, username)
      );
    },
    onUpdateUserBankDetails: newBankDetails => {
      // return dispatch(actionsManageUsers.updateUserBankDetails(newBankDetails));
    },
    onUpdateUserProfileDetails: (newProfileDetails, value,check_empty_value) => {
      return dispatch(
        actionsManageUsers.updateUserProfileDetails(newProfileDetails, value,check_empty_value)
      );
    },
    onUpdateUserDeviceDetails: newDeviceDetails => {
      // return dispatch(actionsManageUsers.updateUserDeviceDetails(newDeviceDetails));
    },
    onUpdatedocuments: documentLink => {
      // return dispatch(actionsManageUsers.updateDocument(documentLink));
    },
    onChangeEmployeeStatus: (userid, status) => {
      return dispatch(actionsManageUsers.changeEmployeeStatus(userid, status));
    },
    onGetUserDocument: userid => {
      return dispatch(actionsManageUsers.getUserDocument(userid));
    },
    onDeleteDocument: docId => {
      return dispatch(actionsManageUsers.deleteDocument(docId));
    },
    onUserManagePayslipsData: userid => {
      return dispatch(
        actionsManagePayslips.get_user_manage_payslips_data(userid)
      );
    },
    onFetchTeams: () => {
      return dispatch(actions.requestTeamList());
    },
    onGetStages: id => {
      return dispatch(actionsManageUsers.getSteps(id));
    },
    onHandleChangeSteps: (userid, stepid) => {
      return dispatch(actionsManageUsers.changeSteps(userid, stepid));
    },
    onAddNewSalary: new_salary_data => {
      return dispatch(
        actions_manageSalary.add_user_new_salary(new_salary_data)
      );
    },
    showHeading:(data)=>{
      return dispatch(actions.showHeading(data));
    }
  };
};

const VisibleManageUsers = connect(
  mapStateToProps,
  mapDispatchToProps
)(ManageUsers);
const RouterVisibleManageUsers = withRouter(VisibleManageUsers);

export default RouterVisibleManageUsers;

ManageUsers.propTypes = {
  onIsAlreadyLogin: PropTypes.func.isRequired,
  onFetchTeam: PropTypes.func.isRequired,
  onUserProfileDetails: PropTypes.func.isRequired,
  onGetUserDocument: PropTypes.func.isRequired,
  onUserManagePayslipsData: PropTypes.func.isRequired,
  onUpdateUserBankDetails: PropTypes.func.isRequired,
  onUpdateUserDeviceDetails: PropTypes.func.isRequired,
  onUpdateUserProfileDetails: PropTypes.func.isRequired,
  onChangeEmployeeStatus: PropTypes.func.isRequired,
  onUsersList: PropTypes.func.isRequired,
  onUpdatedocuments: PropTypes.func.isRequired,
  usersList: PropTypes.object.isRequired,
  manageUsers: PropTypes.object.isRequired,
  router: PropTypes.object.isRequired
};
