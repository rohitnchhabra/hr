import React from "react";
import { connect } from "react-redux";
import { withRouter } from "react-router";
import { compose } from "redux";
import _ from "lodash";
import { isNotUserValid, getLoggedUser } from "../../../services/generic";
import Header from "../../../components/generic/Header";
import UsersListHeader from "../../../components/generic/UsersListHeader";
import DisabledUserDetails from "../../../components/manageUser/DisabledUserDetails";
import UserPayslipsHistory from "../../../components/salary/managePayslips/UserPayslipsHistory";
import UpdateEmployeeDocument from "../../../modules/manageUsers/components/UpdateEmployeeDocument";
import * as actionsDisabledEmployee from "../../../redux/generic/actions/usersList";
import * as actionsManageUsers from "../../../redux/manageUsers/actions/manageUsers";
import * as actionsManagePayslips from "../../../redux/salary/actions/managePayslips";
import * as actions from '../../../redux/actions'
import isMobile from "../../../components/hoc/WindowResize";
import { Alert } from "reactstrap";
import Heading from "../../../components/generic/Heading";

class PageDisabledEmployes extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      disabled_users: [],
      defaultUserDisplay: "",
      selected_user_name: "",
      f_char: "",
      selected_user_jobtitle: "",
      selected_user_id: "",
      userDetails: "",
      key: 0,
      user_documents: {},
      user_payslip_history: [],
      isLoggedUserAdmin: false
    };
  }
  componentWillMount() {
    window.scrollTo(0, 0);
    this.props.onFetchDisabledEmployee();
    if (getLoggedUser().data.role === "Admin") {
      this.setState({ isLoggedUserAdmin: true });
    }
    this.props.showHeading("Ex Employee");
  }
  componentWillReceiveProps(props) {
    window.scrollTo(0, 0);
    let isNotValid = isNotUserValid(
      this.props.location.pathname,
      props.loggedUser
    );
    if (isNotValid.status) {
      this.props.history.push(isNotValid.redirectTo);
    }
    this.setState({
      user_payslip_history: props.managePayslips.user_payslip_history,
      user_documents: props.manageUsers.user_documents
    });
  }
  componentDidUpdate(props) {
    const { currentDisableUser } = this.props.teamStats;
    let firstUser = currentDisableUser.currentUser;
    if (this.state.defaultUserDisplay === "") {
      if (this.props.usersList.disabled_users.length > 0) {
        let userList = this.props.usersList.disabled_users;
        let firstUser = userList[this.state.key];
        if (firstUser === undefined) {
          firstUser = userList[0];
        }
        this.onUserClick(firstUser);
      }
    }

    if (currentDisableUser.type !== props.teamStats.currentDisableUser.type) {
      this.setState({
        defaultUserDisplay: firstUser.user_Id,
        selected_user_name: firstUser.name,
        f_char: firstUser.name.charAt(0),
        selected_user_jobtitle: firstUser.jobtitle,
        selected_user_id: firstUser.user_Id,
        userDetails: firstUser
      });
    }
  }
  onUserClick = firstUser => {
    window.scrollTo(0, 0);
    this.setState({
      defaultUserDisplay: firstUser.user_Id,
      selected_user_name: firstUser.name,
      f_char: firstUser.name.charAt(0),
      selected_user_jobtitle: firstUser.jobtitle,
      selected_user_id: firstUser.user_Id,
      userDetails: firstUser
    });
    this.props.onGetUserDocument(firstUser.user_Id);
    this.props.onUserManagePayslipsData(firstUser.user_Id);
  };
  changeEmployeeStatus = (userid, status) => {
    this.props.onChangeEmployeeStatus(userid, status).then(() => {
      this.props.onFetchDisabledEmployee();
      if (this.props.usersList.disabled_users.length > 0) {
        let userList = this.props.usersList.disabled_users;
        let firstUser = userList[0];
        this.onUserClick(firstUser);
      }
    });
  };
  render() {
    let disabledUsers = _.orderBy(
      this.props.usersList.disabled_users,
      "user_Id",
      "asc"
    );
    let userDetails = "";
    if (!_.isEmpty(this.state.userDetails)) {
      userDetails = (
        <DisabledUserDetails
          userDetails={this.state.userDetails}
          changeEmployeeStatus={this.changeEmployeeStatus}
        />
      );
    }
    const { isLoggedUserAdmin, user_payslip_history } = this.state;
    return (
      <div>
        <div id="content" className="app-content box-shadow-z0" role="main">

          <Header
            pageTitle={"Disabled Employees"}
            showLoading={this.props.frontend.show_loading}
            userListHeader
          />
          <UsersListHeader
            disabledUser
            users={disabledUsers}
            selectedUserId={this.state.selected_user_id}
            onUserClick={this.onUserClick}
          />
          <div className="app-body" id="view">
            <div className="p-t p-x">
              <div className="row justify-content-end">
                {/* <div className="col-sm-3 hidden-xs" id="fixedScroll">
                  <UsersList
                    disabledUser
                    users={disabledUsers}
                    selectedUserId={this.state.selected_user_id}
                    onUserClick={this.onUserClick}
                    top={5}
                  />
                </div> */}
                <div className={` col-xs-12 ex-employee-details pr-0`}>
                  <div className="box">
                    <div className="box-body">{userDetails}</div>
                  </div>
                  {!isLoggedUserAdmin && !user_payslip_history && (
                    <Alert
                      className="alert-transparent rem-fade mb-4"
                      color="info"
                    >
                      Note! Only Admin and Payroll roles have access to employee
                      payslips
                    </Alert>
                  )}
                  <div className="row no-gutters">
                    <div className="col-md-6">
                      <div className="box mr-1">
                        <div className="box-body">
                          {
                            <div className="profile-input">
                              <h6 className="text-center">Previous Payslips</h6>
                              <hr />
                              <UserPayslipsHistory
                                user_payslip_history={
                                  this.state.user_payslip_history
                                }
                              />
                            </div>
                          }
                        </div>
                      </div>
                    </div>
                    <div className="col-md-6">
                      <div className="box ml-1">
                        <div className="box-body">
                          <div className="profile-input">
                            <UpdateEmployeeDocument
                              disabled
                              user_documents={this.state.user_documents}
                              user_id={this.state.selected_user_id}
                              onUpdatedocuments={this.props.onUpdatedocuments}
                              {...this.props}
                            />
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    frontend: state.frontend.toJS(),
    loggedUser: state.logged_user.userLogin,
    usersList: state.usersList.toJS(),
    managePayslips: state.managePayslips.toJS(),
    manageUsers: state.manageUsers.toJS(),
    teamStats: state.teamStats
  };
}
const mapDispatchToProps = dispatch => {
  return {
    onFetchDisabledEmployee: () => {
      return dispatch(actionsDisabledEmployee.getDisabledUsersList());
    },
    onChangeEmployeeStatus: (userid, status) => {
      return dispatch(actionsManageUsers.changeEmployeeStatus(userid, status));
    },
    onDeleteDocument: docId => {
      return dispatch(actionsManageUsers.deleteDocument(docId));
    },
    onGetUserDocument: userid => {
      return dispatch(actionsManageUsers.getUserDocument(userid));
    },
    onUserManagePayslipsData: userid => {
      return dispatch(
        actionsManagePayslips.get_user_manage_payslips_data(userid)
      );
    },
    showHeading:(data)=>{
      return dispatch(actions.showHeading(data));
    }
  };
};

export default compose(isMobile)(
  withRouter(
    connect(
      mapStateToProps,
      mapDispatchToProps
    )(PageDisabledEmployes)
  )
);
