import React from "react";
import { connect } from "react-redux";
import { notify } from "../../../services/notify";
import FormAddNewEmployee from "../components/FormAddNewEmployee";
import FormAddNewEmployeeDetails from "../components/FormAddNewEmployeeDetails";
import { withRouter } from "react-router";
import * as actions from "../../../redux/actions";
import * as actionsUsersList from "../../../redux/generic/actions/usersList";
import * as actionsManageUsers from "../../../redux/manageUsers/actions/manageUsers";
import { Nav, NavItem, NavLink, Row, Col, Progress } from "reactstrap";
import Widget from "../../../components/Widget";
import classnames from "classnames";
import Heading from "../../../components/generic/Heading";

class AddNewEmployee extends React.Component {
  constructor(props) {
    super(props);
    this.props.onIsAlreadyLogin();
    this.state = {
      currentStep: 1,
      user_id: "",
      progress: 50,
      message1: "",
      message2: "",
      password_generated:""
    };
    this.props.onUsersList();
  }

  componentDidMount(){
    this.props.showHeading("New Employee")
  }

  callAddNewEmployee = newEmployeeDetails => {
    this.setState({
      loading: true
    });
    this.props.onAddNewEmployee(newEmployeeDetails).then(
      data => {
        this.setState({
          message1: data.message
        });
        this.props.onUsersList();
        this.setState({
          user_id: data.user_id,
          password_generated: data.password_generated,
        });
        this.setState({ currentStep: 2, progress: 100 });
        let uname = this.props.usersList.username;
      },
      error => {
        this.setState(
          {
            loading: false,
            message2: error
          },
          () => {
            if (this.state.message2 && window.screen.availWidth < 576) {
              window.scrollTo(0, 0);
            }
          }
        );
      }
    );
  };

  render() {
    const { currentStep, user_id, password_generated} = this.state;
    return (
      <div>
        <div id="content" className="app-content box-shadow-z0" role="main">
          <Col className="mb-5 add-new-employee-wrapper p-0">
            <Widget
              id="add-new-Employee"
              className={"formWizard add-new-Employee pr-0 pl-0"}
            >
              <Col className="col-sm-12 col-md-3 col-lg-3 p-0">
                <Nav className={"wizardNavigation d-block"} id="table-content ">
                  <NavItem id="navItem-size">
                    <NavLink
                      className={classnames("p-3 pr-0", {
                        active: this.state.currentStep == 1
                      })}
                    >
                      <span className="text-success pr-1">01</span>
                      <span>Basic Information </span>
                    </NavLink>
                  </NavItem>
                  {window.screen.availWidth < 768 && (
                    <Col className="col-sm-12 col-lg-9 pl-0 pr-0 mt-3 ">
                      <div className="tab-content">
                        {currentStep === 1 && (
                          <FormAddNewEmployee
                            callAddNewEmployee={this.callAddNewEmployee}
                            message2={this.state.message2}
                          />
                        )}
                      </div>
                    </Col>
                  )}
                  <NavItem id="navItem-size">
                    <NavLink
                      className={classnames("p-3 pr-0", {
                        active: this.state.currentStep == 2
                      })}
                    >
                      <span className="text-success pr-1">02</span>
                      <span>Personal Information</span>
                    </NavLink>
                  </NavItem>
                  {window.screen.availWidth < 768 && (
                    <Col className="col-sm-12 col-lg-9 pl-0 pr-0 mt-2 ">
                      <div className="tab-content">
                        {currentStep === 2 && (
                          <FormAddNewEmployeeDetails
                            message1={this.state.message1}
                            user_id={user_id}
                          />
                        )}
                      </div>
                    </Col>
                  )}
                </Nav>
              </Col>

              {window.screen.availWidth >= 768 && (
                <Col className="col-sm-12 col-md-9  col-lg-9 pr-0 pl-2 ">
                  <div className="tab-content">
                    {currentStep === 1 && (
                      <FormAddNewEmployee
                        callAddNewEmployee={this.callAddNewEmployee}
                        message2={this.state.message2}
                      />
                    )}
                    {currentStep === 2 && (
                      <FormAddNewEmployeeDetails
                        message1={this.state.message1}
                        user_id={user_id}
                        password_generated={password_generated}
                      />
                    )}
                  </div>
                </Col>
              )}
            </Widget>
          </Col>
        </div>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    frontend: state.frontend.toJS(),
    loggedUser: state.logged_user.userLogin,
    manageUsers: state.manageUsers.toJS(),
    usersList: state.usersList.toJS()
  };
}

const mapDispatchToProps = dispatch => {
  return {
    onIsAlreadyLogin: () => {
      return dispatch(actions.isAlreadyLogin());
    },
    onAddNewEmployee: newEmployeeDetails => {
      return dispatch(actionsManageUsers.addNewEmployee(newEmployeeDetails));
    },
    onUsersList: () => {
      return dispatch(actionsUsersList.get_users_list());
    },
    showHeading:(data)=>{
      return dispatch(actions.showHeading(data));
    }
  };
};

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(AddNewEmployee)
);
