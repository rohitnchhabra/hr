import React from "react";
import { connect } from "react-redux";
import { withRouter } from "react-router";
import { isNotUserValid } from "../../../services/generic";
import AlertNotification from "../../../components/generic/AlertNotification";
import EmployeeDetails from "../../../components/manageUser/EmployeeDetails";
import * as actionsManageUsers from "../../../redux/manageUsers/actions/manageUsers";
import * as actions from '../../../redux/actions';
import Heading from "../../../components/generic/Heading";

class AllEmployesDetails extends React.Component {
  constructor(props) {
    super(props);
  }
  componentWillMount() {
    this.props.getAllEmployeeDetails();
    this.props.showHeading("All Employess");
  }
  componentWillReceiveProps(props) {
    let { location, loggedUser } = props;
    let isNotValid = isNotUserValid(location.pathname, loggedUser);
    if (isNotValid.status) {
      this.props.history.push(isNotValid.redirectTo);
    }
  }
  

  render() {
    const { manageUsers, frontend } = this.props;
    let salary_total = 0;
  if (manageUsers.allEmpolyesDetails.length) {
    for (var i = 0; i < manageUsers.allEmpolyesDetails.length; i++) {
      if(manageUsers.allEmpolyesDetails[i].current_salary !== undefined) {
        salary_total += Number(manageUsers.allEmpolyesDetails[i].current_salary);
      }
    }
  }
  
    
    return (
      <div className="all-employees-container">
        <div className="row no-gutters mb-4">
          <div className='d-flex employees-header align-self-end'> 
                <div>
                  <span> Total </span>
                  <span> <small className='font-weight-bold ml-2'>{manageUsers.allEmpolyesDetails.length} Employees</small> </span>
                </div>
                <div>
                  <span className ='mx-2'>Total salary</span>
                  <small className='font-weight-bold'><strong>Rs {Number(salary_total).toFixed(2)}</strong></small>
                </div>
              {/* Total <small className='font-weight-bold ml-2'>{manageUsers.allEmpolyesDetails.length} Employees</small>  */}
          </div>
        </div>
        <AlertNotification message={manageUsers.status_message} />
        <div id="content" className="app-content box-shadow-z0 all-employees-details-wrapper" role="main">
          <div className="app-body" id="view">
            <div>
              <div className="row ml-0 mr-0">
                <div
                  className="col-md-12 col-sm-12 col-xs-12 p bg-white"
                  id="manage-user"
                >
                  <div className="box ">
                    <div className="p-a block ">
                    {/* <div className='d-flex'>
                    <div><h2 className="font-weight-bold"> Employees </h2></div>
                      
                      <div className='ml-4 '>Total <i className='font-weight-bold'>{manageUsers.allEmpolyesDetails.length}</i> Employees</div>
                      </div> */}
                      {/* <hr /> */}
                      <EmployeeDetails
                        allEmpolyesDetails={manageUsers.allEmpolyesDetails}
                      />
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    frontend: state.frontend.toJS(),
    loggedUser: state.logged_user.userLogin,
    managePayslips: state.managePayslips.toJS(),
    manageUsers: state.manageUsers.toJS(),
    teamList: state.teamList.teamList
  };
}

const mapDispatchToProps = dispatch => {
  return {
    getAllEmployeeDetails: () => {
      return dispatch(actionsManageUsers.getAllEmployeeDetails());
    },
    showHeading:(data)=>{
      return dispatch(actions.showHeading(data));
    }
  };
};

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(AllEmployesDetails)
);
