import React from "react";
import { connect } from "react-redux";
import { withRouter } from "react-router";
import { isNotUserValid } from "../../../services/generic";
import ViewLeavesSummary from "../../../components/leave/leavesSummary/ViewLeavesSummary";
import * as actions_leavesSummary from "../../../redux/leave/actions/leavesSummary";
import * as actions from '../../../redux/actions'
import Heading from "../../../components/generic/Heading";

class LeavesSummary extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      defaultUserDisplay: "",
      daysummary_userid: "",
      daysummary_date: ""
    };
  }
  componentWillMount() {
    let d = new Date();
    let year = d.getFullYear();
    let month = d.getMonth() + 1; // +1 since getMonth starts from 0
    this.props.on_all_leaves_summary(year, month);
    this.props.showHeading("Leaves Summary")
  }
  componentWillReceiveProps(props) {
    window.scrollTo(0, 0);
    let isNotValid = isNotUserValid(
      this.props.location.pathname,
      props.loggedUser
    );
    if (isNotValid.status) {
      this.props.history.push(isNotValid.redirectTo);
    }
  }
  render() {
    return (
      <div className="leeave-summary-wrapper">
        <div id="content" className="app-content box-shadow-z0" role="main">
          <ViewLeavesSummary
            componentData={this.props.leavesSummary}
            on_all_leaves_summary={this.props.on_all_leaves_summary}
          />
        </div>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    frontend: state.frontend.toJS(),
    loggedUser: state.logged_user.userLogin,
    leavesSummary: state.leavesSummary.toJS()
  };
}
const mapDispatchToProps = dispatch => {
  return {
    on_all_leaves_summary: (year, month) => {
      return dispatch(
        actions_leavesSummary.get_all_leaves_summary(year, month)
      );
    },
    showHeading: (data) => {
      return dispatch(
        actions.showHeading(data)
      );
    },
  };
};

const VisibleLeavesSummary = connect(
  mapStateToProps,
  mapDispatchToProps
)(LeavesSummary);

const RouterVisibleLeavesSummary = withRouter(VisibleLeavesSummary);

export default RouterVisibleLeavesSummary;
