import React from "react";
import { connect } from "react-redux";
import { withRouter } from "react-router";
import { CONFIG } from "../../../config/index";
import { isNotUserValid } from "../../../services/generic";
import Header from "../../../components/generic/Header";
import ListLeaves from "../../../components/leave/manageLeaves/ListLeaves";
import ViewLeave from "../../../modules/leave/components/manageLeaves/ViewLeave";
import LeaveColorReference from "../../../components/leave/manageLeaves/LeaveColorReference";
import * as actions from "../../../redux/actions";
import * as actions_listLeaves from "../../../redux/leave/actions/listLeaves";
import * as actions_manageLeave from "../../../redux/leave/actions/manageLeave";
import Widget from "../../../components/Widget/Widget";
import isEmpty from "lodash/isEmpty";
import isEqual from "lodash/isEqual";
import find from "lodash/find";
import filter from "lodash/filter";
import size from "lodash/size"; 
import cloneDeep from "lodash/cloneDeep";
import Heading from "../../../components/generic/Heading";

class ManageLeaves extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: true,
      selectedTab: "",
      leaveListItems: [],
      all_leaves: [],
      selectedLeave: {},
      width: "",
      listLeavesHeight:0
    };
    this.leaveDetailsRef = React.createRef(null);
    this.doLeaveStatusChange = this.doLeaveStatusChange.bind(this);
    this.filterLeaveList = this.filterLeaveList.bind(this);
    this.selectLeave = this.selectLeave.bind(this);
  }

  componentWillMount() {
    this.props.onListLeaves(this.props.loggedUser.data.role);
    this.updateWindowDimensions();
    window.addEventListener("resize", this.updateWindowDimensions);
    this.props.showHeading("Manage Leaves")
  }

  componentWillUnmount() {
    window.removeEventListener("resize", this.updateWindowDimensions);
  }

  updateWindowDimensions = () => {
    this.setState({ width: window.innerWidth });
  };

  componentWillReceiveProps(props) {
    let selectedTab = "";
    let { location, loggedUser } = props;
    let isNotValid = isNotUserValid(location.pathname, loggedUser);
    if (isNotValid.status) {
      this.props.history.push(isNotValid.redirectTo);
    }
    if (
      props.loggedUser.data.role === CONFIG.ADMIN ||
      props.loggedUser.data.role === CONFIG.GUEST
    ) {
      selectedTab = "PendingAdmin";
    } else if (props.loggedUser.data.role === CONFIG.HR) {
      selectedTab = "Pending";
    }
    if (!isEqual(props.listLeaves.all_leaves, this.state.all_leaves)) {
      let tab = localStorage.getItem("activeTab");
      if (!isEmpty(tab)) {
        selectedTab = tab;
      }
      this.setState(
        {
          all_leaves: props.listLeaves.all_leaves,
          selectedTab: selectedTab
        },
        () => {
          this.filterLeaveList(selectedTab);
        }
      );
    }
  }
  componentDidUpdate(prevProps, prevState) {
    if (
      this.leaveDetailsRef.current &&
      this.leaveDetailsRef.current.clientHeight !== this.state.listLeavesHeight
    ) {
      this.setState({
        listLeavesHeight: this.leaveDetailsRef.current.clientHeight
      });
    }
  }
  doLeaveStatusChange(id, newstatus, messagetouser) {
    this.props.onChangeLeaveStatus(id, newstatus, messagetouser);
  }
  selectLeave(leaveId) {
    if (leaveId !== this.state.selectedLeave.id) {
      var select = find(this.state.leaveListItems, { id: leaveId });
      this.setState({
        selectedLeave: select
      });
    }
  }

  filterLeaveList(activeTab) {
    var all_leaves = this.state.all_leaves;
    let newLeavesList;
    if (activeTab === "Pending") {
      newLeavesList = filter(all_leaves, function(o) {
        return o.status === "Pending" && parseInt(o.hr_approved) === 0;
      });
    } else if (activeTab === "ApprovedByHr") {
      newLeavesList = filter(all_leaves, function(o) {
        return o.status === "Pending" && parseInt(o.hr_approved) === 1;
      });
    } else if (activeTab === "PendingAdmin") {
      newLeavesList = filter(all_leaves, function(o) {
        return (
          o.status === "Pending" &&
          (parseInt(o.hr_approved) === 2 ||
            parseInt(o.hr_approved) === 0 ||
            parseInt(o.hr_approved) === 1)
        );
      });
    } else {
      newLeavesList = filter(all_leaves, { status: activeTab });
    }

    var selectedLeave = newLeavesList[0] || {};
    if (!isEmpty(this.state.selectedLeave)) {
      var select = filter(newLeavesList, { id: this.state.selectedLeave.id });
      if (size(select) > 0) {
        selectedLeave = select[0];
      }
    }

    this.setState({
      loading: false,
      leaveListItems: newLeavesList,
      selectedLeave: selectedLeave,
      selectedTab: activeTab
    });
    localStorage.setItem("activeTab", activeTab);
  }
  render() {
    const { width,listLeavesHeight } = this.state;
    let styles = cloneDeep(this.constructor.styles);
    let status_message = "";
    if (this.props.manageLeave.status_message !== "") {
      status_message = (
        <span className="label label-lg primary pos-rlt m-r-xs">
          <b className="arrow left b-primary" />
          {this.props.manageLeave.status_message}
        </span>
      );
    }

    let tabContent;
    if (
      !this.state.loading &&
      (isEmpty(this.state.selectedLeave) || isEmpty(this.state.leaveListItems))
    ) {
      tabContent = (
        <div
          className="row-col row-col-xs b-b"
          id="no_manage_leave"
          style={styles.spinContainer}
        >
          <span className="" style={styles.spiner}>
            No data found
          </span>
        </div>
      );
    } else if (
      !this.state.loading &&
      (!isEmpty(this.state.selectedLeave) &&
        !isEmpty(this.state.leaveListItems))
    ) {
      tabContent = (
        <div className="row no-gutter " id="manage_leave_data">
          <div className="left-value-slide col-sm-12 col-md-3" >
            <ListLeaves
              listItems={this.state.leaveListItems}
              selectedLeave={this.state.selectedLeave}
              selectLeave={this.selectLeave}
              height={listLeavesHeight}
              {...this.props}
            />
          </div>
          <div
            ref={this.leaveDetailsRef}
            id="leaveDetails"
            className={`${this.state.selectedTab} leave-detail-border col-sm-12 col-md-9  bg-white`}
          >
            <ViewLeave
              selectedLeave={this.state.selectedLeave}
              doLeaveStatusChange={this.doLeaveStatusChange}
              {...this.props}
            />
          </div>
        </div>
      );
    } else if (this.state.loading) {
      tabContent = (
        <div className="row-col row-col-xs b-b" style={styles.spinContainer}>
          <i
            className="fa fa-spinner fa-pulse"
            style={styles.spiner}
            aria-hidden="true"
          />
        </div>
      );
    }
    return (
      <div className="manage-leave-conatiner"> 
        <div className="manage-header ">
          <LeaveColorReference
            filterLeaveList={this.filterLeaveList}
            selectedTab={this.state.selectedTab}
            userRole={this.props.loggedUser.data.role}
            windowInnerwidth={width}
          />
        </div>
        <div
          id="content"
          className="app-content box-shadow-z0 manage-leaves"
          role="main"
        >
          <Header
            pageTitle={"Manage Leaves" + status_message}
            showLoading={this.props.frontend.show_loading}
          />
          <div className="app-body mt-3" id="view">
            <Widget className="back-widget p-0 ">
              <div className="">{tabContent}</div>
            </Widget>
          </div>
        </div>
      </div>
    );
  }
}

ManageLeaves.styles = {
  spinContainer: {
    textAlign: "center",
    fontSize: "50px",
    color: "#808080"
  },
  spiner: {
    margin: "50px auto"
  }
};
function mapStateToProps(state) {
  return {
    frontend: state.frontend.toJS(),
    loggedUser: state.logged_user.userLogin,
    listLeaves: state.listLeaves.toJS(),
    manageLeave: state.manageLeave.toJS()
  };
}
const mapDispatchToProps = dispatch => {
  return {
    onListLeaves: role => {
      return dispatch(actions_listLeaves.getAllLeaves(role));
    },
    onAddDescription: (leaveid, hr, data) => {
      return dispatch(actions_manageLeave.onAddDescription(leaveid, hr, data));
    },
    onAddExtraDay: (leaveid, token, data) => {
      return dispatch(actions_manageLeave.onAddExtraDay(leaveid, token, data));
    },
    onChangeLeaveStatus: (leaveid, newstatus, messagetouser) => {
      return dispatch(
        actions_manageLeave.changeLeaveStatus(leaveid, newstatus, messagetouser)
      );
    },
    onDocRequired: (leaveid, data, comment) => {
      return dispatch(actions_manageLeave.docRequired(leaveid, data, comment));
    },
    leaveRevertRequest: (leaveid, token) =>
      dispatch(actions_manageLeave.leaveRevertRequest(leaveid, token)),
      showHeading:(data)=>{
        return dispatch(actions.showHeading(data));
      }
  };
};

const VisibleManageLeaves = connect(
  mapStateToProps,
  mapDispatchToProps
)(ManageLeaves);

const RouterVisibleManageLeaves = withRouter(VisibleManageLeaves);

export default RouterVisibleManageLeaves;
