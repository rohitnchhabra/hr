import React from "react";
import { Alert } from "reactstrap";
import { connect } from "react-redux";
import { withRouter } from "react-router";
import * as _ from "lodash";
import { isNotUserValid } from "../../../services/generic";
import { CONFIG } from "../../../config/index";
import UserPicHeadTitle from "../../../components/generic/UserPicHeadTitle";
import UsersListHeader from "../../../components/generic/UsersListHeader";
import ApplyLeaveForm from "../../../modules/leave/components/applyLeave/ApplyLeaveForm";
import * as actions from "../../../redux/actions";
import * as actions_usersList from "../../../redux/generic/actions/usersList";
import * as actions_apply_leave from "../../../redux/leave/actions/applyLeave";
import * as actions_manageLeave from "../../../redux/leave/actions/manageLeave";
import { getLocationId } from "../../../helper/helper";
import AlertFader from "../../../components/generic/AlertFader"

class ApplyLeave extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      defaultUserDisplay: "",
      selected_user_name: "",
      selected_user_image: "",
      selected_user_jobtitle: "",
      selected_user_id: "",
      show_status_message: true,
      showMessage:{message : '',type:''},
      leaveId:"",
      file_showMessage:"",
      doc_open:false
    };
    this.onUserClick = this.onUserClick.bind(this);
    this.doApplyLeave = this.doApplyLeave.bind(this);
  }

  componentWillMount(){
    let isNotValid = isNotUserValid(
      this.props.location.pathname,
      this.props.loggedUser
    );
    if (isNotValid.status) {
      this.props.history.push(isNotValid.redirectTo);
    }
    const {
      location: { pathname }
    } = this.props;
    if (
      (this.props.loggedUser.data.role === CONFIG.ADMIN ||
      this.props.loggedUser.data.role === CONFIG['HR Payroll Manager'] ||
      this.props.loggedUser.data.role === CONFIG.HR) && getLocationId(pathname.split("/")[pathname.split("/").length - 1])
    ){
      this.props.onUsersList();
    }
    this.onUserClick(
      this.props.location.search.split("=")[
        this.props.location.search.split("=").length - 1
      ]
    );
    this.props.showHeading("Apply Leave")
  }
  componentWillReceiveProps(props) {
    // window.scrollTo(0, 0);
    let isNotValid = isNotUserValid(
      this.props.location.pathname,
      props.loggedUser
    );
    if (isNotValid.status) {
      this.props.history.push(isNotValid.redirectTo);
    }
    const {
      location: { pathname }
    } = this.props;
    if (
      this.props.loggedUser.data.role!==props.loggedUser.data.role && 
      (props.loggedUser.data.role === CONFIG.ADMIN ||
        props.loggedUser.data.role === CONFIG.HR) && getLocationId(pathname.split("/")[pathname.split("/").length - 1])
    ) {
      if (this.state.defaultUserDisplay === "") {
        props.onUsersList();
      }
    }
    if (this.props.location.search !== props.location.search) {
      this.onUserClick(
        props.location.search.split("=")[
          props.location.search.split("=").length - 1
        ]
      );
    }
  }
  componentDidUpdate(prevProps) {
    const {
      location: { pathname }
    } = this.props;  
    if (
      (this.props.loggedUser.data.role === CONFIG.ADMIN ||
      this.props.loggedUser.data.role === CONFIG.HR) && getLocationId(pathname.split("/")[pathname.split("/").length - 1]) 
    ) {
      if (this.state.defaultUserDisplay === "" || this.state.defaultUserDisplay != this.props.userSelected.userId) {
        if (this.props.usersList.users.length > 0) {
          const userId =
            this.props.userSelected && this.props.userSelected.userId
              ? this.props.userSelected.userId
              : this.props.location.search &&
                this.props.location.search.split("=")[
                  this.props.location.search.split("=").length - 1
                ];
          // if(userId)
          let firstUser = this.props.usersList.users[0];
          let defaultUserId = firstUser.user_Id;
          this.onUserClick(`${userId ? userId : defaultUserId}`);
        }
      }
    }
    if(prevProps.applyLeave.status_message!==this.props.applyLeave.status_message){
      this.setState({
        showMessage:this.props.applyLeave.status_message
      },()=>{
        this.setState({
          showMessage:{}
        })
      })
    }
  }
  onUserClick = userid => {
    const {status} = this.props.userSelected
    this.props.onSelectedUser({userid,status:!status});
    let selected_user_name = "";
    let selected_user_image = "";
    let selected_user_jobtitle = "";
    let selected_user_id = "";

    if (this.props.usersList.users.length > 0) {
      let userDetails = _.find(this.props.usersList.users, { user_Id: userid });
      if (typeof userDetails !== "undefined") {
        selected_user_name = userDetails.name;
        selected_user_image = userDetails.image;
        selected_user_jobtitle = userDetails.jobtitle;
        selected_user_id = userDetails.user_Id;
        this.props.history.push("apply_emp_leave?selectedUser=" + userid);
      }
      this.setState({
        defaultUserDisplay: userid,
        selected_user_name: selected_user_name,
        selected_user_image: selected_user_image,
        selected_user_jobtitle: selected_user_jobtitle,
        selected_user_id: selected_user_id,
        show_status_message: false
      });
    }
  };
  showSuccessMessage = () => {
    const showMessage = {message : "Document is uploaded.." , type:'success'}
        this.setState({showMessage},()=>{
          this.setState({showMessage:{}})
        })
  }
  doApplyLeave(
    start,
    end,
    days,
    reason,
    userid,
    day_status,
    leaveType,
    late_reason,
    doc_link,
    rh_dates
  ) {
    this.setState({ show_status_message: true });
    this.props
      .onApplyLeave(
        start,
        end,
        days,
        reason,
        userid,
        day_status,
        leaveType,
        late_reason,
        doc_link,
        rh_dates
      )
      .then(data => {
        const showMessage = {message : data.message , type:'success'}
        window.scrollTo(0,0);
        this.setState({showMessage},()=>{
          this.setState({showMessage:{},leaveId:data.leave_id,doc_open:true})
        })
      })
      .catch(error => {
        const showMessage = {message : error.message?error.message:error , type:'danger'};
        window.scrollTo(0,0);
        this.setState({showMessage},()=>{
          this.setState({showMessage:{}})
        })
      });
  }
  onClose = ()=>{
    this.setState({doc_open:false})
  }

  render() {
    const {
      location: { pathname }
    } = this.props;
    let status_message = "";
    if (
      this.props.applyLeave.status_message !== "" &&
      this.state.show_status_message === true
    ) {
      status_message = (
        <span
          className="well"
          style={{ background: "#60cffa", padding: "5px", marginLeft: "8px" }}
        >
          {this.props.applyLeave.status_message}
        </span>
      );
    }
    let mainDivs =
      (this.props.loggedUser.data.role === CONFIG.ADMIN ||
        this.props.loggedUser.data.role === CONFIG['HR Payroll Manager'] ||
        this.props.loggedUser.data.role === CONFIG.HR) &&
      getLocationId(pathname.split("/")[pathname.split("/").length - 1]) ? (
        <div className="row">
          {/* <div className="col-sm-3 hidden-xs" id="fixedScroll">
          <UsersList users={this.props.usersList.users} selectedUserId={this.state.selected_user_id} onUserClick={this.onUserClick} props={this.props} top={10} />
        </div> */}
          <div className="col">
            <UserPicHeadTitle
              profileImage={this.state.selected_user_image}
              name={this.state.selected_user_name}
              jobTitle={this.state.selected_user_jobtitle}
            />
            {/* <Widget bodyClass="mt-0">
              <div className="widget-top-overflow widget-padding-md clearfix bg-warning text-white">
                <h3 className="mt-lg mb-lg">
                  <span className="fw-semi-bold">
                    {this.state.selected_user_name}&nbsp;
                  </span>
                  <span className='apply_jobtitle'>{this.state.selected_user_jobtitle}</span>
                </h3>
              </div>
              <div
                className="post-user mt-negative-lg"
                style={{ position: "relative" }}
              >
                <span className="thumb-lg pull-left mr mt-n-sm">
                  <img
                    className="rounded-circle"
                    src={
                      this.state.selected_user_image
                        ? this.state.selected_user_image
                        : a5
                    }
                    alt="..."
                  />
                </span>
              </div>
            </Widget> */}

            {/* <div className="box">
            <div className="p-a text-center">
              <a href="" className="text-md m-t block">{this.state.selected_user_name}</a>
              <p>
                <small>{this.state.selected_user_jobtitle}</small>
              </p>
            </div>
          </div> */}
            <div className="box">
              <div className="box-body">
                <ApplyLeaveForm
                  forAdmin
                  doApplyLeave={this.doApplyLeave}
                  selectedUserId={this.state.selected_user_id}
                  leaveId={this.state.leaveId}
                  showSuccessMessage={this.showSuccessMessage}
                  onClose={this.onClose}
                  doc_open={this.state.doc_open}
                  {...this.props}
                />
              </div>
            </div>
          </div>
        </div>
      ) : (
        <div className="box">
          <div className="box-body">
            <ApplyLeaveForm
              doApplyLeave={this.doApplyLeave}
              forAdmin={false}
              leaveId={this.state.leaveId}
              showSuccessMessage={this.showSuccessMessage}
              onClose={this.onClose}
              doc_open={this.state.doc_open}
              {...this.props}
            />
          </div>
        </div>
      ); //ApplyLeaveForm

    return (
      <div className="apply-leave-container">
        <div id="content" className="app-content box-shadow-z0" role="main">
          <AlertFader>
            {this.state.showMessage.message && (
              <Alert
                className="alert-transparent rem-fade rh-section rh-alert"
                color={this.state.showMessage.type}
              >
                <span className="alert-bold">
                  {this.state.showMessage.type === "success"
                    ? "INFO"
                    : "ALERT"}
                </span>
                : <span>{this.state.showMessage.message}</span>
              </Alert>
            )}
          </AlertFader>
          <UsersListHeader
            users={this.props.usersList.users}
            selectedUserId={this.state.selected_user_id}
            onUserClick={this.onUserClick}
          />
          {mainDivs} 
        </div>
      </div>
    );
  }
}

ApplyLeave.styles = {
  height100per: {
    minHeight: "150px"
  }
};

function mapStateToProps(state) {
  return {
    frontend: state.frontend.toJS(),
    loggedUser: state.logged_user.userLogin,
    usersList: state.usersList.toJS(),
    applyLeave: state.applyLeave.toJS(),
    userSelected: state.userListSidebar
  };
}
const mapDispatchToProps = dispatch => {
  return {
    onApplyLeave: (
      from_date,
      to_date,
      no_of_days,
      reason,
      userId,
      day_status,
      leaveType,
      late_reason,
      doc_link,
      rh_dates
    ) => {
      return dispatch(
        actions_apply_leave.apply_leave(
          from_date,
          to_date,
          no_of_days,
          reason,
          userId,
          day_status,
          leaveType,
          late_reason,
          doc_link,
          rh_dates
        )
      );
    },
    onDaysBetweenLeaves: (startDate, endDate) => {
      return dispatch(
        actions_apply_leave.getDaysBetweenLeaves(startDate, endDate)
      );
    },
    onUsersList: () => {
      return dispatch(actions_usersList.get_users_list());
    },
    onDocRequired: (leaveid, data, comment) => {
      return dispatch(actions_manageLeave.docRequired(leaveid, data, comment));
    },
    onSelectedUser: userId => {
      return dispatch(actions.selectedUser(userId));
    },
    getUserRhListCompensation:data=>{
      return dispatch(actions_apply_leave.getUserRhListCompensation(data));
    },
    showHeading:(data)=>{
      return dispatch(actions.showHeading(data))
    }
   };
};

const VisibleApplyLeave = connect(
  mapStateToProps,
  mapDispatchToProps
)(ApplyLeave);

const RouterVisibleApplyLeave = withRouter(VisibleApplyLeave);

export default RouterVisibleApplyLeave;
