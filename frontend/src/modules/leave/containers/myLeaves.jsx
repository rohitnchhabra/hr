import React from "react";
import { connect } from "react-redux";
import { withRouter } from "react-router";
import { isNotUserValid } from "../../../services/generic";
import UserLeavesList from "../../../modules/leave/components/myLeaves/UserLeavesList";
import * as actions_myLeaves from "../../../redux/leave/actions/myLeaves";
import * as actions_salary from "../../../redux/salary/actions/viewSalary";
import { Alert } from "reactstrap";
import AlertFader from "../../../components/generic/AlertFader";
import RHLeaves from "../components/RHLeaves/RHLeaves";
import { getYearArray } from "../../../services/generic";
import { confirm, notify } from "../../../services/notify";
import ApplyRHModel from "../../../components/leave/RHLeaveList/ApplyModal";
import * as actions_apply_leave from "../../../redux/leave/actions/applyLeave";
import LeaveDetail from "../components/RHLeaves/LeaveDetail";
import Heading from "../../../components/generic/Heading";
import * as actions from '../../../redux/actions';
import GenericTable from "../../../components/generic/table/Table";

const myLeaveListTable = (listItem, onChange, rowStyle, index, extraProps) => {
  const monthNames = [
    "Jan",
    "Feb",
    "Mar",
    "Apr",
    "May",
    "Jun",
    "Jul",
    "Aug",
    "Sep",
    "Oct",
    "Nov",
    "Dec"
  ];
  return (
    <>
      <td>
        {monthNames[Number(listItem.month-1)]}
        {", " + listItem.year}
      </td>
      <td>{listItem.total_leave_taken}</td>
      <td>{listItem.leave_balance}</td>
      <td>{listItem.allocated_leaves}</td>
      <td>{listItem.paid_leaves}</td>
      <td>{listItem.unpaid_leaves}</td>
      <td>{listItem.final_leave_balance}</td>
    </>
  );
};
class MyLeaves extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      year: "",
      showModal: false,
      inputValue: "",
      showerror: false,
      reCall: true,
      showMessage:"",
      height:"",
      LeaveStatus:""
    };
  }
  componentWillMount() {
    this.props.onMyLeavesList();
    this.year = getYearArray();
    this.props.onSalaryDetails();

    this.setState({ year: `${this.year[3]}` });
    this.props.showHeading("My Leaves")
  }
  componentDidUpdate() {
    if(this.state.showMessage) {
      this.props.onMyLeavesList();
      this.setState({
        showMessage:""
      })
    }
  }
  componentWillReceiveProps(props) {
    const { id } = this.props.loggedUser.data;
    window.scrollTo(0, 0);
    let isNotValid = isNotUserValid(
      this.props.location.pathname,
      props.loggedUser
    );
    if (isNotValid.status) {
      this.props.history.push(isNotValid.redirectTo);
    }
    if (id && this.state.reCall) {
      this.props.getRHList(this.year[3], id);
      this.props.getRHStatus(this.year[3], id);
      this.setState({
        reCall: false
      });
    }
  }
  showSuccessMessage = () => {
    this.setState({showMessage:"Document is uploaded.."})
  }
  handleYearChange = e => {
    const { id } = this.props.loggedUser.data;
    this.setState({ year: e.target.value });
    this.props.getRHList(e.target.value, id);
    this.props.getRHStatus(e.target.value, id);
  };

  handleApplyClick = leave => {
    this.setState({
      currentRH: leave,
      showModal: true
    });
  };
  onApplyRHLeave = () => {
    const { id } = this.props.loggedUser.data;
    const { type_text, raw_date } = this.state.currentRH;
    this.props
      .onApplyLeave(
        raw_date,
        raw_date,
        1,
        this.state.inputValue,
        "",
        "",
        type_text
      )
      .then(() => {
        this.props.onMyLeavesList();
        this.props.getRHList(this.year[3], id);
        this.props.getRHStatus(this.year[3], id);
        confirm("RH is Successfully Applied", "", "success");
      })
      .catch(message => {
        this.setState({
          LeaveStatus:{
            message:message.message,
            err:1
          }
        },()=>this.setState({
          LeaveStatus:""
        }))
        
      });
    if (this.state.inputValue) {
      this.setState({
        showModal: false,
        inputValue: "",
        showError: false
      });
    } else {
      this.setState({
        showError: true
      });
    }
  };
  onInputChange = e => {
    this.setState({
      inputValue: e.target.value
    });
  };
  setHeight = height => {
    this.setState({
      height:height
    })
  }
  showLeaveStatus=(data)=>{
    this.setState({
      LeaveStatus:data
    },()=>this.setState({
      LeaveStatus:""
    }))
  }

  render() {
    return (
      <div className="my-leave-view">
        <AlertFader>
              {this.state.showMessage ? (
                <Alert
                  className="alert-transparent rem-fade rh-section rh-alert"
                  color={"success"}
                >
                  {this.state.showMessage}
                </Alert>
              ) : null}
              {this.state.LeaveStatus.message ? (
                <Alert
                  className="alert-transparent rem-fade rh-section rh-alert"
                  color={this.state.LeaveStatus.err?"danger":"success"}
                >
                  {this.state.LeaveStatus.message}
                </Alert>
              ) : null}
            </AlertFader>
        <div id="content" className="app-content box-shadow-z0" role="main">
          <ApplyRHModel
            show={this.state.showModal}
            handleHide={() => {
              this.setState({
                showModal: false,
                inputValue: "",
                showError: false
              });
            }}
            onApplyRHLeave={this.onApplyRHLeave}
            inputChange={this.onInputChange}
            inputValue={this.state.inputValue}
            stateData={this.state}
          />
          <div className="app-body" id="view" className="myleave-section">
            <div className="row">
              <LeaveDetail
                RHStatus={this.props.RHLeaveList.RHStatus}
                leaveStaus={this.props.salary}
              />
              <div className="col-md-6">
                <UserLeavesList {...this.props} showSuccessMessage={this.showSuccessMessage} height={this.state.height} showLeaveStatus={this.showLeaveStatus} />
              </div>
              <div className="col-md-6 rh-leave-container">
                <RHLeaves
                  stateData={this.state}
                  yearArray={this.year}
                  handleYearChange={this.handleYearChange}
                  RHLeaveList={this.props.RHLeaveList.RHLeaves.rh_list}
                  RHStatus={this.props.RHLeaveList.RHStatus}
                  handleApplyClick={this.handleApplyClick}
                  setHeight={this.setHeight}
                />
              </div>
              <div className="col-md-12 my-leave-table pt-4" id ="table-view-myleave">
                <GenericTable
                  list={myLeaveListTable}
                  headerData={[
                    "month",
                    "total leave taken",
                    "leave balance",
                    "alloacted leaves",
                    "paid leaves",
                    "unpaid leaves",
                    "final leave balance"
                  ]}
                  tableListData={
                    this.props.salary && this.props.salary.payslip_history
                  }
                  className={"myleavetable"}
                />
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    frontend: state.frontend.toJS(),
    loggedUser: state.logged_user.userLogin,
    userLeaves: state.userLeaves.toJS(),
    applyLeave: state.applyLeave.toJS(),
    salary: state.salary.toJS(),
    RHLeaveList: state.userLeaves.toJS()
  };
}
const mapDispatchToProps = dispatch => {
  return {
    onMyLeavesList: () => {
      return dispatch(actions_myLeaves.getMyLeaves());
    },
    onApplyLeave: (
      from_date,
      to_date,
      no_of_days,
      reason,
      userId,
      day_status,
      leaveType,
      late_reason
    ) => {
      return dispatch(
        actions_apply_leave.apply_leave(
          from_date,
          to_date,
          no_of_days,
          reason,
          userId,
          day_status,
          leaveType,
          late_reason
        )
      );
    },
    onCancelLeave: (userId, from_date) => {
      return dispatch(actions_myLeaves.cancelLeave(userId, from_date));
    },
    getRHList: (year, id) => dispatch(actions_myLeaves.getRHList(year, id)),
    getRHStatus: (year, id) => dispatch(actions_myLeaves.getRHStatus(year, id)),
    onSalaryDetails: () => {
      return dispatch(actions_salary.getSalaryDetails());
    },
    showHeading:(data)=>{
      return dispatch(actions.showHeading(data));
    }
  };
};

const VisibleMyLeaves = connect(
  mapStateToProps,
  mapDispatchToProps
)(MyLeaves);

const RouterVisibleMyLeaves = withRouter(VisibleMyLeaves);

export default RouterVisibleMyLeaves;
