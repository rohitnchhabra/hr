import React from "react";
import { connect } from "react-redux";
import { withRouter } from "react-router";

import RHStatList from "../../../components/leave/rhStats/rhStatList";
import { getYearArray } from "../../../services/generic";
import * as actions_RhStats from "../../../redux/leave/actions/rh_stats";
import * as actions from '../../../redux/actions'
import Heading from "../../../components/generic/Heading";

class RHStats extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      year: getYearArray()[3],
      showerror: false,
      reCall: true
    };
  }

  componentWillMount() {
    this.props.onRHStatRequest();
    //get an array of year
    //this array contain last 2 year and current year
    let date = new Date();
    let currentYear = date.getFullYear();
    this.year = [];
    for (let i = 0; i < 3; i++) {
      this.year.push(parseInt(currentYear) + i - 2);
    }
    this.props.showHeading("RH Stats")
  }

  handleYearChange = e => {
    const { id } = this.props.loggedUser.data;
    this.setState({ year: e.target.value });
    this.props.onRHStatRequest(e.target.value);
  };

  render() {
    return (
      <div>
        <div id="content" className="app-content box-shadow-z0" role="main">
          <div className="row no-gutters mb-4">
            <div className="d-flex col-md-2 col-6 align-items-center">
              <div className="px-2">Year:</div>
              <div>
                <select
                  className="form-control"
                  ref="year_holidays"
                  onChange={e => {
                    this.handleYearChange(e);
                  }}
                  value={this.state.year}
                  style={{ minHeight: "0" }}
                  disabled={this.props.frontend.show_loading ? true : false}
                >
                  {this.year &&
                    this.year.map((data, index) => (
                      <option key={index} value={data}>
                        {data}
                      </option>
                    ))}
                </select>
              </div>
            </div>
          </div>

          <div className="app-body" id="view">
            <div className="col-md-12 pl-0 pr-0">
              <RHStatList
                // handleYearChange={this.handleYearChange}
                rhStatsList={this.props.rhStats.data}
                stateData={this.state}
                // yearArray={this.year}
                // isRHLoading={this.props.frontend.show_loading}
              />
            </div>
          </div>
        </div>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    frontend: state.frontend.toJS(),
    loggedUser: state.logged_user.userLogin,
    rhStats: state.rhStats.toJS()
  };
}
const mapDispatchToProps = dispatch => {
  return {
    onRHStatRequest: year => {
      return dispatch(actions_RhStats.getRHStats(year));
    },
    showHeading:(data)=>{
      return dispatch(actions.showHeading(data));
    }
  };
};

const VisibleMyRHStats = connect(
  mapStateToProps,
  mapDispatchToProps
)(RHStats);

const RouterVisibleMyRHStats = withRouter(VisibleMyRHStats);

export default RouterVisibleMyRHStats;
