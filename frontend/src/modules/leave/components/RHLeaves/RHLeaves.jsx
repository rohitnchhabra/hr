import React from "react";
import * as _ from "lodash";
import RHLeavesList from "../../../../components/leave/RHLeaveList/RHLeaveList";
class RHLeaves extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      display_height:""
    }
  }

  displayHeight=(display_size)=>{
    this.setState({display_size})
    this.props.setHeight(display_size)
  }
  
  render() {
    let length = this.props.RHLeaveList && this.props.RHLeaveList.length;
    var style = {
      height:this.state.display_size
    };
    const { yearArray, RHLeaveList, handleApplyClick, RHStatus } = this.props;
    let img = (
      <img
        src="./socialMediaIcons/holidays.svg"
        className="w-40 img-circle m-x-md"
      />
    );
    
    return (
      <div className="row rh-leaves">
        <div className="rh-wrapper">
       
          <div className="col-md-12 restrictedHolidayList">
            <div className="rh-leave"> RESTRICTED HOLIDAY LIST</div>
              <div className="list white scroll inset" style={style} >
              {RHLeaveList ? (
                _.map(this.props.RHLeaveList, (leave, key) => {
                  return (
                    <RHLeavesList
                      key={key}
                      leave={leave}
                      handleApply={this.handleApply}
                      cancelLeave={this.cancelLeave}
                      handleApplyClick={handleApplyClick}
                      displayHeight={this.displayHeight}
                      length={length}
                    />
                  );
                })
              ) : (
                <h2>{img} No RH This Year.</h2>
              )}
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default RHLeaves;
