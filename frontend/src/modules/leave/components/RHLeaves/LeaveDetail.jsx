import React, { Component } from "react";
import $ from "jquery";
import { CircularProgressbar } from "react-circular-progressbar";
import BarChart from "../../../../components/generic/charts/BarsChart";
import moment from "moment";
import "react-circular-progressbar/dist/styles.css";
import Heading from "../../../../components/generic/Heading";
import cloneDeep from "lodash/cloneDeep";
const percentage = 66;
export default class LeaveDetail extends Component {

  getData = () => { 
    
    var date = new Date();
    date.setFullYear(date.getFullYear() - 1);
    date.setHours(0,0,0,0)

    var result = this.props.leaveStaus.payslip_history.filter( o=> new Date(o.year + "-" + o.month) >= date);

    let Chartdata = [[], [], [], [],[]];
    if (
      result &&
      result.length
    ) {
      result.forEach((item, index) => {
        var newDate = moment.utc();
        newDate.set("year", item.year);
        newDate.set("month", item.month-1);
        newDate.set("date", 1);
        newDate.startOf("day");

        Chartdata[0].push([
          newDate.format("x"),
          parseFloat(item.leave_balance)
        ]);
        Chartdata[1].push([newDate.format("x"), item.paid_leaves]);
        Chartdata[2].push([newDate.format("x"), item.unpaid_leaves]);
        Chartdata[3].push([newDate.format("x"), item.final_leave_balance]);
        Chartdata[4].push([newDate.format("x"), item.total_leave_taken]);
      });
      return Chartdata;
    }
  };
  getDataSeries(cdata) {
    // eslint-disable-line
    let data = cloneDeep(cdata);
    return [
      {
        label: "Leave Balance",
        data: data[0],
        bars: {
          show: true,
          barWidth: 12 * 24 * 60 * 60 * 300,
          fill: 0.5,
          lineWidth: 0,
          order: 1
        }
      },
      {
        label: "Paid Leaves",
        data: data[1],
        bars: {
          show: true,
          barWidth: 12 * 24 * 60 * 60 * 300,
          fill: 1,
          lineWidth: 0,
          order: 2
        }
      },
      {
        label: "Unpaid Leaves",
        data: data[2],
        bars: {
          show: true,
          barWidth: 12 * 24 * 60 * 60 * 300,
          fill: 0.5,
          lineWidth: 0,
          order: 3
        }
      },
      {
        label: "Final Leave Balance",
        data: data[3],
        bars: {
          show: true,
          barWidth: 12 * 24 * 60 * 60 * 300,
          fill: 0.5,
          lineWidth: 0,
          order: 4
        }
      },
      {
        label: "Total Leave Taken",
        data: data[4],
        bars: {
          show: true,
          barWidth: 12 * 24 * 60 * 60 * 300,
          fill: 0.5,
          lineWidth: 0,
          order: 5
        }
      }
    ];
  }

  render() {
    const y_axis = 5;
    const { payslip_history } = this.props.leaveStaus;
    const len = payslip_history.length;
    return (
      <div className="col-xs-12 leave-detail-container">
        <div className="statistics-leave">
              STATISTICS <b>MY LEAVES</b>
            </div>
            <div className="box pl-3 pt-3 my-4">
              {this.props.leaveStaus.payslip_history &&
              this.props.leaveStaus.payslip_history.length  > 3  ? (
                <BarChart getData={this.getData} getDataSeries={this.getDataSeries} y_axis_ticker={5} x_axis_length={1} />
              ) : null}
            </div>
        <div className="col-lg-6 col-xs-12 col-sm-12 left">
          <div className="col-md-9 col-xs-12 col-sm-12">
            
            <div id="first-border" className="leave_my">
              <div className="allocated-leave leaves">
                <span className="status">L</span>Allocated Leaves{" "}
                <span className="count">
                  {len && payslip_history[0].allocated_leaves}{"/ month"}
                </span>
              </div>
              <div className="leave-balance leaves">
                <span className="status">L</span>Leave Balance{" "}
                <span className="count">
                  {len && payslip_history[0].final_leave_balance}
                </span>
              </div>
              <div className="total-leave-taken leaves">
                <span className="status">L</span>Total Leaves Taken{" "}
                <span className="count">
                  {len && payslip_history[0].total_leave_taken}
                </span>
              </div>
              <div className="paid-leave-taken leaves">
                <span className="status">L</span>Paid Leaves Taken{" "}
                <span className="count">
                  {len && payslip_history[0].paid_leaves}
                </span>
              </div>
              <div className="unpaid-leave-taken leaves">
                <span className="status">L</span>Unpaid Leaves Taken{" "}
                <span className="count">
                  {len && payslip_history[0].unpaid_leaves}
                </span>
              </div>
            </div>
          </div>
          <div className="col-md-3 col-xs-12 col-sm-12" style={{ textAlign: "center" }}>
            <CircularProgressbar
              value={
                ((len && payslip_history[0].total_leave_taken) /
                  (Number(len && payslip_history[0].final_leave_balance) +
                    Number(len && payslip_history[0].total_leave_taken))) *
                100
              }
              text={
                (len && payslip_history[0].total_leave_taken) +
                "/" +
                (Number(len && payslip_history[0].final_leave_balance) +
                  Number(len && payslip_history[0].total_leave_taken))
              }
            />
            <div style={{ fontSize: "12px", fontWeight: "600" }}>
              Final Leave Balance
            </div>
          </div>
        </div>
        <div className="col-lg-6 col-xs-12 col-sm-12 right">
          <div className="col-md-9 col-xs-12 col-sm-12">
            <div className="statistics-rh">
              STATISTICS <b>RH</b>
            </div>
            <div id="second-border" className="leave_my">
              <div className="approved rh">
                <span className="status">A</span>RH Approved{" "}
                <span className="count">{this.props.RHStatus.rh_approved}</span>
              </div>
              <div className="cancelled rh">
                <span className="status">R</span>RH Cancelled{" "}
                <span className="count">{this.props.RHStatus.rh_rejected}</span>
              </div>
              <div className="quater rh">
                <span className="status">RH</span>RH Per Quater{" "}
                <span className="count">{this.props.RHStatus.rh_can_be_taken_this_quarter}</span>
              </div>
              <div className="apply rh">
                <span className="status">RH</span>Pending RH You Can Apply
                <span className="count">{this.props.RHStatus.rh_left}</span>
              </div>
              <div className="compensation rh">
                <span className="status">RH</span>Cancelled RH: Compensation Used{" "}
                <span className="count">
                  {this.props.RHStatus.rh_compensation_used}
                </span>
              </div>
              <div className="pending-compensation rh">
                <span className="status">L</span>Pending Cancelled RH As
                Compensation{" "}
                <span className="count">
                  {this.props.RHStatus.rh_compensation_pending}
                </span>
              </div>
            </div>
          </div>
          <div className="col-md-3 col-xs-12 col-sm-12 mb-sm-5" style={{ textAlign: "center" }}>
            <CircularProgressbar
              value={(this.props.RHStatus.rh_left / 5) * 100}
              text={this.props.RHStatus ? (this.props.RHStatus.rh_left ? this.props.RHStatus.rh_left : "0") + "/" + (this.props.RHStatus.rh_can_be_taken ? this.props.RHStatus.rh_can_be_taken : "0") : ""}
            />
            <div style={{ fontSize: "12px", fontWeight: "600" }}>
              {" "}
              Total RH Available
            </div>
          </div>
        </div>
      </div>
    );
  }
}
