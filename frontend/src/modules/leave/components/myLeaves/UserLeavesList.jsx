import React from 'react';
import * as _ from 'lodash';
import {notify,confirm} from '../../../../services/notify';
import {getToken} from '../../../../services/generic';
import MyLeavesList from '../../../../components/leave/myLeaves/MyLeavesList';

class UserLeavesList extends React.Component {
  constructor (props) {
    super(props);
    this.state = {
      id:         '',
      user_token: '',
      file:[],
      imageUrl:'',
      type: "",
      documentType: "",
      cancelLeaveStatus:""
    };
    this.cancelLeave = this.cancelLeave.bind(this);
    this.handleOpen = this.handleOpen.bind(this);
    this.callUpdateDocuments = this.callUpdateDocuments.bind(this);
  }

  componentWillReceiveProps (props) {
    this.setState({user_token: getToken()});
    
  }

  cancelLeave (userId, from_date) {
    confirm("Are you sure you want to cancel leave?").then(isConfirm => {
      if (isConfirm) {
        this.props.onCancelLeave(userId, from_date).then((data) => {
          this.setState({
            cancelLeaveStatus:{
              err:0,
              message:data
            }
          },()=>this.props.showLeaveStatus(this.state.cancelLeaveStatus))
        }).catch((err) => {
          this.setState({
            cancelLeaveStatus:{
              err:1,
              message:err
            }
          },()=>this.props.showLeaveStatus(this.state.cancelLeaveStatus))
        });
      }

    })
    
    }

    handleFileChange = files => {
      this.setState({
        file: files,
        type:
          files[0].name &&
          files[0].name.split(".")[files[0].name.split(".").length - 1],
        documentType: "leave_doc"
      });
      const reader = new FileReader();
  
      reader.onloadend = () => {
        this.setState({
          imageUrl: reader.result
        });
      };
      if (files) {
        reader.readAsDataURL(files[0]);
        this.setState({
          imageUrl: reader.result
        });
      } else {
        this.setState({
          imageUrl: ""
        });
      }
    };

    callUpdateDocuments = e => {
      let stop = false;
      if (!this.state.imageUrl) {
        stop = true;
        notify("Warning!", "Please select a file", "warning");
      }
      if (stop) {
        e.preventDefault();
      }
    };
    successfulFileUpload = () => {
      this.setState({
        imageUrl: "",
        file: []
      });
    }; 
    handleRomoveImage=()=>{
      this.setState({ imageUrl: "",file:[]})
    } 

  handleOpen (id) {
    this.setState({id: id});
  }

  callUpdateDocuments (e) {
    let docProof = this.refs.file.value;
    let stop = false;
    if (docProof === '') {
      stop = true;
      notify('Please select a file');
    }
    if (stop) {
      e.preventDefault();
    }
  }

  render () {
    let length = this.props.userLeaves.leaves && this.props.userLeaves.leaves.length
    let page_url = window.location.href;
  
    var style = {
      height:this.props.height
    };
    
    let leavesList = _.map(this.props.userLeaves.leaves, (leave, key) => {
      return <MyLeavesList key={key} length = {length}
       leave={leave} 
       handleOpen={this.handleOpen} 
       cancelLeave={this.cancelLeave} 
       display_height={this.display_height} 
       handleFileChange={this.handleFileChange}
       file={this.state.file}
       imageUrl={this.state.imageUrl}
       callUpdateDocuments={this.callUpdateDocuments}
       user_token={this.state.user_token}
       type={this.state.type}
       id={this.state.id}
       successfulFileUpload={this.successfulFileUpload}
       handleRomoveImage={this.handleRomoveImage}
       handleClose={this.handleClose}
       showSuccessMessage={this.props.showSuccessMessage}
       />;
    });
    return (
      <div className="row">
      <div className="row-col my-leaves-list">
        <div className="my-leave">MY LEAVES</div>
          <div className="list white scroll inset" style={style}> 
            {leavesList}
          </div>
        </div>
      </div>
    );
  }
}

export default UserLeavesList; 
