import React, { Component } from "react";
import PropTypes from "prop-types";
import moment from "moment";
import { Calendar, DateRange } from "react-date-range";
import { notify } from "../../../../services/notify";
import { Button, Alert } from "reactstrap";
import { getToken } from "../../../../services/generic";
import Dropzone from "react-dropzone";
import UploadImageComp from "../../../uploadImageCompressed/UploadImageComp";
import Select from "react-dropdown-select";

import AddLeaveDocument from "./AddLeaveDocument";

class ApplyLeaveForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      status_message: "",
      open: false,
      id: "",
      form_from_date: "",
      form_to_date: "",
      form_no_of_days: "",
      form_reason: "",
      day_status: "",
      show_half_day_button: "",
      leaveType: "",
      late_reason: "",
      doc_link: "",
      isLoading: false,
      active_link_first: false,
      active_link_second: false,
      file: [],
      type: "",
      documentType: "",
      imageUrl: "",
      user_token: "",
      showMessage: "",
      datesArr: [],
      applyLeaveYear: ""
    };
  }

  componentDidUpdate(props, prevState) {
    var token = getToken();
    if (token !== this.state.user_token) {
      this.setState({ user_token: getToken() });
    }

    // this.setState({user_token: getToken()});
    // window.scrollTo(0, 0);
    if (
      this.state.form_from_date != "" &&
      this.state.form_to_date != "" &&
      (prevState.form_from_date!==this.state.form_from_date || prevState.form_to_date!==this.state.form_to_date)
    ) {
      this.props
        .onDaysBetweenLeaves(this.state.form_from_date, this.state.form_to_date)
        .then(res => {
          let date = new Date(res.end_date);
          let year = date.getFullYear();
          this.setState({
            applyLeaveYear: year
          });
        })
        .catch(res => {});
    }

    if (
      this.state.leaveType == "RH Compensation" &&
      ((prevState.leaveType !== this.state.leaveType &&
        this.state.applyLeaveYear) ||
        prevState.applyLeaveYear !== this.state.applyLeaveYear ||
        (this.state.applyLeaveYear &&
          this.props.forAdmin &&
          this.props.selectedUserId !== props.selectedUserId))
    ) {
        this.props.getUserRhListCompensation({
        year:this.state.applyLeaveYear,
        user_id:this.props.forAdmin?this.props.selectedUserId:this.props.loggedUser.data.id
      })
    }

    if (this.props.userSelected.status !== props.userSelected.status) {
      this.props.onDaysBetweenLeaves(
        this.state.form_from_date,
        this.state.form_to_date
      );
    }
  }
  componentWillReceiveProps(props) {
    this.setState({ user_token: getToken() });
  }

  handleClose = e => {
    this.setState({
      open: false,
      status_message: ""
    });
  };
  handleOpen = e => {
    this.setState({
      status_message: "",
      open: true
    });
  };
  _apply_half_day_1 = shift => {
    if (shift == 1) {
      this.setState({
        form_no_of_days: "0.5",
        day_status: "1",
        active_link_first: true,
        active_link_second: false
      });
    } else if (shift == 2) {
      this.setState({
        form_no_of_days: "0.5",
        day_status: "2",
        active_link_first: false,
        active_link_second: true
      });
    }
  };

  handleStartDate = date => {
    let startDate = date.format("YYYY-MM-DD");
    this.setState({ form_from_date: startDate},()=>{
      if(this.state.form_to_date && (new Date(this.state.form_from_date) > new Date(this.state.form_to_date))){
        this.setState({
          form_to_date:""
        })
      }
    });
  };
  handleEndDate = date => {
    let endDate = date.format("YYYY-MM-DD");
    this.setState({ form_to_date: endDate});
  };

  handleFileChange = files => {
    this.setState({
      file: files,
      type:
        files[0].name &&
        files[0].name.split(".")[files[0].name.split(".").length - 1],
      documentType: "leave_doc"
    });
    const reader = new FileReader();

    reader.onloadend = () => {
      this.setState({
        imageUrl: reader.result
      });
    };
    if (files) {
      reader.readAsDataURL(files[0]);
      this.setState({
        imageUrl: reader.result
      });
    } else {
      this.setState({
        imageUrl: ""
      });
    }
  };
  callUpdateDocuments = e => {
    let stop = false;
    if (!this.state.imageUrl) {
      stop = true;
      notify("Warning!", "Please select a file", "warning");
    }
    if (stop) {
      e.preventDefault();
    }
  };

  successfulFileUpload = () => {
    this.props.onClose();
    this.setState({
      imageUrl: "",
      file: []
    });
  };
  handleRomoveImage = () => {
    this.setState({ imageUrl: "", file: [] });
  };

  doApplyLeave = evt => {
    var reason_letter_count = this.state.form_reason;
    let datesArr = this.state.datesArr;
    let rh_dates_arr = [];
    datesArr.forEach((val, i) => {
      rh_dates_arr.push(val.date);
    });
    if (this.props.forAdmin == true) {
      this.props.doApplyLeave(
        this.state.form_from_date,
        this.state.form_to_date,
        this.state.form_no_of_days,
        this.state.form_reason,
        this.props.selectedUserId,
        this.state.day_status,
        this.state.leaveType,
        this.state.late_reason,
        this.state.doc_link,
        rh_dates_arr
      );
      this.setState({
        form_from_date: "",
        form_to_date: "",
        form_no_of_days: "",
        form_reason: "",
        show_half_day_button: "",
        day_status: "",
        leaveType: "",
        late_reason: "",
        doc_link: ""
      });

      // notify("leave Applied");
    } else {
      this.props.doApplyLeave(
        this.state.form_from_date,
        this.state.form_to_date,
        this.state.form_no_of_days,
        this.state.form_reason,
        "",
        this.state.day_status,
        this.state.leaveType,
        this.state.late_reason,
        this.state.doc_link,
        rh_dates_arr
      );
      this.setState({
        form_from_date: "",
        form_to_date: "",
        form_no_of_days: "",
        form_reason: "",
        show_half_day_button: "",
        day_status: "",
        leaveType: "",
        late_reason: "",
        doc_link: ""
      });
    }
    evt.preventDefault();
  };
  componentWillReceiveProps(props) {
    if (this.props.applyLeave.status_message === "Leave applied.") {
      this.setState({ isLoading: false });
    }
    let num_working_days = "0";
    if (
      props.applyLeave.count_working_days != "" &&
      props.applyLeave.count_working_days != 0 &&
      this.props.applyLeave.count_working_days !==
        props.applyLeave.count_working_days
    ) {
      num_working_days = props.applyLeave.count_working_days;
      this.setState({
        form_no_of_days: num_working_days
      });
    }
  }
  rhCompensationList = () => {
    let options = [];
    this.props.applyLeave &&
      this.props.applyLeave.rh_list_compensation &&
      this.props.applyLeave.rh_list_compensation.forEach((value, i) => {
        options.push({
          value: value.name,
          label: value.name,
          date: value.raw_date
        });
      });
    return options;
  };
  render() {
    const {
      frontend: { show_loading }
    } = this.props;
    let dateDiff = moment(moment().format("YYYY-MM-DD")).diff(
      this.state.form_from_date || moment().format("YYYY-MM-DD"),
      "days"
    );
    let apply_half_day_buttons = "";
    if (this.state.form_no_of_days == 1 && this.state.leaveType!=="RH Compensation") {
      apply_half_day_buttons = (
        <>
          <Button
            color="link"
            className={
              this.state.active_link_first == true ? "first_half_link" : ""
            }
            onClick={() => this._apply_half_day_1(1)}
          >
            Apply Leave For First Half
          </Button>
          {" / "}
          <Button
            color="link"
            className={
              this.state.active_link_second == true ? "second_half_link" : ""
            }
            onClick={() => this._apply_half_day_1(2)}
          >
            Apply Leave For Second Half
          </Button>
        </>
      );
    }
    let width = "63%";
    if (this.props.forAdmin == true) {
      width = "82%";
    }

    return (
      <div>
        {!this.props.doc_open ? (
          <div className="row">
            <div className="col-lg-4 col-md-5 col-sm-12 mb-4 apply_leave_calendar">
              <div className="calendar-to-view text-center ">
                <h6 className="pb-2">FROM</h6>
                <div id="date-calendar">
                  <Calendar onChange={this.handleStartDate} />
                </div>
              </div>
              <div className="calendar-from-view col text-center">
                <h6 className="pb-2">TO</h6>
                <div id="date-calendar">
                  <Calendar
                    onChange={this.handleEndDate}
                    minDate={`${moment(
                      this.state.form_from_date
                        ? this.state.form_from_date
                        : new Date()
                    ).format("DD/MM/YYYY")}`}
                  />
                </div>
              </div>
            </div>
            <div className="col-lg-8 col-md-7 col-sm-12 pt-sm-4 pl-0">
              <h6 className="text-center pb-2">LEAVE SUMMARY</h6>

              <form role="form" onSubmit={this.doApplyLeave}>
                <div className="mr-5">
                  <div id="leave-summary">
                    {/* <div className=" text-muted ">
                      Your leave starts from
                    </div> */}
                    {/* <div className="apply-date-detail mb-4">
                  <table className="w-100">
                    <tr className="border-bottom">
                      <td className="pb-2" width="70">
                        From
                      </td>
                      <td className="pb-2">
                        {this.state.form_from_date
                          ? moment(this.state.form_from_date).format(
                              "D MMMM YYYY"
                            )
                          : null}
                      </td>
                    </tr>
                    <tr>
                      <td className="pt-2">To</td>
                      <td className="pt-2">
                        {this.state.form_to_date
                          ? moment(this.state.form_to_date).format(
                              "D MMMM YYYY"
                            )
                          : null}
                      </td>
                    </tr>
                  </table>
                </div> */}

                    <div className=" d-flex align-items-start mb-4">
                      <div className="w-25">From</div>
                      <div className="w-75">
                        <div className="from_date_value border">
                          {this.state.form_from_date
                            ? moment(this.state.form_from_date).format(
                                "D MMMM YYYY"
                              )
                            : null}
                        </div>
                      </div>
                    </div>

                    <div className=" d-flex align-items-start mb-4">
                      <div className="w-25">To</div>
                      <div className="w-75">
                        <div className="from_date_value border">
                          {this.state.form_to_date
                            ? moment(this.state.form_to_date).format(
                                "D MMMM YYYY"
                              )
                            : null}
                        </div>
                      </div>
                    </div>
                    <div className="d-flex mb-4">
                      <div className="w-25">Leave Type</div>
                      <div className="option-leave-type">
                        <select
                          value={this.state.leaveType}
                          onChange={e => {
                            this.setState({ leaveType: e.target.value });
                          }}
                          className="form-control"
                          required
                        >
                          <option value="" disabled>
                            Leave Option
                          </option>
                          <option value="Casual Leave"> Casual Leave </option>
                          <option value="Sick Leave"> Sick Leave </option>
                          <option value="RH Compensation">
                            {" "}
                            RH Compensation{" "}
                          </option>
                        </select>
                      </div>
                    </div>
                    {this.state.leaveType == "RH Compensation" &&
                      this.props.applyLeave &&
                      this.props.applyLeave.rh_list_compensation &&
                      this.props.applyLeave.rh_list_compensation.length > 0 && (
                        <div className="d-flex mb-4">
                          <div className="w-25">Rh Leave</div>
                          <div className="option-leave-type">
                            <Select
                              options={this.rhCompensationList()}
                              multi={true}
                              placeholder="Select Rh Leave"
                              Custom
                              color="#3abf94"
                              required={true}
                              onChange={values =>
                                this.setState({
                                  datesArr: values
                                })
                              }
                            />
                          </div>
                        </div>
                      )}
                    <div className=" d-flex align-items-start mb-4">
                      <div className="w-25">No. of Days</div>
                      <div className="w-75">
                        <div className="leave-number-days border">
                          {this.state.form_no_of_days
                            ? this.state.form_no_of_days
                            : 0}
                        </div>
                        <div className="text-primary">
                          {apply_half_day_buttons}
                        </div>
                      </div>
                    </div>

                    <div className="d-flex mb-4">
                      <div className="w-25">Reason</div>
                      <div className="leave-cause">
                        <textarea
                          className="form-control"
                          placeholder="Give reason"
                          ref="reason"
                          onChange={() =>
                            this.setState({
                              form_reason: this.refs.reason.value
                            })
                          }
                          value={this.state.form_reason}
                        />
                      </div>
                    </div>
                    {dateDiff > 0 ? (
                      <div className="d-flex mb-4">
                        <div className="w-25">Reason For Late Applying</div>
                        <div className="w-75">
                          <input
                            className="form-control"
                            type="text"
                            onChange={e =>
                              this.setState({ late_reason: e.target.value })
                            }
                            value={this.state.late_reason}
                            required
                          />
                        </div>
                      </div>
                    ) : null}
                    <div className="d-flex mb-4">
                      {/* <div className="w-25">Share Link</div> */}
                      <div className="leave-document">
                        {/* <textarea
                      className="form-control"
                      type="url"
                      placeholder="Send link of your document to support your leave"
                      pattern="http.*"
                      value={this.state.doc_link}
                      onChange={e => {
                        this.setState({ doc_link: e.target.value });
                      }}
                    /> */}
                        {/* <div className="doc_link_note text-secondary">* Upload your leave documents proof anywhere on cloud website like your company google drive account or dropbox. Put the link generated here</div> */}
                        <div className=" offset-md-3 text-center mt-3">
                          <Button
                            type="submit"
                            className="btn btn-success btn-block"
                            disabled={show_loading ? true : false}
                          >
                            {show_loading ? "Loading..." : "Apply"}
                          </Button>
                        </div>
                      </div>
                    </div>
                    <AddLeaveDocument
                      handleClose={this.handleClose}
                      open={this.state.open}
                    />
                  </div>
                </div>
              </form>
            </div>
          </div>
        ) : (
          <div>
            <Alert color="info" fade={false}>
              You can upload document optionally as well for faster leave
              approvals.
            </Alert>
            <div className="d-flex">
              <div className="w-25">Document Upload</div>
              <div className="m-auto pt-5 w-75">
                <Dropzone onDrop={this.handleFileChange}>
                  {({ getRootProps, getInputProps }) => {
                    return (
                      <section className="container m-0 p-0 col-sm-12">
                        <div {...getRootProps({ className: "dropzone" })}>
                          <input {...getInputProps()} />
                          <div className="drag_and_drop align-items-center">
                            {this.state.file &&
                              this.state.file.map((file, index) => (
                                <div className="font-weight-bold">
                                  {" "}
                                  {file.name}
                                </div>
                              ))}
                            {this.state.imageUrl ? (
                              <img
                                src={this.state.imageUrl}
                                className="mx-3 img-fluid profile-doc-preview"
                                alt="Preview"
                              />
                            ) : (
                              <p className="uploading_doc">
                                <i className="fi flaticon-upload" />
                              </p>
                            )}

                            <p className="doc_upload_place">
                              Drop a document here or click to select file to
                              upload
                            </p>
                          </div>
                        </div>
                      </section>
                    );
                  }}
                </Dropzone>
                <div>
                  <UploadImageComp
                    validateDocuments={this.callUpdateDocuments}
                    fileParams={{
                      file: this.state.file,
                      document_type: "leave_doc",
                      token: this.state.user_token,
                      type: this.state.type,
                      imageUrl: this.state.imageUrl,
                      leaveid: this.props.leaveId,
                      file_upload_action: "upload_leave_document"
                    }}
                    url={"generic_upload_url"}
                    onSuccessfulFileUpload={this.successfulFileUpload}
                    handleRomoveImage={this.handleRomoveImage}
                    showSuccessMessage={this.props.showSuccessMessage}
                  />
                </div>
                <Button
                  className="mt mr-2"
                  color="success"
                  onClick={() => this.props.onClose()}
                >
                  BACK
                </Button>
                <Button
                  className="mt"
                  color="success"
                  onClick={() =>
                    this.props.forAdmin
                      ? this.props.history.push("manage_leaves")
                      : this.props.history.push("my_leaves")
                  }
                >
                  Skip Upload Document
                </Button>
              </div>
            </div>
          </div>
        )}
      </div>
    );
  }
}
ApplyLeaveForm.propTypes = {
  doApplyLeave: PropTypes.func.isRequired,
  applyLeave: PropTypes.shape({
    start_date: PropTypes.string,
    end_date: PropTypes.string
  })
};
export default ApplyLeaveForm;
