import React from "react";
import { CONFIG } from "../../../../config/index";
import { getToken } from "../../../../services/generic";
import { Button, ListGroup, ListGroupItem, Table, Alert } from "reactstrap";
import map from "lodash/map";
import moment from "moment";
import Dialog from "material-ui/Dialog";
import FlatButton from "material-ui/FlatButton";
import RaisedButton from "material-ui/RaisedButton";
import TextField from "material-ui/TextField";

class ViewLeave extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      messagetouser: "",
      notifyMsg: "Document Required Notification Sent",
      document_required: true,
      messageByHr: "",
      edit: false,
      editedComment: "",
      hrcomment: "",
      open: false,
      reason: ""
    };
  }

  componentWillReceiveProps(props) {
    this.setState({ messagetouser: "", edit: false });
  }
  handleRevert = () => {
    this.props.leaveRevertRequest(this.props.selectedLeave.id, getToken());
  };
  handleSave = data => {
    if (this.state.messageByHr) {
      this.props.onAddDescription(
        this.props.selectedLeave.id,
        this.state.messageByHr,
        data
      );
      if (data == 1 || data == 2) {
        this.setState({
          edit: false,
          hrcomment: this.state.messageByHr,
          messageByHr: ""
        });
      }
    } else {
      this.setState({ hrErrorText: "Please add the description" });
    }
  };

  handleUpdate = data => {
    this.props.onAddDescription(
      this.props.selectedLeave.id,
      this.state.editedComment,
      data
    );
    this.setState({
      edit: false,
      editedComment: ""
    });
  };

  handleExtraDay = day => {
    this.props.onAddExtraDay(this.props.selectedLeave.id, getToken(), day);
  };
  handleNotify = () => {
    this.props.onDocRequired(this.props.selectedLeave.id, "1", "");
  };
  handleComment = () => {
    this.props.onDocRequired(
      this.props.selectedLeave.id,
      "",
      this.state.messagetouser
    );
  };
  handleEdit = () => {
    let comments = this.props.selectedLeave.hr_comment;
    this.setState({
      edit: true,
      editedComment: comments
    });
  };

  changeStatus(leaveid, newstatus) {
    this.props.doLeaveStatusChange(
      leaveid,
      newstatus,
      // this.state.messagetouser,
      this.state.reason
    )
    this.setState({
      open:false,
      reason:''
    })
  }
  handleOpen = () => {
    this.setState({
      open: true
    });
  };
  handleClose = () => {
    this.setState({
      open: false
    });
  };

  showAdminControl = () => {
    if (this.props.loggedUser.data.role === CONFIG.ADMIN) {
      return (
        <>
          {this.props.selectedLeave.late_reason === "" &&
          this.props.selectedLeave !== "Rejected" &&
          this.props.loggedUser.data.role === CONFIG.ADMIN ? null : (
            <>
              <Button
                className="m-1"
                outline
                color="primary"
                onClick={() => {
                  this.handleExtraDay("0.5");
                }}
              >
                Add Half Day
              </Button>
              <Button
                className="m-1"
                outline
                color="primary"
                onClick={() => {
                  this.handleExtraDay("1");
                }}
              >
                Add Full Day
              </Button>
            </>
          )}
          {this.props.selectedLeave.extra_day === "0" &&
          this.props.loggedUser.data.role === CONFIG.ADMIN ? null : (
            <Button
              className="m-1"
              outline
              color="danger"
              onClick={() => {
                this.handleExtraDay("0");
              }}
            >
              Remove Extra Day
            </Button>
          )}
        </>
      );
    }
  };

  _getChangeStatusButtons(leaveid, status) {
    let statusList = ["Approved", "Pending", "Rejected"];

    let soptions = map(statusList, (s, k) => {
      if (s == status) {
      } else if (s === "Approved") {
        return (
          <Button
            key={k}
            className="btn-rounded-f btn mr-2 approve-btn"
            // color="success"
            onClick={() => this.changeStatus(leaveid, s)}
          >
            Approve
          </Button>
        );
      } else if (s === "Pending") {
        return (
          <Button
            key={k}
            className="btn-rounded-f btn mr-2"
            color="warning"
            onClick={() => this.changeStatus(leaveid, s)}
          >
            Mark Pending
          </Button>
        );
      } else if (s === "Rejected") {
        const actions = [
          <FlatButton
            label="Cancel"
            primary
            onClick={this.handleClose}
            style={{ marginRight: 5 }}
          />,
          <RaisedButton
            label="Submit"
            primary
            onClick={()=>this.changeStatus(leaveid,s)}
            disabled={!this.state.reason}
          />
        ];
        return (
          <>
            <Button
              key={k}
              className="btn-rounded-f btn reject-btn"
              // color="danger"
              onClick={() => this.handleOpen()}
            >
              Reject
            </Button>
            <Dialog
              title={"REASON FOR REJECTION"}
              titleStyle={{ opacity: "0.56" }}
              modal={false}
              actions={actions}
              open={this.state.open}
              onRequestClose={this.handleClose}
              autoScrollBodyContent
            >
              <TextField
                fullWidth
                // ref="value"
                floatingLabelText={"Reason"}
                onChange={e => {
                  this.setState({
                    reason: e.target.value
                  });
                }}
                value={this.state.reason}
              />
            </Dialog>
          </>
        );
      }
    });
    return soptions;
  }

  _getStatus = val => {
    switch (val) {
      case "Pending":
        val = `btn-warning`;
        break;
      case "Approved":
        val = "btn-success";
        break;
      case "Rejected":
        val = "btn-danger";
        break;
      default:
        val = "btn-warning";
    }
    return val;
  };

  _getLastAppliedLeaves(dd) {
    return (
      <ListGroupItem className="list_group_item">
        <Table className="table-responsive">
          <thead className="bg-body">
            <tr className="heading-leave-old">
              <th width="15%">Applied on</th>
              <th width="25%">Date</th>
              <th width="10%">Duration</th>
              <th width="35%">Reason</th>
              <th width="15%">Status</th>
            </tr>
          </thead>
          <tbody className="leave-old-took">
            {dd.map(row => (
              <tr>
                <td>{moment(row.applied_on).format("DD MMMM, YYYY")}</td>
                <td>
                  {moment(row.from_date).format("DD MMMM, YYYY")}
                  <br /> to <br />
                  {moment(row.to_date).format("DD MMMM, YYYY")}
                </td>
                <td>{row.no_of_days} Days</td>
                <td className="text-break">{row.reason}</td>
                <td
                  className={
                    row.status == "Pending"
                      ? "text-warn"
                      : row.status == "Approved"
                      ? "text-success"
                      : "text-danger"
                  }
                >
                  {row.status}
                </td>
              </tr>
            ))}
          </tbody>
        </Table>
      </ListGroupItem>
    );
  }
  render() {
    let changeStatusButton = this._getChangeStatusButtons(
      this.props.selectedLeave.id,
      this.props.selectedLeave.status
    );
    // previoud leaves
    let last_applied_leaves = this.props.selectedLeave.last_applied_leaves;
    let last_applied_leaves_html = "";
    if (
      typeof last_applied_leaves !== "undefined" &&
      last_applied_leaves.length > 0
    ) {
      let old_leaves = this._getLastAppliedLeaves(
        this.props.selectedLeave.last_applied_leaves.filter(data => {
          return (
            moment(data.applied_on).format("YYYY") === moment().format("YYYY")
          );
        })
      );
      last_applied_leaves_html = (
        <div>
          <hr className="border-heading" />
          <h6 className="ml-2-ml-rem-2">
            LEAVE <b>DETAILS</b>
          </h6>
          <ListGroup className="list-group-sortable b-b">
            {old_leaves}
          </ListGroup>
        </div>
      );
    }
    let display = "";
    if (
      this.props.loggedUser.data.role === CONFIG.ADMIN &&
      this.props.selectedLeave.hr_comment === ""
    ) {
      display = "none";
    }
    let HRDisplay = "";
    if (this.props.loggedUser.data.role === CONFIG.ADMIN) {
      HRDisplay = "none";
    }
    let adminDisplay = "";
    if (this.props.loggedUser.data.role === CONFIG.HR) {
      adminDisplay = "none";
    }
    let status = this.props.selectedLeave.status;
    if (
      this.props.selectedLeave.status === "Pending" &&
      this.props.selectedLeave.hr_approved === "1"
    ) {
      status = "Approved By HR";
    } else if (
      this.props.selectedLeave.status === "Pending" &&
      this.props.selectedLeave.hr_approved === "2"
    ) {
      status = "Rejected By HR";
    }
    return (
      <div className="item">
        <div className="m-t">
          <div className="col-xs-12  leave-profile pl-1">
            <div className="text-center">
              <div
                className={`btn px-5 py-1 text-uppercase ${this._getStatus(
                  status
                )}`}
              >
                {status}
              </div>
              <div className="m-a-0 m-b-xs  mt-2">
                <h5 className="font-weight-bold py-2 m-0">
                  {this.props.selectedLeave.user_profile_name}
                </h5>
                <div className="pb-3">
                  {this.props.selectedLeave.user_profile_jobtitle}
                </div>
              </div>
            </div>
            <div className="grid-table-view d-flex text-center mr-1">
              <div className="grid-table-view-part1">
                <div className="grid-table-view-heading">APPLIED ON</div>
                <div className="grid-table-view-detail">
                  <b>
                    {moment(this.props.selectedLeave.applied_on).format("ll")}
                  </b>
                </div>
              </div>
              <div className="grid-table-view-part2">
                <div className="grid-table-view-heading">DATE</div>
                <div className="grid-table-view-detail">
                  <b>
                    {`${moment(this.props.selectedLeave.from_date).format(
                      "DD MMM"
                    )}, ${moment().format("YYYY")}`}{" "}
                    {this.props.selectedLeave.from_date !==this.props.selectedLeave.to_date &&  <>
                    To{" "}
                    {`${moment(this.props.selectedLeave.to_date).format(
                      "DD MM"
                    )}, ${moment().format("YYYY")}`}
                    </>}
                  </b>
                </div>
              </div>
              <div className="grid-table-view-part3">
                <div className="grid-table-view-heading">DURATION</div>
                <div className="grid-table-view-detail">
                  <b>{this.props.selectedLeave.no_of_days} Days</b>
                </div>
              </div>
              {this.props.selectedLeave && this.props.selectedLeave.rh_names && JSON.parse(this.props.selectedLeave.rh_names).length>0 && <div className="grid-table-view-part3">
                <div className="grid-table-view-heading">LEAVE NAME</div>
                 <div className="grid-table-view-detail">
                  {this.props.selectedLeave && this.props.selectedLeave.rh_names && JSON.parse(this.props.selectedLeave.rh_names).map((val,i)=>
                    val+ (i!==JSON.parse(this.props.selectedLeave.rh_names).length-1 ? ", ":"")
                  )}
                </div> 
              </div>
              }
              <div className="grid-table-view-par4">
                <div className="grid-table-view-heading">LEAVE TYPE</div>
                <div className="grid-table-view-detail text-success">
                  {this.props.selectedLeave.leave_type}
                </div>
              </div>
             
            </div>
            <div className="d-flex justify-content-around mt-4 ">
            <div className="text-center ">
              <div className="cause-reason font-weight-bold">REASON</div>
              <div className="reason-detail">
                <span className="reason-view text-break">
                  {this.props.selectedLeave.reason}
                </span>
              </div>
              <br />
              {this.props.selectedLeave.late_reason !== "" ? (
                <>
                  <div className="cause-reason font-weight-bold">
                    REASON FOR LATE APPLYING
                  </div>
                  <div className="reason-detail mb-2">
                    <span className="reason-view text-break">
                      {this.props.selectedLeave.late_reason}
                    </span>
                  </div>
                </>
              ) : null}
              {this.props.selectedLeave.rejected_reason? <>
                  <div className="cause-reason font-weight-bold">
                    REASON FOR REJECTION
                  </div>
                  <div className="reason-detail mb-2">
                    <span className="reason-view text-break">
                      {this.props.selectedLeave.rejected_reason}
                    </span>
                  </div>
                </>:null}
            </div>
            <div className="text-center" >
            <div className="cause-reason font-weight-bold">LEAVE DOCUMENT</div>
            <div>
              <span>{this.props.selectedLeave.doc_link == "N/A" ||
                  this.props.selectedLeave.doc_link == "" ? 
                  <button class="link border-0 bg-body " onClick={this.handleNotify}> <small className="text-primary">Notify Document Required</small> </button>
                 :  <a href= {this.props.selectedLeave.doc_link} target="_blank"  rel="noopener noreferrer">
                    <span> View Document</span>
              </a> }
                 </span></div>
            </div>
            </div>
            {this.props.loggedUser.data.role === CONFIG.ADMIN ? (
              <div className="text-center">{changeStatusButton}</div>
            ) : null}
            {!this.props.selectedLeave.comment &&
            this.props.loggedUser.data.role === CONFIG.ADMIN ? (
              <div className="admin-comment py-3 mt-3 d-flex flex-wrap">
                <div className="px-3 pt">Comment</div>
                <div className="textarea-font-size d-flex flex-wrap">
                  <form onSubmit={this.handleComment}>
                    <div>
                    <textarea
                        className="textarea-placeholder  align-middle"
                        rows="1"
                        cols="50"
                        onChange={e =>
                          this.setState({ messagetouser: e.target.value })
                        }
                        value={this.state.messagetouser}
                        placeholder="Do comment to user"
                      />
                      {/* <input
                        className="md-input align-middle"
                        onChange={e =>
                          this.setState({ messagetouser: e.target.value })
                        }
                        value={this.state.messagetouser}
                        style={{ width: "97%" }}
                      /> */}
                    </div>
                    <div className="textarea-button  mt-2 justify-content-end mr-3 d-flex flex-wrap">
                      <Button
                        className="m-1"
                        color="primary"
                        type="submit"
                        onClick={this.handleComment}
                        disabled={this.state.messagetouser ? false : true}
                      >
                        Comment
                      </Button>
                    </div>
                  </form>
                </div>
              </div>
            ) : (
              <div className="admin-comment py-3 mt-3 d-flex flex-wrap">
                <div className="px-3 pt">Comment</div>
                <div className="textarea-font-size d-flex flex-wrap">
                  <div>
                  <textarea
                        disabled
                        className="textarea-placeholder  align-middle"
                        rows="1"
                        cols="50"
                        value= {
                          this.props.selectedLeave.comment
                            ? this.props.selectedLeave.comment
                            : "No comment from admin"
                        }
                        placeholder="No comment from admin"
                      />
                    {/* <input
                      className="text md-input align-middle"
                      disabled
                      value={
                        this.props.selectedLeave.comment
                          ? this.props.selectedLeave.comment
                          : "No comment from admin"
                      }
                    /> */}
                  </div>
                </div>
              </div>
            )}

            <div className="hr-description py-3 mt-3 d-flex flex-wrap">
              <div className="px-3 pt">HR Description</div>
              <div className="textarea-font-size d-flex flex-wrap">
                <div>
                  {status === "Pending" &&
                  this.props.loggedUser.data.role === CONFIG.HR ? (
                    <>
                      <textarea
                        className="textarea-placeholder  align-middle"
                        rows="1"
                        cols="50"
                        onChange={e =>
                          this.setState({
                            messageByHr: e.target.value,
                            hrErrorText: ""
                          })
                        }
                        value={this.state.messageByHr}
                        placeholder="write entire leave detail after talking to employee"
                      />
                      {this.state.hrErrorText ? (
                        <small className="text-danger">
                          {this.state.hrErrorText}
                        </small>
                      ) : null}
                    </>
                  ) : (
                    <textarea
                      className="textarea-placeholder hr-comment align-middle"
                      rows="1"
                      cols="150"
                      disabled
                      value={
                        this.props.selectedLeave.hr_comment
                          ? this.props.selectedLeave.hr_comment
                          : "No comment from HR"
                      }
                    />
                  )}
                </div>
                <div className="textarea-button  mt-2 d-flex flex-wrap">
                  {status == "Pending" &&
                  this.props.loggedUser.data.role === CONFIG.HR ? (
                    <>
                      <Button
                        className="m-1"
                        outline
                        color="success"
                        onClick={() => {
                          this.handleSave("1");
                        }}
                      >
                        HR Approval
                      </Button>
                      <Button
                        className="m-1"
                        outline
                        color="danger"
                        onClick={() => {
                          this.handleSave("2");
                        }}
                      >
                        HR Rejected
                      </Button>
                    </>
                  ) : (
                    ""
                  )}
                  {this.showAdminControl()}
                  {this.props.selectedLeave.doc_link == "N/A" ||
                  this.props.selectedLeave.doc_link == "" ? null : (
                    <form
                      method="get"
                      target="_blank"
                      action={this.props.selectedLeave.doc_link}
                      className="m-0"
                    >
                      <Button
                        className="m-1"
                        outline
                        color="primary"
                        style={{ fontSize: "12px" }}
                      >
                        View Document
                      </Button>
                    </form>
                  ) }
                  {/* {this.props.selectedLeave.doc_link == "N/A" ||
                  this.props.selectedLeave.doc_link == "" ?  
                  <Button
                    className="m-1"
                    outline
                    color="primary"
                    onClick={this.handleNotify}
                  >
                    Notify Document Required
                  </Button> : null } */}
                </div>
              </div>
            </div>
          </div>
          <div className="col-xs-12  leave-details px-rem-2">
            {this.props.selectedLeave.doc_link === "N/A" &&
            this.props.selectedLeave.doc_require === "1" ? (
              <Alert color="success" fade={false} className={"py-3 mt-3"}>
                {this.state.notifyMsg}
              </Alert>
            ) : null}
            {this.props.selectedLeave.extra_day !== "0" ? (
              <div>
                Extra Day Added -{" "}
                <i>
                  <b>{this.props.selectedLeave.extra_day}</b>
                </i>
              </div>
            ) : null}
            {last_applied_leaves_html}
          </div>
        </div>
      </div>
    );
  }
}

ViewLeave.styles = {
  leaveDiv: {
    marginBottom: "10px"
  }
};

export default ViewLeave;
