import React, { Component } from "react";
import { Button } from "reactstrap";
import { withRouter } from "react-router";
import "./denied.scss";

function AccessDenied(props) {
  return (
    <div className="denied">
      <span className="number-alert"> Access Denied </span>
      <br />
      <span className="text-alert-one">
        You are unauthorized to the Action
        <br />
        Contact Admin!!
      </span>
    </div>
  );
}

export default withRouter(AccessDenied);
