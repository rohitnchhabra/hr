import React, { Component } from "react";
import { Button } from "reactstrap";
import { withRouter } from "react-router";
import "./denied.scss";

function Denied(props) {
  return (
    <div className="denied">
      <span className="number-alert"> 404 </span>
      <br />
      <span className="text-alert-one">
        Oops, it seems that this page does
        <br />
        not exist.
      </span>
      <br />

      <div className="back-button">
        <Button
          outline
          color="success"
          onClick={() => {
            props.history.goBack();
          }}
        >
          Go Back
        </Button>
      </div>
    </div>
  );
}

export default withRouter(Denied);
