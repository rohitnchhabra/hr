import React from 'react';
import {connect} from 'react-redux';
import {withRouter} from 'react-router';
import {notify} from '../../../services/notify';
import {isNotUserValid} from '../../../services/generic';
import WorkingHoursSummary from '../../../components/workingHours/WorkingHoursSummary';
import * as actionsWorkingHoursSummary from '../../../redux/workingHours/actions/workingHoursSummary';
import * as actions from '../../../redux/actions'
import Heading from '../../../components/generic/Heading';

class ManageWorkingHours extends React.Component {
  constructor (props) {
    super(props);
    this.preDate= "";
    this.preHours= "";
    this.state = {
      'defaultUserDisplay': '',
      'daysummary_userid':  '',
      'daysummary_date':    '',
      'selectedButton':     ''
    };
    this.onWorkingHoursChange = this.onWorkingHoursChange.bind(this);
  }
  componentWillMount () {
    let d = new Date();
    let year = d.getFullYear();
    let month = d.getMonth() + 1;
    this.props.onWorkingHoursSummary(year, month);
    this.props.showHeading("Manage Working Hours")
  }
  componentWillReceiveProps (props) {
    let isNotValid = isNotUserValid(this.props.location.pathname, props.loggedUser);
    if (isNotValid.status) {
      this.props.history.push(isNotValid.redirectTo);
    }
    if (props.workingHoursSummary.status_message !== '') {
      notify(props.workingHoursSummary.status_message);
    }
  }
  onWorkingHoursChange (date, hours) {
  if(this.preDate !== date || this.preHours !== hours) {
    if (hours !== '') {
      this.props.onUpdateDayWorkingHours(date, hours).then((data) => {}, (error) => {
        notify(error);
      });
    }
    this.preDate = date;
    this.preHours = hours;
  }
}

  onHandleWorkingHoursSummary = (year, month, buttonClicked) =>{
    this.setState({ 'selectedButton': buttonClicked });
    this.props.onWorkingHoursSummary(year, month);
  }
  render () {
    return (
      <div>
        <div id="content" className="app-content box-shadow-z0" role="main" >
          <div className="app-body" id="view">
            <div className="">
              <div className="row mx-0">
                <div className="col-md-12 px-0">
                  <WorkingHoursSummary workingHoursSummary={this.props.workingHoursSummary} onWorkingHoursChange={this.onWorkingHoursChange} onWorkingHoursSummary={this.onHandleWorkingHoursSummary} buttonClicked={this.state.selectedButton} />
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

function mapStateToProps (state) {
  return {
    frontend:            state.frontend.toJS(),
    loggedUser:          state.logged_user.userLogin,
    userDaySummary:      state.userDaySummary.toJS(),
    workingHoursSummary: state.workingHoursSummary.toJS()
  };
}
const mapDispatchToProps = (dispatch) => {
  return {
    onWorkingHoursSummary: (year, month) => {
      return dispatch(actionsWorkingHoursSummary.get_working_hours_summary(year, month));
    },
    onUpdateDayWorkingHours: (date, time) => {
      return dispatch(actionsWorkingHoursSummary.update_day_working_hours(date, time));
    },
    showHeading:(data)=>{
      return dispatch(actions.showHeading(data));
    }
  };
};

const VisibleManageWorkingHours = connect(mapStateToProps, mapDispatchToProps)(ManageWorkingHours);

const RouterVisibleManageWorkingHours = withRouter(VisibleManageWorkingHours);

export default RouterVisibleManageWorkingHours;
