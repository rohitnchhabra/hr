import React from "react";
import { connect } from "react-redux";
import { withRouter } from "react-router";
import { notify } from "../../../services/notify";
import { isNotUserValid } from "../../../services/generic";
import UsersListHeader from "../../../components/generic/UsersListHeader";
import UserPicHeadTitle from "../../../components/generic/UserPicHeadTitle";
import ListUserWorkingHours from "../../../components/workingHours/ListUserWorkingHours";
import FormAddUserWorkingHours from "../../../modules/workingHours/components/FormAddUserWorkingHours";
import * as actionsUsersList from "../../../redux/generic/actions/usersList";
import * as actionsManageUserWorkingHours from "../../../redux/workingHours/actions/manageUserWorkingHours";
import * as actions from '../../../redux/actions'
import Heading from "../../../components/generic/Heading";
import { compose } from "redux";
import isMobile from "../../../components/hoc/WindowResize";
import FormMultipleUserWorkingHours from "../components/FormMultipleUserWorkingHours";
import { Alert } from "reactstrap";

class ManageUserWorkingHours extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      defaultUserDisplay: "",
      daysummary_userid: "",
      daysummary_date: ""
    };
    this.onUserClick = this.onUserClick.bind(this);
    this.onShowDaySummary = this.onShowDaySummary.bind(this);
    this.callAddUserWorkingHours = this.callAddUserWorkingHours.bind(this);
  }
  componentWillMount() {
    this.props.onUsersList();
    this.props.showHeading("Manage Employees Working Hours")
  }
  componentWillReceiveProps(props) {
    let isNotValid = isNotUserValid(
      this.props.location.pathname,
      props.loggedUser
    );
    if (isNotValid.status) {
      this.props.history.push(isNotValid.redirectTo);
    }

    if (this.state.defaultUserDisplay === "") {
      if (props.usersList && props.usersList.users && props.usersList.users.length > 0) {
        let firstUser = props.usersList.users[0];
        let defaultUserId = firstUser.user_Id;
        this.onUserClick(defaultUserId);
      }
    }
    if (
      JSON.stringify(this.props.manageUserWorkingHours.userInfo) !==
      JSON.stringify(props.manageUserWorkingHours.userInfo)
    ) {
      this.setState({
        defaultUserDisplay: props.manageUserWorkingHours.userInfo.user_Id
      });
    }
  }
  onUserClick(userid) {
    this.setState({ defaultUserDisplay: userid });
    this.props.onUserWorkingHoursData(userid);
  }
  onShowDaySummary(userid, date) {
    this.setState({ daysummary_userid: userid, daysummary_date: date });
    this.props.onUserDaySummary(userid, date);
  }

  callAddUserWorkingHours(userid, date, workingHours, reason) {
    this.props.onAddUserWorkingHours(userid, date, workingHours, reason).then(
      data => {},
      error => {
        notify(error);
      }
    );
  }

  callMultipleAddUserWorkingHours = (
    userid,
    week_day,
    week_of_month,
    day_type,
    working_hours,
    date_start,
    date_end,
    reason
  ) => {
    this.props
      .onMultipleAddUserWorkingHours(
        userid,
        week_day,
        week_of_month,
        day_type,
        working_hours,
        date_start,
        date_end,
        reason
      )
      .then(
        data => {},
        error => {
          notify(error);
        }
      );
  };

  render() {
    let statusMessage = "";
    if (this.props.manageUserWorkingHours.status_message !== "") {
      statusMessage = (
        <span className="label label-lg primary pos-rlt m-r-xs">
          <b className="arrow left b-primary"></b>
          {this.props.manageUserWorkingHours.status_message}
        </span>
      );
    }

    let selectedUserImage = "";
    let selectedUserName = "";
    let selectedUserJobtitle = "";
    let selectedUserId = "";
    try {
      selectedUserImage = this.props.manageUserWorkingHours.userInfo.image;
      selectedUserName = this.props.manageUserWorkingHours.userInfo.username;
      selectedUserJobtitle = this.props.manageUserWorkingHours.userInfo
        .jobtitle;
      selectedUserId = this.props.manageUserWorkingHours.userInfo.user_Id;
    } catch (err) {}
    return (
      <div className="manage-working-wrapper">
        <div id="content" className="app-content box-shadow-z0" role="main">
          <UsersListHeader
            users={this.props.usersList.users}
            selectedUserId={this.state.selected_user_id}
            onUserClick={this.onUserClick}
          />
          <UserPicHeadTitle
            profileImage={selectedUserImage}
            name={selectedUserName}
            jobTitle={selectedUserJobtitle}
          />
          <div className="app-body" id="view">
            <div className="row mr-0 ml-0">
              {/* <div className="col-sm-3 hidden-xs" id="fixedScroll">
                  <UsersList users={this.props.usersList.users}
                    selectedUserId={this.state.defaultUserDisplay}
                    onUserClick={this.onUserClick}
                    top={10}
                    />
                </div> */}
              <div className="col-12 user-working-hours-view pl-0 pr-0">
                <div className="row no-gutter">
                  <div className="col-sm-12 col-lg-9 bg-white">
                    <div className="p-a block">
                      <h6 className="text-center">ADD NEW</h6>
                      <hr />
                      <Alert
                        color={"info"}
                      >
                        This section is used to setup specific hours for an employee on
                        a particular day. Let's say on a certain day an employee needs
                        to leave early for a sale meeting and will be in office for just
                        5hrs. Here you can set the hours for that day and employee won't
                        get any notification for working less on that specific date.
                      </Alert>
                      <FormAddUserWorkingHours
                        {...this.props}
                        userid={selectedUserId}
                        callAddUserWorkingHours={this.callAddUserWorkingHours}
                        isMobile={this.props.isMobile}
                      />
                    </div>
                    <div className="p-a block">
                      <h6 className="text-center">Change User Working Days</h6>
                      <hr />
                      <FormMultipleUserWorkingHours
                        {...this.props}
                        userid={selectedUserId}
                        callMultipleAddUserWorkingHours={
                          this.callMultipleAddUserWorkingHours
                        }
                        isMobile={this.props.isMobile}
                      />
                    </div>
                  </div>
                  <div className="col-sm-12 col-lg-3 bg-white">
                    <div className="p-a block ">
                      <h6 className="text-center ">PREVIOUS WORKING HOURS</h6>
                      <hr />
                      <div className="hour-overflow scroll ">
                        <ListUserWorkingHours
                          displayData={
                            this.props.manageUserWorkingHours.displayData
                          }
                        />
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    frontend: state.frontend.toJS(),
    loggedUser: state.logged_user.userLogin,
    usersList: state.usersList.toJS(),
    manageUserWorkingHours: state.manageUserWorkingHours.toJS()
  };
}
const mapDispatchToProps = dispatch => {
  return {
    onUsersList: () => {
      return dispatch(actionsUsersList.get_users_list());
    },
    onUserWorkingHoursData: userid => {
      return dispatch(
        actionsManageUserWorkingHours.get_managed_user_working_hours(userid)
      );
    },
    onAddUserWorkingHours: (userid, date, workingHours, reason) => {
      return dispatch(
        actionsManageUserWorkingHours.add_user_working_hours(
          userid,
          date,
          workingHours,
          reason
        )
      );
    },
    onMultipleAddUserWorkingHours: (
      userid,
      week_day,
      week_of_month,
      day_type,
      working_hours,
      date_start,
      date_end,
      reason
    ) => {
      return dispatch(
        actionsManageUserWorkingHours.addMultipleUserPendingHour(
          userid,
          week_day,
          week_of_month,
          day_type,
          working_hours,
          date_start,
          date_end,
          reason
        )
      );
    },
    showHeading:(data)=>{
      return dispatch(actions.showHeading(data));
    }
  };
};


export default compose(
  connect(
    mapStateToProps,
    mapDispatchToProps
  ),
  isMobile,
  withRouter
)(ManageUserWorkingHours);
