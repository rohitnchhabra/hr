import React from 'react';
import {connect} from 'react-redux';
import {withRouter} from 'react-router';
import {notify} from '../../../services/notify';
import {isNotUserValid} from '../../../services/generic';
import UserPendingHoursList from '../components/UserPendingHoursList';
import * as actionsUsersList from '../../../redux/generic/actions/usersList';
import * as actionPendingHour from '../../../redux/workingHours/actions/managePendingLeave';
import * as actions_apply_leave from '../../../redux/leave/actions/applyLeave';
import * as actionsManageUserPendingHours from '../../../redux/workingHours/actions/manageUserPendingHour';
import Heading from '../../../components/generic/Heading';
import * as actions from '../../../redux/actions';

class ManageUserPendingHours extends React.Component {
  constructor (props) {
    super(props);
    this.state = {
      'defaultUserDisplay': '',
      'daysummary_date':    '',
      status_message:       '',
      active:               'active',
      firstArrow:           'show',
      secondArrow:          'hidden',
      thirdArrow:           'hidden',
      pendingList:          'show',
      pendingUserList:      'hidden',
      open:                 false,
      edit:                 false
    };
    this.callAddUserPendingHours = this.callAddUserPendingHours.bind(this);
    this.callFetchPendingUserList = this.callFetchPendingUserList.bind(this);
    this.doApplyLeave = this.doApplyLeave.bind(this);
    this.callOnDaysBetweenLeaves = this.callOnDaysBetweenLeaves.bind(this);
  }
  componentWillMount () {
    let d = new Date();
    let year = d.getFullYear().toString();
    let months = ('0' + (d.getMonth() + 1)).slice(-2).toString();
    this.props.onUserPendingHoursData(year, months);
    this.props.onUsersList();
    this.props.showHeading("Manage Employee Pending Hours")
  }
  componentWillReceiveProps (props) {
    let isNotValid = isNotUserValid(this.props.location.pathname, props.loggedUser);
    if (isNotValid.status) {
      this.props.history.push(isNotValid.redirectTo);
    }
  }


  callAddUserPendingHours (userid, pendingHour, empId) {
    this.setState({show_status_message: true});
    this.props.onAddUserPendingHours(userid, pendingHour, empId).then((message) => {
      notify('Success !', message, 'success');
    }).catch((error) => {
      notify('Error !', error, 'error');
    });
  }

  callFetchPendingUserList () {
    this.onUserPendingHoursData();
  }

  doApplyLeave (start, end, days, reason, userid, day_status, leaveType, late_reason, pending_id, year, month) {
    this.setState({show_status_message: true});
    this.props.onApplyLeave(start, end, days, reason, userid, day_status, leaveType, late_reason, pending_id, year, month).then((data) => {
      this.props.onUserPendingHoursData(year, month);
      notify('Success !', data, 'success');
    }).catch((error) => {
      notify('Error !', error, 'error');
    });
  }

  callOnDaysBetweenLeaves () {
    this.props.onDaysBetweenLeaves();
  }

  doCompenseate = () => {
    const url = process.env.REACT_APP_BASE_URL+"/attendance/API_HR/crons.php?action=calculate_previous_month_pending_time";
    window.open(url, '_blank');
  }

  render () {
    let pending_hour_list = <UserPendingHoursList
      callFetchPendingUserList={this.callFetchPendingUserList}
      manageUserPendingHours={this.props.manageUserPendingHours}
      onUserPendingHoursData={this.props.onUserPendingHoursData}
      doApplyLeave={this.props.doApplyLeave}
      doApplyHalfLeave={this.props.doApplyHalfLeave}
      callOnDaysBetweenLeaves={this.props.callOnDaysBetweenLeaves}
      {...this.props} />;

    // let statusMessage = '';
    // if (this.props.manageUserPendingHours.status_message !== '') {
    //   statusMessage = <span className="label label-lg primary pos-rlt m-r-xs">
    //     <b className="arrow left b-primary"></b>{this.props.manageUserPendingHours.status_message}</span>;
    // }

    return (
      <div className="mange-pendinghrs-wrapper">
        <div id="content" className="app-content box-shadow-z0" role="main">
          <div>
             <button type="button" class="btn time_remains" onClick={this.doCompenseate}>Compensation Time</button>
          </div>
          
          <div className="app-body" id="view">
              <div className="row ml-0 mr-0">
                {pending_hour_list}
              </div>
          </div>
        </div>
      </div>
    );
  }
}

function mapStateToProps (state) {
  return {
    frontend:               state.frontend.toJS(),
    loggedUser:             state.logged_user.userLogin,
    usersList:              state.usersList.toJS(),
    manageUserPendingHours: state.manageUserPendingHours.toJS(),
    applyLeave:             state.applyLeave.toJS()
  };
}
const mapDispatchToProps = (dispatch) => {
  return {
    onUsersList: () => {
      return dispatch(actionsUsersList.get_users_list());
    },
    onUserPendingHoursData: (year, month) => {
      return dispatch(actionsManageUserPendingHours.getUserPendingHourList(year, month));
    },
    onAddUserPendingHours: (userId, pendingHour, empId, year, month) => {
      return dispatch(actionsManageUserPendingHours.addUserPendingHour(userId, pendingHour, empId, year, month));
    },
    onApplyHalfLeave: (no_of_days, userId, day_status, pending_id, year, month) => {
      return dispatch(actionPendingHour.applyPendingLeave(no_of_days, userId, day_status, pending_id, year, month));
    },
    onApplyLeave: (from_date, to_date, no_of_days, reason, userId, day_status, leaveType, late_reason, pending_id, year, month) => {
      return dispatch(actions_apply_leave.apply_leave(from_date, to_date, no_of_days, reason, userId, day_status, leaveType, late_reason, pending_id, year, month));
    },
    onDaysBetweenLeaves: (startDate, endDate) => {
      return dispatch(actions_apply_leave.getDaysBetweenLeaves(startDate, endDate));
    },
    showHeading:(data)=>{
      return dispatch(actions.showHeading(data));
    }

  };
};

const VisibleManageUserPendingHours = connect(mapStateToProps, mapDispatchToProps)(ManageUserPendingHours);

const RouterVisibleManageUserPendingHours = withRouter(VisibleManageUserPendingHours);

export default RouterVisibleManageUserPendingHours;
