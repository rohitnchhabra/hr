import React, { Component } from "react";
import { Button, Input } from "reactstrap";
import { notify } from "../../../services/notify";
import { Spinner } from "reactstrap";
import Dialog from "material-ui/Dialog";
import { Calendar } from "react-date-range";
import { ButtonFlat } from "../../../components/generic/buttons/Button";
var moment = require("moment");

export default class PendingHoursRow extends Component {
  constructor(props) {
    super(props);
    this.state = {
      dropDownValue: "",
      pendingList: this.props.row,
      key: this.props.slNo,
      day_status: "",
      pending_id: "",
      year: "",
      month: "",
      userId: "",
      half_day: "0.5",
      full_day: "1",
      officetime: "9:00",
      pendingTime: "",
      user_Id: "",
      empId: "",
      show_status_message: false,
      buttonSelectedForUser: "",
      dialogOpen: false,
      form_from_date: moment().format("YYYY-MM-DD"),
      form_to_date: moment().format("YYYY-MM-DD"),
      form_no_of_days: "",
      form_reason: "",
      show_half_day_button: "",
      leaveType: "",
      late_reason: ""
    };
  }

  componentWillMount() {
    this.setState({
      userId: this.props.row.user_Id,
      pending_id: this.props.row.id,
      year: this.props.manageUserPendingHours.year,
      month: this.props.manageUserPendingHours.displayData.month,
      pendingTime: this.props.row.extra_time,
      empId: this.props.row.id,
      pendingList: this.props.row
    });
  }
  componentWillReceiveProps(props) {
    this.setState({
      pending_id: props.row.id,
      userId: props.row.user_Id,
      year: props.manageUserPendingHours.displayData.year,
      month: props.manageUserPendingHours.displayData.month,
      pendingTime: props.row.extra_time,
      empId: props.row.id,
      pendingList: props.row
    });
    let num_working_days = "0";
    if (
      props.applyLeave.count_working_days != "" &&
      props.applyLeave.count_working_days != 0
    ) {
      num_working_days = props.applyLeave.count_working_days;
    }
    this.setState({
      form_from_date: props.applyLeave.start_date,
      form_to_date: props.applyLeave.end_date,
      pending_id: props.row.id,
      userId: props.row.user_Id,
      year: props.manageUserPendingHours.displayData.year,
      month: props.manageUserPendingHours.displayData.month,
      form_no_of_days: num_working_days
    });
  }

  onSaveClick = dropDownValue => {
    this.setState({ buttonSelectedForUser: this.props.row.user_Id });
    this.props.onbuttonClicked("");    
    if (dropDownValue === "AHLeave") {
      this.onApplyHalfDayLeave();
    } else if (dropDownValue === "AFLeave") {
      this.onApplyFullDayLeave();
    } else if (dropDownValue === "MergeToNext") {
      this.onMergingToNextDay();
    } else if (dropDownValue === "ALeave") {
      this.props.onDaysBetweenLeaves(
        this.state.form_from_date,
        this.state.form_to_date
      );
      this.handleDialogOpen();
    } else {
      return;
    }
  };

  onApplyHalfDayLeave = () => {
    const day_status = "1";
    this.props
      .onApplyHalfLeave(
        this.state.half_day,
        this.state.userId,
        day_status,
        this.state.pending_id,
        this.state.year,
        this.state.month
      )
      .then(data => {
        this.props.onUserPendingHoursData(this.state.year, this.state.month).then(res=>{
          notify("Success !", "Half day leave Applied", "success");
          this.setState({
            form_no_of_days: "",
            pending_id: "",
            year: "",
            month: "",
            userId: "",
            show_status_message: true,
            buttonSelectedForUser: "",
            dropDownValue: ""
          });
          }
        ).catch(error=>{
          notify(error);
        });
        
      })
      .catch(error => {
        notify("Error !", error, "error");
      });
  };

  onApplyFullDayLeave = () => {
    const day_status = "";
    this.props
      .onApplyHalfLeave(
        this.state.full_day,
        this.state.userId,
        day_status,
        this.state.pending_id,
        this.state.year,
        this.state.month
      )
      .then(data => {
        
        this.props.onUserPendingHoursData(this.state.year, this.state.month).then(res=>{
          notify("Success !", "Full day leave Applied", "success");
          this.setState({
            form_no_of_days: "",
            pending_id: "",
            year: "",
            month: "",
            userId: "",
            show_status_message: true,
            buttonSelectedForUser: "",
            dropDownValue: ""
          });
        }).catch(error=>{
          notify(error);
        });
       
      })
      .catch(error => {
        notify("Error !", error, "error");
      });
  };

  onMergingToNextDay = () => {
    var min = this.state.officetime;
    var penMin = this.state.pendingTime;
    var times = [];
    var times1 = min.split(":");
    var times2 = penMin.split(":");
    for (var i = 0; i < 2; i++) {
      times1[i] = isNaN(parseInt(times1[i])) ? 0 : parseInt(times1[i]);
      times2[i] = isNaN(parseInt(times2[i])) ? 0 : parseInt(times2[i]);
      times[i] = times1[i] + times2[i];
    }

    var minutes = times[1];
    var hours = times[0];

    if (minutes % 60 === 0) {
      let res = minutes / 60;
      hours += res;
      minutes = minutes - 60 * res;
    }

    var pendingHour =
      hours <= 9 ? "0" + hours + ":" + minutes : hours + ":" + minutes;
    var userId = this.state.userId;
    const empId = this.state.empId;
    this.props.callAddUserPendingHours(
      userId,
      pendingHour,
      empId,
      this.state.year,
      this.state.month
    );
  };

  handleDialogOpen = () => {
    this.setState({
      dialogOpen: true
    });
  };
  handleDialogClose = () => {
    this.setState({
      dialogOpen: false,
      form_from_date: "",
      form_to_date: "",
      form_no_of_days: "",
      form_reason: "",
      show_half_day_button: "",
      day_status: "",
      leaveType: "",
      late_reason: "",
      pending_id: "",
      year: "",
      userId: "",
      month: "",
      buttonSelectedForUser: ""
    });
  };

  _apply_half_day_1 = shift => {
    if (shift == 1) {
      this.setState({
        form_no_of_days: "0.5",
        day_status: "1"
      });
    } else if (shift == 2) {
      this.setState({
        form_no_of_days: "0.5",
        day_status: "2"
      });
    }
  };

  handleStartDate = date => {
    let startDate = date.format("YYYY-MM-DD");
    this.setState({ form_from_date: startDate, form_no_of_days: "" }, () => {
      this.props.onDaysBetweenLeaves(startDate, this.state.form_to_date);
    });
  };
  handleEndDate = date => {
    let endDate = date.format("YYYY-MM-DD");
    this.setState({ form_to_date: endDate, form_no_of_days: "" }, () => {
      this.props.onDaysBetweenLeaves(this.state.form_from_date, endDate);
    });
  };

  doApplyLeave = evt => {
    evt.preventDefault();
    this.props
      .onApplyLeave(
        this.state.form_from_date,
        this.state.form_to_date,
        this.state.form_no_of_days,
        this.state.form_reason,
        this.state.userId,
        this.state.day_status,
        this.state.leaveType,
        this.state.late_reason,
        this.state.pending_id,
        this.state.year,
        this.state.month
      )
      .then(data => {
        notify("Success", "leave Applied", "success");
        this.handleDialogClose();
        // this.props.onUserPendingHoursData(this.state.year, this.state.month);
        this.setState({
          form_from_date: "",
          form_to_date: "",
          form_no_of_days: "",
          form_reason: "",
          show_half_day_button: "",
          day_status: "",
          leaveType: "",
          late_reason: "",
          pending_id: "",
          year: "",
          month: "",
          userId: "",
          show_status_message: true,
          buttonSelectedForUser: "",
          dropDownValue: ""
        });
      })
      .catch(error => {
        notify("Error !", error, "error");
      });
  };

  render() {
    const { pendingList, key } = this.state;
    let pendingMessage = pendingList.time_detail.t_detail;
    let pendingMsgMap = pendingMessage.map((msg, i) => {
      return (
        <ul style={{ padding: "0 0 0 17px", marginTop: "10px" }} key={i}>
          <li style={{ aling: "center", fontSize: "11px" }}>{msg.message} </li>
        </ul>
      );
    });
    let dateDiff = moment(moment().format("YYYY-MM-DD")).diff(
      this.state.form_from_date || moment().format("YYYY-MM-DD"),
      "days"
    );
    let apply_half_day_button_1 = "";
    let apply_half_day_button_2 = "";
    if (this.state.form_no_of_days == 1) {
      apply_half_day_button_1 = (
        <ButtonFlat
          className="text-accent"
          onClick={() => this._apply_half_day_1(1)}>Apply Leave For First Half</ButtonFlat>
      );
      apply_half_day_button_2 = (
        <ButtonFlat
          className="text-accent"
          onClick={() => this._apply_half_day_1(2)}
        >Apply Leave For Second Half</ButtonFlat>
      );
    }

    let width = "63%";
    if (this.props.forAdmin == true) {
      width = "82%";
    }
    return (
      <>
        <tr key={key}>
          <td className="p-r-xs">{key + 1}</td>
          <td>{pendingList.name}</td>
          {pendingList.pending_hour >= 9 ? (
            <td>
              <mark
                style={{
                  color: "#ffffff",
                  aling: "center",
                  backgroundColor: "#ff0000"
                }}
              >
                {pendingList.pending_hour} {"hr"} {pendingList.pending_minute}{" "}
                {"min"}
              </mark>
              {pendingMsgMap}
            </td>
          ) : (
            <td>
              <mark>
                {" "}
                {pendingList.pending_hour} {"hr"} {pendingList.pending_minute}{" "}
                {"min"}{" "}
              </mark>
              {pendingMsgMap}
            </td>
          )}

          {pendingList.status ? (
            <td className="max-w-200">
              {"Updated On : "}
              {pendingList.date}
              <br />
              {pendingList.status}
            </td>
          ) : (
            <td>
              <mark>{"Pending"}</mark>
            </td>
          )}
          {pendingList.status_merged === "0" ? (
            <td className="d-flex">
              <Input
                type="select"
                value={this.state.dropDownValue}
                onChange={e => {
                  this.setState({
                    dropDownValue: e.target.value
                  });
                }}
                style={{ flex: "2" }}
              >
                <option value="">Select An Action</option>
                <option value={"ALeave"}>Apply Leave</option>
                <option value={"AHLeave"}>Apply Half Day Leave</option>
                <option value={"AFLeave"}>Apply Full Day Leave</option>
                <option value={"MergeToNext"}>Merge To Next Working Day</option>
              </Input>
              {this.state.buttonSelectedForUser === this.props.row.user_Id &&
              this.props.manageUserPendingHours.status_message === "" ? (
                <Button
                  color="primary"
                  className={"ml-2"}
                  style={{ flex: "1", minWidth: "70px", cursor: "not-allowed" }}
                >
                  <Spinner color="light" size="sm" />
                </Button>
              ) : (
                <Button
                  color="primary"
                  className={"ml-2"}
                  style={{ flex: "1", minWidth: "70px" }}
                  onClick={() => {
                    this.onSaveClick(this.state.dropDownValue);
                  }}
                  disabled={this.state.dropDownValue ? false : true}
                >
                  {this.state.dropDownValue === "ALeave" ? "Apply" : "Save"}
                </Button>
              )}
            </td>
          ) : (
            <td>
              {" "}
              <mark>{"No Action Required"} </mark>
            </td>
          )}
        </tr>
        <Dialog
          title="Add As Leave "
          modal={false}
          open={this.state.dialogOpen}
          onRequestClose={this.handleDialogClose}
          contentStyle={{ width: "80%", maxWidth: "auto" }}
          autoScrollBodyContent
        >
          <div className="row">
            <div className="col-sm-4 text-center">
              <h6>Select Start Date</h6>
              <Calendar onChange={this.handleStartDate} />
            </div>
            <div className="col-sm-4 text-center">
              <h6>Select End Date</h6>
              <Calendar onChange={this.handleEndDate} />
            </div>
            <div className="col-sm-4">
              <h5 className="text-center">Leave Summary</h5>
              <br />

              <form onSubmit={this.doApplyLeave}>
                <div className="box-body">
                  <div className="streamline b-l m-l">
                    <div className="sl-item b-success">
                      <div className="sl-icon">
                        <i className="fa fa-check" />
                      </div>
                      <div className="sl-content">
                        <div className="sl-date text-muted">
                          Your leave starts from
                        </div>
                        <div>{this.state.form_from_date}</div>
                      </div>
                    </div>
                    <div className="sl-item b-info">
                      <div className="sl-content">
                        <div style={{ width: width }}>
                          <select
                            value={this.state.leaveType}
                            onChange={e => {
                              this.setState({ leaveType: e.target.value });
                            }}
                            className="form-control"
                            required
                          >
                            <option value="" disabled>
                              Select Option
                            </option>
                            <option value="Casual Leave"> Casual Leave </option>
                          </select>
                        </div>
                      </div>
                    </div>
                    <div className="sl-item b-info">
                      <div className="sl-content">
                        <div className="sl-date text-muted">No. of days</div>
                        <div>{this.state.form_no_of_days}</div>
                        <div>
                          <small style={{ fontSize: "12px" }}>
                            {apply_half_day_button_1}
                          </small>
                          <small style={{ fontSize: "12px" }}>
                            {apply_half_day_button_2}
                          </small>
                        </div>
                      </div>
                    </div>
                    {dateDiff > 0 ? (
                      <div className="sl-item b-warning">
                        <div className="sl-content">
                          <div className="sl-date text-muted">
                            Reason For Late Applying
                          </div>
                          <div className="form-group">
                            <input
                              type="text"
                              className="form-control"
                              onChange={e =>
                                this.setState({ late_reason: e.target.value })
                              }
                              value={this.state.late_reason}
                              required
                            />
                          </div>
                        </div>
                      </div>
                    ) : null}
                    <div className="sl-item b-warning">
                      <div className="sl-content">
                        <div className="sl-date text-muted">Reason</div>
                        <div className="form-group">
                          <input
                            className="form-control"
                            type="text"
                            ref="reason"
                            onChange={() =>
                              this.setState({
                                form_reason: this.refs.reason.value
                              })
                            }
                            value={this.state.form_reason}
                          />
                        </div>
                      </div>
                    </div>
                    <div className="sl-item b-success">
                      <div className="sl-icon">
                        <i className="fa fa-check red" />
                      </div>
                      <div className="sl-content">
                        <div className="sl-date text-muted">
                          Your leave ends on
                        </div>
                        <div>{this.state.form_to_date}</div>
                      </div>
                    </div>
                  </div>
                </div>

                <div className="form-group row m-t-md">
                  <div className="text-center">
                    <Button
                      type="submit"
                      color="success"
                      size="md"
                      onClick={this.doApplyLeave}
                    >
                      Apply Leave
                    </Button>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </Dialog>
      </>
    );
  }
}
