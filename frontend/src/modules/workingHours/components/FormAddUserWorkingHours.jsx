import React from "react";
import $ from "jquery";
import { Calendar } from "react-date-range";
import "timepicker/jquery.timepicker.css";
import "timepicker/jquery.timepicker.min.js";
import { Button } from "reactstrap";

class FormAddUserWorkingHours extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      userid: "",
      date: "",
      working_hours: "",
      reason: ""
    };
    this.handleDateChange = this.handleDateChange.bind(this);
  }

  componentDidMount() {
    $(".timepickerInput").timepicker({
      minTime: "5:00",
      maxTime: "12:00",
      timeFormat: "h:i",
      step: 10
    });
  }

  handleDateChange(date) {
    let selectedDate = date.format("YYYY-MM-DD");
    this.setState({ date: selectedDate });
  }

  componentWillReceiveProps(props) {
    this.setState({ userid: props.userid });
  }

  render() {
    return (
      <div className="row no-gutter">
        <div className="col-md-6 col-xs-12">
          <div className="p-a block text-center table-responsive">
            <h6 className="">Select Date</h6>
            <Calendar
              onInit={this.handleDateChange}
              onChange={this.handleDateChange}
            />
          </div>
        </div>
        <div className="col-md-6 col-xs-12">
          <div className="p-a block text-center">
            <h6 className="pb-4">Selected Date</h6>
            <div className="d-flex pb-2">
              <div className="col-xs-5 workingHourData">
                <h6>Date</h6>
              </div>
              <div className="col-xs-7 workingHourTextbox">
                {" "}
                {this.state.date}
              </div>
            </div>

            <div className="d-flex pb-2">
              <div className="col-xs-5 workingHourData">
                <h6>Select Time</h6>
              </div>
              <div className="col-xs-7  workingHourTextbox ">
                {" "}
                <input
                  type="text"
                  placeholder="Select Time"
                  className="timepickerInput bod-none w-100"
                  ref="workingtime"
                  onBlur={() =>
                    this.setState({
                      working_hours: this.refs.workingtime.value
                    })
                  }
                  value={this.state.working_hours}
                />{" "}
              </div>
            </div>

            <div className="d-flex pb-2">
              <div className="col-xs-5 workingHourData">
                <h6>
                  Enter <br /> Reason
                </h6>
              </div>
              <div className="col-xs-7">
                <textarea
                  type="text"
                  placeholder="Reason"
                  className="md-input w-100 form-control"
                  ref="reason"
                  onChange={() =>
                    this.setState({ reason: this.refs.reason.value })
                  }
                  value={this.state.reason}
                />
              </div>
            </div>

            <div className="offset-5">
              <Button
                color="success"
                className='w-100'
                onClick={() =>
                  this.props.callAddUserWorkingHours(
                    this.state.userid,
                    this.state.date,
                    this.state.working_hours,
                    this.state.reason
                  )
                }
              >
                Add
              </Button>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default FormAddUserWorkingHours;
