import React from "react";
import _ from "lodash";
import PropTypes from "prop-types";
import { CONFIG } from "../../../config/index";
import { notify } from "../../../services/notify";
import PendingHourSummary from "./PendingHourSummary";
import AddUserPendingHour from "./AddUserPendingHour";
import AddAsLeaveHour from "./AddAsLeaveHour";
import AddAsHalfDayLeave from "./AddAsHalfDayLeave";
import { Button, Input } from "reactstrap";
import PendingHoursRow from "./PendingHoursRow";
import { Spinner } from "reactstrap";

export default class UserPendingHoursList extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      pendingTimeList: {},
      usersList: [],
      openMerge: false,
      openLeave: false,
      dropDownValue: "",
      buttonClicked: ""
    };
  }

  componentWillMount() {
    this.setState({
      pendingTimeList: this.props.manageUserPendingHours,
      usersList: this.props.manageUserPendingHours.displayData.user_list
    });
  }

  componentWillReceiveProps(props) {
    if (!props.loggedUser.isLoggedIn) {
      this.props.history.push("/logout");
    } else {
      if (props.loggedUser.data.role === CONFIG.ADMIN) {
      } else {
        this.props.history.push("/home");
      }
    }
    this.setState({
      pendingTimeList: props.manageUserPendingHours,
      usersList: props.manageUserPendingHours.displayData.user_list
    });
  }

  buttonClicked = (check) =>{
    this.setState({buttonClicked: check})
  }

  callAddUserPendingHours = (
    userId,
    pendingHour,
    date,
    reason,
    empId,
    year,
    month
  ) => {
    this.props
      .onAddUserPendingHours(
        userId,
        pendingHour,
        date,
        reason,
        empId,
        year,
        month
      )
      .then(
        message => {
          this.setState({
            reason: ""
          });
          this.handleCloseMerge();
        },
        error => {
          notify("Error !", error, "error");
        }
      );
  };
  handleOpenMerge = () => {
    this.setState({
      openMerge: true
    });
  };
  handleCloseMerge = () => {
    this.setState({
      openMerge: false,
      reason: ""
    });
  };

  handleOpenLeave = () => {
    this.setState({
      openLeave: true
    });
  };
  handleCloseLeave = () => {
    this.setState({
      openMerge: false,
      reason: ""
    });
  };

  render() {
    // Map UserPendingDetails -->
    var pendingList = this.state.usersList || [];

    let pendingTimeMap = pendingList.map((val, i) => {
      return (
        <PendingHoursRow
          row={val}
          slNo={i}
          {...this.props}
          callAddUserPendingHours={this.callAddUserPendingHours}
          onbuttonClicked = {this.buttonClicked}
        />
      );
    });

    return (
      <>
        {this.props &&
        !Array.isArray(this.props.manageUserPendingHours.displayData) ? (
          <PendingHourSummary
            callFetchPendingTime={this.callFetchPendingUserList}
            manageUserPendingHours={this.props.manageUserPendingHours}
            onUserPendingHoursData={this.props.onUserPendingHoursData}
            isLoading = {this.props.frontend.show_loading}
            buttonClicked = {this.state.buttonClicked}
            onbuttonClicked = {this.buttonClicked}
            {...this.props}
          />
        ) : (
          <div className="fc-toolbar" style={{ width: "100%" }}>
            <Spinner color="light" style={{ marginTop: "0.6rem" }} />
          </div>
        )}
        <div className="col-md-12 b-r box table-responsive">
          <div className="p-a block">
            {pendingTimeMap.length > 0 ? (
              <table key="" className="table table-striped">
                <thead className="active">
                  <tr>
                    <th className="p-r-xs">{"Sr."}</th>
                    <th>User Name</th>
                    <th className="text-center">Pending Time</th>
                    <th>Status</th>
                    <th>Actions</th>
                  </tr>
                </thead>
                <tbody>{pendingTimeMap}</tbody>
              </table>
            ) : (
              <div>
                <h5 className="text-center">{"Not Updated !"}</h5>
              </div>
            )}
          </div>
        </div>
      </>
    );
  }
}
