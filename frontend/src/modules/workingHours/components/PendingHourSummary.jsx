import React, { useState } from "react";
import PropTypes from "prop-types";
import { Spinner } from "reactstrap";

const PendingHourSummary = ({
  manageUserPendingHours,
  onUserPendingHoursData,
  isLoading,
  buttonClicked,
  onbuttonClicked
}) => {
  const onChangeMonthData = check => {
    let pendingData = manageUserPendingHours.displayData;
    let year = "";
    let month = "";

    if (check === "previous") {
      onbuttonClicked(check)
      if (Object.keys(pendingData).length > 0) {
        year = pendingData.previousMonth.year;
        month = pendingData.previousMonth.month;
      }
      onUserPendingHoursData(year, month);
    } else if (check === "next") {
      onbuttonClicked(check);
      if (Object.keys(pendingData).length > 0) {
        year = pendingData.nextMonth.year;
        month = pendingData.nextMonth.month;
      }
      onUserPendingHoursData(year, month);
    }
  };
  return (
    <>
      <div
        className="fullcalendar fc fc-ltr fc-unthemed"
        style={{ width: "100%" }}
      >
        <div className="fc-toolbar">
          <div className="fc-left">
            {isLoading && buttonClicked === "previous" ? (
              <button
                type="button"
                className="fc-prev-button fc-button fc-state-default fc-corner-left fc-corner-right"
              >
                <Spinner color="light" />
              </button>
            ) : (
              <button
                type="button"
                className="fc-prev-button fc-button fc-state-default fc-corner-left fc-corner-right"
                onClick={() => onChangeMonthData("previous")}
              >
                <span className="fc-icon fc-icon-left-single-arrow" />
              </button>
            )}
          </div>
          <div className="fc-right">
            {isLoading && buttonClicked === "next" ? (
              <button
                type="button"
                className="fc-prev-button fc-button fc-state-default fc-corner-left fc-corner-right"
              >
                <Spinner color="light" />
              </button>
            ) : (
              <button
                type="button"
                className="fc-next-button fc-button fc-state-default fc-corner-left fc-corner-right"
                onClick={() => onChangeMonthData("next")}
              >
                <span className="fc-icon fc-icon-right-single-arrow" />
              </button>
            )}
          </div>
          <div className="fc-center">
            <h2>
              {manageUserPendingHours.displayData.monthName}-{manageUserPendingHours.displayData.year}
            </h2>
          </div>
          <div className="fc-clear" />
        </div>
      </div>
    </>
  );
};
PendingHourSummary.propTypes = {
  manageUserPendingHours: PropTypes.shape({
    previousMonth: PropTypes.object.isRequired,
    nextMonth: PropTypes.object.isRequired,
    monthName: PropTypes.string.isRequired,
    year: PropTypes.number.isRequired
  }).isRequired
};
export default PendingHourSummary;

