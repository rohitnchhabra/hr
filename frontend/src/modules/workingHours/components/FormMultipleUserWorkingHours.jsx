import React from "react";
import $ from "jquery";
import "timepicker/jquery.timepicker.css";
import "timepicker/jquery.timepicker.min.js";
import { Button } from "reactstrap";
import GenericDatePicker from "../../../components/generic/GenericDatePicker";
import { weekDays } from "../../../helper/helper";
import moment from "moment";

class FormMultipleUserWorkingHours extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      userid: "",
      week_day: "",
      week_of_month: "",
      day_type: "",
      working_hours: "",
      date_start: moment().format("YYYY-MM-DD"),
      date_end: moment().format("YYYY-MM-DD"),
      reason: ""
    };
    this.handleDateChange = this.handleDateChange.bind(this);
  }

  componentDidMount() {
    $(".timepickerInput").timepicker({
      minTime: "5:00",
      maxTime: "12:00",
      timeFormat: "h:i",
      step: 10
    });
  }

  handleDateChange(date) {
    let selectedDate = date.format("YYYY-MM-DD");
    this.setState({ date: selectedDate });
  }

  componentWillReceiveProps(props) {
    this.setState({ userid: props.userid });
  }

  render() {
    return (
      <>
        <div className="row no-gutter">
          <div className="col-md-6 col-xs-12">
            <div className="p-a block text-center">
              <div className="d-flex pb-2">
                <div className="col-xs-5 workingHourData">
                  <h6>From</h6>
                </div>
                <div className="col-xs-7 add-working">
                  {" "}
                  <GenericDatePicker
                    date={this.state.date_start}
                    handleDateChange={date =>
                      this.setState({
                        date_start: moment(date).format("YYYY-MM-DD")
                      })
                    }
                    type={this.props.isMobile < 768 ? "absolute" : "inline"}
                  />{" "}
                </div>
              </div>

              <div className="d-flex pb-2">
                <div className="col-xs-5 workingHourData">
                  <h6>Day</h6>
                </div>
                <div className="col-xs-7">
                  <select
                    className="form-control"
                    value={this.state.week_day}
                    onChange={e => {
                      this.setState({ week_day: e.target.value });
                    }}
                  >
                    <option value="">Select a day</option>
                    {weekDays.map((dat, i) => {
                      return <option value={dat}>{dat}</option>;
                    })}
                  </select>
                </div>
              </div>

              <div className="d-flex pb-2">
                <div className="col-xs-5 workingHourData">
                  <h6>Day Type</h6>
                </div>
                <div className="col-xs-7">
                  <select
                    className="form-control"
                    value={this.state.day_type}
                    onChange={e => {
                      this.setState({ day_type: e.target.value });
                    }}
                  >
                    <option value="">Select day type</option>
                    {["Working", "Non Working"].map((dat, i) => {
                      return <option value={dat}>{dat}</option>;
                    })}
                  </select>
                </div>
              </div>
            </div>
          </div>
          <div className="col-md-6 col-xs-12">
            <div className="p-a block text-center">
              <div className="d-flex pb-2">
                <div className="col-xs-5 workingHourData">
                  <h6>To</h6>
                </div>
                <div className="col-xs-7 add-working">
                  {" "}
                  <GenericDatePicker
                    date={this.state.date_end}
                    handleDateChange={date =>
                      this.setState({
                        date_end: moment(date).format("YYYY-MM-DD")
                      })
                    }
                    type={this.props.isMobile < 768 ? "absolute" : "inline"}
                  />{" "}
                </div>
              </div>

              <div className="d-flex pb-2">
                <div className="col-xs-5 workingHourData">
                  <h6>Week of the Month</h6>
                </div>
                <div className="col-xs-7">
                  <select
                    className="form-control"
                    value={this.state.week_of_month}
                    onChange={e => {
                      this.setState({ week_of_month: e.target.value });
                    }}
                  >
                    <option value="">Select week of the month</option>
                    {[
                      { val: 1, name: "1st" },
                      { val: 2, name: "2nd" },
                      { val: 3, name: "3rd" },
                      { val: 4, name: "4th" },
                      { val: 5, name: "5th" }
                    ].map((dat, i) => {
                      return <option value={dat.val}>{dat.name}</option>;
                    })}
                  </select>
                </div>
              </div>

              <div className="d-flex pb-2">
                <div className="col-xs-5 workingHourData">
                  <h6>Working Hours</h6>
                </div>
                <div className="col-xs-7  workingHourTextbox ">
                  {" "}
                  <input
                    type="text"
                    placeholder="Select Time"
                    className="timepickerInput bod-none w-100"
                    ref="workinghours"
                    onBlur={() =>
                      this.setState({
                        working_hours: this.refs.workinghours.value
                      })
                    }
                    value={this.state.working_hours}
                  />{" "}
                </div>
              </div>
            </div>
          </div>

          <div className="col-md-12 col-xs-12">
            <div className="p-a block text-center">
              <div className="d-flex pb-2">
                <div className="col-xs-5 workingHourData">
                  <h6>
                    Reason
                  </h6>
                </div>
                <div className="col-xs-7">
                  <textarea
                    type="text"
                    placeholder="Reason"
                    className="md-input w-100 form-control"
                    ref="other_reason"
                    onChange={() =>
                      this.setState({
                        reason: this.refs.other_reason.value
                      })
                    }
                    value={this.state.reason}
                  />
                </div>
              </div>
            </div>
          </div>

          <div className="col-md-12 col-xs-12">
            <div className="p-a block text-center">
              <Button
                color="success"
                className="w-100"
                onClick={() =>
                  this.props.callMultipleAddUserWorkingHours(
                    this.state.userid,
                    this.state.week_day,
                    this.state.week_of_month,
                    this.state.day_type,
                    this.state.working_hours,
                    this.state.date_start,
                    this.state.date_end,
                    this.state.reason
                  )
                }
              >
                Add
              </Button>
            </div>
          </div>
        </div>
      </>
    );
  }
}

export default FormMultipleUserWorkingHours;
