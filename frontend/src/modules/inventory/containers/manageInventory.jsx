import React from "react";
import { connect } from "react-redux";
import { withRouter } from "react-router";
import _ from "lodash";
import { notify } from "../../../services/notify";
// import { isNotUserValid } from "../../../services/generic";
import InventoryList from "../../../modules/inventory/components/InventoryList";
import * as actionsManageDevice from "../../../redux/inventory/actions/inventory";
import * as actionsUsersList from "../../../redux/generic/actions/usersList";
import * as actionsManageUsers from "../../../redux/manageUsers/actions/manageUsers";
import * as actions from '../../../redux/actions'
import Heading from "../../../components/generic/Heading";
import { Button } from "reactstrap";

class InventorySystem extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      defaultUserDisplay: "",
      search: "",
      status_message: "",
      active: "active",
      firstArrow: "show",
      secondArrow: "hidden",
      thirdArrow: "hidden",
      deviceList: "",
      viewUser: "hidden",
      viewUserNew: "hidden",
      fourthArrow: "hidden",
      open: false,
      edit: false,
      deviceId: "",
      user_profile_detail: {},
      user_assign_machine: [],
      getByIdData: {},
      unapprovedList: {},
      openUnapprove: "",
      selectedTab: false,
      activeSecondTab: "tab21"
    };
  }
  componentWillMount() {
    this.props.onFetchDevice();
    this.props.onUsersList();
    this.props.onFetchDeviceType();
    this.props.onFetchDeviceStatus();
    this.props.onFetchDeviceCount();
    this.props.onFetchUnapprovedUser();
    this.props.showHeading(`${this.props.match.params.device
      .charAt(0)
      .toUpperCase() + this.props.match.params.device.slice(1)}s`)
  }
  componentWillReceiveProps(props) {
    if (!_.isEqual(this.props, props)) {
      // let isNotValid = isNotUserValid(
      //   this.props.location.pathname,
      //   props.loggedUser
      // );
      this.setState({
        username: props.manageUsers.username,
        user_profile_detail: props.manageUsers.user_profile_detail,
        user_assign_machine: props.manageUsers.user_assign_machine
      });
      if (props.manageDevice.showTab) {
        this.openPage("unapproved_user");
        this.setState({ selectedTab: true });
        this.props.onNoTab();
      }
    }
  }

  componentDidUpdate() {
    if (this.state.defaultUserDisplay === "") {
      if (this.props.usersList.users && this.props.usersList.users.length > 0) {
        let firstUser = this.props.usersList.users[0];
        let defaultUserId = firstUser.user_Id;
        let defaultUserName = firstUser.username;
        this.onUserClick(defaultUserId, defaultUserName);
      }
    }
  }
  onUserClick = (userid, username) => {
    let selectedUserName = "";
    let selectedUserImage = "";
    let selectedUserJobtitle = "";
    let selectedUserId = "";
    if (this.props.usersList.users.length > 0) {
      let userDetails = _.find(this.props.usersList.users, { user_Id: userid });
      if (typeof userDetails !== "undefined") {
        selectedUserName = userDetails.name;
        selectedUserImage = userDetails.slack_profile.image_192;
        selectedUserJobtitle = userDetails.jobtitle;
        selectedUserId = userDetails.user_Id;
      }
    }
    this.setState({
      defaultUserDisplay: userid,
      selectedUserName: selectedUserName,
      selectedUserImage: selectedUserImage,
      selectedUserJobtitle: selectedUserJobtitle,
      selectedUserId: selectedUserId
    });
    this.props.onUserProfileDetails(userid, username);
  };

  callUpdateUserDeviceDetails = newDeviceDetails => {
    this.props.onUpdateUserDeviceDetails(newDeviceDetails).then(
      data => {},
      error => {
        notify("Error !", error, "error");
      }
    );
  };
  callFetchDeviceType = () => {
    this.onFetchDeviceType();
  };
  callFetchDeviceStatus = () => {
    this.onFetchDeviceStatus();
  };
  editAction = (device, edit, open) => {
    this.props.onUserEditData(device, edit, open);
  };
  openPage = (toDisplay, activeSecondTab) => {
    this.setState({ activeSecondTab });
    if (toDisplay === "device_list") {
      this.setState({
        deviceList: "",
        firstArrow: "show",
        viewUser: "hidden",
        viewUserNew: "hidden",
        secondArrow: "hidden",
        thirdArrow: "hidden",
        thirdArrow: "hidden",
        fourthArrow: "hidden"
      });
    } else if (toDisplay === "view_user") {
      this.setState({
        deviceList: "hidden",
        firstArrow: "hidden",
        viewUser: "row",
        secondArrow: "show",
        thirdArrow: "hidden",
        viewUserNew: "hidden",
        thirdArrow: "hidden",
        fourthArrow: "hidden"
      });
    } else if (toDisplay === "view_user_new") {
      this.setState({
        deviceList: "hidden",
        firstArrow: "hidden",
        viewUser: "hidden",
        viewUserNew: "hidden",
        secondArrow: "hidden",
        thirdArrow: "show",
        thirdArrow: "hidden",
        fourthArrow: "hidden"
      });
      this.props.history.push("/app/inventoryOverviewDetail");
    } else if (toDisplay === "unapproved_user") {
      this.setState({
        deviceList: "show",
        firstArrow: "hidden",
        viewUser: "hidden",
        secondArrow: "hidden",
        thirdArrow: "hidden",
        viewUserNew: "hidden",
        fourthArrow: "show"
      });
    }
  };

  openEditDevice = id => {
    this.props.onGetDeviceById(id).then(val => {
      this.setState({
        edit: true,
        open: true,
        deviceId: id,
        status_message: "",
        getByIdData: val
      });
    });
  };
  deleteDevices = id => {
    this.props.onDeleteDevice(id).then(val => {
      this.setState({
        status_message: ""
      });
      this.props.onFetchDevice();
    });
  };
  handleClose = () => {
    this.setState({
      open: false,
      status_message: "",
      edit: false,
      id: "",
      machine_type: "",
      machine_name: "",
      machine_price: "",
      serial_no: "",
      purchase_date: "",
      operating_system: "",
      status: "",
      comment: "",
      warranty_comment: "",
      repair_comment: "",
      warranty: "",
      user_Id: ""
    });
  };
  addDevice = () => {
    this.setState({
      edit: false,
      open: true
    });
    this.props.onUserEditData({}, this.state.edit, this.state.open);
    this.props.history.push("/app/addInventory");
  };

  handleAddDialog = () => {
    this.setState({
      deviceId: "",
      open: true,
      status_message: "",
      edit: false
    });
  };
  callAssign = (id, userId) => {
    this.setState({ user: userId });
    this.props.onCallAssign(id, userId).then(
      message => {
        this.setState({
          status_message: message
        });
        this.props.onFetchDevice();
      },
      error => {
        notify("Error !", error, "");
      }
    );
  };
  unapprovedList = () => {
    this.setState({
      openUnapprove: "true",
      unapprovedList: this.props.manageDevice.unapprovedList
    });
  };
  callUnapprovedId = id => {
    this.props.onFetchApprovedUser(id.id);
  };
  toggleSecondTabs = tab => {
    if (this.state.activeSecondTab !== tab) {
      this.setState({
        activeSecondTab: tab
      });
    }
  };

  handleDeleteSoldDevice = (e,name) => {
    e.stopPropagation();
    this.props.onCallDeleteSoldDeviceGroup(name);
  };

  render() {
    console.log(this.props.manageDevice.deviceList,"pppppppppp")
    let { isGetData } = this.props.manageDevice;
    return (
      <div id="content" className="app-content box-shadow-z0" role="main">
        <div className="app-body" id="view">
          <div className="pt-2">
            <div className={this.state.deviceList}>
              {this.props.manageDevice.deviceList.length ? (
                <InventoryList
                  openEditDevice={this.openEditDevice}
                  deleteDevices={this.deleteDevices}
                  fourthArrow={this.state.fourthArrow}
                  unapproveList={this.unapprovedList}
                  callUnapprovedId={this.callUnapprovedId}
                  editAction={this.editAction}
                  deviceTypeData={val => {
                    this.setState({
                      search: val
                    });
                  }}
                  onFetchDeviceType={this.props.onFetchDeviceType}
                  onFetchDeviceStatus={this.props.onFetchDeviceStatus}
                  usersList={this.props.usersList}
                  match={this.props.match}
                  manageDevice={this.props.manageDevice}
                  onGetDevice={this.props.onGetDevice}
                  onAssignDevice={this.props.onAssignDevice}
                  onAddInventoryComment={this.props.onAddInventoryComment}
                  onFetchDevice={this.props.onFetchDevice}
                  onCallDeviceType={this.props.onCallDeviceType}
                  history={this.props.history}
                  deviceList={this.props.manageDevice.deviceList}
                  onCallDeviceStatus={this.props.onCallDeviceStatus}
                  onDeleteDeviceStatus={this.props.onDeleteDeviceStatus}
                  onUpdateDevice={this.props.onUpdateDevice}
                  isGetData={isGetData}
                  location={this.props.location}
                  handleDeleteSoldDevice={this.handleDeleteSoldDevice}
                  addDevice={this.addDevice}
                />
              ) : null}
            </div>
          </div>
        </div>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    frontend: state.frontend.toJS(),
    usersList: state.usersList.toJS(),
    manageUsers: state.manageUsers.toJS(),
    loggedUser: state.logged_user.userLogin,
    manageDevice: state.manageDevice.toJS()
  };
}
const mapDispatchToProps = dispatch => {
  return {
    onGetDevice: id => {
      return dispatch(actionsManageDevice.getDeviceById(id));
    },
    onUsersList: () => {
      return dispatch(actionsUsersList.get_users_list());
    },
    onUserProfileDetails: (pathName, userid, username) => {
      return dispatch(
        actionsManageUsers.getUserProfileDetails(pathName, userid, username)
      );
    },
    onUpdateUserDeviceDetails: newDeviceDetails => {
      return dispatch();
      // actionsManageUsers.updateUserDeviceDetails(newDeviceDetails)
    },
    onAddNewMachine: newMachineDetails => {
      return dispatch(actionsManageDevice.addNewMachine(newMachineDetails));
    },
    onFetchDevice: () => {
      return dispatch(actionsManageDevice.get_machines_detail());
    },
    onGetDeviceById: id => {
      return dispatch(actionsManageDevice.getDeviceById(id));
    },
    onUpdateDevice: (id, machineData) => {
      return dispatch(actionsManageDevice.updateDevice(id, machineData));
    },
    onDeleteDevice: id => {
      return dispatch(actionsManageDevice.deleteDevice(id));
    },
    onCallAssign: (deviceId, id) => {
      return dispatch(actionsManageDevice.assignDevice(deviceId, id));
    },
    onCallDeviceType: deviceList => {
      return dispatch(actionsManageDevice.assignDeviceType(deviceList));
    },
    onCallDeviceStatus: (statusValue, colorValue) => {
      return dispatch(
        actionsManageDevice.assignDeviceStatus(statusValue, colorValue)
      );
    },
    onFetchDeviceType: () => {
      return dispatch(actionsManageDevice.getDeviceType());
    },
    onFetchDeviceStatus: () => {
      return dispatch(actionsManageDevice.getDeviceStatus());
    },
    onDeleteDeviceStatus: (checkValue, new_status) => {
      return dispatch(
        actionsManageDevice.deleteDeviceStatus(checkValue, new_status)
      );
    },
    onFetchDeviceCount: () => {
      return dispatch(actionsManageDevice.deviceCount());
    },
    onFetchUnapprovedUser: () => {
      return dispatch(actionsManageDevice.unapprovedUser());
    },
    onFetchApprovedUser: id => {
      return dispatch(actionsManageDevice.approvedUser(id));
    },
    onUserEditData: (device, edit, open) => {
      return dispatch(actionsManageDevice.editDeviceData(device, edit, open));
    },
    onNoTab: () => {
      return dispatch(actionsManageDevice.noTab());
    },
    onGetDevice: device_id => {
      return dispatch(actionsManageDevice.getDeviceById(device_id));
    },
    onAssignDevice: assign_device => {
      return dispatch(actionsManageDevice.assignDevice(assign_device));
    },
    onAddInventoryComment: add_inventory_comment => {
      return dispatch(
        actionsManageDevice.addInventoryComment(add_inventory_comment)
      );
    },
    onFetchDevice: () => {
      return dispatch(actionsManageDevice.get_machines_detail());
    },
    onCallDeleteSoldDeviceGroup: name => {
      return dispatch(actionsManageDevice.deleteSoldDeviceGroup(name));
    },
    showHeading: data => {
      return dispatch(actions.showHeading(data));
    }
  };
};

const VisibleInvetorySystem = connect(
  mapStateToProps,
  mapDispatchToProps
)(InventorySystem);

const RouterVisibleInventorySystem = withRouter(VisibleInvetorySystem);

export default RouterVisibleInventorySystem;
