import React from "react";
import { connect } from "react-redux";
import { withRouter } from "react-router";
import { notify,confirm } from "../../../services/notify";
import { isNotUserValid } from "../../../services/generic";
import DeviceDetails from "../../../components/inventory/deviceDetails";
import * as actionsMyProfile from "../../../redux/myProfile/actions/myProfile";
import * as actions from "../../../redux/actions";
import AlertFader from "../../../components/generic/AlertFader";
import * as actionsManageDevice from "../../../redux/inventory/actions/inventory";
import AssignDevice from "../../../modules/inventory/components/AssignDevice";
import * as actionsUsersList from "../../../redux/generic/actions/usersList";
import {
  Button,
  Alert,
  Modal,
  ModalBody,
  ModalFooter,
  ModalHeader,
  Spinner
} from "reactstrap";
import map from "lodash/map";
import filter from "lodash/filter";
import "../../../../../frontend/src/styles/assets/my_inventory.scss";
import Heading from "../../../components/generic/Heading";
import { compose } from "redux";
import isMobile from "../../../components/hoc/WindowResize";
import cloneDeep from 'lodash/cloneDeep'

class MyInventory extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      user_id: [],
      status_message: "",
      openUnassigned: false,
      openAssigned: false,
      user_profile_detail: {},
      user_assign_machine: [],
      device: [],
      unassignDeviceList: [],
      activeAuditId: "",
      auditMsg: "",
      activeItemName: "",
      show_alert_message: false,
      activeAudits: [],
      activeUnassign: [],
      isAssignLoading: false,
      loading: true,
      large: false,
      audit_comment_type: "",
      unassgnMessage:"",
      userId:'',
      inventory_id:'',
      inputBox:false,
      usersList:[],
      isUserListsNmachines:false,
      isMachines:false
    };
    this.count=0
  }
  componentWillMount() {
    this.props.getGenericConfig();
    this.props.onUnassignDeviceList();
    this.props.onMyProfileDetails();
    this.props.onGetInventoryDetails();
    this.props.onGetMyInventory().then(() => {
      this.setState({ loading: false });
    });
    this.props.showHeading("My Inventory")
  }

  handleSkipAudit=()=>{
    this.props.onGetMyInventory(true).then(() => {
      this.setState({ loading: false });
    });
  }

  componentWillReceiveProps(props) {
    let isNotValid = isNotUserValid(
      this.props.location.pathname,
      props.loggedUser
    );
    let action_type = props.loggedUser.data.role_actions.includes("get_enable_user")
    if((action_type)) {
      this.count++;
      if(this.count === 1) {
        this.props.onUsersList();
      }
    }
    
    if (isNotValid.status && isNotValid.redirectTo !== "my_inventory") {
      this.props.history.push(isNotValid.redirectTo);
    }
    this.setState({
      user_profile_detail: props.myProfile.user_profile_detail,
      user_assign_machine:isNotValid.status && isNotValid.redirectTo == "my_inventory" ? 
                      ( props.myProfile.myInventory &&
                        props.myProfile.myInventory.length &&
                        props.myProfile.myInventory.filter((inventory, i)=>
                        inventory.audit_current_month_status.status == false ||
                        inventory.is_unassign_request == 1 ||
                        (inventory.hasOwnProperty('is_ownership_change_req_handler') &&
                         inventory.is_ownership_change_req_handler ==1))
                      ):
                        props.myProfile.myInventory,
      show_alert_message: true
                           });
    if (props && props.myProfile && props.myProfile.myInventory) {
      map(props.myProfile.myInventory, val => {
        if (!val.audit_current_month_status.status) {          
          this.setState({ show_alert_message: false });
        }
      });
    }
  }

  componentDidUpdate(){
    const {user_assign_machine, isUserListsNmachines} = this.state;
    const {users} = this.props.usersList;
    if(users && users.length > 0 && user_assign_machine && user_assign_machine.length > 0 && !isUserListsNmachines){
      let isOwnerShipRequest  = user_assign_machine[0].hasOwnProperty('is_ownership_change_req_handler') && user_assign_machine[0].is_ownership_change_req_handler ==1 ? true : false
      if(isOwnerShipRequest){
        let userList = cloneDeep(this.props.usersList.users);      
        let user = userList.find((item,i)=>item.user_Id == user_assign_machine[0].ownership_change_req_by_user);
        userList = userList.filter((item,i)=>item.user_Id !== user_assign_machine[0].ownership_change_req_by_user);      
        userList.splice(0,0,{...user})
        this.setState({usersList:userList})
      }
        this.setState({
          userId:isOwnerShipRequest ? 
          user_assign_machine[0].ownership_change_req_by_user :
          user_assign_machine[0].user_Id,
          inventory_id:user_assign_machine[0].id,
          isUserListsNmachines:true
        })
    }
  }

  callUpdateUserDeviceDetails = newDeviceDetails => {
    this.props.onUpdateDeviceDetails(newDeviceDetails);
  };
  callAssignDevice = assign_device => {
    this.handleCloseAssign();
    this.props.onAssignDevice(assign_device).then(
      data => {
        this.setState({ loading: true });
        this.props.onGetMyInventory().then(() => {
          this.setState({ loading: false });
        });
        notify("Success!", data, "success");
      },
      error => {
        notify("Error!", error, "error");
      }
    );
  };
  unassignDevice = val => {
    this.setState({
      openUnassigned: true,
      status_message: "",
      device: val
    });
  };
  handleClose = () => {
    this.setState({
      openUnassigned: false,
      status_message: ""
    });
  };
  handleCloseAssign = () => {
    this.setState({
      openAssigned: false,
      status_message: ""
    });
  };
  handleAuditType = (e, activeAuditId) => {
    let auditValue =  e ? e.target.value : '';
    this.setState({
      audit_comment_type: auditValue,
      inputBox: auditValue === "issue" ||
      auditValue === "critical_issue" ? true : false

      // activeAudits: activeBtn
    });
  }
  handleAddDialog = () => {
    this.setState({
      user_id: this.props.loggedUser.data.id,
      unassignDeviceList: this.props.unassignedDeviceList.unassignedDeviceList,
      openAssigned: true,
      status_message: ""
    });
  };
  callAddUserComment = addUserCommentDetails => {
    const activeBtn = this.state.activeUnassign.concat(
      addUserCommentDetails.inventory_id
    );
    this.setState({
      activeUnassign: activeBtn,
      isAssignLoading: true
    });
    this.props.onAddUserComment(addUserCommentDetails).then((data) => {
      this.setState({
        openUnassigned: false,
        status_message: "",
        unassgnMessage:data,
      },()=>{
        this.setState({unassgnMessage:''})
      });
      this.props.onUnassignDeviceList();
      this.props.onGetMyInventory().then(() => {
        const remainActive = filter(this.state.activeUnassign, function(n) {
          return n !== addUserCommentDetails.inventory_id;
        });
        this.setState({
          activeUnassign: remainActive,
          isAssignLoading: false
        });
      });
    });
  };
  handleAuditClick = (id, val) => {
    this.toggle(id);
    this.setState({
      activeAuditId: val.id,
      activeItemName: val.machine_name
    });
  };
  AssignDevice = () => {
    let assign_device = {
      user_id:this.state.userId,
      inventory_id:this.state.inventory_id
    }   
    confirm('Are you sure ?', "Do you want to assign this device?" , 'warning').then((res) => {
      if (res) {
        this.props.onAssignDevice(assign_device).then ( () => {
          this.props.onGetMyInventory().then( ()=> {
            this.setState({ loading: false,user_assign_machine: this.props.myProfile.myInventory ? this.props.myProfile.myInventory : ["No Inventory Assign"] });
          })
        })
      }
    });
  }

  toggle = id => {
    this.setState(prevState => ({
      [id]: !prevState[id]
    }));
  };
  handleAuditSubmit = (id, activeAuditId, auditMsg, auditType) => {
    const activeBtn = this.state.activeAudits.concat({id:activeAuditId,status:auditType});
    this.setState({
      activeAudits: activeBtn,
      inputBox:false

    });
    // this.toggle(id);
    this.props
      .onAddAuditComment(activeAuditId, auditMsg, auditType)
      .then(res => {
        this.props.onGetMyInventory();
        this.setState({ audit_comment_type: "" });
      })
      .catch(() => {
        const remainActive = filter(this.state.activeAudits, function(n) {
          return n !== activeAuditId;
        });
        this.setState({
          activeAudits: remainActive
        });
      });
    this.setState({
      auditMsg: ""
    });
  };

  handleDropdown=(id,auditStatus)=>{
    if(this.state.activeAudits.find((item,i)=>{
      return item.id==id
    }) && auditStatus==false && this.props.showPending){
      let newItem =this.state.activeAudits.filter((val,i)=>{
        return val.id==id
      })
      this.setState({
        audit_comment_type:newItem[0].status
      })
    }
    else{
      this.setState({
        audit_comment_type:""
      })
    }
  }


  handleCardClick=(details)=>{        
    let isOwnerShipRequest  = details.hasOwnProperty('is_ownership_change_req_handler') && details.is_ownership_change_req_handler ==1 ? true : false
    this.setState({
      userId:isOwnerShipRequest ? details.ownership_change_req_by_user :  details.user_Id,
      inventory_id:details.id
    })
    if(isOwnerShipRequest){
      let userList = cloneDeep(this.props.usersList.users);      
      let user = userList.find((item,i)=>item.user_Id == details.ownership_change_req_by_user);
      userList = userList.filter((item,i)=>item.user_Id !== details.ownership_change_req_by_user);      
      userList.splice(0,0,{...user})
      this.setState({usersList:userList})
    }
  }

  render() {
    const {user_assign_machine} = this.state;
    let request_unassign =false;
    user_assign_machine && user_assign_machine.length && user_assign_machine.forEach((item,i)=>{
      if(item.is_unassign_request === "1" && item.is_unassign_request_handler === 1 ){
        request_unassign=true;
      }})
    const userList = this.props.usersList.users;    
    const userName = map(this.state.usersList.length > 0 ? this.state.usersList : userList, (val, i) => {
      return (
        <option key={i} value={val.user_Id}>
          {val.username}
        </option>
      );
    });
    const {
      auditMsg,
      activeAuditId,
      activeItemName,
      show_alert_message,
      loading
    } = this.state;
    return (
      <div id="my-inventory-page">
        <div id="content" className="app-content box-shadow-z0" role="main">
          <div className="d-flex justify-content-end my-inventory-header ">
          <AssignDevice
            handleCloseAssign={this.handleCloseAssign}
            openAssign={this.state.openAssigned}
            handleAddDialog={this.handleAddDialog}
            unassignDeviceList={this.state.unassignDeviceList}
            callAssignDevice={this.callAssignDevice}
            user_id={this.state.user_id}
            loggedUser={this.props.loggedUser}
            show_alert_message={show_alert_message}
            loading={loading}
            handleSkipAudit={this.handleSkipAudit}
            allInventories={this.props.unassignedDeviceList.device}
          />
          </div>
          <AlertFader>
          {this.state.unassgnMessage && (
            <Alert
              className="alert-transparent rem-fade rh-section rh-alert"
              color="success"
            >
              {" "}
              <span>{this.state.unassgnMessage}</span>
            </Alert>
          )}
        </AlertFader>
          <div className="my-inventory-modal">
            <Modal
              backdropClassName="YourCustomClass"
              size="lg"
              isOpen={this.state.large}
              toggle={() => this.toggle("large")}
            >
              <ModalHeader toggle={() => this.toggle("large")}>
                Audit of {activeItemName}
              </ModalHeader>
              <ModalBody className="bg-white">
                <div className="form-group row ml-0 mr-0">
                  <label className="col-sm-2 form-control-label">Message</label>
                  <div className="col-sm-9">
                    <small className="text-red-np">
                      <i>
                        Add a short description about inventory. Eg. Working
                        fine, keys not working, etc.
                      </i>
                    </small>
                    <input
                      type="text"
                      className="form-control"
                      value={auditMsg}
                      onChange={e =>
                        this.setState({
                          auditMsg: e.target.value
                        })
                      }
                      onKeyUp={e => {
                        if (e.keyCode === 13) {
                          if (auditMsg != "") {
                            this.handleAuditSubmit(
                              "large",
                              activeAuditId,
                              auditMsg
                            );
                          }
                        }
                      }}
                      required
                    />
                    {auditMsg === "" ? (
                      <small className="text-red-np">
                        {" "}
                        Please write something
                      </small>
                    ) : null}
                  </div>
                </div>
              </ModalBody>
              <ModalFooter>
                <Button color="link" onClick={() => this.toggle("large")}>
                  Close
                </Button>
                <Button
                  color="success"
                  onClick={() => {
                    if (auditMsg != "") {
                      this.handleAuditSubmit("large", activeAuditId, auditMsg);
                    }
                  }}
                >
                  Save changes
                </Button>
              </ModalFooter>
            </Modal>
          </div>
          <div className="pt-2">
            {!show_alert_message ? (
              <Alert color="danger alert-transparent" className="remove-fade">
                <p className="my_inventory_alert">
                  <h6 className="highlight">IMPORTANT:</h6>
                  <h6>- Your monthly audit of inventories is pending.</h6>
                  <h6>
                  - Select your audit report from the drop down for each of your inventory items
                  </h6>
                  <h6>
                    - Once you are done with all the inventories you will be able
                    to access rest of HR app.
                  </h6>
                </p>
              </Alert>
            ) : null}
          </div>
          <div className="pt-2">
            {request_unassign ? (
              <Alert color="danger alert-transparent" className="remove-fade">
                <p className="my_inventory_alert">
                  <h6 className="highlight">IMPORTANT:</h6>
                  <h6>- An employee has raised a request for an inventory item to be unassigned. </h6>
                  <h6>- You need to check the inventory condition, collect it from the employee and assign it either to your self or someone else.</h6>
                </p>
              </Alert>
            ) : null}
          </div>
          {loading ? (
            <Spinner className="myInventoryspinner" />
          ) : (
            <DeviceDetails
              pictures={this.state.pictures}
              unassignDevice={this.unassignDevice}
              userAssignMachine={this.state.user_assign_machine}
              callUpdateUserDeviceDetails={this.callUpdateUserDeviceDetails}
              loggedUser={this.props.loggedUser}
              handleAuditClick={this.handleAuditClick}
              activeAuditId={this.state.activeAuditId}
              activeAudits={this.state.activeAudits}
              showPending={this.props.showPending}
              toggle={this.toggle}
              inputBox = {this.state.inputBox}
              callAddUserComment={this.callAddUserComment}
              inventoryAuditComments={this.props.inventoryAuditComments}
              onAuditMsgChange={text => {
                this.setState({ auditMsg: text });
              }}
              audit_comment_type={this.state.audit_comment_type}
              auditTypeChange={this.handleAuditType}
              auditMsg={this.state.auditMsg}
              handleAuditSubmit={this.handleAuditSubmit}
              activeUnassign={this.state.activeUnassign}
              isAssignLoading={this.state.isAssignLoading}
              loading={loading}
              userName={userName}
              AssignDevice ={this.AssignDevice}
              handleCardClick={this.handleCardClick}
              onUserChange={(value, inv_id) => {
                this.setState({
                  userId: value,
                  inventory_id: inv_id
                });
              }}
              isMobile={this.props.isMobile}
              handleDropdown={this.handleDropdown}
            />
          )}
        </div>
        {/* <UnassignDevice
          callAddUserComment={this.callAddUserComment}
          user_Id={this.state.user_profile_detail.user_Id}
          handleClose={this.handleClose}
          open={this.state.openUnassigned}
          device={this.state.device}
        /> */}
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    frontend: state.frontend.toJS(),
    loggedUser: state.logged_user.userLogin,
    myProfile: state.myProfile.toJS(),
    unassignedDeviceList: state.manageDevice.toJS(),
    showPending: state.myProfile.toJS().showPending,
    usersList: state.usersList.toJS(),
    inventoryAuditComments: state.settings.inventory_audit_comments
  };
}
const mapDispatchToProps = dispatch => {
  return {
    onAddAuditComment: (id, msg, auditType) => {
      return dispatch(actionsMyProfile.addInventoryAudit(id, msg, auditType));
    },
    onMyProfileDetails: () => {
      return dispatch(actionsMyProfile.getMyProfileDetails());
    },
    onUpdateDeviceDetails: newDeviceDetails => {
      return dispatch();
      // actionsMyProfile.updateUserDeviceDetails(newDeviceDetails)
    },
    onAddUserComment: addUserCommentDetails => {
      return dispatch(
        actionsManageDevice.addUserComment(addUserCommentDetails)
      );
    },
    onUnassignDeviceList: () => {
      return dispatch(actionsManageDevice.unassignDeviceList());
    },
    onAssignDevice: assign_device => {
      return dispatch(actionsManageDevice.assignDevice(assign_device));
    },
    onGetInventoryDetails:()=>{
      return dispatch(actionsManageDevice.get_machines_detail())
    },
    onGetMyInventory: (payload) => {
      return dispatch(actionsMyProfile.getMyInventory(payload));
    },
    onUsersList: () => {
      return dispatch(actionsUsersList.get_users_list());
    },
    getGenericConfig: () => dispatch(actions.getGenricLoginConfig()),
    showHeading: (data) => dispatch(actions.showHeading(data)),
  };
};

const RouterVisibleMyInventory = compose(isMobile)(withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(MyInventory))
);

export default RouterVisibleMyInventory;