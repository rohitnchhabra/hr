import React, { useState } from "react";
import { notify } from "../../../services/notify";
import { getToken } from "../../../services/generic";
import { CONFIG } from "../../../config/index";
import UploadImageComp from "../../uploadImageCompressed/UploadImageComp";
import Heading from "../../../components/generic/Heading";
import Dropzone from "react-dropzone";
import { Button, Alert } from "reactstrap";

export default function AddInventoryDocument({ machineId, onclose, file_type , remove_type, uploaed_pic , showSuccessMessage }) { 
  let doc_list = [{ name: "Photo" }, { name: "Warranty" }, { name: "Invoice" }];
  const [state, setState] = useState({
    inventory_invoice: "",
    inventory_warranty: "",
    inventory_photo: "",
    page_url: window.location.href,
    document_type: "",
    file: [],
    imageUrl: "",
    toShow:false
  });
  const [file, setFile] = useState([]);
  const onUploadTypeChange = document => {
    setState({ ...state, document_type: document, imageUrl: "" });
    setFile([]);
  };
  const onFileDrop = files => {
    setFile(files);
    setState({
      ...state,
      type:
        files[0].name &&
        files[0].name.split(".")[files[0].name.split(".").length - 1]
    });
    const reader = new FileReader();

    reader.onloadend = () => {
      setState({
        ...state,
        imageUrl: reader.result
      });
    };
    if (files) {
      reader.readAsDataURL(files[0]);
      setState({
        ...state,
        imageUrl: reader.result
      });
    } else {
      setState({
        ...state,
        imageUrl: ""
      });
    }
  };
  const validateDocuments = e => {
    let type = state.document_type;
    let stop = false;
    if (type === "") {
      stop = true;
      notify("Warning!", "Please select document type.", "warning");
    } else if (!state.imageUrl) {
      stop = true;
      notify("Warning!", "Please select a file", "warning");
    }
    else {
      setFile([]);
      setState({
        ...state,
        toShow: true,
        document_type:''
      });
    }
    if (stop) {
      e.preventDefault();
    }
    
  };

  const successfulFileUpload = () => {
    setState({
      ...state,
      imageUrl: "",
      file: [],
      document_type: ""
    });

  };
   
  const handleRomoveImage=()=>{
    setState({ file:[], imageUrl: "",document_type:''})
    setFile([])
  }
  const onhandledclose = () => {
    setState({
      ...state,
      toShow: true
    });
    onclose();
  } 
  let option =
    file_type.length >0 &&  file_type.map((item)=>{
      
        if(item == "Photo") {
          return <option value="inventory_photo"> Photo</option>
        }
        else if (item == "Warranty") {
          return <option value="inventory_warranty">Warranty</option>
        }
        else if (item == "Invoice") {
          return <option value="inventory_invoice">Invoice</option>
        }
    })
  
  return (
    <div className="row mx-0">

    <div className="col-sm-8">
    
    <div className="col-sm-12 document_upload_inventory_page px-0">
      <Heading
        subHeading
        title="Upload"
        boldText="New Documents"
        className="add-inventory-document-heading ml-0"
      />
      <div className="box m-b-lg form-my-doc p-2" id="uploadDoc">
        <Alert color="info" fade={false}>
          You can upload multiple documents together{" "}
        </Alert>
        <form
          action={CONFIG.upload_url}
          method="POST"
          encType="multipart/form-data"
        >
          <div className="form-group doc_inventory_upload d-flex">
            <label className="col-sm-3 col-12 p-0">Document Type</label>
            <select
              className="form-control col-sm-9 col-12"
              // ref="status"
              value={state.document_type}
              onChange={e => {
                onUploadTypeChange(e.target.value);
              }}
            >
              <option value="" disabled>
                --Select document--
              </option>
              {option}
            </select>
          </div>
          <input
            type="hidden"
            name="document_type"
            // value={this.state.document_type}
          />
          <div className="form-group d-flex doc_inventory_upload">
            <label className="col-sm-3 col-12 p-0">Upload Document </label>
            <Dropzone onDrop={onFileDrop}>
              {({ getRootProps, getInputProps }) => {
                return (
                  <section className="container m-0 p-0 col-sm-9 col-12">
                    <div {...getRootProps({ className: "dropzone" })}>
                      <input {...getInputProps()} />

                      <div className="drag_and_drop align-items-center">
                        {file &&
                          file.map((file, index) => (
                            <div className="font-weight-bold" key={file.name}>
                              {" "}
                              {file.name}
                            </div>
                          ))}
                        {state.imageUrl ? (
                          <img
                            src={state.imageUrl}
                            className="mx-3 img-fluid inventory-doc-preview"
                            alt="Preview"
                          />
                        ) : (
                          <p className="uploading_doc">
                            <i className="fi flaticon-upload" />
                          </p>
                        )}

                        <p className="doc_upload_place">
                          Drop a document here or click to select file to upload
                        </p>
                      </div>
                    </div>
                  </section>
                );
              }}
            </Dropzone>
          </div>

          <div className="form-group offset-sm-3">
            <UploadImageComp
              validateDocuments={validateDocuments}
              fileParams={{
                file: file,
                document_type: state.document_type,
                inventory_id: machineId,
                token: getToken(),
                // type: state.type,
                imageUrl: state.imageUrl,
                file_upload_action: state.document_type
              }}
              url={"generic_upload_url"}
              // loadingSuccess={this.state.loading}
              onSuccessfulFileUpload={successfulFileUpload}
              handleRomoveImage={handleRomoveImage}
              uploaed_pic={uploaed_pic}
              remove_type={remove_type}
              showSuccessMessage={showSuccessMessage}
            />
            {state.toShow ? 
            <Button
              className="px-6 mb-4"
              color="success"
              onClick={() => {
                onhandledclose()
              }}
            >
            Add New Inventory
            </Button> : '' }
          </div>
        </form>
      </div>
    </div>
    </div>
    <div className="col-sm-4">
    <Heading subHeading boldText="Documents"  className={"sub-heading-val"} />
            
              
              <ul className=" doc-list list-group m-b thumbnail p-3 ">
              {file_type.length > 0 &&doc_list.map((item,i)=>(
                
                  <li className="d-flex" key={i}>        
                    {!file_type.includes(item.name)?<span className="list-number bg-success text-white mb-1 list-number-align">
                        <i className="tick_list glyphicon glyphicon-ok mt-2" />
                      </span>:<span className="list_number">{i + 1}</span>}                    
                    <small>{item.name}</small>
                  </li>
                ))}
              </ul>
    </div>
    </div>
  );
}
