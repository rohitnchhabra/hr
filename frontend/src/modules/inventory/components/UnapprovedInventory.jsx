import React, { Component } from "react";
import { connect } from "react-redux";
import { withRouter } from "react-router";
import _ from "lodash";
import { notify } from "../../../services/notify";
import * as actionsManageDevice from "../../../redux/inventory/actions/inventory";
import * as actionsUsersList from "../../../redux/generic/actions/usersList";
import * as actionsManageUsers from "../../../redux/manageUsers/actions/manageUsers";
import * as actions from '../../../redux/actions';
import Heading from "../../../components/generic/Heading";
import DeviceDetailSection from "./DeviceDetailSection";
import { CONFIG } from "../../../config";
import moment from "moment";
import * as a5 from "../../../static/avatar.png";
import map from "lodash/map";
import cloneDeep from "lodash/cloneDeep";
import isEqual from "lodash/isEqual";
import { compose } from "redux";
import isMobile from "../../../components/hoc/WindowResize";
import findIndex from "lodash/findIndex";

class UnapprovedInventory extends Component {
  constructor(props) {
    super(props);
    this.state = {
      edit: true,
      open: false,
      openStatus: false,
      id: "",
      openSnackbar: false,
      user: "",
      status_message: "",
      deviceTypeList: [],
      deviceStatusList: [],
      deviceList: [],
      file_upload_action: "inventory_files",
      deviceVal: "",
      unapprovedList: props.manageDevice.unassignedDeviceList,
      approveDialog: false,
      headerData: [],
      machineDetail: [],
      comment: "",
      inventory_id: "",
      user_id: "",
      addComment: false,
      editContent: [],
      document_type: "",
      file: []
    };
  }

  componentWillMount() {
    this.props.onFetchDevice();
    this.props.onUsersList();
    this.props.onFetchDeviceType();
    this.props.onFetchDeviceStatus();
    this.props.onFetchDeviceCount();
    this.props.onFetchUnassignedInventory();
    this.props.showHeading("Unassigned Inventory")

    if(this.props.manageDevice && this.props.manageDevice.unassignedDeviceList.length){
    let index = this.state.machineId
        ? findIndex(this.props.manageDevice.unassignedDeviceList, e => {
            return e.id === this.state.machineId;
          })
        : 0;
      this.setState({
        unapprovedList: this.props.manageDevice.unassignedDeviceList,
        machineDetail: [this.props.manageDevice.unassignedDeviceList[index]],
        backupMachineDetail: [
          this.props.manageDevice.unassignedDeviceList[index]
        ],
        user_id: this.props.manageDevice.unassignedDeviceList[index].user_Id
          ? this.props.manageDevice.unassignedDeviceList[index].user_Id
          : "",
        inventory_id: this.props.manageDevice.unassignedDeviceList[0].id
      });
    }
  }

  componentDidUpdate(prevProps) {
    if (
      !_.isEqual(
        prevProps.manageDevice.unassignedDeviceList,
        this.props.manageDevice.unassignedDeviceList
      )
    ) {
      let index = this.state.machineId
        ? findIndex(this.props.manageDevice.unassignedDeviceList, e => {
            return e.id === this.state.machineId;
          })
        : 0;
      this.setState({
        unapprovedList: this.props.manageDevice.unassignedDeviceList,
        machineDetail: [this.props.manageDevice.unassignedDeviceList[index]],
        backupMachineDetail: [
          this.props.manageDevice.unassignedDeviceList[index]
        ],
        user_id: this.props.manageDevice.unassignedDeviceList[index].user_Id
          ? this.props.manageDevice.unassignedDeviceList[index].user_Id
          : "",
        inventory_id: this.props.manageDevice.unassignedDeviceList[0].id
      });
    }
  }

  callUpdateDocuments = e => {
    let type = this.state.document_type;
    let stop = false;
    if (type === "") {
      stop = true;
      notify("Warning!", "Please select document type.", "warning");
    } else if (!this.state.imageUrl) {
      stop = true;
      notify("Warning!", "Please select a file", "warning");
    }
    if (stop) {
      e.preventDefault();
    }
  };

  handleAddComment = (add_inventory_comment, device_id) => {
    this.props.onAddInventoryComment(add_inventory_comment).then(
      data => {
        this.props.onFetchDevice();
      },
      error => {
        notify("Error!", error, "error");
      }
    );
    this.setState({
      comment: "",
      addComment: false
    });
  };

  onDrop = files => {
    this.setState({
      file: files,
      type:
        files[0].name &&
        files[0].name.split(".")[files[0].name.split(".").length - 1]
    });
    const reader = new FileReader();

    reader.onloadend = () => {
      this.setState({
        imageUrl: reader.result
      });
    };
    if (files) {
      reader.readAsDataURL(files[0]);
      this.setState({
        imageUrl: reader.result
      });
    } else {
      this.setState({
        imageUrl: ""
      });
    }
  };

  addComment = () => {
    this.setState({
      addComment: true
    });
  };

  selectCard = id => {
    let devices = this.state.unapprovedList;

    let cardShown = devices.filter(item => {
      return item.id === id;
    });
    this.setState({
      machineDetail: [...cardShown],
      backupMachineDetail: [...cardShown],
      machineId: id,
      user_id: cardShown[0].user_Id ? cardShown[0].user_Id : "",
      inventory_id: id
    });
  };

  openEditDevice = device => {
    this.setState({
      edit: true,
      open: false
    });
    this.props.onUserEditData(device, this.state.edit, this.state.open);
    this.props.history.push("/app/addInventory");
  };

  deleteDevices = (id, userId) => {
    this.props.onDeleteDevice(id, userId).then(val => {
      this.setState({
        status_message: "",
        machineId: ""
      });
      this.props.onFetchDevice();
      this.props.onFetchUnassignedInventory();
    });
  };

  sendUnapprovedId = id => {
    this.setState({ id: id });
    this.props.onFetchApprovedUser(id);
  };

  AssignDevice = (assign_device, device_id) => {
    this.device_id = device_id;
    this.props.onAssignDevice(assign_device).then(
      data => {
        this.props.onFetchDevice();
        this.props.onFetchUnassignedInventory();
        this.setState({
          user_id: "",
          inventory_id: ""
        });
      },

      error => {
        notify("Error!", error, "error");
      }
    );
  };

  handleAddComment = (add_inventory_comment, device_id) => {
    this.props.onAddInventoryComment(add_inventory_comment).then(
      data => {
        this.props.onFetchDevice();
        this.props.onFetchUnassignedInventory();
      },
      error => {
        notify("Error!", error, "error");
      }
    );
    this.setState({
      comment: "",
      addComment: false
    });
  };

  addComment = () => {
    this.setState({
      addComment: true
    });
  };

  onEditClick = (edit ,isEdit)=> {
    let ec = this.state.editContent;
    if(!isEdit){
    ec=[]
    }else{
      ec.push(...edit);
    }
    this.setState({ editContent: ec });
  };

  onUploadTypeChange = document => {
    this.setState({ document_type: document, file: [] ,imageUrl:''});
  };

  onEditChange = (name, value) => {
    let cardShown = this.state.unapprovedList.filter(item => {
      return item.id === this.state.machineDetail[0].id;
    });
    let deviceDetail = cloneDeep(this.state.machineDetail[0]);
    deviceDetail[name] = value;
    this.setState({
      machineDetail: [deviceDetail],
      backupMachineDetail: cardShown
    });
  };

  onEditCancel = () => {
    let deviceDetail = cloneDeep(this.state.backupMachineDetail);
    this.setState({ machineDetail: deviceDetail, editContent: [] });
  };

  onEditSubmit = () => {
    this.setState({
      loading: true
    });
    let apiData = {
      machine_type: this.state.machineDetail[0].machine_type,
      machine_name: this.state.machineDetail[0].machine_name.trim(),
      machine_price: this.state.machineDetail[0].machine_price.trim(),
      serial_no: this.state.machineDetail[0].serial_number.trim(),
      purchase_date: moment(this.state.machineDetail[0].purchase_date).format(),
      operating_system: this.state.machineDetail[0].operating_system,
      status: this.state.machineDetail[0].status,
      comment: this.state.machineDetail[0].comments.trim(),
      warranty: moment(this.state.machineDetail[0].warranty_end_date).format(),
      warranty_comment: this.state.machineDetail[0].warranty_comment.trim(),
      repair_comment: this.state.machineDetail[0].repair_comment.trim(),
      bill_no: this.state.machineDetail[0].bill_number.trim(),
      user_Id: this.state.machineDetail[0].user_Id,
      unassign_comment: this.state.machineDetail[0].unassign_comment,
      warranty_years:
        moment(this.state.machineDetail[0].warranty_end_date).format() -
        moment(this.state.machineDetail[0].purchase_date).format()
    };
    this.props
      .onUpdateDevice(this.state.machineDetail[0].id, apiData)
      .then(message => {
        this.setState({
          loading: false,
          error: false,
          alertMessage: message,
          editContent: []
        });
        this.props.onFetchUnapprovedUser();
        this.setState({
          alertMessage: ""
        });
      })
      .catch(message => {
        this.setState({
          msg: message,
          loading: false,
          error: true,
          alertMessage: message
        });
        this.setState({
          alertMessage: ""
        });
      });
  };

  successfulFileUpload = () => {
    this.props.onFetchUnassignedInventory();
    this.setState({
      imageUrl: "",
      file: [],
      document_type: ""
    });
  };

  handleRomoveImage=()=>{
    this.setState({ imageUrl: "",file:[]})
  }

  render() {
    
    let path = CONFIG.inventory_images;
    let userList;
    if (this.props.usersList.users) {
      userList = [
        { user_Id: "", username: "Unassign Device" },
        ...this.props.usersList.users
      ];
    }
    const userName = map(userList, (val, i) => {
      return (
        <option key={i} value={val.user_Id}>
          {val.username}
        </option>
      );
    });
    let isSubmit = !isEqual(
      this.state.machineDetail && this.state.machineDetail[0],
      this.state.backupMachineDetail && this.state.backupMachineDetail[0]
    );
    const history =
      this.state.machineDetail.length &&
      this.state.machineDetail[0].history &&
      map(this.state.machineDetail[0].history, (val, i) => (
        <li>
          <span className={`thumb-xs pull-left mr-sm`}>
            <img className="rounded-circle" src={a5} alt="..." />
          </span>
          <div>
            <h6 className={`commentUser fs-sm fw-semi-bold`}>
              {val.updated_by_user}
              <br />
              <small>
                {moment(val.updated_at).format("Do MMM YY, h:mm a")}
              </small>
            </h6>
            <p>{val.assign_unassign_user_name? val.comment=="Inventory Assigned"?val.comment + " to " + val.assign_unassign_user_name:val.comment + " from " + val.assign_unassign_user_name :val.comment}</p>
          </div>
        </li>
      ));

    return (
      <div>
        {this.state.machineDetail.length ? (
          <DeviceDetailSection
            state={this.state}
            selectCard={id => {
              this.selectCard(id);
            }}
            openEditDevice={device => this.openEditDevice(device)}
            deleteDevices={this.deleteDevices}
            sendUnapprovedId={id => {
              this.sendUnapprovedId(id);
            }}
            onUserChange={(value, inv_id) => {
              this.setState({
                user_id: value,
                inventory_id: inv_id
              });
            }}
            AssignDevice={(assign_device, device_id) => {
              this.AssignDevice(assign_device, device_id);
            }}
            onCommentChange={(comment, inventory_id) => {
              this.setState({
                comment: comment,
                inventory_id: inventory_id
              });
            }}
            handleAddComment={(add_inventory_comment, device_id) => {
              this.handleAddComment(add_inventory_comment, device_id);
            }}
            addComment={() => {
              this.addComment();
            }}
            history={history}
            path={path}
            devices={this.props.manageDevice.unassignedDeviceList}
            userName={userName}
            isApprove={true}
            deviceStatuses={this.props.manageDevice.statusList}
            deviceTypes={this.props.manageDevice.deviceList}
            onEditChange={this.onEditChange}
            onEditClick={this.onEditClick}
            onEditCancel={this.onEditCancel}
            isSubmit={isSubmit}
            onEditSubmit={this.onEditSubmit}
            CONFIG={CONFIG}
            onFileDrop={this.onDrop}
            handleDialogOpen={() => {
              this.setState({ open: true });
            }}
            handleDialogClose={() => {
              this.setState({ open: false });
            }}
            onSuccessfulFileUpload={this.successfulFileUpload}
            onUploadTypeChange={this.onUploadTypeChange}
            validateDocuments={this.callUpdateDocuments}
            isMobile={this.props.isMobile}
            handleRomoveImage={this.handleRomoveImage}
          />
        ) : null}
      </div>
    );
  }
}
function mapStateToProps(state) {
  return {
    frontend: state.frontend.toJS(),
    usersList: state.usersList.toJS(),
    manageUsers: state.manageUsers.toJS(),
    loggedUser: state.logged_user.userLogin,
    manageDevice: state.manageDevice.toJS()
  };
}
const mapDispatchToProps = dispatch => {
  return {
    onGetDevice: id => {
      return dispatch(actionsManageDevice.getDeviceById(id));
    },
    onUsersList: () => {
      return dispatch(actionsUsersList.get_users_list());
    },
    onUserProfileDetails: (pathName, userid, username) => {
      return dispatch(
        actionsManageUsers.getUserProfileDetails(pathName, userid, username)
      );
    },
    onUpdateUserDeviceDetails: newDeviceDetails => {
      return dispatch();
      // actionsManageUsers.updateUserDeviceDetails(newDeviceDetails)
    },
    onAddNewMachine: newMachineDetails => {
      return dispatch(actionsManageDevice.addNewMachine(newMachineDetails));
    },
    onFetchDevice: () => {
      return dispatch(actionsManageDevice.get_machines_detail());
    },
    onGetDeviceById: id => {
      return dispatch(actionsManageDevice.getDeviceById(id));
    },
    onUpdateDevice: (id, machineData) => {
      return dispatch(actionsManageDevice.updateDevice(id, machineData));
    },
    onDeleteDevice: (id, userId) => {
      return dispatch(actionsManageDevice.deleteDevice(id, userId));
    },
    onCallAssign: (deviceId, id) => {
      return dispatch(actionsManageDevice.assignDevice(deviceId, id));
    },
    onCallDeviceType: deviceList => {
      return dispatch(actionsManageDevice.assignDeviceType(deviceList));
    },
    onCallDeviceStatus: (statusValue, colorValue) => {
      return dispatch(
        actionsManageDevice.assignDeviceStatus(statusValue, colorValue)
      );
    },
    onFetchDeviceType: () => {
      return dispatch(actionsManageDevice.getDeviceType());
    },
    onFetchDeviceStatus: () => {
      return dispatch(actionsManageDevice.getDeviceStatus());
    },
    onDeleteDeviceStatus: checkValue => {
      return dispatch(actionsManageDevice.deleteDeviceStatus(checkValue));
    },
    onFetchDeviceCount: () => {
      return dispatch(actionsManageDevice.deviceCount());
    },
    onFetchUnapprovedUser: () => {
      return dispatch(actionsManageDevice.unapprovedUser());
    },
    onFetchApprovedUser: id => {
      return dispatch(actionsManageDevice.approvedUser(id));
    },
    onUserEditData: (device, edit, open) => {
      return dispatch(actionsManageDevice.editDeviceData(device, edit, open));
    },
    onNoTab: () => {
      return dispatch(actionsManageDevice.noTab());
    },
    onGetDevice: device_id => {
      return dispatch(actionsManageDevice.getDeviceById(device_id));
    },
    onAssignDevice: assign_device => {
      return dispatch(actionsManageDevice.assignDevice(assign_device));
    },
    onAddInventoryComment: add_inventory_comment => {
      return dispatch(
        actionsManageDevice.addInventoryComment(add_inventory_comment)
      );
    },
    onFetchDevice: () => {
      return dispatch(actionsManageDevice.get_machines_detail());
    },
    onFetchUnassignedInventory: () => {
      return dispatch(actionsManageDevice.unassignDeviceList());
    },
    showHeading: data => {
      return dispatch(actions.showHeading(data));
    }
  };
};

export default compose(
  connect(
    mapStateToProps,
    mapDispatchToProps
  ),
  isMobile,
  withRouter
)(UnapprovedInventory);
