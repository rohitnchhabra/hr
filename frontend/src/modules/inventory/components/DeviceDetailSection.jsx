import React, { useState } from "react";
import { notify, confirm } from "../../../services/notify";
import {
  getLowerCase,
  getLoggedUser,
  getToken
} from "../../../services/generic";
import { CONFIG } from "../../../config";
import { Button, Card, CardBody, Input, Alert, Table } from "reactstrap";
import classnames from "classnames";
import moment from "moment";
import Heading from "../../../components/generic/Heading";
import AlertFader from "../../../components/generic/AlertFader";
import Dropzone from "react-dropzone";
import UploadImageComp from "../../uploadImageCompressed/UploadImageComp";
import FlatButton from "material-ui/FlatButton";
import GenericDatePicker from "../../../components/generic/GenericDatePicker";
import GenricCardList from "../../../components/generic/GenricCardList";
import useDomNodeHeight from "../../../components/customHooks/useDomNodeHeight";
import Viewer from "react-viewer";
let itemArray = [
  "machineType",
  "purchaseDate",
  "warantyExp",
  "price",
  "serialNo",
  "internalBillNo",
  "assignUser",
  "machineName",
  "status"
];
export default function DeviceDetailSection(props) {
  let doc_list = [{ name: "Photo" }, { name: "Warranty" }, { name: "Inovice" }];
  const [success, setSuccess] = useState("");
  const showSuccessMessage = () => {
    setSuccess("File successfully uploaded");
    setSuccess("");
  };

  let info = props.state.machineDetail && props.state.machineDetail[0];
  const actions = [
    <FlatButton
      label="Close"
      primary={true}
      onClick={props.handleDialogClose}
    />
  ];
  const onPressCtrlEnter = e => {
    if ((e.ctrlKey || e.metaKey) && (e.charCode === 13 || e.charCode === 10)) {
      props.handleAddComment(props.state, props.state.machineDetail[0].id);
    }
  };
  const [clicked, setClicked] = useState();
  const [isEdit, setIsEdit] = useState(false);
  const [deviceDetailRef, height] = useDomNodeHeight(clicked);
  const handleEditClick = () => {
    if(isEdit) {
      props.onEditCancel();
    }
    setIsEdit(!isEdit);
    props.onEditClick(itemArray, !isEdit);
  };

  const details = () => {
    return (
      <div className="col-md-9">
        <div
          className={classnames(
            props.state.machineDetail[0].status === "Working"
              ? "workingStatus"
              : "not-workingStatus",
            "overflow-auto px-0",
            " machineDetail row"
          )}
          ref={deviceDetailRef}
        >
          <div className="col-md-8">
            <div className="col-md-6">
              <div className="machineDetailInfo">
                <h6 className="lightGrey">Machine Type</h6>
                {props.state.editContent.includes("machineType") ? (
                  <select
                    className="form-control"
                    value={
                      props.state.machineDetail &&
                      props.state.machineDetail[0].machine_type
                    }
                    onChange={evt => {
                      props.onEditChange("machine_type", evt.target.value);
                    }}
                  >
                    <option value="" disabled>
                      --Select Device--
                    </option>
                    {props.deviceTypes.map((val, i) => {
                      return (
                        <option key={i} value={val}>
                          {" "}
                          {val}
                        </option>
                      );
                    })}
                  </select>
                ) : (
                  <h6 className="darkGrey">
                    {props.state.machineDetail[0].machine_type}
                  </h6>
                )}
              </div>
              <div className="machineDetailInfo">
                <h6 className="lightGrey">Purchase Date</h6>
                {props.state.editContent.includes("purchaseDate") ? (
                  <GenericDatePicker
                    date={
                      props.state.machineDetail &&
                      props.state.machineDetail[0].date_of_purchase
                    }
                    handleDateChange={date =>
                      props.onEditChange("purchase_date", moment(date).format())
                    }
                    type={props.isMobile < 768 ? "absolute" : "inline"}
                  />
                ) : (
                  <h6 className="darkGrey">
                    {props.state.machineDetail &&
                      props.state.machineDetail[0].date_of_purchase}
                  </h6>
                )}
              </div>
              <div className="machineDetailInfo">
                <h6 className="lightGrey">Warranty Expire</h6>
                {props.state.editContent.includes("warantyExp") ? (
                  <GenericDatePicker
                    date={
                      props.state.machineDetail &&
                      props.state.machineDetail[0].warranty_end_date
                    }
                    handleDateChange={date =>
                      props.onEditChange(
                        "warranty_end_date",
                        moment(date).format()
                      )
                    }
                    type={props.isMobile < 768 ? "absolute" : "inline"}
                  />
                ) : (
                  <h6 className="darkGrey">
                    {props.state.machineDetail &&
                      props.state.machineDetail[0].warranty_end_date}
                  </h6>
                )}
              </div>
              <div className="machineDetailInfo">
                <h6 className="lightGrey">Price</h6>

                {props.state.editContent.includes("price") ? (
                  <Input
                    fullWidth
                    className="form-control"
                    value={props.state.machineDetail[0].machine_price}
                    onChange={evt => {
                      props.onEditChange("machine_price", evt.target.value);
                    }}
                  />
                ) : (
                  <h6 className="darkGrey">
                    {props.state.machineDetail[0].machine_price}
                  </h6>
                )}
              </div>
              <div className="machineDetailInfo">
                <h6 className="lightGrey">Serial Number</h6>
                {props.state.editContent.includes("serialNo") ? (
                  <Input
                    value={props.state.machineDetail[0].serial_number}
                    className="form-control"
                    onChange={evt => {
                      props.onEditChange("serial_number", evt.target.value);
                    }}
                  />
                ) : (
                  <h6 className="darkGrey">
                    {props.state.machineDetail[0].serial_number}
                  </h6>
                )}
              </div>
              <div className="machineDetailInfo">
                <h6 className="lightGrey">Internal Serial No.</h6>
                {props.state.editContent.includes("internalBillNo") ? (
                  <Input
                    value={props.state.machineDetail[0].bill_number}
                    className="form-control"
                    onChange={evt => {
                      props.onEditChange("bill_number", evt.target.value);
                    }}
                  />
                ) : (
                  <h6 className="darkGrey">
                    {props.state.machineDetail[0].bill_number}
                  </h6>
                )}
              </div>
            </div>
            <div className="col-md-6">
              <div className="machineDetailInfo">
                <div className="d-flex">
                  <h6 className="darkGrey" style={{ fontWeight: "600" }}>
                    {props.state.machineDetail &&
                    props.state.machineDetail[0].name
                      ? props.state.machineDetail[0].name
                      : "Not Assigned"}
                  </h6>
                </div>
                {props.state.editContent.includes("assignUser") && props.state.machineDetail[0].status.toLowerCase() !=="sold"&& (
                  <>
                    <select
                      onChange={e =>
                        props.onUserChange(
                          e.target.value,
                          props.state.machineDetail[0].id
                        )
                      }
                      className="form-control"
                      value={props.state.user_id}
                    >
                      <option disabled>--Select User--</option>
                      {props.userName}
                    </select>
                    {props.state.machineDetail[0].status.toLowerCase() !=="sold"&& <Button
                      color="primary"
                      className="mt-1"
                      size="md"
                      onClick={() =>
                        props.AssignDevice(
                          props.state,
                          props.state.machineDetail.length &&
                            props.state.machineDetail[0].id
                        )
                      }
                    >
                      Assign Inventory
                    </Button>}
                  </>
                )}

                <h6 className="lightGrey">
                  {props.state.machineDetail[0].designation
                    ? props.state.machineDetail[0].designation
                    : "No Designation Data"}
                </h6>
              </div>
              <div className="machineDetailInfo">
                <h6 className="lightGrey">Machine Name</h6>
                {props.state.editContent.includes("machineName") ? (
                  <Input
                    fullWidth
                    value={props.state.machineDetail[0].machine_name}
                    onChange={evt => {
                      props.onEditChange("machine_name", evt.target.value);
                    }}
                  />
                ) : (
                  <h6 className="darkGrey">
                    {props.state.machineDetail[0].machine_name}
                  </h6>
                )}
              </div>
              <div className="machineDetailInfo">
                <h6 className="lightGrey">Status</h6>

                {props.state.editContent.includes("status") ? (
                  <select
                    className="form-control"
                    value={props.state.machineDetail[0].status}
                    onChange={evt =>
                      props.onEditChange("status", evt.target.value,props.state.machineDetail[0])
                    }
                  >
                    {props.deviceStatuses.map((val, i) => {
                      return (
                        <option key={i} value={val.status}>
                          {" "}
                          {val.status}
                        </option>
                      );
                    })}
                  </select>
                ) : (
                  <h6 className="linkInactive">
                    {props.state.machineDetail[0].status}
                  </h6>
                )}
              </div>
              <div className="machineDetailInfo">
                <h6 className="lightGrey">Comments</h6>
                <h6 className="darkGrey">
                  {props.state.machineDetail &&
                  props.state.machineDetail[0].repair_comment !== ""
                    ? props.state.machineDetail[0].repair_comment
                    : "N/A"}
                </h6>
              </div>
              <div>
                {getLoggedUser().data.role === CONFIG.ADMIN || CONFIG.HR ? (
                  <>
                    {props.isSubmit && isEdit ? (
                      <>
                        <Button
                          color="success"
                          className=" mb-xs mr-xs md-btn m-b-sm indigo"
                          onClick={e => {
                            e.nativeEvent.stopImmediatePropagation();
                            props.onEditSubmit();
                          }}
                        >
                          Submit
                        </Button>{" "}
                        <Button
                          color="success"
                          className=" mb-xs mr-xs md-btn m-b-sm indigo"
                          onClick={e => {
                            e.nativeEvent.stopImmediatePropagation();
                            setIsEdit(false);
                            props.onEditCancel();
                          }}
                        >
                          Cancel
                        </Button>{" "}
                      </>
                    ) : null}
                    <Button
                      color="danger"
                      className=" mb-xs mr-xs md-btn m-b-sm indigo"
                      onClick={() => {
                        confirm(
                          "Are you sure ?",
                          "Do you want to delete this device?",
                          "warning"
                        ).then(res => {
                          if (res) {
                            props.deleteDevices(
                              props.state.machineDetail[0].id
                            );
                          }
                        });
                      }}
                    >
                      Delete
                    </Button>
                  </>
                ) : null}
                <Button
                  color="success"
                  className=" mb-xs mr-xs md-btn m-b-sm indigo"
                  onClick={() => {
                    handleEditClick();
                  }}
                >
                  {isEdit ? "Cancel" : "Edit"}
                </Button>
              </div>
            </div>
            <div className="col-md-12 machineDetail">
              <label>
                {/* Users */}
                {/* <i
                className="glyphicon glyphicon-pencil mb-1 ml-1"
                onClick={() => props.onEditClick("assignUser")}
              /> */}
              </label>
              {/* {props.state.editContent.includes("assignUser") && (
              <>
                <select
                  onChange={e =>
                    props.onUserChange(
                      e.target.value,
                      props.state.machineDetail[0].id
                    )
                  }
                  className="form-control"
                  value={props.state.user_id}
                >
                  <option disabled>--Select User--</option>
                  {props.userName}
                </select>
                <Button
                  color="primary"
                  className="mt-1"
                  size="md"
                  onClick={() =>
                    props.AssignDevice(
                      props.state,
                      props.state.machineDetail.length &&
                        props.state.machineDetail[0].id
                    )
                  }
                >
                  Assign Inventory 
                </Button>
              </>
            )} */}
            </div>
          </div>

          <div className="col-md-4">
            {props.state.machineDetail[0].fileInventoryPhoto && (
              <div>
                <a
                  href={props.state.machineDetail[0].fileInventoryPhoto}
                  target="_blank"
                >
                  <img
                    className="w-100 inventory-device-picture"
                    src={props.state.machineDetail[0].fileInventoryPhoto}
                  />{" "}
                </a>
                <div>Inventory Photo</div>
              </div>
            )}
            <div className="machineDetail">
              {props.state.addComment ? (
                <div>
                  <div className="row">
                    <div className="col">
                      <textarea
                        placeholder="Your comment"
                        className="form-control resize-y"
                        onChange={e =>
                          props.onCommentChange(
                            e.target.value,
                            props.state.machineDetail[0].id
                          )
                        }
                        onKeyPress={onPressCtrlEnter}
                        value={props.state.addcomment}
                      />
                    </div>
                  </div>
                  <br />
                  <Button
                    color="success"
                    size="md"
                    onClick={() =>
                      props.handleAddComment(
                        props.state,
                        props.state.machineDetail[0].id
                      )
                    }
                  >
                    Submit
                  </Button>
                </div>
              ) : (
                <div className="addComment mt" onClick={props.addComment}>
                  <span>+</span>
                  <span className="addCommentText">Add comment</span>
                </div>
              )}
              <div className="mt-1">
                <ul className="post-comments scroll mt-sm">{props.history}</ul>
              </div>
            </div>
          </div>
          {/* <div className = "col p-2">
          {doc_list.map((item,key)=> (
            <div key={key}>
            {props.state.machineDetail&&props.state.machineDetail[0].fileInventoryInvoice &&
            item.name === "Inovice" ? (
              <Alert color="success" fade={false}>
                    Invoice is Uploaded..
                </Alert> 
            ) : props.state.machineDetail[0].fileInventoryWarranty &&
            item.name === "Warranty" ? (
                <Alert color="success" fade={false}>
                Warranty is Uploaded..
            </Alert> 
            ) : props.state.machineDetail[0].fileInventoryPhoto &&
            item.name === "Photo" ? (
                <Alert color="success" fade={false}>
                Photo is Uploaded..
            </Alert> 
            ) : (
              ""
            )}
          </div>
          ))}
          </div> */}
          <div className="col-md-12">
            <AlertFader>
              {success ? (
                <Alert fade={false} color={"success"}>
                  {success}
                </Alert>
              ) : null}
            </AlertFader>
          </div>

          <div className="row m-0 w-100">
            <div className="col-sm-8">
              <Heading subHeading title="Upload" boldText="New Documents" />
              <div className="box m-b-lg form-my-doc p-2" id="uploadDoc">
                <div>
                  <Alert color="info" fade={false}>
                    You can upload multiple documents together{" "}
                  </Alert>
                </div>

                <form
                  action={props.CONFIG.upload_url}
                  method="POST"
                  encType="multipart/form-data"
                >
                  <div className="form-group  d-flex">
                    <label className="col-sm-3">Document Type</label>
                    <select
                      className="form-control"
                      // ref="status"
                      value={props.state.document_type}
                      onChange={e => {
                        props.onUploadTypeChange(e.target.value);
                      }}
                    >
                      <option value="" disabled>
                        --Select document--
                      </option>
                      {/* {info.fileInventoryPhoto ? "": <option value="inventory_photo">Photo</option>}
                      { <option value="inventory_warranty">Warranty</option>}
                     {info.fileInventoryInvoice ? "": <option value="inventory_invoice">Invoice</option>} */}
                      <option value="inventory_photo">Photo</option>
                      <option value="inventory_warranty">Warranty</option>
                      <option value="inventory_invoice">Invoice</option>
                    </select>
                  </div>
                  <input
                    type="hidden"
                    name="document_type"
                    // value={this.state.document_type}
                  />
                  <div className="form-group d-flex">
                    <label className="col-sm-3">Upload Document </label>
                    <Dropzone onDrop={props.onFileDrop}>
                      {({ getRootProps, getInputProps }) => {
                        return (
                          <section className="container m-0 p-0 col-sm-9">
                            <div {...getRootProps({ className: "dropzone" })}>
                              <input {...getInputProps()} />

                              <div className="drag_and_drop">
                                {props.state.file &&
                                  props.state.file.map((file, index) => (
                                    <div
                                      className="font-weight-bold"
                                      key={file.name}
                                    >
                                      {" "}
                                      {file.name}
                                    </div>
                                  ))}
                                {props.state.imageUrl ? (
                                  <img
                                    src={props.state.imageUrl}
                                    className="mx-3 img-fluid inventory-doc-preview"
                                    alt="Preview"
                                  />
                                ) : (
                                  <p className="uploading_doc">
                                    <i className="fi flaticon-upload" />
                                  </p>
                                )}
                                <p className="doc_upload_place">
                                  Drop a document here or click to select file
                                  to upload
                                </p>
                              </div>
                            </div>
                          </section>
                        );
                      }}
                    </Dropzone>
                  </div>
                  <Viewer
                    visible={props.state.open}
                    onClose={props.handleDialogClose}
                    images={[{ src: props.state.imageUrl, alt: "" }]}
                  />
                  <div className="form-group offset-sm-3">
                    <UploadImageComp
                      validateDocuments={props.validateDocuments}
                      fileParams={{
                        file: props.state.file,
                        document_type: props.state.document_type,
                        inventory_id: props.state.inventory_id,
                        token: getToken(),
                        type: props.state.type,
                        imageUrl: props.state.imageUrl,
                        file_upload_action: props.state.document_type
                      }}
                      url={"generic_upload_url"}
                      // loadingSuccess={this.state.loading}
                      onFileCompression={props.onFileCompression}
                      onSuccessfulFileUpload={props.onSuccessfulFileUpload}
                      handleRomoveImage={props.handleRomoveImage}
                      showSuccessMessage={showSuccessMessage}
                    />
                  </div>
                  {props.state.compressedFile && (
                    <div className="col-sm-9 offset-sm-3 font-italic compressed-file">
                      File Size{" "}
                      {(props.state.file[0].size / 1000000).toFixed(2)}
                      mb, compressed to{" "}
                      {(props.state.compressedFile.size / 1000000).toFixed(2)}
                      mb
                    </div>
                  )}
                </form>
              </div>
            </div>
            <div className="col-sm-4">
              <Heading subHeading boldText="Documents" />
              <ul className=" doc-list list-group m-b thumbnail p-3 ">
                {doc_list.map((v, i) => (
                  <li className="d-flex" key={i}>
                    {props.state.machineDetail &&
                    props.state.machineDetail[0].fileInventoryInvoice &&
                    v.name === "Inovice" ? (
                      <span className="list-number bg-success text-white mb-1 list-number-align">
                        <i className="tick_list glyphicon glyphicon-ok mt-2" />
                      </span>
                    ) : props.state.machineDetail[0].fileInventoryWarranty &&
                      v.name === "Warranty" ? (
                      <span className="list-number bg-success text-white mb-1 list-number-align">
                        <i className="tick_list glyphicon glyphicon-ok mt-2" />
                      </span>
                    ) : props.state.machineDetail[0].fileInventoryPhoto &&
                      v.name === "Photo" ? (
                      <span className="list-number bg-success text-white mb-1 list-number-align">
                        <i className="tick_list glyphicon glyphicon-ok mt-2" />
                      </span>
                    ) : (
                      <span className="list_number">{i + 1}</span>
                    )}
                    <small>{v.name}</small>
                  </li>
                ))}
              </ul>
            </div>
          </div>
          <div className="col-md-12">
            <Heading subHeading title="Document" boldText="Directory" />

            <Table className="bg-white">
              <thead>
                <tr className="upload_doc_title my-4">
                  <th className="hidden-sm-down">DOCUMENT VIEW</th>
                  <th className="hidden-sm-down">TYPE</th>
                </tr>
              </thead>
              <tbody className="upload_doc_detail">
                {(props.state.machineDetail &&
                  props.state.machineDetail[0].fileInventoryInvoice) ||
                props.state.machineDetail[0].fileInventoryPhoto ||
                props.state.machineDetail[0].fileInventoryWarranty ? (
                  doc_list.map((v, i) => {
                    if (
                      props.state.machineDetail[0].fileInventoryInvoice &&
                      v.name === "Inovice"
                    ) {
                      return (
                        <tr key={i}>
                          <td>
                            <div className="">
                              <a
                                href={
                                  props.state.machineDetail[0]
                                    .fileInventoryInvoice
                                }
                                target="_blank"
                              >
                                <img
                                  src={
                                    props.state.machineDetail[0]
                                      .fileInventoryInvoice
                                  }
                                  className="image_doc_view"
                                />
                              </a>
                            </div>
                          </td>
                          <td className="list_doc_file">{v.name}</td>
                          <td className="list_doc_file"></td>
                        </tr>
                      );
                    } else if (
                      props.state.machineDetail[0].fileInventoryWarranty &&
                      v.name === "Warranty"
                    ) {
                      return (
                        <tr key={i}>
                          <td>
                            <div className="">
                              <a
                                href={
                                  props.state.machineDetail[0]
                                    .fileInventoryWarranty
                                }
                                target="_blank"
                              >
                                <img
                                  src={
                                    props.state.machineDetail[0]
                                      .fileInventoryWarranty
                                  }
                                  className="image_doc_view"
                                />
                              </a>
                            </div>
                          </td>
                          <td className="list_doc_file">{v.name}</td>
                          <td className="list_doc_file"></td>
                        </tr>
                      );
                    } else if (
                      props.state.machineDetail[0].fileInventoryPhoto &&
                      v.name === "Photo"
                    ) {
                      return (
                        <tr key={i}>
                          <td>
                            <div className="">
                              <a
                                href={
                                  props.state.machineDetail[0]
                                    .fileInventoryPhoto
                                }
                                target="_blank"
                              >
                                <img
                                  src={
                                    props.state.machineDetail[0]
                                      .fileInventoryPhoto
                                  }
                                  className="image_doc_view"
                                />
                              </a>
                            </div>
                          </td>
                          <td className="list_doc_file">{v.name}</td>
                          <td className="list_doc_file"></td>
                        </tr>
                      );
                    }
                  })
                ) : (
                  <tr>
                    <td className="text-center" colSpan={3}>
                      No Available Data
                    </td>
                  </tr>
                )}
              </tbody>
            </Table>
          </div>
        </div>
      </div>
    );
  };

  return (
    <div className="d-flex flex-wrap flex-lg-nowrap">
      {props.state.machineDetail.length > 0 && props.devices.length > 0 ? (
        <>
          <div
            className="assign-member scroll col-md-3 p-0"
            style={{
              height: props.isMobile > 476 ? `${height}px` : "unset",
              maxHeight: props.isMobile < 476 ? "1500px" : "unset"
            }}
          >
            {props.devices.map((item, index) => {
              return (
                <>
                  <GenricCardList
                    onCardSelected={() => {
                      props.onEditCancel();
                      setIsEdit(false);
                      props.selectCard(item.id);
                      setClicked(c => c + 1);
                    }}
                    cardBodyClassName={
                      props.state.machineDetail.length &&
                      item.id === props.state.machineDetail[0].id
                        ? "lightGreen"
                        : ""
                    }
                    cardClassname={"border-0"}
                    heading={item.machine_name}
                    subHeading={item.machine_type}
                    sec_Heading={item.bill_number}
                  />
                  {window.screen.availWidth < 768 &&
                    item.id === props.state.machineDetail[0].id &&
                    details()}
                </>
              );
            })}
          </div>
          {props.state.machineDetail.length
            ? window.screen.availWidth >= 768 && details()
            : null}
        </>
      ) : props.isGetData ? (
        <Alert color="info" fade={false}>
          No {props.state.search} found with status {props.state.device_status}
        </Alert>
      ) : (
        ""
      )}
    </div>
  );
}
