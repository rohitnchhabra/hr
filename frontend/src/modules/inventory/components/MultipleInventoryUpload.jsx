import React, { useState, useEffect } from "react";
import { CONFIG } from "../../../config/index";
import Dropzone from "react-dropzone";
import { Button, Alert, Input } from "reactstrap";
import { getToken } from "../../../services/generic";
import { useDispatch, useSelector } from "react-redux";
import { getTempUploadedInventory } from "../../../redux/inventory/actions/inventory";

import UploadMultipleFiles from "../../uploadImageCompressed/UploadMultipleFiles";
import * as actionsManageDevice from "../../../redux/inventory/actions/inventory";
import * as actions from '../../../redux/actions'
import { Column, Table, AutoSizer, Grid } from "react-virtualized";
import InputText from "../../../components/generic/input/InputText";
import "react-virtualized/styles.css"; // only needs to be imported once
import GenericDatePicker from "../../../components/generic/GenericDatePicker";
import isMobile from "../../../components/hoc/WindowResize";
var moment = require("moment");

export default function MultipleInventoryUpload() {
  const [state, setState] = useState({
    inventory_invoice: "",
    inventory_warranty: "",
    inventory_photo: "",
    page_url: window.location.href,
    document_type: "",
    file: [],
    imageUrls: "",
    toShow: false,
    file_types: "",
    status: ""
  });

  const dispatch = useDispatch();

  const [file, setFile] = useState([]);

  const [field, setField] = useState({});

  const manageDevice = useSelector(state => state.manageDevice.toJS());

  const validateDocuments = e => {};

  useEffect(() => {
    dispatch(getTempUploadedInventory());
    dispatch(actionsManageDevice.getDeviceType());
    let file_types = ["Photo", "Warranty", "Inovice"];
    setState({ ...state, file_types: file_types });
    dispatch(actions.showHeading("Bulk Add Inventory"))
  }, []);

  const successfulFileUpload = () => {
    setState({ ...state, file: [], imageUrls: "", document_type: "" });
    setFile([]);
    dispatch(getTempUploadedInventory());
  };

  const handleRemoveImage = i => {
    let filesToUpload = file.filter((element, index) => {
      return i !== index;
    });
    const urls = [];

    if (filesToUpload) {
      filesToUpload.forEach((v, i) => {
        setFile(filesToUpload);
        setState({
          ...state,
          file: filesToUpload
        });
        const reader = new FileReader();

        reader.onloadend = () => {
          urls.push(reader.result);
        };
        reader.readAsDataURL(filesToUpload[i]);
      });
      setState({
        ...state,
        imageUrls: urls
      });
    } else {
      setState({
        ...state,
        imageUrls: [],
        file: []
      });
    }
  };

  const onFileDrop = files => {
    const urls = [];
    if (files) {
      files.forEach((v, i) => {
        setFile(files);
        setState({
          ...state,
          type:
            files[0].name &&
            files[0].name.split(".")[files[0].name.split(".").length - 1]
        });
        const reader = new FileReader();

        reader.onloadend = () => {
          // setState({
          //     ...state,
          //     imageUrls: reader.result
          // });
          urls.push(reader.result);
        };
        reader.readAsDataURL(files[i]);
      });
      setState({
        ...state,
        imageUrls: urls
      });
    } else {
      setState({
        ...state,
        imageUrls: []
      });
    }
  };

  const renderImage = data => {
    return (
      <div className="px-3">
        <img src={data} className="w-100" />{" "}
      </div>
    );
  };

  const handleChange = (e, fieldName) => {
    setField({
      ...field,
      [fieldName]: e.target.value
    });
  };

  const hanldleDateChange = (date, fieldName) => {
    setField({
      ...field,
      [fieldName]: date
    });
  };

  const renderInputBox = (lable, i) => {
    const identifier = lable
      .toLowerCase()
      .split(" ")
      .join("_");
    return (
      <div className="px-3">
        <Input
          className="pl-2"
          placeholder={lable}
          onChange={e => handleChange(e, identifier + i)}
          value={field[identifier + i] ? field[identifier + i] : ""}
        />
      </div>
    );
  };

  const machineType = (e, fieldName, data) => {
    let bill_no = "901";
    let internal = bill_no.replace(/^\D+/g, "");
    let internal_serial_no =
      e.target.value.substring(0, 2).toUpperCase() + "-" + internal;
    setField({
      ...field,
      [fieldName]: e.target.value,
      ["internal_serial_no" + data]: internal_serial_no
    });
  };

  const onHandleFileType = (e, fieldName, data) => {
    setField({
      ...field,
      [fieldName]: e.target.value
    });
  };

  const deleteInventory = id => {
    dispatch(actionsManageDevice.deleteTempUploadedInventory(id));
  };

  const renderSelectBox = (lable, data) => {
    let fieldIdentifier = lable
      .toLowerCase()
      .split(" ")
      .join("_");
    return (
      <div className="px-3">
        <select
          className="form-control pl-2"
          value={field[fieldIdentifier + data]}
          onChange={e =>
            lable == "Category"
              ? machineType(
                  e,
                  lable
                    .toLowerCase()
                    .split(" ")
                    .join("_") + data,
                  data
                )
              : onHandleFileType(
                  e,
                  lable
                    .toLowerCase()
                    .split(" ")
                    .join("_") + data,
                  data
                )
          }
        >
          <option value="">{`--Select ${lable}--`}</option>
          {lable == "Category"
            ? manageDevice.deviceList &&
              manageDevice.deviceList.length > 0 &&
              manageDevice.deviceList.map((item, i) => (
                <option key={i} value={item}>{item}</option>
              ))
            : state.file_types &&
              state.file_types.length &&
              state.file_types.map(item => (
                <option value={item}>{item}</option>
              ))}
        </select>
      </div>
    );
  };

  const onAssignInventory = i => {
    let check = false;
    let purchase_date = field["purchase_date" + i]
      ? new Date(field["purchase_date" + i])
      : new Date();
    let purchaseFormattedDate = moment(purchase_date).format("Y-MM-DD");
    let warrantyFormattedDate = moment(purchase_date)
      .add(1, "years")
      .format("Y-MM-DD");

    if(field["category" + i] && field["item_name" + i] && field["serial_no" + i] && field["internal_serial_no" + i]) {
      let apiData = {
        machine_type: field["category" + i].trim(),
        machine_name: field["item_name" + i].trim(),
        serial_no: field["serial_no" + i].trim(),
        bill_no: field["internal_serial_no" + i].trim(),
        file_id: manageDevice.tempInventory[i].file_id,
        id: manageDevice.tempInventory[i].id,
        purchase_date: purchaseFormattedDate,
        warranty: warrantyFormattedDate,
        status: "working"
      };
  
      dispatch(actionsManageDevice.addNewMachine(apiData, true))
        .then(
          res =>
            setState({
              ...state,
              status: {
                err: 0,
                message: res
              }
            }),
          setField({})
        )
        .catch(err => {
          setState({
            ...state,
            status: {
              err: 1,
              message: err
            }
          });
        });
    }
  };

  const renderSaveButton = (id, data) => {
    return (
      <>
        <Button
          color="success"
          className=""
          onClick={() => onAssignInventory(data)}
        >
          save{" "}
        </Button>
        <i
          className="material-icons delete-button pl-2 vertical-middle pointer"
          style={{ color: "red" }}
          onClick={() => {
            deleteInventory(id);
          }}
          aria-hidden="true"
        >
          delete
        </i>
      </>
    );
  };

  const renderDatePicker = (label, i) => {
    return (
      <div className="px-3">
        <GenericDatePicker
          className="pl-2"
          date={field[label + i] ? field[label + i] : new Date()}
          emptyLabel={label}
          handleDateChange={date => hanldleDateChange(date, label + i)}
        />
      </div>
    );
  };

  const list = manageDevice.tempInventory.map((v, i) => [
    renderImage(v.file_name, i),
    renderInputBox("Item Name", i),
    renderSelectBox("Category", i),
    renderInputBox("Internal Serial No", i),
    renderInputBox("Serial No", i),
    renderDatePicker("purchase_date", i),
    renderSaveButton(v.id, i)
  ]);

  list.unshift([
    "Image",
    "Item Name",
    "Category",
    "Internal Serial No",
    "Serial No",
    "purchase_date"
  ]);

  const cellRenderer = ({ columnIndex, key, rowIndex, style, parent }) => {
    return (
      <div key={key} style={style}>
        {list[rowIndex][columnIndex]}
      </div>
    );
  };

  return (
    <div>
      <Alert color="info" fade={false}>
        You can upload multiple documents together{" "}
      </Alert>
      <form
        action={CONFIG.upload_url}
        method="POST"
        encType="multipart/form-data"
      >
        <div className="form-group d-flex doc_inventory_upload">
          <label className="col-sm-3 col-12 p-0">Upload Document </label>
          <Dropzone onDrop={onFileDrop} multiple={true}>
            {({ getRootProps, getInputProps }) => {
              return (
                <section className="container m-0 p-0 col-sm-9 col-12">
                  <div {...getRootProps({ className: "dropzone" })}>
                    <input {...getInputProps()} />

                    <div className="drag_and_drop align-items-center">
                      {file &&
                        file.map((file, index) => (
                          <div className="font-weight-bold" key={file.name}>
                            {" "}
                            {file.name}
                          </div>
                        ))}
                        {/* state.imageUrls.map(v => (
                          <img
                            src={v}
                            className="mx-3 img-fluid inventory-doc-preview"
                            alt="Preview"
                          />
                        )) */}
                      {state.imageUrls && state.imageUrls.length ? (
                        null
                      ) : (
                        <p className="uploading_doc">
                          <i className="fi flaticon-upload" />
                        </p>
                      )}

                      <p className="doc_upload_place">
                        Drop a document here or click to select file to upload
                      </p>
                    </div>
                  </div>
                </section>
              );
            }}
          </Dropzone>
        </div>

        <div className="form-group offset-sm-3">
          <UploadMultipleFiles
            validateDocuments={validateDocuments}
            fileParams={{
              file: file,
              document_type: "multipleInventory",
              // document_type: state.document_type,
              // inventory_id: machineId,
              token: getToken(),
              // type: state.type,
              imageUrl: state.imageUrls,
              file_upload_action: "inventory_temp_images"
            }}
            url={"generic_upload_url"}
            // loadingSuccess={this.state.loading}
            //   onSuccessfulFileUpload={successfulFileUpload}
            handleRemoveImage={handleRemoveImage}
            //   uploaed_pic={uploaed_pic}
            //   remove_type={remove_type}
            //   showSuccessMessage={showSuccessMessage}
            notCompressForTesting={true}
            successfulFileUpload={successfulFileUpload}
          />
        </div>
      </form>
      <AutoSizer className="AutoSizer">
        {({ height, width }) => (
          <div className="col-md-12 bulk-inventory-table">
            <Grid
              // headerRowRenderer={headerRowRenderer}
              cellRenderer={cellRenderer}
              columnCount={7}
              columnWidth={170}
              // height={300}
              // rowCount={list.length}
              // rowHeight={30}
              // width={300}

              width={width}
              height={height}
              
              rowHeight={150}
              rowCount={manageDevice.tempInventory.length + 1}
              // rowGetter={({ index }) => manageDevice.tempInventory[index]}
            />
          </div>
        )}
      </AutoSizer>
    </div>
  );
}
