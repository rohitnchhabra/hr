import React from "react";
import Dialog from "material-ui/Dialog";
import PropTypes from "prop-types";
import { Button} from "reactstrap";
import RaisedButton from "material-ui/RaisedButton";
import CircularProgress from "material-ui/CircularProgress";

export default class AssignDevice extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      searchValue: "",
      filteredData: [],
      show:false
    };
  }
  onSearch = e => {
    this.setState({ searchValue: e.target.value });
  };

  handleSkipAudit=()=>{
    this.props.handleSkipAudit();
  }

  findItem = () => {
    const {allInventories} = this.props;
    let filterdata = ""
    this.setState({show:true})
    if (this.state.searchValue) {
       filterdata = this.props.unassignDeviceList.filter(device =>
        device.serial_number.includes(`${this.state.searchValue}`)
      ).length > 0 ? this.props.unassignDeviceList.filter(device =>
        device.serial_number==this.state.searchValue
      ):this.props.unassignDeviceList.filter(device =>
        device.bill_number==this.state.searchValue
      )
      
        if(filterdata.length > 0) {
          this.setState({ filteredData: filterdata,show:false  });
        }
        else {
          let notFoundInventory = allInventories.filter(device =>
            device.serial_number.includes(`${this.state.searchValue}`)
          ).length > 0 ? allInventories.filter(device =>
            device.serial_number==this.state.searchValue
          ):allInventories.filter(device =>
            device.bill_number==this.state.searchValue
          )
          let response = ''
          if(notFoundInventory.length >0){
            response = [{inventoryNotFoundError:`Device is currently assigned to ${notFoundInventory[0].name}. If you would like to assign device to yourself you can proceed.`}]
          }
          else{
            response = [{inventoryNotFoundError:"Either device is not available or you entered an invalid serial number."}]
          } 
          this.setState({ filteredData: response,show:false });
        }      
    } 
  };
  handleOpen = e => {
    e.stopPropagation();
    this.props.handleAddDialog();
  };
  close_dialog = () => {
    this.props.handleCloseAssign();
  };

  handleAssign = inventory_id => {
    const { user_id, callAssignDevice } = this.props;
    const { searchValue } = this.state;
    if(inventory_id ){
      callAssignDevice({inventory_id,user_id});
    }else{
      callAssignDevice({
        inventory_id,
        user_id ,
        inventory_serial_number:searchValue
      });
    }
  };

  render() {
    const filter_data = this.state.filteredData;    
    const actions = <>
      <RaisedButton
        label="Close"
        primary
        onClick={e => {
          this.close_dialog();
          this.setState({ searchValue: "",filteredData: "" });
        }}
      />
      {filter_data.length > 0 && filter_data[0] !== "Not Available" ?
      <RaisedButton
        className="mx-1"
        label="Assign Device"
        primary
        onClick={() => this.handleAssign(filter_data[0].id)}
      />:""}
    </>
    return (
      <div>
          <>
          <Button
            color="success"
            className="mb-xs mr-xs"
            onClick={this.handleOpen}
          >
            {" "}
            Assign Device{" "}
          </Button>
        </>
        
        {(!this.props.loading && this.props.loggedUser.data.hasOwnProperty("right_to_skip_inventory_audit") && this.props.loggedUser.data.right_to_skip_inventory_audit ) &&
          <Button
            color="primary"
            className="mb-xs mr-xs"
            onClick={this.handleSkipAudit}
           >
            {" "}
            Skip Audit{" "}
           </Button>
          }
        {this.props.unassignDeviceList.length > 0 &&
        <Dialog
          title="Assign Device"
          titleStyle={{ opacity: "0.56" }}
          actions={actions}
          modal={false}
          open={this.props.openAssign}
          contentStyle={{ width: "50%", maxWidth: "none" }}
          className="Dialog_box_view_inventory"
          autoScrollBodyContent
        >
          <div className="row m-0 w-100 py-2">
            <div className="col-sm-8">
              <input
                type="text"
                className="form-control w-100 "
                placeholder="Search Device by Serial Number"
                value={this.state.searchValue}
                onChange={this.onSearch}
              />
            </div>
            <div className="col-sm-3">
            {this.state.show ? <CircularProgress size={30} thickness={3}/>: 
              <Button className="inventory_find_view" color="primary" onClick={this.findItem}>
                Find
              </Button>}
            </div>
            
          </div>
          {filter_data.length > 0 && !filter_data[0].hasOwnProperty("inventoryNotFoundError") ? 
          <div className="pl-2">
          <div className="d-flex">
            <div className="px-2 font-weight-bold">Device</div>
            <div className="px-2">{filter_data[0].machine_type}</div>
          </div>
          <div className="d-flex">
            <div className="px-2 font-weight-bold">Name</div>
            <div className="px-2">{filter_data[0].machine_name}</div>
          </div></div>:<div className="pl font-weight-bold"> {filter_data.length >0 && filter_data[0].inventoryNotFoundError} </div>} 
        </Dialog>}
      </div>
    );
  }
}

AssignDevice.propTypes = {
  callAssignDevice: PropTypes.func.isRequired
};
