import React, { Component } from "react";
import Header from "../../../components/generic/Header";
import { connect } from "react-redux";
import * as actions from "../../../redux/actions";
import { CONFIG } from "../../../config";
import * as actionsUsersList from "../../../redux/generic/actions/usersList";
import * as actionsManageUsers from "../../../redux/manageUsers/actions/manageUsers";
import * as actionsManageDevice from "../../../redux/inventory/actions/inventory";
import * as actionsMyProfile from "../../../redux/myProfile/actions/myProfile";
import { getToken } from "../../../services/generic";
import { CSVLink } from "react-csv/lib";
import moment from "moment";
import { dateFormatter } from "../../../helper/helper";
import { Link } from "react-router-dom";
import Heading from "../../../components/generic/Heading";
import classnames from "classnames";
import filter from "lodash/filter";
import AuditInventoryEmployeeWise from "./AuditInventoryEmployeeWise";
import GenricCardList from "../../../components/generic/GenricCardList";
import _ from "lodash";
import AuditWiseList from "./AuditWiseList";
import { Row, Col, Nav, NavItem, NavLink } from "reactstrap";

class AuditInventoryList extends Component {
  constructor() {
    super();
    let date = new Date();
    this.state = {
      month: date.getMonth() + 1,
      year: date.getYear() + 1900,
      tab: 0,
      activeAudits: [],
      machineType:"",
      tab:0,
    };
    this.Year = [];
  }
  componentWillMount() {
    this.props.onUsersList();
    this.props.getGenericConfig();
    for (let i = 0; i < 5; i++) {
      this.Year.push(parseInt(this.state.year) + i - 1);
    }
    this.props.auditList(getToken(), this.state.month, this.state.year);
    this.props.showHeading("Audit Summary")
  }

  handleAuditSubmit = (activeAuditId, auditMsg, auditType) => {
    const activeBtn = this.state.activeAudits.concat(activeAuditId);
    this.setState({
      activeAudits: activeBtn
    });
    // this.toggle(id);
    this.props
      .onAddAuditComment(activeAuditId, auditMsg, auditType)
      .then(res => {
        // const active = filter(this.state.activeAudits, function(n) {
        //   return n !== activeAuditId;
        // });
        // this.setState({
        //   activeAudits: active
        // });
        this.props.auditList(getToken(), this.state.month, this.state.year);
      })
      .catch(() => {
        const remainActive = filter(this.state.activeAudits, function(n) {
          return n !== activeAuditId;
        });
        this.setState({
          activeAudits: remainActive
        });
      });
  };

  onMonthChange = e => {
    this.setState({ month: e.target.value });
    this.props.auditList(getToken(), e.target.value, this.state.year);
  };
  onYearChange = e => {
    this.setState({ year: e.target.value });
    this.props.auditList(getToken(), this.state.month, e.target.value);
  };

  setSelected=(machineType)=>{
    this.setState({machineType});
  }
  render() {
    let auditComments={
      all_good: " Issue discussed with employee and is not an issue",
      critical_issue: "Issue is genuine and will get it repaired",
      issue: "Issue is genuine and will be rectified later on"
  }
    let rows = null;
    let datas = [];
    const {machineType, tab} = this.state;
    const path = CONFIG.inventory_images;
    const optionsMonth = dateFormatter().months.map((item, index) => {
      return (
        <option key={index} value={index + 1}>
          {item}
        </option>
      );
    });
    const optionsYear = this.Year.map((item, index) => {
      return (
        <option key={index} value={item}>
          {item}
        </option>
      );
    });
    let totalPerItem = []
    let count = {};
    if (Object.keys(this.props.manageDevice.auditData).length !== 0) {
      this.props.manageDevice.auditData.audit_list.forEach((item)=> totalPerItem.push(item.machine_type));
      totalPerItem.forEach((i)=> count[i] = (count[i]||0) + 1);
      rows = this.props.manageDevice.auditData.audit_list.map((item, index) => {
        datas.push({
          "Inventory Id": item.id,
          "Inventory Name": item.machine_name,
          "Inventory Type": item.machine_type,
          "Assigned To": item.assigned_name,
          "Assigned Id": item.assigned_user_id,
          Comment: item.comment,
          "Audit By Name": item.audit_done_by,
          "Audit By User Id": item.audit_done_by_user_id,
          Month: item.month,
          Year: item.year
        });
        return null
      });
    }
    return (
      <div>
        <Header
          pageTitle={"Inventories Audit"}
          showLoading={this.props.frontend.show_loading}
        />
        <div className="row no-gutters">
          <div className="col-md-2 day-color-referance white p-2">
            <h4>
              {Object.keys(this.props.manageDevice.auditData).length !==
                0 &&
                this.props.manageDevice.auditData.stats.total_inventories}
            </h4>
            <div className="text-u-c text-sm">Total Inventory</div>
          </div>
          <div className="col-md-10">
            <div className="row no-gutters">
              <div className="col-xs-12 col-sm-4 day-color-referance grey-600 p-2">
                <h4>
                  {Object.keys(this.props.manageDevice.auditData).length !==
                    0 && this.props.manageDevice.auditData.stats.audit_pending}
                </h4>
                <div className="text-u-c text-sm"> Pending Audit</div>
              </div>

              <div className="col-xs-12 col-sm-4 day-color-referance light-blue-100 p-2">
                <h4>
                  {Object.keys(this.props.manageDevice.auditData).length !==
                    0 && this.props.manageDevice.auditData.stats.audit_done}
                </h4>
                <div className="text-u-c text-sm">Done Audit</div>
              </div>

              <div className="col-xs-12 col-sm-4 day-color-referance indigo p-2">
                <h4>
                  {Object.keys(this.props.manageDevice.auditData).length !==
                    0 &&
                    this.props.manageDevice.auditData.stats
                      .unassigned_inventories}
                </h4>
                <div className="text-u-c text-sm">Unassigned Inventory</div>
              </div>

              <div className="col-xs-12 col-sm-4 day-color-referance green p-2">
                <h4>
                  {Object.keys(this.props.manageDevice.auditData).length !==
                    0 && this.props.manageDevice.auditData.stats.audit_good}
                </h4>
                <div className="text-u-c text-sm"> Audit Good</div>
              </div>

              <div className="col-xs-12 col-sm-4 day-color-referance orange p-2">
                <h4>
                  {Object.keys(this.props.manageDevice.auditData).length !==
                    0 && this.props.manageDevice.auditData.stats.audit_issue}
                </h4>
                <div className="text-u-c text-sm"> Audit Issue</div>
              </div>

              <div className="col-xs-12 col-sm-4 day-color-referance red p-2">
                <h4>
                  {Object.keys(this.props.manageDevice.auditData).length !==
                    0 &&
                    this.props.manageDevice.auditData.stats
                      .audit_critical_issue}
                </h4>
                <div className="text-u-c text-sm"> Audit Critical Issue</div>
              </div>
            </div>
          </div>
        </div>

        <div className="mt-4">
          <div
            className="row no-gutters pt-1 pb-2"
            style={{ position: "relative" }}
          >
            <div className="col-md-2 link-dn-2">
              <select
                className="form-control"
                onChange={this.onYearChange}
                value={this.state.year}
              >
                {optionsYear}
              </select>
            </div>
            <div className="col-md-2 link-dn-2 px-sm-3">
              <select
                className="form-control"
                onChange={this.onMonthChange}
                value={this.state.month}
              >
                {optionsMonth}
              </select>
            </div>
            <div className="col-md-3 dn-csv-link">
            <CSVLink
              className="download-csv-link"
              data={datas}
              filename={`audit-${moment().format("MMM-YYYY-DD-MMM-YYYY")}.csv`}
            >
              {" "}
              <i className="fi flaticon-download" />
            </CSVLink>
            </div>
          </div>
          <Nav className="status_header bg-body manage-users-nav-bar d-flex mb-4" tabs>
            <NavItem className="flex-fill text-center">
              <NavLink
                className={`${tab === 0 ? "border-warning" : "active"} leave-tab-header`}          
                onClick={() => {
                  this.setState({tab:0})
                }}
              >
                <span className="leave-status-link">Employee Wise</span>
              </NavLink>
            </NavItem>
            <NavItem className="flex-fill text-center">
              <NavLink
                className={`${tab === 1 ? "border-success" : "active"} leave-tab-header`}          
                onClick={() => {
                  this.setState({tab:1})
                }}
              >
                <span className="leave-status-link">Audit Wise</span>
              </NavLink>
            </NavItem>
          </Nav>
          {/* <div>
            <div
              className={classnames("manage-users-tab", "mb-2", "p-3", {
                active: this.state.tab === 0
              })}
              onClick={() => {
                this.setState({ tab: 0 });
              }}
            >
              Employee Wise
            </div>
            <div
              className={classnames("manage-users-tab", "mb-2", "p-3", {
                active: this.state.tab === 1
              })}
              onClick={() => {
                this.setState({ tab: 1 });
              }}
            >
              Audit
            </div>
          </div> */}
          {this.state.tab === 0 ? (
            this.props.manageDevice.auditData &&
            this.props.manageDevice.auditData.audit_list_employee_wise &&
            this.props.manageDevice.auditData.audit_list_employee_wise
              .length ? (
              <AuditInventoryEmployeeWise
                audit_list_employee_wise={
                  this.props.manageDevice.auditData.audit_list_employee_wise
                }
                auditDataStatus={this.props.manageDevice.auditDataStatus}
                auditComments={auditComments}
                handleAuditSubmit={this.handleAuditSubmit}
                activeAudits={this.state.activeAudits}
                showPending={this.props.showPending}
                auditBise={false}
              />
            ) : null
          ) : (
            this.props.manageDevice.auditData &&
            this.props.manageDevice.auditData.audit_list_employee_wise &&
            this.props.manageDevice.auditData.audit_list_employee_wise
              .length ? (
              <AuditWiseList
                audit_list_employee_wise={
                  _.uniqBy(this.props.manageDevice.auditData.audit_list,"machine_type")
                }
                machineType={machineType}
                count={count}
                setSelected={this.setSelected}
                employee_wise_list = {this.props.manageDevice.auditData.audit_list}
                history={this.props.history}
              />
            ) : null
          )}
        </div>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    frontend: state.frontend.toJS(),
    usersList: state.usersList.toJS(),
    manageUsers: state.manageUsers.toJS(),
    loggedUser: state.logged_user.userLogin,
    manageDevice: state.manageDevice.toJS(),
    showPending: state.myProfile.toJS().showPending,
    inventoryAuditComments: state.settings.inventory_audit_comments
  };
}

const mapDispatchToProps = dispatch => ({
  onUsersList: () => dispatch(actionsUsersList.get_users_list()),
  onUserProfileDetails: (userid, username) =>
    dispatch(actionsManageUsers.getUserProfileDetails(userid, username)),
  auditList: (token, month, year) =>
    dispatch(actionsManageDevice.getAuditList(token, month, year)),
  getGenericConfig: () => dispatch(actions.getGenricLoginConfig()),
  onAddAuditComment: (id, msg, auditType) => {
    return dispatch(actionsMyProfile.addInventoryAudit(id, msg, auditType));
  },
  showHeading:(data)=>{
    return dispatch(actions.showHeading(data))
  }
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(AuditInventoryList);
