import React from "react";
import Dialog from "material-ui/Dialog";
import RaisedButton from "material-ui/RaisedButton";
import { notify, confirm } from "../../../services/notify";
// import style from '../../../styles/inventory/viewUser.scss'
import { Button } from "reactstrap";
import isEmpty from "lodash/isEmpty";
import Task from "../../../components/Task/Task";

export default class AddDeviceStatus extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      background: "",
      colorPicker: "hide",
      statusType: "",
      checkValue: "",
      status_message: "",
      option: {},
      new_status: ""
    };
    this.addMoreStatus = this.addMoreStatus.bind(this);
    this.handleDelete = this.handleDelete.bind(this);
    this.handleChangeComplete = this.handleChangeComplete.bind(this);
    this.handleStatusClose = this.handleStatusClose.bind(this);
  }

  callDeleteDeviceStatus = (checkValue, new_status) => {
    this.props.onDeleteDeviceStatus(checkValue, new_status).then(val => {
      if (val.error === 1) {
        this.setState({
          statusType: "",
          background: "",
          checkValue: "",
          option: {},
          new_status: ""
        });
        notify("Error !", "This Device Status Type Is In Use", "error");
        this.handleStatusClose();
      } else if (val.message) {
        this.setState({
          status_message: val.message
        });
        this.handleStatusClose();
        this.props.onFetchDeviceStatus();
      } else {
        this.setState({
          statusType: "",
          background: "",
          checkValue: "",
          option: {},
          new_status: ""
        });
        this.handleStatusClose();
        this.props.onFetchDeviceStatus();
      }
    });
  };

  componentWillReceiveProps(props) {
    this.setState({
      statusList: props.deviceStatusList,
      open: props.open
    });
  }

  addMoreStatus(event) {
    event.preventDefault();
    let statusValue = this.state.statusType.trim();
    if (statusValue !== "") {
      this.props.callAddStatus(statusValue);
      this.setState({
        statusType: "",
        background: ""
      });
    } else {
      notify("Warning !", "First Fill Status Type", "warning");
    }
  }

  handleChangeComplete(color) {
    this.setState({ background: color.hex });
  }

  handleDelete() {
    this.callDeleteDeviceStatus(this.state.checkValue, this.state.new_status);
  }

  handleStatusClose() {
    // this.props.handleStatusClose();
    this.setState({
      openStatus: false,
      statusType: "",
      background: "",
      option: {},
      new_status: "",
      checkValue: ""
    });
  }

  render() {
    const actions = [
      <RaisedButton
        label="Close"
        primary
        onClick={this.props.handleStatusClose}
      />
    ];
    return (
      <>
        <Button color="success" onClick={this.props.handleStatusOpen} className="align-self-center">
          Add Status Type
        </Button>
        <Dialog
          title={"ADD STATUS TYPE"}
          titleStyle={{ opacity: "0.56" }}
          actions={actions}
          modal={false}
          open={this.props.open}
          onRequestClose={this.handleStatusClose}
          autoScrollBodyContent
        >
          <div className="row m-0">
            <div className="col-sm-12">
              <label>Status Type List</label>
              <ol>
                {this.props.manageDevice.statusList.map((val, i) => {
                  return (
                    <li
                      key={i}
                      className="d-sm-flex status-list justify-content-between"
                    >
                      <div className="col-sm-3 ml-1 p-0 d-flex">
                        <Task
                          className="manage_role_size"
                          text={""}
                          index={i}
                          id={val.id}
                          status={this.state.checkValue === val.status}
                          handleChangeSteps={() => {
                            if (Number(val.is_default) == 1) {
                              return null;
                            } else {
                              this.setState({
                                checkValue: val.status,
                                option: val
                              });
                            }
                          }}
                          disabled={Number(val.is_default) == 1}
                        />
                        <div
                          style={{ borderLeft: `2px solid ${val.color}` }}
                          className="d-flex align-items-center"
                        >
                          <span className="pl-2">{val.status}</span>
                        </div>
                      </div>
                      <div className="d-sm-flex my-sm-0 my-2 col-sm-9">
                      {this.state.option.id === val.id &&
                      Number(this.state.option.total_inventories) > 0 ? (
                        <select
                          className="form-control mr-sm-2 mb-sm-0 mb-2"
                          value={this.state.new_status}
                          onChange={e => {
                            this.setState({ new_status: e.target.value });
                          }}
                        >
                          <option>Selet status for existing inventories</option>
                          {this.props.manageDevice.statusList.map((val, i) => {
                            if (val.status === this.state.checkValue) {
                              return null;
                            } else {
                              return (
                                <option
                                  value={val.status}
                                  disabled={
                                    val.status === this.state.checkValue
                                  }
                                >
                                  {val.status}
                                </option>
                              );
                            }
                          })}
                        </select>
                      ) : null}
                      {!isEmpty(this.state.option) &&
                      this.state.option.id === val.id ? (
                        <button
                          type="button"
                          aria-hidden="true"
                          className="delete-status btn btn-danger btn-sm"
                          onClick={() => {
                            if (
                              this.state.new_status === "" &&
                              Number(this.state.option.total_inventories) > 0
                            ) {
                              notify(
                                "Error !",
                                "Please Select a status for existing inventories",
                                "error"
                              );
                            } else {
                              confirm(
                                "Are you sure ?",
                                "Do you want to delete this Device Status ?",
                                "warning"
                              ).then(res => {
                                if (res) {
                                  this.handleDelete();
                                }
                              });
                            }
                          }}
                        >
                          Delete
                        </button>
                        
                      ) : null}
                      </div>
                    </li>
                  );
                })}
              </ol>
              <vr />
            </div>
            <div className="row m-0 w-100">
              <label className="col-sm-2 col-form-label">Add Status</label>
              <div className="col-sm-7 mb-sm-0 mb-2">
                <input
                  type="text"
                  className="form-control w-100"
                  placeholder="Add Status"
                  ref="value"
                  value={this.state.statusType}
                  onBlur={e => {
                    this.setState({
                      statusType: this.state.statusType.trim(),
                      colorPicker: "hide"
                    });
                  }}
                  onChange={e => {
                    this.setState({
                      colorPicker: "show",
                      statusType: e.target.value
                    });
                  }}
                />
              </div>
              <div className="col-sm-3">
                <Button
                  color="primary"
                  disabled={this.state.statusType === ""}
                  className="w-100"
                  onClick={this.addMoreStatus}
                >
                  Add Status
                </Button>
              </div>
              {/* {this.state.statusType ? (
                <div className="col-sm-12 well">
                  <label>Add Color</label>
                  <div className="row">
                    <div className="col-sm-6 ">
                      <GithubPicker
                        color={this.state.background}
                        onChangeComplete={this.handleChangeComplete}
                        triangle={"top-left"}
                      />
                    </div>
                    <div className="col-sm-6">
                      <div className="panel panel-default">
                        <div className="panel-heading">Selected Color</div>
                        <div
                          style={{
                            backgroundColor: this.state.background,
                            height: "41px"
                          }}
                          value={this.state.background}
                          className="panel-body"
                        ></div>
                      </div>
                    </div>
                  </div>
                </div>
              ) : null} */}
            </div>
          </div>
        </Dialog>
      </>
    );
  }
}
