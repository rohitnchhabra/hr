import React, { useState, useEffect } from "react";
import { Button, Spinner } from "reactstrap";
import classnames from "classnames";
import map from "lodash/map";
import GenricCardList from "../../../components/generic/GenricCardList";
import cloneDeep from "lodash/cloneDeep";

export default function AuditInventoryEmployeeWise({
  audit_list_employee_wise: audit_list,
  auditComments,
  handleAuditSubmit,
  activeAudits,
  showPending,
  auditDataStatus
}) {
  const [selected, setSelected] = useState(0);
  const [comment, setComment] = useState({});
  const [error, setError] = useState({});
  let inventoryStatusClass;
  useEffect(() => {
    let state = cloneDeep(comment);
    map(activeAudits, (data, i) => {
      delete state[data];
    });
  }, [audit_list[selected].inventories]);
  if (
    audit_list &&
    audit_list[selected] &&
    audit_list[selected].inventories.length
  ) {
    inventoryStatusClass = audit_list[selected].inventories.some(
      v => v.comment_type === "critical_issue"
    )
      ? "status-critical-issue"
      : audit_list[selected].inventories.some(v => v.comment_type === "issue")
      ? "status-issue"
      : audit_list[selected].inventories.some(
          v => v.comment_type === "all_good"
        )
      ? "status-good"
      : "status-pending";
  }

  useEffect(()=>{
    if(auditDataStatus){
      setComment({})
    }
  },[auditDataStatus])

  let details = () => {
    return (
      <div className="col-md-9 px-0">
        <div
          className={classnames(
            inventoryStatusClass,
            "overflow-auto px-0",
            "bg-white",
            "ml-sm-1"
          )}
        >
          {audit_list &&
          audit_list[selected] &&
          audit_list[selected].inventories.length
            ? audit_list[selected].inventories.map((v, i) => {
                return (
                  <div className="px-4">
                    <div className="machineDetailInfo p-t">
                      <h4 className="darkGrey">
                        {v.machine_name} ({v.id})
                      </h4>
                      <h6 className="lightGrey">{v.machine_type}</h6>
                      <div className="d-flex">
                        <h6 className="lightGrey">Internal Serial No :</h6>
                        <h6 className="darkGrey">{v.bill_number}</h6>
                      </div>
                    </div>
                    <div className="machineDetailInfo">
                      {v.audit_done_by_user_id ? (
                        <div>
                          <div>
                            <b>
                              <i>
                                Commented by: {v.audit_done_by} (
                                {v.audit_done_by_user_id})
                              </i>
                            </b>
                          </div>
                          {v.comment ? v.comment :v.comment_type}
                        </div>
                      ) : (
                        "Audit pending"
                      )}
                    </div>
                    {v.comment_type === "issue" ||
                    v.comment_type === "critical_issue" ? (
                      <div className="machineDetailInfo width50">
                        <div>
                          <b>Comment after talking with employee</b>
                        </div>
                        <select
                          onChange={e => {
                            e.persist();
                            if (
                              e.target.value === "issue" ||
                              e.target.value === "critical_issue"
                            ) {
                              setComment(previous => ({
                                ...previous,
                                [v.id]: {
                                  comment_type: e.target.value,
                                  auditMsg: auditComments[e.target.value]
                                }
                              }));
                            } else {
                              setComment(previous => ({
                                ...previous,
                                [v.id]: { comment_type: e.target.value,
                                  auditMsg: auditComments[e.target.value] }
                              }));
                            }
                          }}
                          value={
                            comment[v.id] && comment[v.id].comment_type
                              ? comment[v.id].comment_type
                              : ""
                          }
                          className="form-control mb-1 mt-1"
                        >
                          <option value="">Select a status</option>
                          {map(auditComments, (data, key) => {
                            return <option value={key}>{data}</option>;
                          })}
                        </select>
                        {/* {(comment[v.id] &&
                          comment[v.id].comment_type === "issue") ||
                        (comment[v.id] &&
                          comment[v.id].comment_type === "critical_issue") ? (
                          <div className="d-flex flex-column">
                            <input
                              type="text"
                              className="form-control mb-1 mt-1"
                              value={comment[v.id].auditMsg}
                              onChange={e => {
                                // setError(false);
                                e.persist();
                                setComment(previous => ({
                                  ...previous,
                                  [v.id]: {
                                    ...previous[v.id],
                                    auditMsg: e.target.value
                                  }
                                }));
                                setError(previous => ({
                                  ...previous,
                                  [v.id]: {
                                    error: false
                                  }
                                }));
                              }}
                              onKeyUp={e => {
                                if (e.keyCode === 13) {
                                  if (comment[v.id].auditMsg != "") {
                                    handleAuditSubmit(
                                      v.id,
                                      comment[v.id].auditMsg,
                                      comment[v.id].comment_type
                                    );
                                  } else {
                                    setError(previous => ({
                                      ...previous,
                                      [v.id]: {
                                        error: true
                                      }
                                    }));
                                  }
                                }
                              }}
                              required
                            />
                            {error[v.id] && error[v.id].error ? (
                              <small
                                style={{ color: "red" }}
                                className="text-right"
                              >
                                Fill the field
                              </small>
                            ) : null}
                          </div>
                        ) : null} */}
                        {comment[v.id] && comment[v.id].comment_type !== "" ? (
                          <>
                            <Button
                              size="sm"
                              color="success"
                              className="mb-1 float-right"
                              onClick={
                                comment[v.id].comment_type === "all_good"
                                  ? () => {
                                      handleAuditSubmit(
                                        v.id,
                                        comment[v.id].auditMsg,
                                        comment[v.id].comment_type
                                      );
                                    }
                                  : comment[v.id].auditMsg !== ""
                                  ? () => {
                                      handleAuditSubmit(
                                        v.id,
                                        comment[v.id].auditMsg,
                                        comment[v.id].comment_type
                                      );
                                    }
                                  : () => {
                                      setError(previous => ({
                                        ...previous,
                                        [v.id]: {
                                          error: true
                                        }
                                      }));
                                    }
                              }
                            >
                              {activeAudits.includes(v.id) && showPending ? (
                                <Spinner color="light" size="sm" />
                              ) : (
                                <small>Submit</small>
                              )}
                            </Button>
                          </>
                        ) : null}
                      </div>
                    ) : null}
                  </div>
                );
              })
            : "No Inventory Assigned"}
        </div>
      </div>
    );
  };

  return (
    <>
      <div className="assign-member scroll col-md-3 p-0">
        {audit_list
          ? audit_list.map((item, index) => {
              return (
                <>
                  <GenricCardList
                    onCardSelected={() => {
                      setSelected(index);
                    }}
                    cardBodyClassName={`${
                      Number(item.audit_critical_issue) > 0
                        ? "status-critical-issue-card"
                        : Number(item.audit_issue) > 0
                        ? "status-issue-card"
                        : Number(item.audit_pending_count) > 0
                        ? "status-pending-card"
                        : Number(item.audit_good) > 0
                        ? "status-good-card"
                        : ""
                    } ${index === selected ? "card-selected" : ""}`}
                    cardClassname={"border-0 mb-1 pointer"}
                    heading={item.employee.emp_name}
                    subHeading={`${"Emp ID: " + item.employee.emp_userid}`}
                  />
                  {window.screen.availWidth <= 768 &&
                  audit_list &&
                  audit_list[selected].employee.emp_userid ===
                    item.employee.emp_userid
                    ? details()
                    : null}
                </>
              );
            })
          : null}
      </div>
      {window.screen.availWidth >= 768 ? details() : null}
    </>
  );
}
