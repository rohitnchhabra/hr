import React from "react";
import { connect } from "react-redux";
import { compose } from "redux";
import { withRouter } from "react-router";
import AlertNotification from "../../../components/generic/AlertNotification";
import * as actionsManageDevice from "../../../redux/inventory/actions/inventory";
import * as actionsUsersList from "../../../redux/generic/actions/usersList";
import * as actionsManageUsers from "../../../redux/manageUsers/actions/manageUsers";
import * as actions from '../../../redux/actions'
// import style from "../../../styles/inventory/viewUser.scss";
import { isNotUserValid } from "../../../services/generic";
import { Input, Button, Spinner, Alert } from "reactstrap";
import Heading from "../../../components/generic/Heading";
import AlertFader from "../../../components/generic/AlertFader";
import AddInventoryDocument from "./AddInventoryDocument";
import isMobile from "../../../components/hoc/WindowResize";
import GenericDatePicker from "../../../components/generic/GenericDatePicker";
var moment = require("moment");
let newdate;
let selectedOption = "0";
let purchase;
let warranty;
let datef;

class FormAddNewInventory extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      open: false,
      edit: false,
      id: "",
      user: "",
      autoOk: true,
      machine_type: "",
      machine_name: "",
      machine_price: "",
      serial_no: "",
      purchase_date: new Date(),
      operating_system: "",
      comment: "",
      warranty_comment: "",
      repair_comment: "",
      bill_no: "",
      warranty: "",
      user_Id: "unassign",
      msg: "",
      deviceTypeList: [],
      deviceStatusList: [],
      status: " working",
      loading: false,
      unassign_comment: "",
      warranty_years: "1",
      modalDeviceDialog: false,
      modalStatusDialog: false,
      openMachineDocument: false,
      openDownload:false,
      isMobile: false,
      file_type:[],
      uploaed_pic:[],
      success:""
    };
    this.handleChangeDate = this.handleChangeDate.bind(this);
    this.handleAddDevice = this.handleAddDevice.bind(this);
    this.handleAssign = this.handleAssign.bind(this);
    this.handleAddDialog = this.handleAddDialog.bind(this);
    this.openEditDevice = this.openEditDevice.bind(this);
  }

  componentWillMount() {
    this.props.onUsersList();
    this.props.onFetchDeviceType();
    this.props.onFetchDeviceStatus();
    let file_type=["Photo","Warranty","Inovice"]
    this.setState({file_type:file_type})
  }
  openEditDevice(id) {
    this.props.onGetDeviceById(id).then(val => {
      this.setState({
        edit: true,
        open: true,
        deviceId: id,
        status_message: "",
        getByIdData: val
      });
    });
  }
  componentWillReceiveProps(props) {
    let isNotValid = isNotUserValid(
      this.props.location.pathname,
      props.loggedUser
    );
    this.setState({
      open: props.open,
      edit: props.edit,
      deviceTypeList: props.manageDevice.deviceList,
      deviceStatusList: props.manageDevice.statusList,
      username: props.manageUsers.username,
      user_profile_detail: props.manageUsers.user_profile_detail,
      user_assign_machine: props.manageUsers.user_assign_machine,
    });
    if(props.manageDevice.internal_no.toString() !== this.props.manageDevice.internal_no.toString() ){
      this.setState({bill_no:props.manageDevice.internal_no.toString()})
    }
    if (props.manageDevice.editData.edit) {
      purchase = moment(props.manageDevice.editData.device.date_of_purchase)._d;
      warranty = moment(props.manageDevice.editData.device.warranty_end_date)
        ._d;

      // var a = moment(props.manageDevice.editData.device.warranty_end_date);
      // var b = moment(props.manageDevice.editData.device.date_of_purchase);
      // var diffDuration = moment.duration(a.diff(b));
      // let year=diffDuration.years();
      // let month=diffDuration.months();
      // let day=diffDuration.days();
      // if(year>0 && month==0 && day==0){
      //   this.setState({
      //     warranty_years:year
      //   })
      // }
      // else if(year<0 && month==6 && day==0){
      //   this.setState({
      //     warranty_years:month
      //   })
      // }
      // else{
      //   this.setState({
      //     warranty_years:''
      //   })
      // }
      if (!this.state.id) {
        this.setState({
          id: props.manageDevice.editData.device.id,
          machine_type: props.manageDevice.editData.device.machine_type,
          machine_name: props.manageDevice.editData.device.machine_name,
          machine_price: props.manageDevice.editData.device.machine_price,
          serial_no: props.manageDevice.editData.device.serial_number,
          purchase_date: purchase,
          operating_system: props.manageDevice.editData.device.operating_system,
          status: props.manageDevice.editData.device.status,
          comment: props.manageDevice.editData.device.comments,
          warranty_comment: props.manageDevice.editData.device.warranty_comment,
          repair_comment: props.manageDevice.editData.device.repair_comment,
          bill_no: props.manageDevice.editData.device.bill_number,
          warranty: warranty,
          user_Id: props.manageDevice.editData.device.user_Id,
          unassign_comment: props.manageDevice.editData.device.unassign_comment,
          warranty_years: props.manageDevice.editData.device.warranty_years
        });
      }
      if (
        this.props.manageDevice.status_message ===
        "Successfully Updated into table"
      ) {
        this.props.manageDevice.status_message = "";
      }
    } else if (
      this.props.manageDevice.status_message ==
      "Inventory added successfully and need to be approved by admin!!"
    ) {
      this.setState({
        id: "",
        machine_type: "",
        machine_name: "",
        machine_price: "",
        serial_no: "",
        purchase_date: "",
        operating_system: "",
        status: "",
        comment: "",
        warranty_comment: "",
        repair_comment: "",
        bill_no: "",
        warranty: "",
        user_Id: "unassign",
        loading: false,
        unassign_comment: "",
        warranty_years: "0"
      });
      this.props.manageDevice.status_message = "";
    }
  }
  onclose =()=>{
    let file_type=["Photo","Warranty","Inovice"]
    this.setState({
      openDownload:false,
      machine_name: "",
      purchase_date: new Date(),
      warranty_years: "1",
      machine_type: "",
      status: " working",
      bill_no: "",
      user_Id: "unassign",
      machine_price:"",
      serial_no: "",
      unassign_comment: "",
      isMobile: false,
      uploaed_pic:[],
      file_type:file_type
    }) 
    this.props.onFetchDeviceType();
  }
  handleAddDevice() {
    newdate = new Date(this.state.purchase_date);
    let purchaseDate = new Date(this.state.purchase_date);
    if (selectedOption == 0.5) {
      newdate.setMonth(newdate.getMonth() + 6);
    } else if (selectedOption == 1) {
      newdate.setFullYear(newdate.getFullYear() + 1);
    } else if (selectedOption == 2) {
      newdate.setFullYear(newdate.getFullYear() + 2);
    } else if (selectedOption == 3) {
      newdate.setFullYear(newdate.getFullYear() + 3);
    } else if (selectedOption == 5) {
      newdate.setFullYear(newdate.getFullYear() + 5);
    }

    let purchaseFormattedDate = moment(purchaseDate).format("Y-MM-DD");
    let someFormattedDate;
    if (newdate !== "None") {
      someFormattedDate = moment(newdate).format("Y-MM-DD");
    } else {
      someFormattedDate = "None";
    }
    let apiData =
     {
      machine_type: this.state.machine_type,
      machine_name: this.state.machine_name.trim(),
      machine_price: this.state.machine_price.trim(),
      serial_no: this.state.serial_no.trim(),
      purchase_date: purchaseFormattedDate,
      operating_system: this.state.operating_system,
      status: this.state.status,
      comment: this.state.comment.trim(),
      warranty: someFormattedDate,
      warranty_comment: this.state.warranty_comment.trim(),
      repair_comment: this.state.repair_comment.trim(),
      bill_no: this.state.bill_no,
      user_Id: this.state.user_Id,
      unassign_comment: this.state.unassign_comment,
      warranty_years: this.state.warranty_years
    };
    let resetFields = {
      machine_type: "",
      machine_name: "",
      machine_price: "",
      serial_no: "",
      purchase_date: "",
      operating_system: "",
      comment: "",
      warranty_comment: "",
      repair_comment: "",
      bill_no: "",
      warranty: "",
      user_Id: "",
      unassign_comment: "",
      warranty_years: ""
    };
    let validate = true;
    this.setState({
      loading: true
    });
    if (validate && !this.props.manageDevice.editData.edit) {
      this.props.onAddNewMachine(apiData).then(
        val => {
          this.setState({
            loading: false,
            error: false,
            alertMessage: val
          });
          this.setState({
            alertMessage: ""
          });
          // this.props.history.push(
          //   `/app/inventoryOverviewDetail/${this.state.machine_type}`
          // );
          this.props.onFetchDevice();
          this.setState({openDownload:true})
        },
        error => {
          this.setState({
            loading: false,
            error: true,
            alertMessage: error
          });
          this.setState({
            alertMessage: ""
          });
        }
      );
    } else if (validate) {
      this.props
        .onUpdateDevice(this.state.id, apiData)
        .then(message => {
          this.setState({
            loading: false,
            error: false,
            alertMessage: message + "" + "redirecting in 5sec"
          });
          this.props.onFetchDevice();
          setTimeout(() => {
            this.props.history.push(
              `/app/inventoryOverviewDetail/${this.state.machine_type}`
            );
          }, 5000);
          this.setState({
            alertMessage: ""
          });
        })
        .catch(message => {
          this.setState({
            msg: message,
            loading: false,
            error: true,
            alertMessage: message
          });
          this.setState({
            alertMessage: ""
          });
        });
    }
    return false;
    
  }

  componentDidUpdate(prevProps) {
    if (
      prevProps.manageDevice.addedMachine.error !==
      this.props.manageDevice.addedMachine.error
    ) {
      this.toggleAddMachineDocument();
    }
    
    if(this.props.manageDevice.editData!==prevProps.manageDevice.editData){
      let editData = this.props.manageDevice.editData;
      if(editData.edit){
        this.props.showHeading("Edit Inventory")
      }
      else{
        this.props.showHeading("Add Inventory")
      }
    }
    
  }
  handleAssign(deviceId, Userid) {
    this.setState({ userId: Userid });
    this.props.callAssign(deviceId, Userid);
  }

  handleChangeDate(event, date) {
    this.setState({
      purchase_date: date
    });
  }
  showSuccessMessage = () => {
    this.setState({success:"File successfully uploaded"},()=>{
      this.setState({success:""})
    })
  }
  handleAddDialog() {
    this.setState({
      deviceId: "",
      open: true,
      status_message: "",
      edit: false
    });
  }
  warranty_date = e => {
    e.preventDefault();
    selectedOption = e.target.value;
    this.setState({
      warranty_years: e.target.value
    });
  };
  machineType = e => {
    let internal = this.state.bill_no.replace( /^\D+/g, '');
    let device_type = e.target.value.substring(0, 2).toUpperCase()+"-"+internal
    this.setState({machine_type:e.target.value,bill_no:device_type})
  }
  remove_type = type => {
    let doc_type = '';
    if(type == "inventory_photo") {
      doc_type = "Photo"
    }
    else if (type == "inventory_warranty") {
      doc_type = "Warranty"
    }
    else if (type == "inventory_invoice") {
      doc_type = "Inovice"
    }
    let filteredItems = this.state.file_type.filter(item => item !== doc_type)
    this.setState({
      file_type:filteredItems
    })
  }
  toggleAddMachineDocument = () =>
    this.setState(prevState => ({
      openMachineDocument: !prevState.openMachineDocument
    }));

  toShowMobile = () => {
    this.setState({isMobile:!this.state.isMobile})
  }
  uploaed_pic = type => {
    let array = [...this.state.uploaed_pic]
     array.push(type)
    this.setState({
      uploaed_pic:array
    })
  }  

  render() {

    let uploaduccess = []
    uploaduccess = this.state.uploaed_pic.length > 0 && this.state.uploaed_pic.map((item) =>{
      return(
        <Alert color= "success"> {item} </Alert>
      ) 
    })
    let userList = this.props.usersList.users.map((val, i) => {
      return (
        <option key={val.id + val.username} id={i} value={val.user_Id}>
          {val.name}
        </option>
      );
    });
    let editData = this.props.manageDevice.editData;
    return (

      <div
        id="content"
        className="app-content box-shadow-z0 addInventoryContainer"
        role="main"
      >

        <AlertFader>
          {this.state.alertMessage || this.state.success ? (
            <Alert fade={false} color={this.state.error ? "danger" : "success"}>
              {this.state.alertMessage} {this.state.success}
            </Alert>
          ) : null}
        </AlertFader>
        {!this.state.openDownload ?
        <div className="addinventory">
          <AlertNotification message={this.state.msg} />
          <div className="col-xs-12 p-0">
            <div className="col-md-6 input-wrapper py-1">
              <label className="addInventory-label" htmlFor="machineName">
                Item Name
              </label>
              <Input
                fullWidth
                placeholder="Item Name" 
                onChange={e => this.setState({ machine_name: e.target.value })}
                onBlur={e =>
                  this.setState({
                    machine_name: this.state.machine_name.trim()
                  })
                }
                value={this.state.machine_name}
                required
              />
            </div>
            <div className="col-md-6 input-wrapper py-1">
              <label className="addInventory-label" htmlFor="M/DType">
                Machine/Device Type
              </label>

              <select
                className="form-control"
                ref="machine_type"
                value={this.state.machine_type}
                onChange={this.machineType}
              >
                <option value="" disabled>
                  --Select Device--
                </option>
                {this.state.deviceTypeList.map((val, i) => {
                  return (
                    <option key={i} value={val}>
                      {" "}
                      {val}
                    </option>
                  );
                })}
              </select>
            </div>
          </div>
          
          <div className={((this.props.isMobile < 600 === true ? (this.state.isMobile === false ? true : false):false) ? "d-none" : "col-md-12 px-0 ")}>
          <div className="col-xs-12 p-0 ">
            <div className="col-md-6 input-wrapper py-1">
              <label className="addInventory-label" htmlFor="dateOfWExpire">
                {editData.device && editData.device.warranty_end_date
                  ? "Warranty Expiry Date"
                  : "Warranty Period"}
              </label>
              {editData.device && editData.device.warranty_end_date ? (
                <Input
                  disabled
                  value={editData.device.warranty_end_date}
                  className="w-100 bg-white"
                />
              ) : (
                <select
                  value={this.state.warranty_years}
                  ref="warranty_period"
                  onChange={this.warranty_date}
                  className="form-control"
                  required
                >
                  <option value="0">None</option>
                  <option value="0.5">6 Months</option>
                  <option value="1">1 year</option>
                  <option value="2">2 year</option>
                  <option value="3">3 year</option>
                  <option value="5">5 year</option>
                </select>
              )}
            </div>
            <div className="col-md-6 input-wrapper py-1">
              {/* {this.state.warranty ? "Date Of Purchase" : ""} */}
              <label className="addInventory-label" htmlFor="dateOfPurchase">
                Date Of Purchase
              </label>
              <GenericDatePicker
                date={this.state.purchase_date ? this.state.purchase_date : null}
                emptyLabel="Date Of Purchase"
                handleDateChange={date => {
                  this.setState({ purchase_date: date });
                }}
                type={this.props.isMobile < 768 ? "absolute" : "inline"}
              />
            </div>
          </div>
          <div className="col-xs-12 p-0 ">
            <div className="col-md-6 input-wrapper py-1">
              <label className="addInventory-label" htmlFor="Status">
                Status
              </label>
              <select
                className="form-control"
                ref="status"
                value={this.state.status}
                onChange={e => this.setState({ status: e.target.value })}
                required
              >
                <option value="" disabled>
                  --Select Status--
                </option>
                {this.state.deviceStatusList.map((val, i) => {
                  return (
                    <option key={i} value={val.status}>
                      {" "}
                      {val.status}
                    </option>
                  );
                })}
              </select>
            </div>
            <div className="col-md-6 input-wrapper py-1">
              <label className="addInventory-label" htmlFor="IS.no">
                Internal Serial No
              </label>
              <Input
                placeholder="Internal Serial no "
                fullWidth
                onChange={e => this.setState({ bill_no: e.target.value })}
                onBlur={e => {
                  this.setState({ bill_no: this.state.bill_no });
                }}
                value={this.state.bill_no}
              />
            </div>
          </div>
         
          <div className="col-xs-12 p-0">
            {!editData.edit ? (
              <div className="col-md-6 input-wrapper py-1">
                <label className="addInventory-label" htmlFor="assignUser">
                  Assign User
                </label>
                <select
                  value={this.state.user_Id}
                  onChange={evt => {
                    this.setState({ user_Id: evt.target.value });
                  }}
                  className="form-control"
                  required
                >
                  <option value="unassign">Not Assigned To Anyone</option>
                  {userList}
                </select>
              </div>
            ) : null}
            <div className="col-md-6 input-wrapper py-1">
              <label className="addInventory-label" htmlFor="Price">
                Price
              </label>
              <Input
                placeholder="Price"
                fullWidth
                onChange={e => this.setState({ machine_price: e.target.value })}
                onBlur={e => {
                  this.setState({
                    machine_price: this.state.machine_price.trim()
                  });
                }}
                value={this.state.machine_price}
                required
              />
            </div>
          </div>
          <div className="col-xs-12 p-0 ">
          <div className="col-xs-12 p-0">
            <div className="col-md-6 input-wrapper py-1">
              <label className="addInventory-label" htmlFor="S.no">
                Serial No
              </label>
              <Input
                placeholder="Serial No"
                fullWidth
                onChange={e => this.setState({ serial_no: e.target.value })}
                onBlur={e => {
                  this.setState({ serial_no: this.state.serial_no.trim() });
                }}
                value={this.state.serial_no}
              />
            </div>
            {this.state.user_Id == "unassign" ? (
              <div className="col-md-6 input-wrapper py-1">
                <label className="addInventory-label" htmlFor="assignUser">
                  Comments
                </label>
                <Input
                  placeholder="Any Comments"
                  fullWidth
                  onChange={e =>
                    this.setState({ unassign_comment: e.target.value })
                  }
                  onBlur={e => {
                    this.setState({
                      unassign_comment: this.state.unassign_comment.trim()
                    });
                  }}
                  value={this.state.unassign_comment}
                />
              </div>
            ) : null}
            
          </div>
          </div>
        </div>
        {(this.props.isMobile < 600 ? (!this.state.isMobile?<span className="text-success font-weight-bold more_add_inventory" onClick ={this.toShowMobile}>More field </span>:<span className="text-success font-weight-bold more_add_inventory" onClick ={this.toShowMobile}>Less field </span>):'')}
            <div className="col-md-4 float-right text-right py-2">
              <Button
                color="primary"
                className="add-inven-btn"
                onClick={
                  this.state.loading ? null : e => this.handleAddDevice(e)
                }
              >
                {this.state.loading ? (
                  <Spinner color="light" className="inventoryAction" />
                ) : editData.edit ? (
                  "Update Inventory"
                ) : (
                  " Save Item"
                )}
              </Button>
            </div>
          
        </div> :

        <AddInventoryDocument
        onclose={this.onclose}
        file_type={this.state.file_type}
        showSuccessMessage={this.showSuccessMessage}
        remove_type={this.remove_type}
        uploaed_pic={this.uploaed_pic}
        machineId={
          this.props.manageDevice.addedMachine.data &&
          this.props.manageDevice.addedMachine.data.inventory_id
        }
      />
       }

        {/* <Dialog
          open={this.state.openMachineDocument}
          onClose={this.toggleAddMachineDocument}
          title="Add New Inventory Doucment"
          modal={false}
        >
          <AddInventoryDocument
            machineId={
              this.props.manageDevice.addedMachine.data &&
              this.props.manageDevice.addedMachine.data.inventory_id
            }
          />
        </Dialog> */}
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    frontend: state.frontend.toJS(),
    usersList: state.usersList.toJS(),
    manageUsers: state.manageUsers.toJS(),
    loggedUser: state.logged_user.userLogin,
    manageDevice: state.manageDevice.toJS()
  };
}
const mapDispatchToProps = dispatch => {
  return {
    onUsersList: () => {
      return dispatch(actionsUsersList.get_users_list());
    },
    onUserProfileDetails: (userid, username) => {
      return dispatch(
        actionsManageUsers.getUserProfileDetails(userid, username)
      );
    },
    onAddNewMachine: newMachineDetails => {
      return dispatch(actionsManageDevice.addNewMachine(newMachineDetails));
    },
    onFetchDevice: () => {
      return dispatch(actionsManageDevice.get_machines_detail());
    },
    onGetDeviceById: id => {
      return dispatch(actionsManageDevice.getDeviceById(id));
    },
    onUpdateDevice: (id, machineData) => {
      return dispatch(actionsManageDevice.updateDevice(id, machineData));
    },
    onFetchDeviceType: () => {
      return dispatch(actionsManageDevice.getDeviceType());
    },
    onFetchDeviceStatus: () => {
      return dispatch(actionsManageDevice.getDeviceStatus());
    },
    onShowTab: () => {
      return dispatch(actionsManageDevice.showTab());
    },
    onCallDeviceType: deviceList => {
      return dispatch(actionsManageDevice.assignDeviceType(deviceList));
    },
    onDeleteDeviceStatus: checkValue => {
      return dispatch(actionsManageDevice.deleteDeviceStatus(checkValue));
    },
    showHeading: data => {
      return dispatch(actions.showHeading(data));
    }
  };
};

export default compose(
  connect(
    mapStateToProps,
    mapDispatchToProps
  ),
  isMobile,
  withRouter
)(FormAddNewInventory)
