import React from "react";
import Dialog from "material-ui/Dialog";
import PropTypes from "prop-types";
import { Button } from "reactstrap";

export default class UnassignDevice extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      comment: "",
      inventory_id: ""
    };
    this.baseState = this.state;
  }

  handleAddUserComment = () => {
    let { inventory_id, comment } = this.state;
    this.props.callAddUserComment({ inventory_id, comment });
    this.setState({
      comment: ""
    });
  };
  handleChange = e => {
    this.setState({
      comment: e.target.value,
      inventory_id: this.props.device.id
    });
  };

  resetForm = () => {
    this.setState(this.baseState);
    this.props.handleClose();
  };

  render() {
    return (
      <div>
        <Dialog
          title="Unassign Device"
          titleStyle={{ opacity: "0.56" }}
          modal={false}
          open={this.props.open}
          onRequestClose={this.resetForm}
          contentStyle={{ width: "70%", maxWidth: "none" }}
          autoScrollBodyContent
          className="unassign-device"
        >
          <div className="col-md-12 abcd">
            <div className="row -py-sm">
              <div className="col-sm-3">
                <p style={{ opacity: "0.56" }}>Device Name:</p>
              </div>

              <div className="col-sm-2">
                <p>{this.props.device.machine_name}</p>
              </div>

              <div className="col-sm-3">
                <p style={{ opacity: "0.56" }}>Device Type:</p>
              </div>

              <div className="col-sm-4">
                <p>{this.props.device.machine_type}</p>
              </div>
            </div>

            <div className="row p-y-sm">
              <div className="col-sm-3">
                <p style={{ opacity: "0.56" }}>Assign Date:</p>
              </div>

              <div className="col-sm-2">
                <p>{this.props.device.assign_date}</p>
              </div>

              <div className="col-sm-3">
                <p style={{ opacity: "0.56" }}>Serial Number:</p>
              </div>

              <div className="col-sm-4">
                <p>{this.props.device.serial_number}</p>
              </div>
            </div>

            <div className="row p-y-sm">
              <div className="col-md-12" style={{ opacity: "0.56" }}>
                {"Comment:"}
                <textarea
                  value={this.state.comment}
                  style={{ width: "100%" }}
                  onChange={this.handleChange}
                />
              </div>
            </div>

            <Button
              color="primary"
              onClick={() => {
                this.handleAddUserComment();
                this.resetForm();
              }}
              style={{ opacity: "0.76", marginTop: "2%" }}
            >
              Unassign Device
            </Button>
          </div>
        </Dialog>
      </div>
    );
  }
}

UnassignDevice.propTypes = {
  callAddUserComment: PropTypes.func.isRequired
};
