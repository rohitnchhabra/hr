import React from "react";
import _ from "lodash";
import { notify} from "../../../services/notify";
import { getLowerCase, getLoggedUser } from "../../../services/generic";
import AddDeviceStatus from "../../../modules/inventory/components/AddDeviceStatus";
import { CONFIG } from "../../../config";
import { CSVLink } from "react-csv";
import { deviceKeys } from "../../../services/index";
import {
  Alert,Button
} from "reactstrap";
import map from "lodash/map";
import moment from "moment";
import * as a5 from "../../../static/avatar.png";
import DeviceDetailSection from "./DeviceDetailSection";
import cloneDeep from "lodash/cloneDeep";
import isEqual from "lodash/isEqual";
import { compose } from "redux";
import isMobile from "../../../components/hoc/WindowResize";

let devices;
class InventoryList extends React.Component {  
  constructor(props) {
    super(props);
    this.state = {
      edit: true,
      open: false,
      openStatus: false,
      id: "",
      openSnackbar: false,
      user: "",
      status_message: "",
      search: this.props.match.params.device,
      deviceTypeList: [],
      deviceStatusList: [],
      device_status: "",
      deviceList: [],
      deviceVal: "",
      unapprovedList: [],
      approveDialog: false,
      headerData: [],
      machineDetail: [],
      tab: "tab-general",
      comment: "",
      inventory_id: "",
      user_id: "",
      addComment: false,
      editContent: [],
      document_type: "",
      file: [],
      compressedFile: null
    };
    this.selected = false;
    this.addComment = this.addComment.bind(this);
    this.handleAssign = this.handleAssign.bind(this);
    this.handleDeviceTypeFilter = this.handleDeviceTypeFilter.bind(this);
    this.handleStatusTypeFilter = this.handleStatusTypeFilter.bind(this);
  }
  componentWillMount() {
    // this.props.onFetchDeviceType().then(val => {
    //   this.setState({
    //     deviceTypeList: val,
    //     deviceStatusList: val,
    //     search: this.props.match.params.device
    //   });
    // });

    this.setState({
      deviceTypeList: this.props.deviceList,
      deviceStatusList: this.props.deviceList,
      search: this.props.match.params.device
    });
    this.props.onFetchDeviceStatus().then(val => {
      this.setState({ deviceStatusList: val });
    });
  }

  componentDidMount() {
    this.handleDeviceTypeFilter(this.props.match.params.device);
  }

  handleHeaderData(deviceList) {
    let headerData = [];
    let headerLabel = "";
    if (deviceList && deviceList.length >= 1) {
      let headerObject = deviceKeys;
      _.map(headerObject, (keys, k) => {
        let label = keys.split("_").join(" ");
        headerLabel = label.charAt(0).toUpperCase() + label.slice(1);
        let header = {
          label: headerLabel,
          key: keys
        };
        headerData.push(header);
      });
      return headerData;
    }
  }

  componentDidUpdate(prevProps, prevState) {
    if (
      this.props.manageDevice.inventoryDeleteStatus.error !=
      prevProps.manageDevice.inventoryDeleteStatus.error
    ) {
      if (this.props.manageDevice.inventoryDeleteStatus.error == 1) {
        notify(
          "Warning !",
          this.props.manageDevice.inventoryDeleteStatus.message,
          "warning"
        );
      }
      if (this.props.manageDevice.inventoryDeleteStatus.error == 0) {
        notify(
          "Success !",
          this.props.manageDevice.inventoryDeleteStatus.message,
          "success"
        );
      }
    }
    if (
      !_.isEqual(prevProps.manageDevice.device, this.props.manageDevice.device)
    ) {
      this.handleDeviceTypeFilter(this.props.match.params.device);
    }
    if (
      !_.isEqual(
        prevProps.manageDevice.unapprovedList,
        this.props.manageDevice.unapprovedList
      )
    ) {
      this.setState({ unapprovedList: this.props.manageDevice.unapprovedList });
    }
    if (
      prevState.deviceList.length == 0 &&
      this.state.deviceList.length > 1 &&
      this.props.location.state
    ) {
      this.selectCard(this.props.location.state.id);
    }
  }

  openEditDevice = device => {
    this.setState({
      edit: true,
      open: false
    });
    this.props.editAction(device, this.state.edit, this.state.open);
    this.props.history.push("/app/addInventory");
  };
  callAddStatus = (statusValue, colorValue) => {
    this.props.onCallDeviceStatus(statusValue, colorValue).then(res => {
      if (res.error == 0) {
        this.setState({
          statusType: "",
          background: "",
          checkValue: ""
        });
        // notify("", res.message, "");
        this.props.onFetchDeviceStatus();
      } else {
        notify("Error !", res.message, "error");
      }
    });
  };

  callAddDevice = deviceType => {
    this.props.onCallDeviceType(deviceType).then(
      message => {
        this.setState({
          status_message: message
        });
        this.props.onFetchDeviceType();
      },
      error => {
        notify("Error !", error, "error");
      }
    );
  };
  handleStatusClose = () => {
    this.setState({
      openStatus: false,
      statusType: "",
      background: ""
    });
  };
  handleStatusOpen = () => {
    this.setState({
      openStatus: true
    });
  };

  handleOpen = e => {
    e.stopPropagation();
    this.setState({
      open: true
    });
  };
  handleClose = () => {
    this.setState({
      open: false
    });
  };

  handleAssign(id, userId) {
    this.setState({ user: userId });
    this.props.callAssign(id, userId);
  }

  callDeleteDeviceStatus = (checkValue, new_status) => {
    this.props.onDeleteDeviceStatus(checkValue, new_status).then(val => {
      if (val.error === 1) {
        this.setState({
          statusType: "",
          background: "",
          checkValue: ""
        });
        notify("Error !", "This Device Status Type Is In Use", "error");
        this.handleStatusClose();
      } else if (val.message) {
        this.setState({
          status_message: val.message
        });
        notify("", this.state.status_message, "info");
        this.handleStatusClose();
      } else {
        this.setState({
          statusType: "",
          background: "",
          checkValue: ""
        });
        this.handleStatusClose();
      }
    });
    this.props.onFetchDeviceStatus();
  };

  async handleDeviceTypeFilter(deviceType) {
    if (
      this.state.deviceTypeList.length ===
      this.props.manageDevice.deviceList.length
    ) {
      let devices = this.props.manageDevice.device;

      if (this.state.device_status !== "") {
        devices = this.state.deviceList;
      }
      if (deviceType !== "") {
        devices = _.filter(
          devices,
          row => getLowerCase(row.machine_type) === getLowerCase(deviceType)
        );
      } else {
        if (this.state.device_status !== "") {
          devices = _.filter(
            this.props.manageDevice.device,
            row =>
              getLowerCase(row.status) ===
              getLowerCase(this.state.device_status)
          );
        }
      }
      if (this.state.device_status !== "" && deviceType !== "") {
        devices = _.filter(
          this.props.manageDevice.device,
          row =>
            getLowerCase(row.machine_type) === getLowerCase(deviceType) &&
            getLowerCase(row.status) === getLowerCase(this.state.device_status)
        );
      }
      let header = this.handleHeaderData(devices);

      this.setState(
        {
          deviceList: devices,
          search: deviceType,
          headerData: header,
          machineDetail: devices.length ? [devices[0]] : [],
          backupMachineDetail: devices.length ? [devices[0]] : [],
          user_id:
            devices.length && devices[0].user_Id ? devices[0].user_Id : "",
          inventory_id: devices.length ? devices[0].id : ""
        },
        () => {
          if (this.state.machineId) {
            this.selectCard(this.state.machineId);
          }
        }
      );
    }
  }

  async handleStatusTypeFilter(statusType) {
    let status = this.props.manageDevice.device;
    if (this.state.search !== "") {
      status = this.state.deviceList;
    }
    if (statusType !== "") {
      status = _.filter(
        status,
        row => getLowerCase(row.status) === getLowerCase(statusType)
      );
    } else {
      if (this.state.search !== "") {
        status = _.filter(
          this.props.manageDevice.device,
          row =>
            getLowerCase(row.machine_type) === getLowerCase(this.state.search)
        );
      }
    }
    if (statusType !== "" && this.state.search !== "") {
      status = _.filter(
        this.props.manageDevice.device,
        row =>
          getLowerCase(row.machine_type) === getLowerCase(this.state.search) &&
          getLowerCase(row.status) === getLowerCase(statusType)
      );
    }
    if(statusType == "User_Not_Assign") {
      status = _.filter(this.state.deviceList,row=>row.user_Id == null)
    }

    let header = this.handleHeaderData(status);
    this.setState({
      deviceList: status,
      device_status: statusType,
      headerData: header,
      machineDetail: status.length ? [status[0]] : [],
      backupMachineDetail: status.length ? [status[0]] : [],
      user_id: devices.length && devices[0].user_Id ? devices[0].user_Id : "",
      inventory_id: devices.length ? devices[0].id : ""
    });
  }

  capitalize = string => {
    return (
      string && string.charAt(0).toUpperCase() + string.slice(1).toLowerCase()
    );
  };

  selectCard = id => {
    if (this.state.deviceList.length > 1) {
      let devices = this.state.deviceList;
      let cardShown = devices.filter(item => {
        return item.id === id;
      });
      this.setState({
        machineDetail: [...cardShown],
        machineId: id,
        user_id: cardShown[0].user_Id ? cardShown[0].user_Id : "",
        backupMachineDetail: [...cardShown],
        inventory_id: id
      });
    }
  };

  sendUnapprovedId = id => {
    this.setState({ id: id });
    this.props.callUnapprovedId({ id });
  };

  toggleTab = tabName => {
    this.setState({
      tab: tabName
    });
  };

  AssignDevice = (assign_device, device_id) => {
    this.device_id = device_id;
    this.props.onAssignDevice(assign_device).then(
      data => {
        this.props.onFetchDevice();
        this.setState({
          user_id: "",
          inventory_id: "",
          machineId: "",
          editContent: []
        });
        this.selectCard(device_id)
      },
      error => {
        notify("Error!", error, "error");
      }
    );
  };

  onUploadTypeChange = document => {
    this.setState({ document_type: document, file: [],imageUrl:'' });
  };

  onEditChange = (name, value,machine) => {
    if(machine && machine.name && name==="status" && value.toLowerCase()==="sold"){
       this.setState({
         notSelectError:true
       })
       return 
    }
    let cardShown = this.state.deviceList.filter(item => {
      return item.id === this.state.machineDetail[0].id;
    });
    let deviceDetail = cloneDeep(this.state.machineDetail[0]);
    deviceDetail[name] = value;
    this.setState({
      machineDetail: [deviceDetail],
      backupMachineDetail: cardShown,
      notSelectError:false
    });
  };

  onDrop = files => {
    this.setState({
      file: files,
      type:
        files[0].name &&
        files[0].name.split(".")[files[0].name.split(".").length - 1]
    });
    const reader = new FileReader();

    reader.onloadend = () => {
      this.setState({
        imageUrl: reader.result
      });
    };
    if (files) {
      reader.readAsDataURL(files[0]);
      this.setState({
        imageUrl: reader.result
      });
    } else {
      this.setState({
        imageUrl: ""
      });
    }
  };

  onEditClick = (edit,isEdit) => {    
    let ec = this.state.editContent;
    if(!isEdit){
    ec=[]
    }else{
      ec.push(...edit);
    }
    this.setState({ editContent: ec });
  };

  onEditCancel = () => {
    let deviceDetail = cloneDeep(this.state.backupMachineDetail);
    this.setState({ machineDetail: deviceDetail, editContent: [] });
  };

  onEditSubmit = () => {
    this.setState({
      loading: true
    });
    let apiData = {
      machine_type: this.state.machineDetail[0].machine_type,
      machine_name: this.state.machineDetail[0].machine_name.trim(),
      machine_price: this.state.machineDetail[0].machine_price.trim(),
      serial_no: this.state.machineDetail[0].serial_number.trim(),
      purchase_date: moment(this.state.machineDetail[0].purchase_date).format(),
      operating_system: this.state.machineDetail[0].operating_system,
      status: this.state.machineDetail[0].status,
      comment: this.state.machineDetail[0].comments.trim(),
      warranty: moment(this.state.machineDetail[0].warranty_end_date).format(),
      warranty_comment: this.state.machineDetail[0].warranty_comment.trim(),
      repair_comment: this.state.machineDetail[0].repair_comment.trim(),
      bill_no: this.state.machineDetail[0].bill_number.trim(),
      user_Id: this.state.machineDetail[0].user_Id,
      unassign_comment: this.state.machineDetail[0].unassign_comment,
      warranty_years:
        moment(this.state.machineDetail[0].warranty_end_date).format() -
        moment(this.state.machineDetail[0].purchase_date).format()
    };
    this.props
      .onUpdateDevice(this.state.machineDetail[0].id, apiData)
      .then(message => {
        this.setState({
          loading: false,
          error: false,
          alertMessage: message,
          editContent: []
        });
        this.props.onFetchDevice();
        this.setState({
          alertMessage: ""
        });
      })
      .catch(message => {
        this.setState({
          msg: message,
          loading: false,
          error: true,
          alertMessage: message
        });
        this.setState({
          alertMessage: ""
        });
      });
  };

  callUpdateDocuments = e => {
    let type = this.state.document_type;
    let stop = false;
    if (type === "") {
      stop = true;
      notify("Warning!", "Please select document type.", "warning");
    } else if (!this.state.imageUrl) {
      stop = true;
      notify("Warning!", "Please select a file", "warning");
    }
    if (stop) {
      e.preventDefault();
    }
  };

  handleAddComment = (add_inventory_comment, device_id) => {
    this.props.onAddInventoryComment(add_inventory_comment).then(
      data => {
        this.props.onFetchDevice();
      },
      error => {
        notify("Error!", error, "error");
      }
    );
    this.setState({
      comment: "",
      addComment: false
    });
  };

  addComment = () => {
    this.setState({
      addComment: true
    });
  };

  successfulFileUpload = () => {
    this.props.onFetchDevice();
    this.setState({
      imageUrl: "",
      file: [],
      document_type: ""
    });
  };

  onFileCompression = (compressedFile = null) => {
    this.setState({ compressedFile });
  };

  handleRomoveImage=()=>{
    this.setState({ imageUrl: "",document_type:"",file:[]})
  }

  render() {
    let working = this.state.deviceList.filter(item => (item.status == "Working") || (item.status == "working"));
    let non_working = this.state.deviceList.filter(item => (item.status == "Not Working"));
    let userList;
    if (this.props.usersList.users) {
      userList = [
        { user_Id: "", username: "Unassign Device" },
        ...this.props.usersList.users
      ];
    }
    const userName = map(userList, (val, i) => {
      return (
        <option key={i} value={val.user_Id}>
          {val.username}
        </option>
      );
    });
    let fileNames = this.props.match.params.device;
    // if (this.props.searchVal.length >= 1) {
    //   fileNames = this.props.searchVal.toLowerCase();
    // }
    let isSubmit = !isEqual(
      this.state.machineDetail && this.state.machineDetail[0],
      this.state.backupMachineDetail && this.state.backupMachineDetail[0]
    );
    let path = CONFIG.inventory_images;

    const role = getLoggedUser().data.role;
    var statusList = this.state.deviceStatusList || [];
    let statusDrop = statusList.map((val, i) => {
      return (
        <option value={val.status} key={i}>
          {val.status}
        </option>
      );
    });

    let listDropMap = this.state.deviceTypeList.map((val, i) => {
      return (
        <option value={val} key={i}>
          {val}
        </option>
      );
    });
    devices =
      this.props.fourthArrow === "show"
        ? this.state.unapprovedList
        : this.state.deviceList;
    const history =
      this.state.machineDetail.length &&
      this.state.machineDetail[0].history &&
      map(this.state.machineDetail[0].history, (val, i) => (
        <li key={val.updated_at + i}>
          <span className={`thumb-xs pull-left mr-sm`}>
            <img className="rounded-circle" src={a5} alt="..." />
          </span>
          <div>
            <h6 className={`commentUser fs-sm fw-semi-bold`}>
              {val.updated_by_user}
              <br />
              <small>
                {moment(val.updated_at).format("Do MMM YY, h:mm a")}
              </small>
            </h6>
            <p>{val.assign_unassign_user_name? val.comment=="Inventory Assigned"?val.comment + " to " + val.assign_unassign_user_name:val.comment + " from " + val.assign_unassign_user_name :val.comment}</p>
          </div>
        </li>
      ));
    return (
      
      <>
        {this.state.notSelectError &&
          <Alert color="danger">You need to unassign this inventory before setting its status to Sold</Alert>

        }
      <div className="d-sm-flex justify-content-between align-items-center pb-2">
        <div className="pb-2 d-flex text-sm">
          <div className="pr-2"><span>Total</span> <strong> {this.state.deviceList.length} </strong></div>
          <div className="px-2"><span>Working Device</span> <strong> {working.length} </strong></div>
          <div className="px-2"><span>Non-Working Device</span> <strong> {non_working.length} </strong></div>
          <div className="px-2"><span>Unassigned Device</span> <strong> {_.filter(this.state.deviceList,row=>row.user_Id == null).length} </strong></div>
        </div>
        <div>
          <Button color="success" onClick={this.props.addDevice}>
            Add New Inventory
          </Button>
        </div>
      </div>
        <div
          className={`d-flex ${
            this.props.fourthArrow === "hidden"
              ? "justify-content-between"
              : "justify-content-end"
          } flex-wrap flex-lg-nowrap`}
        >
          {this.props.fourthArrow === "hidden" ? (
            <div className="d-flex flex-lg-row flex-column flex-wrap flex-md-nowrap">
              <div className="form-group selectDevice d-flex">
                <div className="mr-2 align-self-end">
                  <label>Status</label>
                </div>
                <div className="inventory-box">
                  <select
                    className="form-control"
                    ref="device_status"
                    value={this.state.device_status}
                    onChange={e => {
                      this.handleStatusTypeFilter(e.target.value);
                    }}
                  >
                    <option value="">All</option>
                    {statusDrop}
                    <option value="User_Not_Assign">Unassigned</option>
                  </select>
                </div>
              </div>
            </div>
          ) : null}
          <div className="report">
            <div className="buttonbox d-sm-flex">
              <div className="d-flex align-items-center pb-sm-0 pb-2">
              <div
                onClick={e => this.props.handleDeleteSoldDevice(e, this.props.match.params.device)}
                className="delete-sold-inventory pr-3"
                >
                  Delete All Sold{" "}
                  {this.props.match.params.device}
                </div>
              <div className="pr-3">
                <CSVLink
                  className="linkInactive"
                  data={this.state.deviceList}
                  headers={this.state.headerData}
                  filename={`inventory-${fileNames}-report-${moment().format(
                    "YYYY-MMMM-DD"
                  )}.csv`}
                >
                  Download Report
                </CSVLink>
              </div>
              </div>
              <>
                <AddDeviceStatus
                  callAddStatus={this.callAddStatus}
                  handleStatusClose={this.handleStatusClose}
                  handleStatusOpen={this.handleStatusOpen}
                  open={this.state.openStatus}
                  callDeleteDeviceStatus={this.callDeleteDeviceStatus}
                  deviceStatusList={this.state.deviceStatusList}
                  {...this.props}
                />
              </>
              {/* <div>
                <AddDeviceDialoge
                  callAddDevice={this.callAddDevice}
                  handleClose={this.handleClose}
                  handleOpen={this.handleOpen}
                  open={this.state.open}
                  deviceTypeList={this.state.deviceTypeList}
                  {...this.props}
                />
              </div> */}
            </div>
          </div>
        </div>
        <div className="parent-inventory-list">
          <DeviceDetailSection 
            state={this.state}
            selectCard={id => {
              this.selectCard(id);
            }}
            openEditDevice={device => this.openEditDevice(device)}
            deleteDevices={(data,e) => {
              this.setState({ machineId: "" });
              this.props.deleteDevices(data);
            }}
            onUserChange={(value, inv_id) => {
              this.setState({
                user_id: value,
                inventory_id: inv_id
              });
            }}
            AssignDevice={(assign_device, device_id) => {
              this.AssignDevice(assign_device, device_id);
            }}
            onCommentChange={(comment, inventory_id) => {
              this.setState({
                comment: comment,
                inventory_id: inventory_id
              });
            }}
            handleAddComment={(add_inventory_comment, device_id) => {
              this.handleAddComment(add_inventory_comment, device_id);
            }}
            addComment={() => {
              this.addComment();
            }}
            history={history}
            path={path}
            devices={devices}
            userName={userName}
            isApprove={false}
            deviceStatuses={this.props.manageDevice.statusList}
            deviceTypes={this.props.manageDevice.deviceList}
            onEditChange={this.onEditChange}
            onEditClick={this.onEditClick}
            onEditCancel={this.onEditCancel}
            isSubmit={isSubmit}
            onEditSubmit={this.onEditSubmit}
            CONFIG={CONFIG}
            handleDialogOpen={() => {
              this.setState({ open: true });
            }}
            handleDialogClose={() => {
              this.setState({ open: false });
            }}
            onFileDrop={this.onDrop}
            onFileCompression={this.onFileCompression}
            onSuccessfulFileUpload={this.successfulFileUpload}
            onUploadTypeChange={this.onUploadTypeChange}
            validateDocuments={this.callUpdateDocuments}
            isMobile={this.props.isMobile}
            isGetData={this.props.isGetData}
            handleRomoveImage={this.handleRomoveImage}
          />
        </div>
      </>
    );
  }
}

export default compose(isMobile)(InventoryList);
