import React from "react";
import classnames from "classnames";
import GenricCardList from "../../../components/generic/GenricCardList";

export default function AuditInventoryEmployeeWise({
  audit_list_employee_wise: audit_list,
  machineType,
  count,
  setSelected,
  employee_wise_list,
  history,
}) {
  let details = () => {
    let firstItem = ''
    return (
      <div className="col-md-9 px-0">
        <div
          className={classnames(
            "overflow-auto px-0",
            "bg-white",
            "ml-1"
          )}
        >
          {employee_wise_list.map((v, i) => {
              if(!firstItem){
                firstItem = v.machine_type
              }
                if(v.machine_type === machineType || (!machineType && firstItem == v.machine_type)){
                return (
                  <div className="px-4">
                    <div className="machineDetailInfo p-t">
                      <h4 className="darkGrey">
                        <span
                            onClick={()=>history.push({
                                pathname: `/app/inventoryOverviewDetail/${v.machine_type}`,
                                state: { id: v.id }
                              })}
                            className="inventory-overview-details-a"
                            >
                            {v.machine_name} ({v.id})
                        </span>
                      </h4>
                      <h6 className="lightGrey">{v.machine_type}</h6>
                      <div className="d-flex">
                        <h6 className="lightGrey">Internal Serial No :</h6>
                        <h6 className="darkGrey">{v.bill_number}</h6>
                      </div>
                    </div>
                    <div className="machineDetailInfo">
                      {v.audit_done_by_user_id ? (
                        <div>
                          <div>
                            <b>
                              <i>
                                Commented by: {v.audit_done_by} (
                                {v.audit_done_by_user_id})
                              </i>
                            </b>
                          </div>
                          {v.comment ? v.comment :v.comment_type}
                        </div>
                      ) : (
                        "Audit pending"
                      )}
                    </div>
                  </div>
                )}
              })
            }
        </div>
      </div>
    );
  };

  return (
    <>
      <div className="assign-member scroll col-md-3 p-0">
        {audit_list
          ? audit_list.map((item, index) => {
              return (
                <>
                  <GenricCardList
                    onCardSelected={() => {
                      setSelected(item.machine_type);
                    }}
                    cardBodyClassName={(item.machine_type === machineType || (!machineType && index == 0 )) ? "status-pending-card" : ""}
                    cardClassname={"border-0 m-b pointer"}
                    heading={item.machine_type}
                    subHeading={`${"Total: " + count[item.machine_type]}`}
                  />
                  {window.screen.availWidth <= 768 
                    ? details()
                    : null}
                </>
              );
            })
          : null}
      </div>
      {window.screen.availWidth >= 768 ? details() : null}
    </>
  );
}
