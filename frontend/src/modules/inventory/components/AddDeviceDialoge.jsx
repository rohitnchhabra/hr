import React from 'react';
import Dialog from 'material-ui/Dialog';
import _ from 'lodash';
import {notify, confirm} from '../../../services/notify';
import FlatButton from 'material-ui/FlatButton';
// import style from '../../../styles/inventory/viewUser.scss'
import { Button } from 'reactstrap';
import Task from "../../../components/Task/Task";

export default class AddDeviceDialoge extends React.Component {
  constructor (props) {
    super(props);
    this.state = {
      background: '',
      deviceType: '',
      open:       false,
      deviceList: [],
      checkValue: []
    };
  }
  componentWillReceiveProps (props) {
    this.setState({deviceList: props.deviceTypeList, open: props.open});
  }

  handleDelete = () => {
    let checkValue = this.state.checkValue;
    let deviceList = this.state.deviceList;
    checkValue.map((val) => {
      _.pullAt(deviceList, val);
    });
    this.props.onCallDeviceType(deviceList).then((val) => {
      if (val.data.not_delete) {
        this.setState({deviceList: this.state.deviceList, checkValue: []});
        notify('Oops', 'This Device Type Is In Use', 'error');
        this.props.handleClose();
      } else {
        this.setState({deviceList, deviceType: '', checkValue: []});
        notify('Success', 'Device Type Deleted', 'success');
        this.props.handleClose();
      }
    });
  }

  setValue = e => {
    if (!this.state.checkValue.find((item)=> item == e)) {
      let array = this.state.checkValue;
      array.push(e);
      this.setState({checkValue: array});
    } else {
      let array = this.state.checkValue;
      _.pull(array, e);
      this.setState({
        checkValue: array
      });
    }
  }

  addMoreDevice = () => {
    var deviceType = this.state.deviceType.trim();
    if (!_.isEmpty(deviceType)) {
      var deviceList = this.state.deviceList;
      let arr = _.filter(deviceList, device => device.toLowerCase() === deviceType.trim().toLowerCase());
      if (arr.length > 0) {
        notify('Oops', 'This Device Type Already In Use', 'error');
        this.setState({
          deviceType: ''
        });
      } else {
        deviceList.push(deviceType);
        this.setState({
          deviceType: '',
          deviceList: deviceList
        });
      }
      this.props.callAddDevice(this.state.deviceList);
    }
  }

  render () {
    const actions = [
      <FlatButton label="Delete" secondary style={{marginRight: 5}} onClick={() => {
        if (this.state.checkValue !== '') {
          confirm('Are you sure ?', 'Do you want to delete this Device Type ?', 'warning').then((res) => {
            if (res) {
              this.handleDelete();
            }
          });
        }
      }} />,
      <FlatButton label="Cancel" primary onClick={this.props.handleClose} style={{marginRight: 5}} />,
    ];
    return (
      <div className="pl-2">
        <Button color="success" onClick={this.props.handleOpen}>Add Device Type</Button>
        <Dialog
          title={'ADD DEVICE TYPE'}
          titleStyle={{opacity: '0.56'}}
          actions={actions}
          modal={false}
          open={this.state.open}
          onRequestClose={this.props.handleClose}
          autoScrollBodyContent
        >
          <div className="row m-0">
            <div className="col-sm-12">
              <label>Device Type List</label>
              <ol>
                {this.state.deviceList.map((val, i) => {
                  return (
                    <li
                      key={i}
                      className="d-flex status-list justify-content-between"
                  >
                    <div className="col-sm-3 ml-1 p-0 d-flex">
                        <Task
                          className="manage_role_size"
                          text={""}
                          index={i}
                          id={i}
                          status={this.state.checkValue.find((item)=> item == i)}
                          handleChangeSteps={() => {
                              this.setValue(i);
                          }}
                        />
                        <div
                          className="d-flex align-items-center"
                        >
                          <span className="pl-2">{val}</span>
                        </div>
                      </div>
                  </li> )}
                  )}
                  </ol>
                  </div>
          <div className="row m-0 w-100">
              <label className="col-sm-2 col-form-label">Add Type</label>
              <div className="col-sm-7">
                <input
                  type="text"
                  className="form-control w-100"
                  placeholder="Add Type"
                  value={this.state.deviceType}
                  onBlur={e => {
                    this.setState({
                      deviceType: this.state.deviceType.trim(),
                      colorPicker: "hide"
                    });
                  }}
                  onChange={e => {
                    this.setState({
                      colorPicker: "show",
                      deviceType: e.target.value
                    });
                  }}
                />
              </div>
              <div className="col-sm-3">
                <Button
                  color="primary"
                  disabled={this.state.deviceType === ""}
                  className="w-100"
                  onClick={this.addMoreDevice}
                >
                  Add Type
                </Button>
              </div>
          </div>
          </div> 
        </Dialog>
      </div>
    );
  }
}
