import React from 'react';
import _ from 'lodash';
import RichTextEditor from 'react-rte';
import { Link } from "react-router-dom";
import {Alert,Label} from 'reactstrap';
// import update from 'react/lib/update';
import {confirm} from '../../../services/notify';
import FlatButton from 'material-ui/FlatButton';
import RaisedButton from 'material-ui/RaisedButton';
import AlertFader from '../../../components/generic/AlertFader';
import {Button} from "reactstrap";
// import EditableDiv from '../../../components/editor/EditableDiv';
import $ from 'jquery';
import Heading from '../../../components/generic/Heading';
import Checkbox from "../../../components/generic/input/Checkbox";
import MailTemplate from './MailTemplate';
import * as a5 from "../../../static/avatar.png";
import TemplateTab from "./TemplateTab"
var FormData = require('form-data');
var moment = require('moment');




class Variables extends React.Component {
  constructor (props) {
    super(props);
    this.state = {
      usersList:            [],
      openDialog:           false,
      templateId:           '',
      templateName:         '',
      templateSubject:      '',
      templateBody:         RichTextEditor.createEmptyValue(),
      errName:              '',
      errSubject:           '',
      pValue:               [],
      openSendMailDialog:   false,
      recipient:            [],
      cc:                   [],
      bcc:                  [],
      recipientType:        'Recipient',
      openVarDialog:        false,
      openPreview:          false,
      sentMail:             {},
      recipientEmailId:     '',
      recipientNotFound:    false,
      emailValidationError: '',
      upload_file:          [],
      uploadedPDF:          [],
      LinearProgressBar:    [],
      header:               '',
      templateVariables:    [],
      templateVariablesAfterClear: [],
      tmpKey:'',
      status_msg:"",
      toggle_pdf:false,
      toggle_attachment:false,
      selectedTab:"email"
    };

    this.openCreateTemplate = this.openCreateTemplate.bind(this);
    this.handleCloseDialog = this.handleCloseDialog.bind(this);
    this.handleCloseDialog1 = this.handleCloseDialog1.bind(this);
    this.saveTemplate = this.saveTemplate.bind(this);
    this.deleteTemplate = this.deleteTemplate.bind(this);
    this.forwardTemplate = this.forwardTemplate.bind(this);
    this.onClickLabel = this.onClickLabel.bind(this);
    this.handleContentChange = this.handleContentChange.bind(this);
    this.handleClose = this.handleClose.bind(this);
    this.closeMailPreview = this.closeMailPreview.bind(this);
    this.submitEmail = this.submitEmail.bind(this);
    this.hideError = this.hideError.bind(this);
    this.variables = [];
  }
  componentWillReceiveProps (props) {
    if (!props.loggedUser.isLoggedIn) {
      this.props.history.push('/logout');
    } 
    this.setState({usersList: props.employee});
  }
  
  componentDidMount(){
    this.props.showHeading("Mail Templates")
  }

  onHandleChange=(val)=>{
    this.setState({
      templateSubject:val
    })
  }

  toggleEdit=()=>{
    this.setState({
      openDialog:false
    })
  }

  onTogglePdf=(e)=>{
    this.setState({
      toggle_pdf:!e
    })
  }

  onToggleAttachment=(e)=>{
    this.setState({
      toggle_attachment:!e
    })
  }

  saveTemplate=(template,file)=>{
    if (_.isEmpty(template.message_subject)) {
          this.setState({errSubject: 'Subject empty'});
        }
    else if(_.isEmpty(template.message)){
      this.setState({errSubject: 'Message empty'});
    }
    else{
      const formData = new FormData();
      if(file[0] && file[0].type){
      formData.append("attachment_file", file[0]);
      }
      formData.append("for", template.for);
      formData.append("doc_type", template.Doc_type);
      formData.append("message_subject", template.message_subject);
      formData.append("message_key", template.message_key);
      formData.append("subject", template.subject);
      formData.append("message", template.message.toString("html"));
      this.props.onSaveTemplate(formData).then(res=>{
        this.props.onFetchTemplate(true);    
        this.setState({
          status_msg:res
        },this.setState({
          status_msg:""
        }))
      })
    }
  }
    
  deleteTemplate (tmp) {
    confirm('Are you sure ?', 'Do you want to delete this template', 'warning').then((res) => {
      if (res) {
        this.props.onDeleteTemplate(tmp.message_key).then(res=>{
          this.setState({
            status_msg:res
          },()=>{
            this.setState({
              status_msg:""
            })
          })
        })
      }
    });
  }
  

  
  replaceVariablesWithValue (templ, str, value) {
    if (value !== undefined) {
      if (value.indexOf('<p>') > -1) {
        let no_lines = value.split('\n').length;
        if (no_lines > 1) {
          value = value.replace(/<p/img, '<div');
          value = value.replace(/<\/p/img, '</div');
        } else {
          value = value.replace(/(<p[^>]+?>|<p>|<\/p>)/img, '');
        }
      }
      var index = templ.body.indexOf(str);
      var i;
      for (i = 0; i <= 20; i++) {
        if (templ.body.indexOf(str) === -1) {
          break;
        }
        templ.body = _.replace(templ.body, str, value);
      }
      templ.name = _.replace(templ.name, str, value);
      templ.subject = _.replace(templ.subject, str, value);
    }
    return templ;
  }


  forwardTemplate (tmp) {
    let tmpVariables = []
    if( tmp.template_variables && tmp.template_variables.length > 0 ){
      tmpVariables = tmp.template_variables.map(k => {
        return {
          key: k,
          value: ''
        }
      })
    }
    this.setState({
      // openSendMailDialog: true,
      tmpKey: tmp.message_key,
      templateVariables:  tmpVariables,

    });
  }
  openCreateTemplate () {
    this.setState({openDialog: true});
  }
  handleCloseDialog () {
    this.setState({
      openDialog:         false,
      templateId:         '',
      templateName:       '',
      templateSubject:    '',
      templateBody:       RichTextEditor.createEmptyValue(),
      errName:            '',
      errSubject:         '',
      openSendMailDialog: false,
      recipient:          []
    });
    this.props.onClearGenePreSendEmail()
  }
  handleCloseDialog1 () {
    this.setState({
      openDialog:         false,
      templateId:         '',
      templateName:       '',
      templateSubject:    '',
      templateBody:       RichTextEditor.createEmptyValue(),
      errName:            '',
      errSubject:         '',
      openSendMailDialog: false,
      recipient:          [],
      uploadedPDF:        []
    });
  }
  toggleDialog (back, front) {
    $('#' + back).toggle();
    $('#' + front).toggle();
  }
  filterList (searchText) {
    let usersList = this.props.employee.employee, // this.props.usersList.users,
      list = [];
    usersList.map((user) => {
      if (user.name.toLowerCase().indexOf(searchText) !== -1) {
        list.push(user);
      }
    });
    this.setState({usersList: list});
  }
  selectUser (data, status, recipientType = this.state.recipientType) {
    if (recipientType === 'Recipient') {
      let recipient = this.state.recipient;
      status ? recipient[0] = data : _.pullAllBy(recipient, [data], 'email');
      this.setState({recipient: recipient});

      this._applyTemplateVariables(this.state.templateId)

    } else if (recipientType === 'CC') {
      let recipient = this.state.cc;
      status ? recipient.push(data) : _.pullAllBy(recipient, [data], 'user_Id');
      this.setState({cc: recipient});
    } else if (recipientType === 'BCC') {
      let recipient = this.state.bcc;
      status ? recipient.push(data) : _.pullAllBy(recipient, [data], 'user_Id');
      this.setState({bcc: recipient});
    }
  }

  submitEmail (email) {
    var pattern = /^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/;
    email = email.trim();
    if (_.isEmpty(email)) {
      this.setState({emailValidationError: 'Empty field'});
    } else if (!email.match(pattern)) {
      this.setState({emailValidationError: 'Not a valid email'});
    } else {
      this.setState({emailValidationError: '', recipientEmailId: ''});
      this.selectUser({user_Id: '#', name: email, email: email}, true, this.state.recipientType);
    }
  }

  onClickLabel (label, indexLabel, recipientType) {
    this.selectUser(label, false, recipientType);
    let templateVariables = []
     this.state.templateVariables.forEach((item)=>{
        item.value ="" 
        templateVariables.push(item)
      })
    
    this.setState({templateVariables})
  }
  handleContentChange (value) {
    this.setState({templateBody: value});
  }
  closeMailPreview () {
    this.setState({openPreview: false, sentMail: {}});
  }
  showError (id, errorMsg) {
    $('#' + id + ' span').remove();
    $('#' + id).fadeIn().append('<span>' + errorMsg + '<span>');
  }
  hideError (e, id) {
    e.preventDefault();
    $('#' + id).fadeOut(0);
    $('#' + id + ' span').remove();
  }
  
  handleClose () {
    this.setState({openVarDialog: false, pValue: _.remove(this.state.pValue)});
  }
  filterLeaveList=(tab)=>{
   this.setState({
       selectedTab:tab
   })
  }
  _applyTemplateVariables = ( templateId ) => {
    const { templateVariables } = this.state
    let recipient = ""
    let format = 'DD-MM-YYYY';
    if (this.state.recipient.length > 0) {
      let id = this.state.recipient[0].user_Id;
      recipient = _.find(this.state.usersList, function (o) { return o.user_Id == id; });
    }
    if( recipient && templateVariables.length > 0 ){
      templateVariables.map((tvar,key) => {
        let newVal = ""
        if( recipient[tvar.key] ){
          newVal = recipient[tvar.key]
        }
        if(tvar.key == 'employee_name' ){
          newVal = recipient["name"]
      }else if (tvar.key === 'salary') {
        newVal = recipient.salary_detail;
        newVal = newVal.toString();
      } else if (tvar.key === 'employee_user_name') {
        newVal = recipient.username;
      } else if (tvar.key === 'employee_email_id') {
        newVal = recipient.work_email;
      }  else if (tvar.key === 'employee_user_id') {
        newVal = recipient.user_Id;
      } else if (tvar.key === 'employee_number') {
        newVal = recipient.emergency_ph1;
      } else if (tvar.key === 'employee_title') {
        newVal = recipient.jobtitle;
      } else if (tvar.key === 'employee_name') {
        newVal = recipient.name;
      }  else if (tvar.key === 'training_completion_date') {
        var mydate = new Date(recipient.training_completion_date);
        if (mydate !== 'Invalid Date') {
          newVal = moment(mydate).format(format);
        } else {
          newVal = 'training_completion_date';
        }
      } else if (tvar.key == 'termination_date') {
        mydate = new Date(recipient.termination_date);
        if (mydate != 'Invalid Date') {
          newVal = moment(mydate).format(format);
        } else {
          newVal = 'termination_date';
        }
      }
        templateVariables[key] = {
            ...templateVariables[key],
            value: newVal
        }

      })

    }
    this.setState({
      templateVariables: templateVariables
    })
  }

  _onChangeTemplateVariable = (e) => {
    const { templateVariables } = this.state
    let objIndex = templateVariables.findIndex((obj => obj.key == e.target.name ));
    templateVariables[objIndex] = {
      ...templateVariables[objIndex],
      value: e.target.value
    }
    this.setState({
      templateVariables: templateVariables
    })
  }

  _renderTemplateVariablesForm = () => {
    const { templateVariables } = this.state
    return (
      <form className="form-mtn-4">
      {
        templateVariables.map((tvar,i) => {
          return (
            <div key={i}>
              <div className="col-md-6 capitalize-div">
                {tvar.key.split("_").join(" ")}
              </div>
              <div className="col-md-6 mtn-btm-5 mt-5">
                <input 
                  type="text" 
                  name={tvar.key}
                  className="form-control" 
                  onChange={this._onChangeTemplateVariable} 
                  value={tvar.value} 
                />
              </div>
            </div>
          )
        })
      }
      </form>
    )
  }

    _getTemplateDataForAPI = () => {
        const {templateVariables} = this.state;
        let ret = {}
        templateVariables.forEach((item)=>{
            ret[item.key] = item.value
        })        
        return ret
    }

    _validationForApi=()=>{
      const {templateVariables} = this.state;
      const a = templateVariables.filter((item)=>item.value !=="")
      if(templateVariables.length){
        if(a.length){
          return true
        }else{
          return false
        }
      }else{
        return true
      }
    }

  _generateTemplatePreview = (val,data,requiredField) => {
    const {tmpKey} = this.state;
    // if(this._validationForApi()){
      const payload = {
        data:{...data,...requiredField},
        message_key:tmpKey,
        pdf:this.state.toggle_pdf
      }
      this.props.onGenerateTemplatePreview(payload)
    // }
  }

  _sentEmailTemplatePreview = (val,data,requiredField,message, cc, bcc) => {    
    const {tmpKey, recipient} = this.state;
    let to = [];
    // if(this._validationForApi()){
    //   this.state.cc.forEach((item)=>cc.push(item.email))
    //   this.state.bcc.forEach((item)=>bcc.push(item.email))
    //   recipient.forEach((item)=>to.push(item.email))
      const payload = {
        message_key:tmpKey,
        data:{...data,...requiredField},
        to:[val],
        cc:[cc],
        bcc:[bcc],
        attachment:this.state.toggle_attachment,
        message
      }
      this.props.onsentEmailPreview(payload).then((succ)=>{
        this.setState({
          openSendMailDialog:false,
          status_msg:succ
        },()=>{
          this.setState({
            status_msg:""
          })
        })
      })
    // }
  }

  render () {
    const generatedPrePdf = this.props.templates.generated_preview
    let protocol=window.location.protocol;
    let fileList = [];
    const actionsCreateTemplate = [
      <FlatButton label="Close" className="m-r-5" primary onClick={this.handleCloseDialog} />,
      <RaisedButton label={_.isEmpty(this.state.templateId) ? 'SAVE' : 'Update'} primary onClick={this.saveTemplate} />
    ];
    const actionsSendMail = [
      <div className="dialog-container"> 
          <FlatButton label="Close" primary onClick={this.handleCloseDialog} />
          <span className="span-mr-r"/>
          {this.state.toggle_pdf && <RaisedButton label={'generate preview'} primary onClick={this._generateTemplatePreview} />}
          <span className="span-mr-l" />
          <span className="span-mr-l" />
          {<RaisedButton label={'send'} primary onClick={this._sentEmailTemplatePreview} />}
      </div>,
      
    ];

        // ------------------------------------
    let listChartItems = [],
      recipientType = this.state.recipientType, // $('input[name="recipientType"]:checked').val(),
      recipient = [];
    if (recipientType === 'Recipient') {
      recipient = this.state.recipient;
    } else if (recipientType === 'CC') {
      recipient = this.state.cc;
    } else if (recipientType === 'BCC') {
      recipient = this.state.bcc;
    }

    _.map(this.state.usersList, (user, i) => {
      let check = false;
      
      if (_.filter(recipient, _.matches({user_Id: user.user_Id})).length > 0) {
        check = true;
      }
      listChartItems.push(
        <li className="mb-sm b-b p-t p-b llll" key={i}>
          <div className="form-group d-flex">
            <Checkbox
              id={`checkbox${i}`}
              name={`checkbox${i}`}
              checked={check}
              onClick={()=>this.selectUser({user_Id: user.user_Id, name: user.name, email: user.work_email}, !check)}
            /> 
           <span className="avatar ml-4">
              <img src={user.image? user.image: a5} />
            </span>
           <Label for={`checkbox${i}`} className="ml-2">{user.name}</Label>
          </div>
        </li>);
    });
        // -----------------pending Variables
    let pendingVar = [];
    _.map(this.state.pValue, (variable, i) => {
      pendingVar.push(
        <div className="form-group" key={i}>
          <label>Enter value for {variable.name} :</label>
          <input type="text" className="form-control" onChange={(e) => { variable.value = e.target.value; this.setState({pValue: this.state.pValue}); }} value={variable.value} />
        </div>);
    });
    return (
      <div className="app-body" id="view">
        <div className="col-xs-12 col-sm-12 px-0" style={{'float': 'right'}}>
          <div className="row mx-0">
            <div className="col-md-12 col-sm-12 col-xs-12 px-0">
              <div className='row template-view mx-0'>
              <TemplateTab
              filterLeaveList={this.filterLeaveList}
              selectedTab={this.state.selectedTab}
              />
                <div className="col-md-12 px-0 d-flex justify-content-end mb-4 mt-3">
                  <Link to="/app/add_templates">
                    <Button color="success">
                      Add Template
                    </Button>
                  </Link>
                
                </div>
                <div className='col-md-12 col-xs-12 col-sm-12 px-0'>

                  <AlertFader>
                    {this.state.status_msg && (
                      <Alert
                        className="alert-transparent rem-fade rh-section rh-alert"
                        color="success"
                      >
                        <span className="alert-bold">
                          INFO
                        </span>
                        : <span>{this.state.status_msg}</span>
                      </Alert>
                    )}
                  </AlertFader>
                  {this.props.templates.add_template_status && (
                      <Alert
                        className="alert-transparent rem-fade rh-section rh-alert"
                        color="success"
                      >
                        <span className="alert-bold">
                          INFO
                        </span>
                        : <span>{this.props.templates.add_template_status }</span>
                      </Alert>
                    )}
                </div>
                <div className={this.state.paper} id='templatesList' className="col-md-12 px-0">
                <MailTemplate
                  templates={this.props.templates}
                  forwardTemplate={this.forwardTemplate}
                  toggleDialog={this.toggleDialog}
                  variables={this.props.sytem_and_user_variables.sytem_and_user_variables}
                  openDialog={this.state.openDialog}
                  handleContentChange={this.handleContentChange}
                  saveTemplate={this.saveTemplate}
                  state={this.state}
                  onHandleChange={this.onHandleChange}
                  toggleEdit={this.toggleEdit}
                  deleteTemplate={this.deleteTemplate}
                  usersList={this.state.usersList}
                  _sentEmailTemplatePreview={this._sentEmailTemplatePreview}
                  _generateTemplatePreview={this._generateTemplatePreview}
                  selectedTab={this.state.selectedTab}
                />
                  
                </div >
              </div>
            </div>
          </div>
   
        </div>
      </div>
    );
  }
}

export default Variables;
