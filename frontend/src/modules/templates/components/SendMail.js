import React, { useState, useEffect } from "react";
import RichTextEditor from "react-rte";
import SendTo from "./SendTo";
import { Button, Alert } from "reactstrap";
import RequiredModal from "./RequiredModal";
import debounce from "lodash/debounce";
import TextField from "@material-ui/core/TextField";

var debounceSentMail = null;

export default function SendMail(props) {
  const [template, setTemplate] = useState({
    ...props.template,
    message: props.template.message
  });
  const [isOpenModal, setIsOpenModal] = useState(false);
  const [requiredField, setRequiredField] = useState({});
  const [value, setValue] = React.useState(null);
  const [cc, setCc] = React.useState("");
  const [bcc, setBcc] = React.useState("");
  const [selectValue, setSelectValue] = React.useState(null);
  const [showEditor, setShowEditor] = React.useState(false);
  const [error, setError] = React.useState({});
  const [showPreviewLoader, setShowPreviewLoader] = React.useState(false);
  useEffect(() => {
    debounceSentMail = debounce(() => sendMail(), 3000);
  }, []);
  let timeout;

  useEffect(() => {
    timeout = setTimeout(() => {
      sendPreview();
    }, 2000);
    selectValue && setShowPreviewLoader(true);
    selectValue && setShowEditor(true);

    // debounceSentMail();te
  }, [value, selectValue]);

  useEffect(() => {
    if (props.templates.isLoading) {
      setShowPreviewLoader(true);
    } else {
      setShowPreviewLoader(false);
    }
  }, [props.templates.isLoading]);
  useEffect(() => {
    setTemplate({
      ...props.templates.generated_preview.data,
      message: RichTextEditor.createValueFromString(
        props.templates.generated_preview.data
          ? props.templates.generated_preview.data.Message
          : "",
        "html"
      )
    });
    if (
      props.templates.generated_preview.data &&
      props.templates.generated_preview.data.missing_payload.length > 0
    ) {
      setIsOpenModal(true);
    } else {
      setIsOpenModal(false);
    }
  }, [
    props.templates.generated_preview.data &&
      props.templates.generated_preview.data.Message,
    props.templates.generated_preview.data &&
      props.templates.generated_preview.data.missing_payload.length
  ]);

  const handleEditorChange = val => {
    setTemplate({
      ...template,
      message: val
    });
  };

  const sendPreview = () => {
    let email = selectValue ? selectValue.work_email : value;
    const reg = /\S+@\S+\.\S+/;
    if (reg.test(email)) {
      setIsOpenModal(false);
      setShowPreviewLoader(true);
      setShowEditor(true);
      props._generateTemplatePreview(email, selectValue, requiredField);
    }
  };
  const sendMail = () => {
    let email = selectValue ? selectValue.work_email : value;
    let message = template.message.toString("html");
    const reg = /\S+@\S+\.\S+/;
    const errors = {};
    if (cc && !reg.test(cc)) {
      errors.cc = true;
    }
    if (bcc && !reg.test(bcc)) {
      errors.bcc = true;
    }
    if (!reg.test(email)) {
      errors.email = true;
    }
    if (!Object.values(errors).length) {
      props._sentEmailTemplatePreview(
        email,
        selectValue,
        requiredField,
        message,
        cc,
        bcc
      );
      setError(errors);
    } else {
      setError(errors);
    }
  };
  const handleButtonClick = docType => {
    if (docType == "document") {
      let divContents = template.message.toString("html");
      var downloadPdf = window.open();
      downloadPdf.document.write(divContents);
      downloadPdf.print();
      downloadPdf.close();
    } else {
      sendMail();
    }
  };
  return (
    <>
      <SendTo
        usersList={props.usersList}
        _sentEmailTemplatePreview={props._sentEmailTemplatePreview}
        sendPreview={sendPreview}
        setValue={e => {
          clearTimeout(timeout);
          setValue(e.target.value);
        }}
        setSelectValue={setSelectValue}
        value={value}
        selectValue={selectValue}
      />
      <TextField
        variant="outlined"
        margin="normal"
        fullWidth
        label="cc"
        type="email"
        value={cc}
        onChange={e => setCc(e.target.value)}
      />
      {error.cc ? "Please fill appropriate email" : ""}
      <TextField
        variant="outlined"
        margin="normal"
        fullWidth
        label="bcc"
        type="email"
        value={bcc}
        onChange={e => setBcc(e.target.value)}
      />
      {error.bcc ? "Please fill appropriate email" : ""}
      <RequiredModal
        isOpenModal={isOpenModal}
        generated_preview={props.templates.generated_preview}
        setRequiredField={setRequiredField}
        requiredField={requiredField}
        sendPreview={sendPreview}
      />
      <div style={{ color: "red" }}>
        Enter any email address inside or outside of company
      </div>
      {showPreviewLoader ? (
        <Alert className="alert-transparent" fade={false} color="info">
          Hold on generating preview...
        </Alert>
      ) : !showEditor ? (
        <Alert className="alert-transparent" fade={false} color="info">
          Enter Email Address to generate preview
        </Alert>
      ) : (
        <>
          <RichTextEditor
            className="editor-style"
            value={template.message}
            onChange={val => {
              handleEditorChange(val);
            }}
          />
          <Button
            color="primary"
            onClick={() => handleButtonClick(props.template.Doc_type)}
            className="send-btn-template"
          >
            {props.template.Doc_type == "document" ? "Download" : "Send"}
          </Button>
        </>
      )}
    </>
  );
}
