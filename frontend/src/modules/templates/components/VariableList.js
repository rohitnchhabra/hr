import React from 'react';
import { Badge } from 'reactstrap';

export default function VariableList({list}) {
    return (
        <div className="mt-2 row no-gutters">
            {list.map((val, i) => (
                <div className="py-1 col-md-3 col-sm-4 col-6 " key={i}>
                    <Badge color="primary">
                        {val.name}
                    </Badge>
                </div>
            ))}
        </div>
    )
}
