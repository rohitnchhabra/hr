import React, { useState, useEffect } from "react";
import classnames from "classnames";
import GenericCardList from "../../../components/generic/GenricCardList";
import Paper from "material-ui/Paper";
import _ from "lodash";
import useDomNodeHeight from "../../../components/customHooks/useDomNodeHeight";
import { Badge, Button, Label } from "reactstrap";
import RichTextEditor from "react-rte";
import TextField from "material-ui/TextField";
import InputText from "../../../components/generic/input/InputText";
import SendMail from "./SendMail";
import VariableList from "./VariableList";

const MailTemplate = ({
  templates,
  forwardTemplate,
  variables,
  saveTemplate,
  toggleEdit,
  _generateTemplatePreview,
  deleteTemplate,
  usersList,
  _sentEmailTemplatePreview,
  selectedTab
}) => {
  const [template, setTemplate] = useState({});
  const [clicked, setClicked] = useState(0);
  const [deviceDetailsRef, height] = useDomNodeHeight(clicked);
  const [openDialog, handleDialog] = useState(false);
  const [attachment_file, handleFileChange] = useState({});
  const [sendMail, setSendMail] = useState(false);

  useEffect(() => {
    if (
      templates.success == true &&
      templates.templates.length > 0 &&
      Object.keys(template).length == 0
    ) {
      setTemplate(
        templates.templates.filter(val => val.Doc_type === selectedTab)[0]
      );
    } else if (
      templates.success == true &&
      templates.templates.length > 0 &&
      Object.keys(template).length > 0
    ) {
      let new_template = templates.templates.filter((item, i) => {
        return item.id == template.id;
      });
      setTemplate(new_template[0]);
    }
  }, [templates.success]);

  useEffect(() => {
    let firstContent = templates.templates.filter(
      val => val.Doc_type === selectedTab
    );
    if (firstContent.length) {
      setTemplate(firstContent[0]);
      setSendMail(false);
    } else {
      setTemplate({})
    }
  }, [selectedTab]);

  const handleEditorChange = val => {
    setTemplate({
      ...template,
      message: val
    });
  };
  const onSaveTemplate = template => {
    handleDialog(false);
    saveTemplate(template, attachment_file);
    setTemplate({ ...template, message: template.message.toString("html") });
  };
  const editTemplate = () => {
    setTemplate({
      ...template,
      message: RichTextEditor.createValueFromString(template.message, "html")
    });
    handleDialog(true);
    setSendMail(false);
  };
  const closeDialog = () => {
    setTemplate(templates.templates.find((v)=>v._id === template._id));
    handleDialog(false);
  };

  const cardImage = item => {
    if (item.Doc_type == "document") {
      return (
        <div>
          <i className="fa fa-file-pdf-o template-icon"></i>{" "}
        </div>
      );
    } else if (item.Doc_type == "email") {
      return (
        <div>
          <i className="fa fa-envelope-o template-icon"></i>
        </div>
      );
    }
  };
  const handelSendMailClick = () => {
    forwardTemplate(template);
    setSendMail(true);
  };
  const templateDetail = () => {
    return (
      <div className="col-md-9 px-0 mb-md-0 mb-4">
        {template && Object.keys(template).length > 0 && (
          <div className="row no-gutters" ref={deviceDetailsRef}>
            <div className="bg-white ml-1 col-md-12">
              <div className="d-flex pull-right py-2">
                {!sendMail && (
                  <button
                    type="button"
                    className="md-btn p-2 mail-template-button mr-1"
                    onClick={() => handelSendMailClick()}
                  >
                    {template.Doc_type == "document"
                      ? "Download PDF"
                      : "Send Mail"}
                  </button>
                )}
                <button
                  type="button"
                  className="md-btn p-2 mail-template-button mr-1"
                  onClick={() => {
                    openDialog ? onSaveTemplate(template) : editTemplate();
                  }}
                >
                  {openDialog ? "Update Template" : "Edit Template"}
                </button>
                {!template.default && (
                  <button
                    type="button"
                    className="md-btn p-2 mail-template-button mr-1"
                    onClick={() => {
                      deleteTemplate(template);
                    }}
                  >
                    Delete Template
                  </button>
                )}

                {openDialog && (
                  <button
                    type="button"
                    className="md-btn p-2 mail-template-button"
                    onClick={closeDialog}
                  >
                    Close
                  </button>
                )}
              </div>
              <div className="col-md-6 col-sm-12 col-xs-12 m-b">
                {openDialog ? (
                  <>
                    <TextField
                      fullWidth
                      value={template.message_subject}
                      onChange={e =>
                        setTemplate({
                          ...template,
                          message_subject: e.target.value
                        })
                      }
                    />
                    <div className="form-group col-md-12 px-0">
                      <h6>ATTACHMENT</h6>
                      <InputText
                        type="file"
                        name="myFile"
                        onchange={e => {
                          handleFileChange(Array.from(e.target.files));
                        }}
                      />
                    </div>
                  </>
                ) : (
                  <>
                    <h3>{template.message_subject}</h3>
                  </>
                )}
              </div>
              {sendMail ? (
                <div>
                  <SendMail
                    template={template}
                    variables={variables}
                    usersList={usersList}
                    _sentEmailTemplatePreview={_sentEmailTemplatePreview}
                    _generateTemplatePreview={_generateTemplatePreview}
                    templates={templates}
                  />
                </div>
              ) : openDialog ? (
                <>
                  <RichTextEditor
                    className="editor-style"
                    value={template.message}
                    onChange={val => {
                      handleEditorChange(val);
                    }}
                  />
                  <VariableList list={variables} />
                </>
              ) : (
                <div className="col-md-12 col-sm-12 col-xs-12 m-b template-body">
                  <div
                    dangerouslySetInnerHTML={{ __html: template.message }}
                  ></div>
                  {template.attachment_file_name ? (
                    <h6 className="border-top py-2">
                      <span className="font-weight-bold">
                        {"Attachments" + " - "}
                      </span>
                      <span className="template_file_name">
                        {template.attachment_file_name}
                      </span>
                    </h6>
                  ) : null}
                </div>
              )}
            </div>
          </div>
        )}
      </div>
    );
  };
  return (
    <div className="row no-gutters">
      <div className="col-md-3 p-0 mail-template-list scroll">
        {templates && templates.templates.length > 0
          ? templates.templates
              .filter(val => val.Doc_type === selectedTab)
              .map((item, index) => {
                return (
                  <div key={index}>
                    <GenericCardList
                      onCardSelected={() => {
                        setTemplate({
                          ...item,
                          message: item.message.toString("html")
                        });
                        handleDialog(false);
                        setSendMail(false);
                        {
                          window.screen.availWidth >= 768 &&
                            setClicked(click => click + 1);
                        }
                      }}
                      cardBodyClassName={
                        item._id === template._id
                          ? "lightGreen d-flex justify-content"
                          : "d-flex"
                      }
                      cardClassname={"border-0 m-b"}
                      heading={item.message_key}
                      subHeading={""}
                      // cardBodyImage={cardImage(item)}
                      headingClassName={"col-md-10 px-0"}
                      imageClassName={"col-md-2"}
                    />
                    {window.screen.availWidth < 768 &&
                      item._id === template._id &&
                      templateDetail()}
                  </div>
                );
              })
          : null}
      </div>
      {window.screen.availWidth >= 768 && templateDetail()}
    </div>
  );
};

export default MailTemplate;
