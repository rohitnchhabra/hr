import React, { useState } from "react";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import DialogTitle from "@material-ui/core/DialogTitle";
import Button from '@material-ui/core/Button';
import{Alert} from "reactstrap";
import uniqBy from 'lodash/uniqBy';
import GenericDatePicker from "../../../components/generic/GenericDatePicker";
import isMobile from "../../../components/hoc/WindowResize";
import moment from 'moment';

export default function RequiredModal({
  isOpenModal,
  generated_preview,
  requiredField,
  setRequiredField,
  sendPreview,
}) {

  const onHandleChange = e => {
    setRequiredField({
      ...requiredField,
      [e.target.name]: e.target.value
    });
  };
  const onDateChange = (key, value) => {
    setRequiredField({
      ...requiredField,
      [key]: moment(value).format("DD MMM YYYY")
    });
  }
  let uniqueList = [];
  if(generated_preview && generated_preview.data) {
    uniqueList = uniqBy(generated_preview.data.missing_payload, 'key');
  }

  return (
    <Dialog
      open={isOpenModal}
      // onClose={handleClose}
      aria-labelledby="alert-dialog-title"
      aria-describedby="alert-dialog-description"
    >
      <DialogTitle id="alert-dialog-title">
        <Alert color="danger">The template is configured to have some pre-filled data which needs to be filled before we can generated a preview. Please put in appropriate values so that a proper preview can be generated.</Alert>        
      </DialogTitle>
      <DialogContent>
        <DialogContentText id="alert-dialog-description">
          <form className="form-mtn-4">
            {uniqueList.map((tvar, i) => {
                return (
                  <div key={i} className="mb-2">
                    <div className="col-md-6 capitalize-div">
                      {tvar.key.split("_").join(" ")}
                    </div>
                    <div className="col-md-6 mtn-btm-5">
                        {tvar.type === 'date'
                        ? <GenericDatePicker
                            className="ml-0"
                            handleDateChange={value => onDateChange(tvar.key, value)}
                            type={isMobile < 768 ? "absolute" : "inline"}
                            notClearable
                            date={requiredField[tvar.key] ? moment(requiredField[tvar.key], 'DD MMM YYYY') : null}
                          />
                        :
                        <input
                            type="text"
                            id={tvar.key}
                            name={tvar.key}
                            className="form-control"
                            onChange={e => onHandleChange(e)}
                            value={requiredField[tvar.key]}
                          />
                        }
                    </div>
                  </div>
                );
              })}
          </form>
        </DialogContentText>
      </DialogContent>
      <DialogActions>
      <Button onClick={()=>{sendPreview()}} color="primary">
            Generate
          </Button>
      </DialogActions>
    </Dialog>
  );
}
