import React from "react";
import PropTypes from "prop-types";
import { Nav, NavItem, NavLink } from "reactstrap";

const LeaveColorReference = ({ filterLeaveList, selectedTab}) => {
//   if (userRole === CONFIG.ADMIN) {
    return (
      <Nav className="status_header bg-body manage-users-nav-bar d-flex" tabs>
        <NavItem className="flex-fill text-center">
          <NavLink
            className={`${selectedTab === "email" ? "border-warning" : "active"} leave-tab-header`}
            onClick={() => {
              filterLeaveList("email");
            }}
          >
            <span className="leave-status-link text-warning">Emails</span>
          </NavLink>
        </NavItem>
        <NavItem className=" flex-fill text-center">
          <NavLink
            className={`${selectedTab === "document" ? "border-success" : "active"} leave-tab-header`}
            onClick={() => {
              filterLeaveList("document");
            }}
          >
            <span className="leave-status-link text-success">Documents</span>
          </NavLink>
        </NavItem>
        <NavItem className="flex-fill text-center">
          <NavLink
            className={`${selectedTab === "notification" ? "border-danger" : "active"} leave-tab-header`}
            onClick={() => {
              filterLeaveList("notification");
            }}
          >
            <span className="leave-status-link text-danger">Notifications</span>
          </NavLink>
        </NavItem>
      </Nav>
    );
//   } else if (userRole === CONFIG.HR || userRole === CONFIG['HR Payroll Manager'] ) {
//     return( 
//     <Nav className="status_header bg-body manage-users-nav-bar d-flex" tabs>
//       <NavItem className="flex-fill text-center">
//         <NavLink
//           className={`${selectedTab === "Pending" ? "border-warning" : "active"} leave-tab-header`}          
//           onClick={() => {
//             filterLeaveList("Pending");
//           }}
//         >
//           <span className="leave-status-link text-warning">Pending Leaves</span>
//         </NavLink>
//       </NavItem>
//       <NavItem className="flex-fill text-center">
//         <NavLink
//           className={`${selectedTab === "ApprovedByHr" ? "border-success" : "active"} leave-tab-header`}          
//           onClick={() => {
//             filterLeaveList("ApprovedByHr");
//           }}
//         >
//           <span className="leave-status-link text-successs">Approved By HR</span>
//         </NavLink>
//       </NavItem>
//     </Nav>
//     );
//   }
};

LeaveColorReference.propTypes = {
  filterLeaveList: PropTypes.func.isRequired,
  selectedTab: PropTypes.string.isRequired,
  userRole: PropTypes.string.isRequired
};

export default LeaveColorReference;
