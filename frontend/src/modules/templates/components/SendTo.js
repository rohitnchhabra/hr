import React, { useEffect } from "react";
/* eslint-disable no-use-before-define */

import TextField from "@material-ui/core/TextField";
import Autocomplete from "@material-ui/lab/Autocomplete";
import { Button } from "reactstrap";
import SendMail from "./SendMail";
import debounce from "lodash/debounce";
export default function SendTo({
  usersList,
  setValue,
  setSelectValue,
  value,
  selectValue
}) {
  const defaultProps = {
    options: usersList,
    getOptionLabel: (option: FilmOptionType) => option.work_email
  };
  // const flatProps = {
  //   options: usersList.map(option => option.title)
  // };
 

  return (
    <div>
      <Autocomplete
        {...defaultProps}
        id="disable-open-on-focus"
        disableOpenOnFocus
        debug
        value={selectValue}
        onChange={(event, newValue) => {
          setSelectValue(newValue);
        }}
        renderInput={params => (
          <div>
            <TextField
              {...params}
              label="To"
              margin="normal"
              fullWidth
              variant="outlined"
              value={value}
              onChange={setValue}
            />
          </div>
        )}
      />
    </div>
  );
}
interface FilmOptionType {
  work_email: string;
}
