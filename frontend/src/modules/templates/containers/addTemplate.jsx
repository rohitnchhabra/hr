import React from 'react';
import {connect} from 'react-redux';
import {withRouter} from 'react-router';
import Menu from '../../../components/generic/Menu';
import {isNotUserValid} from '../../../services/generic';
import Header from '../../../components/generic/Header';
import Template from '../components/Template';
import * as actions from '../../../redux/actions';
import * as actions_salary from '../../../redux/salary/actions/viewSalary';
import * as actions_templates from '../../../redux/templates/actions/templates';

class TemplateContainer extends React.Component {
  constructor (props) {
    super(props);
  }
  componentWillMount () {
    // this.props.onFetchUserSalaryDetails().then(() => {
    this.props.onFetchTemplate();
      // this.props.onFetchVariables();
    // });
    this.props.onFetchSystemAndUserVariables();
    this.props.requestUsersList()
  }
  componentWillReceiveProps (props) {
    let isNotValid = isNotUserValid(props.match.path, props.loggedUser);
    if (isNotValid.status) {
      this.props.history.push(isNotValid.redirectTo);
    }
  }

  componentWillUnmount() {
    this.props.onClearGenePreSendEmail();
  }
  render () {        
    return (
      <div>
        <Menu {...this.props} />
        <div id="content" className="app-content box-shadow-z0" role="main">
          <Header pageTitle={'Email Template'} {...this.props} />
          <Template {...this.props} />
        </div>
      </div>
    );
  }
}
function mapStateToProps (state) {
  return {
    frontend:   state.frontend.toJS(),
    loggedUser: state.logged_user.userLogin,
    templates:  state.template.toJS(),
    // employee:   state.empSalaryList.toJS(),
    employee: state.usersList.toJS().users,
    sytem_and_user_variables:  state.template.toJS(),
  };
}
const mapDispatchToProps = (dispatch) => {
  return {
    onFetchTemplate: (isUpdate) => {
      return dispatch(actions_templates.get_templates(isUpdate));
    },
    // onFetchVariables: () => {
    //   return dispatch(actions_templates.get_variable());
    // },
    onSaveTemplate: (formData) => {
      return dispatch(actions_templates.save_templates(formData));
    },
    onDeleteTemplate: (t_key) => {
      return dispatch(actions_templates.delete_template(t_key));
    },
    onSendMail: (email) => {
      return dispatch(actions_templates.send_mail(email));
    },
    onDownloadPdf: (template, fileName) => {
      return dispatch(actions_templates.download_pdf(template, fileName));
    },
    onFetchUserSalaryDetails: () => {
      return dispatch(actions_salary.fetchUserSalaryDetails());
    },
    onGenerateTemplatePreview: (data) => {
      return dispatch(actions_templates.generateTemplatePreview(data));
    },
    onsentEmailPreview: (data) => {
      return dispatch(actions_templates.emailTemplatePreview(data));
    },
    onClearGenePreSendEmail: (data) => {
      return dispatch(actions_templates.clearGenePreSendEmail(data));
    },
    onFetchSystemAndUserVariables:()=>{
      return dispatch(actions_templates.fetchSystemAndUserVariable());
    },
    requestUsersList: () => {
      return dispatch(actions.requestUsersList());
    },
    showHeading: (data) => {
      return dispatch(actions.showHeading(data));
    },
  };
};

const VisibleTemplateContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(TemplateContainer);

const RouterVisibleTemplateContainer = withRouter(VisibleTemplateContainer);

export default RouterVisibleTemplateContainer;
