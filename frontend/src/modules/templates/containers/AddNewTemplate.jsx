import React, { Component, createRef } from "react";
import { connect } from "react-redux";
import { withRouter } from "react-router";
import InputText from "../../../components/generic/input/InputText";
import { Form, Label, Button } from "reactstrap";
import RichTextEditor from "react-rte";
import Heading from "../../../components/generic/Heading";
import * as actions from '../../../redux/actions'
import * as actions_templates from "../../../redux/templates/actions/templates";
import ImageCompressor from "image-compressor.js";
import { qualityValue } from "../../../helper/helper";
import VariableList from "../components/VariableList";

class AddNewTemplate extends Component {
  state = {
    doc_type: "",
    for: "",
    message: RichTextEditor.createEmptyValue(),
    subject: "",
    attachment_file: [],
    message_key: "",
    message_subject: "",
    working: "",
    copySelectedTemplate:""
  };

  addTemplateMessage = val => {
    this.setState({
      message: val
    });
  };
  componentWillMount() {
    this.props.onFetchTemplate();
    this.props.onFetchSystemAndUserVariables();
    this.props.showHeading("Add Template")
  }
  handleFileChange = e => {
    this.setState({ attachment_file: Array.from(e.target.files) });
  };

  handleSubmit = e => {
    e.preventDefault();
    const file = this.state.attachment_file;
    if (!file) {
      return;
    } else if (file[0] && file[0].type && !file[0].type.includes("image")) {
      const formData = new FormData();
      formData.append("attachment_file", file[0]);
      formData.append("doc_type", this.state.doc_type);
      formData.append("message_subject", this.state.message_subject);
      formData.append("message_key", this.state.message_key);
      formData.append("message", this.state.message.toString("html"));
      this.props.onAddTemplate(formData).then(res => {
        this.props.history.push("/app/mail_templates");
      });
    } else if (file[0] && file[0].type) {
      let quality = qualityValue(file[0]);
      let imageCompressor = new ImageCompressor();
      imageCompressor
        .compress(file[0], {
          quality: quality
        })
        .then(compressedFile => {
          const formData = new FormData();
          formData.append("attachment_file", compressedFile);
          formData.append("doc_type", this.state.doc_type);
          formData.append("message_subject", this.state.message_subject);
          formData.append("message_key", this.state.message_key);
          formData.append("message", this.state.message.toString("html"));
          this.props.onAddTemplate(formData).then(res => {
            this.props.history.push("/app/mail_templates");
          });
        });
    } else {
      const formData = new FormData();
      formData.append("attachment_file", file[0]);
      formData.append("doc_type", this.state.doc_type);
      formData.append("message_subject", this.state.message_subject);
      formData.append("message_key", this.state.message_key);
      formData.append("message", this.state.message.toString("html"));
      this.props.onAddTemplate(formData).then(res => {
        this.props.history.push("/app/mail_templates");
      });
    }
  };
  handleChange = e => {
    const { templates } = this.props.template;
    if(!e.target.value) {
      this.setState({copySelectedTemplate: '', message: RichTextEditor.createEmptyValue()})
    } else {
      let filteredArray = templates.filter(
        val => e.target.value === val.message_key
      );
      this.setState({
        copySelectedTemplate: e.target.value,
        message: RichTextEditor.createValueFromString(
          filteredArray[0].message,
          "html"
        )
      });
    }
  };
  render() {
    const { params } = this.state;
    const { templates } = this.props.template;
    return (
      <>
        <div className="row no-gutters p-3 bg-white">
          <Form
            method="POST"
            encType="multipart/form-data"
            onSubmit={this.handleSubmit}
          >
            <div className="form-group col-md-6 px-0">
              <Label for="subject">Name</Label>
              <InputText
                onchange={e =>
                  this.setState({
                    message_key: e.target.value
                  })
                }
                required
                value={this.state.message_key}
              />
            </div>
            <div className="form-group col-md-6 pr-0">
              <Label for="subject">Subject</Label>
              <InputText
                onchange={e =>
                  this.setState({
                    message_subject: e.target.value
                  })
                }
                required
                value={this.state.message_subject}
              />
            </div>
            <div className="form-group col-md-6 px-0">
              <Label>Document Type</Label>
              <select
                className="form-control"
                value={this.state.doc_type}
                onChange={e =>
                  this.setState({
                    doc_type: e.target.value
                  })
                }
                required
              >
                <option value="" disabled>--- Select Doc Type ---</option>
                <option value="document">document</option>
                <option value="email">email</option>
                <option value="notification">notification</option>
              </select>
            </div>
            {this.state.doc_type === "email" && (
              <div className="form-group col-md-6 pr-0">
                <Label for="subject">Attachment</Label>
                <InputText
                  type="file"
                  name="myFile"
                  onchange={this.handleFileChange}
                />
              </div>
            )}
            <div
              className={
                "form-group col-md-6  " +
                (this.state.doc_type == "email" ? "px-0" : "pr-0")
              }
            >
              <Label>Copy from existing template</Label>
              <select
                className="form-control"
                value={this.state.copySelectedTemplate}
                onChange={this.handleChange}
              >
                <option key="-1" value="" disabled={!this.state.copySelectedTemplate}>{this.state.copySelectedTemplate ? "No Template" : "--- Select Doc from existing template to copy ---" }</option>
                {templates.map((val, i) => (
                  <option value={val.message_key} key={i}>
                    {val.message_key}
                  </option>
                ))}
              </select>
            </div>
            <div className="col-md-12 px-0">
              <RichTextEditor
                className="editor-style add-template-editor"
                value={this.state.message}
                onChange={this.addTemplateMessage}
              />
            </div>
            <div className="col-md-12 px-0 py-3">
              <Button className="pull-right" color="success" type="submit">
                Add Template
              </Button>
            </div>
          </Form>
          <VariableList list={this.props.system_and_user_variables.sytem_and_user_variables} />
        </div>
      </>
    );
  }
}
function mapStateToProps(state) {
  return {
    frontend: state.frontend.toJS(),
    loggedUser: state.logged_user.userLogin,
    template: state.template.toJS(),
    system_and_user_variables: state.template.toJS()
  };
}
const mapDispatchToProps = dispatch => {
  return {
    onAddTemplate: formData => {
      return dispatch(actions_templates.add_templates(formData));
    },
    onFetchTemplate: () => {
      return dispatch(actions_templates.get_templates());
    },
    onFetchSystemAndUserVariables: () => {
      return dispatch(actions_templates.fetchSystemAndUserVariable());
    },
    showHeading:(data)=>{
      return dispatch(actions.showHeading(data));
    }
  };
};

const AddNewTemplateContainer = withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(AddNewTemplate)
);

// const AddNewTemplateContainer = withRouter(AddNewTemplateContainer);

export default AddNewTemplateContainer;
