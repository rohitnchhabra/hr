import React from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { withRouter } from "react-router";
import { isNotUserValid } from "../../../services/generic";
import TeamList from "../../../modules/team/components/TeamList";
import TeamDetails from "../../../modules/team/components/TeamDetails";
import * as actions from "../../../redux/actions";
import TextField from "material-ui/TextField";
import Heading from "../../../components/generic/Heading";
import RaisedButton from "material-ui/RaisedButton";
import FlatButton from "material-ui/FlatButton";
import AlertFader from "../../../components/generic/AlertFader";
import {
  Button,
  Nav,
  NavItem,
  NavLink,
  Alert,
  Modal,
  ModalHeader,
  ModalBody,
  ModalFooter
} from "reactstrap";

class ViewTeam extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      empList: [],
      all_Teams: "",
      active: "active",
      addNewTeam: "",
      viewTeam: "hidden",
      firstArrow: "show",
      secondArrow: "hidden",
      activeSecondTab: "addNewTeam",
      openDialog: false,
      floatingLabel: "",
      hint: "",
      teamError: "",
      teamName: "",
      dialogTitle: "",
      isDeleted: false,
      isAdded: false,
      errorData: "",
      teamListLength: "",
      isloggedUserAdmin:false,
    };
    this.openPage = this.openPage.bind(this);
    this.openCreateTeam = this.openCreateTeam.bind(this);
    this.handleClose = this.handleClose.bind(this);
    this.saveTeam = this.saveTeam.bind(this);
    this.callSaveApi = this.callSaveApi.bind(this);
    this.onSelectTeam = this.onSelectTeam.bind(this);
  }
  componentWillMount() {
    this.props.requestTeamList();
    this.props.showHeading("Team(s)")
  }
  componentWillReceiveProps(props) {
    window.scrollTo(0, 0);
    let isNotValid = isNotUserValid(
      this.props.location.pathname,
      props.loggedUser
    );
    if (isNotValid.status) {
      this.props.history.push(isNotValid.redirectTo);
    }
    if (props.addTeam.isLoading != this.props.addTeam.isLoading) {
      this.handleClose();
      let message = props.addTeam.message;
      this.setState(
        {
          alertMsg: message
        },
        () => {
          this.clearAlert();
        }
      );
    }
    if (props.teamList.data.length != this.props.teamList.data.length) {
      this.setState({ teamListLength: props.teamList.data.length });
    }
  }
  openCreateTeam() {
    this.setState({
      dialogTitle: "Add New Team",
      openDialog: true,
      floatingLabel: "Team Name",
      hint: "Enter Team Name",
      teamError: "",
      teamName: ""
    });
  }
  handleClose() {
    this.setState({
      dialogTitle: "",
      openDialog: false,
      floatingLabel: "",
      hint: ""
    });
  }

  openPage(toDisplay) {
    if (toDisplay === "addNewTeam") {
      this.setState({
        addNewTeam: "",
        viewTeam: "hidden",
        firstArrow: "show",
        secondArrow: "hidden",
        activeSecondTab: toDisplay
      });
    } else {
      this.setState({
        addNewTeam: "hidden",
        viewTeam: "",
        firstArrow: "hidden",
        secondArrow: "show",
        activeSecondTab: toDisplay
      });
    }
  }

  toggleSecondTabs = tab => {
    if (this.state.activeSecondTab !== tab) {
      this.setState({
        activeSecondTab: tab
      });
    }
  };
  saveTeam() {
    let teamName = this.state.teamName;
    if (teamName === "") {
      this.setState({
        teamError: "Required"
      });
    } else {
      let dataToSend =
        this.props.teamList &&
        this.props.teamList.data &&
        this.props.teamList.data.length > 0
          ? this.props.teamList.data
          : [];
      dataToSend.push(teamName);
      this.callSaveApi("save", dataToSend);
    }
  }

  callSaveApi(func, newArray) {
    this.props.requestAddTeam(newArray);
    switch (func) {
      case "save":
        return this.setState({
          isAdded: true,
          isDeleted: false
        });
      case "delete":
        return this.setState({
          isAdded: false,
          isDeleted: true
        });
      default:
        return null;
    }
    
  }
  toggle = () => {
    this.setState({ openDialog: false });
  };

  clearAlert = () => {
    this.setState({ alertMsg: "" });
  };

  onSelectTeam = emp => {
    if (emp.target.value) {
      let selectedTeam = emp.target.value;
      this.props.requestGetTeam({ selectedTeam });
    }
  };
  render() {
    const actions = [
      <FlatButton
        label="Back"
        primary
        onClick={this.handleClose}
        className="m-r"
      />,
      <RaisedButton label="Submit" primary onClick={this.saveTeam} />
    ];

    let teams = (this.props.teamList && this.props.teamList.data) || [];

    return (
      <div>
        <div
          id="content"
          className="app-content box-shadow-z0 viewteam-wrapper"
          role="main"
        >
          <div className="app-body" id="view_team">
            <div className="row mx-0">
              <Modal
                className="modal-viewTeam"
                isOpen={this.state.openDialog}
                fade={false}
                toggle={() => this.toggle()}
              >
                <ModalHeader className="bod-none" toggle={() => this.toggle()}>
                  {this.state.dialogTitle}
                </ModalHeader>
                <ModalBody className="bg-white border-none">
                  <div>
                    <TextField
                      ref="value"
                      floatingLabelText={this.state.floatingLabel}
                      floatingLabelFixed
                      hintText={this.state.hint}
                      fullWidth
                      errorText={this.state.teamError}
                      value={this.state.teamName}
                      onChange={e => {
                        this.setState({
                          teamName: e.target.value
                        });
                      }}
                    />
                  </div>
                </ModalBody>
                <ModalFooter className="bod-none">{actions}</ModalFooter>
              </Modal>

              <div className="col-xs-12 team_padding_list px-0">
                <div className="row mx-0">
                  <div
                    className={
                      this.state.activeSecondTab == "viewTeam"
                        ? "col-xs-6 user-icon-col px-0"
                        : "col-xs-6 user-icon-col px-0"
                    }
                  >
                  </div>
                  <div
                    className={
                      this.state.activeSecondTab == "viewTeam"
                        ? "col-xs-6 addTeamBtn btn detail_button_team p-0"
                        : "col-xs-6 addTeamBtn btn p-0"
                    }
                  >
                    <Button
                      color="success"
                      className="team-add-button md-btn md-raised  m-b-sm indigo"
                      onClick={this.openCreateTeam}
                    >
                      Add Team
                    </Button>
                  </div>
                </div>
              </div>
            </div>

            <AlertFader>
              {this.state.alertMsg ? (
                <Alert
                  className="alert-transparent rem-fade rh-section rh-alert"
                  color={
                    this.state.isAdded
                      ? "success"
                      : this.state.isDeleted
                      ? "danger"
                      : ""
                  }
                >
                  {this.state.alertMsg}
                </Alert>
              ) : null}
            </AlertFader>
            <div className=" page_team_page">
              <div className="bar_team_page">
                <div className="bg-light p-2">
                  <Nav pills className="team-view-navvar"> 
                  <div className="tab-one">
                      <NavItem>
                        <NavLink
                          href="#"
                          className={
                            this.state.activeSecondTab === "addNewTeam" &&
                            "active"
                          }
                          onClick={() => {
                            this.openPage("addNewTeam");
                          }}
                        >
                          All
                        </NavLink>
                      </NavItem>
                  
                      <NavItem>
                        <NavLink
                          href="#"
                          className={
                            this.state.activeSecondTab === "viewTeam" && "active"
                          }
                          onClick={() => {
                            this.openPage("viewTeam");
                          }}
                        >
                          Team Details
                        </NavLink>
                      </NavItem>
                    </div>
                    {this.state.activeSecondTab === "viewTeam" && 
                      <div className="tab-twco">
                          <div className="form-group  d-flex">
                            <select
                              className="form-control"
                              onChange={emp => this.onSelectTeam(emp)}
                            >
                              <option value="">--Select team--</option>
                              {teams.map((team, keys) => {
                                return (
                                  <option key={keys} value={team}>
                                    {team}
                                  </option>
                                );
                              })}
                            </select>
                          </div>
                        </div>
                        }
                  </Nav>
                </div>
              </div>
            </div>
            <div className={this.state.addNewTeam}>
              <TeamList
                {...this.props}
                handleClose={this.handleClose}
                {...this.state}
                openPage={this.openPage}
                callSaveApi={this.callSaveApi}
                isLoading={this.props.teamList.isLoading}
              />
            </div>
            <div className={`${this.state.viewTeam}`}>
              <div className=" team-details-table">
                <TeamDetails
                  {...this.state}
                  loggedUser={this.props.loggedUser.data.role}
                  team={this.props.team}
                  openPage={this.openPage}
                  isLoading={this.props.team.isLoading}
                  teams={teams}
                  onSelectTeam={this.onSelectTeam}
                />
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
function mapStateToProps(state) {  
  return {
    frontend: state.frontend.toJS(),
    loggedUser: state.logged_user.userLogin,
    usersList: state.usersList.toJS(),
    employee: state.empSalaryList.toJS(),
    teamList: state.teamList.teamList,
    addTeam: state.teamList.addTeam,
    team: state.teamList.team,
    errorMessage: state.teamList.errorMessage
  };
}
const mapDispatchToProps = dispatch => {
  return bindActionCreators(actions, dispatch);
};

const VisibleViewTeam = connect(
  mapStateToProps,
  mapDispatchToProps
)(ViewTeam);

const RouterVisibleViewTeam = withRouter(VisibleViewTeam);

export default RouterVisibleViewTeam;
