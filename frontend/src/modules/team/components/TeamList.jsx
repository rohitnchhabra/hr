import React from "react";
import * as _ from "lodash";

import GenericTable from "../../../components/generic/table/Table";

const list = function(listItem, onChange, rowStyle, index) {
  return (
    <>
      <td>
        <span>{listItem}</span>
      </td>
      <td
        className="del-text"
        onClick={evt => {
          evt.stopPropagation();
          onChange(listItem);
        }}
      >
        Delete
      </td>
    </>
  );
};

class TeamList extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      openDialog: false,
      floatingLabel: "",
      hint: "",
      teamError: "",
      teamName: "",
      dialogTitle: "",
      activeSecondTab: "tab22"
    };
    this.deleteTeam = this.deleteTeam.bind(this);
  }

  deleteTeam(teamName) {
    let teams = (this.props.teamList && this.props.teamList.data) || [];
    let newdata = [];
    _.map(teams, (vari, i) => {
      if (vari !== teamName) {
        newdata.push(vari);
      }
    });
    this.props.callSaveApi("delete", newdata);
  }

  toggleSecondTabs = tab => {
    if (this.state.activeSecondTab !== tab) {
      this.setState({
        activeSecondTab: tab
      });
    }
  };
  render() {
    let teams = [];
    if (
      this.props.teamList &&
      this.props.teamList.data &&
      this.props.teamList.data.length > 0
    ) {
      teams = this.props.teamList.data;
    }

    return (
      <div className="app-body" id="view">
        <div className="value-table-row ">
          <div className="">
            <div className="viewTeam-table table-condensed margin-t ">
              <GenericTable
                tableListData={teams}
                onChange={this.deleteTeam}
                list={list}
                headerData={[
                  "<span>Team Name</span>",
                  "Action"
                ]}
                isLoading={this.props.isLoading}
                col_Name={"team_page_table"}
              />
            </div>
          </div>
        </div>
      </div>
    );
  }
}
export default TeamList;
