import React from "react";
import "whatwg-fetch";
import GenericTable from "../../../components/generic/table/Table";

const list = (listItem, onChange, rowStyle, index, extraProps) => {
  return (
    <>
      <td>{listItem.name}</td>
      <td>{listItem.jobtitle}</td>
      <td>
         {listItem.salary_detail}
      </td>
      <td>
        {listItem.holdin_amt_detail.holding_amt === ""
          ? "Not Applicable"
          : listItem.holdin_amt_detail.holding_amt}
      </td>
      <td>{listItem.dateofjoining}</td>
      <td>{listItem.previous_increment}</td>
      <td>{listItem.no_of_days_join}</td>
    </>
  );
};

const TeamDetails = props => {
  return (
    <div>         
      <div className="viewTable-detail">
        <div className="detail-team-view">
          {props.team && (
            <GenericTable
              className="view-teamdetail-table"
              openPage={props.openPage}
              activeSecondTab={props.activeSecondTab}
              headerData={[
                "Employee Name",
                "Designation",
                "Total Salary",
                "Holding Amount Details",
                "Date of Joining",
                "Last Increment",
                "Duration"
              ]}
              tableListData={props.team.data}
              list={list}
              loggedUser={props.loggedUser}
              isLoading={props.isLoading}
              istable_head = {"table_heading"}
              istable_body = {"body_table"}
            />
          )}
        </div>
      </div>
    </div>
  );
};

export default TeamDetails;
