import React from "react";
import PropTypes from "prop-types";
import Message from "../../../components/generic/Message";
import map from "lodash/map";
import { Alert, Row } from "reactstrap";
import GenericTable from "../../../components/generic/table/Table";
import docImg from "../../../static/doc-image.png";
import Checkbox from "../../../components/generic/input/Checkbox";
import $ from "jquery";

$(document).ready(function() {
  doThisScript();
});

$(window).on("resize", function() {
  doThisScript();
});

function doThisScript() {
  if ($(window).width() <= "576") {
    $(".table-responsive").addClass("border-remove-table");
    $(".border-remove-table").removeClass("table-responsive");
  } else {
    $(".border-remove-table").addClass("table-responsive");
    $(".table-responsive").removeClass("border-remove-table");
  }
}

const list = (listItem, onChange, rowStyle, index, extraProps) => {
  const docId = listItem.name.replace(/ /g, "");
  let isReadStrip = listItem.read
    ? " is-read-document"
    : " is-not-read-document";
  return (
    <>
      <td className="policy-document-td" style={extraProps.style}>
          <Checkbox id="notifySalaryUpdateToEmp"
                  name="notifySalaryUpdateToEmp"
                  checked={listItem.read ? true : false}
                  onClick={() => extraProps.updateReadStatus(listItem, listItem.doc_id)}
                />
        <Row className="w-100">
          <div className="col-md-1 col-xs-11 col-sm-11">
            <a
              className={isReadStrip}
              href={listItem.link}
              target="_blank"
              rel="noopener noreferrer"
              onClick={() => extraProps.updateReadStatus(listItem)}
            >
              <div className="mobile_policy_view_doc">
                {" "}
                <img className="w-100 policy-document" src={docImg} alt="" />
                <strong className="view_only_mobile ">{listItem.name}</strong>
              </div>
            </a>
          </div>
          <div className="col-md-11 col-xs-11 col-sm-11">
            <h5 key={index} id={docId} className={"mb-2 " + isReadStrip}>
              <strong className="policy_document_name">{listItem.name}</strong>
            </h5>
            <a
              className={"isMobilePreview " + isReadStrip}
              href={listItem.link}
              target="_blank"
              rel="noopener noreferrer"
              onClick={() => extraProps.updateReadStatus(listItem)}
              style={{ fontSize: "12px" }}
            >
              {listItem.link}
            </a>
          </div>
        </Row>
      </td>
      {/* <td className={isReadStrip}>{listItem.updated_time ? moment(listItem.updated_time).format("MMM Do YYYY") : "Data not Found"}</td> */}
    </>
  );
  // }
};

class DocumentsList extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      errClass: "hidden",
      errMsg: "",
      policiesDocData:[]
    };
  }
  componentDidUpdate(prevProps){
    //When all mandatory docs are read by user
    if(!prevProps.isPolicyDocReadByUser && this.props.isPolicyDocReadByUser){
      this.props.history.push('/app/overview')
    }
  }
  hideError = e => {
    e.preventDefault();
    this.setState({
      errClass: "hidden",
      errMsg: ""
    });
  };
  updateReadStatus = (doc, doc_id) => {

    this.props.handleReadDoc(doc_id)
      

    let updateDoc = [];
    map(this.props.policyDocumentsData, (document, i) => {
      if (document.read !== 0) {
        updateDoc.push(document.name);
      }
    });
    updateDoc.push(doc.name);
    this.props.onUpdateReadStatus(updateDoc); 
  };

  render() {
    const policyDocData = this.props.isPolicyDocReadByUser 
     ? this.props.policyDocumentsData
     : this.props.unReadDocs;
    let pageHeading =
      this.props &&
      this.props.page_headings &&
      this.props.page_headings.find(
        o => o.reference.toLowerCase() == "policy documents"
      );
    return (
      <div className="policy-doc-table mt-3 " id="view">
        {pageHeading && pageHeading.value && pageHeading.value != "" ? (
          <Alert className="alert-transparent rem-fade mb-4" color="info">
            <strong className="pr-2">Info:</strong>
            <small>{pageHeading.value}</small>
          </Alert>
        ) : null}
        <div>
          <Message
            className={this.state.errClass}
            message={this.state.errMsg}
            onClick={this.hideError}
          />
        </div>
        <div id="rem-padding">
          {this.props.policyDocumentsData && (
            <GenericTable
              className={"mobile-view-table"}
              tableListData={policyDocData}
              headerData={["NAME"]}
              list={list}
              updateReadStatus={this.updateReadStatus}
              style={{ wordBreak: "break-all" }}
              widget_mobile={"laptop-padding"}
              isPolicyDocReadByUser={this.props.isPolicyDocReadByUser}
            />
          )}
        </div>
      </div>
    );
  }
}

DocumentsList.propTypes = {
  policyDocumentsData: PropTypes.array.isRequired,
  onUpdateReadStatus: PropTypes.func.isRequired
};

export default DocumentsList;
