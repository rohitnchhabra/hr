import React from "react";
import _ from "lodash";
import InputText from "../../../components/generic/input/InputText";

class FormUploadPolicyDocument extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      nameofdoc: "",
      linkofdoc: "",
      errName: "",
      errLink: "",
      mandatory:"",
      docType:""
    };
    this.submitDocs = this.submitDocs.bind(this);
  }
  submitDocs() {
    let name = this.state.nameofdoc.trim();
    let link = this.state.linkofdoc.trim();
    let state = true;
    let mandatory=this.state.mandatory;
    this.setState({
      errName: "",
      errLink: ""
    });
    if (state && _.isEmpty(name)) {
      state = false;
      this.setState({
        errName: "Please enter document name"
      });
    }
    if (state && _.isEmpty(link)) {
      state = false;
      this.setState({
        errLink: "Please enter document link"
      });
    }
    if (state) {
      let docs = this.props.docs;
      docs.push({ name: name, link: link, updated_time: new Date().getTime(),mandatory:mandatory });
      this.props.submitDocs(docs);
      this.setState({
        nameofdoc: "",
        linkofdoc: ""
      });
    }
  }

  docType=(e)=>{
    this.setState({
      docType:e.target.value
    })
    if(e.target.value=="0"){
      this.setState({
        mandatory:true
      })
    }
    else{
      this.setState({
        mandatory:false
      })
    }
  }
  render() {
    return (
      <div>
        <div className="">
          <form className="mb-0">
            <div className="row m-0">
              <div className="document_value_insert col-sm-4 p-0">
                <InputText
                  value={this.state.nameofdoc}
                  placeHolder="Name of doc"
                  onchange={e => {
                    this.setState({
                      nameofdoc: e.target.value
                    });
                  }}
                />
              </div>
              <div className="link_document_insert col-sm-4">
                <InputText
                  value={this.state.linkofdoc}
                  placeHolder="Link of doc"
                  onchange={e => {
                    this.setState({
                      linkofdoc: e.target.value
                    });
                  }}
                />
              </div>
              <div className="col-sm-2 pl-0">
                <div className="form-group">
                  <select
                    className="form-control"
                    onChange={e =>
                      this.docType(e)
                    }
                    value={this.state.docType}
                  >
                    <option value="">--select doc type--</option>
                    <option value="0">mandatory</option>
                    <option value="1">not mandatory</option>
                  </select>
                </div>
              </div>
              <div className="document_upload_submit col-sm-2 pl-0">
                <button
                  type="button"
                  class="btn btn-success pull-right btn-responsive w-100"
                  onClick={this.submitDocs}
                >
                  SUBMIT
                </button>
              </div>
            </div>
          </form>
        </div>
      </div>
    );
  }
}

export default FormUploadPolicyDocument;
