import React from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { withRouter } from "react-router";
import Header from "../../../components/generic/Header";
import { isNotUserValid } from "../../../services/generic";
import DocumentsList from "../../../modules/policyDocuments/components/DocumentsList";
import * as actions from "../../../redux/actions";
import Heading from "../../../components/generic/Heading";
import { Alert } from "reactstrap";

class PolicyDocumentContainer extends React.Component {

  constructor(props){
    super(props);
    this.state ={data:[], isUnreadDocs:true,unReadDocs:[]}
  }

  componentWillMount() {
    this.props.requestUserPolicyDocument();
    this.props.getGenricLoginConfig();
    this.props.showHeading("Policy Documents directory")
  }
  componentWillReceiveProps(props) {
    let isNotValid = isNotUserValid(
      this.props.location.pathname,
      props.loggedUser
    );
    if (isNotValid.status && isNotValid.redirectTo !== "policy_documents") {
      this.props.history.push(isNotValid.redirectTo);
    }
  }

  componentDidUpdate(props){
    if(this.props.policyDocuments.isSuccess !== props.policyDocuments.isSuccess){
      if(this.props.policyDocuments.isSuccess){
        let policyData = [], unReadDocs  = []
      let allDocs = this.props.policyDocuments.data &&
        this.props.policyDocuments.data.length > 0 &&
        [...this.props.policyDocuments.data].reverse()
        if(allDocs && allDocs.length){
          allDocs.forEach((element,index)=>{
            policyData.push({...element,doc_id:index+1})
          })
        }
        if(policyData && policyData.length){
           unReadDocs = policyData.filter(doc => !doc.read && doc.mandatory);
        }
        this.setState({data:policyData,unReadDocs})
      }
    }
  }

  handleReadDoc=(doc_id)=>{
    let data = [], unReadDocs = []
    const isPolicyDocReadByUser =
      this.props.loggedUser.data.is_policy_documents_read_by_user === 1;
      if(!isPolicyDocReadByUser){
        this.state.unReadDocs.forEach((element,i)=>{
          if(element.doc_id === doc_id){
            let a = element
            a.read =1 
            unReadDocs.push({...a})
          }else{
            unReadDocs.push(element)
          }
        })
        this.setState({unReadDocs});
      }else{
        this.state.data.forEach((element,i)=>{
          if(element.doc_id === doc_id){
            let a = element
            a.read =1 
            data.push({...a})
          }else{
            data.push(element)
          }
        })
        this.setState({data});
      }
  }

  render() {
    const isPolicyDocReadByUser =
      this.props.loggedUser.data.is_policy_documents_read_by_user === 1;
      const {data, unReadDocs} = this.state; 
    return (
      <div className="policy-documents-view">
        <Header
          pageTitle={"Policy Document"}
          showLoading={this.props.policyDocuments.isLoading}
        />
        {!isPolicyDocReadByUser && (
          <Alert className="alert-transparent rem-fade mb-4" color="info">
            You have some important unread policy documents, please click and
            read those documents to be able to use HR System. Once the documents
            are read you will be able to automatically use HRMS
          </Alert>
        )}
        <DocumentsList
          page_headings={this.props.page_headings}
          policyDocumentsData={data}
          onUpdateReadStatus={this.props.requestUpdateReadStatus}
          isPolicyDocReadByUser={isPolicyDocReadByUser}
          history={this.props.history}
          handleReadDoc={this.handleReadDoc}
          unReadDocs={unReadDocs}
        />
      </div>
    );
  }
}
function mapStateToProps(state) {
  return {
    loggedUser: state.logged_user.userLogin,
    policyDocuments: state.policyDocuments.policyDocument,
    page_headings: state.settings.page_headings
  };
}
const mapDispatchToProps = dispatch => {
  return bindActionCreators(actions, dispatch);
};

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(PolicyDocumentContainer)
);
