import React from 'react';
import {connect} from 'react-redux';
import {withRouter} from 'react-router';
import {bindActionCreators} from 'redux';
import {isNotUserValid} from '../../../services/generic';
import Message from '../../../components/generic/Message';
import FormUploadPolicyDocument from '../../../modules/policyDocuments/components/formUploadPolicyDocument';
import ListAllPolicyDocument from '../../../components/policyDocuments/ListAllPolicyDocument';
import * as actions from '../../../redux/actions';
import Heading from '../../../components/generic/Heading';
import { Alert } from "reactstrap";

class UploadPolicyDocumentContainer extends React.Component {
  constructor (props) {
    super(props);
    this.state = {
      docs:     [],
      errClass: 'hidden',
      errMsg:   '',
      visible:false
    };
    this.submitDocs = this.submitDocs.bind(this);
    this.hideError = this.hideError.bind(this);
    this.submitNewListofDocs = this.submitNewListofDocs.bind(this);
  }
  componentWillMount () {
    this.props.requestUserPolicyDocument();
    this.props.showHeading("Upload Policy Documents")
  }
  componentWillReceiveProps (props) {
    let isNotValid = isNotUserValid(this.props.location.pathname, props.loggedUser);
    if (isNotValid.status) {
      this.props.history.push(isNotValid.redirectTo);
    }
    this.setState({
      docs: props.policyDocuments.data
    });

  }
  hideError (e) {
    e.preventDefault();
    this.setState({
      errClass: 'hidden',
      errMsg:   ''
    });
  }
  submitNewListofDocs (newList) {
    this.props.requestSubmitDocs(newList);
  }
  submitDocs (docs) {
    this.props.requestSubmitDocs(docs);
    this.setState({visible:true},()=>{
      window.setTimeout(()=>{
        this.setState({visible:false})
      },5000)
    });
  }
  render () {
    return (
      <div className="upload-policy-documents-wrapper">
        {this.state.visible ?
        <Alert
              className="alert-transparent rem-fade rh-section rh-alert"
              color="success"
            >
            <span>Succesfully Uploaded Policy Document.</span>
            </Alert>: ""}
        <div id="content" className="app-content box-shadow-z0" role="main">
          <div className="app-body" id="view">
            <div className="row">
              <Message className={this.state.errClass} message={this.state.errMsg} onClick={this.hideError} />
            </div>
              <div className="upload_doc_file mt-4">
              <div className="form_to_uploaddoc" id="submitDocs">
                <FormUploadPolicyDocument submitDocs={this.submitDocs} docs={this.state.docs} {...this.props} />
              </div>
              <br />
              <div className="upload_list_doc" id="listDocs">
                <ListAllPolicyDocument policyDocuments={this.state.docs} submitNewListofDocs={this.submitNewListofDocs} />
              </div>
              </div>

          </div>
        </div>
      </div>
    );
  }
}
function mapStateToProps (state) {
  return {
    loggedUser:      state.logged_user.userLogin,
    policyDocuments: state.policyDocuments.policyDocument
  };
}

const mapDispatchToProps = (dispatch) => { return bindActionCreators(actions, dispatch); };

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(UploadPolicyDocumentContainer));
