import React, { Component } from "react";
import { connect } from "react-redux";
import { Button } from "reactstrap";
import axios from "axios";
import ImageCompressor from "image-compressor.js";
import { qualityValue } from "../../helper/helper";
import { CONFIG } from "../../config/index";
import {Spinner} from "reactstrap";

class UploadMultipleFiles extends Component {
  constructor(props) {
    super(props);
    this.state = {
      filesToUpload: [],
      arr: [],
      len: 0,
      loading: false 
    };
    this.files = [];
  }

  componentDidUpdate(prevProps, prevState) {
    if (
      JSON.stringify(this.props.fileParams) !==
      JSON.stringify(prevProps.fileParams)
    ) {
      this.setState({
        filesToUpload: this.props.fileParams
      });
    }
    if (
      prevState.len !== this.state.len &&
      this.state.filesToUpload.file &&
      this.state.filesToUpload.file.length == this.state.len
    ) {
      this.setState({loading: false});
      let fileArray = this.files;
      const url = CONFIG[`${this.props.url}`];
      let formData = new FormData();
      for (let key in this.state.filesToUpload) {
        formData.append(key, this.state.filesToUpload[key]);
      }
    
      formData.delete("imageUrl");
      formData.append("submit", "Upload");
      fileArray.forEach((element, i) => {
        formData.append("link" + i, element, element.name);
      });
      axios
        .post(url, formData)
        .then(data => {
          this.props.successfulFileUpload();
        })
        .catch(err => {});
    }
  }

  uploadFile = e => {
    e.preventDefault();
    let fileArray = this.state.filesToUpload.file;
    if (fileArray.length == 0) {
      return;
    } else {
      this.setState({loading: true});
      fileArray.forEach((element, i) => {
        if (
          !element.type.includes("image") ||
          (element.type.includes("image") && element.size < 500000)
        ) {
          this.files.push(element);
          this.setState((prev) =>({len: prev.len + 1}));
        } else {
          let quality = qualityValue(element);
          let imageCompressor = new ImageCompressor();
          imageCompressor
            .compress(element, {
              quality: quality
            })
            .then(compressedFile => {
              this.files.push(compressedFile);
              this.setState((prev) =>({len: prev.len + 1}));
            });
        }
      });
    }
  };

  render() {
    return (
      <>
        {this.state.filesToUpload &&
          this.state.filesToUpload.file &&
          this.state.filesToUpload.file.length > 0 && (
            <>
              {this.state.filesToUpload.file.map((file, i) => (
                <div>
                  <span>{file.name}</span>
                  <span
                    onClick={() => this.props.handleRemoveImage(i)}
                    className="remove-image-icon"
                  >
                    X
                  </span>
                </div>
              ))}
              <form onSubmit={e => this.uploadFile(e)}>
                <Button
                  className="px-6 px-5"
                  color="success"
                  type="submit"
                  onClick={e => this.uploadFile(e)}
                  disabled={this.state.loading}
                >
                  { !this.state.loading ?
                    this.state.filesToUpload.file.length > 1
                      ? "Upload All"
                      : "Upload"
                    : <Spinner color="white" />
                  }
                </Button>
              </form>
            </>
          )}
      </>
    );
  }
}
const mapStateToProps = state => ({
  _loading: state.uploadImage.loading,
  loggedUser: state.logged_user.userLogin
});
export default connect(mapStateToProps)(UploadMultipleFiles);
