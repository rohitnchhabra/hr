import React, { Component } from "react";
import { connect } from "react-redux";
import { Button } from "reactstrap";
import axios from "axios";
import ImageCompressor from "image-compressor.js";
import { qualityValue } from "../../helper/helper";
import { CONFIG } from "../../config/index";
import { notify } from "../../services/notify";
import PropTypes from "prop-types";
import isEqual from "lodash/isEqual";
class UploadImageComp extends Component {
  constructor(props) {
    super(props);
    this.state = {
      filesToUpload: []
    };
  }
  static propTypes = {
    /**Function for validating documents */
    validateDocuments: PropTypes.func.isRequired,
    /**Object which includes file, document_type and other information as required */
    fileParams: PropTypes.shape({
      document_type: PropTypes.string.isRequired,
      file: PropTypes.array.isRequired
    }).isRequired,
    /**Function to be called on successful file upload */
    onSuccessfulFileUpload: PropTypes.func,
    /**Url for uploading files */
    url: PropTypes.string.isRequired,
    showSuccessMessage: PropTypes.func
  };

  static defaultProps = {
    onSuccessfulFileUpload: () => {},
    showSuccessMessage: () => {},
    handleClose: () => {}
  };

  componentDidUpdate(prevProps) {
    const { file } = this.props.fileParams;
    if (
      this.props.fileParams.document_type &&
      !isEqual(prevProps.fileParams, this.props.fileParams) &&
      file.length > 0
    ) {
      if (this.props.hasOwnProperty("handleRemoveSelectedDocs")) {
        this.props.handleRemoveSelectedDocs(this.state.filesToUpload, "remove");
      }
      if (
        this.state.filesToUpload.findIndex(data => {
          return data.file[0].name === file[0].name;
        }) < 0
      ) {
        this.setState({
          filesToUpload: [
            ...this.state.filesToUpload,
            {
              ...this.props.fileParams
            }
          ]
        });
      }
    }

    if (
      this.props.fileParams.file.length &&
      (prevProps.fileParams.file[0] === undefined ||
        prevProps.fileParams.file[0].name !==
          this.props.fileParams.file[0].name)
    ) {
      this.setState({
        filesToUpload: this.state.filesToUpload.map(file =>
          file.document_type === this.props.fileParams.document_type
            ? {
                ...file,
                loading: false,
                success: false,
                compressionMessage: "",
                file: this.props.fileParams.file
              }
            : file
        )
      });
    }

    if (this.state.filesToUpload) {
      let array = this.state.filesToUpload;
      array.map((files, index) => {
        if (files.success === true) {
          array.splice(index, 1);
          this.setState({
            filesToUpload: array
          });
        }
      });
    }

    // const allUploaded
  }

  handlerClear = e => {
    this.props.validateDocuments && this.props.validateDocuments(e);
  };

  handleSubmit = e => {
    e.preventDefault();
    for (let i = 0; i < this.state.filesToUpload.length; i++) {
      if (
        // i <= index &&
        !this.state.filesToUpload[i].success &&
        !this.state.filesToUpload[i].loading
      ) {
        setTimeout(() => this.uploadFile(this.state.filesToUpload[i]), 50);
      }
    }
  };

  uploadFile = fileParams => {
    const { file: fileArray } = fileParams;
    const file = fileArray[0];
    const fileName = "link_1"; //the name must be same as in backend
    const url = CONFIG[`${this.props.url}`];
    if (!file) {
      return;
    } else if (
      !file.type.includes("image") ||
      (file.type.includes("image") && file.size < 500000) || 
      this.props.notCompressForTesting
    ) {
      this.setState({
        filesToUpload: this.state.filesToUpload.map(f =>
          f.document_type === fileParams.document_type
            ? {
                ...f,
                loading: true,
                compressionMessage: `File Size ${(file.size / 1000000).toFixed(
                  2
                )} mb`
              }
            : f
        )
      });
      const formData = new FormData();

      for (let key in fileParams) {
        formData.append(key, fileParams[key]);
      }
      formData.delete("imageUrl");
      fileArray.forEach((element,i) => {
        formData.append("link"+(i+1),element);
      });
      formData.append("submit", "Upload");
      if (this.props.isLoading) {
        this.props.isLoading();
      }
      axios
        .post(url, formData)
        .then(data => {
          // notify("Success !", `File uploaded successfully`, "success");
          this.setState({
            filesToUpload: []
          });
          this.props.handleClose();
          this.props.showSuccessMessage();
          this.props.onSuccessfulFileUpload();
          // {this.props.uploaed_pic ? ()=>{this.props.uploaed_pic(fileParams.document_type)}:''}
          this.props.uploaed_pic &&
            this.props.uploaed_pic(fileParams.document_type);
          this.props.remove_type &&
            this.props.remove_type(fileParams.document_type);
        })
        .catch(error => {
          if (error.request && error.request.status === 413) {
            if (this.props.isError) {
              this.props.isError();
            }
            notify("Error", "File too large to upload", "error");
            this.setState({
              filesToUpload: this.state.filesToUpload.map(file =>
                file.document_type === fileParams.document_type
                  ? { ...file, loading: false, success: false }
                  : file
              )
            });
          } else {
            if (error) {
              if (this.props.isError) {
                this.props.isError();
              }
              notify(
                "Error",
                error.request && error.request.statusText,
                "error"
              );
              this.setState({
                filesToUpload: this.state.filesToUpload.map(file =>
                  file.document_type === fileParams.document_type
                    ? { ...file, loading: false, success: false }
                    : file
                )
              });
            }
          }
        });
    } else {
      let quality = qualityValue(file);
      let imageCompressor = new ImageCompressor();
      imageCompressor
        .compress(file, {
          quality: quality
        })
        .then(compressedFile => {
          this.setState({
            filesToUpload: this.state.filesToUpload.map(f =>
              f.document_type === fileParams.document_type
                ? {
                    ...f,
                    loading: true,
                    compressionMessage: `File Size ${(
                      file.size / 1000000
                    ).toFixed(2)} mb, compressed to ${(
                      compressedFile.size / 1000000
                    ).toFixed(2)}mb`
                  }
                : f
            )
          });
          const formData = new FormData();
          for (let key in fileParams) {
            formData.append(key, fileParams[key]);
          }
          formData.delete("imageUrl"); //IMPORTANT: it is the image in ImagePreview
          formData.append(fileName, compressedFile, file.name);
          formData.append("submit", "Upload");
          if (this.props.isLoading) {
            this.props.isLoading();
          }
          axios
            .post(url, formData)
            .then(data => {
              if (data.data.error === 0) {
                // notify("Success !", `File uploaded successfully`, "success");
                this.setState({
                  filesToUpload: []
                });
                this.props.handleClose();
                this.props.showSuccessMessage();
                // this.props.onFileCompression(null);
                this.props.onSuccessfulFileUpload();
              } else if (data.data.error === 1) {
                notify("Error !", data.data.message, "error");
                this.setState({
                  filesToUpload: this.state.filesToUpload.map(file =>
                    file.document_type === fileParams.document_type
                      ? { ...file, loading: false, success: false }
                      : file
                  )
                });
              }
            })
            .catch(error => {
              if (this.props.isError) {
                this.props.isError();
              }
              if (error.request && error.request.status === 413) {
                notify("Error", "File too large to upload", "error");
                this.setState({
                  filesToUpload: this.state.filesToUpload.map(file =>
                    file.document_type === fileParams.document_type
                      ? { ...file, loading: false, success: false }
                      : file
                  )
                });
              } else {
                if (this.props.isError) {
                  this.props.isError();
                }
                notify(
                  "Error",
                  error.request && error.request.statusText,
                  "error"
                );
                this.setState({
                  filesToUpload: this.state.filesToUpload.map(file =>
                    file.document_type === fileParams.document_type
                      ? { ...file, loading: false, success: false }
                      : file
                  )
                });
              }
            });
        });
    }
  };

  handleRomoveImage = (index, documentType) => {
    let filesToUpload = this.state.filesToUpload.filter((a, i) => i != index);
    if (this.props.hasOwnProperty("handleRemoveSelectedDocs")) {
      this.props.handleRemoveSelectedDocs(documentType, "add");
    }
    if (index == this.state.filesToUpload.length - 1) {
      this.props.handleRomoveImage();
    }
    this.setState({ filesToUpload });
  };

  render() {
    return (
      <>
        {this.state.filesToUpload.map((files, index) => (
          <>
            {/* {files.document_type && files.file && ( */}
              <div>
                {files.loading ? (
                  <>
                    <div> {files.compressionMessage}</div>
                    <div>Uploading {files.document_type}...</div>
                  </>
                ) : (
                  <div className="m-t-5">
                    {files.successMsg ? (
                      `Document ${files.document_type} uploaded successfully`
                    ) : (
                      <span>
                        {files.document_type === "inventory_invoice"
                          ? "Invoice"
                          : files.document_type === "inventory_warranty"
                          ? "Warranty"
                          : files.document_type === "inventory_photo"
                          ? "Photo"
                          : files.document_type}{" "}
                        - {files.file[0].name}
                        <span
                          onClick={() =>
                            this.handleRomoveImage(index, files.document_type)
                          }
                          className="remove-image-icon"
                        >
                          X
                        </span>
                      </span>
                    )}
                  </div>
                )}
              </div>
            {/* )} */}
          </>
        ))}
        {this.state.filesToUpload.length > 0 && (
          <form
            onSubmit={
              this.props.handleSubmit
                ? this.props.handleSubmit
                : e => this.handleSubmit(e)
            }
          >
            <Button
              className="px-6 px-5"
              color="success"
              type="submit"
              onClick={e => this.handlerClear(e)}
            >
              {this.state.filesToUpload.length > 1 ? "Upload All" : "Upload"}
            </Button>
          </form>
        )}
      </>
    );
  }
}
const mapStateToProps = state => ({
  _loading: state.uploadImage.loading,
  loggedUser: state.logged_user.userLogin
});
export default connect(mapStateToProps)(UploadImageComp);
