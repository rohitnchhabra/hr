import React from 'react';
import {connect} from 'react-redux';
import {withRouter} from 'react-router';
import {isNotUserValid} from '../../../services/generic';
import Header from '../../../components/generic/Header';
import FormMyDocuments from '../../../modules/myDocuments/components/FormMyDocuments';
import * as actionsMyDocument from '../../../redux/myDocuments/actions/myDocument';
import * as actionsManageDevice from "../../../redux/inventory/actions/inventory";
import * as actionsUploadFile from "../../../redux/uploadImageComp/actions/uploadImageComp";
import * as actions from '../../../redux/actions';
import Heading from '../../../components/generic/Heading';

class MyDocuments extends React.Component {
  constructor (props) {
    super(props);
    this.state = {
      my_document: [],
      message:     ''
    };
  }
  componentWillMount () {
    this.props.onGetMyDocuments();
    this.props.showHeading("My Documents")
  }
  componentWillReceiveProps (props) {
    window.scrollTo(0, 0);
    let isNotValid = isNotUserValid(this.props.location.pathname, props.loggedUser);
    if (isNotValid.status) {
      this.props.history.push(isNotValid.redirectTo);
    }
    this.setState({
      my_document: props.myDocuments.my_document,
      message:     props.myDocuments.status_message
    });
  }
  render () {
    return (
      <div>
        <div id="content" className="app-content box-shadow-z0" role="main">
          <Header pageTitle={'My Document'} {...this.props} />
          <div className="app-body" id="view">
            {/* <div className="padding"> */}
              <FormMyDocuments my_documents={this.state.my_document} user_id={this.props.loggedUser.data.id} callUpdateDocuments={this.props.onUpdatedocuments} {...this.props} />
            {/* </div> */}
          </div>
        </div>
      </div>
    );
  }
}

function mapStateToProps (state) {
  return {
    frontend:    state.frontend.toJS(),
    loggedUser:  state.logged_user.userLogin,
    myProfile:   state.myProfile.toJS(),
    myDocuments: state.myDocument.toJS()
  };
}
const mapDispatchToProps = (dispatch) => {
  return {
    onGetMyDocuments: () => {
      return dispatch(actionsMyDocument.getMyDocument());
    },
    onDeleteDocument: (docId) => {
      return dispatch(actionsMyDocument.deleteDocument(docId));
    },
    onFetchDevice: () => {
      return dispatch(actionsManageDevice.get_machines_detail());
    },
    onFetchUnapprovedUser: () => {
      return dispatch(actionsManageDevice.unapprovedUser());
    },
    onUploadFile: (formData, url) => {
      return dispatch(actionsUploadFile.uploadFile(formData, url));
    },
    showHeading:(data)=>{
      return dispatch(actions.showHeading(data));
    }
  };
};

const VisibleMyDocuments = connect(
  mapStateToProps,
  mapDispatchToProps
)(MyDocuments);

const RouterVisibleMyDocuments = withRouter(VisibleMyDocuments);

export default RouterVisibleMyDocuments;
