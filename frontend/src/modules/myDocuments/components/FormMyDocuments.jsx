import React from "react";
import { CONFIG } from "../../../config/index";
import { notify } from "../../../services/notify";
import { getToken } from "../../../services/generic";
import ListDocuments from "../../../components/myDocuments/ListDocuments";
import UploadImageComp from "../../uploadImageCompressed/UploadImageComp";
import Dropzone from "react-dropzone";
import { Table, Alert } from "reactstrap";
import AlertFader from "../../../components/generic/AlertFader";
import Heading from "../../../components/generic/Heading";
import moment from "moment";
import { docs_list } from "../../../helper/helper";
import _ from "lodash"

class FormMyDocuments extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      document_type: "",
      token: "",
      file: [],
      imageUrl: "",
      successMsg: "",
      documents_list:docs_list
    };
    this.dropzoneRef = React.createRef();
  }

  componentWillReceiveProps(props) {
    this.setState({
      token: getToken()
    });
  }
  handleClose = () => {
    this.setState({ open: false });
  };

  onDrop = files => {
    this.setState({
      file: files,
      type:
        files[0].name &&
        files[0].name.split(".")[files[0].name.split(".").length - 1]
    });
    const reader = new FileReader();

    reader.onloadend = () => {
      this.setState({
        imageUrl: reader.result
      });
    };
    if (files) {
      reader.readAsDataURL(files[0]);
      this.setState({
        imageUrl: reader.result
      });
    } else {
      this.setState({
        imageUrl: ""
      });
    }
  };
  handleOpen = () => {
    this.setState({ open: true });
  };
  callUpdateDocuments = e => {
    let type = this.state.document_type;
    let stop = false;
    if (type === "") {
      stop = true;
      notify("Warning!", "Please select document type.", "warning");
    } else if (!this.state.imageUrl) {
      stop = true;
      notify("Warning!", "Please select a file", "warning");
    } else if (this.refs.declear.checked !== true) {
      stop = true;
      notify("Warning!", "Mark declaration before submit", "warning");
    }

    if (stop) {
      e.preventDefault();
    }
  };
  deleteDocument = docId => {
    this.props
      .onDeleteDocument(docId)
      .then(msg => {
        this.props.onGetMydocuments();
        notify("Success!", msg.toString(), "success");
      })
      .catch(err => {
        notify("Error!", err.toString(), "error");
      });
  };

  successfulFileUpload = () => {
    this.setState({
      imageUrl: "",
      file: [],
      document_type: ""
    });
    this.refs.declear.checked = false;
  };

  errorFileUpload = () => {
    this.setState({
      uploadMessage: ""
    });
  };

  onSuccessfulFileUpload = () => {
    this.successfulFileUpload();
    if (this.props.loggedUser.data.role === "Admin") {
      this.props.onFetchDevice();
      this.props.onFetchUnapprovedUser();
      this.props.onGetMyDocuments();
    } else if (this.props.loggedUser.data.role === "HR") {
      return;
      // this.props.onFetchDevice();
    } else {
      this.props.onGetMyDocuments();
    }
  };

  showSuccessMessage = () => {
    const { document_type } = this.state;
    this.setState(
      { successMsg: `${document_type} has been uploaded successfully` },
      () => {
        this.setState({ successMsg: "" });
      }
    );
  };

  handleRomoveImage=()=>{
    this.setState({ imageUrl: "",file:[]})
  }

  handleRemoveSelectedDocs= (fileUploaded, type)=>{
    if(type === "remove"){
      let documents_list =[];
      if(fileUploaded.length){
              this.state.documents_list.forEach((files,i)=>{
              if(fileUploaded[fileUploaded.length-1].document_type !== files.name ){
                documents_list.push(files)
              }
          })
        this.setState({documents_list});
      }
    }else{
      let documents_list = this.state.documents_list;
      documents_list.push({name:fileUploaded, value:fileUploaded});
      this.setState({documents_list});
    }
  }

  render() {
    const { successMsg, documents_list } = this.state;    
    return (
      <>
        <div className="row no-gutter p-t-md">
          <div className="col-sm-8 pl-0">
            <Heading subHeading title="Upload" boldText="New Documents" />
            <AlertFader>
              {successMsg && (
                <Alert
                  fade={false}
                  className={"upload_alert_notify mr-3"}
                  color={"success"}
                >
                  {successMsg}
                </Alert>
              )}
            </AlertFader>
            <div className="box p-a-md m-b-lg mr-3 form-my-doc" id="uploadDoc">
              <Alert color="info" fade={false}>
                If you need to upload back side of the document, you can upload it by first uploading the front side saving it and then again select the same document time and upload it again.
                <br />
                You can also upload it by selecting "Other Documents"
              </Alert>
              <form
                action={CONFIG.upload_url}
                method="POST"
                encType="multipart/form-data"
              >
                <div className="form-group  d-flex form_mobile_document">
                  <label className="col-sm-3 form_document_label">
                    Document Type
                  </label>
                  <select
                    className="upload_doc_list form-control col-sm-9 pl-2"
                    ref="document_type"
                    onChange={() => {
                      this.setState({
                        document_type: this.refs.document_type.value,
                        file: [],
                        imageUrl: ""
                      });
                      // this.refs.file.value = "";
                    }}
                    value={this.state.document_type}
                  >
                    <option value="">--- Select Doc Type ---</option>
                    {documents_list.map((v, i) => (
                      <option value={v.value}>{v.name}</option>
                    ))}
                  </select>
                </div>
                <input
                  type="hidden"
                  name="document_type"
                  value={this.state.document_type}
                />
                <div className="form-group d-flex form_mobile_document">
                  <label className="col-sm-3 form_document_label_type">
                    Upload Document{" "}
                  </label>
                  <Dropzone onDrop={this.onDrop}>
                    {({ getRootProps, getInputProps }) => {
                      return (
                        <section className="container m-0 p-0 col-sm-9">
                          {/* {this.state.imageUrl && 
                              <div onClick={this.handleRomoveImage} className="remove-image-icon">X</div>
                          } */}
                          <div {...getRootProps({ className: "dropzone" })}>
                            <input {...getInputProps()} />

                            <div className="drag_and_drop align-items-center">
                              {this.state.file &&
                                this.state.file.map((file, index) => (
                                  <div className="font-weight-bold">
                                    {" "}
                                    {file.name}
                                  </div>
                                ))}
                            
                              {this.state.imageUrl ?this.state.type==="pdf"?
                        
                          // <embed src={this.state.imageUrl} style={{width:"100%",height:"100%"}} ></embed>
                          <iframe src={this.state.imageUrl} style={{width:"200px"}} frameborder="0"></iframe>

                              : (
                                <img
                                  src={this.state.imageUrl}
                                  className="mx-3 img-preview"
                                  alt="Preview"
                                />
                              ) : (
                                <p className="uploading_doc">
                                  <i className="fi flaticon-upload" />
                                </p>
                              )}

                              <p className="doc_upload_place">
                                Drop a document here or click to select file to
                                upload
                              </p>
                            </div>
                          </div>
                        </section>
                      );
                    }}
                  </Dropzone>
                </div>
                <div className="form-group offset-sm-3 ">
                  <input
                    type="checkbox"
                    ref="declear"
                    className="vertical-middle t-scale-1"
                  />
                  <span className="declaration text-danger font-weight-bold">
                    <small className="declare_statement">
                      By uploading this document you certify that these document
                      are true and all information is correct
                    </small>
                  </span>
                </div>
                <div className="form-group offset-sm-3">
                  <UploadImageComp
                    validateDocuments={this.callUpdateDocuments}
                    fileParams={{
                      ...this.state,
                      user_id: this.props.user_id,
                      file_upload_action: "user_document"
                    }}
                    onSuccessfulFileUpload={this.onSuccessfulFileUpload}
                    url={"generic_upload_url"}
                    showSuccessMessage={this.showSuccessMessage}
                    handleRomoveImage={this.handleRomoveImage}
                    handleRemoveSelectedDocs={this.handleRemoveSelectedDocs}
                  />
                </div>
              </form>
            </div>
          </div>
          <ListDocuments
            myDocuments={this.props.my_documents}
            deleteDocument={this.deleteDocument}
            docs_list={docs_list}
          />
        </div>
        <div className="row no-gutter">
          <Heading subHeading title="Document" boldText="Directory" />
          <Table className="bg-white">
            <thead>
              <tr className="upload_doc_title my-4">
                <th className="hidden-sm-down">DOCUMENT VIEW</th>
                <th className="hidden-sm-down">TYPE</th>
                <th className="hidden-sm-down">LAST MODIFIED</th>
              </tr>
            </thead>
            <tbody className="upload_doc_detail">
              {this.props.my_documents.length ? (
                this.props.my_documents.map((v, i) => (
                  <tr key={i}>
                    <td>
                      <div>
                        <iframe
                          src={`${v.doc_link}`}
                          className="image_doc_view"
                          title={v.document_type}
                        />
                      </div>
                    </td>
                    <td className="list_doc_file">{v.document_type}</td>
                    <td className="list_doc_file">
                      {moment(v.last_modified).format("Do MMMM YYYY")}
                    </td>
                  </tr>
                ))
              ) : (
                <tr>
                  <td className="text-center" colSpan={3}>
                    No Available Data
                  </td>
                </tr>
              )}
            </tbody>
          </Table>
        </div>
      </>
    );
  }
}

export default FormMyDocuments;
