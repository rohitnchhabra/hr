import React, { Component } from "react";
import { CONFIG } from "../../../config/index";
import { notify , confirm } from "../../../services/notify";
import { getToken } from "../../../services/generic";
import * as actions from "../../../redux/actions";
import Header from "../../../components/generic/Header";
import Heading from "../../../components/generic/Heading";
import UploadImageComp from "../../uploadImageCompressed/UploadImageComp";
import { connect } from "react-redux";
import { withRouter } from "react-router";
import FlatButton from "material-ui/FlatButton";
import { Alert } from "reactstrap";
import Dropzone from "react-dropzone";
import { docs_list } from "../../../helper/helper";
import GenericTable from "../../../components/generic/table/Table";
import AlertFader from "../../../components/generic/AlertFader";


const list = (listItem, onChange, rowStyle, index, extraProps) => {
  return (
    <>
        <td>
          {listItem.doc_link.includes("drive.google.com") ? 
        <iframe
                    src={listItem.doc_link}
                  /> :
                  <a href={listItem.doc_link} target="_blank">
                  <img className="table_image_preview" src = {listItem.doc_link}></img></a>}
        </td>
        <td> 
          {listItem.document_type}
        </td>
        <td>
          <a className="text-danger" onClick={e => {
              confirm(
                "Are you sure ?",
                "Do you want to delete this Document ?",
                "warning"
              ).then(res => {
                if (res) {
                  extraProps.deleteDoc(listItem.id, listItem.user_Id);
                }
              });
            }} > Delete </a>
        </td>
        </>

  )}
class FormAddDocument extends Component {
  constructor(props) {
    super(props);
    this.state = {
      document_type: "",
      token: "",
      file: [],
      imageUrl: "",
      successMsg: "",
      documents_list:docs_list
    };
    this.dropzoneRef = React.createRef();
  }
  callUpdateDocuments = e => {
    let type = this.state.document_type;
    let stop = false;
    if (type === "") {
      stop = true;
      notify("Warning!", "Please select document type.", "warning");
    } else if (!this.state.imageUrl) {
      stop = true;
      notify("Warning!", "Please select a file", "warning");
    } else if (this.refs.declear.checked !== true) {
      stop = true;
      notify("Warning!", "Mark declaration before submit", "warning");
    }

    if (stop) {
      e.preventDefault();
    }
  };

  componentWillMount(props) {
    this.setState({
      token: getToken()
    });
  }
  componentDidUpdate(prevProps) {
    if (
      prevProps.user_profile_detail.user_Id !==
      this.props.user_profile_detail.user_Id
    ) {
      this.user_id = this.props.user_profile_detail.user_Id;
    }
  }
  handleFileChange = e => {
    this.setState({ file: Array.from(e.target.files) });
    const file = this.refs.file.files[0];
    const reader = new FileReader();

    reader.onloadend = () => {
      this.setState({
        imageUrl: reader.result
      });
    };
    if (file) {
      reader.readAsDataURL(file);
      this.setState({
        imageUrl: reader.result
      });
    } else {
      this.setState({
        imageUrl: ""
      });
    }
  };

  clearForm = () => {
    this.setState({ document_type: "", file: [], imageUrl: "" });
  };

  onDrop = files => {
    this.setState({
      file: files,
      type:
        files[0].name &&
        files[0].name.split(".")[files[0].name.split(".").length - 1]
    });
    const reader = new FileReader();

    reader.onloadend = () => {
      this.setState({
        imageUrl: reader.result
      });
    };
    if (files) {
      reader.readAsDataURL(files[0]);
      this.setState({
        imageUrl: reader.result
      });
    } else {
      this.setState({
        imageUrl: ""
      });
    }
  };
  successfulFileUpload = () => {
    this.setState({
      imageUrl: "",
      file: [],
      document_type: ""
    });
    this.refs.declear.checked = false;
    this.props.onGetUserDocument(this.user_id);
  };

  showSuccessMessage = () => {
    const { document_type } = this.state;
    this.setState(
      { successMsg: `${document_type} has been uploaded successfully` },
      () => {
        this.setState({ successMsg: "" });
      }
    );
  };

  handleRomoveImage=()=>{
    this.setState({ imageUrl: "",file:[],document_type:""})
  }

  handleRemoveSelectedDocs= (fileUploaded, type)=>{
    if(type === "remove"){
      let documents_list =[];
      if(fileUploaded.length){
              this.state.documents_list.forEach((files,i)=>{
              if(fileUploaded[fileUploaded.length-1].document_type !== files.name ){
                documents_list.push(files)
              }
          })
        this.setState({documents_list});
      }
    }else{
      let documents_list = this.state.documents_list;
      documents_list.push({name:fileUploaded, value:fileUploaded});
      this.setState({documents_list});
    }
  }

  render() {
    const { successMsg, documents_list } = this.state;
    return (
      <div>
        <div id="content" className="app-content box-shadow-z0" role="main">
          <Header
            pageTitle={"Add Documents"}
            showLoading={this.props.frontend.show_loading}
          />
          <AlertFader>
            {successMsg && (
              <Alert
                fade={false}
                className={"upload_alert_notify mr-3"}
                color={"success"}
              >
                {successMsg}
              </Alert>
            )}
          </AlertFader>
          <Alert color="info" fade={false}>
            You can upload multiple documents together{" "}
          </Alert>
          <div className="mychanges">
            <div
              className="box p-a-md m-b-lg form-my-doc mt"
              id="uploadDoc"
            >
              <form
                action={CONFIG.upload_url}
                method="POST"
                encType="multipart/form-data"
              >
                <div className="form-group  d-flex flex-wrap">
                  <label className="col-lg-3 col-sm-3 col-12 doc-label">
                    Document Type
                  </label>
                  <select
                    className="upload_doc_list form-control col-lg-9 col-sm-9 col-12 pl-2"
                    ref="document_type"
                    onChange={() => {
                      this.setState({
                        document_type: this.refs.document_type.value,
                        file: [],
                        imageUrl: ""
                      });
                    }}
                    value={this.state.document_type}
                  >
                    <option value="">--- Select Doc Type ---</option>
                    {documents_list.map((v, i) => (
                      <option value={v.value}>{v.name}</option>
                    ))}
                  </select>
                </div>
                <input
                  type="hidden"
                  name="document_type"
                  value={this.state.document_type}
                />
                <div className="form-group d-flex flex-wrap">
                  <label className="col-lg-3 col-sm-3 col-12 doc-label">
                    Upload Document{" "}
                  </label>
                  <Dropzone onDrop={this.onDrop}>
                    {({ getRootProps, getInputProps }) => {
                      return (
                        <section className="container m-0 p-0 col-sm-9 col-12 col-lg-9">
                          <div {...getRootProps({ className: "dropzone" })}>
                            <input {...getInputProps()} />
                            <div className="drag_and_drop align-items-center">
                              {this.state.file &&
                                this.state.file.map((file, index) => (
                                  <div className="font-weight-bold">
                                    {" "}
                                    {file.name}
                                  </div>
                                ))}
                              {this.state.imageUrl ? (
                                <img
                                  src={this.state.imageUrl}
                                  className="mx-3 img-fluid profile-doc-preview"
                                  alt="Preview"
                                />
                              ) : (
                                <p className="uploading_doc">
                                  <i className="fi flaticon-upload" />
                                </p>
                              )}

                              <p className="doc_upload_place">
                                Drop a document here or click to select file to
                                upload
                              </p>
                            </div>
                          </div>
                        </section>
                      );
                    }}
                  </Dropzone>
                </div>
                <div className="form-group offset-lg-3 offset-sm-3">
                  <input
                    type="checkbox"
                    ref="declear"
                    className="vertical-middle t-scale-1"
                  />
                  <span className="declaration text-danger font-weight-bold">
                    <small className="declare_statement">
                      By uploading this document you certify that these document
                      {/* are true and all information is correct */}
                    </small>
                  </span>
                </div>
                <div className="form-group offset-sm-3">
                  <UploadImageComp
                    validateDocuments={this.callUpdateDocuments}
                    fileParams={{
                      ...this.state,
                      user_id: this.user_id,
                      file_upload_action: "user_document"
                    }}
                    onSuccessfulFileUpload={this.successfulFileUpload}
                    url={"generic_upload_url"}
                    showSuccessMessage={this.showSuccessMessage}
                    handleRomoveImage={this.handleRomoveImage}
                    handleRemoveSelectedDocs={this.handleRemoveSelectedDocs}
                  />
                </div>
              </form>
            </div>
          </div>
          {this.props.user_documents.length > 0 && 
          <div>
          <Heading subHeading boldText="Documents"  className={"sub-heading-val"} />
            
              
          <ul className=" doc-list list-group m-b thumbnail p-3 ">
          {documents_list.length > 0 && documents_list.map((value,i)=>(
            
              <li className="d-flex" key={i}>  
                    
                {this.props.user_documents.find(item => item.document_type === value.name) ? <span className="list-number bg-success text-white mb-1 list-number-align">
                    <i className="tick_list glyphicon glyphicon-ok mt-2" />
                  </span>:<span className="list_number">{i + 1}</span>}                    
                <small>{value.name}</small>
              </li>
            ))}
          </ul></div>}          
          <div className="col-sm-12 uploaded-documents-block px-0">
              <h4>Uploaded <strong>Documents</strong></h4>
              <GenericTable
              tableListData={this.props.user_documents }
              headerData={["Document View","Type","Action"]}
              list={list}
              className={"payslip-table"}
              deleteDoc={this.props.deleteDocument}
              col_Name={"px-0"}
              row_Name={"document_row"}
              widget_mobile={"px-0"}
            />
            </div>
        </div>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    frontend: state.frontend.toJS(),
    loggedUser: state.logged_user.userLogin,
    usersList: state.usersList.toJS()
  };
}
const mapDispatchToProps = dispatch => {
  return {
    requestUsersList: () => {
      return dispatch(actions.requestUsersList());
    }
  };
};
const VisibleAddDocuments = connect(
  mapStateToProps,
  mapDispatchToProps
)(FormAddDocument);

const FormAddDocuments = withRouter(VisibleAddDocuments);

export default FormAddDocuments;
