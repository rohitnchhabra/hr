import React from "react";
import EmployeeActualSalary from "../managePayslips/EmployeeActualSalary";
import Checkbox from "../../../../components/generic/input/Checkbox";
import { Alert } from "reactstrap";
import * as _ from "lodash";
import $ from "jquery";
import AlertFader from "../../../../components/generic/AlertFader";

class FormGeneratePaySlip extends React.Component {
  constructor(props) {
    super(props);
    this.monthlyPayslip = this.monthlyPayslip.bind(this);
    this.state = {
      test_bonus: "",
      test_misc: "",
      user_id: "",
      year: "",
      month: "",
      month_name: "",
      name: "",
      designation: "",
      joining_date: "",
      total_working_days: "",
      days_present: "",
      paid_leaves: "",
      unpaid_leaves: "",
      total_leave_taken: "",
      allocated_leaves: "",
      leave_balance: "",
      final_leave_balance: "",
      basic: "",
      epf: "",
      hra: "",
      loan: "",
      conveyance: "",
      advance: "",
      medical_allowance: "",
      misc_deduction: "",
      misc_deduction_2: "",
      special_allowance: "",
      tds: "",
      arrear: "",
      extra_arrear: "",
      arrear_for_month: "",
      bonus: "",
      extra_work: "",
      total_earning: "",
      total_deduction: "",
      net_salary: "",
      send_email: 0,
      visible: "show",
      allowance_bonus: "Special Allowance",
      checked_status:false,
      key: 1,
      extra_days:"",
      status_message:"",
      total_holding_amount:""
    };
    this.loyalty_bonus = this.loyalty_bonus.bind(this);
  }
  componentDidMount() {
    var i;
    for (i = new Date().getFullYear(); i > 2000; i--) {
      $("#yearpicker").append($("<option value=" + i + ">" + i + "</option>"));
    }
    var j;
    for (j = 12; j >= 1; j--) {
      if(j>9){
        $("#monthpicker").append($("<option value=" + j + ">" + j + "</option>"));
      }
      else{
        $("#monthpicker").append($("<option value=" +"0"+ j + ">" + "0"+ j + "</option>"));
      }
      
    }
  }
  componentDidUpdate() {
    let e_basic = this.state.basic;
    let e_hra = this.state.hra;
    let e_conveyance = this.state.conveyance;
    let e_medical_allowance = this.state.medical_allowance;
    let e_special_allowance = this.state.special_allowance;
    let e_arrear = this.state.arrear;
    let e_bonus = this.state.bonus;
    let n_total_earning =
      +e_basic +
      +e_hra +
      +e_conveyance +
      +e_medical_allowance +
      +e_special_allowance +
      +e_arrear +
      +e_bonus;
    let e_epf = this.state.epf;
    let e_loan = this.state.loan;
    let e_advance = this.state.advance;
    let e_misc_deduction = this.state.misc_deduction;
    let e_misc_deduction_2 = this.state.misc_deduction_2;
    let e_tds = this.state.tds;
    let n_total_deduction =
      +e_epf +
      +e_loan +
      +e_advance +
      +e_misc_deduction +
      +e_tds
      +Number(e_misc_deduction_2);

      // this key is add to total deduction but dont know why
      // +e_misc_deduction_2;
      
    let n_net_salary = n_total_earning - n_total_deduction;

    n_total_earning = n_total_earning.toFixed(2);
    n_total_deduction = n_total_deduction.toFixed(2);
    n_net_salary = n_net_salary.toFixed(2);
    if (n_net_salary != this.state.net_salary) {
      this.setState({
        total_earning: n_total_earning,
        total_deduction: n_total_deduction,
        net_salary: n_net_salary
      });
    }
  }
  componentWillReceiveProps(props) {
    let user_id = "";
    let year = "";
    let month = "";
    let month_name = "";
    let name = "";
    let designation = "";
    let joining_date = "";
    let total_working_days = 0;
    let days_present = 0;
    let paid_leaves = 0;
    let unpaid_leaves = 0;
    let total_leave_taken = 0;
    let allocated_leaves = 0;
    let leave_balance = 0;
    let final_leave_balance = 0;
    let basic = 0;
    let epf = 0;
    let hra = 0;
    let loan = 0;
    let conveyance = 0;
    let advance = 0;
    let medical_allowance = 0;
    let misc_deduction = 0;
    let misc_deduction_2 = 0;
    let special_allowance = 0;
    let tds = 0;
    let arrear = 0;
    let extra_arrear = "";
    let arrear_for_month = "";
    let bonus = 0;
    let extra_work = 0;
    let total_earning = 0;
    let total_deduction = 0;
    let net_salary = 0;
    let total_holding_amount=0;

    // if(this.props.pending_Payslip.status_message!==props.pending_Payslip.status_message){
    //   this.setState({
    //     status_message:props.pending_Payslip.status_message
    //   },()=>{
    //     this.setState({
    //       status_message:""
    //     })
    //   })
    // }

    if (typeof this.props.user_id != "undefined") {
      user_id = this.props.user_id;
    }
    if (typeof this.props.name != "undefined") {
      name = this.props.name;
    }
    if (typeof this.props.designation != "undefined") {
      designation = this.props.designation;
    }

    if (typeof props.user_data_for_payslip != "undefined") {
      //---info
      if (typeof props.user_data_for_payslip != "undefined") {
        let d = props.user_data_for_payslip;
        if (typeof d.year != "undefined") {
          year = d.year;
        }

        if (typeof d.month != "undefined") {
          month = d.month;
        }

        if (typeof d.month_name != "undefined") {
          month_name = d.month_name;
        }

        if (typeof d.dateofjoining != "undefined") {
          joining_date = d.dateofjoining;
        }
        if (typeof d.total_working_days != "undefined") {
          total_working_days = d.total_working_days;
        }
        if (typeof d.paid_leaves != "undefined") {
          paid_leaves = d.paid_leaves;
        }

        //total_leave_taken

        if (
          typeof d.total_leave_taken != "undefined" &&
          d.total_leave_taken != null
        ) {
          total_leave_taken = d.total_leave_taken;
        }
        if (typeof d.leave_balance != "undefined" && d.leave_balance != null) {
          leave_balance = d.leave_balance;
        }
        if (typeof d.paid_leaves != "undefined" && d.paid_leaves != null) {
          paid_leaves = d.paid_leaves;
        }
        if (
          typeof d.final_leave_balance != "undefined" &&
          d.final_leave_balance != null
        ) {
          final_leave_balance = d.final_leave_balance;
        }

        if (typeof d.leaves_allocated != "undefined") {
          allocated_leaves = d.leaves_allocated;
        }
        if (typeof d.unpaid_leaves != "undefined") {
          unpaid_leaves = d.unpaid_leaves;
        }
        if (typeof d.days_present != "undefined") {
          days_present = d.days_present;
        }
        if (typeof d.net_salary != "undefined") {
          net_salary = d.net_salary;
        }
        if (typeof d.total_earning != "undefined") {
          total_earning = d.total_earning;
        }
      }

      //---salary
      if (typeof props.user_data_for_payslip.salary_detail != "undefined") {
        let SalaryDetails = props.user_data_for_payslip.salary_detail;

        if (typeof SalaryDetails.Basic != "undefined") {
          basic = SalaryDetails.Basic;
        }
        if (typeof SalaryDetails.EPF != "undefined") {
          epf = SalaryDetails.EPF;
        }
        if (typeof SalaryDetails.HRA != "undefined") {
          hra = SalaryDetails.HRA;
        }
        if (typeof SalaryDetails.Loan != "undefined") {
          loan = SalaryDetails.Loan;
        }
        if (typeof SalaryDetails.Conveyance != "undefined") {
          conveyance = SalaryDetails.Conveyance;
        }
        if (typeof SalaryDetails.Advance != "undefined") {
          advance = SalaryDetails.Advance;
        }
        if (typeof SalaryDetails.Medical_Allowance != "undefined") {
          medical_allowance = SalaryDetails.Medical_Allowance;
        }
        if (typeof SalaryDetails.Misc_Deductions != "undefined") {
          misc_deduction = SalaryDetails.Misc_Deductions;
        }
        if (SalaryDetails.total_holding_amount) {
          total_holding_amount = SalaryDetails.total_holding_amount;
        }
        if (SalaryDetails.total_deduction) {
          total_deduction = SalaryDetails.total_deduction;
        }
        if (typeof SalaryDetails.misc_deduction2 != "undefined") {
          misc_deduction_2 = SalaryDetails.misc_deduction2;
        }
        if (typeof SalaryDetails.Special_Allowance != "undefined") {
          special_allowance = SalaryDetails.Special_Allowance;
        }
        if (typeof SalaryDetails.TDS != "undefined") {
          tds = SalaryDetails.TDS;
        }
        if (typeof SalaryDetails.Arrears != "undefined") {
          arrear = SalaryDetails.Arrears;
        }

        if (typeof SalaryDetails.bonus != "undefined") {
          bonus = SalaryDetails.bonus;
        }
      }
    }

    if (typeof props.user_data_for_payslip.extra_arrear != "undefined") {
      extra_arrear = props.user_data_for_payslip.extra_arrear;
    }

    if (typeof props.user_data_for_payslip.arrear_for_month != "undefined") {
      arrear_for_month = props.user_data_for_payslip.arrear_for_month;
    }

    if(this.props.user_data_for_payslip.user_Id!==props.user_data_for_payslip.user_Id){
      this.setState({
        extra_days:"",
        checked_status:false
      })
      this.setState({
        user_id: user_id,
        name: name,
        year: this.props.year?this.props.year: year,
        month: this.props.month?this.props.month: month,
        month_name: month_name,
        designation: designation,
        joining_date: joining_date,
        total_working_days: total_working_days,
        days_present: days_present,
        paid_leaves: paid_leaves,
        unpaid_leaves: unpaid_leaves,
        total_leave_taken: total_leave_taken,
        allocated_leaves: allocated_leaves,
        leave_balance: leave_balance,
        final_leave_balance: final_leave_balance,
        basic: basic,
        epf: epf,
        hra: hra,
        loan: loan,
        conveyance: conveyance,
        advance: advance,
        medical_allowance: medical_allowance,
        misc_deduction: misc_deduction,
        misc_deduction_2: misc_deduction_2,
        special_allowance: special_allowance,
        tds: tds,
        arrear: arrear,
        extra_arrear: (this.props.user_data_for_payslip.user_Id==props.user_data_for_payslip.user_Id?this.state.extra_arrear !== "" ? this.state.extra_arrear : extra_arrear:extra_arrear),
        arrear_for_month:(this.props.user_data_for_payslip.user_Id==props.user_data_for_payslip.user_Id?this.state.arrear_for_month !== "" ? this.state.arrear_for_month : arrear_for_month:arrear_for_month),
        bonus: bonus,
        extra_work: extra_work,
        total_earning: total_earning,
        total_deduction: total_deduction,
        net_salary: net_salary,
        total_holding_amount
      });
    }

  
  }
  monthlyPayslip() {
    this.props.callMonthlyPayslip(
      this.props.user_id,
      this.state.year,
      this.state.month
    );
  }
  loyalty_bonus(e) {
    this.setState({checked_status:!this.state.checked_status})
    let checked_status = !this.state.checked_status;
    let stateToSet = {};
    if (checked_status == true) {
      stateToSet.allowance_bonus = "Loyalty Bonus";
      stateToSet.key = 2;
    } else {
      stateToSet.allowance_bonus = "Special Allowance";
      stateToSet.key = 1;
    }
    this.setState(stateToSet);
  }
  addArrear = () => {
    this.props.saveArrear(
           this.state.user_id,
           this.state.extra_arrear,
           this.state.arrear_for_month
         )
         this.setState({extra_arrear:"",arrear_for_month:""})
  }

  render() {
    let pending_Payslip = this.props.pending_Payslip
    const missDeduction = this.props.actualSalary.Misc_Deductions;
    let styles = _.cloneDeep(this.constructor.styles);
    let date = this.state.applicable_from;     
    return (
      <div className="d-flex flex-column">
        <div className="d-flex justify-content-between generate-payslips-name">
          <div>
            <h3 className="selected_username">{this.props.name}</h3>
            <h6 className="payslip-emp-id">
              Employee ID : {this.props.user_id}
            </h6>
          </div>
          <div>
            <form
              onSubmit={evt => {
                evt.preventDefault();
                let s = this.state;
                this.setState({
                  ...this.state,send_email_only:0,send_slack_message:0
                },()=>{this.props.callCreateUserPayslip(this.state, evt,)})
              }}
            >
              <div className="row mx-0">
                <div className="px-1">
                  <input
                    disabled={this.props.pending_Payslip.status_message.error==1?true:false}
                    type="submit"
                    value="Generate Payslip"
                    className="col-xs-12 md-btn p-2 create-payslip-button bg-success"
                  />
                </div>
                <div>
                  <button
                    type="button"
                    className="col-xs-12 md-btn p-2 mail-payslip-button"
                    onClick={e => {
                      this.setState({
                        ...this.state,send_email_only:1,send_slack_message:1
                      },()=>{this.props.callCreateUserPayslip(this.state, e)})
                    }}
                  >
                    Send Payslip
                  </button>
                </div>
                {/* <div className=" px-1">
                  <button
                    type="button"
                    className="col-xs-12 md-btn p-2 slack-payslip-button"
                    onClick={e => {
                      let s = this.state;
                      s.send_email_only = 0;
                      s.send_slack_message = 1;
                      this.props.callCreateUserPayslip(s, e);
                    }}
                  >
                    create and send slack message
                  </button>
                </div> */}
              </div>
            </form>
          </div>
        </div>
            <AlertFader>
            {this.props.pending_Payslip.status_message.message && (
              <Alert
                fade={false}
                className="alert-transparent"
                color={this.props.pending_Payslip.status_message.error?"danger":"success"}
              >
                {this.props.pending_Payslip.status_message.message}
              </Alert>
            )}
          </AlertFader>
        {this.props.payslipMonth && this.props.payslipMonth == new Date().getMonth() &&
        <Alert
                
                color={"success"}
              >
                <span className='text'>Payslip is already Generated</span>
              </Alert>}
         { pending_Payslip && pending_Payslip.last_month_payslip_pending && pending_Payslip.last_month_payslip_pending.user_data_for_payslip && pending_Payslip.last_month_payslip_pending.user_data_for_payslip.net_salary ? (
          <div className="col-xs-12 payslip-status-info mb-2 pl-2 pending_salary_value">
            <div className="d-flex">
              <div>Previous Month Payslip pending :</div>
              <div className="pl-3">
                { pending_Payslip.last_month_payslip_pending.user_data_for_payslip.net_salary}
              </div>
            </div>
            <div className="d-flex">
              <div>Net Salary Including Previous Month : </div>
              <div className="pl-3">
                {pending_Payslip &&
                  pending_Payslip.user_data_for_payslip
                    .net_salary_including_previous_month}
              </div>
            </div>
          </div>
        ) : (
          ""
        )} 

          <div>
            {this.props.before_Joining &&
            this.props.before_Joining.length > 0 ? (
              <div className="col-xs-12 p-2 mb-2 payslip-info-status">
                <div className="">
                  <span className="payslip-emp-id">
                    No Of Days Before Joining -
                  </span>
                  <span className="text-center pl-2">
                    {this.props.before_Joining &&
                      this.props.before_Joining.length}
                  </span>
                </div>
                <div className=" d-flex">
                  <div className="payslip-emp-id">Dates -:</div>
                  <div className="col-xs-12 ">
                    {_.map(this.props.before_Joining, (leave, key) => (
                      <span className="date-to-show-before" key={key}> {leave} ,</span> 
                    ))}
                  </div>
                </div>
              </div>
            ) : (
              ""
            )}
            {this.props.pending_leaves && this.props.pending_leaves.length >0 && <div className="col-xs-12 p-2 payslip-info-status">
              <div className="">
                <span className="payslip-emp-id">
                  No of days for which timing is missing from employee:
                </span>
                <span className="text-center pl-2">
                  {this.props.pending_leaves &&
                    this.props.pending_leaves.length}
                </span>
              </div>
              <div className=" d-flex">
                <div className="payslip-emp-id">Dates of Leave :{" "}</div>
                <div className="col-xs-12">
                  {_.map(this.props.pending_leaves, (leave, key) => (
                    <span className="date-to-show-before" key={key}> {leave} {key == this.props.pending_leaves.length-1 ? "" :"," }</span>
                  ))}
                </div>
              </div>
              <div className="notify-employee-button mt-1">
                  <button
                    type="button"
                    className="col-xs-12 md-btn p-2 "
                    onClick={e => {
                      this.props.handleNotifyEmployeeTimeMissing();
                    }}
                  >
                    Notify Employee
                  </button>
                </div>
            </div>}
            {this.props.employee_applied_pending_leaves && this.props.employee_applied_pending_leaves.length ? 
            <div className="col-xs-12 p-2 payslip-info-status mt">
              <div className="">
                <span className="payslip-emp-id">
                No of leaves which are applied by employee, but pending approval:
                </span>
                <span className="text-center pl-2">
                  {this.props.employee_applied_pending_leaves &&
                    this.props.employee_applied_pending_leaves.length}
                </span>
              </div>
              <div className=" d-flex">
                <div className="payslip-emp-id">Dates of Leave:</div>
                <div className="col-xs-12">
                  {_.map(this.props.employee_applied_pending_leaves, (leave, key) => (
                    <span className="date-to-show-before" key={key}> {leave} {key == this.props.employee_applied_pending_leaves.length-1 ? "" :"," }</span>
                  ))}
                </div>
              </div>
            </div>:""}
           {this.props.pending_leaves && this.props.pending_leaves.length==0 && this.props.employee_applied_pending_leaves && this.props.employee_applied_pending_leaves.length==0 &&
              <div className="col-xs-12 payslip-status-info p-2">
                <strong>Info:</strong>
                <span className="pl-1 payslip-emp-id">No Pending Leaves!</span>
            </div>
          }
          </div>

        <div className="row mx-0 mt-3 ">
          <div className="col-md-7 ">
          <div className="d-flex justify-content-between px-2 py-2 bg-body ">
              <div className="salary-title salary_view_payslip pl-2"> LEAVE <strong> DETAILS</strong> </div>
            </div>
            <div className=" row mx-0 d-flex payslip_leave_detail justify-content-between px-2 bg-body">
            <div className="col-sm-5 px-0">
                <div className="d-flex justify-content-between p-2">
                  <div>Designation</div>
                  <div>{this.state.designation}</div>
                </div>
                <div className="d-flex justify-content-between p-2">
                  <div>Joining Date</div>
                  <div>{this.state.joining_date}</div>
                </div>
                <div className="d-flex justify-content-between p-2">
                  <div>Days Present</div>
                  <div>{this.state.days_present}</div>
                </div>
                <div className="d-flex justify-content-between p-2">
                  <div>Total Working Days</div>
                  <div>{this.state.total_working_days}</div>
                </div> 
              </div> 

              <div className="col-sm-5 col-sm-offset-2 px-0">
                <div className="d-flex justify-content-between p-2">
                  <div>Paid Leave</div>
                  <div>{this.state.paid_leaves}</div>
                </div>
                <div className="d-flex justify-content-between p-2">
                  <div>Leave Accumulated</div>
                  <div>{this.state.allocated_leaves}</div>
                </div>
                <div className="d-flex justify-content-between p-2">
                  <div>Total Leave Taken</div>
                  <div>{this.state.total_leave_taken}</div>
                </div>
                <div className="d-flex justify-content-between p-2">
                  <div>Final Leave Balance</div>
                  <div>{this.state.final_leave_balance}</div>
                </div>
                <div className="d-flex justify-content-between p-2">
                  <div>Leave Without Pay</div>
                  <div>{this.state.unpaid_leaves}</div>
                </div> 
              </div> 
              
            </div>
            <div className="d-flex justify-content-between px-2 py-2  ">
              <div className="salary-title salary_view_payslip pl-2"> YEAR & <strong> MONTH</strong> </div>
            </div>
            <div className=" px-2 mb-3 payslip-bonus">
              <div className=" row mx-0 d-flex justify-content-between payslip_sub_heading p-2 ">
                <div className="col-sm-5 px-0">
                  <div className="d-flex justify-content-between align-items-baseline p-2">
                    <div>Year</div>
                    <div><select
                          id="yearpicker"
                          className="payslip-yearpicker"
                          value={this.state.year}
                          onChange={evt => {
                            this.setState({year: evt.target.value});
                            this.props.callMonthlyPayslip(
                              this.props.user_id,
                              evt.target.value,
                              this.state.month
                            );
                          }}
                        ></select></div>
                  </div>
                  <div className="d-flex justify-content-between align-items-baseline p-2">
                    <div>{"Loyalty Bonus"}</div>
                    <div className="check_loyal"><Checkbox 
                          checked={this.state.checked_status}
                          onClick={e => {this.loyalty_bonus(e);}}
                          /></div>
                  </div> 
                </div> 

                <div className="col-sm-5 col-sm-offset-2 px-0">
                <div className="d-flex justify-content-between align-items-baseline p-2">
                  <div>{"Month"}</div>
                  <div><select
                          id="monthpicker"
                          value={this.state.month}
                          className="payslip-monthpicker"
                          onChange={evt => {
                            this.setState({ visible: "show",month:evt.target.value });
                            this.props.callMonthlyPayslip(
                              this.props.user_id,
                              this.state.year,
                              evt.target.value
                            );
                          }}
                        ></select>
                        <span> {this.state.month_name} </span> 
                        </div>
                </div>
                <div className="d-flex justify-content-between align-items-baseline pl-2">
                  <div className="col-sm-6">{"Extra Days"}</div>
                  <div className=""><input
                          className="w-75 p-1  payslip-text-feild "
                          type="text"
                          name="extra_work"
                          placeholder="No. of Days"
                          value={this.state.extra_days}
                          onChange={e => {
                            let test =
                              (this.props.actualSalary.total_earning /
                                this.state.total_working_days) *
                              e.target.value;
                            this.setState({ bonus: parseInt(test),extra_days:e.target.value });
                          }}
                        />
                    </div>
                </div>
                 
              </div> 
                
              </div>
            </div>
            <div>
            <div className="d-flex justify-content-between px-2 py-2 bg-body ">
              <div className="salary-title salary_view_payslip pl-2"> ADD <strong> ARREARS</strong> </div>
            </div>
              <div className="p-2 payslip-Arrear">
                <div className="d-flex payslip_arial_display">
                  <div className="d-sm-flex payslip_arrear_response col-12">
                  <div className="d-sm-flex col-sm-11">
                    <div className="d-flex align-items-xl-center col-sm-6 mb-sm-0 mb-2">
                      <div className="pr responsive_extra_arrear col-6 col-sm-5">{"Extra Arrear"}</div>
                      <input
                          required={true}
                          className="w-50 p-1 payslip-text-feild col-6"
                          onChange={evt =>
                            this.setState({ extra_arrear: evt.target.value })
                          }
                          type="text"
                          name="extra_arrear"
                          value={this.state.extra_arrear}
                        />
                    </div>
                    <div className="d-flex align-items-xl-center col-sm-6 mb-sm-0 mb-2 pr-sm-3">
                      <div className="pr responsive_extra_arrear col-6 col-sm-6">{"Arrear for Month"}</div>
                        <input
                          required={true}
                          className="w-50 p-1 payslip-text-feild col-6 col-sm-6"
                          onChange={evt =>
                            this.setState({ arrear_for_month: evt.target.value })
                          }
                          type="text"
                          name="extra_arrear"
                          value={this.state.arrear_for_month}
                        />
                    </div>
                    </div>
                  <div className="payslip-add-arrear col-sm-1">
                    <input
                    type="button"
                    onClick={() =>this.addArrear()}
                    //   this.props.saveArrear(
                    //     this.state.user_id,
                    //     this.state.extra_arrear,
                    //     this.state.arrear_for_month
                    //   ),
                    //   this.setState({ arrear_for_month: evt.target.value })
                    // }
                    id="arrear1"
                    value="Add"
                    className="md-btn mail-payslip-button offset-sm-0 offset-6 col-6 col-sm-12"
                  ></input>
                    </div>
                    
                  </div>
                </div>
                {/* <div>
                  <input
                    type="button"
                    onClick={() =>
                      this.props.saveArrear(
                        this.state.user_id,
                        this.state.extra_arrear,
                        this.state.arrear_for_month
                      )
                    }
                    id="arrear1"
                    value="Add Arrear"
                    className="md-btn md-raised info"
                  ></input>{" "}
                </div> */}
                {/* <div className="pt-2"><strong>Note:</strong> <small>"This section should be used to add any previous
                       pending arrears for employee. Extra Arrears is the actual amount per month
                        which you want to add for an employee and Arrears for month is the number
                         of month for which arrears needs to be added. This will actually 
                         automatically take into account any leaves as well for
                          the previous months".</small></div> */}
              </div>
            </div>
            <div className="mt-2 total_payslip_value">
              <div className="d-flex row mx-0  justify-content-between p-2 payslip_sub_heading">
                      <div className="col-sm-5 px-0">
                      <h6><strong className="pl-2">Earning</strong></h6>
                        <div className="d-flex justify-content-between p-2">
                          <div>Basic</div>
                          <div>{this.state.basic}</div>
                        </div>
                        <div className="d-flex justify-content-between p-2">
                          <div>HRA</div>
                          <div>{this.state.hra}</div>
                        </div>
                        <div className="d-flex justify-content-between p-2">
                          <div>Conveyance</div>
                          <div>{this.state.conveyance}</div>
                        </div>
                        <div className="d-flex justify-content-between p-2">
                          <div>Medical Allowance</div>
                          <div>{this.state.medical_allowance}</div>
                        </div>
                        <div className="d-flex justify-content-between p-2">
                          <div>Special Allowance</div>
                          <div>{this.state.special_allowance}</div>
                        </div> 
                        <div className="d-flex justify-content-between p-2">
                          <div>Arrears</div>
                          <div>{this.state.arrear}</div>
                        </div> 
                        <div className="d-flex justify-content-between p-2 align-items-center">
                          <div className="col-sm-6">Bonus</div>
                          <div><input
                            className=" w-100 p-1 payslip-text-feild pr-0 text-right"
                            type="text"
                            value={this.state.bonus}
                            ref="bonus"
                            placeholder="Bonus Salary"
                            onChange={evt =>
                              this.setState({ bonus: evt.target.value })
                            }
                          /></div>
                        </div>
                      </div> 

                      <div className="col-sm-5 col-sm-offset-2 px-0">
                      <h6><strong className="pl-2">Deductions</strong></h6>
                <div className="d-flex justify-content-between p-2">
                  <div>EPF</div>
                  <div>{this.state.epf}</div>
                </div>
                <div className="d-flex justify-content-between p-2">
                  <div>Loan</div>
                  <div>{this.state.loan}</div>
                </div>
                <div className="d-flex justify-content-between p-2">
                  <div>Advance</div>
                  <div>{this.state.advance}</div>
                </div>
                <div className="d-flex justify-content-between p-2">
                  <div>Holding Amount</div>
                  <div>{this.state.total_holding_amount}</div>
                </div>
                <div className="d-flex justify-content-between p-2">
                  <div>TDS</div>
                  <div>{this.state.tds}</div>
                </div>
                <div className="d-flex justify-content-between p-2 align-items-center">
                  <div>Misc Deductions</div>
                  <div><input
                            className=" w-100 p-1 payslip-text-feild pr-0 text-right"
                            type="text"
                            value={this.state.misc_deduction_2}
                            placeholder="Misc Deduction"
                            onChange={evt =>
                              this.setState({ misc_deduction_2: evt.target.value })
                            }
                          />
                  </div>
                </div>
                <div className="d-flex justify-content-between p-2">
                  <div>Unpaid Leaves Amount</div>
                  <div>{this.props.user_data_for_payslip.total_leaves_amount_deducted}</div>
                </div> 
              </div> 
                      
                
              </div>
              <div className="total_value_detail_payslip font-weight-bold border-none pt-3">
                <div className="py-2 left_total">
                  <div>TOTAL EARNING</div>
                  <div> {this.state.total_earning} </div>
                </div>
                <div className="py-2 left_total">
                  <div>TOTAL DEDUCTIONS</div>
                  <div> {this.state.total_deduction} </div>
                </div>
                <div className="py-2 left_total">
                  <div>NET SALARY (RS.)</div>
                  <div> {this.state.net_salary} </div>
                </div>
              </div>
            </div>
          </div>

          <div className="col-md-5 responsive-total-salary">
            <EmployeeActualSalary
              employee_actual_salary={this.props.employee_actual_salary}
              user_data_for_payslip={this.props.user_data_for_payslip}
            />
          </div>
        </div>
      </div>
    );
  }
}

FormGeneratePaySlip.styles = {
  leaveDiv: {
    marginBottom: "10px"
  }
};

export default FormGeneratePaySlip;
