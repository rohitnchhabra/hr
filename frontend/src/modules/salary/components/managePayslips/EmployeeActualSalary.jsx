import React from 'react';
import SalaryTable from "../../../../components/generic/SalaryTable";

class EmployeeActualSalary extends React.Component {
  constructor (props) {
    super(props);
    this.state = {
      basic: '',
      epf: '',
      hra: '',
      loan: '',
      conveyance: '',
      advance: '',
      medical_allowance: '',
      misc_deduction: '',
      special_allowance: '',
      tds: '',
      arrear: '',
      bonus: '',
      total_earning: '',
      total_deduction: '',
      net_salary: ''
    };
  }

  componentWillReceiveProps (props) {
    let basic = 0;
    let epf = 0;
    let hra = 0;
    let loan = 0;
    let conveyance = 0;
    let advance = 0;
    let medical_allowance = 0;
    let misc_deduction = 0;
    let special_allowance = 0;
    let tds = 0;
    let arrear = 0;
    let bonus = 0;
    let total_earning = 0;
    let total_deduction = 0;
    let net_salary = 0;

    let SalaryDetails = props.employee_actual_salary;

    if (typeof SalaryDetails.Basic !== 'undefined') {
      basic = SalaryDetails.Basic;
    }
    if (typeof SalaryDetails.EPF !== 'undefined') {
      epf = SalaryDetails.EPF;
    }
    if (typeof SalaryDetails.HRA !== 'undefined') {
      hra = SalaryDetails.HRA;
    }
    if (typeof SalaryDetails.Loan !== 'undefined') {
      loan = SalaryDetails.Loan;
    }
    if (typeof SalaryDetails.Conveyance !== 'undefined') {
      conveyance = SalaryDetails.Conveyance;
    }
    if (typeof SalaryDetails.Advance !== 'undefined') {
      advance = SalaryDetails.Advance;
    }
    if (typeof SalaryDetails.Medical_Allowance !== 'undefined') {
      medical_allowance = SalaryDetails.Medical_Allowance;
    }
    if (typeof SalaryDetails.Misc_Deductions !== 'undefined') {
      misc_deduction = SalaryDetails.Misc_Deductions;
    }
    if (typeof SalaryDetails.Special_Allowance !== 'undefined') {
      special_allowance = SalaryDetails.Special_Allowance;
    }
    if (typeof SalaryDetails.TDS !== 'undefined') {
      tds = SalaryDetails.TDS;
    }
    if (typeof SalaryDetails.Arrears !== 'undefined') {
      arrear = SalaryDetails.Arrears;
    }
    if (typeof SalaryDetails.total_earning !== 'undefined') {
      total_earning = SalaryDetails.total_earning;
    }
    if (typeof SalaryDetails.net_salary !== 'undefined') {
      net_salary = SalaryDetails.net_salary;
    }
    if(props.user_data_for_payslip.salary_detail){
      total_deduction=props.user_data_for_payslip.salary_detail.total_deduction;
    }

    this.setState({
      basic: basic,
      epf: epf,
      hra: hra,
      loan: loan,
      conveyance: conveyance,
      advance: advance,
      medical_allowance: medical_allowance,
      misc_deduction: misc_deduction,
      special_allowance: special_allowance,
      tds: tds,
      arrear: arrear,
      bonus: bonus,
      total_earning: total_earning,
      total_deduction: total_deduction,
      net_salary: net_salary
    });
  }
  
  render () {
    const userday_payslip = this.props && this.props.user_data_for_payslip ;
    const valueDecimal = val => {
      let v = Number(val);
      return Math.round(v) === v ? v : v.toFixed(2);
    };
    return (
      <div className="bg-body h-100">
        <div>
        <div className="d-flex justify-content-between px-2 py-2  ">
              <div className="salary-title salary_view_payslip"> SALARY <strong> DETAILS</strong> </div>
            </div>
          <div className="total_payslip_salary mb-1">
            <div className="d-flex justify-content-between px-2 py-2 ">
              <div className="salary-title">Applicable From</div>
              <div>{userday_payslip.applicable_from}</div>
            </div>
            <div className="d-flex justify-content-between px-2 py-2 ">
              <div className="salary-title">Applicable Till</div>
              <div>{userday_payslip.applicable_till}</div>
            </div>
            <div className="d-flex justify-content-between px-2 py-2  ">
              <div className="salary-title">Leaves Allocated</div>
              <div>{userday_payslip.leaves_allocated}</div>
            </div>
            <div className="d-flex justify-content-between px-2 py-2  ">
              <div className="salary-title">Increment Amount</div>
              <div>0</div>
            </div>
            <div className="d-flex justify-content-between px-2 py-2  ">
              <div className="salary-title">Updated On</div>
              <div>{userday_payslip.last_updated_on}</div>
            </div>
          </div>
          <div>
          <div className="col-md-12 pl-2 ">
             <SalaryTable earning = {"success"} data={this.state}/>
          </div>
          
          <div className= "col-md-12 pl-2 ">
             <SalaryTable earning = {"deduction"} data={this.state}/>
          </div>
          </div>

        <div className="total_detail_value ml-2 font-weight-bold payslip-emp-id ">
        <div className=" py-2 response_total_earning">
          <div> EARNING</div>
          <div> {valueDecimal(this.state.total_earning)} </div>
        </div>
        <div className=" py-2 response_total_earning">
          <div> DEDUCTIONS</div>
          <div> {valueDecimal(this.state.total_deduction)} </div>
        </div>
        <div className=" py-2 response_total_earning">
          <div>NET SALARY (RS.)</div>
          <div>{valueDecimal(this.state.net_salary)} </div>
        </div>
      </div> 








          {/* <table className="table">
            <tbody>
              <tr>
                <td className="text-center"><b><u>Earnings</u></b></td>
                <td></td>
                <td className="text-center"><b><u>Deductions</u></b></td>
                <td></td>
              </tr>
              <tr>
                <td>Basic</td>
                <td>{this.state.basic}</td>
                <td>EPF</td>
                <td>{this.state.epf}</td>
              </tr>
              <tr>
                <td>HRA</td>
                <td>{this.state.hra}</td>
                <td>Loan</td>
                <td>{this.state.loan}</td>
              </tr>
              <tr>
                <td>Conveyance</td>
                <td>{this.state.conveyance}</td>
                <td>Advance</td>
                <td>{this.state.advance}</td>
              </tr>
              <tr>
                <td>Medical Allowance</td>
                <td>{this.state.medical_allowance}</td>
                <td>Misc Deductions</td>
                <td>
                  {this.state.misc_deduction}
                </td>
              </tr>
              <tr>
                <td>Special Allowance</td>
                <td>{this.state.special_allowance}</td>
                <td>TDS</td>
                <td>{this.state.tds}</td>
              </tr>
              <tr>
                <td>Arrears</td>
                <td>{this.state.arrear}</td>
                <td></td>
                <td></td>
              </tr>
              <tr>
                <td>Bonus</td>
                <td>
                {this.state.bonus}
                </td>
                <td></td>
                <td></td>
              </tr>
              <tr>
                <td><h5>Total Earnings</h5></td>
                <td><h5>{this.state.total_earning}</h5></td>
                <td><h5>Total Deductions</h5></td>
                <td>{this.state.total_deduction}</td>
              </tr>
              <tr>
                <td><h3>Net Salary</h3></td>
                <td><h3>{this.state.net_salary}</h3></td>
                <td></td>
                <td></td>
              </tr>
            </tbody>
          </table> */}
        </div>
      </div>
    );
  }
}

export default EmployeeActualSalary;
