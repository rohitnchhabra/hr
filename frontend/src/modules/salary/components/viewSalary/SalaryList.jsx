import React from 'react';
import * as _ from 'lodash';
import profilePlaceholder from "../../../../static/profilePlaceholder.png";
var moment = require('moment');

class SalaryList extends React.Component {
  constructor (props) {
    super(props);
    this.state = {
      empList: this.props.empList
    };
    this.search = this.search.bind(this);
  }

  search (searchValue) {
    if (searchValue !== 'null' && searchValue !== '' && !_.isEmpty(searchValue)) {
      var emps = [];
      _.forEach(this.props.empList, function (emp, i) {
        if ((!_.isEmpty(emp.empName) && emp.empName.toLowerCase().indexOf(searchValue.toLowerCase()) !== -1) ||
         (!_.isEmpty(emp.designation) && emp.designation.toLowerCase().indexOf(searchValue.toLowerCase()) !== -1) ||
         (emp.salary.toString().indexOf(searchValue.toString()) !== -1) ||
         (!_.isEmpty(emp.dateOfJoining) && emp.dateOfJoining.toString().indexOf(searchValue.toString()) !== -1) ||
         (!_.isEmpty(emp.noOfDaysSinceJoined) && emp.noOfDaysSinceJoined.toString().indexOf(searchValue.toString()) !== -1) ||
         (!_.isEmpty(emp.preSalaryIncDetail) && emp.preSalaryIncDetail.toLowerCase().indexOf(searchValue.toLowerCase()) !== -1) ||
         (!_.isEmpty(emp.nextSallaryInc) && emp.nextSallaryInc.toLowerCase().indexOf(searchValue.toLowerCase()) !== -1)) {
          emps.push({
            'image':               emp.image,
            'empName':             emp.empName,
            'designation':         emp.designation,
            'salary':              emp.salary,
            'holdingAmountDetail': emp.holdingAmountDetail,
            'dateOfJoining':       emp.dateOfJoining,
            'noOfDaysSinceJoined': emp.noOfDaysSinceJoined,
            'preSalaryIncDetail':  emp.preSalaryIncDetail,
            'nextSallaryInc':      emp.nextSallaryInc
          });
        }
      });
      this.setState({
        empList: emps
      });
    } else {
      this.setState({
        empList: this.props.empList
      });
    }
  }
  render () {
    let row = _.map(this.state.empList, (emp, key) => {
      return (
        <tr key={key}>
          <td className="align-items-md-center d-flex">
            <div className="list-left">
              <span className="w-40 avatar">
                <img src={emp.image || profilePlaceholder } alt ={emp.empName} /> 
              </span>
            </div>
            <div>{emp.empName}</div>
          </td>
          {/* <td>{emp.empName}</td> */}
          <td>{emp.designation}</td>
          <td>{parseFloat(emp.salary).toFixed(2)}</td>
          <td>{emp.holdingAmountDetail != ''
            ? <ul className="align-items-baseline d-flex flex-column">
              <li>Holding amount : {emp.holdingAmountDetail.holding_amt}</li>
              <li>Start date : {emp.holdingAmountDetail.holding_start_date}</li>
              <li>End date : {emp.holdingAmountDetail.holding_end_date}</li>
              <li>Reason : {emp.holdingAmountDetail.reason}</li>
            </ul> : ''
          }</td>
          <td>{moment(emp.dateOfJoining).format('Do MMM YYYY')}</td>
          <td>{emp.noOfDaysSinceJoined}</td>
          <td>{parseFloat(emp.preSalaryIncDetail).toFixed(2)}</td>
          <td>{moment(emp.nextSallaryInc).format('Do MMM YYYY')}</td>
        </tr>
      );
    });
    return (
      <div>
        <div className="row no-gutter">
          <div className="col-md-6 p-r">
            <div className="form-group d-flex align-items-baseline">
              <label style={{'fontSize': 15}} className="mr-3">Search:</label>
              <input type="text" id="search-form" className="form-control" ref="search" onChange={() => this.search(this.refs.search.value)} />
            </div>
          </div>
        </div>
        <div className="row no-gutter">
          <div className="col-12">
            <div className="box">
              <div className="box salary-view-admin">
                <div className="m-a-0"></div>
                <div className="table-responsive">
                  <table className="table">
                    <thead className="">
                      <tr className="salary-page-row">
                        <th >Basic Info</th>
                        {/* <th>Employe name</th> */}
                        <th>Designation</th>
                        <th>Current Salary</th>
                        <th style={{'textAlign': 'center'}} >Holding Amount Details</th>
                        <th>Date of Joining</th>
                        <th >No of Days Since Joined</th>
                        <th >Previous Increment Details</th>
                        <th >Next Increment</th>
                      </tr>
                    </thead>
                    <tbody className="body-for-salarydetail">
                      {row}
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default SalaryList;
