import React, { useState } from "react";
import { Row, Col, Table, Label, Input, Button } from "reactstrap";
import moment from "moment";

export default function NewSalary({ isMobile, salaries }) {
  const [loadMore, setLoadMore] = useState(false);
  if (!isMobile) {
    return (
      <div className="not-displayed my-3 text-secondary font-weight-bold"> 
      ** Your salary details are hidden on desktop for your own security purpose,
        you can only view your salary on mobile.
      </div>
    );
  }
  const valueDecimal = val => {
    let v = Number(val);
    return Math.round(v) === v ? v : v.toFixed(2);
  };
  const earning_fields = [
    {
      text: "Basic",
      field: "Basic"
    },
    {
      text: "HRA",
      field: "HRA"
    },
    {
      text: "Conveyance",
      field: "Conveyance"
    },
    {
      text: "Medical Allowance",
      field: "Medical_Allowance"
    },
    {
      text: "Special Allowance",
      field: "Special_Allowance"
    }
  ];

  const deduction_fields = [
    {
      text: "Holding Amount",
      field: "Basic"
    },
    {
      text: "EPF",
      field: "EPF"
    },
    {
      text: "Loan",
      field: "Loan"
    },
    {
      text: "Misc Deductions",
      field: "Misc_Deductions"
    },
    {
      text: "TDS",
      field: "TDS"
    }
  ];
  
  return (
    <div>
      {salaries.slice(0, !loadMore ? 1 : salaries.length).map((item, key) => (
        <div className="box" key={key}>
          <Row className="no-gutter mx-0 earning-deduction-tabble">
            <Col className="form-action text-sm px-0">
            <div className="mysalary_view_list">
                <div><span className="salary-title">Applicable From: </span>{" "}
                <strong> {moment(item.test.applicable_from).format("DD-MMMM-YYYY") ||
                  "--"}{" "}</strong></div>
                  <span className="divider padding-horizontal mysalary_mobile">|</span>
                <div>
                <span className="salary-title"> Applicable Till: </span>{" "}
                <strong> {`${
                  item.test.applicable_month
                    ? item.test.applicable_month + " months"
                    : "--"
                } (${moment(item.test.applicable_till).format("DD-MMMM-YYYY") ||
                  "--"})`}{" "}</strong></div>
                  <span className="divider padding-horizontal mysalary_mobile">|</span>
                <div>  
                
                <span className="salary-title"> Increment Amount: </span>{" "}
                <strong> {item.Increment_Amount || "--"}{" "}</strong></div>
                <span className="divider padding-horizontal mysalary_mobile">|</span>
                <div>
                <span className="salary-title"> Leaves Allocated: </span>{" "}
                <strong>{item.test.leaves_allocated || "--"}{" "}</strong></div>
                <span className="divider padding-horizontal mysalary_mobile">|</span>
                <div>
                
                <span className="salary-title"> Updated On: </span>{" "}
                <strong> {moment(item.test.last_updated_on).format("DD-MMMM-YYYY") ||
                  "--"}</strong></div>
              </div>
            </Col>
          </Row>
          <div className="d-flex no-gutter  earning-deduction-tabble">
            <div className="flex-fill border">
              <div className="bg-success py-2 px-4 text-white">
                TOTAL <strong>EARNING</strong>
              </div>
              <div>
                {earning_fields.map((v, i) => (
                  <div
                    key={i}
                    className="d-flex justify-content-between px-4 py-2 border-btm"
                  >
                    <div>{v.text}</div>
                    <div>{valueDecimal(item[v.field])}</div>
                  </div>
                ))}
              </div>
            </div>
            <div className="flex-fill  border b-5x ml-3 right-deduction-table">
              <div className="bg-danger py-2  px-4 text-white">
                TOTAL <strong>DEDUCTIONS</strong>
              </div>
              <div>
                {deduction_fields.map((d)=>(
                  <div className="d-flex justify-content-between px-4 py-2 border-btm">
                    <div>{d.text}</div>
                    <div>{valueDecimal(item[d.field])}</div>
                  </div>
                ))}
              </div>
            </div>
          </div>
          <div className="my-salary-detail-income mt-2 font-weight-bold border">
        <div className="net-value py-2">
          <div>TOTAL EARNING</div>
          <div className="text-center"> {(
                      item.Basic * 1 +
                      item.HRA * 1 +
                      item.Conveyance * 1 +
                      item.Medical_Allowance * 1 +
                      item.Special_Allowance * 1 +
                      item.Arrears * 1
                    ).toFixed(2)} </div>
        </div>
        <div className="net-value py-2">
          <div>TOTAL DEDUCTIONS</div>
          <div className="text-center"> {(
                      item.EPF * 1 +
                      item.Loan * 1 +
                      item.Advance * 1 +
                      item.Misc_Deductions * 1 +
                      item.TDS * 1
                    ).toFixed(2)} </div>
        </div>
        <div className="net-value py-2">
          <div>NET SALARY (RS.)</div>
          <div className="text-center"> {(item.test.total_salary * 1).toFixed(2)} </div>
        </div>
      </div>
      
      </div>
        ))}
        <div className="text-center mx-2">
          {salaries.length > 1 &&
            <Button
              className="mysalary_button"
              outline
              color="primary"
              onClick={() => setLoadMore(!loadMore)}
            >
              {!loadMore ? "load more" : "load less"}
            </Button>
          }
        </div>
        
    </div>
  );
}
