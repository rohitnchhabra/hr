import React from "react";
import * as _ from "lodash";
import moment from "moment";
import Checkbox from "../../../../components/generic/input/Checkbox";
import ToggleButton from "react-toggle-button";
import * as MainCONFIG from "../../../../config/index";
import { compose } from "redux";
import isMobile from "../../../../components/hoc/WindowResize";
import GenericDatePicker from "../../../../components/generic/GenericDatePicker";
// possible conditions with diff slab
// below 15000, epf mandatory
// 15000 - 22000 with epf
// 15000 - 22000 without epf
// 22000 - 30000 with epf
// 22000 - 30000 without epf
// 30000+ with epf
// 30000+ without epf

const CONFIG = {
  slab_1: {
    // below 15000, epf mandatory
    basic: 0.5,
    // epf: 0.25,
    hra: 0.1,
    conveyance: 0.1,
    medical_allowance: 0.1,
    special_allowance: 0.2
    // tds: 0.10,
  },
  slab_2: {
    // 15001 - 21999 with epf
    basic: 0.5,
    // epf: 0.20,
    hra: 0.1,
    conveyance: 0.1,
    medical_allowance: 0.1,
    special_allowance: 0.2
    // tds: 0.10,
  },
  slab_3: {
    // 15000 - 22000 without epf
    basic: 0.5,
    // epf: 0.15,
    hra: 0.1,
    conveyance: 0.1,
    medical_allowance: 0.1,
    special_allowance: 0.2
    // tds: 0.10,
  },
  slab_4: {
    // 22000 - 30000 with epf
    basic: 0.5,
    // epf: 0.30,
    hra: 0.1,
    conveyance: 0.1,
    medical_allowance: 0.1,
    special_allowance: 0.2
    // tds: 0.10,
  },
  slab_5: {
    // 22000 - 30000 without epf
    basic: 0.5,
    // epf: 0.30,
    hra: 0.1,
    conveyance: 0.1,
    medical_allowance: 0.1,
    special_allowance: 0.2
    // tds: 0.10,
  },
  slab_6: {
    // 30000+ with epf
    basic: 0.5,
    // epf: 0.30,
    hra: 0.1,
    conveyance: 0.1,
    medical_allowance: 0.1,
    special_allowance: 0.2
    // tds: 0.10,
  },
  slab_7: {
    // 30000+ without epf
    basic: 0.5,
    // epf: 0.30,
    hra: 0.1,
    conveyance: 0.1,
    medical_allowance: 0.1,
    special_allowance: 0.2
    // tds: 0.10,
  }
};

const getConfig = (salaryAmt, isEpf) => {
  let config = CONFIG.slab_1;
  if (salaryAmt <= 15000) {
    config = CONFIG.slab_1;
  } else if (salaryAmt >= 15001 && salaryAmt <= 21999) {
    if (isEpf) {
      config = CONFIG.slab_2;
    } else {
      config = CONFIG.slab_3;
    }
  } else if (salaryAmt >= 22000 && salaryAmt <= 29999) {
    if (isEpf) {
      config = CONFIG.slab_4;
    } else {
      config = CONFIG.slab_5;
    }
  } else if (salaryAmt >= 30000) {
    if (isEpf) {
      config = CONFIG.slab_6;
    } else {
      config = CONFIG.slab_7;
    }
  }
  return config;
};



class AddSalaryForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      formData: {
        user_id: this.props.userid || "",
        applicable_from: new Date(),
        applicable_till: null,
        applicable_month: "",
        total_salary: "0",
        total_earning: "0",
        total_deduction: "0",
        leave: "0",
        increment_amount: "0",
        basic: "0",
        hra: "0",
        conveyance: "0",
        medical_allowance: "0",
        special_allowance_backup: "0",
        special_allowance: "0",
        arrear: "0",
        epf: "0",
        loan: "0",
        advance: "0",
        misc_deduction: "0",
        hra_claim: "0",
        standard_deduction: "0",
        eighty_c_deduction: "0",
        tds: "0",
        saved_tax_amount: "0",
        isEpf: true,
        notifySalaryUpdateToEmp:false
      },
      taxBreakUp: false,
      isAuto: true
    };
  }
  handleApplicableFrom = date => {
    let { formData } = this.state;
    formData["applicable_from"] = date;
    this.setState({
      formData
    });
  };

  editTemplate=()=>{
    this.props.history.push("/app/mail_templates")
  }

  updateData = () => {
    let { formData } = this.state;
    let e_basic = formData.basic;
    let e_hra = formData.hra;
    let e_conveyance = formData.conveyance;
    let e_medical_allowance = formData.medical_allowance;
    let e_special_allowance = formData.special_allowance;
    let e_arrear = formData.arrear;
    let e_epf = formData.epf;
    let saved_tax_amount = formData.saved_tax_amount;
    let e_misc_deduction = formData.misc_deduction;
    let e_tds = formData.tds;
    // let e_eighty_c_deduction = formData.eighty_c_deduction;
    // let actual_salary = formData.total_salary;
    let n_total_deduction = +e_epf + +e_tds + +e_misc_deduction;
    let n_total_earning =Math.floor(
      +e_basic +
      +e_hra +
      +e_conveyance +
      +e_medical_allowance +
      +e_special_allowance +
      +e_arrear +
      +saved_tax_amount);
    let n_total_salary = n_total_earning;
    this.setState({
      formData: {
        ...this.state.formData,
        total_earning: n_total_earning,
        total_salary: n_total_earning,
        total_deduction: formData.total_salary?formData.total_salary:n_total_deduction,
        increment_amount:
          this.props.latest_salary_literally &&
          this.props.latest_salary_literally.test &&
          this.props.latest_salary_literally.test.total_salary
            ? n_total_salary -
              this.props.latest_salary_literally.test.total_salary
            : 0
      }
    });
  };

  componentWillReceiveProps(props) {
    let { formData } = this.state;  
    if (formData.user_id != props.userid) {
      let data = {
        user_id: props.userid
      };
      this.setState({
        formData: { ...this.state.formData, ...data }
      });
    }
  }
  componentDidUpdate(preprops) {
    if(preprops.prev_sal !== this.props.prev_sal ) {
      let value = this.props.prev_sal&&this.props.prev_sal[this.props.prev_sal.length-1]
      this.props.prev_sal ? this.setState({
        formData:{
          ...this.state.formData,
          basic:value.Basic,
          hra:value.HRA,
          conveyance:value.Conveyance,
          medical_allowance:value.Medical_Allowance,
          special_allowance:value.Special_Allowance,
          misc_deduction:value.Misc_Deductions,
          leave:value.test.leaves_allocated,
          total_earning:value.total_net_salary
        },isAuto:false
      }):
      this.setState({
        formData:{
          ...this.state.formData,
          basic:0,
          hra:0,
          conveyance:0,
          medical_allowance:0,
          special_allowance:0,
          misc_deduction:0,
          leave:0,
          total_earning:0
        },isAuto:true
      })
  }
  }

  addSalary = () => {
    if(this.props.addTimes < 2) {
    let dataObject = _.cloneDeep(this.state.formData);
    delete dataObject.special_allowance_backup;
    delete dataObject.saved_tax_amount;
    dataObject.applicable_from = moment(dataObject.applicable_from).format(
      "YYYY-MM-DD"
    )
    this.props.callAddUserSalary(dataObject); 
    }
  };
  onClose = () => {
    this.props.toggle();
  };

  onValueChange = e => {
    if (
      e.target.name === "misc_deduction" ||
      e.target.name === "eighty_c_deduction"
    ) {
      this.setState({
        formData: { ...this.state.formData, [e.target.name]: e.target.value,total_salary:Number(this.state.formData.total_salary)!==0?this.state.formData.total_salary:this.state.formData.total_earning }
      });
    } else {
      this.setState(
        {
          formData: { ...this.state.formData, [e.target.name]: e.target.value }
        },
        () => {
          this.updateData();
        }
      );
    }
  };

  onMiscBlur = e => {
    let misc_deduction = e.target.value ? e.target.value : 0;
    let special_allowance =
      Number(this.state.formData.special_allowance_backup) - e.target.value;
    this.setState(
      {
        formData: { ...this.state.formData, special_allowance, misc_deduction }
      },
      () => {
        this.updateData();
      }
    );
  };

  onHraClaimblur = e => {
    let { formData } = this.state;
    let hra_claim = e.target.value ? e.target.value : 0;
    formData.hra_claim = hra_claim * 1;
    this.setState({ formData }, () => {
      this.handleTotalChange();
    });
  };

  onStandardDeductionblur = e => {
    let { formData } = this.state;
    let standard_deduction = e.target.value ? e.target.value : 0;
    formData.standard_deduction = standard_deduction * 1;
    this.setState({ formData }, () => {
      this.handleTotalChange();
    });
  };

  on80Cblur = e => {
    let { formData } = this.state;
    let eighty_c_deduction = e.target.value ? e.target.value : 0;
    formData.eighty_c_deduction = eighty_c_deduction * 1;
    this.setState({ formData }, () => {
      this.handleTotalChange();
    });
  };

  handleChange = e => {
    let { formData } = this.state;
    formData[e.target.name] = e.target.value;
    this.setState({ formData });
  };

  handlerClick = name => {
    this.setState({
      formData: {
        ...this.state.formData,
        notifySalaryUpdateToEmp: !this.state.formData.notifySalaryUpdateToEmp
      }
    });
  }

  _getTaxBreakUp = ({
    amount,
    hraClaim,
    standardDeduction,
    eightcDeduction
  }) => {
    let taxConfig = MainCONFIG.CONFIG.taxConfig;
    let educationCessPercentage = taxConfig.educationCessPercentage;
    let pendingTaxableAmount =
      amount - hraClaim - standardDeduction - eightcDeduction;
    let breakUp = [];
    let totalTaxableSalary = 0;
    let totalTax = 0;
    let totalEducationCess = 0;
    for (let k in taxConfig.taxRange) {
      let taxData = taxConfig.taxRange[k];
      let taxRangeLimit = taxData.max - taxData.min;
      let taxableAmount = taxRangeLimit;
      if (pendingTaxableAmount <= taxRangeLimit) {
        taxableAmount = pendingTaxableAmount;
      }
      if (taxableAmount > 0) {
        taxData.taxCalculated = (taxData.rate / 100) * taxableAmount;
        totalTax += taxData.taxCalculated;
      }
      if (k != 0) {
        totalTaxableSalary += taxableAmount;
      }
      pendingTaxableAmount = pendingTaxableAmount - taxableAmount;
      taxData.taxableAmount = taxableAmount;
      breakUp.push(taxData);
    }

    // educatiom cess calculation
    if (totalTax > 0) {
      totalEducationCess = (educationCessPercentage / 100) * totalTax;
      totalTax += totalEducationCess;
    }

    return {
      totalYearlySalary: amount,
      totalTax: totalTax, // this also includes education cess
      totalEducationCess: totalEducationCess,
      totalMonthlyEducationCess:
        totalEducationCess > 0 ? totalEducationCess / 12 : 0,
      totalTaxMonthly: totalTax > 0 ? totalTax / 12 : 0,
      totalTaxableSalary: totalTaxableSalary,
      breakUp: breakUp,
      standardDeduction: standardDeduction,
      eightcDeduction: eightcDeduction
    };
  };

  handleTotalChange = e => {
    let salaryAmt = e ? e.target.value : this.state.formData.total_salary;
    // let taxableAmount =
    //   salaryAmt && salaryAmt > 0 ? salaryAmt - 50000 / 12 : null;
    let { formData } = this.state;
    let config = getConfig(salaryAmt, formData.isEpf);

    let taxBreakUp = this._getTaxBreakUp({
      amount: salaryAmt * 12,
      hraClaim: formData.hra_claim,
      standardDeduction: formData.standard_deduction,
      eightcDeduction: formData.eighty_c_deduction
    });
    this.setState({
      taxBreakUp: taxBreakUp
    });

    formData.basic = salaryAmt * config.basic;
    formData.hra = salaryAmt * config.hra;
    formData.conveyance = salaryAmt * config.conveyance;
    formData.medical_allowance = salaryAmt * config.medical_allowance;
    formData.special_allowance = salaryAmt * config.special_allowance;
    formData.special_allowance_backup = salaryAmt * config.special_allowance;
    
    // TDS is currently removed from the system, setting it to default 0 --- rohit
    // formData.tds = taxBreakUp.totalTaxMonthly;
    formData.tds = 0;
    
    formData.total_salary = salaryAmt;
    this.setState({ formData }, () => {
      this.updateData();
    });
  };

  render() {
    let { formData, taxBreakUp } = this.state;
    // let styles = _.cloneDeep(this.constructor.styles);
    // let date = this.state.applicable_from;
    let opt = [];
    for (var i = 1; i <= 24; i++) {
      opt.push(
        <option key={i} value={i}>
          {i} months
        </option>
      );
    }
    // let educationCessPercentage =
    //   MainCONFIG.CONFIG.taxConfig.educationCessPercentage;
    // console.log(this.state.isAuto, formData,
    //  this.state.formData.total_earning);
    
    return (
      <div className="col-md-12 p-0">
        {this.props.openForm ? (
          <>
            <div className=" col-md-12 p-0 salary-blocks-margin salary-row-bg add-salary p-2 bg-white pl">

             <div className="col-xs-12 basicInfo p-0 pb-2">
             <div className="d-flex justify-content-between pb-2">
                <div><span
                    className="glyphicon glyphicon-remove slaray_close "
                    onClick={this.onClose}
                  ></span></div>
                </div>
          <div className="col-sm-12 col-lg-3 input-wrapper pl-0">
            <label className="addEmployee-label" for="dateOfJoining">
            Applicable From :
            </label>
            <div className="datepicker holding_datepicker">
            <GenericDatePicker
                    handleDateChange={this.handleApplicableFrom}
                    type={this.props.isMobile < 768 ? "absolute" : "inline"}
                    date={formData["applicable_from"]}
                  />
            </div>
          </div>
          <div className="col-sm-12 col-lg-3 input-wrapper">
            <label className="addEmployee-label"> Applicable Months :</label>
            <div className="datepicker">
            <select
                    className="form-control holding_form_view"
                    name="applicable_month"
                    value={formData["applicable_month"]}
                    onChange={this.handleChange}
                  >
                    <option value="">Select</option>
                    {opt}
                  </select>
            </div>
          </div>
          <div className="col-sm-12 col-lg-3 input-wrapper pl-0">
            <label className="addEmployee-label" for="dateOfJoining">
            Leaves :
            </label>
            <div className="datepicker">
            <input
                    type="text"
                    className="form-control leave-field px-2 holding_form_view"
                    name="leave"
                    onChange={this.handleChange}
                    value={formData["leave"]}
                  />
            </div>
          </div>
          {this.props.prev_sal&&this.props.prev_sal.length > 0 ?
          <div className="col-sm-12 col-lg-3 input-wrapper pr-0">
            <label className="addEmployee-label"> Incremented Amount :</label>
            <div className="datepicker">
            <input
                    type="text"
                    className="form-control px-2 holding_form_view"
                    onChange={e =>
                      this.setState({ increment_amount: e.target.value })
                    }
                    value={formData.increment_amount}
                    readOnly
                  />
            </div>
          </div> : "" }
        </div>
                <div className="col-md-12 col-sm-10 col-xs-12 salary-add-block add-salary-addition-width p-0">
                  <div className="col-md-12 px-0">
                    <div className="col-sm-12 salary-total-title">
                      TOTAL <span className="font-weight-bold">EARNING</span>
                    </div>
                    <div className="col-md-12 p-2 d-flex">
                      <div className=" salary-total-value px-2"> Total</div>
                      <div className=" salary-total-value add_total_value">
                        <input
                          type="text"
                          className="form-control input-sm px-2 w-50"
                          name="total_salary"
                          readOnly={!this.state.isAuto}
                          // onChange={this.handleChange}
                          onChange={this.handleTotalChange}
                          value={
                            this.state.isAuto
                              ? formData["total_salary"]
                              : Math.ceil(this.state.formData.total_earning)
                          }
                        />
                      </div>
                      <div className="col salary-total-value">
                        &#8377; {parseFloat(formData.total_earning).toFixed(2)}
                      </div>
                      <div className="d-flex">
                        <div className="px-2">Apply Auto </div>
                        <ToggleButton
                          value={this.state.isAuto}
                          onToggle={() => {
                            this.setState({
                              isAuto: !this.state.isAuto
                            });
                          }}
                        />
                      </div>
                    </div>
                  </div>
                  <div className="earning_detail_step col-md-12 p-0">
                    <div className="col-sm-12 botm-mar padingrl ">
                      {/* <div className="col-sm-2 cell salary-basic-width"> */}
                      <div className="col-sm-10 salary-title">Basic</div>
                      <div className="col-sm-2 salary-input-wrapper">
                        <input
                          name="basic"
                          type="text"
                          className="form-control input-sm px-2"
                          ref="basic"
                          onChange={this.onValueChange}
                          readOnly={this.state.isAuto}
                          value={formData.basic}
                        />
                      </div>
                      {/* </div> */}
                    </div>
                    <div className="col-sm-12 botm-mar padingrl">
                      <div className="col-sm-10 salary-title">HRA</div>
                      <div className="col-sm-2 salary-input-wrapper">
                        <input
                          name="hra"
                          type="text"
                          className="form-control input-sm px-2"
                          ref="hra"
                          onChange={this.onValueChange}
                          readOnly={this.state.isAuto}
                          value={formData.hra}
                        />
                      </div>
                    </div>
                    <div className="col-sm-12 botm-mar padingrl">
                      <div className="col-sm-10 salary-title">Conveyance</div>
                      <div className="col-sm-2 salary-input-wrapper">
                        <input
                          name="conveyance"
                          type="text"
                          className="form-control input-sm px-2"
                          ref="conveyance"
                          onChange={this.onValueChange}
                          readOnly={this.state.isAuto}
                          value={formData.conveyance}
                        />
                      </div>
                    </div>
                    <div className="col-sm-12 botm-mar padingrl">
                      <div className="col-sm-10 salary-title">
                        Medical Allowance
                      </div>
                      <div className="col-sm-2 salary-input-wrapper">
                        <input
                          name="medical_allowance"
                          type="text"
                          className="form-control input-sm px-2"
                          ref="medical_allowance"
                          onChange={this.onValueChange}
                          readOnly={this.state.isAuto}
                          value={formData.medical_allowance}
                        />
                      </div>
                    </div>
                    <div className="col-sm-12 botm-mar padingrl">
                      <div className="col-sm-10 salary-title">
                        Special Allowance
                      </div>
                      <div className="col-sm-2 salary-input-wrapper">
                        <input
                          name="special_allowance"
                          type="text"
                          className="form-control input-sm px-2"
                          ref="special_allowance"
                          onChange={this.onValueChange}
                          readOnly={this.state.isAuto}
                          value={formData.special_allowance}
                        />
                      </div>
                    </div>
                    {/* <div className="col-sm-2 cell salary-arrears-width">
              <div className="col-sm-12 salary-title">Arrears</div>
              <div className="col-sm-12 salary-input-wrapper">
                <input name="arrear" type="text" className="form-control input-sm" ref="arrear" onChange={this.onValueChange} value={formData.arrear} />
              </div>
            </div> */}
                    <br />

                    {/*
            <div className="col-sm-12">
              <div className="col-sm-12 salary-total-title">Deductions</div>
              <div className="col-sm-12 salary-total-value">&#8377; {parseFloat(formData.total_deduction).toFixed(2)}</div>
            </div>
          */}

                    <div className="col-sm-12 botm-mar padingrl text-danger">
                      <div className="col-sm-10 salary-title">
                        Misc Deductions
                      </div>
                      <div className="col-sm-2 salary-input-wrapper">
                        <input
                          name="misc_deduction"
                          type="text"
                          className="form-control input-sm text-danger px-2"
                          ref="misc_deduction"
                          onChange={this.onValueChange}
                          // onChange={this.onMiscBlur}
                          readOnly={this.state.isAuto}
                          value={formData.misc_deduction}
                          // disabled={
                          //   Number(formData["total_salary"]) > 0 ? false : true
                          // }
                        />
                      </div>
                    </div>

                    {/* <div className="col-sm-12 botm-mar padingrl text-info">
                      <div className="col-sm-10 salary-title">EPF</div>
                      <div className="col-sm-2 salary-input-wrapper">
                        <input
                          name="epf"
                          type="text"
                          className="form-control input-sm text-info px-2"
                          ref="epf"
                          onChange={this.onValueChange}
                          value={formData.epf}
                          readOnly={this.state.isAuto}
                        />
                      </div>
                    </div> */}
                  </div>
                </div>

                {/*TDS calulation*/}
                {/* <div className="col-sm-12 cell center salary-options-width add-salary-container py-3">
              <i className="material-icons add-icon" onClick={() => this.addSalary()}>
                add_circle_outline
              </i>
            </div> */}
            <div></div>
              
              <div className="col-sm-12 notify-salary-block mt-2 d-flex align-items-center">
              <div>
                <button
                    className="btn btn-success addsalary-btn"
                    onClick={() => this.addSalary()}
                  >
                    Submit
                  </button></div>

              <div className="pl pt-2">
                <Checkbox id="notifySalaryUpdateToEmp"
                  name="notifySalaryUpdateToEmp"
                  checked={this.state.formData.notifySalaryUpdateToEmp}
                  onClick={this.handlerClick}
                />
                <label htmlFor="notifySalaryUpdateToEmp" className="pr-3">
                  Notify Employee about Salary Update
                </label></div> 
                <small className="py-1 editLink" onClick={this.editTemplate}>(Edit)</small>    
                </div>
                
              </div>
          </>
        ) : (
          ""
        )}
      </div>
    );
  }
}

AddSalaryForm.styles = {
  leaveDiv: {
    marginBottom: "10px"
  }
};

export default compose(isMobile)(AddSalaryForm);
