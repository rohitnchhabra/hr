import React from "react";
import * as _ from "lodash";
import moment from "moment";
import { compose } from "redux";
import isMobile from "../../../../components/hoc/WindowResize";
import GenericDatePicker from "../../../../components/generic/GenericDatePicker";

class AddHoldingForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      user_id: "",
      holding_from: new Date(),
      holding_till: "",
      holding_amount: "",
      reason: ""
    };
    this.handleHoldingFrom = this.handleHoldingFrom.bind(this);
  }
  handleHoldingFrom(date) {
    this.setState({
      holding_from: date
    });
  }
  componentWillReceiveProps(props) {
    if (!_.isEqual(this.props, props)) {
      let holding_from = new Date();
      let holding_till = "";
      let holding_amount = "";
      let reason = "";
      this.setState({
        user_id: props.userid,
        holding_from: holding_from,
        holding_till: holding_till,
        holding_amount: holding_amount,
        reason: reason
      });
    }
  }

  addHolding = () => {
    let dataObject = _.cloneDeep(this.state);
    dataObject.holding_from = moment(dataObject.holding_from).format(
      "YYYY-MM-DD"
    );
    this.props.callAddUserHolding(dataObject);
  };
  render() {
    let opt = [];
    for (var i = 1; i <= 24; i++) {
      opt.push(
        <option key={i} value={i}>
          {i} months
        </option>
      );
    }
    return (
      <div className="row salary-blocks-margin salary-row-bg mx-0 holding_salaries_view">
        <div className="col-xs-12 basicInfo p-0 pb-2">
          <div className="col-sm-12 col-lg-3 input-wrapper">
            <label className="addEmployee-label" for="dateOfJoining">
              Holding From :
            </label>
            <div className="datepicker holding_datepicker">
              <GenericDatePicker
                date={this.state.holding_from}
                handleDateChange={this.handleHoldingFrom}
                type={this.props.isMobile < 768 ? "absolute" : "inline"}
              />
            </div>
          </div>
          <div className="col-sm-12 col-lg-3 input-wrapper">
            <label className="addEmployee-label"> Applicable Months :</label>
            <div className="datepicker">
              <select
                className="form-control pl-2 holding_form_view"
                value={this.state.holding_till}
                onChange={e => this.setState({ holding_till: e.target.value })}
              >
                <option value="">Select Month</option>
                {opt}
              </select>
            </div>
          </div>
          <div className="col-sm-12 col-lg-3 input-wrapper">
            <label className="addEmployee-label" for="dateOfJoining">
              Holding Amount :
            </label>
            <div className="datepicker">
              <input
                type="text"
                className="form-control pl-2 holding_form_view"
                ref="holding_amount"
                onChange={() =>
                  this.setState({
                    holding_amount: this.refs.holding_amount.value
                  })
                }
                value={this.state.holding_amount}
              />
            </div>
          </div>
          <div className="col-sm-12 col-lg-3 input-wrapper">
            <label className="addEmployee-label"> Reason :</label>
            <div className="datepicker">
              <input
                type="text"
                className="form-control pl-2 holding_form_view"
                ref="reason"
                onChange={() =>
                  this.setState({ reason: this.refs.reason.value })
                }
                value={this.state.reason}
              />
            </div>
          </div>
        </div>
        <div className="col-sm-1 col-sm-offset-11 text-center pr-0">
        <button
                    className="btn btn-success"
                    onClick={() => this.addHolding()}
                  >
                    ADD
                  </button>
        </div>
      </div>
    );
  }
}

AddHoldingForm.styles = {
  leaveDiv: {
    marginBottom: "10px"
  }
};
export default compose(isMobile)(AddHoldingForm);
