import React from "react";
import { connect } from "react-redux";
import { withRouter } from "react-router";
import * as _ from "lodash";
import { isNotUserValid } from "../../../services/generic";
import * as actions from "../../../redux/actions";
import * as actions_salary from "../../../redux/salary/actions/viewSalary";
import { Row, Col, Table } from "reactstrap";
import NewSalary from "../components/NewSalary";
import { dateFormatter, isMobileDevice } from "../../../helper/helper";
import Heading from "../../../components/generic/Heading";
import cloneDeep from "lodash/cloneDeep";
class Salary extends React.Component {
  constructor(props) {
    super(props);
    this.viewSalarySummary = this.viewSalarySummary.bind(this);
    this.state = {
      view_salary_id: false,
      salary_details: {},
      holding_amt: "",
      payslip_history: []
    };
  }
  componentWillMount() {
    this.props.onSalaryDetails();
    this.props.getGenricLineConfig();
    this.props.showHeading("My Salary")
  }
  componentWillReceiveProps(props) {
    let isNotValid = isNotUserValid(
      this.props.location.pathname,
      props.loggedUser
    );
    if (isNotValid.status) {
      this.props.history.push(isNotValid.redirectTo);
    }

    let s_salary_details = {};
    let s_salary_history = [];
    let s_payslip_history = [];

    if (this.state.view_salary_id === false) {
      if (
        typeof props.salary.salary_history !== "undefined" &&
        props.salary.salary_history.length > 0
      ) {
        let viewSalaryInfo = props.salary.salary_history[0];
        s_salary_details = viewSalaryInfo;
        s_salary_history = props.salary.salary_history;
      }
      if (
        typeof props.salary.payslip_history !== "undefined" &&
        props.salary.payslip_history.length > 0
      ) {
        s_payslip_history = props.salary.payslip_history;
      }
    }

    this.setState({
      salary_details: s_salary_details,
      salary_history: s_salary_history,
      payslip_history: s_payslip_history
    });
  }

  viewSalarySummary(id) {
    let new_details = this.state.salary_details;
    _.forEach(this.state.salary_history, (d, k) => {
      if (d.test.id === id) {
        new_details = d;
      }
    });
    this.setState({ salary_details: new_details });
  }
  getDataSeries(cdata) {
    // eslint-disable-line
    let data = cloneDeep(cdata);
    return [
      {
        label: "Total Salary",
        data: data[0],
        bars: {
          show: true,
          barWidth: 12 * 24 * 60 * 60 * 300,
          fill: 0.5,
          lineWidth: 0,
          order: 1
        }
      },
      {
        label: "Recieved Salary",
        data: data[1],
        bars: {
          show: true,
          barWidth: 12 * 24 * 60 * 60 * 300,
          fill: 1,
          lineWidth: 0,
          order: 2
        }
      },
      {
        label: "Tax",
        data: data[2],
        bars: {
          show: true,
          barWidth: 12 * 24 * 60 * 60 * 300,
          fill: 0.5,
          lineWidth: 0,
          order: 3
        }
      },
      {
        label: "Deducation",
        data: data[3],
        bars: {
          show: true,
          barWidth: 12 * 24 * 60 * 60 * 300,
          fill: 0.5,
          lineWidth: 0,
          order: 4
        }
      }
    ];
  }
  getData = () => {
    let Chartdata = [[], [], [], []];
    if (
      this.props.salary.payslip_history &&
      this.props.salary.payslip_history.length
    ) {
      this.props.salary.payslip_history.slice(0, 12).forEach((item, index) => {
        var newDate = dateFormatter().months[parseInt(item.month) - 1];
        Chartdata[0].push([newDate, item.total_earning]);
        Chartdata[1].push([newDate, item.total_net_salary]);
        Chartdata[2].push([newDate, item.misc_deduction_2]);
        Chartdata[3].push([newDate, item.total_deductions]);
      });
      return Chartdata;
    }
  };
  render() {
    const { payslip_history } = this.props.salary;
    return (
      <div className="my-salary-view">
        <Row className="no-gutter salary-view-point mx-0">
          <Col className="px-0" md={12} xl={12}>
            <Heading cardHeader title={"SALARY"} boldText={"DETAIL"} />
            {/* <div className="box pl-3 pt-3">
              {this.props.salary.salary_history &&
              this.props.salary.salary_history.length ? (
                <BarChart getData={this.getData} getDataSeries={this.getDataSeries} y_axis_ticker={4000} x_axis_length={1} />
              ) : null}
            </div> */}
          </Col>
        </Row>
        <Row className="mx-0">
          <Col className="px-0">
            <NewSalary
              isMobile={
                isMobileDevice() ||
                this.props.showSalary.webShowSalary.toString() === "1"
              }
              salaries={
                this.props.salary.salary_history
                  ? this.props.salary.salary_history
                  : []
              }
            />
          </Col>
        </Row>
        {isMobileDevice() ||
        this.props.showSalary.webShowSalary.toString() === "1" ? (
          <Row className="no-gutter">
            <Col xl={12}>
              <Heading cardHeader title={"EMPLOYEE"} boldText={"PAYSLIPS"} />
            </Col>
            <Col xl={12}>
              <div className="box employee-payslips-wrapper">
                <Table className="text-center table-lg mb-0 table-responsive d-lg-table mySalary_table">
                  <thead>
                    <tr className="my_salary_table_body">
                      <th>Pay Date</th>
                      <th>Total Earnings (Rs.)</th>
                      <th>Taxes</th>
                      <th>Total Deductions (Rs.)</th>
                      <th>Net Salary</th>
                    </tr>
                  </thead>
                  <tbody className="my_salary_table_body">
                    {this.props.salary.payslip_history &&
                    this.props.salary.payslip_history.length ? (
                      this.props.salary.payslip_history.map((v, i) => (
                        <tr key={i}>
                          <td>
                            {dateFormatter().months[parseInt(v.month) - 1]},{" "}
                            {v.year}
                          </td>
                          <td>{v.total_earnings ? v.total_earnings : "-"}</td>
                          <td>{v.total_taxes ? v.total_taxes : "-"}</td>
                          <td>
                            {v.total_deductions ? v.total_deductions : "-"}
                          </td>
                          <td>
                            {v.total_net_salary ? v.total_net_salary : "-"}
                          </td>
                        </tr>
                      ))
                    ) : (
                      <tr>
                        <td colspan="6" className="salary_not_aval">
                          No Available Previous Payslip
                        </td>
                      </tr>
                    )}
                  </tbody>
                </Table>
              </div>
            </Col>
          </Row>
        ) : null}
      </div>
    );
  }
}

Salary.styles = {
  height100per: {
    minHeight: "150px"
  }
};

function mapStateToProps(state) {
  return {
    frontend: state.frontend.toJS(),
    loggedUser: state.logged_user.userLogin,
    salary: state.salary.toJS(),
    showSalary: state.settings.showSalary
  };
}
const mapDispatchToProps = dispatch => {
  return {
    onSalaryDetails: () => {
      return dispatch(actions_salary.getSalaryDetails());
    },
    getGenricLineConfig: () => dispatch(actions.getGenricLoginConfig()),
    showHeading:(data)=>{
      return dispatch(actions.showHeading(data));
    }
  };
};

const VisibleSalary = connect(mapStateToProps, mapDispatchToProps)(Salary);

const RouterVisibleSalary = withRouter(VisibleSalary);

export default RouterVisibleSalary;
