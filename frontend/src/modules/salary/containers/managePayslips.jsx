import React from "react";
import { connect } from "react-redux";
import { withRouter } from "react-router";
import queryString from "query-string";
import * as _ from "lodash";
import { notify } from "../../../services/notify";
import { isNotUserValid } from "../../../services/generic";
import Heading from "../../../components/generic/Heading";
import ManagePayslipsUsersList from "../../../modules/salary/components/managePayslips/ManagePayslipsUsersList";
import FormGeneratePaySlip from "../../../modules/salary/components/managePayslips/FormGeneratePaySlip";
import EmployeeActualSalary from "../../../modules/salary/components/managePayslips/EmployeeActualSalary";
import UserPayslipsHistory from "../../../components/salary/managePayslips/UserPayslipsHistory";
import DesktopUserList from "../../../components/UserList/DesktopUserList";
import * as actions from "../../../redux/actions";
import * as actions_usersList from "../../../redux/generic/actions/usersList";
import * as actions_managePayslips from "../../../redux/salary/actions/managePayslips";
import Menu from "../../../components/generic/Menu";
import classnames from "classnames";
import ClearFix from "material-ui/internal/ClearFix";

class ManagePayslips extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      year: "",
      month: "",
      selected_user_name: "",
      selected_user_image: "",
      selected_user_jobtitle: "",
      selected_user_id: "",
      defaultUserDisplay: "",
      user_data_for_payslip: {},
      user_payslip_history: [],
      google_drive_emailid: "",
      employee_actual_salary: {},
      selectAll: false,
      checked: [],
      prev_UserName:'',
      prev_UserId:'',
      prev_UserJob:'',
      checkedValue:false
    };

    this.saveArrear = this.saveArrear.bind(this);
  }
  componentWillMount() {
    this.props.onUsersList();
    this.props.showHeading("Payslips")
  }
  componentWillReceiveProps(props) {
    let isNotValid = isNotUserValid(
      this.props.location.pathname,
      props.loggedUser
    );
    if (isNotValid.status) {
      this.props.history.push(isNotValid.redirectTo);
    }
    let s_google_drive_emailid = "";
    let s_user_data_for_payslip = {};
    let s_user_payslip_history = [];
    let s_all_users_latest_payslip = [];
    let s_employee_actual_salary = {};

    if (typeof props.managePayslips.user_data_for_payslip !== "undefined") {
      s_user_data_for_payslip = props.managePayslips.user_data_for_payslip;
    }

    if (typeof props.managePayslips.user_payslip_history !== "undefined") {
      s_user_payslip_history = props.managePayslips.user_payslip_history;
    }

    if (typeof props.managePayslips.all_users_latest_payslip !== "undefined") {
      s_all_users_latest_payslip =
        props.managePayslips.all_users_latest_payslip;
    }

    if (typeof props.managePayslips.google_drive_emailid !== "undefined") {
      s_google_drive_emailid = props.managePayslips.google_drive_emailid;
    }

    if (typeof props.managePayslips.employee_actual_salary !== "undefined") {
      s_employee_actual_salary = props.managePayslips.employee_actual_salary;
    }

    this.setState({
      user_data_for_payslip: s_user_data_for_payslip,
      user_payslip_history: s_user_payslip_history,
      all_users_latest_payslip: s_all_users_latest_payslip,
      google_drive_emailid: s_google_drive_emailid,
      employee_actual_salary: s_employee_actual_salary
    });
   
  }

  componentDidUpdate (props) {
    // window.scrollTo(0,0);
    if (this.state.defaultUserDisplay == "") {
      if (this.props.usersList.users.length > 0)  {
        let firstUser = this.props.usersList.users[0];
        const queryParams = queryString.parse(this.props.location.search);
        let defaultUserId = queryParams.selectedUser || firstUser.user_Id;
        this.onUserClick(defaultUserId);
      }
    }
    const {managePayslips} = this.props;
    const {employee_missing_timings_loading, employee_missing_timings} = managePayslips;
    if(employee_missing_timings_loading !==props.managePayslips.employee_missing_timings_loading){
      const {message} = employee_missing_timings;
      if(message){
        notify(message);
      }
    }
  }

  onUserClick = userid => {
    let selected_user_name = "";
    let selected_user_image = "";
    let selected_user_jobtitle = "";
    let selected_user_id = "";
    this.setState({
      prev_UserName:this.state.selected_user_name,
      prev_UserId:this.state.selected_user_id,
      prev_UserJob:this.state.selected_user_jobtitle
    })
    if (this.props.usersList.users.length > 0) {
      let userDetails = _.find(this.props.usersList.users, { user_Id: userid });
      if (typeof userDetails !== "undefined") {
        this.props.setUserPayslipMonthYearRequest({ 
          month:"",
          year:"",
          userid,
          "username":userDetails.name,
          "userImage":userDetails.slack_profile.image_192,
          "jobTitle":userDetails.jobtitle })
        selected_user_name = userDetails.name;
        selected_user_image = userDetails.slack_profile.image_192;
        selected_user_jobtitle = userDetails.jobtitle;
        selected_user_id = userDetails.user_Id;
        this.props.history.push(
          "manage_payslips?selectedUser=" + userDetails.user_Id
        );
      }
    }
    this.setState({
      defaultUserDisplay: userid,
      selected_user_name: selected_user_name,
      selected_user_image: selected_user_image,
      selected_user_jobtitle: selected_user_jobtitle,
      selected_user_id: selected_user_id
    });
    let d = new Date();
    let year = d.getFullYear();
    let month = d.getMonth() + 1;
    if (this.state.year == "" && this.state.month == "") {
      this.props.onUserManagePayslipsData(userid);
    } else {
      this.props.onUserMonthlyManagePayslipsData(
        userid
      );
    }
  };
  saveArrear = (user_id, extra_arrear, arrear_for_month) => {
    this.props.onCreateArrear(user_id, extra_arrear, arrear_for_month);
  };

  callCreateUserPayslip = (payslipData, evt) => {
    this.setState({
      prev_UserName:this.state.selected_user_name,
      prev_UserId:this.state.selected_user_id,
      prev_UserJob:this.state.selected_user_jobtitle
    })
    evt.preventDefault();
    this.props.onCreateUserPayslip(payslipData).then(
      data => {
        this.onUserClick(this.state.selected_user_id);
      },
      error => {
        notify(error);
      }
    );
  };

  callMonthlyPayslip = (userid, year, month) => {
    this.setState({ year: year, month: month });
    let userDetails = _.find(this.props.usersList.users, { user_Id: userid });
    if (typeof userDetails !== "undefined") {
      this.props.setUserPayslipMonthYearRequest({ 
        month,
        year,
        userid,
        "username":userDetails.name,
        "userImage":userDetails.slack_profile.image_192,
        "jobTitle":userDetails.jobtitle })
    }
    this.props.onUserMonthlyManagePayslipsData(userid, year, month);
  };

  callEmailPayslips = ids => {
    if (ids.length == 0) {
      notify("Select an employee!!!");
    } else {
      this.props.onEmailPayslips(ids).then(
        data => {
          notify(data);
          this.props.onUsersList();
          this.onUserClick(this.state.selected_user_id);
        },
        error => {
          notify(error);
        }
      );
    }
  };

  responseGoogle = response => {
    let accessToken = response.getAuthResponse().access_token;
    if (accessToken == "") {
      notify("Access token is empty!!!");
    } else {
      this.props.onSaveGoogleAccessToken(accessToken).then(
        data => {
          notify(data);
        },
        error => {
          notify(error);
        }
      );
    }
  };
  onSelectAll = () => {
    this.setState(prevState => ({ selectAll: !prevState.selectAll }));
    const check = [];
    if (!this.state.selectAll) {
      this.props.usersList.users.map(user => {
        check.push(user.user_Id);
      });
    }
    this.setState({ checked: check },()=>{
      if(this.state.checked.length>0){
        this.setState({checkedValue:true})
      }
      else{
        this.setState({checkedValue:false})
      }
    });
  };
  onCheckboxClick = id => {
    if (this.state.checked.includes(id)) {
      this.setState({ checked: this.state.checked.filter(c => c !== id) },()=>{
        if(this.state.checked.length > 0) {
          this.setState({checkedValue:true})
        }
        else{
          this.setState({checkedValue:false})
        }
      });
      
    } else {
      this.setState({ checked: [...this.state.checked, id],checkedValue:true });
    }
  };

  sendMouseHoverDataParent = status => {
    this.setState({
      hoverStatus: status
    });
  };

  handleNotifyEmployeeTimeMissing = () => {
    let loader = this.props.frontend.show_loading; 
    const {userid} = this.props.teamStats.userPayslipMonthYear;
    let userId = loader ? this.state.prev_UserId : userid
    let year=this.state.user_data_for_payslip.year;
    let month=this.state.user_data_for_payslip.month;
      this.props.onNotifyEmpoyeeTimeMissing(userId,year,month)
  }

  render() {
    let payslipMonth = this.state.user_payslip_history.length ? this.state.user_payslip_history[0].month : '';
    let userPayId = this.state.user_payslip_history.length ? this.state.user_payslip_history[0].user_Id: '';
    let loader = this.props.frontend.show_loading; 
    let selectedUserId = "";
    const selectAll = {
      name: "selectAll",
      onClick: this.onSelectAll,
      checked: this.state.selectAll
    };
    const {userid,userImage,jobTitle,username, status} = this.props.teamStats.userPayslipMonthYear;
    let userId = userid
    let name_User = username
    let designation = jobTitle;
    return (
      <div>
        <Menu {...this.props} />
        <div className="d-flex justify-content-end mb-2 payslips-buttons-container">
          <ManagePayslipsUsersList
            users={this.props.usersList.users}
            selectedUserId={this.state.selected_user_id}
            onUserClick={this.onUserClick}
            all_users_latest_payslip={this.state.all_users_latest_payslip}
            callEmailPayslips={this.callEmailPayslips}
            onGetTransferList={this.props.onGetTransferList}
            google_drive_emailid={this.state.google_drive_emailid}
            {...this.props}
            checkedValue={this.state.checkedValue}
            checked={this.state.checked}
          />
        </div>
        <div className="app-body" id="view">
          <div>
            <div className="d-flex">
            
              <div
                className={classnames("px-0 mp-userlist", {
                  active: this.state.hoverStatus
                })}
              >
                {this.props.usersList.users &&
                  this.props.usersList.users.length > 0 && (
                    <DesktopUserList
                      usersList={this.props.usersList.users}
                      selectedUserId={this.state.selected_user_id}
                      onUserClick={this.onUserClick}
                      selectAll={selectAll}
                      selectUsers
                      onCheckboxClick={this.onCheckboxClick}
                      checked={this.state.checked}
                      sendMouseHoverDataParent={this.sendMouseHoverDataParent}
                      userPayId={userPayId}
                      payslipMonth={payslipMonth}
                      payslipData={this.props.managePayslips}
                    />
                  )}
                {/* <ManagePayslipsUsersList users={this.props.usersList.users} selectedUserId={this.state.selected_user_id} onUserClick={this.onUserClick} all_users_latest_payslip={this.state.all_users_latest_payslip} callEmailPayslips={this.callEmailPayslips} onGetTransferList={this.props.onGetTransferList} google_drive_emailid={this.state.google_drive_emailid} {...this.props} /> */}
              </div>
              <div
                className={classnames("pr-0 pl-0 mp-payslips", {
                  active: this.state.hoverStatus
                })}
              >
                <div className="row no-gutter">
                  <div className="col-xs-12">
                    <div className="p-a block box">
                      {/* <h2 className="selected_username">{this.state.selected_user_name}</h2>
                        <h6>Employee ID : {this.state.selected_user_id}</h6> */}
                      {/* <h6 className="text-center">Generate Payslip</h6> */}
                      <FormGeneratePaySlip
                        user_id={userId}
                        name={name_User}
                        year={this.state.year}
                        month={this.state.month}
                        designation={designation}
                        user_data_for_payslip={this.state.user_data_for_payslip}
                        callCreateUserPayslip={this.callCreateUserPayslip}
                        callMonthlyPayslip={this.callMonthlyPayslip}
                        handleNotifyEmployeeTimeMissing={this.handleNotifyEmployeeTimeMissing}
                        pending_leaves={
                          this.props.managePayslips
                            .employee_toshow_pending_leaves
                        }
                        before_Joining={
                          this.props.managePayslips
                            .employee_toshow_days_before_joining
                        }
                        employee_applied_pending_leaves={
                          this.props.managePayslips
                              .employee_applied_pending_leaves
                        }
                        saveArrear={this.saveArrear}
                        actualSalary={this.state.employee_actual_salary}
                        employee_actual_salary={
                          this.state.employee_actual_salary
                        }
                        pending_Payslip={this.props.managePayslips}
                        payslipMonth={payslipMonth}
                      />
                    </div>

                    <div>
                      <div>
                        {/* <h6 className="text-center">
                              <u>EmployeeActualSalary</u>
                            </h6> */}
                        {/* <EmployeeActualSalary employee_actual_salary={this.state.employee_actual_salary} /> */}
                      </div>
                      <div>
                        {/* <h6 className="text-center">
                              <u>Previous Payslips</u>
                            </h6>
                            <hr /> */}
                        <UserPayslipsHistory
                          user_payslip_history={this.state.user_payslip_history}
                        />
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              
            </div>
          </div>
        </div>
        {/* </div> */}
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    frontend: state.frontend.toJS(),
    loggedUser: state.logged_user.userLogin,
    usersList: state.usersList.toJS(),
    managePayslips: state.managePayslips.toJS(),
    teamStats:state.teamStats,
    status:state.teamStats.userPayslipMonthYear.status,
  };
}
const mapDispatchToProps = dispatch => {
  return {
    onUsersList: () => {
      return dispatch(actions_usersList.get_users_list());
    },
    onUserManagePayslipsData: userid => {
      return dispatch(
        actions_managePayslips.get_user_manage_payslips_data(userid)
      );
    },
    onUserMonthlyManagePayslipsData: (userid, year, month) => {
      return dispatch(
        actions_managePayslips.get_user_month_manage_payslips_data(
          userid,
          year,
          month
        )
      );
    },
    onCreateUserPayslip: payslipData => {
      return dispatch(actions_managePayslips.create_user_payslip(payslipData));
    },
    onEmailPayslips: payslips_ids => {
      return dispatch(actions_managePayslips.email_payslips(payslips_ids));
    }, 
    onSaveGoogleAccessToken: accessToken => {
      return dispatch(
        actions_managePayslips.save_google_access_token(accessToken)
      );
    },
    onCreateArrear: (user_id, extra_arrear, arrear_for_month) => {
      return dispatch(
        actions_managePayslips.create_arrear(
          user_id,
          extra_arrear,
          arrear_for_month
        )
      );
    },
    setUserPayslipMonthYearRequest:(payload) => {
      return dispatch(
        actions.setUserPayslipMonthYearRequest(
          payload
        )
      );
    },
    onGetTransferList: userIds => {
      return dispatch(actions_managePayslips.getTransferList(userIds));
    },
    onNotifyEmpoyeeTimeMissing:(userId,year,month) =>{
      return dispatch(actions_managePayslips.get_async_notify_emp_missing_timings(userId,year,month))
    },
    showHeading:(data)=>{
      return dispatch(actions.showHeading(data));
    }
  };
};

const VisibleManagePayslips = connect(
  mapStateToProps,
  mapDispatchToProps
)(ManagePayslips);

const RouterVisibleManagePayslips = withRouter(VisibleManagePayslips);

export default RouterVisibleManagePayslips;
