import React from "react";
import { connect } from "react-redux";
import { withRouter } from "react-router";
import moment from "moment";
import * as _ from "lodash";
import queryString from "query-string";
import { notify , confirm } from "../../../services/notify";
import { CONFIG } from "../../../config/index";
import Menu from "../../../components/generic/Menu";
import { isNotUserValid } from "../../../services/generic";
import Header from "../../../components/generic/Header";
import UsersList from "../../../components/generic/UsersList";
import UsersListHeader from "../../../components/generic/UsersListHeader";
import Heading from "../../../components/generic/Heading";
import UserPicHeadTitle from "../../../components/generic/UserPicHeadTitle";
import AlertFader from "../../../components/generic/AlertFader";
import UserHistoryHolding from "../../../components/salary/manageSalary/UserHistoryHolding";
import AddHoldingForm from "../../../modules/salary/components/manageSalary/AddHoldingForm";
import AddSalaryForm from "../../../modules/salary/components/manageSalary/AddSalaryForm";
import * as actions from "../../../redux/actions";
import * as actions_usersList from "../../../redux/generic/actions/usersList";
import * as actions_manageSalary from "../../../redux/salary/actions/manageSalary";
import SalaryBlock from "../../../components/generic/SalaryBlock";
import { Button,Alert } from "reactstrap";
import { isEmpty, isEqual } from "lodash";
import Dialog from "material-ui/Dialog";
import FlatButton from "material-ui/FlatButton";
class ManageSalary extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      selected_user_name: "",
      selected_user_image: "",
      selected_user_jobtitle: "",
      selected_user_id: "",
      defaultUserDisplay: "",
      salary_history: [],
      holding_history: [],
      user_latest_salary_details: {},
      user_latest_holding_details: {},
      user_date_of_joining: {},
      msg: "",
      current_date: {},
      subList: [],
      openForm:false,
      addTimes:0,
      deleteTimes:0,
      isLeaveAllocatedModal:false,
      previousMonthAllocatedLeaves:'',
      updateAllocatedLeave:'',
      user_id:'',
      action:''
    };

    this.onUserClick = this.onUserClick.bind(this);
    this.callAddUserSalary = this.callAddUserSalary.bind(this);
    this.callAddUserHolding = this.callAddUserHolding.bind(this);
    this.viewSalarySummary = this.viewSalarySummary.bind(this);
    this.callDeleteUserSalary = this.callDeleteUserSalary.bind(this);
  }
  componentWillMount() {
    this.props.onUsersList({ sort_by: "salary" });
    var current_date = moment().format("YYYY-MM-DD");
    this.props.showHeading("Employee Salaries")
  }

  componentWillReceiveProps(props) {
    if (!isEqual(this.props, props)) {
      if (this.props.location.search !== props.location.search) {
        this.onUserClick(
          props.location.search.split("=")[
            props.location.search.split("=").length - 1
          ]
        );
      }
      let isNotValid = isNotUserValid(
        this.props.location.pathname,
        props.loggedUser
      );
      if (isNotValid.status) {
        this.props.history.push(isNotValid.redirectTo);
      }
      this.setState({ subList: this.props.usersList.users });
      let s_salary_history = [];
      let s_user_latest_salary_details = {};
      let s_holding_history = [];
      let s_user_latest_holding_details = {};
      if ((props.manageSalary.salary_structure)) {
        if (
          typeof props.manageSalary.salary_structure.salary_details !==
            "undefined" &&
          props.manageSalary.salary_structure.salary_details
        ) {
          s_salary_history = _.sortBy(
            props.manageSalary.salary_structure.salary_details,
            "date"
          ).reverse();
        }
        if (
          typeof props.manageSalary.salary_structure.holding_details !==
            "undefined" &&
          props.manageSalary.salary_structure.holding_details.length > 0
        ) {
          s_holding_history = props.manageSalary.salary_structure.holding_details.reverse();
          s_user_latest_holding_details = s_holding_history[0];
          this.setState({});
        }
        this.setState({
          salary_history: s_salary_history,
          user_latest_salary_details: s_user_latest_salary_details,
          holding_history: s_holding_history,
          user_latest_holding_details: s_user_latest_holding_details
        });
      }
    }
  }

  componentDidUpdate(props) {
    if (this.state.defaultUserDisplay == "") {
      if (this.props.usersList.users.length > 0) {
        let firstUser =
          this.props.loggedUser.data.role == CONFIG.HR ||
          (this.props.loggedUser.data.role == CONFIG.ADMIN &&
            this.state.subList[0])
            ? this.state.subList[0]
            : this.props.usersList.users[0];
        const queryParams = queryString.parse(this.props.location.search);
        let defaultUserId = queryParams.selectedUser || firstUser.user_Id;
        this.props.onSelectedUser(defaultUserId);
        this.onUserClick(defaultUserId);
      }
    }
    const {
            updated_employee_allocated_leaves_loader,
            updated_employee_allocated_leaves
        } = this.props.manageSalary        
        if(updated_employee_allocated_leaves_loader !== props.manageSalary.updated_employee_allocated_leaves_loader){
          const {message} = updated_employee_allocated_leaves
          if(message){
            notify(message)
          }
        }
  }

  onUserClick(userid) {
    let selected_user_name = "";
    let selected_user_image = "";
    let selected_user_jobtitle = "";
    let selected_user_id = "";
    let selected_user_date_of_joining = "";

    if (this.props.usersList.users.length > 0) {
      let userDetails = _.find(this.props.usersList.users, { user_Id: userid });
      if (typeof userDetails !== "undefined") {
        selected_user_name = userDetails.name;
        selected_user_image = userDetails.profileImage;
        selected_user_jobtitle = userDetails.jobtitle;
        selected_user_id = userDetails.user_Id;
        selected_user_date_of_joining = userDetails.dateofjoining;
        this.props.history.push(
          "manage_salary?selectedUser=" + userDetails.user_Id
        );
      }
    }
    this.setState({
      defaultUserDisplay: userid,
      selected_user_name: selected_user_name,
      selected_user_image: selected_user_image,
      selected_user_jobtitle: selected_user_jobtitle,
      selected_user_id: selected_user_id,
      selected_user_date_of_joining: selected_user_date_of_joining
    });

    this.props.onUserSalaryDetails(userid).then(val => {  
      this.setState({ msg: val.data.message,addTimes:0,deleteTimes:0 });
    });
  }

  callAddUserSalary(new_salary_details) {
    this.props.onAddNewSalary(new_salary_details).then(
      data => this.setState({openForm:false,addTimes:this.state.addTimes+1}) ,
      error => {
        if( Array.isArray(error) && error[0] == "applicable_from_error" ){
          confirm(error[1], error[2], '', '', 'Continue').then((confirm) => {
            if(confirm){
              new_salary_details.force_update = true;
              this.props.onAddNewSalary(new_salary_details).then(
                data => this.setState({openForm:false,addTimes:this.state.addTimes+1}),
                error => {
                  notify(error)
                }
              )
            }
          })
        } else {
          notify(error)
        }
      }
    );
  }

  callAddUserHolding(new_holding_details) {
    this.props.onAddNewHolding(new_holding_details).then(
      data => {},
      error => {
        notify(error);
      }
    );
  }
  viewSalarySummary(e, id) {
    e.stopPropagation();
    let new_details = this.state.salary_details;
    _.forEach(this.state.salary_history, (d, k) => {
      if (d.test.id == id) {
        new_details = d;
      }
    });
    this.setState({ user_latest_salary_details: new_details });
  }
  callDeleteUserSalary(e, user_id, salary_id) {
    if(this.state.deleteTimes < 2) {
    e.stopPropagation();
    confirm("Delete", "Are you sure ?", "error").then((res)=> {
      if(res){
      this.setState({deleteTimes:this.state.deleteTimes+1})
      this.props.onDeleteUserSalary(user_id, salary_id)
    }
    })
  }
  }
  handleOpen = () => {
    this.setState({openForm:true})
  }
  toggle = () => {
    this.setState({openForm:false})
  }

  handleLeaveAllocatedModal=(action)=>{
      const {salary_structure} = this.props.manageSalary
      const {salary_details, payslip_history} = salary_structure
      if(salary_details && salary_details.length && payslip_history && payslip_history.length ){
        let previousMonthAllocatedLeaves = action === 'allocate' ?
                                       salary_details[0].test.leaves_allocated : 
                                       payslip_history[0].final_leave_balance
        this.setState({
          user_id:salary_structure.id,
          action,
          previousMonthAllocatedLeaves,
        },()=>{
          this.setState({
            isLeaveAllocatedModal:!this.state.isLeaveAllocatedModal
          })
        })
      }
  }

  handleSubmitAllocatedLeaves=()=>{
    const {user_id, updateAllocatedLeave, action} = this.state;
    if(updateAllocatedLeave){
      this.setState({
        isLeaveAllocatedModal:false, 
        updateAllocatedLeave:''
      })
      this.props.OnUpdateEmployeeLeavesAllocated(user_id, updateAllocatedLeave,action)
    }
  }
  
  render() {   
    let prev_salary=this.props.manageSalary && this.props.manageSalary.salary_structure && this.props.manageSalary.salary_structure.salary_details
    let data;
    const {action} = this.state;
    const message = this.state.msg;
    if (message) {
      data = (
        <>
          {message}
        </>
      );
    } else {
      data = this.state.salary_history.map((item, index) => {
        return (
          <SalaryBlock
            key={index}
            userid={this.state.selected_user_id}
            callAddUserHolding={this.callAddUserHolding}
            salaryStructure={this.props.manageSalary.salary_structure}
            item={item}
            displayPage="manage"
            viewSalarySummary={this.viewSalarySummary}
            callDeleteUserSalary={this.callDeleteUserSalary}
            handleLeaveAllocatedModal={this.handleLeaveAllocatedModal}
          />
        );
      });
    }
    const actions = [
      <FlatButton label="cencel" secondary style={{marginRight: 5}} onClick={() => {
        this.handleLeaveAllocatedModal()
        // if (this.state.checkValue !== '') {
        //   confirm('Are you sure ?', 'Do you want to delete this Device Type ?', 'warning').then((res) => {
        //     if (res) {
        //       this.handleDelete();
        //     }
        //   });
        // }
      }} />,
      <FlatButton label="Update" primary onClick={this.handleSubmitAllocatedLeaves} style={{marginRight: 5}} />,
    ];
    return (
      <div>
        <Menu {...this.props} />
        <div id="content" className="app-content box-shadow-z0" role="main">
          <Header
            pageTitle={"Manage Salaries"}
            showLoading={this.props.frontend.show_loading}
            userListHeader
          />

          <Dialog
          title={action === 'allocate' ? 'ALLOCATED LEAVES ' : 'FINAL LEAVES BALANCE'}
          titleStyle={{opacity: '0.56'}}
          actions={actions}
          modal={false}
          open={this.state.isLeaveAllocatedModal}
          onRequestClose={this.handleLeaveAllocatedModal}
          autoScrollBodyContent
        >
          <div className="row m-0">
            <div className="row m-0 w-100">
                <label className="allocate-lavel col-form-label">
                    <strong>Previous Month {action === 'allocate' ?  "Allocated Leaves: " : 'Final Leaves Balance: '}</strong>
                </label>
                <div className="">
                  <input
                    type="text"
                    className="form-control w-100"
                    placeholder={action === 'allocate' ? "Allocated Leaves " :"Final Leave Balance"}
                    value={this.state.previousMonthAllocatedLeaves}
                    readOnly
                  />
                </div>
            </div>
            <div className="row m-0 mt-1 w-100">
                <label className="allocate-lavel col-form-label">
                  <strong>{action === 'allocate' ?  "Update Allocated  Leaves: " : 'Update Final Leaves Balance: '}</strong>
                </label>
                <div className="">
                  <input
                    type="text"
                    className="form-control w-100"
                    placeholder={action === 'allocate' ? "Update Allocated Leaves " :"Update Final Leave Balance"}
                    value={this.state.updateAllocatedLeave}
                    onChange={e => {
                        this.setState({
                          updateAllocatedLeave:e.target.value,
                        });
                    }}
                  />
                </div>
            </div>
          </div> 
        </Dialog>

          <UsersListHeader
            users={this.props.usersList.users}
            selectedUserId={this.state.selected_user_id}
            onUserClick={this.onUserClick}
          />
          <AlertFader>
              {this.props.manageSalary.status_message ? (
                <Alert
                  className="alert-transparent rem-fade rh-section rh-alert"
                  color= "success"
                >
                  {this.props.manageSalary.status_message}
                </Alert>
              ) : null}
            </AlertFader>
          <div className="app-body" id="view">
            <div>
              <div className="row manage-salary-view">
                <div className="col">
                  <UserPicHeadTitle
                    name={this.state.selected_user_name}
                    profileImage={this.state.selected_user_image}
                    jobTitle={this.state.selected_user_jobtitle}
                    />
                  <div className="d-flex justify-content-between">
                    <div> <h6 > Salary <strong> Revision </strong> </h6> </div>
                    {!this.state.openForm && <div> 
                      <Button color="primary" onClick={this.handleOpen}>Add Salary</Button>
                      </div>}
                  </div>
                    <div className="leave-btns-wrapper">
                      <Button color="primary" onClick={()=>this.handleLeaveAllocatedModal('allocate')}>Update Allocated Leaves</Button>
                      <Button color="primary" onClick={()=>this.handleLeaveAllocatedModal('')}>Update Final Leave Balance</Button>
                    </div>
                  
                  <div className="row mx-0 m-t-xs">
                  <div className="content-salary col-md-12 p-0">
                  <AddSalaryForm
                          {...this.props}
                          userid={this.state.selected_user_id}
                          callAddUserSalary={this.callAddUserSalary}
                          user_latest_salary_details={
                            this.state.user_latest_salary_details
                          }
                          latest_salary_literally={this.state.salary_history[0]}
                          toggle={this.toggle}
                          openForm={this.state.openForm}
                          prev_sal={prev_salary}
                          addTimes={this.state.addTimes}
                        />
                  </div>
                  
                    {/* <div className="p-a block ">
                      <h6 className="text-center">Add New</h6>
                      <hr />
                      <AddSalaryForm {...this.props} userid={this.state.selected_user_id} callAddUserSalary={this.callAddUserSalary} user_latest_salary_details={this.state.user_latest_salary_details}/>
                    </div> */}
                    <div className="p-a block p-0 col-md-12">

                      {/* <h6 className="text-center">Salary Revision</h6>
                      <hr /> */}
                      <div className="content-salary">
                      {message && this.props.loggedUser.data.role === "HR"  ? <Alert
                        className="alert-transparent rem-fade rh-section rh-alert"
                        color="danger"
                      >
                        <span className="font-weight-bold">ALERT:</span>{" "}
                        <span>{data}</span>
                      </Alert>:data}
                      </div>
                    </div>
                  </div>
                  <div className="box m-t-xs">
                    <div className="p-a block ">
                      <h6 className="text-center">Holding Revision</h6>
                      <hr />
                      <div className="content-salary">
                        <UserHistoryHolding data={this.state.holding_history} />
                        <AddHoldingForm
                          {...this.props}
                          userid={this.state.selected_user_id}
                          callAddUserHolding={this.callAddUserHolding}
                          user_latest_salary_details={
                            this.state.user_latest_salary_details
                          }
                        />
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    frontend: state.frontend.toJS(),
    loggedUser: state.logged_user.userLogin,
    usersList: state.usersList.toJS(),
    manageSalary: state.manageSalary.toJS()
  };
}
const mapDispatchToProps = dispatch => {
  return {
    onUsersList: data => {
      return dispatch(actions_usersList.get_users_list(data));
    },
    onUserSalaryDetails: userid => {
      return dispatch(actions_manageSalary.get_user_salary_details(userid));
    },
    onAddNewSalary: new_salary_data => {
      return dispatch(
        actions_manageSalary.add_user_new_salary(new_salary_data)
      );
    },
    onAddNewHolding: new_holding_data => {
      return dispatch(
        actions_manageSalary.add_user_new_holding(new_holding_data)
      );
    },
    onDeleteUserSalary: (user_id, salary_id) => {
      return dispatch(
        actions_manageSalary.delete_user_salary(user_id, salary_id)
      );
    },
    OnUpdateEmployeeLeavesAllocated:(user_id, new_allocated_leaves, actionType)=>{
      return dispatch(
        actions_manageSalary.updateEmployeeAllocatedLeaves(user_id, new_allocated_leaves, actionType)
      )
    },
    onSelectedUser: userId => {
      return dispatch(actions.selectedUser(userId));
    },
    showHeading:(data)=>{
      return dispatch(actions.showHeading(data));
    }
  };
};

const VisibleManageSalary = connect(
  mapStateToProps,
  mapDispatchToProps
)(ManageSalary);

const RouterVisibleManageSalary = withRouter(VisibleManageSalary);

export default RouterVisibleManageSalary;
