import React from 'react';
import {connect} from 'react-redux';
import {withRouter} from 'react-router';
import * as _ from 'lodash';
import Menu from '../../../components/generic/Menu';
import {isNotUserValid} from '../../../services/generic';
import Header from '../../../components/generic/Header';
import SalaryList from '../../../modules/salary/components/viewSalary/SalaryList';
import * as actions_salary from '../../../redux/salary/actions/viewSalary';
import * as actions from '../../../redux/actions'
import Heading from "../../../components/generic/Heading";

class ViewSalary extends React.Component {
  constructor (props) {
    super(props);
    this.state = {
      empList: []
    };
  }
  componentWillMount () {
    this.props.onFetchUserSalaryDetails();
    this.props.showHeading("View Salary")
  }
  componentWillReceiveProps (props) {
    let isNotValid = isNotUserValid(this.props.location.pathname, props.loggedUser);
    if (isNotValid.status) {
      this.props.history.push(isNotValid.redirectTo);
    }
    let emp = [];
      if (props.employee.employee.length > 0) {
        _.forEach(props.employee.employee, function (ob, i) {
          emp.push({
            'image':               ob.profileImage,
            'empName':             ob.name,
            'designation':         ob.jobtitle,
            'salary':              ob.salary_detail,
            'holdingAmountDetail': ob.holdin_amt_detail,
            'dateOfJoining':       ob.dateofjoining,
            'noOfDaysSinceJoined': String(ob.no_of_days_join),
            'preSalaryIncDetail':  String(ob.previous_increment),
            'nextSallaryInc':      ob.next_increment_date
          });
        });
        this.setState({empList: emp});
      }
  }


  render () {
    if (this.state.empList.length) {
      var salary_total=0;
      for (var i = 0; i < this.state.empList.length; i++) {
        if(this.state.empList[i].salary && this.state.empList[i].salary!=="NaN") {
          salary_total += Number(this.state.empList[i].salary);
        }
      }
    }
    let table = (this.state.empList.length > 0)
      ? <SalaryList {...this.props} empList={this.state.empList} />
      : '';
    return (
      <div>
        <Menu {...this.props} />
        <div id="content" className="app-content box-shadow-z0" role="main">
          <Header pageTitle={'View Salary'} showLoading={this.props.frontend.show_loading} />
          <div className="app-body" id="view">
            <div>
            <div className="d-flex align-items-baseline">
              <div className='d-flex employees-header'> 
                  <div>
                    <span> Total </span>
                    <span> <small className='font-weight-bold ml-2'>{this.state.empList.length} Employees</small> </span>
                  </div>
                  <div>
                    <span className ='mx-2'>Total salary</span>
                    <small className='font-weight-bold'><strong>Rs {salary_total}  </strong></small>
                  </div>
                {/* Total <small className='font-weight-bold ml-2'>{manageUsers.allEmpolyesDetails.length} Employees</small>  */}
              </div>
            </div>
              {table}
            </div>
          </div>
        </div>
      </div>
    );
  }
}
function mapStateToProps (state) {
  return {
    frontend:   state.frontend.toJS(),
    loggedUser: state.logged_user.userLogin,
    usersList:  state.usersList.toJS(),
    employee:   state.empSalaryList.toJS()
  };
}
const mapDispatchToProps = (dispatch) => {
  return {
    onFetchUserSalaryDetails: () => {
      return dispatch(actions_salary.fetchUserSalaryDetails());
    },
    showHeading:(data)=>{
      return dispatch(actions.showHeading(data));
    }
  };
};

const VisibleViewSalary = connect(mapStateToProps, mapDispatchToProps)(ViewSalary);

const RouterVisibleViewSalary = withRouter(VisibleViewSalary);

export default RouterVisibleViewSalary;
