import React from "react";
import { Switch, Route } from "react-router-dom";
import { render } from "../../../test/utils";
import { waitFor } from "@testing-library/react";
import userEvent from "@testing-library/user-event";

import "@testing-library/jest-dom";
import "@testing-library/jest-dom/extend-expect";

import Login from "./Login";

import { rest } from "msw";
import { setupServer } from "msw/node";

test("login page renders ", () => {
  const { getByTestId } = render(<Login />);
  expect(getByTestId("login")).toBeInTheDocument(true);
});

const configapiresponse = {
  error: false,
  data: {
    login_types: {
      normal_login: true,
      google_login: false,
      google_auth_client_id: "",
    },
  },
};
const loginapiresponse = {
  error: 0,
  data: {
    token: "token",
    userid: "766",
    message: "Success Login",
  },
};

const server = setupServer();

beforeAll(() => server.listen());
afterEach(() => server.resetHandlers());
afterAll(() => server.close());
test("backend not responding", async () => {
  server.use(
    rest.post("*", (req, res, ctx) => {
      if (JSON.parse(req.body).action === "get_generic_configuration") {
        return res(ctx.status(500), ctx.json("error simulated"));
      }
    })
  );
  let { getByTestId } = render(<Login />);
  await waitFor(() => {
    expect(getByTestId("backend-error")).toBeInTheDocument(true);
  });
});

test("login failure", async () => {
  server.use(
    rest.post("*", (req, res, ctx) => {
      if (JSON.parse(req.body).action === "get_generic_configuration") {
        return res(ctx.status(200),ctx.json(configapiresponse));
      } else {
        return res(
          ctx.status(200),
          ctx.json({ error: 1, data: { message: "Invalid Login" } })
        );
      }
    })
  );
  let { container, queryByText, getByTestId } = render(<Login />);
if(!configapiresponse.error){

  await waitFor(() => {
    expect(getByTestId("loginButton")).toBeInTheDocument(true);
  });
  userEvent.click(getByTestId("loginButton"));

  expect(queryByText("Loading...")).toBeInTheDocument(); //on button click we should have loading
  await waitFor(() => {
    expect(queryByText("Login")).toBeInTheDocument(true);
  });
  expect(getByTestId("error-message").childElementCount).toBe(1);
}
});

test("login click calls api and shows loading", async () => {
  server.use(
    rest.post("*", (req, res, ctx) => {
      if (req.body.action === "get_generic_configuration") {
        return res(ctx.json(configapiresponse));
      } else if (req.body.action === "login") {
        return res(ctx.json(loginapiresponse));
      }
    })
  );

  let { container, queryByText, getByTestId } = render(
    <Switch>
      <Route path="/" component={Login} />
      <Route path="/app/*" component={() => <div data-testid="app"></div>} />
    </Switch>
  );
    await waitFor(() => {
      expect(getByTestId("loginButton")).toBeInTheDocument(true);
    });
    userEvent.click(getByTestId("loginButton"));

    expect(queryByText("Loading...")).toBeInTheDocument(); //on button click we should have loading
    await waitFor(() => {
      expect(queryByText("Login")).toBeInTheDocument(true);
    });
    expect(getByTestId("loginButton")).toHaveProperty("disabled", false);
  
});

test("google button available", async () => {
  const apiresponse = {
    error: false,
    data: {
      login_types: {
        normal_login: false,
        google_login: true,
        google_auth_client_id: "",
      },
    },
  };
  server.use(
    rest.post("*", (req, res, ctx) => {
      if (JSON.parse(req.body).action === "get_generic_configuration") {
        return res(ctx.json(apiresponse));
      }
    })
  );
  let { getByTestId } = render(<Login />);

  await waitFor(() => {
    expect(getByTestId("googleLoginButton")).toBeInTheDocument(true);
  });
});

test("google button not available", async () => {
  const apiresponse = {
    error: false,
    data: {
      login_types: {
        normal_login: false,
        google_login: true,
        google_auth_client_id: "",
      },
    },
  };
  server.use(
    rest.post("*", (req, res, ctx) => {
      if (JSON.parse(req.body).action === "get_generic_configuration") {
        return res(ctx.json(apiresponse));
      }
    })
  );
  let { getByTestId } = render(<Login />);

  await waitFor(() => {
    expect(getByTestId("backend-success")).toBeInTheDocument(true);
    expect(getByTestId("googleLoginButton")).toBeInTheDocument(false);
  });
})
