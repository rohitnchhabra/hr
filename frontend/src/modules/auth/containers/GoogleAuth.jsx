import React, { useState } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import GoogleLogin from "react-google-login";
import { Button } from "reactstrap";
import cx from "classnames";
import { userLoginRequest } from "../../../redux/actions";
import s from "./Login.module.scss";

function GoogleAuth(props) {
  const [error, seterror] = useState(false);
  const onSuccessGoogleLogin = (response) => {
    if (response.accessToken) {
      let googleAuthToken = response.accessToken;
      props.userLoginRequest({
        username: "",
        password: "",
        googleAuthToken,
      });
    }
  };

  const { loggedUser, loginTypes } = props;
  return (
    <div data-testid="googleLoginButton">
      <GoogleLogin
        clientId={
          (loginTypes &&
            loginTypes.data &&
            loginTypes.data.google_auth_client_id) ||
          ""
        }
        render={(renderProps) => (
          <div className={s.socialButtons}>
            <Button
              onClick={renderProps.onClick}
              disabled={renderProps.disabled}
              color="primary"
              className={cx(s.socialButton, "mb-2")}
            >
              <i className={cx(s.socialGoogle, s.socialIcon)} />
              <span className={s.socialText}>
                {loggedUser.isGoogleLoading
                  ? "Loading..."
                  : "Login with your company google account"}
              </span>
            </Button>
          </div>
        )}
        onSuccess={onSuccessGoogleLogin}
        cookiePolicy={"single_host_origin"}
      />
    </div>
  );
}

function mapStateToProps(state) {
  return {
    loggedUser: state.logged_user.userLogin,
    loginTypes: state.settings.loginTypes,
  };
}

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators(
    {
      userLoginRequest,
    },
    dispatch
  );
};

const VisibleGoogleAuth = connect(
  mapStateToProps,
  mapDispatchToProps
)(GoogleAuth);

export default VisibleGoogleAuth;
