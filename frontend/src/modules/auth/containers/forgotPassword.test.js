import React from "react";
import { Switch, Route } from "react-router-dom";
import { render } from "../../../test/utils";
import { waitFor} from "@testing-library/react";
import userEvent from "@testing-library/user-event";

import "@testing-library/jest-dom";
import "@testing-library/jest-dom/extend-expect";

import ForgotPassword from "./forgotPassword";

import { rest } from "msw";
import { setupServer } from "msw/node";

const location = {
  pathname: "/forgot_password",
};

test("forgot password page renders ", () => {
  const { getByTestId } = render(<ForgotPassword location={location} />);
  expect(getByTestId("forgot-password")).toBeInTheDocument(true);
});

const server = setupServer();
const resetPasswordResponse = {
  error: 0,
  data: {
    message: "Password reset Successfully. Check you slack for new password!!",
  },
};
const resetPasswordfailure = {
  error: 1,
  data: {
    message: "User not exists!!",
  },
};

beforeAll(() => server.listen());
afterEach(() => server.resetHandlers());
afterAll(() => server.close());

test("reset password click calls api and shows loading", async () => {
  const { getByTestId, getByText } = render(
    <ForgotPassword location={location} />
  );

  const input = getByTestId("username");
  userEvent.type(input, "testusername");

  expect(input.value).toBe("testusername");

  server.use(
    rest.post(
      "*",
      (req, res, ctx) => {
        if (req.body.action === "forgot-password") {
          return res(ctx.json(resetPasswordResponse));
        }
      }
    )
  );
  await waitFor(() => {
    expect(getByTestId("reset-password")).toBeInTheDocument(true);
  });
  userEvent.click(getByTestId("reset-password"));
  expect(getByText("Loading...")).toBeInTheDocument();
});

test("reset passord failure", async () => {
  let { queryByText, getByTestId } = render(
    <ForgotPassword location={location} />
  );
  const input = getByTestId("username");
  userEvent.type(input, "testusername");
  expect(input.value).toBe("testusername");
  server.use(
    rest.post(
      "*",
      (req, res, ctx) => {
        if (JSON.parse(req.body).action === "forgot-password") {
          return res(ctx.json(resetPasswordfailure));
        }
      }
    )
  );

  await waitFor(() => {
    expect(getByTestId("reset-password")).toBeInTheDocument(true);
  });
  userEvent.click(getByTestId("reset-password"));

  expect(queryByText("Loading...")).toBeInTheDocument(); //on button click we should have loading
  await waitFor(() => {
    expect(getByTestId("reset-password")).toBeInTheDocument(true);
  });
});

test("login link click and pushes to login page ", async () => {

  let { getByTestId } = render(
      <Switch>
        <Route path="/page_login" render={()=><div data-testid="login"></div>}/>
        <Route exact path="/" component={ForgotPassword} />
      </Switch>
  );
  expect(getByTestId("login-link")).toBeInTheDocument(true);
  userEvent.click(getByTestId("login-link"));
  expect(getByTestId("login")).toBeInTheDocument(true);
  
});
