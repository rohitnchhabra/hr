import React,{useEffect} from "react";
import { useDispatch, useSelector } from "react-redux";
import { isNotUserValid } from "../../../services/generic";
import { notify } from "../../../services/notify";
import * as actions from "../../../redux/actions";
import ForgotPasswordView from "../components/ForgotPassword/ForgotPasswordView";
const ForgotPassword = props => {
  const dispatch = useDispatch();
  const requestForgotPassword = form_username => {
    dispatch(actions.requestForgotPassword(form_username));
  };
  const forgotPassword = useSelector(state => state.logged_user.forgotPassword);
  const loggedUser = useSelector(state => state.logged_user.userLogin);
  useEffect(() => {
    let isNotValid = isNotUserValid(props.location.pathname, loggedUser);
    if (isNotValid.status && isNotValid.redirectTo !== "/logout") {
      props.history.push(isNotValid.redirectTo);
    }
  }, [isNotUserValid.redirectTo]);

  useEffect(() => {
    const {isError,isSuccess,message}=forgotPassword
    if (isError || isSuccess) {
      let megType = isError ? "error" : "success";
      notify(megType + " ! ", message, megType);
    }
  }, [forgotPassword.isError || forgotPassword.isSuccess]);
  return (
    <ForgotPasswordView
      requestForgotPassword={requestForgotPassword}
      forgotPassword={forgotPassword}
      loggedUser={loggedUser}
      {...props}
    />
  );
};

export default ForgotPassword;
