import React from "react";
import { withRouter, Link } from "react-router-dom";
import { connect } from "react-redux";
import { isNotUserValid } from "../../../services/generic";
import {
  isAlreadyLogin,
  userLoginRequest,
  getGenricLoginConfig,
} from "../../../redux/actions";
import { useState, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import LoginView from "../components/login/LoginView";

const Login = (props) => {
  const dispatch = useDispatch();
  const state = useSelector((state) => state);
  const [error, setStatus] = useState(false);

  useEffect(() => {
    dispatch(getGenricLoginConfig());
    if (localStorage.getItem("userToken")) {
      props.history.push("/app/overview");
    }
  }, []);

  const loggedUser = state.logged_user.userLogin;
  const loginTypes = state.settings.loginTypes.data;
  const loginTypeStatus = state.settings.loginTypes;
  const frontend = state.frontend;
  
  let isNotValid = isNotUserValid(props.location.pathname, loggedUser);

  useEffect(() => {
    if (isNotValid.status && isNotValid.redirectTo !== "/logout") {
      props.history.push(`/app/${isNotValid.redirectTo}`);
    }
  }, [isNotValid.redirectTo]);

  useEffect(() => {
    if (loginTypeStatus.isError) {
      localStorage.clear();
    }
  }, [loginTypeStatus.isError]);

  // const _submit = (evt) => {
  //   evt.preventDefault();
  //   let username = form_login_username;
  //   let password = form_login_password;
  //   doLogin(username, password);
  // };

  const doLogin = (username, password) => {
    var count = 0;
    for (var i = 0; i < username.length; i++) {
      if (username.charAt(i) === " ") {
        count++;
      }
    }
    if (count === 0) {
      setStatus(false);
      dispatch(userLoginRequest({ username, password }));
    } else {
      setStatus(true);
    }
  };
  const onSuccessGoogleLogin = response => {
    if (response.accessToken) {
      let googleAuthToken = response.accessToken;
      dispatch(
        userLoginRequest({
          username: "",
          password: "",
          googleAuthToken
        })
      );
    }
  };

  return (
    <LoginView
      doLogin={doLogin}
      loggedUser={loggedUser}
      loginTypeStatus={loginTypeStatus}
      frontend={frontend}
      loginTypes={loginTypes}
      onSuccessGoogleLogin={onSuccessGoogleLogin}
    />
  );
};

export default withRouter(Login);
