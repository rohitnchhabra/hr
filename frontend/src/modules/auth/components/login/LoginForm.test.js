import React from "react";

import { render } from "@testing-library/react";
import userEvent from "@testing-library/user-event";

import "@testing-library/jest-dom";
import "@testing-library/jest-dom/extend-expect";

import LoginForm from "./LoginForm";

test("login form submits", () => {
  const onSubmit = jest.fn();
  const { getByText } = render(<LoginForm onLogin={onSubmit} />);

  userEvent.click(getByText("Login"));
  expect(onSubmit).toHaveBeenCalled();
});
