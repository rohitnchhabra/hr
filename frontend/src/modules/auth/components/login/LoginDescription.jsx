import React from "react";
import Heading from "../../../../components/generic/Heading/Heading";
import styles from "./LoginDescription.module.scss";

const LoginDescription = () => {
  return (
    <div id={styles["right"]}>
      <Heading className="small-head" title="About Excellence HRMS" />

      <div className={`${styles["aboutFeat"]} mb-4`}>
        Say goodbye to cluttered spreadsheets From attendence to timesheets and
        performance, run HR on the cloud. Get more done with a simple user
        interface, effortless automation, and efficient processing.
      </div>
      <Heading className="small-head" title="features" />
      <div className="featureDetail">
        <ul className={styles["featurelist"]}>
          <li>
            <span>Core HR: Attendence,Leaves & Timesheets</span>
          </li>
          <li>
            <span>Manage Employee & Maintain Employee Life Cycle</span>
          </li>
          <li>
            <span>Salary Management</span>
          </li>
          <li>
            <span>Mail Templates & Variables</span>
          </li>
          <li>
            <span>Employee Document Management</span>
          </li>
          <li>
            <span>Auditing and Inventory Management</span>
          </li>
          <li>
            <span>Team Management</span>
          </li>
        </ul>
      </div>
    </div>
  );
};

export default LoginDescription;
