import React from "react";
import { action } from "@storybook/addon-actions";
import LoginForm from "./LoginForm";
import { withKnobs, boolean} from "@storybook/addon-knobs";

export default {
  title: "LoginForm",
  // component: LoginForm,
  decorators: [withKnobs],
};

export const loginForm = () => (
  <LoginForm
    disabled={boolean("Disabled", false)}
    onChange={action("changed")}
    onLogin={action("clicked")}
  />
);
