import React from "react";

import { render, queryByAttribute } from "@testing-library/react";

import "@testing-library/jest-dom";
import "@testing-library/jest-dom/extend-expect";

import LoginDescription from "./LoginDescription";

test("login description renders properly", () => {
  const getById = queryByAttribute.bind(null, "id");

  const dom = render(<LoginDescription />);
  const desc = getById(dom.container, "right");
  expect(desc).toBeInTheDocument(true)
});
