import React, { useState } from "react";
import { ButtonDark } from "../../../../components/generic/buttons/Button";
import styles from "./LoginForm.module.scss";
import PropTypes from "prop-types";
import { GenericAlertDanger } from "../../../../components/generic/Alert/Alert";

function LoginForm({ isError, isLoading, error, message, onLogin, ...props }) {
  const [form_login_password, setPassword] = useState("");
  const [form_login_username, setUsername] = useState("");

  const doLogin = (evt) => {
    evt.preventDefault();
    onLogin(form_login_username, form_login_password);
  };

  // const push =(evt)=>{
  //   evt.preventDefault();
  //   props.history.push('/forgot_password');
  // }
  return (
    <form className="mt" onSubmit={doLogin}>
      <span data-testid="error-message">
        {(isError || error) &&
          (error ? (
            <GenericAlertDanger className="alert-sm w-100">
              Username is invalid
            </GenericAlertDanger>
          ) : (
            <GenericAlertDanger className="alert-sm w-100">
              {message}
            </GenericAlertDanger>
          ))}
      </span>
      <div className="form-group">
        <input
          autoFocus
          className="form-control "
          value={form_login_username}
          onChange={(e) => setUsername(e.target.value)}
          type="text"
          required
          name="username"
          placeholder="Username"
        />
      </div>
      <div className="form-group">
        <input
          className="form-control no-border"
          value={form_login_password}
          onChange={(e) => setPassword(e.target.value)}
          type="password"
          required
          name="password"
          placeholder="Password"
        />
      </div>
      <div className={styles.forget}>
        {/* <span className='checkBox'>Remember me</span> */}
        <span>
          <a href="#/forgot_password" data-testid="password-link">Forgot Password</a>
        </span>
      </div>
      <ButtonDark
        disabled={isLoading}
        id="loginButton"
        type="submit"
        className="w-100 btn-sm"
        data-testid="loginButton"
      >
        {isLoading ? "Loading..." : "Login"}
      </ButtonDark>
    </form>
  );
}

LoginForm.propTypes = {
  isLoading: PropTypes.bool,
  isError: PropTypes.bool,
  message: PropTypes.string,
  onLogin: PropTypes.func.isRequired,
};

export default LoginForm;
