import React from "react";
import s from "./LoginView.module.scss";
import GoogleLogin from "react-google-login";
import { Button } from "reactstrap";
import cx from "classnames";
const GoogleLoginView = ({ loginTypes, loggedUser, onSuccessGoogleLogin }) => {
  return (
    <div data-testid="googleLoginButton">
      <GoogleLogin
        clientId={(loginTypes && loginTypes.google_auth_client_id) || ""}
        render={(renderProps) => (
          <div className={s.socialButtons}>
            <Button
              onClick={renderProps.onClick}
              disabled={renderProps.disabled}
              color="primary"
              className={cx(s.socialButton, "mb-2")}
            >
              <i className={cx(s.socialGoogle, s.socialIcon)} />
              <span className={s.socialText}>
                {loggedUser.isGoogleLoading
                  ? "Loading..."
                  : "Login with your company google account"}
              </span>
            </Button>
          </div>
        )}
        onSuccess={onSuccessGoogleLogin}
        cookiePolicy={"single_host_origin"}
      />
    </div>
  );
};

export default GoogleLoginView;
