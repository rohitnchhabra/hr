import React from "react";
import LoginView from "./LoginView";
import { withKnobs } from "@storybook/addon-knobs";
import { action } from "@storybook/addon-actions";
export default {
  title: "LoginPage",
  decorators: [withKnobs]
};

export const loginPage = () => {
  const frontend = {
    show_loading: "0"
  };
  const loginTypeStatus = {
    isError: false
  };
  const loginTypes = {
    normal_login: true,
    google_login: true
  };
  const loggedUser={
    isGoogleLoading:false
  }
  return (
    <LoginView
      frontend={frontend}
      loginTypeStatus={loginTypeStatus}
      loginTypes={loginTypes}
      loggedUser={loggedUser}
      doLogin={action("clicked")}
      onSuccessGoogleLogin={action("clicked")}
    />
  );
};
