import React from "react";
import LoginDescription from "./LoginDescription";
import LoginForm from "./LoginForm";
import { HeadingWithText } from "../../../../components/generic/Heading/Heading";
import { GenericAlertDangerWithLabel } from "../../../../components/generic/Alert/Alert";
import GenericSpinner from "../../../../components/generic/Spinner/GenericSpinner";
import GetLogo from "../../../../components/auth/login/GetLogo";
import Widget from "../../../../components/generic/Widget/Widget";
import s from "./LoginView.module.scss";
import GoogleLoginView from "./GoogleLoginView";
import PropTypes from 'prop-types'

const LoginView = ({
  doLogin,
  frontend,
  loginTypeStatus,
  loginTypes,
  loggedUser,
  onSuccessGoogleLogin,
}) => {
  return (
    <div className={s.root} id="login" data-testid="login">
      <div
        className="containform bg-light"
        style={{ display: "flex", paddingTop: "10%" }}
      >
        <div id="left" className="bg-light" style={{ paddingLeft: "10%" }}>
          <GetLogo className={s.logo} />
          <HeadingWithText
            title="Human Resource Management System"
            text="Welcome to"
            className="mb-4"
          />
          {frontend && frontend.show_loading === "1" ? (
            <div className="login-spinner">
              <GenericSpinner />
            </div>
          ) : loginTypeStatus.isError === false ? (
            <div data-testid="backend-success">
              <Widget className={`${s.widget} `} id="widgetForm">
                {/* <Navbar /> */}
                {/* <div className="m-y text-sm">Sign in with your username</div> */}
                {loginTypes.normal_login && (
                  <LoginForm
                    onLogin={doLogin}
                    isLoading={loggedUser.isLoading}
                    isError={loggedUser.isError}
                    message={loggedUser.message}
                    // error={error}
                  />
                )}
                {loginTypes.google_login && (
                  <GoogleLoginView
                    loginTypes={loginTypes}
                    loggedUser={loggedUser}
                    onSuccessGoogleLogin={onSuccessGoogleLogin}
                  />
                )}
              </Widget>
            </div>
          ) : (
            <div data-testid="backend-error">
              <GenericAlertDangerWithLabel className="alert-transparent">
                Backend Not Responsding!! Inform System Admin or Try again
                later.
              </GenericAlertDangerWithLabel>
            </div>
          )}
        </div>
        <LoginDescription />
      </div>
    </div>
  );
};
LoginView.prototype={
  doLogin:PropTypes.func.isRequired,
  onSuccessGoogleLogin:PropTypes.func.isRequired

}

export default LoginView;
