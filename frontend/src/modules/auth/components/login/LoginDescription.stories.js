import React from "react";
import LoginDescription from "./LoginDescription";
import { withKnobs} from "@storybook/addon-knobs";

export default {
  title: "LoginDescription",
  decorators: [withKnobs]
};

export const loginDescription = () => <LoginDescription />;
