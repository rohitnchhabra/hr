import React from "react";
import ForgotPasswordView from "./ForgotPasswordView";
import { withKnobs,boolean} from "@storybook/addon-knobs";
export default {
  title: "Forgot Password",
  decorators: [withKnobs]
};

export const forgotPassword = () => {
  const forgotPassword = {
    isError: boolean("isError",false),
    isSuccess: boolean("isSuccess",false),
    message: "",
    isLoading: boolean("isLoading",false)
  };
  return (
    <ForgotPasswordView
      forgotPassword={forgotPassword}
    />
  );
};
