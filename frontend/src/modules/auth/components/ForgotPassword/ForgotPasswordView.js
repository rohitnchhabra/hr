import React, { useState} from "react";
import { notify } from "../../../../services/notify";
import GetLogo from "../../../../components/auth/login/GetLogo";
import s from "../login/LoginView.module.scss";
import Widget from "../../../../components/generic/Widget/Widget";
import {Container} from 'reactstrap' 
import {ButtonDark} from '../../../../components/generic/buttons/Button';
import InputText from '../../../../components/generic/InputText/InputText'
import PropTypes from 'prop-types';
function ForgotPasswordView(props) {
  const [form_username, setUserName] = useState("");
  let {
    requestForgotPassword,
    forgotPassword: { isLoading }
  } = props;
  
  const submit = evt => {
    evt.preventDefault();
    setUserName(form_username)
    if (form_username === "") {
      notify("Warning !", "Enter Username", "warning");
    } else {
      requestForgotPassword({ username: form_username });
    }
  };
  return (
    <div className={s.root} data-testid="forgot-password">
      <Container className="text-center">
        <GetLogo />
      </Container>
      <Widget className={`${s.widget} mx-auto`}>
        <div className="m-y text-sm">Reset Your Password</div>
        <form className="mt" onSubmit={submit}>
          <div className="form-group">
            <InputText
              className="no-border"
              value={form_username}
              onChange={e => setUserName(e.target.value)}
              type="text"
              required
              name="username"
              placeHolder="Username"
              id="username"
              testId="username"
            />
          </div>
          <ButtonDark type="submit" className="w-100 btn-sm" id="reset-password" data-testid="reset-password">
            {isLoading ? "Loading..." : "Reset Password"}
          </ButtonDark>
        </form>
        <div className="mt-3 text-center">
          <a onClick={(e)=>{e.preventDefault(); props.history && props.history.push('/page_login')}} data-testid="login-link">Login</a>
        </div>
      </Widget>
    </div>
  );
}

ForgotPasswordView.prototype={
  requestForgotPassword:PropTypes.func.isRequired,
  forgotPassword:PropTypes.shape({
    isError:PropTypes.bool,
    isSuccess:PropTypes.bool,
    isLoading:PropTypes.bool,
    message:PropTypes.string
  }),
}

export default ForgotPasswordView;
