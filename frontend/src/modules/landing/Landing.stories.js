import React from "react";
import Landing from "./Landing";

export default {
  title: "Landing",
  component: Landing,
};

export const landing = () => (
  <Landing />
);
