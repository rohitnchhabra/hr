import React from "react";
import employee from "../../styles/assets/landing/employee.png";
import payroll from "../../styles/assets/landing/payroll.png";
import inventory from "../../styles/assets/landing/inventory.png";
import leave from "../../styles/assets/landing/leave.png";
import document from "../../styles/assets/landing/document.png";
import policies from "../../styles/assets/landing/policies.png";
import profile from "../../styles/assets/landing/profile.png";
import styles from "./Landing.module.scss";

function Landing() {
  return (
    <div className={styles["landing-content-wrapper"]}>
      <div className={styles.header}>
        All employee information <br></br> in one place
      </div>
      <div className={styles["page-details"]}>
        Simple and intuitive HR software that consolidates all of your employee
        information into a safe and centralized information store that is
        assible from anywhere. All your employee's data can easily be collected,
        imported and stored in the database.
      </div>
      <div className={styles["feature-heading"]}>FEATURES</div>

      <div className={`${styles["cards-wrapper"]} row`}>
        <div className={`${styles["card-content"]} col-sm-4`}>
          <img src={employee} alt="employee-management" />
          <div className={styles["card-heading"]}>Employee Management</div>
          <div className={styles["details"]}>
            All your employee information stored in one central location and
            accessible from anywhere
          </div>
        </div>
        <div className={`${styles["card-content"]} col-sm-4`}>
          <img src={payroll} alt="employee-management" />
          <div className={styles["card-heading"]}>Payroll</div>
          <div className={styles["details"]}>
            HRMS Payroll provides every possible option for allocation of
            employee employershare of PF or ESI all at one place
          </div>
        </div>
        <div className={`${styles["card-content"]} col-sm-4`}>
          <img src={inventory} alt="employee-management" />
          <div className={styles["card-heading"]}>Inventory</div>
          <div className={styles["details"]}>
            Track every item or batch in your inventory with serial number.
            Always keep a track on movement of items, in a single inventory
            management
          </div>
        </div>
      </div>

      <div className="row cards-wrapper">
        <div className={`${styles["card-content"]} col-sm-4`}>
          <img src={leave} alt="employee-management" />
          <div className={styles["card-heading"]}>Leave Management</div>
          <div className={styles["details"]}>
            Customizable leave management system of your own. Approve multiple
            requests and manage applications with an effective leave management.
          </div>
        </div>
        <div className={`${styles["card-content"]} col-sm-4`}>
          <img src={document} alt="employee-management" />
          <div className="card-heading">Document Management</div>
          <div className={styles["details"]}>
            Organize your online documents, create public folders for internal
            policy resources, and share confidential files with the right
            person.
          </div>
        </div>
        <div className={`${styles["card-content"]} col-sm-4`}>
          <img src={policies} alt="employee-management" />
          <div className="card-heading">Policies Management</div>
          <div className={styles["details"]}>
            Upload, organize and share public documents like policies and other
            sensitive information.
          </div>
        </div>
      </div>
      <div className={`row ${styles["cards-wrapper"]} justify-content-center`}>
        <div className={`${styles["card-content"]} col-sm-4`}>
          <img src={profile} alt="employee-management" />
          <div className={styles["card-heading"]}>Profile</div>
          <div className={styles["details"]}>
            Let employees update their own information, and be sure you are
            getting the vital details. Set profile permissions so that employees
            can access right fields and keep all information upto date.
          </div>
        </div>
      </div>
    </div>
  );
}

export default Landing;
