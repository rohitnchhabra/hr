import React, { Component } from 'react'
import { connect } from 'react-redux';
import { withRouter } from 'react-router'
import * as actions from '../../../redux/actions';
import {
  Input,
  Label,
} from 'reactstrap';
import GenericTable from "../../../components/generic/table/Table";
import {DATA} from "../../../components/menu/menuObject";
import { Toast, ToastHeader, Spinner} from 'reactstrap';
import AlertNotification from "../../../components/generic/AlertNotification";
import _ from "lodash";
import Heading from '../../../components/generic/Heading';

const unRestrictedPath =[
                          {"label":"Settings"},
                          {"label":"Logout"},
                          {"label":"My Profile"},
                          {"label":"Apply Leave"},
                          {"label":"Show Menu"},
                        ]
const list = function (listItem,onChange,rowStyle,index){
  return(
    <>
      <td> 
        <div className="abc-checkbox">
          <Input
            id="checkbox2" type="checkbox" checked={listItem.is_enabled}
            onChange={() =>onChange(listItem,index)}
          />
          <Label for="checkbox2" />
        </div>
      </td > 
      <td>{listItem.newLable ? listItem.newLable  : listItem.label}</td>
    </>
  )
}

class ShowMenuContainer extends Component {
  constructor(props){
    super(props);
    this.state={listItem:"",showListData:[],isSelection:false,dropdownOpen:false}
  }

  componentDidMount(){
    this.props.getShowMenuListRequest();
    this.props.showHeading("Show Menu")
  }

  onChange=(item,index)=>{
    const {showListData} =this.state;
    let showListDataClone = showListData
    let page_id =[]
    if(item.page_id.length>0){
      item.page_id.forEach((pageId,i)=>{
          page_id.push(pageId)
      })
    }else{
      page_id =[item.page_id]
    }
    const paylaoad= {
      page : page_id,
      status : !item.is_enabled
     }
      showListDataClone[index].is_enabled =!showListDataClone[index].is_enabled 
      this.setState({showListData:showListDataClone, isSelection:true})
      this.props.sendSelectedOptionRequest(paylaoad)
  }

  componentDidUpdate(previousProps){
    const {showList} =this.props;
    const {isSelection} =this.state;
    let showListData = []
    let validPath =[]
    let child =[]
    if(showList.showListMenu.isSuccess !== previousProps.showList.showListMenu.isSuccess){
      if(!isSelection){
        showList.showListMenu.data.forEach((list,index)=>{
          DATA.forEach((dataList,i)=>{
            let isValidPath = unRestrictedPath.filter((a,i)=>a.label === dataList.label)
            if(`/${list.name}` === dataList.path && isValidPath.length < 1){
              let listItem ={...list,...dataList}
              showListData.push(listItem)
            }
          })
        })
        
        /*let groupIndex = _.groupBy(showListData, function(value) {
          return value.plabel;
        })
        /*let links = _.map(groupIndex, (subChildren, parentLabel) => {
          
          let undefinedList =[]
            if( parentLabel ==="undefined"){
              _.map(subChildren,(list,i)=>{
                let undefinedObeject={}
                  undefinedObeject ={...list,...{newLable:list.label}}
                  undefinedList.push(undefinedObeject)
                  if(subChildren.length-1 ==i){
                    validPath =validPath.concat(undefinedList)
                  }
                return(
                  null
                )
              })
            }
            if(parentLabel !=="undefined"){
              let toChild ={}
              let page_id =[] 
              _.map(subChildren, (subChildrenn, index) => {
                  page_id.push(subChildrenn.page_id)
                  if(subChildren.length-1 == index){
                    toChild = {...subChildrenn,...{page_id:page_id},...{lenth:subChildren.length},...{newLable:parentLabel !=="Policy Documents"? parentLabel :subChildrenn.label }}
                    child.push(toChild)
                  }
                return (
                  null
                )
              })
            }
          return(
           null
          )
        })*/
        validPath= validPath.concat(child)
        this.setState({showListData:validPath})
      }
    }
    if(showList.showStatus.isSuccess !== previousProps.showList.showStatus.isSuccess){
      this.props.getShowMenuListRequest()
    }
  }

  onRowClick=(listItem)=>{
    this.setState({listItem})
  }
    render() {
      const {showList} =this.props;
      const {showListData} = this.state;
      // let groupIndex = _.groupBy(showListData, function(value) {
      //   return value.plabel;
      // });
      
        return (
            <div className="showmenu-view">
               <GenericTable 
                  normalHeaderText="Show menu"
                  boldHeaderText="List"
                  tableListData ={showListData.sort((a, b) => (a.newLable !== b.newLable) ? a.newLable < b.newLable ? -1 : 1 : 0)}
                  onChange={this.onChange}
                  columnSize={12}
                  list={list}
                  onRowClick={this.onRowClick}
                  headerData={["Status", "Module name"]}
               />
               {showList.showStatus.isLoading &&
                 <div className="show-manu-loader">
                    <Toast>
                        <ToastHeader icon={<Spinner size="lg" />}>
                          <div className="updating-msg">Updating...</div>
                        </ToastHeader>
                    </Toast>
               </div>
               }
               <AlertNotification message={showList.showStatus.isSuccess && "Successfully updated"}/>
            </div>
        )
    }
}

const mapStateToProps=state=> {
    return {
      frontend: state.frontend.toJS(),
      loggedUser: state.logged_user.userLogin,
      salary: state.salary.toJS(),
      showList:state.showList
    };
  }
  const mapDispatchToProps = dispatch => {
    return {
      getShowMenuListRequest:()=>dispatch(actions.requestShowMenuListStatus()),
      sendSelectedOptionRequest:(data)=>dispatch(actions.sendSelectedOptionRequest(data)),
      showHeading:(data)=>{
        return dispatch(actions.showHeading(data));
      }

    };
  };
  
  export default withRouter(connect(mapStateToProps, mapDispatchToProps)(ShowMenuContainer))
  