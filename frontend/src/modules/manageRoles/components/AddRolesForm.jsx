import React from 'react';
import _ from 'lodash';
import PropTypes from 'prop-types';
import Dialog from 'material-ui/Dialog';
import {ButtonRaised} from '../../../components/generic/buttons/Button';
import {Button, Input, Alert} from 'reactstrap';

export default class AddRolesForm extends React.Component {
  constructor (props) {
    super(props);
    this.state = {
      open:        false,
      baseRoleId:  '',
      name:        '',
      description: '',
      error:       {baseRoleId: '', name: '', description: ''}
    };
    this.handleOpen = this.handleOpen.bind(this);
    this.handleClose = this.handleClose.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }
  handleOpen () {
    this.setState({
      open:        true,
      baseRoleId:  '',
      name:        '',
      description: ''
    });
  }
  handleClose () {
    this.setState({open: false});
  }
  handleSubmit () {
    let {baseRoleId, name, description} = this.state;
    let error = {baseRoleId: '', name: '', description: ''};
    let valid = true;
    if (_.isEmpty(name)) {
      valid = false;
      error.name = 'This field is required.';
    }
    if (_.isEmpty(description)) {
      valid = false;
      error.description = 'This field is required.';
    }
    this.setState({error});
    if (valid) {
      this.props.callAddNewRole({baseRoleId, name, description});
      this.handleClose();
    }
  }
  handleRestore = () => {
    this.props.handledRestore();
  }

  render () {
    let optionMenu = _.map(this.props.displayData.roles, (role, index) => ( 
        <option key={index} value={role.id}>{role.name}</option>  
    ));
    return (
      <div className="mb" id="role_button">
      <div className="role_add_button">
        <Button size="md" color="primary" id="add_role" onClick={this.handleOpen}>Add New Role</Button>
        {this.props.mode == "development" ?<Button className="mx-2" size="md" color="danger" onClick={this.handleRestore}>Restore Defaults</Button>:null}
      </div>
        
        <Dialog
          id="add_role_dialog"
          title="Add Role"
          modal={false}
          open={this.state.open}
          onRequestClose={this.handleClose}
          contentClassName="dialog-add-role"
          autoScrollBodyContent>
          <Alert color="info" fade={false}>
          Role's are you used to manage different permission's for employee's. HR system has lot of different pages and operations!
            We already have pre-defined roles which allow you manage these different permission across the system for different kind of employee.
            You can add more roles as well if you have specific requirements in your organization .
        </Alert>
          <Input type="select" id="role_selector" value={this.state.baseRoleId} onChange={(e) => { this.setState({baseRoleId: e.target.value})}} className="">
            <option value={''}>Select Base Role</option>
            {optionMenu}  
          </Input>
          <small className="subhint_role">Select a base role which will copy over all permissions of those role to the new role which you will create. Once a new role created, you can edit the permission .</small>
          <Input
            type="text"
            id="role_name"
            placeholder="Name"
            onChange={(e) => (this.setState({name: e.target.value}))}
            value={this.state.name}
            className="mt-2"
          />
          <small className="subhint_role">Put in a name to identify a role like HR, Sr. HR, Sales etc ...</small>
          <small className="text-danger">{this.state.error.name}</small>
          <Input
            id="role_desc"
            type="text"
            placeholder="Description"
            onChange={(e) => (this.setState({description: e.target.value}))}
            value={this.state.description}
            className="mt-2"
          />
          <small className="subhint_role">You can describe your role here .</small>
          <small className="text-danger">{this.state.error.description}</small>
          <ButtonRaised className="col-md-12 m-y-sm indigo" id="role_submit_button" onClick={() => this.handleSubmit()}>Create New Role</ButtonRaised>
        </Dialog>
      </div>
    );
  }
}

AddRolesForm.propTypes = {
  displayData: PropTypes.shape({
    roles: PropTypes.array
  }).isRequired,
  callAddNewRole: PropTypes.func.isRequired
};
