import React from "react";
import { Link } from "react-router-dom";
import PropTypes from "prop-types";
import { confirm } from "../../../services/notify";
import _ from "lodash";
import ToggleButton from "react-toggle-button";
import Task from "../../../components/Task/Task";

const DisplayRolesListNew = ({
  role,
  showActionsList,
  displayData,
  handleChange,
  handleChangeNotification,
  handleDelete,
  toggleShowActions
}) => {
  let rolesData = displayData.roles;
  let rolesList = _.map(rolesData, (value, key) => {
    let rolesId = value.id;
    if (rolesId == role.id) {
      let rolePages = _.map(value.role_pages, (v, k) => {
        let pageId = v.id;
        let roleActions = _.map(v.actions_list, (val, ke) => {
          let actionId = val.id;
          return (
            <li key={ke} className="payrole_checkbox_value">
              <Task
                className="manage_role_size"
                text={val.description}
                index={ke}
                id={pageId}
                status={val.is_assigned}
                handleChangeSteps={e => {
                  handleChange(e, actionId, rolesId);
                }}
                isManageRoles={true}
                disabled = {role.is_system_default_role}
              />
              {/* <input style={{height:"7px"}} type="checkbox" id={'check_' + val.name} value={val.name} checked={val.is_assigned} onChange={(e) => { handleChange(e, actionId, rolesId); }} key={actionId} /> { val.description ? val.description : val.name} */}
            </li>
          );
        });
        return (
          <li key={k} style={{ marginTop: showActionsList ? 10 : 2 }}>
            <Task
              className="manage_role_size"
              text={v.description ? v.description : v.name}
              index={k}
              id={pageId}
              status={v.is_assigned}
              handleChangeSteps={e => {
                handleChange(e, pageId, rolesId);
              }}
              isManageRoles={true}
              secure_level={
                v.secure_level
                  ? "( Secure Level : " + v.secure_level + " ) "
                  : null
              }
              disabled = {role.is_system_default_role}
            />
            {/* <input style={{height:"7px"}} type="checkbox" id={'check_' + v.name} value={v.name} checked={v.is_assigned} onChange={(e) => { handleChange(e, pageId, rolesId); }} key={pageId} />  */}
            {/* { v.secure_level ? '( Secure Level : ' + v.secure_level + ' ) ' : null } */}
            <b></b>

            <ul className="m-b-xs m-l-md">
              {showActionsList ? roleActions : null}
            </ul>
          </li>
        );
      });

      return (
        <div className="p-3">
        <div>
        <h6>PERMISSIONS</h6>
       {role.is_system_default_role ? <small className="font-weight-bold">System default roles can not edited </small> : ""} 
        </div>
          <div className="d-flex justify-content-between sub-managerole-block py-2">
            <div className="sub_managerole">SHOW SUB PERMISSIONS</div>
            <ToggleButton
              value={showActionsList}
              onToggle={e => {
                toggleShowActions();
              }}
            />
          </div>
          <ul>{rolePages}</ul>
        </div>
      );
    }
    return null;
  });
  return <div>{rolesList}</div>;
};

export default DisplayRolesListNew;

DisplayRolesListNew.propTypes = {
  displayData: PropTypes.shape({
    roles: PropTypes.object.isRequired
  }).isRequired,
  handleChange: PropTypes.func.isRequired,
  handleChangeNotification: PropTypes.func.isRequired,
  handleDelete: PropTypes.func.isRequired,
  showActionsList: PropTypes.func.isRequired,
  toggleShowActions: PropTypes.func.isRequired
};
