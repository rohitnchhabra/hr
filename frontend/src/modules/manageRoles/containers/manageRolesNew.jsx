import React, { Component } from "react";
import _ from "lodash";
import { Alert } from "reactstrap";
import { connect } from "react-redux";
import * as actionsManageRoles from "../../../redux/manageRoles/actions/manageRoles";
import { Collapse, Col, Row, Card, Button } from "reactstrap";
import s from "../../../components/profile/Profile.module.scss";
import * as avatar from "../../../static/avatar.png";
// import { bindActionCreators } from "redux";
import * as actions from "../../../redux/actions";
import { notify , confirm } from "../../../services/notify";
import DisplayRolesListsNew from "../../../modules/manageRoles/components/DisplayRolesListsNew";
import AddRolesForm from "../components/AddRolesForm";
import Heading from "../../../components/generic/Heading";
import { getToken } from "../../../services/generic";
import AlertFader from "../../../components/generic/AlertFader";

class manageRolesNew extends Component {
  constructor(props) {
    super(props);
    this.state = {
      status_message: "",
      rolesData: [],
      updateRole: { rolesId: "", actionId: "", pageId: "", notificationId: "" },
      showActionsList: false,
      showList: false,
      show_role: "false",
      token: "",
      message: "",
      error:''
    };
  }
  componentWillMount() {
    this.props.onRolesList();
    this.props.requestUsersList();
    this.setState({
      token: getToken()
    });
    this.props.showHeading("Manage Roles")
  }
  componentWillReceiveProps(props) {
    let {
      manageRoles: { rolesData }
    } = props;
    if (rolesData !== this.state.rolesData) {
      this.setState({
        rolesData
      });
    }
  }
  componentDidUpdate(prevprops) {
    if(prevprops.manageRoles.error_Message !== this.props.manageRoles.error_Message) {
      if(this.props.manageRoles.error_Message.length > 0 ){
        this.setState({error:this.props.manageRoles.error_Message},()=> {
          this.setState({error:''})
        })
      }
     }
  }
  toggle = (i, k) => {
    this.setState({ showList: !this.state.showList, show_role: i });
    const {
      manageRoles: { collapseSelected }
    } = this.props;
    if (collapseSelected === i) {
      this.props.changeCollapseSelected("");
    } else {
      this.props.changeCollapseSelected(i);
    }
  };

  callAddNewRole = newRoleDetails => {
    this.props.onAddNewRole(newRoleDetails);
  };

  handleChange = (e, targetId, roleId) => {
    let rolesData = this.state.rolesData;
    let updateRole = _.clone(this.state.updateRole);
    _.map(rolesData.roles, (role, key) => {
      if (role.id === roleId) {
        _.map(role.role_pages, (page, k) => {
          if (page.id === targetId) {
            page.is_assigned = e.target.checked;
            updateRole.pageId = targetId;
            _.map(page.actions_list, (action, ke) => {
              action.is_assigned = e.target.checked;
            });
          } else {
            _.map(page.actions_list, (action, ke) => {
              if (action.id === targetId) {
                action.is_assigned = e.target.checked;
                updateRole.actionId = targetId;
              }
            });
          }
        });
      }
    });
    updateRole.rolesId = roleId;
    this.setState({ rolesData });
    this.props.onUpdateRole(updateRole);
  };

  changeRole = (e,payload) => {
    this.props.onUpdateUserRole({ userId: payload, roleId: e.target.value });
  }

  handleDelete = (id,name) => {
    confirm("Delete Role", `Are you sure you want to delete role  ${name} ?`, "warning").then((res)=> {
      if(res) {
        this.props.onDelete(id).then(
          data => {
            notify("Success!", data, "success");
          },
          error => {
            notify("Error!", error, "error");
          }
        );
      }
    })
    
  };

  handledRestore = () => {
    let token = this.state.token;
    confirm("Restore Role", "This action will restore all role permissions to their default value. any changes you made to roles will be removed. Are you sure you want to proceed?", "warning").then((res)=> {
      if(res)
      {
        this.props.restoreUserRole(token).then(data => {
          this.props.onRolesList().then( () =>{
            this.setState({message:data.message},()=>{
              this.setState({message:""})
            })
          })  
          },
          error => {
            notify(error);
          }
        ) 
      }
  
    })
    
  };

  render() {
    const mode = process.env.NODE_ENV;
    const {
      manageRoles: { rolesData, collapseSelected },
      match
    } = this.props;
    var slide_open = collapseSelected == this.state.show_role;
    return (
      <div className="manage-roles-wrapper">
        <div className="d-flex justify-content-between manage-role-btn">
          {rolesData && rolesData.roles && (
            <AddRolesForm
              displayData={rolesData}
              callAddNewRole={this.callAddNewRole}
              handledRestore={this.handledRestore}
              mode = {mode}
            />
          )}
        </div>
        <AlertFader>
          {(this.state.message || this.state.error) && (
            <Alert
              className="alert-transparent rem-fade rh-section rh-alert"
              color={this.state.message ? "success" : "danger"}
            >
              {" "}
              <span>{this.state.message}</span>
              <span>{this.state.error}</span>
            </Alert>
          )}
        </AlertFader>
        <div className="row mx-0">
          <div className={slide_open ? "col-md-9 px-0" : "col-md-12 px-0"}>
            {this.state.rolesData && Object.keys(this.state.rolesData).length
              ? this.state.rolesData.roles
                  .filter(role => !role.name.includes("_"))
                  .map((role, index) => {
                    return (
                      <div key={index} className=" card mb-2 border-0">
                        <a
                          className=" p-2 "
                          onClick={e => {
                            e.stopPropagation();
                            this.toggle(role.name, index);
                          }}
                        >
                          <div className="d-flex justify-content-between">
                            <div>
                              <div className="d-flex">
                                <h5>
                                  <button className="btn-link text-body role_btn_name">
                                    {role.name}
                                  </button>
                                </h5>
                                <small className=" pl-2">
                                  <span className="manage-roles-detail">
                                    {rolesData.users_list &&
                                      rolesData.users_list.data.filter(
                                        user =>
                                          user.role_name &&
                                          (role.name ===
                                            user.role_name.replace(/\s/g, "") ||
                                            user.role_name
                                              .split("_")
                                              .includes(role.name))
                                      ).length}
                                  </span>
                                </small>
                              </div>
                        <div className="d-flex">
                          
                              <div
                                className={
                                  collapseSelected === role.name
                                    ? "d-none"
                                    : "mb-0"
                                }
                              >
                                {rolesData.users_list &&
                                rolesData.users_list.data.filter(
                                  user =>
                                    user.role_name &&
                                    (role.name ===
                                      user.role_name.replace(/\s/g, "") ||
                                      user.role_name
                                        .split("_")
                                        .includes(role.name))
                                ).length ? (
                                  rolesData.users_list.data
                                    .filter(
                                      user =>
                                        user.role_name &&
                                        (role.name ===
                                          user.role_name.replace(/\s/g, "") ||
                                          user.role_name
                                            .split("_")
                                            .includes(role.name))
                                    )
                                    .slice(0, 15)
                                    .map((user, i) => {
                                      return (
                                        <span>
                                          <img
                                            className={[
                                              s.profileAvatar,
                                              "rounded-circle before_manage_click_image"
                                            ].join(" ")}
                                            src={
                                              user.image ? user.image : avatar
                                            }
                                            alt="..."
                                          />
                                        </span>
                                      );
                                    })
                                ) : (
                                  <Col>No users have this role.</Col>
                                )}

                              </div>
                            
                            <div className="show_more_design">
                           <div className = {rolesData.users_list &&
                            rolesData.users_list.data.filter(
                              user =>
                                user.role_name &&
                                (role.name ===
                                  user.role_name.replace(/\s/g, "") ||
                                  user.role_name.split("_").includes(role.name)
                                )).length>15 && !this.state.showList?"rounded-designing":"d-none"}><span >{rolesData.users_list &&
                            rolesData.users_list.data.filter(
                              user =>
                                user.role_name &&
                                (role.name ===
                                  user.role_name.replace(/\s/g, "") ||
                                  user.role_name.split("_").includes(role.name)
                                )).length > 0 ?
                             `+${rolesData.users_list &&
                            rolesData.users_list.data.filter(
                              user =>
                                user.role_name &&
                                (role.name ===
                                  user.role_name.replace(/\s/g, "") ||
                                  user.role_name.split("_").includes(role.name)
                                )).length - 15}`:""}</span></div></div></div>
                            </div>
                            <div className="manage-arrow">
                             {role.is_system_default_role ? "" : 
                              <Button
                                className="delete_manage"
                                onClick={e => {
                                  this.handleDelete(role.id,role.name);
                                  e.stopPropagation();
                                }}
                              >
                                <i
                                  className="glyphicon glyphicon-trash"
                                  title="Delete Role"
                                />
                              </Button> }

                              {/* {collapseSelected === role.name && (
                                <Button
                                  className="user_manage_view pt-0"
                                  onClick={e => {
                                    this.props.chatToggle();
                                    e.stopPropagation();
                                  }}
                                >
                                  <i class="fa fa-users"></i>
                                </Button>
                              )} */}

                              <i
                                class={
                                  collapseSelected === role.name
                                    ? "fa fa-angle-up "
                                    : "fa fa-angle-down"
                                }
                              ></i>
                            </div>
                          </div>
                        </a>
                        <Collapse isOpen={role.name === collapseSelected}>
                          <div className="">
                            <Row className="mx-0">
                              {rolesData.users_list &&
                              rolesData.users_list.data.filter(
                                user =>
                                  user.role_name &&
                                  (role.name ===
                                    user.role_name.replace(/\s/g, "") ||
                                    user.role_name
                                      .split("_")
                                      .includes(role.name))
                              ).length ? (
                                rolesData.users_list.data
                                  .filter(
                                    user =>
                                      user.role_name &&
                                      (role.name ===
                                        user.role_name.replace(/\s/g, "") ||
                                        user.role_name
                                          .split("_")
                                          .includes(role.name))
                                  )
                                  .map((user, i) => {
                                    return (
                                      <Col
                                        lg={3}
                                        md={4}
                                        xs={12}
                                        className="role-user-div px-0 mb-2 "
                                      >
                                        <div>
                                          <div className="d-flex mx-2 p-2 manage_role_profile">
                                            <div>
                                              <img
                                                className={[
                                                  s.profileAvatar,
                                                  "rounded-circle image_manage_role"
                                                ].join(" ")}
                                                src={
                                                  user.image
                                                    ? user.image
                                                    : avatar
                                                }
                                                alt="..."
                                              />
                                            </div>

                                            <div className="pl-2">
                                              <div>{user.name}</div>
                                              <div className="text-body manage-role-title">
                                                {user.jobtitle}
                                              </div>
                                              <div className="role_dis_manage">
                                              <select value={user.role_id} onChange = { e => this.changeRole(e,user.user_Id)}>
                                            {this.state.rolesData.roles.map((item)=> 
                                              <option value = {item.id}>{item.name}</option>
                                            )}
                                            </select>
                                              </div>
                                            </div>
                                          </div>
                                          {/* <h5 className="fw-normal">
                                          {user.name}
                                        </h5> */}

                                        </div>
                                        
                                      </Col>
                                    );
                                  })
                              ) : (
                                <Col>No users have this role.</Col>
                              )}
                            </Row>
                          </div>
                        </Collapse>
                      </div>
                    );
                  })
              : null}
          </div>
          <div
            className={`${slide_open ? "col-md-3" : "d-none"} permission-block`}
          >
            {this.state.rolesData && Object.keys(this.state.rolesData).length
              ? this.state.rolesData.roles
                  .filter(role => !role.name.includes("_"))
                  .map((role, index) => {
                    return (
                      <Collapse isOpen={role.name === collapseSelected}>
                        <Card className="">
                          <div>
                            <DisplayRolesListsNew
                              role={role}
                              showActionsList={this.state.showActionsList}
                              displayData={this.state.rolesData}
                              handleClick={id => this.click(id)}
                              handleChange={this.handleChange}
                              handleChangeNotification={(
                                notificationId,
                                rolesId
                              ) =>
                                this.handleChangeNotification(
                                  notificationId,
                                  rolesId
                                )
                              }
                              toggleShowActions={() => {
                                this.setState({
                                  showActionsList: !this.state.showActionsList
                                });
                              }}
                              handleDelete={id => {
                                this.handleDelete(id);
                                this.setState({ deleteId: id });
                              }}
                            />
                          </div>
                        </Card>
                      </Collapse>
                    );
                  })
              : null}
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    frontend: state.frontend.toJS(),
    loggedUser: state.logged_user.userLogin,
    usersList: state.usersList.toJS(),
    manageRoles: state.manageRoles.toJS()
  };
};

const mapDispatchToProps = dispatch => {
  return {
    // ...bindActionCreators(actions, dispatch),
    onAddNewRole: newRoleDetails => {
      return dispatch(actionsManageRoles.addNewRole(newRoleDetails));
    },
    restoreUserRole: token => {
      return dispatch(actionsManageRoles.reStoreRole(token));
    },
    onRolesList: () => {
      return dispatch(actionsManageRoles.getRolesList());
    },
    onUpdateRole: roleUpdateDetails => {
      return dispatch(actionsManageRoles.updateRoles(roleUpdateDetails));
    },
    onUpdateUserRole: userRoleUpdateDetails => {
      return dispatch(actionsManageRoles.updateUserRole(userRoleUpdateDetails));
    },
    onDelete: id => {
      return dispatch(actionsManageRoles.deleteRole(id));
    },
    requestUsersList: () => {
      return dispatch(actions.requestUsersList());
    },
    changeCollapseSelected: selected => {
      return dispatch(actions.changeCollapseSelected(selected));
    },
    onDeleteUserRole: data => {
      return dispatch(actionsManageRoles.requestRemoveUserRole(data));
    },
    showHeading:(data)=>{
      return dispatch(actions.showHeading(data));
    }
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(manageRolesNew);
