import React, { Component } from "react";
import { connect } from "react-redux";
import * as actionsManageRoles from "../../../redux/manageRoles/actions/manageRoles";
import * as actions from "../../../redux/actions";
import * as actionsManageUsers from "../../../redux/manageUsers/actions/manageUsers";
import * as avatar from "../../../static/avatar.png";
import { Row, Col } from "reactstrap";
import s from "../../../components/profile/Profile.module.scss";
import Widget from "../../../components/Widget/Widget";
import { notify } from "../../../services/notify";

class RoleDetails extends Component {
  constructor(props) {
    super(props);
    this.state = {
      tooltips: []
    };
    this.props.onRolesList();
    this.props.requestUsersList();
  }
  toggle(id, field) {
    const newState = [...this.state[field]];
    newState.fill(false);

    if (!this.state[field][id]) {
      newState[id] = true;
    }

    this.setState({
      [field]: newState
    });
  }
  onUserRoleChange = userId => {
    const {
      match: {
        params: { id }
      },
      manageRoles: { rolesData }
    } = this.props;
    const role = rolesData.roles.find(
      role => id === role.name.replace(/\s/g, "")
    );
    this.props
      .onDeleteUserRole({ user_id: userId, role_id: role.id })
      .then(data => {
        notify("Success", data, "success");
      })
      .catch(error => {
        notify("Error", error, "error");
      });
  };
  render() {
    let {
      manageRoles: { rolesData },
      match
    } = this.props;
    return (
      <div>
        <Row>
          {rolesData && Object.keys(rolesData) && rolesData.users_list
            ? rolesData.users_list.data
                .filter(
                  user =>
                    match &&
                    match.params &&
                    user.role_name &&
                    (match.params.id === user.role_name.replace(/\s/g, "") ||
                      user.role_name.split("_").includes(match.params.id))
                )
                .map((user, i) => {
                  return (
                    <Col md={3} xs={12} className="text-center">
                      <Widget>
                        <div>
                          <span className="thumb-xl mb-3">
                            <img
                              className={[
                                s.profileAvatar,
                                "rounded-circle"
                              ].join(" ")}
                              src={user.image ? user.image : avatar}
                              alt="..."
                            />
                          </span>
                          <h5 className="fw-normal">{user.name}</h5>
                          <p>{user.jobtitle}</p>
                          {match &&
                          match.params &&
                          match.params.id !== "Employee" ? (
                            <button
                              className="btn btn-danger btn-sm mb-3"
                              onClick={() => this.onUserRoleChange(user.user_Id)}
                            >
                              Remove
                            </button>
                          ) : null}
                        </div>
                      </Widget>
                    </Col>
                  );
                })
            : null}
        </Row>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    frontend: state.frontend.toJS(),
    loggedUser: state.logged_user.userLogin,
    usersList: state.usersList.toJS(),
    manageRoles: state.manageRoles.toJS()
  };
};

const mapDispatchToProps = dispatch => {
  return {
    onAddNewRole: newRoleDetails => {
      return dispatch(actionsManageRoles.addNewRole(newRoleDetails));
    },
    onRolesList: () => {
      return dispatch(actionsManageRoles.getRolesList());
    },
    onUpdateRole: roleUpdateDetails => {
      return dispatch(actionsManageRoles.updateRoles(roleUpdateDetails));
    },
    onUpdateUserRole: userRoleUpdateDetails => {
      return dispatch(actionsManageRoles.updateUserRole(userRoleUpdateDetails));
    },
    requestUsersList: () => {
      return dispatch(actions.requestUsersList());
    },
    onDeleteUserRole: data => {
      return dispatch(actionsManageRoles.requestRemoveUserRole(data));
    },
    getAllEmployeeDetails: () => {
      return dispatch(actionsManageUsers.getAllEmployeeDetails());
    }
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(RoleDetails);
