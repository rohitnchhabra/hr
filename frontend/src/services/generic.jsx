import _ from "lodash";
import * as jwt from "jwt-simple";
import { CONFIG } from "../config/index";

export function isNotUserValid(path, loggedUser) {
  let tokenData = getLoggedUser().data || [];
  let isEmpty = _.isEmpty(
    _.find(tokenData.role_pages, o => {
      return `/app/${o.page_name}` === path;
    })
  );
  if (!loggedUser.isLoggedIn) {
    return { status: true, redirectTo: "/logout" };
  } else if (loggedUser.data.is_policy_documents_read_by_user === 0) {
    return { status: true, redirectTo: "policy_documents" };
  } else if (loggedUser.data.is_inventory_audit_pending === 1) {
    return { status: true, redirectTo: "my_inventory" };
  } else if (isEmpty) {
    return {
      status: true,
      redirectTo:"overview"
    };
  } else {
    return { status: false, redirectTo: "" };
  }
}

export function setLoggedUser(token, userid) {
  localStorage.setItem("userToken", token);
  localStorage.setItem("userid", userid);
  return jwt.decode(token, CONFIG.jwt_secret_key);
}

export function getLoggedUser() {
  const token = localStorage.getItem("userToken");
  const userId = localStorage.getItem("userid");
  if (_.isUndefined(token) || _.isEmpty(token) || _.isNull(token)) {
    return { token: false, userId: false, data: false };
  }
  
  return { token, userId, data: jwt.decode(token, CONFIG.jwt_secret_key) };
}

export function getToken() {
  return localStorage.getItem("userToken");
}

export function resetLoggedUser() {
  return localStorage.clear();
}

export function getLowerCase(text) {
  return text.trim().toLowerCase();
}

export function getTimePicker() {
  let arr = [],
    i,
    j;
  for (i = 9; i < 21; i++) {
    for (j = 0; j < 12; j++) {
      arr.push(
        ( (i <= 12 ? i > 9 ? i : '0'+ i : '0' + (i-12)) +
           ":" +
           (j === 0 ? "00" : j === 1 ? "05" : 5 * j) +
           (i < 12 ? "AM" : "PM")
       ));
    }
  }
  return arr;
}

export function getYearArray() {
  let date = new Date();
  let year = date.getYear() + 1900;
  let yearArray = [];
  for (let i = 0; i < 7; i++) {
    yearArray.push(parseInt(year) + i - 3);
  }
  return yearArray;
}

export function getYearArrayHoliday (){
  let date = new Date();
  let year = date.getYear() + 1900;
  let yearArray=[];
  for(let i=0; i<5 ; i++){
    yearArray.push(parseInt(year)+i-3)
  }
  return yearArray;
}

export const getFormattedMonth = (month) => {
  return month <= 9 ? "0" + month : month
}

export const getFromDate = (date) => {
  return date.getFullYear() + (date.getMonth() <= 9 ? "-0" : "-") + (date.getMonth() + 1) + (date.getDate() <= 9 ? "-0" : "-") + date.getDate();
}

export const getMonthName=(month)=>{
  const monthNames = ["January", "February", "March", "April", "May", "June",
  "July", "August", "September", "October", "November", "December"
];
return monthNames[month]
}

export const getMonday = d => {
  d = new Date(d);
  var day = d.getDay(),
    diff = d.getDate() - day + (day === 0 || day === 1 ? -6 : 1); // adjust when day is sunday
  return new Date(d.setDate(diff));
};

export const getNextMonady = d => {
  d = new Date(d);
  var day = d.getDay(),
    diff = d.getDate() + day + (day === 0 || day === 1 ? 6 : 1); // adjust when day is sunday
  return new Date(d.setDate(diff));
};

export function getWeekOfMonth(date) {
  let adjustedDate = date.getDate()+date.getDay();
  let prefixes = ['0', '1', '2', '3', '4', '5'];
  return (parseInt(prefixes[0 | adjustedDate / 7])+1);
 }
