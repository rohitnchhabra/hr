import React, { Component } from "react";
import { connect } from "react-redux";
import { withRouter } from "react-router";
import {Spinner} from "reactstrap";
class Load extends Component {
  render() {
    return (
      <div style={{ display: "flex"}}>
        <div className="loader">
          <Spinner color="#000000" />
        </div>
      </div>
    );
  }
}
function mapStateToProps(state) {
  return {
    frontend: state.frontend.toJS(),
    loggedUser: state.logged_user.userLogin
  };
}

export default withRouter(connect(mapStateToProps)(Load));
