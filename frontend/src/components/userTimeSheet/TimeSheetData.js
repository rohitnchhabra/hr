import React, { createRef } from "react";
import $ from "jquery";
import Dropzone from "react-dropzone";
import "timepicker/jquery.timepicker.css";
import "timepicker/jquery.timepicker.min.js";
import { Button, Alert, Spinner } from "reactstrap";
import axios from "axios";
import ImageCompressor from "image-compressor.js";
import { qualityValue, weekDays } from "../../helper/helper";
import { CONFIG } from "../../config/index";
import { getFromDate } from "../../services/generic";
import TextArea from '../../components/generic/TextArea/TextArea'
const htmlToText = require('html-to-text');

var moment = require("moment");

export default class empDaySummary extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      totalTime: "",
      comment: "",
      imageUrl: "",
      fileId: "",
      file: "",
      status: "",
      fileLoading: false
    };
    this.modalRef = createRef();
  }

  componentDidMount() {
    document.addEventListener("click", this.handleClickOutside, true);
  }

  componentDidUpdate(prevProps, prevState) {
    if (
      this.props.userTimeSheetSentData.isSuccess !==
        prevProps.userTimeSheetSentData.isSuccess &&
      this.props.userTimeSheetSentData.isSuccess
    ) {
      $("#modalUserDaySummary").modal("hide");
      this.setState({
        totalTime: "",
        comment: "",
        imageUrl: "",
        fileId: "",
        file: "",
        status: "",
        fileLoading: false
      });
    }
    if (
      JSON.stringify(prevProps.tmsReportData.data) !==
      JSON.stringify(this.props.tmsReportData.data)
    ) {
      if (this.props.tmsReportData.data.report) {
        const text = htmlToText.fromString(this.props.tmsReportData.data.report, {
          wordwrap: 130
        });
        this.setState({
          comment: text
        });
      } else {
        this.setState({
          comment: ""
        });
      }
    }
  }

  componentWillUnmount() {
    document.removeEventListener("click", this.handleClickOutside, true);
  }

  handleClickOutside = event => {
    if (
      this.modalRef.current &&
      !this.modalRef.current.contains(event.target) &&
      !event.target.className.includes("timepicker")
    ) {
      $("#modalUserDaySummary").modal("hide");
    }
  };
  onDrop = files => {
    this.setState({
      file: files,
      type:
        files[0].name &&
        files[0].name.split(".")[files[0].name.split(".").length - 1]
    });
    const reader = new FileReader();

    reader.onloadend = () => {
      this.setState({
        imageUrl: reader.result
      });
    };
    if (files) {
      reader.readAsDataURL(files[0]);
      this.setState({
        imageUrl: reader.result
      },()=>{
        this.uploadFile()
      });
    } else {
      this.setState({
        imageUrl: ""
      });
    }
  };
  sendUserTimeSheetdata = () => {
    const { full_date, user_id, from_date } = this.props;
    const { totalTime, comment, fileId } = this.state;
    let nowDate = new Date();
    let start_date = getFromDate(from_date);
    let applied_date = getFromDate(nowDate);
    if (totalTime) {
      let data = {
        date: full_date,
        user_id: user_id,
        applied_date: applied_date,
        hours: totalTime,
        fileId: fileId,
        comments: comment,
        from_date: start_date
      };
      this.props.onSendUserTimeSheetdata(data);
    }
  };

  uploadFile = () => {
    const token = localStorage.getItem("userToken");
    const fileName = "docs"; //the name must be same as in backend
    const url = CONFIG["generic_upload_url"];
    const file = this.state.file[0];
    if (!file) {
      return;
    } else if (
      !file.type.includes("image") ||
      (file.type.includes("image") && file.size < 500000)
    ) {
      this.setState({
        fileLoading: true
      });
      const formData = new FormData();

      formData.append("docs", file);

      formData.append("submit", "Upload");
      formData.append("file_upload_action", "timesheet_docs");
      formData.append("token", token);
      axios
        .post(url, formData)
        .then(data => {
          this.setState({
            fileId: data.data.fileId,
            status: data.data,
            fileLoading: false
          });
        })
        .catch(error => {
          this.setState({
            status: error,
            fileLoading: false
          });
        });
    } else {
      this.setState({
        fileLoading: true
      });
      let quality = qualityValue(file);
      let imageCompressor = new ImageCompressor();
      imageCompressor
        .compress(file, {
          quality: quality
        })
        .then(compressedFile => {
          const formData = new FormData();

          formData.append(fileName, compressedFile, file.name);
          formData.append("submit", "Upload");
          formData.append("token", token);

          axios
            .post(url, formData)
            .then(data => {
              this.setState({
                fileId: data.data.fileId,
                status: data.data,
                fileLoading: false
              });
            })
            .catch(error => {
              this.setState({
                status: error,
                fileLoading: false
              });
            });
        });
    }
  };

  render() {
    const { userTimeSheetSentData, full_date, tmsReportData } = this.props;
    
    const day = weekDays[(new Date(full_date)).getDay()];
    
    return (
      <div id="modalUserDaySummary" className="modal " data-backdrop="false">
        <div className="modal-dialog modal-lg">
          <div ref={this.modalRef} className="modal-content timesheet-modal">
            <div className="modal-header">
              <div className="row">
                <div className="col-xs-11">
                  <h5 className="modal-title">{day} Time Sheet</h5>
                </div>
                <div className="col-xs-1">
                  <button className="btn btn-icon white" data-dismiss="modal">
                    <i className="fa fa-remove" />
                  </button>
                </div>
              </div>
            </div>
            <div className="modal-body p-lg">
              {this.state.status && (
                <Alert
                  className="alert-transparent"
                  color={this.state.status.error ? "danger" : "success"}
                >
                  {this.state.status.message}
                </Alert>
              )}
              <div className="form-group row align-items-center">
                <label className="col-sm-2 form-control-label">
                  {"Total Time"}
                </label>
                <input
                  type="number"
                  step='0.01'
                  name="totalTime"
                  className="form-control col-sm-10"
                  value={this.state.totalTime}
                  onChange={e =>
                    this.setState({
                      totalTime: e.target.value
                    })
                  }
                  required
                />
              </div>
              <div className="form-group row align-items-center">
                <label className="col-sm-2 form-control-label">
                  {"Comment"}
                </label>
                {tmsReportData.isLoading ? 
                  <><Spinner color="black" /> <span className="ml-3">getting your report from tms...</span></> :
                  <div className="col-sm-10 px-0">
                    <TextArea
                    rows="8"
                    type="text"
                    name="comment"
                    value={this.state.comment}
                    onchange={e =>
                      this.setState({
                        comment: e.target.value
                      })
                    }
                    required
                  />
                  </div>
                }
                {tmsReportData.data && !tmsReportData.data.report ? (
                  <Alert className="offset-sm-2 mt-2" color="bold">
                    Its better if you put your detail reports on tms and HR
                    system will automatically fetch it for you
                  </Alert>
                ) : null}
              </div>
              <div className="row">
                <label className="col-sm-2 form-control-label">
                  {"Upload Tracker Screenshot"}
                </label>
                <div className="col-sm-10 px-0">
                  <Dropzone onDrop={this.onDrop}>
                    {({ getRootProps, getInputProps }) => {
                      return (
                        <section>
                          <div {...getRootProps({ className: "dropzone" })}>
                            <input {...getInputProps()} />
                            <div className="drag_and_drop align-items-center">
                              {this.state.file &&
                                this.state.file.map((file, index) => (
                                  <div className="font-weight-bold">
                                    {" "}
                                    {file.name}
                                  </div>
                                ))}

                              {this.state.imageUrl ? (
                                this.state.type === "pdf" ? (
                                  <iframe
                                    src={this.state.imageUrl}
                                    style={{ width: "200px" }}
                                    frameborder="0"
                                  ></iframe>
                                ) : (
                                  <img
                                    src={this.state.imageUrl}
                                    className="mx-3 img-preview"
                                    alt="Preview"
                                  />
                                )
                              ) : (
                                <p className="uploading_doc">
                                  <i className="fi flaticon-upload" />
                                </p>
                              )}
                              <p className="doc_upload_place">
                                Drop a document here or click to select file to
                                upload
                              </p>
                              {this.state.fileLoading &&<p>Uploading...</p>}
                            </div>
                          </div>
                        </section>
                      );
                    }}
                  </Dropzone>
                </div>
              </div>
              <div className="row py-3">
                <div className="col-sm-10 offset-sm-2 col-10 px-0">
                  <Button
                    color="success pull-right"
                    onClick={this.sendUserTimeSheetdata}
                    disabled={this.state.fileLoading}
                  >
                    {userTimeSheetSentData.isLoading ? <Spinner /> : "Submit"}
                  </Button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
