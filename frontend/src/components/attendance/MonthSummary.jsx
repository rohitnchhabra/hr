import React, { useState } from "react";
import PropTypes from "prop-types";
import DayReference from "./DayReference";
import CompensationSummary from "./compensationSummary";
import Dialog from "material-ui/Dialog";
import {
  Row,
  Col,
  Progress
} from 'reactstrap';
import Widget from '../generic/Widget/Widget';
import avatar from "../../avatar-3637425_960_720.png";
import {Spinner} from "reactstrap";


const MonthSummary = ({ monthlyAttendance }) => { 
  const [dialogOpen, handleDialog]  = useState(false);
  let totalCompletedWorkingHours = (monthlyAttendance.monthSummary.seconds_completed_working_hours/monthlyAttendance.monthSummary.seconds_actual_working_hours) * 100
  let totalPendingWorkingHours = (monthlyAttendance.monthSummary.seconds_pending_working_hours/monthlyAttendance.monthSummary.seconds_actual_working_hours) * 100
  let timeToBeCompensate = (monthlyAttendance.compensationSummary.seconds_to_be_compensate/monthlyAttendance.monthSummary.seconds_actual_working_hours) * 100
  let timeToBeCompensateInMinutes  = monthlyAttendance.compensationSummary &&  monthlyAttendance.compensationSummary.time_to_be_compensate.split(":").slice(0,-1).join(":")
  const handleDialogClose = () =>{
    handleDialog(false);
  }
  let preJoining=false
  monthlyAttendance.attendance.every(element => {
    if(element.isDayBeforeJoining){
      preJoining=true;
      return false;
    }
  });

  return (
  <div>
    <Row className="working-summary-member-card-row mx-0">
          <Col lg={4} xs={12} className="working-summary-member-card">
            <Widget className="widget-all-cards">
              {monthlyAttendance.userName ?
                 <div className="stats-row d-flex">
                    <div className="stat-item-avatar-man d-flex">
                      <img className="mr-3" src={monthlyAttendance.userProfileImage ? monthlyAttendance.userProfileImage : avatar} alt="" />
                      <div>
                        <h5 className='mydash_username'> {monthlyAttendance.userName} </h5>
                        <h6>{monthlyAttendance.userjobtitle}</h6>
                      </div>
                    </div>
                </div> 
              :
                <div  className="attendance-cards-spinner">
                  <Spinner color="black" />
                </div>
              }
            </Widget>
          </Col>
          <Col lg={4} xs={12} className="working-summary-member-card">
            <Widget className="widget-all-cards">
           {monthlyAttendance.userName ? <>
             
              <p className="text-muted d-flex flex-wrap pt-0">
                <small className="mr-lg d-flex align-items-center">
                  <span className="circle bg-success text-success mr-xs" style={{ fontSize: '4px' }}>.</span>
                  Completed
                </small>
                <small className="mr-lg d-flex align-items-center">
                  <span className="circle bg-danger text-danger mr-xs" style={{ fontSize: '4px' }}>.</span>
                  Pending
                </small>
              </p>
              <div className="row progress-stats">
                <div className="col-12">
                  <Progress color="success" value={`${totalCompletedWorkingHours}`} className="progress-xs" />
                </div>
                <div className="col-12 d-flex justify-content-between">
                  <h6 className="mb-xs deemphasize">{monthlyAttendance.monthSummary.completed_working_hours}</h6>
                  <h6 className="mb-xs deemphasize">{monthlyAttendance.monthSummary.pending_working_hours}</h6>
                </div>
                <div className="col-12">
                  <Progress color="danger" value={`${totalPendingWorkingHours}`} className="progress-xs" />
                </div>
              </div>
              </> : 
                <div className="attendance-cards-spinner">
                  <Spinner color="black" />
                </div>}
            </Widget>
          </Col>
          <Col lg={4} xs={12} className="working-summary-member-card">
            <Widget className="widget-all-cards">
              {monthlyAttendance.userName ? <>
              <div className="row progress-stats">
                <div className="col-md-9 col-12">
                  <p className="description deemphasize mb-xs pt-0">Time to be compensate
                  <small className="mobile_preview_manage pull-right pr-3">{ timeToBeCompensate.toFixed(2)}%</small>
                  </p>
                  <Progress color="danger" value={`${timeToBeCompensate}`} className="progress-xs" />
                </div>
                <div className="col-md-3 col-12 text-center manage_preview_mobile">
                  <span className="status rounded rounded-lg bg-body-light">
                    <small>{ timeToBeCompensate.toFixed(2)}%</small>
                  </span>
                </div>
                
              </div> 
              <div className="row">
                  <div className="small-plus-add-icon-attendance col-md-4" onClick={()=>{handleDialog(true)}}>
                    <span className="circle bg-warning text-white add-icon-attendance">
                      <i className="icon_attendance fa fa-info-circle" aria-hidden="true" />
                    </span>
                    <h6 className="working-summary-hours">{timeToBeCompensateInMinutes}</h6>
                  </div>
                  <div className="col-md-8">
                    <div className="col-md-6 col-12 px-0">
                      <p className="description deemphasize mb-xs">Total Working  <span className="mobile_preview_manage pull-right pr-3"> {monthlyAttendance.monthSummary.actual_working_hours} </span>  </p>
                    </div>
                    <div className="col-md-6 col-12 px-0 text-center manage_preview_mobile">
                      <p className="mb-xs">{monthlyAttendance.monthSummary.actual_working_hours}</p>
                    </div>
                  </div>
                </div>
              </>:
              <div  className="attendance-cards-spinner">
                    <Spinner color="black" />
              </div>}
            </Widget>
          </Col>
          </Row>
    <DayReference monthlyAttendance={monthlyAttendance} preJoining={preJoining}/>
    <Dialog 
          className="time_compensation_title"
          title="Time Compensation Summary"
          modal={false}
          open={dialogOpen}
          onRequestClose={handleDialogClose}
          contentStyle={{ width: "80%", maxWidth: "auto" }}
          autoScrollBodyContent
        >
        {monthlyAttendance.compensationSummary &&
        monthlyAttendance.compensationSummary.seconds_to_be_compensate > 0 ? (
          <CompensationSummary monthlyAttendance={monthlyAttendance} />
          ) : (
            <div className="box">
              <div className="box-header text-center">
                <h3 className="description deemphasize mb-xs">Nothing To Compensate</h3>
              </div>
            </div>
          )}
    </Dialog>
  </div>
)};

MonthSummary.propTypes = {
  monthlyAttendance: PropTypes.object.isRequired,
  compensationSummary: PropTypes.object
};

export default MonthSummary;
