import React from "react";
import PropTypes from "prop-types";
import classnames from "classnames";
const DayReference = ({ monthlyAttendance, leaveReference, header, content , abbreviation,preJoining }) => (
  <div>
    <div className="day">
      <div className="row no-gutter m-b text-xs l-h-1x">
        <div className="day-color-reference"> 
          <div className={`${leaveReference ? "pp-a" : "p-a white"}`}>
            <h5>{leaveReference ? header : "Day Reference"}</h5>
          </div>
        </div>
        {monthlyAttendance &&
        <div className="day-color-reference">
          <div className="p-a normal-day-view white">
             <h4>{monthlyAttendance.monthSummary.WORKING_DAY}</h4>
            <div className="text-u-c _600 text-sm"> Working Day</div>
          </div>
        </div>
      }

        <div className="day-color-reference">
          <div className={` ${abbreviation ? "pp-a" : leaveReference ? "pp-a bgg-warning" :  "p-a yellow"}`}>
            {monthlyAttendance && <h4>{monthlyAttendance.monthSummary.NON_WORKING_DAY}</h4>}
            <div className={classnames(
          "text-u-c _600 text-sm",
          leaveReference ? "py-2" : null
        )}> {leaveReference ? content[0] :"Non Working Day"}</div>
          </div>
        </div>

        <div className="day-color-reference">
          <div className={`${abbreviation ? "pp-a" : leaveReference ?  "pp-a red" : "p-a red"}`}>
            {monthlyAttendance && <h4>{monthlyAttendance.monthSummary.LEAVE_DAY}</h4>}
            <div className={classnames(
          "text-u-c _600 text-sm",
          leaveReference ? "py-2" : null
        )}>{leaveReference ? content[1] : "Leave Day"}</div>
          </div>
        </div>

        <div className="day-color-reference">
          <div className={`${abbreviation ? "pp-a" : leaveReference ? "pp-a bgg-success" : "p-a red-100"}`}>
            {monthlyAttendance && <h4>{monthlyAttendance.monthSummary.HALF_DAY}</h4>}
            <div className={classnames(
          "text-u-c _600 text-sm",
          leaveReference ? "py-2" : null
        )}>{leaveReference ? content[2] : "Half Day"}</div>
          </div>
        </div>

        {(abbreviation || leaveReference) &&<div className="day-color-reference">
          <div className={`${abbreviation ? "pp-a" : leaveReference ? "pp-a bgg-white" : "p-a red-100"}`}>
            {monthlyAttendance && <h4>{monthlyAttendance.monthSummary.HALF_DAY}</h4>}
            <div className={classnames(
          "text-u-c _600 text-sm",
          leaveReference ? "py-2" : null
        )}>{leaveReference && content[3] ? content[3] : "Half Day"}</div>
          </div>
        </div>
        }

       {monthlyAttendance && preJoining &&
         <div className="day-color-reference">
          <div className="p-a before_joining_label">
          <h4>{Object.keys(monthlyAttendance.monthSummary).length>0 && "*"}</h4>
            <div className="text-u-c _600 text-sm">Pre Joining</div>
          </div>
        </div>
       }
        {monthlyAttendance && 
         <div className="day-color-reference">
          <div className="p-a indigo">
          <h4>{Object.keys(monthlyAttendance.monthSummary).length>0 && "*"}</h4>
            <div className="text-u-c _600 text-sm">Alert</div>
          </div>
        </div>
       }
      </div>
    </div>
    {!leaveReference &&
      <div className="day  no-gutter row my-3 " id="week_days">
        <div className="col-md-1 ">Monday</div>
        <div className="col-md-1">Tuesday</div>
        <div className="col-md-1">Wednesday</div> 
        <div className="col-md-1">Thursday</div>
        <div className="col-md-1">Friday</div>
        <div className="col-md-1">Saturday</div>
        <div className="col-md-1">Sunday</div>
      </div>
    }
  </div>
);

DayReference.propTypes = {
  monthlyAttendance: PropTypes.shape({
    monthSummary: PropTypes.object.isRequired
  }).isRequired
};

export default DayReference;
