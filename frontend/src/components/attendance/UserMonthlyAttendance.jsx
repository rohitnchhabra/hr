import React from "react";
import PropTypes from "prop-types";
import CalendarStructure from "../../components/generic/employeeMonthGrid/CalendarStructure";
import MonthSummary from "./MonthSummary";
import { Spinner } from "reactstrap";

const UserMonthlyAttendance = ({
  monthlyAttendance,
  monthToggle,
  onShowDaySummary,
  buttonClicked,
}) => {
  const _onChangeMonth = check => {
    if (check === "previous") {
      monthToggle(
        monthlyAttendance.userid,
        monthlyAttendance.previousMonth.year,
        monthlyAttendance.previousMonth.month,
        check
      );
    } else if (check === "next") {
      monthToggle(
        monthlyAttendance.userid,
        monthlyAttendance.nextMonth.year,
        monthlyAttendance.nextMonth.month,
        check
      );
    }
  };
  return (
    <div className="fullcalendar fc fc-ltr fc-unthemed">
      <div className="fc-toolbar">
        <div className="fc-left">
          {monthlyAttendance.isAttendanceLoading &&
          buttonClicked === "previous" ? (
            <button
              type="button"
              className="fc-prev-button fc-button fc-state-default fc-corner-left fc-corner-right"
            >
              <Spinner color="light" />
            </button>
          ) : (
            <button
              type="button"
              className="fc-prev-button fc-button fc-state-default fc-corner-left fc-corner-right"
              onClick={() => _onChangeMonth("previous")}
            >
              <span className="fc-icon fc-icon-left-single-arrow" />
            </button>
          )}
        </div>
        <div className="fc-right">
          {monthlyAttendance.isAttendanceLoading && buttonClicked === "next" ? (
            <button
              type="button"
              className="fc-next-button fc-button fc-state-default fc-corner-left fc-corner-right"
            >
              <Spinner color="light" />
            </button>
          ) : (
            <button
              type="button"
              className="fc-next-button fc-button fc-state-default fc-corner-left fc-corner-right"
              onClick={() => _onChangeMonth("next")}
            >
              <span className="fc-icon fc-icon-right-single-arrow" />
            </button>
          )}
        </div>
        <div className="fc-center">
          <h2>
            {monthlyAttendance.monthName}-{monthlyAttendance.year}
          </h2>
        </div>
        <div className="fc-clear" />
      </div>
      <br />
      <MonthSummary monthlyAttendance={monthlyAttendance} />
      <div className="fc-view-container">
        <div className="fc-view fc-month-view fc-basic-view">
          <div className="fc-body">
            <div className="fc-widget-content">
              <div className="fc-day-grid-container">
                <div className="fc-day-grid">
                  <CalendarStructure
                    userId={monthlyAttendance.userid}
                    month={monthlyAttendance.attendance}
                    onShowDaySummary={onShowDaySummary}
                    userName={monthlyAttendance.userName}
                  />
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

UserMonthlyAttendance.propTypes = {
  monthlyAttendance: PropTypes.shape({
    userid: PropTypes.oneOfType([
      PropTypes.string,
      PropTypes.number,
    ]).isRequired,
    previousMonth: PropTypes.object.isRequired,
    nextMonth: PropTypes.object.isRequired,
    attendance: PropTypes.oneOfType([
      PropTypes.array,
      PropTypes.object,
    ]).isRequired,
    monthName: PropTypes.string.isRequired,
    year: PropTypes.oneOfType([
      PropTypes.string,
      PropTypes.number,
    ]).isRequired
  }).isRequired
};

export default UserMonthlyAttendance;
