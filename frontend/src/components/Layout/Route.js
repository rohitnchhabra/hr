import React, { lazy } from "react";

const Page_ManageLeaves = lazy(() =>
  import("../../modules/leave/containers/manageLeaves")
);
const Page_LeavesSummary = lazy(() =>
  import("../../modules/leave/containers/leavesSummary")
);
const Page_ApplyLeave = lazy(() =>
  import("../../modules/leave/containers/applyLeave")
);
const Page_MyLeaves = lazy(() =>
  import("../../modules/leave/containers/myLeaves")
);
const Page_Holidays = lazy(() =>
  import("../../modules/holidays/containers/holidays")
);
const Page_ManageHolidays = lazy(() =>
  import("../../modules/holidays/containers/manageHolidays")
);
const Page_PolicyDocument = lazy(() =>
  import("../../modules/policyDocuments/containers/PolicyDocument")
);
const Page_UploadPolicyDocument = lazy(() =>
  import("../../modules/policyDocuments/containers/uploadPolicyDocument")
);
const Page_ManageSalary = lazy(() =>
  import("../../modules/salary/containers/manageSalary")
);
const Page_ViewSalary = lazy(() =>
  import("../../modules/salary/containers/viewSalary")
);
const Page_Salary = lazy(() =>
  import("../../modules/salary/containers/salary")
);
const Page_ManagePayslips = lazy(() =>
  import("../../modules/salary/containers/managePayslips")
);
const Page_Home = lazy(() =>
  import("../../modules/attendance/containers/Home")
);
const Page_MonthlyAttendance = lazy(() =>
  import("../../modules/attendance/containers/monthlyAttendance")
);
const Page_UploadAttendance = lazy(() =>
  import("../../modules/attendance/containers/uploadAttendance")
);
const UserInventoryDetails = lazy(() =>
  import("../../components/inventory/ViewUser")
);
const UnapprovedInventories = lazy(() =>
  import("../../modules/inventory/components/UnapprovedInventory")
);
const Page_MyDocuments = lazy(() =>
  import("../../modules/myDocuments/containers/myDocuments")
);
const PageManageUsers = lazy(() =>
  import("../../modules/manageUsers/containers/manageUsers")
);
const PageAllEmployee = lazy(() =>
  import("../../modules/manageUsers/containers/allEmployesDetails")
);
const Page_ManageWorkingHours = lazy(() =>
  import("../../modules/workingHours/containers/manageWorkingHours")
);
const Page_ManageUserWorkingHours = lazy(() =>
  import("../../modules/workingHours/containers/manageUserWorkingHours")
);
const InventorySystem = lazy(() =>
  import("../../modules/inventory/containers/manageInventory")
);
const Page_MyInventory = lazy(() =>
  import("../../modules/inventory/containers/myInventory")
);
const PageManageRoles = lazy(() =>
  import("../../modules/manageRoles/containers/manageRoles")
);
const PageManageRolesNew = lazy(() =>
  import("../../modules/manageRoles/containers/manageRolesNew")
);
const PageRoleDetails = lazy(() =>
  import("../../modules/manageRoles/containers/RoleDetails")
);
const Page_ManageUserPendingHours = lazy(() =>
  import("../../modules/workingHours/containers/manageUserPendingHours")
);
const PageDisabledEmployes = lazy(() =>
  import("../../modules/manageUsers/containers/disabledEmployes")
);
const Page_AddVariables = lazy(() =>
  import("../../modules/templates/containers/addVariables")
);
const Page_mail_template = lazy(() =>
  import("../../modules/templates/containers/addTemplate")
);
const Page_TeamView = lazy(() =>
  import("../../modules/team/containers/viewTeam")
);
const Page_MyProfile = lazy(() =>
  import("../../modules/myProfile/containers/myProfile")
);
const Page_AttendanceReq = lazy(() =>
  import("../../modules/attendance/containers/manageAttendanceReq")
);
const FormAddNewEmployeeDetails = lazy(() =>
  import("../../modules/manageUsers/components/FormAddNewEmployeeDetails")
);
const AddNewEmployee = lazy(() =>
  import("../../modules/manageUsers/containers/AddNewEmployee")
);
const UserDocumentDetails = lazy(() =>
  import("../../modules/manageUsers/components/Userdocuments")
);
const Page_InventoryOverview = lazy(() =>
  import("../../modules/inventoryOverview/container/inventoryOverviewContainer")
);
const RouterAddInventorySystem = lazy(() =>
  import("../../modules/inventory/components/AddInventory")
);
const Page_Audit_Inventory = lazy(() =>
  import("../../modules/inventory/components/AuditInventoryList")
);
const ContainerHealthStats = lazy(() =>
  import("../../modules/healthStats/container/ContainerHealthStats")
);
const Page_settings = lazy(() =>
  import("../../modules/healthStats/container/Settings")
);
const RH_Stats = lazy(() => import("../../modules/leave/containers/rhStats"));

const ShowMenuContainer = lazy(() =>
  import("../../modules/showMenuItem/container/ShowMenuContainer")
);

const Profile = lazy(() => import("../profile/Profile"));

const PageManageDashboard = lazy(() =>
  import("../../modules/manageUsers/containers/manageDashboard")
);

const Page_Overview=lazy(()=> import("../../modules/landing/Landing"))

const MultipleInventoryUpload = lazy(()=> import("../../modules/inventory/components/MultipleInventoryUpload"))

const PageAddTemplate = lazy(() =>
  import("../../modules/templates/containers/AddNewTemplate")
);
const UserTimeSheet = lazy(() =>
  import("../../modules/userTimeSheet/timeSheetcalendar")
);
const TimeSheetEntries = lazy(() =>
  import("../../modules/userTimeSheet/TimeSheetEntries")
);
const TimeSheetPerWeek = lazy(() =>
  import("../../modules/userTimeSheet/TimeSheetPerWeek")
);
export  const Routes=[
    {
    path:"/app/overview",
    component:Page_Overview,
    matchPath:"overview"
    },
    {
    path:"/app/manage_leaves",
    component:Page_ManageLeaves,
    matchPath:"manage_leaves"
    },
    {
    path:"/app/home",
    component:Page_Home,
    matchPath:"home"
    },
    {
    path:"/app/monthly_attendance",
    component:Page_MonthlyAttendance,
    matchPath:"monthly_attendance"
    },
    {
    path:"/app/manage_working_hours",
    component:Page_ManageWorkingHours,
    matchPath:"manage_working_hours"
    },
    {
    path:"/app/holidays",
    component:Page_Holidays,
    matchPath:"holidays"
    },
    {
      path:"/app/manageHolidays",
      component:Page_ManageHolidays,
      matchPath:"manageHolidays"
      },
    {
    path:"/app/team_view",
    component:Page_TeamView,
    matchPath:"team_view"
    },
    {
    path:"/app/apply_leave",
    component:Page_ApplyLeave,
    matchPath:"apply_leave"
    },
    {
    path:"/app/apply_emp_leave",
    component:Page_ApplyLeave,
    matchPath:"apply_emp_leave"
    },
    {
    path:"/app/my_leaves",
    component:Page_MyLeaves,
    matchPath:"my_leaves"
    },
    {
    path:"/app/disabled_employees",
    component:PageDisabledEmployes,
    matchPath:"disabled_employees"
    },
    {
    path:"/app/manage_user_working_hours",
    component:Page_ManageUserWorkingHours,
    matchPath:"manage_user_working_hours"
    },
    {
    path:"/app/manage_user_pending_hours",
    component:Page_ManageUserPendingHours,
    matchPath:"manage_user_pending_hours"
    },
    {
    path:"/app/leaves_summary",
    component:Page_LeavesSummary,
    matchPath:"leaves_summary"
    },
    {
    path:"/app/salary",
    component:Page_Salary,
    matchPath:"salary"
    },
    {
    path:"/app/manage_salary",
    component:Page_ManageSalary,
    matchPath:"manage_salary"
    },
    {
    path:"/app/edit_profile",
    component:Page_MyProfile,
    matchPath:"my_profile"
    },
    {
    path:"/app/my_inventory",
    component:Page_MyInventory,
    matchPath:"my_inventory"
    },
    {
    path:"/app/manage_users/:id?",
    component:PageManageUsers,
    matchPath:"manage_users"
    },
    {
    path:"/app/all_employee",
    component:PageAllEmployee,
    matchPath:"all_employee"
    },
    {
    path:"/app/manage_roles/:id",
    component:PageRoleDetails,
    matchPath:"manage_roles"
    },
    {
    path:"/app/manage_roles",
    component:PageManageRolesNew,
    matchPath:"manage_roles"
    },
    // {
    // path:"/app/manage_roles",
    // component:PageManageRoles,
    // matchPath:"manage_roles"
    // },
    {
    path:"/app/manage_payslips",
    component:Page_ManagePayslips,
    matchPath:"manage_payslips"
  },
  {
    path: "/app/documents",
    component: Page_MyDocuments,
    matchPath: "documents"
  },
  {
    path: "/app/uploadAttendance",
    component: Page_UploadAttendance,
    matchPath: "uploadAttendance"
  },
  {
    path: "/app/view_salary",
    component: Page_ViewSalary,
    matchPath: "view_salary"
  },
  {
    path: "/app/policy_documents",
    component: Page_PolicyDocument,
    matchPath: "policy_documents"
  },
  {
    path: "/app/upload_policy_documents",
    component: Page_UploadPolicyDocument,
    matchPath: "upload_policy_documents"
  },
  {
    path: "/app/add_variables",
    component: Page_AddVariables,
    matchPath: "add_variables"
  },
  {
    path: "/app/mail_templates",
    component: Page_mail_template,
    matchPath: "mail_templates"
  },
  // {
  // path:"/inventoryOverviewDetail",
  // component:Page_InventorySystem
  // },
  // {
  // path:"/app/inventory_system/:device/:id",
  // component:InventoryItem,
  // matchPath:"inventoryOverviewDetail",
  // },
  {
    path: "/app/inventoryOverviewDetail/:device",
    component: InventorySystem,
    matchPath: "inventoryOverviewDetail"
  },
  {
    path: "/app/attendanceReq",
    component: Page_AttendanceReq,
    matchPath: "attendanceReq"
  },
  {
    path: "/app/inventoryOverviewDetail",
    component: Page_InventoryOverview,
    matchPath: "inventoryOverviewDetail"
  },
  {
    path: "/app/user_inventory_details",
    component: UserInventoryDetails,
    matchPath: "user_inventory_details"
  },
  {
    path: "/app/unapproved_inventory",
    component: UnapprovedInventories,
    matchPath: "unapproved_inventory"
  },
  {
    path: "/app/audit_inventory_list",
    component: Page_Audit_Inventory,
    matchPath: "audit_inventory_list"
  },
  {
    path: "/app/health_stats",
    component: ContainerHealthStats,
    matchPath: "health_stats"
  },
  {
    path: "/app/settings",
    component: Page_settings,
    matchPath: "settings"
  },
  {
    path: "/app/rh_status",
    component: RH_Stats,
    matchPath: "rh_status"
  },
  {
    path: "/app/addInventory",
    component: RouterAddInventorySystem,
    matchPath: "addInventory"
  },
  {
    path: "/app/add_new_employee",
    component: AddNewEmployee,
    matchPath: "add_new_employee"
  },
  // {
  // path:"/app/add_new_employee/:id",
  // component:FormAddNewEmployeeDetails,
  // matchPath:"add_new_employee",
  // },
  {
    path: "/app/user_document",
    component: UserDocumentDetails,
    matchPath: "user_document"
  },
  {
    path: "/app/show_menu",
    component: ShowMenuContainer,
    matchPath: "show_menu"
  },
  {
    path: "/app/my_profile",
    component: Profile,
    matchPath: "my_profile"
  },
  {
    path: "/app/page_dashboard",
    component: PageManageDashboard,
    matchPath: "page_dashboard"
  },
  {
    path: "/app/add_teplate",
    component: PageManageDashboard,
    matchPath: "page_dashboard"
  },
  {
    path: "/app/add_templates",
    component: PageAddTemplate,
    matchPath: "addTemplate"
  },
  {
    path: "/app/multipleInventoryUpload",
    component: MultipleInventoryUpload,
    matchPath: "multipleInventoryUpload"
  },
  {
    path: "/app/user_timeSheet",
    component:UserTimeSheet,
    matchPath:"user_timeSheet"
  },
  {
    path: "/app/all_user_timesheet",
    component:TimeSheetEntries,
    matchPath:"all_user_timesheet"
  },
  {
    path: "/app/pending_timesheets_per_month",
    component:TimeSheetPerWeek,
    matchPath:"pending_timesheets_per_month"
    // matchPath:"timesheet_per_week"
  }
];
