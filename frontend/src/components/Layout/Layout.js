import React, { lazy, Suspense } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { Switch, Route, withRouter, Redirect } from "react-router";
import { TransitionGroup, CSSTransition } from "react-transition-group";
import $ from "jquery";
import Hammer from "rc-hammerjs";
import Header from "../Header";
import Sidebar from "../Sidebar";
import UserList from "../UserList/UserList";
import DesktopUserList from "../UserList/DesktopUserList";
import classnames from "classnames";
import { getLocationId } from "../../helper/helper";
import isMobile from "../../components/hoc/WindowResize";
import { compose } from "redux";
import _ from "lodash";
import moment from "moment";
import { Routes } from "./Route";
import isEqual from "lodash/isEqual";
import {
  openSidebar,
  closeSidebar,
  changeActiveSidebarItem,
  toggleSidebar,
  isAlreadyLogin
} from "../../redux/actions";
import s from "./Layout.module.scss";
import { getLoggedUser } from "../../services/generic";
import DeniedComponent from "../../modules/denied/Denied";
import AccessDenied from "../../modules/denied/AccessDenied";
// import { DashboardThemes } from "../../reducers/layout";
import MenuDefault from "../menu/LoaderMenuDefault";

class Layout extends React.Component {
  static propTypes = {
    sidebarStatic: PropTypes.bool,
    sidebarOpened: PropTypes.bool,
    dashboardTheme: PropTypes.string,
    dispatch: PropTypes.func.isRequired
  };

  static defaultProps = {
    sidebarStatic: false,
    sidebarOpened: false
    // dashboardTheme: DashboardThemes.DARK
  };
  constructor(props) {
    super(props);
    this.state = {
      chatOpen: false,
      validRouteForUser: []
    };
    this.props.dispatch(isAlreadyLogin());
    if (!getLoggedUser().token) {
      this.props.history.push("/page_login");
    }
  }
  componentDidUpdate(props) {
    let validRouteForUser = [];
    validRouteForUser.push(Routes[0]);
    if (
      !isEqual(
        this.props.loggedUser.data.role_pages,
        props.loggedUser.data.role_pages
      )
    ) {
      getLoggedUser().data.role_pages &&
        getLoggedUser().data.role_pages.forEach(element => {
          Routes.forEach((pathItem, i) => {
            if (pathItem.matchPath === element.page_name) {
              validRouteForUser.push(pathItem);
            }
          });
        });
      this.setState({ validRouteForUser: _.uniqBy(validRouteForUser, "path") });
    }
  }
  componentDidMount() {
    let validRouteForUser = [];
    validRouteForUser.push(Routes[0]);
    const staticSidebar = JSON.parse(localStorage.getItem("staticSidebar"));
    if (staticSidebar && window.innerWidth > 768) {
      this.props.dispatch(toggleSidebar());
    } else if (this.props.sidebarOpened) {
      setTimeout(() => {
        this.props.dispatch(closeSidebar());
        this.props.dispatch(changeActiveSidebarItem(null));
      }, 2500);
    }

    this.handleResize();
    window.addEventListener("resize", this.handleResize.bind(this));
    getLoggedUser().data.role_pages &&
      getLoggedUser().data.role_pages.forEach(element => {
        Routes.forEach((pathItem, i) => {
          if (pathItem.matchPath === element.page_name) {
            validRouteForUser.push(pathItem);
          }
        });
      });
    this.setState({ validRouteForUser: _.uniqBy(validRouteForUser, "path") });
// check on initial page load
    setTimeout(() => {
      let loggedInTime = moment(getLoggedUser().data.login_date_time, 'D-MMM-YYYY HH:mm:ss');
      let currTime = moment();
      let duration = currTime.diff(loggedInTime, "hours");
      if (duration >= 1) {
        localStorage.clear();
        this.props.history.push("/page_login");
      }
    }, 0);

//check after every 1min
    let session = setInterval(() => {
      let loggedInTime = moment(getLoggedUser().data.login_date_time, 'D-MMM-YYYY HH:mm:ss' );
      let currTime = moment();
      let duration = currTime.diff(loggedInTime, "hours");
      if (duration >= 1) {
        localStorage.clear();
        clearInterval(session);
        this.props.history.push("/page_login");
      }
    }, 60000);
  }

  componentWillUnmount() {
    window.removeEventListener("resize", this.handleResize.bind(this));
  }

  handleResize() {
    if (window.innerWidth <= 768 && this.props.sidebarStatic) {
      this.props.dispatch(toggleSidebar());
    }
  }

  chatToggle = val => {
    if (val !== undefined) {
      this.setState({ chatOpen: val });
      $(".chat-notification-sing").remove();
    } else {
      this.setState({ chatOpen: !this.state.chatOpen });
      $(".chat-notification-sing").remove();
    }
  };

  handleSwipe = e => {
    if ("ontouchstart" in window) {
      if (e.direction === 4 && !this.state.chatOpen) {
        this.props.dispatch(openSidebar());
        return;
      }

      if (e.direction === 2 && this.props.sidebarOpened) {
        this.props.dispatch(closeSidebar());
        return;
      }

      this.setState({ chatOpen: e.direction === 2 });
    }
  };

  sendMouseHoverDataParent = status => {
    this.setState({
      hoverStatus: status
    });
  };
  render() {
    let loggedInTime = moment(getLoggedUser().data.login_date_time, 'D-MMM-YYYY HH:mm:ss');
    let currTime = moment();
    let duration = currTime.diff(loggedInTime, "hours");
    const {
      usersList,
      loggedUser: { data },
      location: { pathname }
    } = this.props;
    if (!getLoggedUser().token) {
      return null;
    }
    let userType = "";
    if (
      pathname === "/app/disabled_employees" &&
      usersList.disabled_users &&
      usersList.disabled_users.length > 0
    ) {
      userType = "disable";
    } else if (
      pathname !== "/app/disabled_employees" &&
      usersList.users &&
      usersList.users.length > 0
    ) {
      userType = "active";
    }
    return (
      <div
        className={[
          s.root,
          this.props.sidebarStatic ? s.sidebarStatic : "",
          this.state.chatOpen ? s.chatOpen : "",
          !this.props.sidebarOpened ? s.sidebarClose : "",
          "sing-dashboard",
          "dashboard-dark"
        ].join(" ")}
      >
        <Sidebar />
        <div
          className={classnames(
            s.wrap,
            pathname.includes("manage_payslips")
              ? "parent-layout-container"
              : ""
          )}
        >
          <div className={s.containerLayout}>
            <Header chatToggle={this.chatToggle} />
            {usersList &&
              ((usersList.users && usersList.users.length > 0) ||
                (usersList.disabled_users &&
                  usersList.disabled_users.length > 0)) && (
                <UserList
                  chatOpen={this.state.chatOpen}
                  usersList={
                    pathname === "/app/disabled_employees"
                      ? this.props.usersList.disabled_users
                      : this.props.usersList.users
                  }
                  chatToggle={this.chatToggle}
                />
              )}
            <Hammer onSwipe={this.handleSwipe}>
              <main className={classnames(s.content, "d-flex p-sm-3")}>
                {this.props.isMobile > 576 &&
                  usersList &&
                  userType &&
                  data &&
                  data.role !== "Employee" &&
                  !pathname.includes("manage_payslips") &&
                  getLocationId(pathname) && (
                    <DesktopUserList
                      chatOpen={true}
                      usersList={
                        pathname === "/app/disabled_employees"
                          ? this.props.usersList.disabled_users
                          : this.props.usersList.users
                      }
                      chatToggle={this.chatToggle}
                      layoutStyle={s}
                      sendMouseHoverDataParent={this.sendMouseHoverDataParent}
                    />
                  )}
                <div
                  className={classnames({
                    "ml-0 app_page_width":
                      usersList &&
                      usersList.users &&
                      usersList.users.length > 0,
                    [s.sidebarStatic]: this.state.hoverStatus,
                    [s.contentWithList]:
                      !this.state.hoverStatus &&
                      getLocationId(pathname) &&
                      this.props.isMobile > 576,
                    [s.contentMinimize]: this.state.hoverStatus,
                    [s.contentFull]:
                      !getLocationId(pathname) || this.props.isMobile < 576
                  })}
                >
                  <Suspense fallback={<MenuDefault />}>
                    <Switch>
                      {this.state.validRouteForUser.map((route, i) => {
                        return (
                          <Route
                            exact
                            path={route.path}
                            key={i}
                            render={props => (
                              <route.component
                                {...props}
                                chatToggle={this.chatToggle}
                              />
                            )}
                          />
                        );
                      })}
                      <Route
                        path="/app/access-denied"
                        component={AccessDenied}
                      />
                      <Route path="/app/*" component={DeniedComponent} />
                    </Switch>
                  </Suspense>
                </div>
              </main>
            </Hammer>
          </div>
        </div>
      </div>
    );
  }
}

function mapStateToProps(store) {
  return {
    sidebarOpened: store.navigation.sidebarOpened,
    sidebarStatic: store.navigation.sidebarStatic,
    usersList: store.usersList.toJS(),
    loggedUser: store.logged_user.userLogin

    // dashboardTheme: store.layout.dashboardTheme
  };
}

export default compose(isMobile)(withRouter(connect(mapStateToProps)(Layout)));
