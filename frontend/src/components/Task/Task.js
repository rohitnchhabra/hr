import React, { PureComponent } from "react";
import PropTypes from "prop-types";
import cx from "classnames";
import Checkbox from "../generic/input/Checkbox";
class Task extends PureComponent {
  static propTypes = {
    id: PropTypes.number.isRequired,
    index: PropTypes.number.isRequired,
    type: PropTypes.string.isRequired,
    title: PropTypes.string.isRequired,
    time: PropTypes.string.isRequired,
    status: PropTypes.bool.isRequired,
    isManageRoles: PropTypes.bool
  };

  render() {
    const {
      id,
      text,
      status,
      handleChangeSteps,
      index,
      isManageRoles,
      secure_level,
      className,
      disabled
    } = this.props;
    function onClick() {
      if (isManageRoles) {
        handleChangeSteps({ target: { checked: !status } });
      } else {
        handleChangeSteps(id);
      }
    }
    return (
      <div className={cx("d-flex align-items-center", { status })} key={index}>
        <Checkbox
          id={`checkbox${id}`}
          name={id}
          checked={status}
          onClick={onClick}
          disabled={disabled}
        />
        <div className="task-content pl-4">
          <h6
            className={className + (status ? " checkedTrue" : " checkedFalse")}
          >
            {" "}
            <span>{secure_level}</span>
            {text}
          </h6>
        </div>
      </div>
    );
  }
}

export default Task;
