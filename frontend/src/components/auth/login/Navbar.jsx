import React from "react";

const Navbar = () => {
  return (
    <div className="text-center pb-3">
      <img src="./favicon.ico" height="25" width="25" />
      <span>HR</span>
    </div>
  );
};

export default Navbar;
