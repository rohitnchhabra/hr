import React from "react";
import logo from "../../../static/new-logo.png";
import PropTypes from 'prop-types';
const GetLogo = ({className}) => {
  return (
    <h5 className={className} >
      <img src={logo} height="60" alt="logo" width="300"  />
    </h5>
  );
};

GetLogo.prototype={
  className:PropTypes.string
}
export default GetLogo;


