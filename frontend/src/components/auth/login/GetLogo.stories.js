import React from "react";
import { withKnobs} from "@storybook/addon-knobs";
import GetLogo from "./GetLogo";

export default {
  title: "logo",
  decorators: [withKnobs],
};

export const log = ()=> (
  <GetLogo/>
);
