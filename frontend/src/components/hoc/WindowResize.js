import React, { Component } from "react";
import debounce from "lodash/debounce";

/** Higher Order Component for checking whether a device is
 *  having window width less than or equal to 768px */
export default WrappedComponent => {
  class isMobile extends Component {
    constructor(props) {
      super(props);
      this.state = {
        width: window.innerWidth
      };
      this.debouncedHandleResize = debounce(this.handleResize, 500);
    }
    componentDidMount() {
      window.addEventListener("resize", this.debouncedHandleResize);
    }
    componentWillUnmount() {
      window.removeEventListener("resize", this.debouncedHandleResize);
    }
    handleResize = () => this.setState({ width: window.innerWidth });
    render() {
      const { width } = this.state;
      const isMobile = width
      return <WrappedComponent isMobile={isMobile} {...this.props} />;
    }
  }
  return isMobile;
};