import React from "react";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import { withRouter } from "react-router";
import * as avatar from "../../static/avatar.png";
import {
  Navbar,
  Nav,
  Dropdown,
  NavItem,
  NavLink,
  Badge,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
  UncontrolledTooltip
} from "reactstrap";
import $ from "jquery";

import {
  toggleSidebar,
  openSidebar,
  closeSidebar,
  changeActiveSidebarItem
} from "../../redux/actions";
import isMobile from "../../components/hoc/WindowResize";
import { compose } from "redux";

import s from "./Header.module.scss";
import { getLocationId } from "../../helper/helper";
import LoadingIcon from "../generic/LoadingIcon";
// import classNames from 'classnames'

class Header extends React.Component {
  static propTypes = {
    sidebarOpened: PropTypes.bool.isRequired,
    sidebarStatic: PropTypes.bool.isRequired,
    chatToggle: PropTypes.func.isRequired,
    dispatch: PropTypes.func.isRequired,
    location: PropTypes.shape({
      pathname: PropTypes.string
    }).isRequired
  };

  constructor(props) {
    super(props);
    this.state = {
      menuOpen: false,
      notificationsOpen: false,
      notificationsTabSelected: 1
    };
  }
  componentDidMount() {
    if (window.innerWidth > 576) {
      setTimeout(() => {
        const $chatNotification = $("#chat-notification");
        $chatNotification
          .removeClass("hide")
          .addClass("animated fadeIn")
          .one(
            "webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend",
            () => {
              $chatNotification.removeClass("animated fadeIn");
              setTimeout(() => {
                $chatNotification
                  .addClass("animated fadeOut")
                  .one(
                    "webkitAnimationEnd mozAnimationEnd MSAnimationEnd" +
                      " oanimationend animationend",
                    () => {
                      $chatNotification.addClass("hide");
                    }
                  );
              }, 6000);
            }
          );
        $chatNotification
          .siblings("#toggle-chat")
          .append('<i class="chat-notification-sing animated bounceIn"></i>');
      }, 4000);
    }

    $("#search-input").on("blur focus", e => {
      $("#search-input")
        .parents(".input-group")
        [e.type === "focus" ? "addClass" : "removeClass"]("focus");
    });
  }

  toggleNotifications = () => {
    this.setState({
      notificationsOpen: !this.state.notificationsOpen
    });
  };

  // collapse/uncolappse
  switchSidebar = () => {    
    if (this.props.sidebarOpened) {
      this.props.dispatch(closeSidebar());
      this.props.dispatch(changeActiveSidebarItem(null));
    } else {
      const paths = this.props.location.pathname.split("/");
      paths.pop();
      this.props.dispatch(openSidebar());
      this.props.dispatch(changeActiveSidebarItem(paths.join("/")));
    }
  };

  // static/non-static
  toggleSidebar = () => {
    this.props.dispatch(toggleSidebar());
    if (this.props.sidebarStatic) {
      localStorage.setItem("staticSidebar", "false");
      this.props.dispatch(changeActiveSidebarItem(null));
    } else {
      localStorage.setItem("staticSidebar", "true");
      const paths = this.props.location.pathname.split("/");
      paths.pop();
      this.props.dispatch(changeActiveSidebarItem(paths.join("/")));
    }
  };

  toggleMenu = () => {
    this.setState({
      menuOpen: !this.state.menuOpen
    });
  };

  render() {
    const {
      logged_user: { data },
      frontend: { show_loading }
    } = this.props;
    const {
      location: { pathname }
    } = this.props;
    return (
      <>
        <Navbar className={`${s.root} d-print-none generic-sticky-header`}>
          <Nav>
            <NavItem>
              <NavLink
                className="d-md-down-none"
                id="toggleSidebar"
                onClick={this.toggleSidebar}
              >
                <i className="la la-bars" />
              </NavLink>
              <UncontrolledTooltip placement="bottom" target="toggleSidebar">
                Turn on/off
                <br />
                sidebar
                <br />
                collapsing
              </UncontrolledTooltip>
              <NavLink className="fs-lg d-lg-none" onClick={this.switchSidebar}>
                <span
                  className="rounded rounded-lg bg-gray text-white d-md-none"
                  id="bar-style"
                >
                  <i className="la la-bars pt-2" />
                </span>
                <i className="la la-bars d-sm-down-none" />
              </NavLink>
            </NavItem>
          </Nav>
          {this.props.heading && this.props.heading.heading && <Nav><NavItem className="font-weight bold align-self-center"><h5 className="mb-0 heading">{this.props.heading.heading}</h5></NavItem></Nav>}
          <Nav className="ml-auto">
            <NavItem>
              <LoadingIcon loading={Boolean(show_loading)} />
            </NavItem>
            <Dropdown
              nav
              isOpen={this.state.menuOpen}
              toggle={this.toggleMenu}
              className="d-sm-down-none"
              id="user-action"
            >
              <DropdownToggle nav>
              <NavItem>
              <NavLink>
                <span className={`${s.avatar} thumb-sm float-left`}>
                  <img
                    className="rounded-circle"
                    src={data && data.profileImage ? data.profileImage : avatar}
                    alt="..."
                  />
                </span>
              </NavLink>
            </NavItem>
              </DropdownToggle>
              <DropdownMenu right className="super-colors">
                {/* <DropdownItem
                  onClick={() => {
                    this.props.history.push("my_profile");
                  }}
                >
                  <i className="la la-user" /> My Profie
                </DropdownItem> */}
                {/* <DropdownItem divider /> */}
                <DropdownItem
                  onClick={() => {
                    this.props.history.push("/app/logout");
                  }}
                >
                  <i className="la la-sign-out" /> Log Out
                </DropdownItem>
              </DropdownMenu>
            </Dropdown>
            { this.props.isMobile < 576 && data && data.role !== "Employee" && getLocationId(pathname) && (
              <NavItem>
                <NavLink
                  className="d-sm-down-none"
                  id="toggle-chat"
                  onClick={() => {
                    this.props.chatToggle();
                  }}
                >
                    <i className="fa fa-users fa-beat" />
                </NavLink>
              </NavItem>
            )}
            {this.props.isMobile < 576 && data && data.role !== "Employee" && getLocationId(pathname) && (
              <NavItem className="fs-lg d-md-none">
                <NavLink
                  href="#"
                  onClick={() => {
                    this.props.chatToggle();
                  }}
                >
                  <span>
                    <i className="fa fa-users fa-beat" />
                  </span>
                </NavLink>
              </NavItem>
            )}
          </Nav>
        </Navbar>
      </>
    );
  }
}

function mapStateToProps(store) {
  return {
    sidebarOpened: store.navigation.sidebarOpened,
    sidebarStatic: store.navigation.sidebarStatic,
    logged_user: store.logged_user.userLogin,
    frontend: store.frontend.toJS(),
    heading:store.heading
  };
}

export default compose(isMobile)(withRouter(connect(mapStateToProps)(Header)));
