import React from "react";
import { Row, Col } from "reactstrap";
import Widget from "../../components/Widget";
import * as a5 from "../../static/avatar.png";
import * as actionsMyProfile from "../../redux/myProfile/actions/myProfile";
import { connect } from "react-redux";
import { withRouter } from "react-router";

import s from "./Profile.module.scss";

class Profile extends React.Component {
  constructor(props) {
    super(props);
    this.props.onMyProfileDetails();
  }
  render() {
    const { myProfile } = this.props;
    const {
      name,
      jobtitle,
      dateofjoining,
      gender,
      dob,
      work_email,
      mobile_ph,
      blood_group,
      emergency_ph1,
      emergency_ph2,
      current_address,
      marital_status,
      team,
      image,
      medical_condition
    } = myProfile.user_profile_detail;
    return (
      <div className={s.root}>
        <Row>
          <Col
            lg={12}
            xs={12}
            style={{ paddingLeft: "20px", paddingRight: "20px" }}
          >
            <Row>
              <Col
                lg={12}
                xs={12}
                style={{ paddingLeft: "20px", paddingRight: "20px" }}
              >
                <Widget bodyClass="mt-0">
                  <div className="widget-top-overflow widget-padding-md clearfix bg-warning text-white">
                    <h3 className="mt-lg mb-lg">
                      <span className="fw-semi-bold">{name}&nbsp;</span>
                      {jobtitle}
                    </h3>
                    <h6 className="mt-lg mb-lg">
                      <span className="fw-semi-bold">
                        Date Of Joining:&nbsp;
                      </span>
                      {dateofjoining}
                    </h6>
                    <ul className="tags text-white pull-right">
                      {/* eslint-disable-next-line */}
                      <li>
                        <a 
                          onClick={() => {
                            this.props.history.push("edit_profile");
                          }}
                        >
                          Edit Profile 
                        </a>
                      </li>
                    </ul>
                  </div>
                  <div
                    className="post-user mt-negative-lg"
                    style={{ position: "relative" }}
                  >
                    <span className="thumb-lg pull-left mr mt-n-sm">
                      <img
                        className="rounded-circle"
                        src={image ? image : a5}
                        alt="..."
                      />
                    </span>
                    <h6 className="m-b-1 fw-normal text-white">
                      Work Email: &nbsp;
                      {work_email}
                    </h6>
                    <p className="fs-mini text-muted">
                      <i className="fa fa-map-marker" /> &nbsp;{current_address}
                    </p>
                  </div>
                </Widget>
              </Col>
            </Row>
            <Row>
              <Col lg={12} xs={12}>
                <Col lg={4} md={12} xs={12}>
                  <Widget className="">
                    <div className="clearfix">
                      <Row className="flex-nowrap">
                        <Col xs="12 text-center">
                          <h6 className="m-0 text-muted">Date Of Birth</h6>
                          <p className="h2 m-0 fw-normal">{dob}</p>
                        </Col>
                      </Row>
                      <Row className="flex-nowrap">
                        <Col xs={6}>
                          <h6 className="m-0 text-muted">Gender</h6>
                          <p className="value5">{gender}</p>
                        </Col>
                        <Col xs="6">
                          <h6 className="m-0 text-muted">Blood Group</h6>
                          <p className="value5">{blood_group}</p>
                        </Col>
                      </Row>
                    </div>
                  </Widget>
                </Col>
                <Col lg={4} md={12} xs={12}>
                  <Widget className="">
                    <div className="clearfix">
                      <Row className="flex-nowrap">
                        <Col xs="12 text-center">
                          <h6 className="m-0 text-muted">Marital Status</h6>
                          <p className="h2 m-0 fw-normal">
                            {marital_status ? marital_status : "Single"}
                          </p>
                        </Col>
                      </Row>
                      <Row className="flex-nowrap">
                        <Col xs="6">
                          <h6 className="m-0 text-muted">Medical Condition</h6>
                          <p className="value5">{medical_condition}</p>
                        </Col>
                        <Col xs={6}>
                          <h6 className="m-0 text-muted">Team</h6>
                          <p className="value5">{team}</p>
                        </Col>
                      </Row>
                    </div>
                  </Widget>
                </Col>
                <Col lg={4} md={12} xs={12}>
                  <Widget className="">
                    <div className="clearfix">
                      <Row className="flex-nowrap">
                        <Col xs="12 text-center">
                          <h6 className="m-0 text-muted">Contact No</h6>
                          <p className="h2 m-0 fw-normal">{mobile_ph}</p>
                        </Col>
                      </Row>
                      <Row className="flex-nowrap">
                        <Col xs="6 text-center">
                          <h6 className="m-0 text-muted">
                            Emergency Contact 1
                          </h6>
                          <p className="value5">{emergency_ph1}</p>
                        </Col>
                        <Col xs="6 text-center">
                          <h6 className="m-0 text-muted">
                            Emergency Contact 2
                          </h6>
                          <p className="value5">{emergency_ph2}</p>
                        </Col>
                      </Row>
                    </div>
                  </Widget>
                </Col>
              </Col>
            </Row>
          </Col>
        </Row>
      </div>
    );
  }
}
function mapStateToProps(state) {
  return {
    myProfile: state.myProfile.toJS(),
    salary: state.salary.toJS()
  };
}
const mapDispatchToProps = dispatch => {
  return {
    onMyProfileDetails: () => {
      return dispatch(actionsMyProfile.getMyProfileDetails());
    }
  };
};

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(Profile)
);
