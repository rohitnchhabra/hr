import React from "react";
import classnames from "classnames";
import PropTypes from "prop-types";
import Box from "../../components/generic/Box/Box";
import Avatar from "../../components/generic/Avatar/Avatar";
import * as a5 from '../../static/avatar.png';
const UserPicHeadTitle = ({
  profileImage,
  editTitleIcon,
  name,
  userName,
  userId,
  jobTitle,
  secondCol,
  ...props
}) => {
  return (
    <Box>
      <div
        className={classnames(
          "d-flex",
          "mx-0",
          "generic-pic-header",
          secondCol ? "justify-content-between" : null
        )}
      >
        <div className="d-flex">
          <div className="genric_User_name">
            <Avatar
              size='medium'
              isProfilePicLoading={props.isProfilePicLoading}
              profileImage={profileImage?profileImage:a5}
            />
          </div>
          <div className="genric_User-info px-2">
            <div className="text-capitalize user-header">
              <h5>{name}</h5>
              {/* <span>{editTitleIcon ? editTitleIcon() : null}</span> */}
            </div>
            <div className="d-flex">
              <b>{jobTitle}</b>
            </div>
            <div>
              {userId && (
                <div>
                  <small className="text-black">
                    <span className="text-secondary">Emp ID:</span>{" "}
                    {userId ? userId : null}
                  </small>
                </div>
              )}
              {userName && (
                <div>
                  <small className="text-black">
                    <span className="text-secondary">Username:</span>{" "}
                    {userName ? userName : null}
                  </small>
                </div>
              )}
            </div>
          </div>
        </div>
        {secondCol ? secondCol() : null}
      </div>
    </Box>
  );
};

UserPicHeadTitle.defaultProps = {
  profileImage: a5,
};
UserPicHeadTitle.prototype = {
  profileImage: PropTypes.string,
  jobTitle: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  userName: PropTypes.string,
  userId: PropTypes.userId,
  secondCol: PropTypes.func,
};

export default UserPicHeadTitle;
