import React from "react";
import { withKnobs} from "@storybook/addon-knobs";
import UserPicHeadTitle from "./UserPicHeadTitle";

export default {
  title: "UserPicHeadTitle",
  decorators: [withKnobs]

};

export const GenericUserPicHeadTitle = () => (
  <UserPicHeadTitle
    name={"Name"}
    userName={"userName"}
    jobTitle={"Job Title"}
  />
);
