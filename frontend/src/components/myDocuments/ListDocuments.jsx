import React from "react";
import PropTypes from "prop-types";
import _ from "lodash";
import Heading from "../generic/Heading";

const ListDocuments = ({ myDocuments, deleteDocument, docs_list }) => {
  let myDocs = _.map(myDocuments, (doc, key) => {
    return (
      <li key={key} className="list-group-item">
        <div className="clear">
          <h5>
            {doc.document_type}
            <span
              className="glyphicon glyphicon-remove-circle pull-right pointer"
              onClick={() => {
                deleteDocument(doc.id);
              }}
            />
          </h5>
          {typeof doc.link_1 === "undefined" ? (
            ""
          ) : (
            <div
              className="col-xs-12"
              dangerouslySetInnerHTML={{ __html: doc.link_1 }}
            />
          )}
        </div>
      </li>
    );
  });
  if (_.isEmpty(myDocs)) {
    myDocs = (
      <li className="list-group-item text-center">
        <span>No document uploaded</span>
      </li>
    );
  }
  return (
    <div className="col-sm-4">
      <Heading subHeading title="Required" boldText="Documents" />
      {/* <h5>
        Required <span className="font-weight-bold">Documents</span>{" "}
      </h5> */}
      <ul className=" doc-list list-group m-b thumbnail p-3 ">
        {docs_list.map((v, i) => (
          <li className="d-flex" key={i}>
          <div className="my-documents-list-li">
              {myDocuments.find(
                document => document.document_type === v.value
              ) ? (
                <span className="list-number bg-success text-white mb-1 list-number-align">
                  <i className="tick_list glyphicon glyphicon-ok mt-2" />
                </span>
              ) : (
                <span className="list_number">{i + 1}</span>
              )}
                <small>{v.name}</small>
            </div>
          </li>
        ))}
      </ul>
    </div>
  );
};

ListDocuments.propTypes = {
  myDocuments: PropTypes.array.isRequired,
  deleteDocument: PropTypes.func.isRequired
};

export default ListDocuments;
