import React, { useState, useCallback, useLayoutEffect } from "react";
/**Returns a function for setting ref and height of the dom node for which ref is set
 * @params {number} clicked for setting height again if height of ref is changed
 */
export default function useDomNodeHeight(clicked) {
  const [height, setHeight] = useState(0);
  const [nodeRef, setNodeRef] = useState(null);
  const ref = useCallback(node => {
    if (node !== null) {
      setHeight(node.clientHeight);
      setNodeRef(node);
    }
  }, []);

  useLayoutEffect(() => {
    if (nodeRef) {
      setHeight(nodeRef.clientHeight);
    }
  }, [clicked]);

  return [ref, height];
}