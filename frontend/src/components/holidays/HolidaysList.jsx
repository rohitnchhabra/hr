import React from "react";
// import ButtonRaised from "../../components/generic/buttons/ButtonRaised";
import { confirm } from "../../services/notify";
// import InputText from "../../components/generic/input/InputText";
import GenericTable from "../generic/table/Table";
import { Alert, Button, Spinner } from "reactstrap";
import "../../styles/assets/holidays.scss";
import "date-fns";

const holidaysList = (listItem, onChange, rowStyle, index, extraProps) => {
  let holiday_type = listItem.type == "1" ? "isRestricted" : "normal";
  return (
    <>
      <td className={holiday_type + " v-align"}>{listItem.name}</td>
      <td className={holiday_type + " v-align"}>{listItem.month}</td>
      <td className={holiday_type + " v-align"}>{listItem.date}</td>
      <td className={holiday_type + " v-align"}>{listItem.dayOfWeek}</td>
      <td className={holiday_type + " v-align"}>{listItem.type_text}</td>
      {extraProps.showDelete ? (
        <td className={holiday_type}>
          <Button
            color="danger"
            size="sm"
            className="delete-holiday"
            id={listItem.id}
            onClick={e => {
              confirm(
                "Are you sure ?",
                "Do you want to delete this holiday ?",
                "warning"
              ).then(res => {
                if (res) {
                  extraProps.deleteHoliday(index, listItem.id);
                }
              });
            }}
            aria-hidden="true"
          >
            {extraProps.deleteHolidayIsLoading &&
            extraProps.deleteHolidayIndex == index ? (
              <Spinner color="light" size="sm" />
            ) : (
              "Delete"
            )}
          </Button>
        </td>
      ) : null}
    </>
  );
};

class HolidaysList extends React.Component {
  render() {
    let year = this.props.state.yearSelected;
    return (
      <div>
        <div className="holidayListData">
          <GenericTable
            list={holidaysList}
            headerData={[
              "HOLIDAY " + year,
              "MONTH",
              "DATE",
              "DAY",
              "TYPE",
              this.props.showDelete && "ACTION"
            ]}
            tableListData={this.props.holidays}
            deleteHoliday={this.props.deleteHoliday}
            doSort={
              this.props.holidays &&
              this.props.holidays.sort(function compare(a, b) {
                var dateA = new Date(a.date);
                var dateB = new Date(b.date);
                return dateA - dateB;
              })
            }
            showDelete={this.props.showDelete}
            isLoading={this.props.isLoading}
            deleteHolidayIsLoading={this.props.deleteHolidayIsLoading}
            deleteHolidayIndex={this.props.deleteHolidayIndex}
            className="holiday-table"
            widget_mobile={"holiday_widget"}
          />
        </div>
      </div>
    );
  }
}

export default HolidaysList;
