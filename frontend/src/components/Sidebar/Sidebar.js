import React from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import s from "./Sidebar.module.scss";
import LinksGroup from "./LinksGroup/LinksGroup";
import { DATA } from "../../menuObject";
import groupBy from "lodash/groupBy";
import icon from "../../static/exce_favicon.png";
import logo from "../../static/excellence.png";
import map from "lodash/map";

import {
  openSidebar,
  closeSidebar,
  changeActiveSidebarItem
} from "../../redux/actions";
import isScreen from "../../core/screenHelper";

class Sidebar extends React.Component {
  static propTypes = {
    sidebarStatic: PropTypes.bool,
    sidebarOpened: PropTypes.bool,
    dispatch: PropTypes.func.isRequired,
    activeItem: PropTypes.string,
    location: PropTypes.shape({
      pathname: PropTypes.string
    }).isRequired
  };

  static defaultProps = {
    sidebarStatic: false,
    sidebarOpened: false,
    activeItem: ""
  };

  onMouseEnter = () => {
    if (!this.props.sidebarStatic && (isScreen("lg") || isScreen("xl"))) {
      const paths = this.props.location.pathname.split("/");
      paths.pop();
      this.props.dispatch(openSidebar());
      this.props.dispatch(changeActiveSidebarItem(paths.join("/")));
    }
  };

  onMouseLeave = () => {
    if (!this.props.sidebarStatic && (isScreen("lg") || isScreen("xl"))) {
      this.props.dispatch(closeSidebar());
      this.props.dispatch(changeActiveSidebarItem(null));
    }
  };
  render() {
    const { role_pages: rolePages, role } = this.props.loggedUser.data;
    let links1 = "",
      links2 = "";
    if (rolePages) {
      let groupPagesByRole = groupBy(
        rolePages,
        value => value.roles && value.roles.includes("employee")
      );
      let groupIndex = groupBy(DATA, value => value.plabel);
        
      links1 = map(groupIndex, (subChildren, parentLabel) => (
        <LinksGroup
          key={parentLabel}
          onActiveSidebarItemChange={activeItem =>
            this.props.dispatch(changeActiveSidebarItem(activeItem))
          }
          activeItem={this.props.activeItem}
          header={parentLabel}
          isHeader
          iconName="flaticon-home"
          index={parentLabel}
          childrenLinks={subChildren}
          rolePages={groupPagesByRole["false"]}
          closeSidebar={closeSidebar}
        />
      ));
      links2 = map(groupIndex, (subChildren, parentLabel) => (
        <LinksGroup
          key={parentLabel}
          onActiveSidebarItemChange={activeItem =>
            this.props.dispatch(changeActiveSidebarItem(activeItem))
          }
          activeItem={this.props.activeItem}
          header={parentLabel}
          isHeader
          iconName="flaticon-home"
          index={parentLabel}
          childrenLinks={subChildren}
          rolePages={groupPagesByRole["true"]}
          closeSidebar={closeSidebar}
        />
      ));
        }
    return (
      <nav
        onMouseEnter={this.onMouseEnter}
        onMouseLeave={this.onMouseLeave}
        className={[
          s.root,
          this.props.sidebarStatic ? s.staticSidebar : "",
          !this.props.sidebarOpened ? s.sidebarClose : ""
        ].join(" ")}
        style={this.props.extraStyle || {}}
      >
        <header className={s.logo}>
          {this.props.sidebarOpened && (
            <a>
              <img onClick={()=>this.props.history.push('/app/overview')} src={logo} className="logo_openbar" />
            </a>
          )}

          {!this.props.sidebarOpened &&
          (
            <a>
              <img src={icon} className="icon_closebar" />
            </a>
          )}
        </header>
        {role !== "Employee" && rolePages.length > 4 && (
          <>
            <h5 className={[s.navTitle, s.groupTitle].join(" ")}>
              {role.toUpperCase()}
            </h5>
            <ul className={s.nav + " pt-0"}>{links1}</ul>
            <h5 className={[s.navTitle, s.groupTitle].join(" ")}>EMPLOYEE</h5>
          </>
        )}
        <ul>{links2}</ul>
      </nav>
    );
  }
}

const mapStateToProps = store => ({
  sidebarOpened: store.navigation.sidebarOpened,
  sidebarStatic: store.navigation.sidebarStatic,
  // alertsList: store.alerts.alertsList,
  activeItem: store.navigation.activeItem,
  loggedUser: store.logged_user.userLogin
});

export default withRouter(connect(mapStateToProps)(Sidebar));
