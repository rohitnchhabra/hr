import React, { Component } from "react";
import PropTypes from "prop-types";
import { NavLink, withRouter } from "react-router-dom";
import { Collapse} from "reactstrap";
import classnames from "classnames";
import find from "lodash/find";
import filter from "lodash/filter";
import s from "./LinksGroup.module.scss";
import { connect } from "react-redux";

const NavigationLink = ({ data, isHeader, pathname ,closeSidebar,dispatch}) => {
 const handleClick=()=>{
  if( window.screen.width<600){
    dispatch(closeSidebar())
  }

  }
  return <li className={classnames("link-wrapper", { [s.headerLink]: isHeader })}>
    {isHeader ? (
      <a
        className={classnames(
          // s.headerLink,
          {
            [s.headerLinkActive]: pathname === `/app${data.path}`
          }
        )}
        style={{
          paddingLeft: isHeader ? "50px" : "0px"
        }}
      >
        <span className={classnames("icon", s.icon)}>
          <i
            className={classnames(data.iconclasses)}
            style={{ lineHeight: "inherit" }}
          />
        </span>
        <NavLink to={`/app${data.path}`} onClick={handleClick}>{data.label}</NavLink>
      </a>
    ) : (
      <NavLink to={`/app${data.path}`} onClick={handleClick}>{data.label}</NavLink>
    )}
  </li>
}

class LinksGroup extends Component {
  /* eslint-disable */
  static propTypes = {
    header: PropTypes.node.isRequired,
    link: PropTypes.string.isRequired,
    childrenLinks: PropTypes.array,
    iconName: PropTypes.string,
    className: PropTypes.string,
    badge: PropTypes.string,
    label: PropTypes.string,
    activeItem: PropTypes.string,
    isHeader: PropTypes.bool,
    index: PropTypes.string,
    onActiveSidebarItemChange: PropTypes.func
  };
  /* eslint-enable */

  static defaultProps = {
    link: "",
    childrenLinks: null,
    className: "",
    isHeader: false,
    activeItem: "",
    label: ""
  };

  constructor(props) {
    super(props);
    this.state = {
      headerLinkWasClicked: true
    };
  }

  togglePanelCollapse = link => {
    this.props.onActiveSidebarItemChange(link);
    this.setState({
      headerLinkWasClicked:
        !this.state.headerLinkWasClicked ||
        (this.props.activeItem &&
          !this.props.activeItem.includes(this.props.index))
    });
  };

  render() {    
    const isOpen =
      this.props.activeItem &&
      this.props.activeItem.includes(this.props.index) &&
      this.state.headerLinkWasClicked;
    const {
      childrenLinks,
      header,
      rolePages,
      location: { pathname }
    } = this.props;

    let childLinks = filter(childrenLinks, subChild => {
      let validPath = subChild.path.replace(/\//g, "");
      
      return find(rolePages, ["page_name", validPath]);
    });

    if (!childLinks.length) {
      return null;
    }
    if (childLinks.length === 1) {
      return (
        <NavigationLink
          data={childLinks[0]}
          isHeader={true}
          pathname={pathname}
          closeSidebar={this.props.closeSidebar}
          dispatch={this.props.dispatch}

        />
      );
    }
    if (header === "undefined") {
      return (
        <>
          {childLinks.map((child, ind) => (
            <NavigationLink
              key={ind}
              data={child}
              isHeader={true}
              pathname={pathname}
              closeSidebar={this.props.closeSidebar}
              dispatch={this.props.dispatch}

            />
          ))}
        </>
      );
    }
    let icon = "";
    if (this.props.header === "Attendance") {
      icon = "glyphicon glyphicon-barcode";
    } else if (this.props.header === "Employees") {
      icon = "glyphicon glyphicon-user";
    } else if (this.props.header === "Leaves") {
      icon = "glyphicon glyphicon-calendar";
    } else if (this.props.header === "Manage Hours") {
      icon = "glyphicon glyphicon-time";
    } else if (this.props.header === "Payroll") {
      icon = "glyphicon glyphicon-briefcase";
    } else if (this.props.header === "Templates") {
      icon = "glyphicon glyphicon-envelope";
    } else if (this.props.header === "Inventory") {
      icon = "glyphicon glyphicon-th";
    } else if (this.props.header === "Settings") {
      icon = "glyphicon glyphicon-cog";
    }
    return (
      <li
        className={classnames(
          "link-wrapper",
          { [s.headerLink]: this.props.isHeader },
          this.props.className
        )}
      >
        <a
          className={classnames(
            {
              [s.headerLinkActive]: childrenLinks.find(
                (v, i) => v.path === pathname.replace("/app", "")
              )
            },
            { [s.collapsed]: isOpen },
            "d-flex"
          )}
          style={{
            paddingLeft: "50px"
          }}
          onClick={() => this.togglePanelCollapse(this.props.header)}
        >
          {this.props.isHeader ? (
            <span className={classnames("icon", s.icon)}>
              <i
                className={classnames(icon)}
                style={{ lineHeight: "inherit" }}
              />
            </span>
          ) : null}
          {this.props.header}
          <b className={["fa fa-angle-left", s.caret].join(" ")} />
        </a>
        <Collapse className={s.panel} isOpen={isOpen}>
          <ul>
            {childLinks.map((child, ind) => (
              <NavigationLink key={ind} data={child}
              dispatch={this.props.dispatch}
              closeSidebar={this.props.closeSidebar}
              />
            ))}
          </ul>
        </Collapse>
      </li>
    );
  }
}


export default withRouter(connect(null,null)(LinksGroup));
