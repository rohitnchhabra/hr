import React from "react";
import Task from "../Task/Task";

const TasksContainer = ({
  data: tasks,
  stage,
  handleChangeSteps,
  stageId,
  collapseId,
  collapseHandler
}) => {
  const totalCompleted = tasks.filter(i => i.status).length;
  return (
    <div className="card-design-for-list">
      <div>
        <div className="d-flex card-list-heading" onClick={() => collapseHandler(stageId)}>
          <h6 >{stage}</h6>
          <span
            className="material-icons expand-icon pull-right"
            data-toggle="collapse"
            data-target={"#empLifeCycle" + stageId}
          >
            expand_more
          </span>
        </div>
        <p className="text-muted mb-0 pt-0 pb-2">
          <small>
            {totalCompleted} of {tasks.length} completed
          </small>
        </p>
      </div>
      {
        <div
          className={
            stageId == collapseId
              ? "empLifeCycle" + " " + stage
              : "empLifeCycle"
          }
          id="empLifeCycle"
        >
          {tasks.map((item, index) => (
            <Task
              key={index}
              index={index}
              {...item}
              handleChangeSteps={handleChangeSteps}
            />
          ))}
        </div>
      }
    </div>
  );
};

export default TasksContainer;
