import React from "react";
import _ from "lodash";
import PropTypes from "prop-types";
import moment from "moment";
import GenericTable from "../../../components/generic/table/Table";

const UserHistoryHolding = ({ data }) => {
  const list = (d, onChange, rowStyle, key, extraProps,month) => {
    return (
      <>
          <td>
              {d.holding_amt}
          </td>
          <td>
            {moment(d.holding_start_date).format("DD-MMMM-YYYY")}
          </td>
          <td>
          {moment(d.holding_end_date).format("DD-MMMM-YYYY")} (
            {d.holding_month} {d.holding_month < 2 ? "month" : "months"})
          </td>
          <td>{d.reason}</td>
          <td>{moment(d.last_updated_on).format("DD-MMMM-YYYY")}</td>
          </>

    )}

  return (
    <div>
      <GenericTable
              tableListData={data }
              headerData={["Holding Amount","Start","End","Reason","Updated on"]}
              widget_mobile ={"laptop-padding"}
              list={list}
              className={"payslip-table"}
            />
      {/* <table className="table table-striped">
        <thead>
          <tr className="striped_salary_view">
            <td>Holding Amount</td>
            <td>Start</td>
            <td>End</td>
            <td>Reason</td>
            <td>Updated on</td>
          </tr>
        </thead>
        <tbody>{holdingHistory}</tbody>
      </table> */}
    </div>
  );
};

UserHistoryHolding.propTypes = {
  data: PropTypes.array.isRequired
};

export default UserHistoryHolding;
