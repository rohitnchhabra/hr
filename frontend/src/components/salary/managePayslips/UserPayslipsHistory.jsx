import React from "react";
import GenericTable from "../../../components/generic/table/Table";
import PropTypes from "prop-types";
import { dateFormatter } from "../../../helper/helper";

const UserPayslipsHistory = ({ user_payslip_history }) => {
  const list = (d) => {
      return (
        <>
            <td>
                {dateFormatter().fullMonths[d.month-1] +","+ d.year}
            </td>
            <td>Rs. 
              {d.total_net_salary}
            </td>
            <td>
            <i class="fa fa-file-pdf-o file_pdf_view  px-2"></i>
              <a href={`${d.payslip_url}`} target="_BLANK">
                Payslip {dateFormatter().months[d.month-1]+"," + d.year}
              </a>
            </td>
            </>

      )}
      
  return (

    <div>
      <div id="content" class="app-content box-shadow-z-0">

        <div>
          <div className="bg-white p-2 responsive_salary_detail ">
          
          <h6>PREVIOUS <strong>PAYSLIPS</strong></h6>
        
        <GenericTable
              tableListData={user_payslip_history }
              headerData={["Date","Amount","Document"]}
              widget_mobile ={"laptop-padding paysliptable_"}
              list={list}
              className={"payslip-table"}
            />
        {/* {previousPayslipsHistoryHtml} */}
        </div></div>
      </div>
    </div>
  );
};

UserPayslipsHistory.propTypes = {
  user_payslip_history: PropTypes.array.isRequired
};

export default UserPayslipsHistory;
