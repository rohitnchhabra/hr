import React from "react";
import PropTypes from "prop-types";
import { withRouter } from "react-router-dom";
import {
  ListGroup,
  ListGroupItem,
  InputGroup,
  InputGroupAddon,
  Input,
  InputGroupText
} from "reactstrap";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import * as avatar from "../../static/avatar.png";
import * as actions from "../../redux/actions";
import * as actionsManageUsers from "../../redux/manageUsers/actions/manageUsers";
import * as actionsManagePayslips from "../../redux/salary/actions/managePayslips";
import * as actionsManageUserWorkingHours from "../../redux/workingHours/actions/manageUserWorkingHours";
import * as actionsManageRoles from "../../redux/manageRoles/actions/manageRoles";
import * as userInventoryDetail from "../../redux/manageUsers/actions/manageUsers";
import classnames from "classnames";
import Checkbox from "../generic/input/Checkbox";
import s from "./UserList.module.scss";
import {setCurrentUserRequest} from "../../redux/actions";

class DesktopUserList extends React.Component {
  static propTypes = {
    chatOpen: PropTypes.bool
  };

  static defaultProps = {
    chatOpen: false,
    chatToggle: () => {}
  };

  constructor(props) {
    super(props);
    const { usersList, userSelected } = this.props;
    this.userListRef = React.createRef();
    this.state = {
      chatMessageOpened: true,
      conversation: Object,
      searchValue: "",
      userList: usersList,
      year: "",
      month: "",
      selectedUser:''
    };
  }

  componentWillMount() {
    document.addEventListener("click", this.handleClicksInside, false);
  }

  componentWillUnmount() {
    document.removeEventListener("click", this.handleClicksInside, false);
  }

  componentDidUpdate(props){
    const {
      location: { pathname }
    } = this.props
      if(pathname.split("/")[2] !== props.location.pathname.split("/")[2] ){
          this.setState({selectedUser:''})
      }
  }

  handleClicksInside = e => {
    if (
      this.userListRef.current.contains(e.target) ||
      e.target.classList.contains("fa-users") ||
      e.target.classList.contains("add-btn-zxc")
    ) {
      return;
    } else {
      this.props.chatToggle(false);
    }
  };

  componentDidMount() {
    let d = new Date();
    let year = d.getFullYear();
    let month = d.getMonth() + 1; // +1 since getMonth starts from 0
    this.setState({ year: year, month: month });
  }

  handleChangeContacts = event => {
    this.setState({ searchValue: event.target.value });
  };

  filterUser = item => {
    const isFindName =
      item.name &&
      item.name.toLowerCase().indexOf(this.state.searchValue.toLowerCase()) !==
        -1;
    return isFindName;
  };

  onListClick = (path, userid, username, currentUser) => {
    const { teamStats} = this.props
     this.setState({
      searchValue:"", selectedUser:userid
    })
    this.props.selectedUser({userid});
    if (path === "/app/home") {
      this.props.requestUserAttendance({
        userid,
        year: this.state.year,
        month: this.state.month
      });
      this.props.chatToggle();
    } else if (path.includes("manage_users")) {
      this.props.history.push(`/app/manage_users/${userid}`)
      this.props.onUserProfileDetails(userid, username);
      this.props.onGetUserDocument(userid);
      this.props.onUserManagePayslipsData(userid);
      this.props.onGetStages(userid);
      this.props.chatToggle();
    } else if (path.includes("manage_user_working_hours")) {
      this.props.onUserWorkingHoursData(userid);
      this.props.chatToggle();
    } else if (path.includes("manage_salary")) {
      this.props.history.push("manage_salary?selectedUser=" + userid);
      this.props.chatToggle();
    } else if (path.includes("apply_emp_leave")) {
      this.props.history.push("apply_emp_leave?selectedUser=" + userid);
      this.props.chatToggle();
    } else if (path.includes("inventoryOverviewDetail")) {
      this.props.onUserProfileDetails(userid, username);
      this.props.chatToggle();
    } else if (path.includes("/manage_roles")) {
      const {
        manageRoles: { collapseSelected, rolesData }
      } = this.props;
      const roleDetails =
        rolesData &&
        rolesData.roles.find(role => {
          return collapseSelected === role.name;
        });
      this.props.onUpdateUserRole({ userId: userid, roleId: roleDetails.id });
      this.props.chatToggle();
    } else if (path.includes("manage_payslips")) {
      this.props.onUserClick(userid);
    }else if(path.includes("disabled_employees")){
      let payload = { currentUser,"type":!teamStats.currentDisableUser.type}
      this.props.onSetCurrentUserRequest(payload)
      this.props.onGetUserDocument(userid);
      this.props.onUserManagePayslipsData(userid);
    } else if(path.includes("all_user_timesheet")) {
      // this.props.timeSheetListRequest(userid);
      // this.props.userTimeSheetRequest(userid);
    }
     else {
      return null;
    }
  };
  handleMouseOver = () => {
    this.setState(
      {
        listOpen: true
      },
      () => {
        this.props.sendMouseHoverDataParent(true);
      }
    );
  };

  handleMouseOut = () => {
    this.props.sendMouseHoverDataParent(false);
    this.setState({
      listOpen: false
    });
  };

  render() {
    const { selectAll } = this.props;
    return (
      <aside
        className={classnames(
          s.desktoproot,
          "dashboard-light bg-transparent position-sticky pr",
          this.state.listOpen ? s.desktopListOpen : s.desktopListClose
        )}
        ref={this.userListRef}
        onMouseEnter={this.handleMouseOver}
        onMouseLeave={this.handleMouseOut}
      >
        <header className={classnames(s.chatHeader)+"pt-0"}>
          {/* <h4 className={classnames(s.chatTitle, "m-1 mx-0")}>
            {this.state.listOpen ? "Employees" : ""}
          </h4> */}
          <div className="input-group input-group-transparent">
            {this.state.listOpen ? (
              <InputGroup size="sm">
                <Input
                  placeholder="Search..."
                  value={this.state.searchValue}
                  onChange={this.handleChangeContacts}
                />
                <InputGroupAddon addonType="append">
                  <InputGroupText>
                    <i className="fa fa-search" />
                  </InputGroupText>
                </InputGroupAddon>
              </InputGroup>
            ) : 
            <div className="search-employee-icon">
            <i className="fa fa-search" /></div>
          }
          </div>
        </header>
        <div
          className={[
            s.chatPanel,
            s.chatContacts,
            `${this.state.listOpen ? "open-list-name" :"close-list-name"}`,
            this.state.chatMessageOpened ? s.chatMessageOpen  : ""
          ].join(" ")}
        >
          {/* <h5 className={s.navTitle}>TODAY</h5> */}

          <ListGroup
            id="chat-sidebar-user-group"
            className={s.chatSidebarUserGroup}
          >
            {selectAll && (
              <div className="px-2 list-group-item">
                <Checkbox
                  name={selectAll.name}
                  onClick={selectAll.onClick}
                  checked={selectAll.checked}
                />
                <label>All</label>
              </div>
            )}
            {this.state.userList &&
              typeof this.state.userList === "object" &&
              this.state.userList.filter(this.filterUser).map((item, i) => (
                <ListGroupItem
                  key={i}
                  onClick={e => {
                    this.onListClick(
                      this.props.location.pathname,
                      item.user_Id,
                      item.username,
                      item
                    );
                  }}
                  className={classnames("px-2", {
                    active: this.state.selectedUser === item.user_Id
                  })}
                >
                  <div className="d-inline-flex w-100 align-items-center">
                    {this.props.selectUsers && (
                      <Checkbox
                        name={item.user_Id}
                        onClick={this.props.onCheckboxClick}
                        checked={this.props.checked.includes(item.user_Id)}
                      />
                    )}{" "}
                    <div className={`${(this.state.selectedUser === item.user_Id ) || ((this.props.userPayId && this.props.userPayId == item.user_Id) ? (this.props.payslipMonth == new Date().getMonth()):'') ? "thumb-sm-img" : "thumb-sm"} float-left mr user-chatbar-img`}>
                      <img
                        className={classnames("border-success rounded-circle",this.props.payslipData &&
                         this.props.payslipData.all_users_latest_payslip && 
                        this.props.payslipData.all_users_latest_payslip.length >0  && 
                        this.props.payslipData.all_users_latest_payslip.find((user,i)=>
                          item.user_Id==user.user_Id
                        )?this.state.selectedUser === item.user_Id?"border-success":"highlight-user border-warning":null)}
                        src={item.profileImage || avatar}
                        alt="..."
                      />
                    </div>
                    <div className="userDetails">
                      <h6
                        className={classnames(
                          s.messageSender,
                          "text-nowrap overflow-hidden"
                        )}
                      >
                        {item.name}
                      </h6>
                      <p className={s.messagePreview}
                      >
                        {item.jobtitle}
                      </p>
                    </div>
                  </div>
                </ListGroupItem>
              ))}
          </ListGroup>
        </div>
      </aside>
    );
  }
}

function mapStateToProps(state) {
  return {
    frontend: state.frontend.toJS(),
    loggedUser: state.logged_user.userLogin,
    manageRoles: state.manageRoles.toJS(),
    userSelected: state.userListSidebar,
    teamStats:state.teamStats,
  };
}
const mapDispatchToProps = dispatch => {
  return {
    ...bindActionCreators(actions, dispatch),
    onUserProfileDetails: (userid, username) => {
      return dispatch(
        actionsManageUsers.getUserProfileDetails(userid, username)
      );
    },
    onSetCurrentUserRequest:payload => {
      return dispatch(setCurrentUserRequest(payload));
    },
    onGetUserDocument: userid => {
      return dispatch(actionsManageUsers.getUserDocument(userid));
    },
    onGetStages: id => {
      return dispatch(actionsManageUsers.getSteps(id));
    },
    onUserManagePayslipsData: userid => {
      return dispatch(
        actionsManagePayslips.get_user_manage_payslips_data(userid)
      );
    },
    onUserWorkingHoursData: userid => {
      return dispatch(
        actionsManageUserWorkingHours.get_managed_user_working_hours(userid)
      );
    },
    onRolesList: () => {
      return dispatch(actionsManageRoles.getRolesList());
    },
    onUpdateUserRole: userRoleUpdateDetails => {
      return dispatch(actionsManageRoles.updateUserRole(userRoleUpdateDetails));
    },
  };
};

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(DesktopUserList)
);
