import React from "react";
import PropTypes from "prop-types";
import { withRouter } from "react-router-dom";
import {
  ListGroup,
  ListGroupItem,
  InputGroup,
  InputGroupAddon,
  Input,
  InputGroupText
} from "reactstrap";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import * as avatar from "../../static/avatar.png";
import * as actions from "../../redux/actions";
import * as actionsManageUsers from "../../redux/manageUsers/actions/manageUsers";
import * as actionsManagePayslips from "../../redux/salary/actions/managePayslips";
import * as actionsManageUserWorkingHours from "../../redux/workingHours/actions/manageUserWorkingHours";
import * as actionsManageRoles from "../../redux/manageRoles/actions/manageRoles";
import * as userInventoryDetail from "../../redux/manageUsers/actions/manageUsers";
import isEqual from "lodash/isEqual";
import s from "./UserList.module.scss";
import {setCurrentUserRequest} from "../../redux/actions";

class UserList extends React.Component {
  static propTypes = {
    chatOpen: PropTypes.bool
  };

  static defaultProps = {
    chatOpen: false
  };

  constructor(props) {
    super(props);
    const {usersList, userSelected } = this.props;
    this.userListRef = React.createRef();
    this.state = {
      chatMessageOpened: true,
      conversation: Object,
      searchValue: "",
      year: "",
      month: "",
      selectedUser: ''
    };
  }

  componentWillMount() {
    document.addEventListener("click", this.handleClicksInside, false);
  }

  componentWillUnmount() {
    document.removeEventListener("click", this.handleClicksInside, false);
  }

  handleClicksInside = e => {
    if (
      this.userListRef.current.contains(e.target) ||
      e.target.classList.contains("fa-users") ||
      e.target.classList.contains("add-btn-zxc")
    ) {
      return;
    } else {
      this.props.chatToggle(false);
    }
  };

  
  componentDidUpdate(props){
    const {
      location: { pathname }
    } = this.props
      if(pathname.split("/")[2] !== props.location.pathname.split("/")[2] ){
          this.setState({selectedUser:''})
      }
    }

  componentDidMount() {
    let d = new Date();
    let year = d.getFullYear();
    let month = d.getMonth() + 1; // +1 since getMonth starts from 0
    this.setState({ year: year, month: month });
  }

  handleChangeContacts = event => {
    this.setState({ searchValue: event.target.value });
  };

  filterUser = item => {
    const isFindName =
      item.name &&
      item.name.toLowerCase().indexOf(this.state.searchValue.toLowerCase()) !==
        -1;
    return isFindName;
  };

  onListClick = (path, userid, username,currentUser) => {
    const { teamStats} = this.props
    this.setState({ selectedUser:userid})
    this.props.selectedUser(userid);
    if (path === "/app/home") {
      this.props.requestUserAttendance({
        userid,
        year: this.state.year,
        month: this.state.month
      });
      this.props.chatToggle();
    } else if (path.includes("manage_users")) {
      this.props.history.push(`/app/manage_users/${userid}`)
      this.props.onUserProfileDetails(userid, username);
      this.props.onGetUserDocument(userid);
      this.props.onUserManagePayslipsData(userid);
      this.props.onGetStages(userid);
      this.props.chatToggle();
    } else if (path.includes("manage_user_working_hours")) {
      this.props.onUserWorkingHoursData(userid);
      this.props.chatToggle();
    } else if (path.includes("manage_salary")) {
      this.props.history.push("manage_salary?selectedUser=" + userid);
      this.props.chatToggle();
    } else if (path.includes("apply_emp_leave")) {
      this.props.history.push("apply_emp_leave?selectedUser=" + userid);
      this.props.chatToggle();
    } else if (path.includes("inventoryOverviewDetail")) {
      this.props.onUserProfileDetails(userid, username);
      this.props.chatToggle();
    } else if (path.includes("/manage_roles")) {
      const {
        manageRoles: { collapseSelected, rolesData }
      } = this.props;
      const roleDetails =
        rolesData &&
        rolesData.roles.find(role => {
          return collapseSelected === role.name;
        });
      this.props.onUpdateUserRole({ userId: userid, roleId: roleDetails.id });
      this.props.chatToggle();
    }else if(path.includes("manage_payslips")){
      const {month,year} = teamStats.userPayslipMonthYear
      this.props.history.push(
        "manage_payslips?selectedUser=" + userid
      );
      this.props.setUserPayslipMonthYearRequest({
                    month,
                    year,
                    userid,
                    username,
                    "userImage":currentUser.slack_profile.image_192,
                    "jobTitle":currentUser.jobtitle
                  });
      if(month === '' && year === ''){
        this.props.onUserManagePayslipsData(userid);
      }else{
        this.props.onUserMonthlyManagePayslipsData(userid, year, month);
      }
      this.props.chatToggle();
    }
    else if(path.includes("disabled_employees")){
      let payload = { currentUser,"type":!teamStats.currentDisableUser.type}
      this.props.onSetCurrentUserRequest(payload)
      this.props.chatToggle();
      this.props.onGetUserDocument(userid);
      this.props.onUserManagePayslipsData(userid);
    } else if(path.includes("all_user_timesheet")) {
      this.props.timeSheetListRequest(userid);
    } else {
      return null;
    }
  };

  render() {
    return (
      <aside
        className={[s.root, this.props.chatOpen ? s.chatOpen : ""].join(" ")}
        ref={this.userListRef}
      >
        <header className={s.chatHeader}>
          <h4 className={s.chatTitle}>Employees</h4>
          <div className="input-group input-group-transparent">
            <InputGroup size="sm">
              <Input
                placeholder="Search..."
                value={this.state.searchValue}
                onChange={this.handleChangeContacts}
              />
              <InputGroupAddon addonType="append">
                <InputGroupText>
                  <i className="fa fa-search" />
                </InputGroupText>
              </InputGroupAddon>
            </InputGroup>
          </div>
        </header>
        <div
          className={[
            s.chatPanel,
            s.chatContacts,
            this.state.chatMessageOpened ? s.chatMessageOpen : ""
          ].join(" ")}
        >
          {/* <h5 className={s.navTitle}>TODAY</h5> */}
          <ListGroup
            id="chat-sidebar-user-group"
            className={s.chatSidebarUserGroup}
          >
            {this.props.usersList &&
              typeof this.props.usersList === "object" &&
              this.props.usersList.filter(this.filterUser).map((item, i) => (
                <ListGroupItem
                  key={i}
                  onClick={e => {
                    this.onListClick(
                      this.props.location.pathname,
                      item.user_Id,
                      item.username,
                      item
                    );
                  }}
                  className={
                    this.state.selectedUser === item.user_Id ? "active" : ""
                  }
                >
                  <div>
                    {this.state.selectedUser === item.user_Id ? (
                      <i
                        className={[
                          "fa fa-circle float-right",
                          `text-success`
                        ].join(" ")}
                      />
                    ) : null}
                    <span className="thumb-sm float-left mr">
                      {item.slack_profile && item.slack_profile.image_original ? (
                        <img
                          className="rounded-circle"
                          src={item.slack_profile.image_original}
                          alt="..."
                        />
                      ) : (
                        <img className="rounded-circle" src={avatar} alt="..." />
                      )}
                    </span>
                    <div className="userDetails">
                      <h6 className={s.messageSender}>{item.name}</h6>
                      <p className={s.messagePreview}>{item.jobtitle}</p>
                    </div>
                  </div>
                </ListGroupItem>
              ))}
          </ListGroup>
        </div>
      </aside>
    );
  }
}

function mapStateToProps(state) {
  return {
    frontend: state.frontend.toJS(),
    loggedUser: state.logged_user.userLogin,
    manageRoles: state.manageRoles.toJS(),
    userSelected: state.userListSidebar,
    teamStats:state.teamStats,
  };
}
const mapDispatchToProps = dispatch => {
  return {
    ...bindActionCreators(actions, dispatch),
    onUserProfileDetails: (userid, username) => {
      return dispatch(
        actionsManageUsers.getUserProfileDetails(userid, username)
      );
    },
    onSetCurrentUserRequest:payload => {
      return dispatch(setCurrentUserRequest(payload));
    },
    onGetUserDocument: userid => {
      return dispatch(actionsManageUsers.getUserDocument(userid));
    },
    onGetStages: id => {
      return dispatch(actionsManageUsers.getSteps(id));
    },
    onUserManagePayslipsData: userid => {
      return dispatch(
        actionsManagePayslips.get_user_manage_payslips_data(userid)
      );
    },
    onUserWorkingHoursData: userid => {
      return dispatch(
        actionsManageUserWorkingHours.get_managed_user_working_hours(userid)
      );
    },
    onRolesList: () => {
      return dispatch(actionsManageRoles.getRolesList());
    },
    onUpdateUserRole: userRoleUpdateDetails => {
      return dispatch(actionsManageRoles.updateUserRole(userRoleUpdateDetails));
    },
    onUserMonthlyManagePayslipsData: (userid, year, month) => {
      return dispatch(
        actionsManagePayslips.get_user_month_manage_payslips_data(
          userid,
          year,
          month
        )
      );
    },
    setUserPayslipMonthYearRequest:(payload) => {
      return dispatch(
        actions.setUserPayslipMonthYearRequest(
          payload
        )
      );
    }
  };
};

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(UserList)
);
