import React from 'react';
import PropTypes from 'prop-types';
import _ from 'lodash';
import {confirm} from '../../services/notify';
import Heading from "../generic/Heading";
import docImg from "../../static/doc-image.png";

const ListAllPolicyDocument = ({policyDocuments, submitNewListofDocs}) => {
  const deleteDocument = (delDoc) => {
    let newList = _.filter(policyDocuments, function (o) { return o.name !== delDoc.name; });
    confirm('Are you sure ?', 'Do you want to delete this Document ?', 'warning').then((res) => {
      if (res) {
        submitNewListofDocs(newList);
      }
    });
  };
  let allDocuments = policyDocuments.slice(0).reverse().map((doc, i) => {
    return (
      <div>
          <table className="table">
            <tbody>
              <tr className="list_preview_list_doc">
               <td className="text-secondary"  >
                <a className="ismobile_show_doc" href={doc.link} target="_blank" rel="noopener noreferrer">
                      <img className="show_doc_picture" src ={docImg} alt=""/>
                      <strong className="text-secondary">{doc.name}</strong>
                </a>
                <strong className="policy_document_title ">{doc.name}</strong><br/> 
                <a className="link_policy_doc" href={doc.link} target="_blank" rel="noopener noreferrer" >{doc.link}</a></td>

                <td><button type="button" tooltip="Delete Document" tooltipPosition="top-left" className="ismobile_button btn btn-danger pull-right" onClick={(evt) => {
                  evt.stopPropagation();
                 deleteDocument(doc);
                 }}>Delete</button></td>
              </tr>
            </tbody>
          </table>
        
      </div>
    );
  });
  return (
      <div>
        <Heading className={"doc_name_file font-weight-bold pb-2"} title={"NAMES"} />
        {allDocuments}
      </div>
  );
};

ListAllPolicyDocument.propTypes = {
  policyDocuments:     PropTypes.array.isRequired,
  submitNewListofDocs: PropTypes.func.isRequired
};

export default ListAllPolicyDocument;
