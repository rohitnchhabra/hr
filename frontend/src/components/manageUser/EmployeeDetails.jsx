import React, { useState } from "react";
import profilePlaceholder from "../../static/profilePlaceholder.png";

const EmployeeDetails = ({ allEmpolyesDetails, allDone }) => {
  const [count, setCount] = useState(10);

  let data = "";
  if (allEmpolyesDetails.length) {
    data = allEmpolyesDetails
      .slice(
        0,
        count <= allEmpolyesDetails.length ? count : allEmpolyesDetails.length
      )
      .map((employee, key) => {
        return (
          <tr key={key}>
            <td className="align-items-md-center d-flex">
              <div className="list-left">
                <span className="w-40 avatar">
                  <img
                    src={employee.profileImage || profilePlaceholder}
                    alt={employee.name}
                  />
                </span>
              </div>
              <div>
                <div className="text-left">{employee.name}</div>
                <div className="text-ellipsis text-left emp_email_value">
                  {employee.work_email}
                </div>
              </div>
            </td>
            <td>{employee.jobtitle}</td>
            <td>{employee.dateofjoining}</td>
            <td>
              {employee.id} - {employee.role_name}
            </td>
            <td>
              {employee.current_salary && `Rs.${employee.current_salary}`}
            </td>
            <td>{employee.team}</td>
          </tr>
        );
      });
  }
  return (
    <div className="content-salary">
      <div className="table-responsive">
        <table className="table">
          <thead className="">
            <tr className="salary-page-row">
              <th>Basic Info</th>
              <th>Job Title</th>
              <th>Joining Date</th>
              <th>EMPLOYEE ID/ROLE</th>
              <th className="text-center">SALARY</th>
              <th>Team</th>
            </tr>
          </thead>

          <tbody className="body-for-salarydetail">{data}</tbody>
        </table>
      </div>
      {allEmpolyesDetails.length == 0 ? (
        <div className="before_loading_data">No Available Data</div>
      ) : (
        ""
      )}
      {count < allEmpolyesDetails.length && (
        <div className="next-page">
          <button
            type="button"
            className="load_more btn"
            onClick={() => setCount(count + 10)}
          >
            Load More
          </button>
        </div>
      )}
    </div>
  );
};

export default EmployeeDetails;
