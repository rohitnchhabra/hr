import React from "react";
import * as a5 from "../../static/avatar.png";
import classnames from "classnames";
import {Spinner} from "reactstrap";

const UserPicHeadTitle = ({
  profileImage,
  editTitleIcon,
  name,
  userName,
  userId,
  jobTitle,
  secondCol,
  details,
  ...props
}) => {
  return (
    <div className="box">
      <div
        className={classnames(
          "d-flex",
          "mx-0",
          "p-3",
          "generic-pic-header",
          secondCol ? "justify-content-between" : null
        )}
      >
        <div className='d-flex'>
          <div className="genric_User_name">
            <span className="avatar w-64 text-center">
              {
                props.isProfilePicLoading ? <Spinner color="red" /> :
                <img src={profileImage ? profileImage : a5} alt={props.isProfilePicLoading} />
              }
            </span>
          </div>
          <div className="genric_User-info px-2">
            <div className="text-capitalize user-header">
              <h5>{name}</h5>
              {/* <span>{editTitleIcon ? editTitleIcon() : null}</span> */}
            </div>
            <div className="d-flex">
              <b>{jobTitle}</b>
            </div>
            <div>
              {userId && <div><small className='text-black'><span className='text-secondary'>Emp ID:</span> {userId?userId:null}</small></div>}
              {userName && <div><small className='text-black'><span className='text-secondary'>Username:</span> {userName?userName:null}</small></div>}
            </div>
          </div>
        </div>
        {secondCol? secondCol() : null}
      </div>
    </div>
  );
};

UserPicHeadTitle.defaultProps = {
  profileImage: ""
};

export default UserPicHeadTitle;
