import React, { PureComponent } from "react";
import "flot/jquery.flot.time";
import "flot-orderbars/js/jquery.flot.orderBars";
const $ = window.jQuery;

class BarChart extends PureComponent {
  componentDidMount() {
    this.initEventListeners();
    window.jQuery("#chartTool").hide();
    this.chart = this.createChart(this.props.getDataSeries(this.props.getData()), this.props.y_axis_ticker, this.props.x_axis_length);
  }
  initEventListeners = () => {

    window.jQuery("#chartContai").on("plothover", (event, pos, item) => {
      if (item) {
        // const x = item.datapoint[0].toFixed(2);
        // const y = item.datapoint[1].toFixed(2);
        window
          .jQuery("#chartTool")
          .html(
            `<i style=display:flex;><div style=background:${
              item.series.color
            };border-radius:4px;height:8px;width:8px;margin-top:6px></div>${
              item.series.data[item.dataIndex][1]
            }days</i> `
          )
          .css({
            top: item.pageY + 5 - window.scrollY,
            left: item.pageX + 5 - window.scrollX
          })
          .fadeIn(200);
      } else {
        window.jQuery("#chartTool").hide();
      }
    });
  };

  createChart(dataSeries) {
    return $.plot(this.$chartContainer, dataSeries, {
      series: {
        bars: {
          show: true,
          barWidth: 12 * 24 * 60 * 60 * 365,
          lineWidth: 0,
          order: 1,
          fillColor: {
            colors: [
              {
                opacity: 1
              },
              {
                opacity: 0.7
              }
            ]
          }
        }
      },
      xaxis: {
        mode: "time",
        tickLength: 10,
        tickSize: [this.props.x_axis_length, "month"],
        axisLabel: "Month",
        axisLabelUseCanvas: true,
        axisLabelFontSizePixels: 13,
        axisLabelPadding: 15,
        timeformat: "%b'%y",
      },
      yaxis: {
        axisLabel: "Value",
        axisLabelUseCanvas: true,
        axisLabelFontSizePixels: 13,
        axisLabelPadding: 5,
        tickSize: this.props.y_axis_ticker
      },
      grid: {
        hoverable: true,
        borderWidth: 0
      },
      legend: {
        backgroundColor: "transparent",
        labelBoxBorderColor: "none"
      },
      colors: ["#a7beff", "#ace5d1", "#ffd7de", "#eedd00"]
    });
  }
  render() {
    return (
      <>
        <div
          id="chartContai"
          ref={r => {
            this.$chartContainer = $(r);
          }}
          style={{ height: "400px" }}
        />
        <div
          className="bar-tooltip"
          ref={r => {
            this.$chartTooltip = $(r);
          }}
          id="chartTool"
        />
      </>
    );
  }
}

export default BarChart;
