import React, { PureComponent } from "react";
import { Row, Col, Breadcrumb, BreadcrumbItem } from "reactstrap";
import $ from "jquery";
/* eslint-disable */
import "easy-pie-chart/dist/jquery.easypiechart.js";
/* eslint-enable */
import Widget from "../../../components/Widget";

class EasyPie extends PureComponent {
  componentDidMount() {
    this.initCharts();
  }

  initCharts() {
    // eslint-disable-line
    $("#easy-pie1").easyPieChart({
      barColor: "#8fe5d4",
      trackColor: "#f8f9fa",
      scaleColor: false,
      lineWidth: 10,
      size: 120
    });

    // $("#easy-pie2").easyPieChart({
    //   barColor: "#ffd7de",
    //   trackColor: "#f8f9fa",
    //   scaleColor: "#f55d5d",
    //   lineCap: "butt",
    //   lineWidth: 22,
    //   size: 140,
    //   animate: 1000
    // });

    // $("#easy-pie3").easyPieChart({
    //   barColor: "#ffebb2",
    //   trackColor: "#f8f9fa",
    //   scaleColor: "#ffc247",
    //   lineCap: "butt",
    //   lineWidth: 22,
    //   size: 140,
    //   animate: 1000
    // });

    // $("#easy-pie4").easyPieChart({
    //   barColor: "#b7b3ff",
    //   trackColor: false,
    //   scaleColor: "#6c757d",
    //   lineCap: "square",
    //   lineWidth: 10,
    //   size: 120,
    //   animate: 1000
    // });
  }

  render() {
     const {chartValue} = this.props;
    return (
      <div>
        <div className="text-center">
          <div
            className="easy-pie-chart-md mb-lg"
            id="easy-pie1"
            data-percent="73"
          >
            {chartValue && chartValue}
          </div>
        </div>
      </div>
    );
  }
}

export default EasyPie;
