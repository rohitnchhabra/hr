import React from "react";
import { Spinner } from "reactstrap";
import {
  getFromDate,
  getWeekOfMonth,
  getNextMonady,
} from "../../services/generic";
import { dateFormatter } from "../../helper/helper";

function Toolbar({
  _onChangeWeek,
  buttonClicked,
  isLoading,
  date,
  month,
  year,
  showWeek,
  showMonth,
  ...props
}) {
  let title = getFromDate(date);
  let disabled = false;
  if (props.timeSheet) {
    disabled = getNextMonady(date) > new Date();
  }
  if (showWeek) {
    disabled = false;
    title =
      "Week " +
      getWeekOfMonth(date) +
      " (" +
      dateFormatter().fullMonths[date.getMonth()] +
      ")";
  }

  if (showMonth) {
    disabled = false;
    title = dateFormatter().fullMonths[month] + " " + year;
  }
  return (
    <div>
      <div className="fc-toolbar">
        <div className="fc-left">
          {isLoading && buttonClicked === "previous" ? (
            <button
              type="button"
              className="fc-prev-button fc-button fc-state-default fc-corner-left fc-corner-right"
            >
              <Spinner color="light" />
            </button>
          ) : (
            <button
              type="button"
              className="fc-prev-button fc-button fc-state-default fc-corner-left fc-corner-right"
              onClick={() => _onChangeWeek("previous")}
            >
              <span className="fc-icon fc-icon-left-single-arrow" />
            </button>
          )}
        </div>
        <div className="fc-right">
          {isLoading && buttonClicked === "next" ? (
            <button
              type="button"
              className="fc-next-button fc-button fc-state-default fc-corner-left fc-corner-right"
            >
              <Spinner color="light" />
            </button>
          ) : (
            <button
              type="button"
              className={`fc-next-button fc-button fc-state-default fc-corner-left fc-corner-right ${
                props.timeSheet ? disabled && "text-gray-light" : ""
              }`}
              onClick={() => _onChangeWeek("next")}
              disabled={disabled}
            >
              <span className="fc-icon fc-icon-right-single-arrow" />
            </button>
          )}
        </div>
        {date && (
          <div className="fc-center">
            <h2>{title}</h2>
          </div>
        )}
        <div className="fc-clear" />
      </div>
    </div>
  );
}

export default Toolbar;
