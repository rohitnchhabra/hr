import React from 'react';
import PropTypes from 'prop-types';
import _ from 'lodash';
import Snackbar from 'material-ui/Snackbar';

const AlertNotification = ({message,handleRequestClose,nameOfClass}) => {
  let showAlert = false;
  let alertMessage = '';
  if (!_.isEmpty(message)) {
    showAlert = true;
    alertMessage = message;
  }
 
  return (
   <div className={nameOfClass}>
      <Snackbar
      open={showAlert}
      message={alertMessage}
      autoHideDuration={2000}
    />
   </div>
  );
};

AlertNotification.propTypes = {
  message: PropTypes.string.isRequired
};
export default AlertNotification;
