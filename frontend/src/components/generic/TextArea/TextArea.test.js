import React from "react";

import { fireEvent, render } from "@testing-library/react";

import "@testing-library/jest-dom";

// import { toHaveProperty } from "@testing-library/jest-dom/matchers";
//
// expect.extend({ toHaveProperty });

import userEvent from "@testing-library/user-event";

import "@testing-library/jest-dom/extend-expect";

import TextArea from "./TextArea";

test("input when the value is changed", () => {
  const setup = () => {
    const mockChange = jest.fn();

    const { getByLabelText } = render(
      <TextArea onChange={mockChange} id="textarea" value="" />
    );
    return getByLabelText("textarea");
  };

  const textarea = setup();
  fireEvent.change(textarea, { target: { value: "Greeting" } });


  expect(textarea.value).toBe("Greeting");
});
