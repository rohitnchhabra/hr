import React from "react";
import Textarea from "./TextArea";

export default function TextAreaWithLabel({ label, ...props }) {
  return (
    <div className="form-group">
      <label>{label}</label>
      <Textarea {...props} />
    </div>
  );
}
