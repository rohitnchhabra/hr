import React, { useState } from "react";
import PropTypes from "prop-types";
import { useEffect } from "react";

const Textarea = ({
  rows,
  placeHolder,
  onChange,
  id,
  name,
  value,
}) => {
  const [text, setText] = useState(value);
  useEffect(()=>{
    if(value){
      setText(value)
    }
  },[value])
  return (
    <textarea
      className={"form-control"}
      placeholder={placeHolder}
      onChange={(e) => {
        setText(e.target.value);
        onChange(e);
      }}
      id={id}
      required
      value={text}
      name={name}
      aria-label="textarea"
      rows={rows?rows:"2"}
    />
  );
};
Textarea.defaultProps={
  rows:"2"
}

Textarea.propTypes = {
  placeHolder: PropTypes.string,
  onChange: PropTypes.func.isRequired,
  id: PropTypes.string.isRequired,
  name: PropTypes.string,
  value: PropTypes.string,
  rows:PropTypes.string
};

export default Textarea;
