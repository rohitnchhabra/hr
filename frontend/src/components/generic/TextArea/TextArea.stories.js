import React from "react";
import { action } from "@storybook/addon-actions";
import TextArea from "./TextArea";
import { withKnobs, boolean, text } from "@storybook/addon-knobs";
import TextAreaWithLabel from "./TextAreaWithLabel";

export default {
  title: "TextArea",
  component: TextArea,
  decorators: [withKnobs]

};

export const textArea = () => (
  <TextArea
    disabled={boolean("Disabled", false)}
    onChange={action("changed")}
    id="TextArea"
  />
);

export const textAreaWithLabel = () => (
  <TextAreaWithLabel
    label={text("Label", "Name")}
    disabled={boolean("Disabled", false)}
    onChange={action("changed")}
    id="TextAreaWithLabel"
  />
);
