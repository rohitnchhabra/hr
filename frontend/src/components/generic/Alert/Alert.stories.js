import React from "react";

import { withKnobs} from "@storybook/addon-knobs";

import GenericAlert, {
  GenericAlertWithLabel,
  GenericAlertDangerWithLabel,
  GenericAlertDanger
} from "./Alert";

export default { title: "Alerts", decorators: [withKnobs] };

export const AlertSuccess = () => {
  return <GenericAlert>Alert Success</GenericAlert>;
};

export const AlertSuccessWithLabel = () => {
  return (
    <GenericAlertWithLabel label="Info : ">
      Alert Success with Label{" "}
    </GenericAlertWithLabel>
  );
};
export const AlertDanger = () => {
  return <GenericAlertDanger>Alert Danger</GenericAlertDanger>;
};

export const AlertDangerWithLabel = () => {
  return (
    <GenericAlertDangerWithLabel label="Alert : ">
      Alert Danger with Label{" "}
    </GenericAlertDangerWithLabel>
  );
};
