import React from "react";
import { Alert } from "reactstrap";
import PropTypes from "prop-types";

const GenericAlert = ({ fade, color, label, className, ...props }) => {
  return (
    <Alert
      {...props}
      color={color ? color : "success"}
      className={`alert ${className ? className : ""}`}
    >
      {props.children}
    </Alert>
  );
};

GenericAlert.prototype = {
  className: PropTypes.string,
  color: PropTypes.oneOf([
    "primary",
    "secondary",
    "success",
    "danger",
    "warning",
    "info",
    "light",
    "dark",
  ]),
};
GenericAlert.defaultProps = {
  color: "success",
};

export default GenericAlert;

export const GenericAlertWithLabel = ({ label, ...props }) => {
  return (
    <GenericAlert {...props}>
      <strong>{label ? label : "Info : "}</strong>
      {props.children}
    </GenericAlert>
  );
};

GenericAlertWithLabel.prototype = {
  ...GenericAlert.PropTypes,
  label: PropTypes.string,
};
GenericAlertWithLabel.defaultProps = {
  ...GenericAlert.defaultProps,
  label: "Info : ",
};

export const GenericAlertDanger = (props) => {
  return (
    <GenericAlert {...props} color="danger">
      {props.children}
    </GenericAlert>
  );
};

GenericAlertDanger.prototype = {
  ...GenericAlert.PropTypes,
};
GenericAlertWithLabel.defaultProps = {
  ...GenericAlert.defaultProps,
};

export const GenericAlertDangerWithLabel = ({ label, ...props }) => {
  return (
    <GenericAlertWithLabel
      {...props}
      color="danger"
      label={label ? label : "Alert : "}
    >
      {props.children}
    </GenericAlertWithLabel>
  );
};

GenericAlertDangerWithLabel.prototype = {
  ...GenericAlertWithLabel.PropTypes,
};
GenericAlertDangerWithLabel.defaultProps = {
  ...GenericAlertWithLabel.defaultProps,
  label: "Alert : ",
};
