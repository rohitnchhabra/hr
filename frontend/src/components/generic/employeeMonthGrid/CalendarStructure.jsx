import React, { Fragment } from "react";
import PropTypes from "prop-types";
import * as _ from "lodash";
import CalendarWeek from "./CalendarWeek";
import times from "lodash/times";

const CalendarStructure = ({
  userId,
  month,
  onShowDaySummary,
  onWorkingHoursChange,
  userName
}) => {
  let day_moves = "move_days";
  let daysOfCalender = _.map(month, (dayData, key) => {
    
    if (dayData.date === "01") {
      let previousDays = 0;
      if(dayData.day === "Sunday") {
        previousDays = 6;
      }
      else if (dayData.day === "Saturday") {
        previousDays = 5;
      } else if (dayData.day === "Friday") {
        previousDays = 4;
      } else if (dayData.day === "Thursday") {
        previousDays = 3;
      } else if (dayData.day === "Wednesday") {
        previousDays = 2;
      } else if  (dayData.day === "Tuesday") {
        previousDays = 1;
      }
      else 
      previousDays = 0;
      return (
        <Fragment key={key} >
          {times(previousDays, (v, i) => (
            <CalendarWeek key={v} dayData={{day_moves}} />
      ))}
          <CalendarWeek
            key={key}
            userId={userId}
            dayData={dayData}
            onShowDaySummary={onShowDaySummary}
            onWorkingHoursChange={onWorkingHoursChange}
            userName={userName}
          />
        </Fragment>
      );
    }
    return (
      <CalendarWeek
        key={key}
        userId={userId}
        dayData={dayData}
        onShowDaySummary={onShowDaySummary}
        onWorkingHoursChange={onWorkingHoursChange}
        userName={userName}
      />
    );
  });
  return <div  id="calendar">{daysOfCalender}</div>;
};

CalendarStructure.propTypes = {
  userId: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number
  ]).isRequired,
  month: PropTypes.array.isRequired,
  onShowDaySummary: PropTypes.func.isRequired
};

export default CalendarStructure;
