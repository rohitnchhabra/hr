import React from "react";
import PropTypes from "prop-types";
import $ from "jquery";
import Day from "./Day";

const CalendarWeek = ({
  userId,
  dayData,
  onShowDaySummary,
  onWorkingHoursChange,
  userName,
  timeSheet,
  className,
  id,
  isDisable
}) => {
  const setCalendarDay = () => {
    let calendarWidth = $("#calendar").width();
  

    let dayInRow = parseInt(calendarWidth / 100);
    
    if (dayInRow < 7 && calendarWidth % 100 > 0) {
      let dayWidth = parseInt(calendarWidth / dayInRow);

      $("#calendar .calendar-day").css({ width: dayWidth + "px" });
    } else if (dayInRow >= 7) {
      $("#calendar .calendar-day").css({ width: calendarWidth / 7 + "px" });
    }
  };
  $(document).ready(function() {
    $(window).on("resize", function() {
      setCalendarDay();
    });
    setCalendarDay();
  });
  let dayHtml = "";
  if (userId) {
    dayHtml = (
      <Day
        forEmployeeHours={false}
        dayData={dayData}
        showDaySummary={onShowDaySummary}
        userid={userId}
        userName={userName}
        timeSheet={timeSheet}
        isDisable={isDisable}
      />
    );
  } else {
    dayHtml = (
      <Day
        forEmployeeHours
        dayData={dayData}
        onWorkingHoursChange={onWorkingHoursChange}
        timeSheet={timeSheet}
        isDisable={isDisable}
      />
    );
  }
  return (
    <div id={`${id?id:"calendarDay"}`} className={`${className?className:"calendar-day"} ${dayData.day_moves == 'move_days'? dayData.day_moves :""}`}>
      <div className="fc-event-container" id={`${dayData.full_date}`}>
        {dayHtml}
      </div>
    </div>
  );
};

CalendarWeek.propTypes = {
  dayData: PropTypes.object.isRequired,
  userId: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number
  ]),
  onShowDaySummary: PropTypes.func,
  onWorkingHoursChange: PropTypes.func
};

export default CalendarWeek;
