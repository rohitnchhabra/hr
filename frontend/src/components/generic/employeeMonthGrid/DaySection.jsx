import React from "react";
import PropTypes from "prop-types";

const DaySection = ({
  block,
  classname,
  dataToDisplay,
  time,
  title,
  officeTime,
}) => {
  let content = "";

  if (block === "type1") {
    content = (
      <span>
        {/* change this class to 'fc-time' to make the text bold */}
        <span className="fc-content">{time}</span>
        <span className="fc-title">
          {officeTime ? (
            <i className="officetime">{`- ${officeTime} hrs`}</i>
          ) : (
            ""
          )}
        </span>
      </span>
    );
    // {title}
  } else if (block === "type2") {
    content = dataToDisplay;
  }
  return (
    <div className={classname}>
      <div className="fc-content">{content}</div>
    </div>
  );
};

DaySection.propTypes = {
  block: PropTypes.string.isRequired,
  classname: PropTypes.string.isRequired,
  dataToDisplay: PropTypes.string,
  time: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.object,
  ]).isRequired,
  title: PropTypes.string
};

export default DaySection;
