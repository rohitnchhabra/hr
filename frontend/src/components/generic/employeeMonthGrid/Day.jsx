import React from "react";
import PropTypes from "prop-types";
import $ from "jquery";
import * as dayType from "../../../redux/attendance/constants";
import DaySection from "./DaySection";
import "timepicker/jquery.timepicker.css";
import "timepicker/jquery.timepicker.min.js";
const styles = {
  timeInputBox: {
    width: "40px"
  }
};


let fl,
  hl,
  wl,
  al = 0;
  
const Day = ({
  dayData,
  classname,
  forEmployeeHours,
  day,
  userid,
  showDaySummary,
  onWorkingHoursChange,
  userName,
  timeSheet,
  isDisable
}) => {
  $(document).ready(function() {
    $("input.timepicker").timepicker({
      minTime: "5:00",
      maxTime: "12:00",
      timeFormat: "h:i",
      step: 10
    });
  });
  
  let d = dayData;
  
  let officeWorkingHours = d.office_working_hours;
  let main = "";
  let _class =
    "fc-day-grid-event fc-h-event fc-event fc-start fc-end fc-draggable ";
  let extraTimebg = "";
  let date = d.date;
  let datebg = "dark-white";
  let workTimeData = d.total_time + " - Total Work Time";
  

  if (d.extra_time_status === "-") {
    extraTimebg = "red";
  } else if (d.extra_time_status === "+") {
    extraTimebg = "green";
  }
  if (d.admin_alert === 1) {
    date = date + " * ";
    datebg = "indigo";
    workTimeData = d.admin_alert_message;
  }

 if(dayData.isDayBeforeJoining) {
  main = (
    <div>
      <DaySection
        classname={_class + "before_joining"}
        block="type1"
        time={<h5>{d.date}</h5>}
        title={d.day}
      />
      </div>
    );
 }
 else { 
  if (!forEmployeeHours && d.day_type === dayType.WORKING_DAY) {
    
    fl = 0;
    hl = 0;
    wl = 0;

    main = (
      <div
        data-toggle={timeSheet?d.status!=="Approved" && d.status!=="Pending" && new Date(d.full_date) <= new Date() && isDisable==false ?"modal":"":"modal"}
        data-target="#modalUserDaySummary"
        onClick={
          () => 
          showDaySummary(
              { userid, full_date: dayData.full_date, in_time: dayData.in_time, out_time: dayData.out_time,userName:userName ,status:dayData.status}
            )
          }
      >
        {d.admin_alert !== 1 ? (
          <>
            <DaySection
              classname={`${_class} ${timeSheet
                ? d.status == "Rejected"
                  ? "rejectedHours"
                  : d.status == "Approved"
                  ? "approvedHours"
                  : d.status == "Pending"
                  ? "pendingHours"
                  : datebg
                : datebg }`}
              block="type1"
              time={<h5>{date} {d.office_working_hours !== "09:00" &&<i className="officetime">{d.office_working_hours + " "}hrs</i>}</h5>}
              title={d.day}
            />
            {!timeSheet && <DaySection
              classname={_class + "dark-white d-flex align-items-center "}
              block="type1"
              time={d.in_time + " - " + d.out_time}
              title=""
            />}
            {timeSheet && (
                <DaySection
                  classname={`_class d-flex align-items-center timesheet-hours pointer ${
                    new Date(d.full_date) > new Date()? "futureDate":
                    d.status == "Rejected"
                      ? "rejectedHours"
                      : d.status == "Approved"
                      ? "approvedHours"
                      : d.status=="Pending"?"pendingHours":"dark-white"
                  } `}
                  block="type1"
                  time={`${d.total_hours} - Total Hours`}
                  title=""
                />
              )}
            {!timeSheet && <DaySection
              classname={
                _class + "dark-white center d-flex align-items-center "
              }
              block="type2"
              dataToDisplay={
                workTimeData === "In/Out Time Missing" ? "" : workTimeData
              }
            />}
            {!timeSheet && <DaySection
              classname={_class + extraTimebg}
              block="type1"
              time={d.extra_time.length === 0  ? "-" : (d.extra_time === 0 ? '':d.extra_time) }
              title=""
            />}
          </>
        ) : (
          <>
          <DaySection
            classname={
              ++al === 1
                ? _class + datebg + "valueAfter"
                : _class + datebg + "valueAfter1"
            }
            block="type1"
            time={<h5>{date} {d.office_working_hours !== "09:00" &&<i className="officetime">{d.office_working_hours + " "}hrs</i>}</h5>}
            title={d.day}
          />
          <DaySection classname={ _class + datebg } block="type2" dataToDisplay={workTimeData === 'In/Out Time Missing'? workTimeData:''} />
          </>
        )}
        {/* // <DaySection classname={_class + datebg } block="type1" time={<h5>{date}</h5>} title={d.day}  />
      // <DaySection classname={_class + 'dark-white d-flex align-items-center ' } block="type1" time={d.in_time + ' - ' + d.out_time} title='' />
      // <DaySection classname={_class + 'dark-white center d-flex align-items-center '} block="type2" dataToDisplay={workTimeData === 'In/Out Time Missing' ? '':workTimeData} />
      // <DaySection classname={_class + extraTimebg } block="type1" time={d.extra_time.length === 0 ?'-':d.extra_time } title='' /> */}
      </div>
    );
  } else if (!forEmployeeHours && d.day_type === dayType.NON_WORKING_DAY) {
    fl = 0;
    hl = 0;
    wl = wl + 1;
    al = 0;
    main = (
      <div>
        <DaySection
          classname={wl === 1 ? _class + "orange" : _class + "oneafter"}
          block="type1"
          time={<h5>{d.date}</h5>}
          title={d.day}
        />
        {/* <DaySection classname={_class + 'yellow'} block="type1" time={''} /> */}
        <DaySection classname={_class + 'yellow'} block="type1" time={d.day_text} />
      </div>
    );
  } else if (!forEmployeeHours && d.day_type === dayType.FUTURE_WORKING_DAY) {
    fl = 0;
    hl = 0;
    wl = 0;
    al = 0;
    main = (
      <div>
        <DaySection
          classname={_class + "light-gray"}
          block="type1"
          time={<h5>{date} {d.office_working_hours !== "09:00" &&<i className="officetime">{d.office_working_hours + " "}hrs</i>}</h5>}
          title={d.day}
        />
        {/* <DaySection classname={_class + 'dark-white'} block="type1" time='' title='' />
      <DaySection classname={_class + 'dark-white'} block="type1" dataToDi splay={d.day_text} />
      <DaySection classname={_class + 'dark-white center'} block="type2" dataToDisplay={"-"} /> */}
      </div>
    );
  } else if (!forEmployeeHours && d.day_type === dayType.LEAVE_DAY) {
    fl = fl + 1;
    hl = 0;
    wl = 0;
    al = 0;

    main = (
      <div>
        <DaySection
          classname={fl === 1 ? _class + "blue" : _class + "after"}
          block="type1"
          time={<h5>{date}</h5>}
          title={d.day}
        />
        <DaySection classname={_class + 'red'} block="type1" time={'On Leave'} title='' />
        <DaySection classname={_class + 'red'} block="type1" time={d.day_text} title='' />
      </div>
    );
  } else if (!forEmployeeHours && d.day_type === dayType.HALF_DAY) {
    fl = 0;
    hl = hl + 1;
    wl = 0;
    al = 0;
    main = (
      <div
      data-toggle="modal"
      data-target="#modalUserDaySummary"
      onClick={
        () => 
          showDaySummary(
            { userid, full_date: dayData.full_date, in_time: dayData.in_time, out_time: dayData.out_time,userName:userName }
          )
        }
      >
        <DaySection
          classname={hl === 1 ? _class + "red-100" : _class + "red-10"}
          block="type1"
          time={<h5>{date} {d.office_working_hours !== "09:00" &&<i className="officetime">{d.office_working_hours + " "}hrs</i>}</h5>}
          title={d.day}
        />
         {timeSheet && <DaySection classname={_class + 'red-100 d-flex align-items-center timesheet-half-day'} block="type2" dataToDisplay={`${d.total_hours} - Total Hours`} title='' /> }
        {!timeSheet && <DaySection classname={_class + 'red-100 d-flex align-items-center'} block="type1" time={d.in_time + " - " + d.out_time} title='' /> }
       { !timeSheet && <DaySection classname={_class + 'red-100 d-flex align-items-center'} block="type1" time={'Half day'} title='' /> }
        {!timeSheet && <DaySection classname={_class + 'red-100 d-flex align-items-center'} block="type2"
              dataToDisplay={
                workTimeData === "In/Out Time Missing" ? "" : workTimeData
              } title='' />  }
      </div>
    );
  } else if (forEmployeeHours && d.day_type === dayType.WORKING_DAY) {
    fl = 0;
    hl = 0;
    wl = 0;
    al = 0;
    main = (
      <div data-toggle="modal">
        <DaySection
          classname={_class + "dark-white"}
          block="type1"
          time={<h5>{d.date}</h5>}
          title={d.day}
          officeTime={d.office_working_hours}
        />
        <DaySection
          classname={_class + "dark-white"}
          block="type1"
          time={<h6>Hours : {officeWorkingHours}</h6>}
          title=""
        />

        <div className="fc-day-grid-event fc-h-event fc-event fc-start fc-end dark-white fc-draggable">
          <div className="fc-content">
            <span className="fc-title">Change to &nbsp;:&nbsp;</span>
            <span className="fc-time">
              <input
                type="text"
                className="timepicker"
                style={styles.timeInputBox}
                onBlur={e =>
                  onWorkingHoursChange(dayData.full_date, e.target.value)
                }
              />
            </span>
          </div>
        </div>
      </div>
    );
  } else if (forEmployeeHours && d.day_type === dayType.NON_WORKING_DAY) {
    fl = 0;
    hl = 0;
    wl = 0;
    al = 0;
    main = (
      <div>
        <DaySection
          classname={_class + "yellow"}
          block="type1"
          time={<h5>{d.date}</h5>}
          title={d.day}
        />
        <DaySection
          classname={_class + "yellow " +`${d.day_text == 'Weekend Off' ? "Weekend_Off":""}`}
          block="type1"
          time={d.day_text == 'Weekend Off' ? "":"Non Working day"}
        />
        <DaySection
          classname={_class + "yellow"}
          block="type1"
          time={d.day_text}
          title=""
        />

        <div className="fc-day-grid-event fc-h-event fc-event fc-start fc-end dark-white fc-draggable">
          <div className="fc-content">
            <span className="fc-title">Change to &nbsp;:&nbsp;</span>
            <span className="fc-time">
              <input
                type="text"
                className="timepicker"
                style={styles.timeInputBox}
                onBlur={e =>
                  onWorkingHoursChange(dayData.full_date, e.target.value)
                }
              />
            </span>
          </div>
        </div>
      </div>
    );
  } else if (d.day_type === dayType.RH_COMPENSATION) {
    main = (
      <div>
        <DaySection
          classname={_class + "cyan half text-body"}
          block="type1"
          time={<h5>{d.date}</h5>}
          title={d.day}
        />
        <DaySection classname={_class + 'cyan d-flex align-items-center text-body'} block="type1" title={''} time={`RH (${d.leave_status})`} />
        <DaySection
          classname={_class + "cyan text-body"}
          block="type1"
          time={d.day_text}
          title=""
        />
      </div>
    );

  }
   }
   
   

  return <div>{main}</div>;
  
};


Day.propTypes = {
  classname: PropTypes.string,
  forEmployeeHours: PropTypes.bool,
  day: PropTypes.string,
  userid: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number
  ]),
  showDaySummary: PropTypes.func,
  onWorkingHoursChange: PropTypes.func,
  dayData: PropTypes.shape({
    date: PropTypes.oneOfType([
      PropTypes.string,
      PropTypes.number
    ]),
    day: PropTypes.string,
    day_type: PropTypes.string,
    full_date: PropTypes.string,
    admin_alert: PropTypes.oneOfType([
      PropTypes.string,
      PropTypes.number
    ]),
    admin_alert_message: PropTypes.string,
    day_text: PropTypes.string,
    extra_time: PropTypes.oneOfType([
      PropTypes.string,
      PropTypes.number
    ]),
    in_time: PropTypes.string,
    out_time: PropTypes.string,
    total_time: PropTypes.string,
    office_working_hours: PropTypes.string,
    text: PropTypes.string
  }).isRequired
};

export default Day;
