import React from "react";
import { action } from "@storybook/addon-actions";
import InputText,{InputTextWithLabel} from "./InputText";
import { withKnobs, boolean, text } from "@storybook/addon-knobs";

export default {
  title: "InputText",
  // component: InputText,
  decorators: [withKnobs],
};

export const inputtext = () => (
  <InputText
    disabled={boolean("Disabled", false)}
    onChange={action("changed")}
    id="inputText"
  />
);

export const inputTextWithLabel = () => (
  <InputTextWithLabel
    label={text("Label", "Name")}
    disabled={boolean("Disabled", false)}
    onChange={action("changed")}
    id={"inputTextWithLabel"}
  />
);
