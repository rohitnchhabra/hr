import React from "react";
import PropTypes from "prop-types";

const InputText = ({
  testId,
  className,
  required,
  ref,
  type,
  maxlength,
  pattern,
  title,
  placeHolder,
  onChange,
  id,
  name,
  value,
  style,
  disabled,
  label,
}) => {
  return (
    <div className="form-group">
      {label ? <label>{label}</label> : null}
      <input
        type={type ? type : "text"}
        title={title}
        pattern={pattern}
        maxLength={maxlength}
        className={"form-control " + className}
        ref={ref}
        placeholder={placeHolder}
        onChange={onChange}
        id={id}
        required={required ? required : false}
        name={name}
        value={value}
        style={style}
        disabled={disabled}
        aria-label="input"
        data-testid={testId}
      />
    </div>
  );
};

InputText.propTypes = {
  className: PropTypes.oneOf([
    "mt-1",
    "mb-1",
    "mb-2",
    "mt-2",
    "pr-0",
    "p-1",
    "text-right",
    "no-border",
  ]),
  placeHolder: PropTypes.string,
  onChange: PropTypes.func.isRequired,
  id: PropTypes.string.isRequired,
  name: PropTypes.string,
  value: PropTypes.string,
  style: PropTypes.string,
  label: PropTypes.string,
};

export function InputTextWithLabel({ label, ...props }) {
  return (
    <div className="form-group">
      <InputText {...props} label={label} />
    </div>
  );
}

InputTextWithLabel.prototype = {
  ...InputText.PropTypes,
  label: PropTypes.string.isRequired,
};

export default InputText;
