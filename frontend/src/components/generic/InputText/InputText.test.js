import React from "react";

import { cleanup, fireEvent, render } from "@testing-library/react";

import "@testing-library/jest-dom";

import userEvent from "@testing-library/user-event";

import InputText from "./InputText";

test("input when the value is changed", () => {
  const setup = () => {
    const mockChange = jest.fn();

    const { getByLabelText } = render(
      <InputText onChange={mockChange} id="input" />
    );
    return getByLabelText("input");
  };

  const input = setup();

  userEvent.type(input, "Greeting");
  expect(input.value).toBe("Greeting");
});
