import React, { Component } from "react";

export default class SalaryTable extends Component {
  render() {
      const {data} = this.props;
    return (
      <div>
          {this.props.earning == "success"?
        <div className=" col-md-12 salary-addition-width payslip-emp-id">
          <div className="total-earning col-sm-12 p-0">
            <div className="total-earn-border col-sm-12 bg-success ">
              TOTAL <span className="font-weight-bold"> EARNINGS </span>{" "}
            </div>
          </div>
          <div className="col-sm-12 detail-earning d-flex mb-2 px-0">
            <div className="d-flex justify-content-between px-2 py-2 border-btm ">
              <div className="salary-title">Basic</div>
              <div> {data.basic} </div>
            </div>
            <div className="d-flex justify-content-between px-2 py-2 border-btm ">
              <div className="salary-title">HRA</div>
              <div>{data.hra}</div>
            </div>
            <div className="d-flex justify-content-between px-2 py-2 border-btm">
              <div className="salary-title">Conveyance</div>
              <div>{data.conveyance}</div>
            </div>
            <div className="d-flex justify-content-between px-2 py-2 border-btm">
              <div className="salary-title">Medical Allowance</div>
              <div>{data.medical_allowance}</div>
            </div>
            <div className="d-flex justify-content-between px-2 py-2 border-btm">
              <div className="salary-title">Special Allowance</div>
              <div>{data.special_allowance}</div>
            </div>
            <div className="d-flex justify-content-between px-2 py-2 border-btm ">
              <div className="salary-title">Arrears</div>
              <div>{data.arrear}</div>
            </div>
            <div className="d-flex justify-content-between px-2 py-2 ">
              <div className="salary-title">Bonus</div>
              <div>{data.bonus}</div>
            </div>
          </div>
        </div>
         :(this.props.earning == "deduction"?
        <div className=" col-md-12 salary-addition-width payslip-emp-id">
          <div className="total-earning col-sm-12 p-0">
            <div className="total-earn-border col-sm-12 bg-danger">
              TOTAL <span className="font-weight-bold"> DEDUCTIONS </span>{" "}
            </div>
          </div>
          <div className="col-sm-12 detail-earning d-flex mb-2 px-0">
            <div className="d-flex justify-content-between px-2 py-2 border-btm ">
              <div className="salary-title">EPF</div>
              <div> {data.epf} </div>
            </div>
            <div className="d-flex justify-content-between px-2 py-2 border-btm ">
              <div className="salary-title">Loan</div>
              <div>{data.loan}</div>
            </div>
            <div className="d-flex justify-content-between px-2 py-2 border-btm">
              <div className="salary-title">Advance</div>
              <div>
              {data.advance}
              </div>
            </div>
            <div className="d-flex justify-content-between px-2 py-2 border-btm">
              <div className="salary-title">Misc Deductions</div>
              <div>
              {data.misc_deduction}
              </div>
            </div>
            <div className="d-flex justify-content-between px-2 py-2 border-btm">
              <div className="salary-title">TDS</div>
              <div>
              {data.tds}
              </div>
            </div>
          </div>
        </div>
        :"")
          }


      </div>
    );
  }
}
