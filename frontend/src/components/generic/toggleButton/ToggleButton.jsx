import React from 'react'
import ToggleButton from "react-toggle-button";
import PropTypes from 'prop-types'

function GenericToggleButton(props) {
    const {onToggle,value}=props;
    return (
        <ToggleButton onToggle={onToggle} value={value} />
    )
}

GenericToggleButton.prototype={
    onToggle:PropTypes.func.isRequired,
    value:PropTypes.bool.isRequired
}

export default GenericToggleButton
