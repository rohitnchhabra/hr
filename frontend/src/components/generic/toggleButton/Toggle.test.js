import React from "react";

import {render} from "@testing-library/react";

import "@testing-library/jest-dom";

import ToggleButton from "./ToggleButton";

test("toggle renders with ON and OFF", () => {
  const mockChange = jest.fn();
  const { container} = render(
    <ToggleButton onToggle={mockChange}  />
  );
  expect(container).toMatchSnapshot()
});
