import React from "react";
import { withKnobs, boolean } from "@storybook/addon-knobs";
import { action } from "@storybook/addon-actions";
import ToggleButton from "./ToggleButton";

export default { title: "Toggle Button", decorators: [withKnobs] };

export const GenericToggleButton = () => {
  return <ToggleButton value={boolean("toggle", false)} onToggle={action("clicked")} />;
};
