import React, { useState,useEffect } from "react";
import classnames from "classnames";
import "./Checkbox.module.scss"
import PropTypes from "prop-types";
const Checkbox = ({ checked, onClick, name, disabled, ...props }) => {
  const [isChecked, setIsChecked] = useState(checked)
  useEffect(()=>{
    setIsChecked(checked)
  },[checked])
  return (
    <div
      className={classnames("form-check mb-sm", { disabled })}
    >
      <input
        className="custom-checkbox"
        type="checkbox"
        checked={isChecked}
        aria-label="checkbox"
        {...props}
      />
      <span
        className={classnames("checkmark", { "box-border": !checked === true })}
        data-testid="checkmark"
        onClick={(evt) => {
          setIsChecked(!isChecked)
          onClick(evt)
        }}
      />
    </div>
  );
};

Checkbox.defaultProps = {
  disabled: false,
};

Checkbox.propTypes = {
  /**For managing status of checkbox, checked or not */
  checked: PropTypes.bool.isRequired,
  /**Identifier for Checkbox */
  name: PropTypes.oneOfType([PropTypes.string, PropTypes.number]).isRequired,
  /**Function which will be called on checkbox click */
  onClick: PropTypes.func.isRequired,
  /**Boolean value for disabling checkbox */
  disabled: PropTypes.bool,
};

export default Checkbox;
