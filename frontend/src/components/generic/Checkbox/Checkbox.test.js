import React from "react";

import {render} from "@testing-library/react";

import "@testing-library/jest-dom";
import Checkbox from "./Checkbox";
import userEvent from "@testing-library/user-event";

test("Checkbox changes after click", () => {
  const mockClick = jest.fn();
  const mockChange = jest.fn();

  const { getByRole,getByTestId } = render(
    <Checkbox
      onClick={mockClick}
      checked={false}
      name="checkbox"
      onChange={mockChange}
    />
  );
  
  const checkbox = getByRole("checkbox")
  

  expect(checkbox.checked).toBe(false);
  expect(getByTestId('checkmark')).toBeInTheDocument(true)
  
  userEvent.click(getByTestId('checkmark'));
  expect(checkbox.checked).toBe(true);
});

test("Checkbox matches the snapshot", () => {
  const mockClick = jest.fn();

  const { container } = render(
    <Checkbox onClick={mockClick} checked={false} name="checkbox" />
  );
  expect(container).toMatchSnapshot();
});

