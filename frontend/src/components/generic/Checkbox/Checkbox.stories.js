import React from "react";
import { action } from "@storybook/addon-actions";
import Checkbox from "./Checkbox";
import { withKnobs, boolean } from "@storybook/addon-knobs";

export default {
  title: "Checkbox",
  component: Checkbox,
  decorators: [withKnobs],
};

export const checkbox = () => (
  <Checkbox
    disabled={boolean("Disabled", false)}
    checked={boolean("Checked", false)}
    onClick={action("clicked")}
    name="checkbox"
  />
);
