import React from "react";

import { render, queryByAttribute} from "@testing-library/react";

import "@testing-library/jest-dom";
import "@testing-library/jest-dom/extend-expect";

import Widget from "./Widget";

test("Widget renders properly", () => {
  const getByclassName = queryByAttribute.bind(null, "class");

  const dom = render(<Widget />);
  const widget = getByclassName(dom.container, "widget");
  expect(widget).toBeInTheDocument(true);
});
