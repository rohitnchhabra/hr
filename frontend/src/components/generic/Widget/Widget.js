import React from "react";
import PropTypes from "prop-types";
// import 'imports-loader?window.jQuery=jquery,this=>window!widgster'; // eslint-disable-line
import s from "./Widget.module.scss"; // eslint-disable-line css-modules/no-unused-class
import Heading from "../Heading/Heading";

const Widget = (props) => {
  const { title, className, children, bodyClass, ...attributes } = props;
  return (
    <section
      className={`${s.widget} ${className ? className : ""}`}
      {...attributes}
    >
      {title &&
        (typeof title === "string" ? (
          <Heading className={s.title}>{title}</Heading>
        ) : (
          <header className={s.title}>{title}</header>
        ))}
      <div className={`${s.widgetBody} widget-body ${bodyClass}`}>
        {children}
      </div>
    </section>
  );
};

Widget.prototype = {
  title: PropTypes.node,
  className:
    PropTypes.oneOf[
      ("mx-auto",
      "mb",
      "pl-0",
      "pr-0",
      "p-0",
      "attendance-upload-widget",
      "attendance-upload-widget-config",
      "thumb-xl")
    ],
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node,
  ]),
  bodyClass: PropTypes.oneOf[("mt", "mt-0")],
};

Widget.defaultProps = {
  title: null,
  className: "",
  children: [],
  bodyClass: "",
};

export const WidgetWithTitle = ({ title, ...props }) => {
  return <Widget title={title} {...props} />;
};
WidgetWithTitle.prototype = {
  ...PropTypes.Widget,
  title: PropTypes.node.isRequired,
};

WidgetWithTitle.defaultProps = {
  ...Widget.defaultProps,
};

export default Widget;
