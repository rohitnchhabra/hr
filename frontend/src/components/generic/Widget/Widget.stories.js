import React from "react";

import { withKnobs } from "@storybook/addon-knobs";

import Widget, { WidgetWithTitle } from "./Widget";

export default { title: "Widget", decorators: [withKnobs] };

export const WidgetData = () => {
  return (
    <Widget>
      <div>Widget Data</div>
    </Widget>
  );
};
export const WidgetDataWithTitle = () => {
  return (
    <WidgetWithTitle title={<h5>Title</h5>}>
      <div>Widget Data</div>
    </WidgetWithTitle>
  );
};
