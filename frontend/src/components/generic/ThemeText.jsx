import React from "react";

export default function ThemeText({light, bold}) {
  return (
    <div className="pb-3">
      {light} <strong>{bold}</strong>
    </div>
  );
}
