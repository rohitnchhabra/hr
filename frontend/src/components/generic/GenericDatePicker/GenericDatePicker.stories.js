import React from "react";
import GenericDatePicker from "./GenericDatePicker";
import { withKnobs} from "@storybook/addon-knobs";

export default {
  title: "Date Picker",
  decorators: [withKnobs],
};

export const DatePicker = () => <GenericDatePicker />;
