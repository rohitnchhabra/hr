import React from "react";

import {render} from "@testing-library/react";

import "@testing-library/jest-dom";

import GenericDatePicker from "./GenericDatePicker";

test("Date Picker matches the snapshot", () => {
  let date= new Date("2020-08-13")
  const { container} = render(
    <GenericDatePicker date={date}/>
  );

  expect(container).toMatchSnapshot()
});
