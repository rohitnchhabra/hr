import React, { useState } from "react";
import "date-fns";
import DateFnsUtils from "@date-io/date-fns";
import {
  MuiPickersUtilsProvider,
  KeyboardDatePicker,
  DatePicker,
} from "@material-ui/pickers";
import styles from "./GenericDatePicker.module.scss";
import PropTypes from 'prop-types';

function GenericDatePicker(props) {
  const [isOpen, setOpenVal] = useState(false);
  const handleDateChange = (date) => {
    const { dob } = props;
    props.handleDateChange(date);
    if (!dob) {
      setOpenVal(false);
    }
  };

 const handleModal = () => {
    if (!isOpen) {
      onOpen();
    }
  };

  const onOpen = () => {
    setOpenVal(true);
  };

  const onClose = () => {
    setOpenVal(false);
  };

  const {
    pickerProviderClass,
    gridClass,
    datePickerClass,
    type,
    date,
    emptyLabel,
    notClearable,
  } = props;
  return (
    <div
      id={styles["date-picker"]}
      onClick={handleModal}
      className={props.className}
    >
      <MuiPickersUtilsProvider
        className={`picker-provider ${pickerProviderClass}`}
        utils={DateFnsUtils}
      >
        <div
        
          className={`picker-grid ${gridClass}`}
          justify="space-around"
        >
          {type === "absolute" ? (
            <KeyboardDatePicker
              emptyLabel={
                date == null && props.hasOwnProperty("emptyLabel")
                  ? emptyLabel
                  : ""
              }
              className={`${styles["keyboard-date-picker"]} ${datePickerClass}`}
              style={{ zIndex: 1600 }}
              margin="normal"
              id="date-picker-dialog"
              format="dd/MM/yyyy"
              views={["year", "month", "date"]}
              open={isOpen}
              value={date}
              onOpen={onOpen}
              onClose={onClose}
              onChange={handleDateChange}
            />
          ) : (
            <DatePicker
              emptyLabel={
                date == null && props.hasOwnProperty("emptyLabel")
                  ? emptyLabel
                  : ""
              }
              open={isOpen}
              clearable={notClearable ? false : true}
              clearLabel={"Clear"}
              disableToolbar={props.hasOwnProperty("dob") ? false : true}
              disableFuture={props.hasOwnProperty("dob") ? true : false}
              views={["year", "month", "date"]}
              autoOk={true}
              className={`${styles["keyboard-date-picker"]} ${datePickerClass}`}
              margin="normal"
              id="date-picker-dialog"
              format="dd/MM/yyyy"
              value={date}
              onOpen={onOpen}
              onClose={onClose}
              onChange={(date) => handleDateChange(date)}
            />
          )}
        </div>
      </MuiPickersUtilsProvider>
    </div>
  );
}

GenericDatePicker.prototype={
    className:PropTypes.oneOf(['ml-0']),
    type:PropTypes.oneOf(['absolute','inline']).isRequired,
    date:PropTypes.object,
    notClearable:PropTypes.bool,
    emptyLabel:PropTypes.string,
    datePickerClass:PropTypes.string,
    gridClass:PropTypes.string,
    pickerProviderClass:PropTypes.string,
    handleDateChange:PropTypes.func.isRequired
}

export default GenericDatePicker;
