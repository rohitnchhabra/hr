import React from "react";
import ReactCSSTransitionGroup from "react-addons-css-transition-group";

export default function AlertFader(props) {
  return (
    <ReactCSSTransitionGroup
      transitionName="example"
      transitionEnterTimeout={5000}
      transitionLeaveTimeout={300}
    >
      {props.children}
    </ReactCSSTransitionGroup>
  );
}
