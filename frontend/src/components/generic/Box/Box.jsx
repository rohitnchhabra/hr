import React from "react";
import style from "./Box.module.scss";
import PropTypes from 'prop-types'
const Box = (props,key) => {
  return (
    <div className={`${style["box"]}`} key={key}>
      <div className={style["box-body"]}>{props.children}</div>
    </div>
  );
};

Box.prototype = {
  key:PropTypes.oneOfType([PropTypes.string,PropTypes.number]),
};

export default Box;
