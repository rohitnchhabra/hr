import React from "react";

import { withKnobs } from "@storybook/addon-knobs";

import Box from "./Box";

export default { title: "Box", decorators: [withKnobs] };

export const GenericBox = () => {
  return (
    <Box>
      <div>Box Data</div>
    </Box>
  );
};
