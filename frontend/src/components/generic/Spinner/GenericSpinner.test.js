import React from "react";

import {render} from "@testing-library/react";

import "@testing-library/jest-dom";

import Spinner from "./GenericSpinner";

test("spinner matches the snapshot", () => {
  const { container} = render(
    <Spinner/>
  );
  expect(container).toMatchSnapshot()
});
