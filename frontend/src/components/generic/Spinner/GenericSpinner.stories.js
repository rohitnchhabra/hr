import React from "react";
import { withKnobs} from "@storybook/addon-knobs";

import GenericSpinner from "./GenericSpinner";

export default { title: "Spinner", decorators: [withKnobs] };

export const Spinner = () => {
  return <GenericSpinner />;
};
