import React from "react";
import {Spinner} from 'reactstrap';
import PropTypes from 'prop-types';

const GenericSpinner = (props) => {
  return <Spinner {...props} size={props.size?props.size:"lg"} color={props.color?props.color:"white"}/>
};
GenericSpinner.prototype={
    className:PropTypes.oneOf(["p-0"]),
    size:PropTypes.oneOf(['sm',"lg"]),
    color:PropTypes.oneOf(['black','red','white','light'])
}
GenericSpinner.defaultProps={
    size:"lg",
    color:"white"
}


export default GenericSpinner;
