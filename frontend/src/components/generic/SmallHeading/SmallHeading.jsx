import React from "react";
import PropTypes from "prop-types";

export function SmallHeading({
  className,
  boldTextClassName,
  title,
  boldText,
}) {
  return (
    <h5 className={className ? className : ""}>
      {title}{" "}
      {boldText ? (
        <span className={boldTextClassName ? boldTextClassName : ""}>
          {boldText}
        </span>
      ) : null}
    </h5>
  );
}
SmallHeading.prototype = {
  title: PropTypes.string.isRequired,
  boldText: PropTypes.string,
  boldTextClassName: PropTypes.oneOf["font-weight-bold,fw-semi-bold"],
};

export function SmallHeadingWithBoldText({ boldTextClassName, ...props }) {
  return (
    <SmallHeading
      boldTextClassName={`font-weight-bold ${
        boldTextClassName ? boldTextClassName : ""
      }`}
      {...props}
    />
  );
}
SmallHeadingWithBoldText.prototype = {
  ...PropTypes.SmallHeading,
  boldText: PropTypes.string.isRequired,
};

export function SmallHeadingWithSemiBoldText({ boldTextClassName, ...props }) {
  return (
    <SmallHeading
      boldTextClassName={`fw-semi-bold ${
        boldTextClassName ? boldTextClassName : ""
      }`}
      {...props}
    />
  );
}
SmallHeadingWithSemiBoldText.prototype = {
  ...PropTypes.SmallHeading,
  boldText: PropTypes.string.isRequired,
};
export default SmallHeading;
