import React from "react";
import { withKnobs, text } from "@storybook/addon-knobs";

import SmallHeading, {
  SmallHeadingWithBoldText,
  SmallHeadingWithSemiBoldText,
} from "../../generic/SmallHeading/SmallHeading";

export default { title: "Small Heading", decorators: [withKnobs] };

export const SmallTitle = () => {
  return <SmallHeading title={text("label", "Small Heading")} />
};

export const SmallTitleWithBoldText = () => {
  return (
    <SmallHeadingWithBoldText
      title={text("label", "Heading")}
      boldText={"Bold text"}
    />
  );
};

export const SmallTitleWithSemiBoldText = () => {
  return (
    <SmallHeadingWithSemiBoldText
      title={text("label", "Heading")}
      boldText={"Semi Bold text"}
    />
  );
};
