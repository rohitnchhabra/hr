import React from "react";

import { render,queryByAttribute} from "@testing-library/react";

import "@testing-library/jest-dom";
import "@testing-library/jest-dom/extend-expect";

import SmallHeading, {
  SmallHeadingWithBoldText,
  SmallHeadingWithSemiBoldText,
} from "../../generic/SmallHeading/SmallHeading";

test("Small heading renders properly", () => {
  let heading = <SmallHeading title="Test Heading" />;
  const { getByText } = render(heading);
  const element = getByText("Test Heading");
  expect(element).toBeInTheDocument();
});

test("Small heading with Bold Text contains class", () => {
  const getByclassName = queryByAttribute.bind(null, "class");

  const dom = render(<SmallHeadingWithBoldText title="Test Heading" boldText="bold text" />);
  const boldText = getByclassName(dom.container.lastChild, "font-weight-bold");
  expect(boldText).toBeInTheDocument(true);
});

test("Small heading with Semi Bold Text contains class", () => {
  const getByclassName = queryByAttribute.bind(null, "class");

  const dom = render(<SmallHeadingWithSemiBoldText title="Test Heading" boldText="bold text" />);
  const boldText = getByclassName(dom.container.lastChild, "fw-semi-bold");
  expect(boldText).toBeInTheDocument(true);
});
