import React from "react";

export default function Heading({
  title,
  className,
  subHeading,
  boldText,
  cardHeader,
}) {
  if (subHeading) {
    return (
      <h5 className={className ? className : ""}>
        {title} <span className="font-weight-bold">{boldText}</span>
      </h5>
    );
  } else if (cardHeader) {
    return (
      <div className={className ? className : "pb-3"}>
        {title} <strong>{boldText}</strong>
      </div>
    );
  } else {
    return <h4 className={className ? className : ""}>{title}</h4>;
  }
}
