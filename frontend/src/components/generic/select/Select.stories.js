import React from "react";
import { withKnobs,text } from "@storybook/addon-knobs";

import Select, { SelectWithLabel } from "../../generic/select/Select";

export default { title: "Select", decorators: [withKnobs] };

export const select = () => {
  return (
    <Select>
      <option value="">--Select--</option>
      <option value="value1">value1</option>
      <option value="value2">value2</option>
      <option value="value3">value3</option>
    </Select>
  );
};
export const label = () => {
  return (
    <SelectWithLabel label={text("label", "label")}>
      <option value="">--Select--</option>
      <option value="value1">value1</option>
      <option value="value2">value2</option>
      <option value="value3">value3</option>
    </SelectWithLabel>
  );
};
