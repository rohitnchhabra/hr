import React from "react";

import {fireEvent, render} from "@testing-library/react";

import "@testing-library/jest-dom";

import Select from "./Select";

test("selct value is changed", () => {
  const mockChange = jest.fn();
  const { container, getByText} = render(
    <Select onChange={mockChange}>
      <option value="value1">value1</option>
    </Select>
  );
  fireEvent.focus(container.querySelector("select"));
  fireEvent.keyDown(container.querySelector("select"), {
    key: "ArrowDown"
  });
  const label=getByText("value1")
  fireEvent.click(getByText('value1'));
  expect(label.value).toBe("value1")
});
