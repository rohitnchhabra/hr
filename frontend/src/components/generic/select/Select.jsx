import React from "react";
import PropTypes from "prop-types";

function Select(props) {
  const { onChange, value, label } = props;
  return (
    <>
      {label ? <label>{label}</label>: null}
      <select className="form-control" onChange={onChange} value={value}>
        {props.children}
      </select>
    </>
  );
}

Select.prototype = {
  onChange: PropTypes.func,
  value: PropTypes.oneOf[(PropTypes.string, PropTypes.number)],
  label:PropTypes.string
};

export function SelectWithLabel(props) {
  return (
    <Select {...props} label={props.label}>
      {props.children}
    </Select>
  );
}

SelectWithLabel.prototype = {
  ...Select.PropTypes,
  label: PropTypes.string.isRequired,
};

export default Select;
