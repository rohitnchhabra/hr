import React from "react";
import { Card, CardBody } from "reactstrap";
import classnames from "classnames";

export default function GenricCardList({
  cardClassname,
  cardBodyClassName,
  onCardSelected,
  heading,
  subHeading,
  sec_Heading,
  request,
  ownershipRequest,
  forRequest,
  cardBodyImage,
  imageClassName,
  headingClassName
}) {
  return (
    <div>
      <Card className={classnames("mb-2",cardClassname)}>
        <CardBody onClick={onCardSelected} className={classnames("p-2",cardBodyClassName)}>
          <div className={headingClassName}>
            <h6 className="mb-0">{heading}</h6>
            <div>{subHeading}</div>
          </div>
          <div className={imageClassName}>{cardBodyImage}</div>
          {forRequest === "1" && !(request) ? <h6>Pending Unassign</h6>:""}
          {request === 1 ? <div>Request for Unassign</div>:""}
          {ownershipRequest === 1 ? <div>Request for Ownership Change</div>:""}
          {sec_Heading ? <div className="text-gray-dark ">{sec_Heading}</div> : null}
        </CardBody>
      </Card>
    </div>
  );
}
