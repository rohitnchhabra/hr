import React, { useState } from "react";
import * as a5 from "../../static/avatar.png";
import PropTypes from "prop-types";
import Dialog from "material-ui/Dialog";
import UserPicHeadTitle from "./UserPicHeadTitle";
import UploadImageComp from "../../modules/uploadImageCompressed/UploadImageComp";
import Dropzone from "react-dropzone";

const UserHorizontalView = ({
  profileImage,
  name,
  jobtitle,
  dateofjoining,
  gender,
  dob,
  workEmail,
  contactNumber,
  inventory,
  handleSubmit,
  handleFileChange,
  file,
  fileRef,
  dailogFlag,
  closeDailog,
  userId,
  ...props
}) => {
  let joining, details;
  if (!inventory) {
    joining = (
      <div className="userHorizontalDetails1">
        {" "}
        <div className="my-profile-details">
          <span className="text-secondary">Joining Date: </span>
          <span>{dateofjoining}</span>
        </div>
        <div className="my-profile-details">
          <span className="text-secondary"> Work Email:</span>{" "}
          <span className="work-email b">{workEmail}</span>
        </div>
        <div className="my-profile-details">
          <span className="text-secondary">Gender: </span>
          <span>{gender}</span>
        </div>
      </div>
    );
    details = (
      <div className="userHorizontalDetails2">
        <div className="my-profile-details">
          <span className="text-secondary"> Date Of Birth:</span>{" "}
          <span>{dob}</span>
        </div>
        <div className="my-profile-details">
          <span className="text-secondary"> Contact Number:</span>{" "}
          <span>{contactNumber}</span>
        </div>
      </div>
    );
  }

  let secondCol = () => {
    return (
      <div>
        {joining}
        {details}
      </div>
    );
  };

  let headTitleIcon = function() {
    return (
      <div className="dot-wrapper">
        <div className="green-dot" />
        <i class="glyphicon glyphicon-pencil pl-3" onClick={props.openDailog} />
      </div>
    );
  };

  return (
    <div class="editProfileTitle">
      <UserPicHeadTitle
        name={name}
        userId={userId}
        jobTitle={jobtitle}
        editTitleIcon={headTitleIcon}
        profileImage={`${profileImage}?${new Date().getTime()}`}
        details={details}
        secondCol={secondCol}
        isProfilePicLoading={props.isProfilePicLoading}
      />
    </div>
  );
};

UserHorizontalView.propTypes = {
  profileImage: PropTypes.string,
  name: PropTypes.string,
  jobtitle: PropTypes.string,
  dateofjoining: PropTypes.string,
  dob: PropTypes.string,
  gender: PropTypes.string,
  workEmail: PropTypes.string,
  inventory: PropTypes.bool
};
UserHorizontalView.defaultProps = {
  profileImage: null,
  name: null,
  jobtitle: null,
  dateofjoining: null,
  dob: null,
  gender: null,
  workEmail: null,
  inventory: false
};
export default UserHorizontalView;
