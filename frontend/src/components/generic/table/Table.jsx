import React, { Component } from "react";
import { Row, Col, Table } from "reactstrap";
import Widget from "../../generic/Widget/Widget";
import PropTypes from "prop-types";
import { Spinner } from "reactstrap";

export default class GenericTable extends Component {
  render() {
    const {
      tableListData,
      boldHeaderText,
      normalHeaderText,
      onChange,
      columnSize,
      headerData,
      list,
      onRowClick,
      rowStyle,
      noDataText,
      className,
      header2,
      show_loading_status,
      isLoading,
      widget_mobile,
      ...extraProps
    } = this.props;    
    return (
      <Row className="overflow-auto" className={this.props.row_Name}>
        <Col lg={columnSize} className={this.props.col_Name}>
          <Widget
          className={widget_mobile}
            title={
              normalHeaderText && (
                <h5>
                  {normalHeaderText}{" "}
                  {boldHeaderText && (
                    <span className="fw-semi-bold">{boldHeaderText}</span>
                  )}
                </h5>
              )
            }
          >
            <Table className={className} responsive>
              <thead>
                <tr
                  id={this.props.istable_head}
                  style={rowStyle ? rowStyle : {}}
                  className={extraProps.tableHeaderRowClass}
                >
                  {headerData &&
                    headerData.map((list, index) => {
                      return (
                        <th
                          key={index + list}
                          dangerouslySetInnerHTML={{ __html: list }}
                        />
                      );
                    })}
                </tr>
              </thead>
              <tbody className = {this.props.istable_body}> 
                {tableListData && tableListData.length > 0 ? (
                  tableListData.map((listItem, index) => {
                    return (
                      <tr
                        onClick={
                          onRowClick
                            ? () => onRowClick(listItem)
                            : () => {
                                return null;
                              }
                        }
                        key={`${index}-${listItem.name || listItem.id || ""}`}
                        style={rowStyle ? rowStyle : {}}
                        className={extraProps.tableBodyRowClass}
                      >
                        {list(
                          listItem,
                          onChange,
                          rowStyle ? rowStyle : {},
                          index,
                          extraProps
                        )}
                      </tr>
                    );
                  })
                ) : (
                  <tr>
                    <td
                      colSpan={headerData ? headerData.length : "12"}
                      className="text-center "
                    >
                      {tableListData && tableListData.length == 0 && isLoading ? <Spinner />:
                        "No Available Data"}
                    </td>
                  </tr>
                )}
              </tbody>
            </Table>
          </Widget>
        </Col>
      </Row>
    );
  }
}

GenericTable.propTypes = {
  tableListData: PropTypes.array.isRequired,
  headerData: PropTypes.array,
  onchange: PropTypes.func,
  columnSize: PropTypes.number.isRequired,
  normalHeaderText: PropTypes.string,
  boldHeaderText: PropTypes.string,
  list: PropTypes.func.isRequired,
  onRowClick: PropTypes.func,
  rowStyle: PropTypes.object
};
