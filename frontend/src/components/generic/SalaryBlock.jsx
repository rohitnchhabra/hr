import React from "react";
import { orderBy } from "lodash";
import moment from "moment";

const SalaryBlock = ({
  item,
  userid,
  displayPage,
  viewSalarySummary,
  callDeleteUserSalary,
  salaryStructure,
  callAddUserHolding,
}) => {
  const total_earning = item.total_earning;
  const total_deduction = item.total_deduction;
  const valueDecimal = val => {
    let v = Number(val);
    return Math.round(v) === v ? v : v.toFixed(2);
  };
  let isHolding = false;
  if (salaryStructure) {
    let orderedSalary = orderBy(
      salaryStructure.salary_details,
      ["date"],
      ["desc"]
    );
    let orderedHolding = orderBy(
      salaryStructure.holding_details,
      ["last_updated_on"],
      ["desc"]
    );
    if (orderedSalary.length > 0 && orderedHolding.length > 0) {
      isHolding =
        orderedSalary[0].date === item.date &&
        orderedHolding[0].holding_amt > 0;
    }
  }
  const handleRemoveHolding = () => {
    let params = {
      user_id: userid,
      holding_from: moment().format("YYYY-MM-DD"),
      holding_till: 12,
      holding_amount: "0",
      reason: "Holding removed using button click"
    };
    callAddUserHolding(params);
  };
  const value = item.total_net_salary;
  return (
    <div
      className="row salary-blocks-margin salary-row-bg mx-0 bg-white px-2"
      onClick={e =>
        displayPage === "manage" && viewSalarySummary(e, item.test.id)
      }
    >
      <div className="col-md-12 col-sm-12 salary-col-title-padding py-2">
      <div className ='d-flex justify-content-between mx-0 align-items-center admin-emp-saleies'>
        <div className="m-0 d-flex admin-slary-view">
        <div className="mb-0">
          <span className="salary-title">Applicable From: </span>{" "}
            <strong>
              {" "}
              {moment(item.test.applicable_from).format("DD-MMMM-YYYY") ||
                "--"}{" "}
            </strong>{" "}
        </div>
          <span className="divider mysalary_mobile">|</span>
          <div className="mb-0">
            <span className="salary-title"> Applicable Till: </span>{" "}
            <strong>
              {" "}
              {`${moment(item.test.applicable_till).format("DD-MMMM-YYYY") ||
                "--"}`}{" "}
            </strong>{" "}
          </div>
          
          <span className="divider mysalary_mobile">|</span>
          <div className="mb-0">
            <span className="salary-title"> Increment Amount: </span>{" "}
            <strong> {item.Increment_Amount || "--"} </strong>{" "}
          </div>
         
          <span className="divider mysalary_mobile">|</span>
          <div className="mb-0">
            <span className="salary-title"> Leaves Allocated: </span>{" "}
            <strong>{item.test.leaves_allocated || "--"} </strong>{" "}
          </div>
          <span className="divider mysalary_mobile">|</span>
          <div className="mb-0">
            <span className="salary-title"> Updated On: </span>{" "}
            <strong>
              {" "}
              {moment(item.test.last_updated_on).format("DD-MMMM-YYYY") ||
                "--"}{" "}
            </strong>
          </div>
          
        </div>
        <div className="mb-0">
            <span className="deduction_option remove-holding-btn-wrapper">
              {isHolding && (
                <span
                  className="remove-holding-btn"
                  onClick={() => handleRemoveHolding()}
                >
                  Remove Holding
                </span>
              )}
              {displayPage === "manage" ? (
                <i
                  className="material-icons delete-icon"
                  onClick={e =>
                    callDeleteUserSalary(e, item.test.user_Id, item.test.id)
                  }
                >
                  delete_forever{" "}
                </i>
              ) : (
                ""
              )}
            </span>
          </div>
        </div>
      </div>
      <div className="col-md-12 salary-col-padding salary-details p-0">
        {/* <div className="col-md-1 col-sm-2 col-xs-12 salary-total-width">
                    <div className="col-sm-12 salary-total-title"></div>
                    <div className="col-sm-12 salary-total-value"></div>
                </div> */}
        <div className="col-md-6 col-xs-12 salary-block pl-0 salary-addition-width">
          <div className="total-earning col-sm-12 p-0">
            <div className="total-earn-border col-sm-12 bg-success">
              TOTAL <span className="font-weight-bold"> EARNINGS </span>{" "}
            </div>
            {/* <div className="col-sm-12 salary-total-value">{total_earning}</div> */}
          </div>
          <div className="col-sm-12 detail-earning d-flex mb-2 px-0">
            <div className="d-flex justify-content-between px-4 py-2 border-btm ">
              <div className="salary-title">Basic</div>
              <div>{valueDecimal(item.Basic)}</div>
            </div>
            <div className="d-flex justify-content-between px-4 py-2 border-btm ">
              <div className="salary-title">HRA</div>
              <div>{valueDecimal(item.HRA)}</div>
            </div>
            <div className="d-flex justify-content-between px-4 py-2 border-btm">
              <div className="salary-title">Conveyance</div>
              <div>
                {valueDecimal(item.Conveyance)}
              </div>
            </div>
            <div className="d-flex justify-content-between px-4 py-2 border-btm">
              <div className="salary-title">Medical Allowance</div>
              <div>
                {valueDecimal(item.Medical_Allowance)}
              </div>
            </div>
            <div className="d-flex justify-content-between px-4 py-2 border-btm">
              <div className="salary-title">Special Allowance</div>
              <div>
                {valueDecimal(item.Special_Allowance)}
              </div>
            </div>
            <div className="d-flex justify-content-between px-4 py-2 ">
              <div className="salary-title">Arrears</div>
              <div>
                {valueDecimal(item.Arrears)}
              </div>
            </div>
          </div>
        </div>
        <div className="col-md-6 col-sm-12 col-xs-12 salary-block p-0">
          <div className="col-sm-12 p-0">
            <div className="col-sm-12 salary-total-title bg-danger">
              TOTAL <span className="font-weight-bold"> DEDUCTIONS </span>
            </div>
            {/* <div className="col-sm-12 salary-total-value">{total_deduction}</div> */}
          </div>
          <div className="col-sm-12 deduction-amount-detail px-0">
            {/* <div className="cell remove-holding-btn-wrapper">
                        { isHolding && <span className="remove-holding-btn" onClick={()=>handleRemoveHolding()}>Remove Holding</span>}
                        </div> */}

            {/* <div className="d-flex justify-content-between px-4 py-2 border-btm">
              <div className="salary-title">EPF</div>
              <div>{valueDecimal(item.EPF)}</div>
            </div>
            <div className="d-flex justify-content-between px-4 py-2 border-btm">
              <div className="salary-title">Loan</div>
              <div>{valueDecimal(item.Loan)}</div>
            </div>
            <div className="d-flex justify-content-between px-4 py-2 border-btm">
              <div className="salary-title">Advance</div>
              <div>
                {valueDecimal(item.Advance)}
              </div>
            </div> */}
            <div className="d-flex justify-content-between px-4 py-2 border-btm">
              <div className="salary-title">Misc Deductions</div>
              <div>
                {valueDecimal(item.Misc_Deductions)}
              </div>
            </div>
            {/* <div className="d-flex justify-content-between px-4 py-2 border-btm ">
              <div className="salary-title">TDS</div>
              <div>{valueDecimal(item.TDS)}</div>
            </div> */}
            <div className="d-flex justify-content-between px-4 py-2">
              <div className="salary-title">Holding Amount</div>
              <div>{valueDecimal(item.total_holding_amount)}</div>
            </div>

            <div className="bottom-part-deduction">
              {/* <div className=" remove-holding-btn-wrapper">
                        { isHolding && <span className="remove-holding-btn" onClick={()=>handleRemoveHolding()}>Remove Holding</span>}
                        </div> */}
              {displayPage === "manage" ? (
                <div className="col-sm-3 col-xs-12 cell center salary-options-width">
                  <div className="text-center">
                    {/* <i
                                        className="material-icons delete-icon"
                                        onClick={(e) => callDeleteUserSalary(e, item.test.user_Id, item.test.id)}
                                    >
                                        delete_forever
                                </i> */}
                  </div>
                </div>
              ) : (
                ""
                // <div className="col-sm-3 col-xs-12 cell">
                //     <div className="col-sm-12 salary-title">Holding Amount</div>
                //     <div className="col-sm-12 salary-holding-btn">
                //         <input type="text"
                //             className="col-md-6 col-sm-6"
                //         />
                //         <input type="button"
                //             className="col-md-6 col-sm-6 sm-btn md-raised info salary-add-holding"
                //             value="Add" />
                //     </div>
                // </div>
              )}
            </div>
          </div>
        </div>
      </div>
      <div className="total_value_detail mt-2 font-weight-bold">
        <div className="net-value py-2">
          <div>TOTAL EARNING</div>
          <div> {total_earning} </div>
        </div>
        <div className="net-value py-2">
          <div>TOTAL DEDUCTIONS</div>
          <div> {total_deduction} </div>
        </div>
        <div className="net-value py-2">
          <div>NET SALARY (RS.)</div>
          <div> {value} </div>
        </div>
      </div>
    </div>
  );
};

export default SalaryBlock;
