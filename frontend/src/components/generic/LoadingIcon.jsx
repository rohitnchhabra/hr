import React from 'react';
import PropTypes from 'prop-types';
import {Spinner} from "reactstrap";



const LoadingIcon = ({loading}) => {
  if (loading == true) {
    return (
      <div>
        {
          loading ? 
            <div className="generic-header-spinner">
              <Spinner color="red" className="p-0"/>
            </div>
          : null
        }
      </div>
    );
  }
  return (
    <div></div>
  );
};

LoadingIcon.propTypes = {
  loading: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.bool,
  ])
};

export default LoadingIcon;
