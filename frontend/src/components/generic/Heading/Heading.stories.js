import React from "react";
import { withKnobs, text} from "@storybook/addon-knobs";

import Heading,{HeadingWithText} from '../../generic/Heading/Heading'

export default { title: "Heading", decorators: [withKnobs] };

export const Title = () => {
  return <Heading title={text('label',"Heading")}/>
};
export const TitleWithText = () => {
    return <HeadingWithText title={text('label',"Heading")} text={'text'}/>
  };
