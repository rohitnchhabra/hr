import React from "react";
import PropTypes from "prop-types";
import styles from "./Heading.module.scss";

export default function Heading(props) {
  const classes = props.className ? props.className.split(" ") : "";

  return (
    <h4
      className={`${
        props.className
          ? classes.map((c, i) => {
              return styles[classes[i]]+ " ";
            })
          : styles["medium-head"]
      } ${styles["heading"]}`}
    >
      {props.title}
    </h4>
  );
}
Heading.prototype = {
  className: PropTypes.oneOf[("medium-head", "small-head", "mb-4")],
  title: PropTypes.string.isRequired,
};
Heading.defaultProps = {
  className: "medium-head",
};
export function HeadingWithText(props) {
  return (
    <>
      <p>{props.text}</p>
      <Heading
        title={props.title}
        className={props.className ? props.className : ""}
      />
    </>
  );
}
HeadingWithText.prototype = {
  ...PropTypes.Heading,
  text: PropTypes.string.isRequired,
};
