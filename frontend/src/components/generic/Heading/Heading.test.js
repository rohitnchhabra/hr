import React from "react";

import { render } from "@testing-library/react";

import "@testing-library/jest-dom";
import "@testing-library/jest-dom/extend-expect";

import Heading from "../../generic/Heading/Heading";

test("heading renders properly", () => {
  let heading = <Heading title="Test Heading" />;
  const { getByText } = render(heading);
  const element = getByText("Test Heading");
  expect(element).toBeInTheDocument();
});
