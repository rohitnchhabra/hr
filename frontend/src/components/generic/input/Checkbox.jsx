import React from "react";
import classnames from "classnames";
import PropTypes from "prop-types";
export default function Checkbox({
  checked,
  onClick,
  name,
  disabled,
  ...props
}) {
  function onCheckBoxClick() {
    onClick(name);
  }
  return (
    <div className={classnames("form-check mb-sm",{disabled})} id="top">
      <input
        className="custom-checkbox"
        type="checkbox"
        checked={checked}
        {...props}
      />
      <span
        className={classnames("checkmark", { "box-border": !checked === true })}
        onClick={(e)=>{
          e.stopPropagation();
          onCheckBoxClick();}}
      />
    </div>
  );
}

Checkbox.defaultProps = {
  disabled: false
};

Checkbox.propTypes = {
  /**For managing status of checkbox, checked or not */
  checked: PropTypes.bool.isRequired,
  /**Identifier for Checkbox */
  name: PropTypes.oneOfType([PropTypes.string, PropTypes.number]).isRequired,
  /**Function which will be called on checkbox click */
  onClick: PropTypes.func.isRequired,
  /**Boolean value for disabling checkbox */
  disabled: PropTypes.bool
};
