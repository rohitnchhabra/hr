import React from "react";
import PropTypes from "prop-types";

const InputText = ({ className,required, ref,type,maxlength,pattern,title, placeHolder, onchange, id, name, value, style, disabled }) => {
  let classname = 'form-control ' + className;
  return (
    <input
      type={type?type:'text'}
      title={title}
      pattern={pattern}
      maxlength={maxlength}
      className={classname}
      ref={ref}
      placeholder={placeHolder}
      onChange={onchange}
      id={id}
      required={required?required:false}
      name={name}
      value={value}
      style={style}
      disabled={disabled || ""}
    />
  );
};

export default InputText;

InputText.propTypes = {
  className: PropTypes.string,
  placeHolder: PropTypes.string.isRequired,
  onchange: PropTypes.func.isRequired,
  id: PropTypes.number,
  name: PropTypes.string,
  value: PropTypes.string,
  style: PropTypes.string
};
