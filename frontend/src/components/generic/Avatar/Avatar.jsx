import React from "react";
import style from "./Avatar.module.scss";
import Spinner from "../Spinner/GenericSpinner";
import * as a5 from "../../../static/avatar.png";
import PropTypes from 'prop-types';
const Avatar = ({
  size,
  isProfilePicLoading,
  profileImage,
  imageClassName
}) => {
  return (
    <div className={`${style["avatar"]} ${size ? style[size] : style["medium"]}`}>
      {isProfilePicLoading ? (
        <Spinner />
      ) : (
        <img
          src={profileImage ? profileImage : a5}
          className={imageClassName ? imageClassName : ""}
          alt={'profileImage'}
        />
      )}
    </div>
  );
};

Avatar.prototype={
    imageClassName:PropTypes.oneOf(['rounded-circle,border-success,border-warning']),
    isProfilePicLoading:PropTypes.boolean,
    profileImage:PropTypes.string,
    size:PropTypes.oneOf(['tiny,small,medium'])

}
Avatar.deafaultProps={
    isProfilePicLoading:false,
    profileImage:a5,
    size:"medium"
}

export default Avatar;
