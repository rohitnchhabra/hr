import React from "react";

import { withKnobs,boolean } from "@storybook/addon-knobs";

import Avatar from "./Avatar";

export default { title: "Avatar", decorators: [withKnobs] };

export const GenericAvatar = () => {
  return (
    <Avatar isProfilePicLoading={boolean("isProfilePicLoading", false)}/>
  );
};

