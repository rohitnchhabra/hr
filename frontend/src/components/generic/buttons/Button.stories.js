import React from 'react';

import { action } from '@storybook/addon-actions';
import { withKnobs, text, boolean } from "@storybook/addon-knobs";

import Button,{ButtonFlat,ButtonRaised,ButtonInfo,ButtonDark,ButtonSuccess ,ButtonPrimary} from "./Button"

export default { title: 'Buttons',  decorators: [withKnobs] };


export const SimpleButton = () => {

    return (
        <Button onClick={action("clicked")} disabled={boolean("Disabled", false)} id="btn">
            {text('label',"Simple Button")}
        </Button>
    )
}
export const FlatButton = () => {
    return (
        <ButtonFlat onClick={action("clicked")} disabled={boolean("Disabled", false)} id="btn-flat">
            {text('label',"Flat Button")}
        </ButtonFlat>
    )
}
export const RaisedButton = () => {
    return (
        <ButtonRaised onClick={action("clicked")} disabled={boolean("Disabled", false)} id="btn-raised">
            {text('label',"Raised Button")}
        </ButtonRaised>
    )
}
export const InfoButton = () => {
    return (
        <ButtonInfo onClick={action("clicked")} disabled={boolean("Disabled", false)} id="btn-info">
        {text('label',"Info Button")}
        </ButtonInfo>
    )
}
export const DarkButton = () => {
    return (
        <ButtonDark onClick={action("clicked")} disabled={boolean("Disabled", false)} id="btn-dark">
        {text('label',"Dark Button")}
        </ButtonDark>
    )
}
export const SuccessButton = () => {
    return (
        <ButtonSuccess onClick={action("clicked")} disabled={boolean("Disabled", false)} id="btn-success">
        {text('label',"Success Button")}
        </ButtonSuccess>
    )
}
export const PrimaryButton = () => {
    return (
        <ButtonPrimary onClick={action("clicked")} disabled={boolean("Disabled", false)} id="btn-primary">
        {text('label',"Primary Button")}
        </ButtonPrimary>
    )
}