import React from 'react'

import { render} from '@testing-library/react'

import '@testing-library/jest-dom'
import userEvent from '@testing-library/user-event'
import '@testing-library/jest-dom/extend-expect'


import Button,{ButtonFlat,ButtonInfo,ButtonRaised, ButtonDark} from "./Button"

test('button renders properly', () => {

    const mockClick = jest.fn()

    let button = <Button onClick={mockClick} id="btn">Test Button</Button>
    const { getByText } = render(button)
    const element = getByText("Test Button")
    expect(element).toBeInTheDocument()

    userEvent.click(element)

    expect(mockClick.mock.calls.length).toBe(1)

})

test('raised button contains class and renders properly', () => {

    const mockClick = jest.fn()

    let button = <ButtonRaised onClick={mockClick} id="btn">Test Button</ButtonRaised>
    const { getByText } = render(button)
    const element = getByText("Test Button")
    expect(element).toBeInTheDocument()
    expect(element.classList.contains('md-raised')).toBe(true)

})


test('flat button contains class and renders properly', () => {

    const mockClick = jest.fn()

    let button = <ButtonFlat onClick={mockClick} id="btn">Test Button</ButtonFlat>
    const { getByText } = render(button)
    const element = getByText("Test Button")
    expect(element).toBeInTheDocument()
    expect(element.classList.contains('md-flat')).toBe(true)


})
test('info button contains class renders properly', () => {

    const mockClick = jest.fn()

    let button = <ButtonInfo onClick={mockClick} id="btn">Test Button</ButtonInfo>
    const { getByText } = render(button)
    const element = getByText("Test Button")
    expect(element).toBeInTheDocument()
    expect(element.classList.contains('btn-info')).toBe(true)
})

test('dark button contains class renders properly', () => {

    const mockClick = jest.fn()

    let button = <ButtonDark onClick={mockClick} id="btn">Test Button</ButtonDark>
    const { getByText } = render(button)
    const element = getByText("Test Button")
    expect(element).toBeInTheDocument()
    expect(element.classList.contains('btn-inverse')).toBe(true)

})