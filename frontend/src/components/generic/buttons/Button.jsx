import React from 'react';
import PropTypes from 'prop-types';

const Button = ({id, type, style,className, onClick,disabled, helperClass,...props}) => {
  let classname=helperClass?helperClass:`btn ${className?className:''}`
  return (
    <button id={id} type={type} className={classname} style={style} onClick={onClick} disabled={disabled ? disabled :false }   {...props} >{props.children}</button>
  );
};

Button.propTypes = {
  id: PropTypes.string.isRequired,
  type: PropTypes.string,
  style: PropTypes.object,
  className: PropTypes.string,
  onClick: PropTypes.func,
  disabled:PropTypes.bool
  
};
Button.defaultProps = {
  disabled:false,
  type:'button'
}

export const ButtonFlat = ({className,...props}) => {
  return (
    <Button helperClass={`md-btn md-flat ${className?className:''}`} {...props}>{props.children}</Button>
  )
}

ButtonFlat.propTypes = {
  ...Button.PropTypes
}
ButtonFlat.defaultProps = {
 ...Button.defaultProps
}


export const ButtonInfo = ({className,...props}) => {
  return (
    <Button helperClass={`btn btn-info ${className?className:''}`} {...props}>{props.children}</Button>
  )
}

ButtonInfo.propTypes = {
  ...Button.PropTypes
}
ButtonInfo.defaultProps = {
  ...Button.defaultProps
 }

export const ButtonRaised = ({className,...props}) => {
  return (
    <Button helperClass={`md-btn md-raised ${className?className:''}`} {...props}>{props.children}</Button>
  );
};

ButtonRaised.propTypes = {
  ...Button.PropTypes
};

ButtonRaised.defaultProps = {
  ...Button.defaultProps
 }
 export const ButtonDark = ({className,...props}) => {
  return (
    <Button helperClass={`btn btn-inverse ${className?className:''}`} {...props}>{props.children}</Button>
  );
};

ButtonDark.propTypes = {
  ...Button.PropTypes
};

ButtonDark.defaultProps = {
  ...Button.defaultProps
 }
 export const ButtonSuccess = ({className,...props}) => {
  return (
    <Button helperClass={`btn btn-success ${className?className:''}`} {...props}>{props.children}</Button>
  );
};

ButtonSuccess.propTypes = {
  ...Button.PropTypes
};

ButtonSuccess.defaultProps = {
  ...Button.defaultProps
 }

 export const ButtonPrimary = ({className,...props}) => {
  return (
    <Button helperClass={`btn btn-primary ${className?className:''}`} {...props}>{props.children}</Button>
  );
};

ButtonPrimary.propTypes = {
  ...Button.PropTypes
};

ButtonPrimary.defaultProps = {
  ...Button.defaultProps
 }


export default Button;
