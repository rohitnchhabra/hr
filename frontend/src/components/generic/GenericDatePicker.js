import React, { Component } from 'react'
import 'date-fns';
import DateFnsUtils from '@date-io/date-fns';
import {
  MuiPickersUtilsProvider,
  KeyboardDatePicker,
  DatePicker
} from '@material-ui/pickers';

export default class GenericDatePicker extends Component {
    state={
            isOpen:false
        }

    handleDateChange = (date) => {
        const {dob} = this.props;
        this.props.handleDateChange(date);
        if(!dob){
            this.setState({isOpen:false})
        }
    };

    handleModal=()=>{
        if(!this.state.isOpen){
            this.onOpen()
        }
    }

    onOpen=()=>{
        this.setState({isOpen:true})
    }

    onClose=()=>{
        this.setState({isOpen:false})
    }
    render() {
        const {isOpen} = this.state;
        const {pickerProviderClass, gridClass, datePickerClass,type,date, emptyLabel, notClearable} = this.props;
        return (
            <div id="date-picker" onClick={this.handleModal} className={this.props.className}>
                <MuiPickersUtilsProvider  className={`picker-provider ${pickerProviderClass}`} utils={DateFnsUtils}>
                    <div container className={`picker-grid ${gridClass}`} justify="space-around">
                        {type==="absolute"?
                            <KeyboardDatePicker
                                emptyLabel={(date ==null && this.props.hasOwnProperty("emptyLabel"))? emptyLabel :''}
                                className={`keyboard-date-picker ${datePickerClass}`}
                                style={{zIndex:1600}}
                                margin="normal"
                                id="date-picker-dialog"
                                format="dd/MM/yyyy"
                                views={["year", "month", "date"]}
                                open={isOpen}
                                value={date}
                                onOpen={this.onOpen}
                                onClose={this.onClose}
                                onChange={this.handleDateChange}
                                // KeyboardButtonProps={{
                                //     'aria-label': 'change date',
                                // }}
                            />:
                            <DatePicker
                                emptyLabel={(date ==null && this.props.hasOwnProperty("emptyLabel"))? emptyLabel :''}
                                open={isOpen}
                                clearable={notClearable ? false : true}
                                clearLabel={"Clear"}
                                disableToolbar={this.props.hasOwnProperty("dob") ? false : true}
                                disableFuture={this.props.hasOwnProperty("dob") ? true : false}
                                views={["year", "month", "date"]}
                                autoOk={true}
                                className={`keyboard-date-picker ${datePickerClass}`}
                                margin="normal"
                                id="date-picker-dialog" 
                                format="dd/MM/yyyy"
                                value={date}
                                onOpen={this.onOpen}
                                onClose={this.onClose}
                                onChange={(date)=>this.handleDateChange(date)}
                                // KeyboardButtonProps={{
                                //     'aria-label': 'change date',
                                // }}
                            />
                        }
                </div>
            </MuiPickersUtilsProvider>
        </div>
        )
    }
}
