import React from "react";
import PropTypes from "prop-types";
// import { ButtonInfo, Button } from "../../../components/generic/buttons";
import {useEffect} from "react";
import $ from 'jquery';

const RHLeavesList = ({ leave, handleApplyClick,displayHeight,length }) => {
  var height = $("#get_ht").height()+24;
  useEffect(()=>{
        displayHeight((length > 5 ? 5 : length)*height);
    },[height])

  let l = leave.type;
  let f_char = l.charAt(0);
  let leaveStatusColor = "";
  if (l === "Restricted") {
    leaveStatusColor = "green-A200";
  } else if (l === "Sick Leave") {
    leaveStatusColor = "blue";
    // } else if (s === "Rejected") {
    //   leaveStatusColor = "red-500";
    // } else if (s === "Cancelled Request") {
    //   leaveStatusColor = "red-100";
  } else {
    leaveStatusColor = "green";
  }
  return (
    <div id ="get_ht" className={`list-item b-l b-l-2x b-${leaveStatusColor}`}>
      <div className="list-left">
        <span className={`w-40 avatar ${leaveStatusColor}`}>{f_char}</span>
      </div>
      <div className="list-body">
        <div className="col-xs-8">
          <div>
            <span className="_500">Holiday : {leave.name}</span>
          </div>
          <div>
            <span className="_500">Date : {leave.date}</span>
          </div>
        </div>
        <div>
          {!leave.status && new Date(leave.date) >= new Date() && (
            <div className="save-btn">
              <button
                type="button"
                className={`btn btn-primary`}
                onClick={() => {
                  handleApplyClick(leave);
                }}
              >
                Apply
              </button>
            </div>
          )}
          {leave.status && (
            <div className={`status_text text-${leaveStatusColor}`}>
              <b>{leave.status}</b>
            </div>
          )}
        </div>
      </div>
    </div>
  );
};

export default RHLeavesList;
