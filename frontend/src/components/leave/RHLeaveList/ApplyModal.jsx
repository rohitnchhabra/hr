import React, { Component } from "react";
import { Button, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';

export default class ApplyRHModal extends Component {
  render() {
    const {
      show,
      handleHide,
      onApplyRHLeave,
      inputValue,
      stateData
    } = this.props;
    return (
      <Modal
        isOpen={show} toggle={handleHide} id="termination-modal" className={"termination-modal entity-page-cancel-modal"}
      >
        <ModalHeader>
          <div className="rh-modal">
            <div>{`RH Name : ${stateData.currentRH &&
              stateData.currentRH.name}`}</div>
            <div>{`Date : ${stateData.currentRH &&
              stateData.currentRH.date}`}</div>
            <div>{`Day : ${stateData.currentRH &&
              stateData.currentRH.day}`}</div>
          </div>
        </ModalHeader>
        <ModalBody>
          <div className="form-group">
            <label htmlFor="reason">Reason : </label>
            <input
              type="text"
              className="form-control"
              id="reason"
              value={inputValue}
              onChange={this.props.inputChange}
              placeholder="Enter reason here"
              required={true}
            />
            {stateData.showError && (
              <div id="error-text">* This field is required</div>
            )}
          </div>
        </ModalBody>
        <ModalFooter>
          <div className="apply-btn">
            <button
              type="button"
              className={`btn btn-primary`}
              onClick={onApplyRHLeave}
            >
              Apply
            </button>
            <button
              type="button"
              className="btn btn-default"
              onClick={handleHide}
            >
              Close
            </button>
          </div>
        </ModalFooter>
      </Modal>
    );
  }
}
