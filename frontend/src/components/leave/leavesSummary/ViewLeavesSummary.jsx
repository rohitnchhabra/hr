import React from "react";
import _ from "lodash";
import PropTypes from "prop-types";
import LeavesSummary from "./LeavesSummary";
import DayReference from "../../../components/attendance/DayReference";

const ViewLeavesSummary = ({ componentData, on_all_leaves_summary }) => {
  const _onChangeMonth = check => {
    if (check == "previous") {
      on_all_leaves_summary(
        componentData.previousMonth.year,
        componentData.previousMonth.month
      );
    } else if (check == "next") {
      on_all_leaves_summary(
        componentData.nextMonth.year,
        componentData.nextMonth.month
      );
    }
  };
  let current_year =
    typeof componentData.year != "undefined" ? componentData.year : "";
  let current_monthName =
    typeof componentData.monthName != "undefined"
      ? componentData.monthName
      : "";
  let summaryHtml = _.map(
    componentData.leavesSummary,
    (userLeaveSummary, key) => {
      return <LeavesSummary key={key} user={userLeaveSummary} />;
    }
  );
  return (
    <div className="app-body" id="view">
      <div>
        <div id="content" className="app-content box-shadow-z0" role="main">
          <div className="app-body" id="view">
            <div>
              <div className="fullcalendar fc fc-ltr fc-unthemed">
                <div className="fc-toolbar">
                  <div className="fc-left">
                    <button
                      type="button"
                      className="fc-prev-button fc-button fc-state-default fc-corner-left fc-corner-right"
                      onClick={() => _onChangeMonth("previous")}
                    >
                      <span className="fc-icon fc-icon-left-single-arrow" />
                    </button>
                  </div>
                  <div className="fc-right">
                    <button
                      type="button"
                      className="fc-next-button fc-button fc-state-default fc-corner-left fc-corner-right"
                      onClick={() => _onChangeMonth("next")}
                    >
                      <span className="fc-icon fc-icon-right-single-arrow" />
                    </button>
                  </div>
                  <div className="fc-center">
                    <h2>
                      {current_monthName}-{current_year}
                    </h2>
                  </div>
                  <div className="fc-clear" />
                </div>
                <br />
                <DayReference
                 leaveReference = {true}
                 content={["Pending", "Rejected", "Approved","Time Missing"]}
                 header = "Leave Reference"
                />
                <DayReference
                 leaveReference = {true}
                 content={["CL - CASUAL LEAVE ", "SL - SICK LEAVE", "RHC - RH COMPENSATION", "TM - Entry/Exit Time Missing"]}
                 header = "Abbreviation"
                 abbreviation ={true}
                />
                <div className="fc-view fc-month-view fc-basic-view">
                  {summaryHtml.length ? (
                    summaryHtml
                  ) : (
                    <div className="view-leaves-loader">
                      {/* <Spinner color="primary" size="lg" /> */}
                      <div>
                        <h6>
                          {" "}
                          <i>
                            *This page show leave summary for all employee's on
                            a single page . Depending on the number of your
                            employee this can take Up-To 1-minutes to load.
                          </i>
                        </h6>
                      </div>
                    </div>
                  )}
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

ViewLeavesSummary.propTypes = {
  componentData: PropTypes.shape({
    leavesSummary: PropTypes.array.isRequired,
    month: PropTypes.number,
    monthName: PropTypes.string,
    username: PropTypes.string,
    previousMonth: PropTypes.object.isRequired,
    nextMonth: PropTypes.object.isRequired,
    year: PropTypes.number
  }).isRequired,
  on_all_leaves_summary: PropTypes.func.isRequired
};

export default ViewLeavesSummary;
