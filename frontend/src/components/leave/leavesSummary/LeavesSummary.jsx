import React from "react";
import PropTypes from "prop-types";
import _ from "lodash";
import moment from "moment";

const LeavesSummary = ({ user }) => {
  let pendingPunchingDays = _.map(user.attendance, (day, key) => {
    let checkClass = "";
    let leaveType = "";
    if (day.leave_status === "pending") {
      checkClass = "b-warn";
    } else if (day.leave_status === "rejected") {
      checkClass = "b-danger";
    } else if (day.leave_status === "approved") {
      checkClass = "b-success";
    }
    if (day.day_type === "WORKING_DAY") {
      if (day.in_time === "" || day.out_time === "") {
        checkClass = "bgg-white";
        leaveType = "TM";
      }
    }
    if (day.day_type === "LEAVE_DAY") {
      if (day.leave_type.toLowerCase() === "casual leave") {
        leaveType = "CL";
      } else if (day.leave_type.toLowerCase() === "restricted") {
        leaveType = "RHC";
      } else if (day.leave_type.toLowerCase() === "sick leave") {
        leaveType = "SL";
      }
    }
    let showText = "";
    if (day.day_type === "HALF_DAY") {
      if (day.leave_type.toLowerCase() === "casual leave") {
        leaveType = "CL";
      } else if (day.leave_type.toLowerCase() === "restricted") {
        leaveType = "RHC";
      } else if (day.leave_type.toLowerCase() === "sick leave") {
        leaveType = "SL";
      }
      showText = "Half Day";
    }
    return (
      <div key={key} className={`user-leave-view-container ${checkClass}`}>
        <div>

        <div className={`leave-type-circle ${checkClass}`}>{leaveType}</div>
        {showText ? (
          <div className={`red-100-text`}>HD</div>
        ) : null}
        </div>

        <div className="details-sl-content">
          <div>{moment(day.display_date).format("LL")}</div>
          <div style={{ "text-overflow": "ellipsis", overflow: "hidden" }}>
            {day.day_text}
          </div>
        </div>
      </div>
    );
  });
  return (
    <React.Fragment>
      <div
        className="col-sm-4 col-md-3 col-xs-12 pl-0 pr-2 "
      >
        <div className="box box-responsive-5 scroll">
          <div className="box-header user-image">
            <div>
              <img
                style={{ width: "40px", borderRadius: "50%" }}
                alt="user-img"
                src={
                  user && user.profileImage
                    ? user.profileImage
                    : "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTEge-PiQqrFDwU1Us5-Y_4q2ckub2VaUQFI9_YCbAkfefclr1b"
                }
              />
            </div>
            <div className="user-details-container">
              <h3>{user.name}</h3>
              <small>{user.jobtitle}</small>
            </div>
          </div>
          <div className="box-body leave-details-view">
            <div className="streamline">{pendingPunchingDays}</div>
          </div>
        </div>
      </div>
    </React.Fragment>
  );
};

LeavesSummary.propTypes = {
  user: PropTypes.shape({
    attendance: PropTypes.array.isRequired,
    name: PropTypes.string.isRequired,
    jobtitle: PropTypes.string.isRequired
  }).isRequired
};

export default LeavesSummary;
