import React from "react";
import * as _ from "lodash";
import GenericTable from "../../generic/table/Table";

const rhStatsList = (listItem, onChange, rowStyle, index, extraProps) => {
  return (
    <>
      <td>{listItem.user_id}</td>
      <td>
        {listItem.name}
        <br />
        <span className="job-title">{listItem.designation}</span>
      </td>
      <td>{listItem.stats.rh_can_be_taken}</td>
      <td>
        <div>{listItem.stats.rh_approved}</div>
        <div className="rh_leave_names">
          {listItem.stats.rh_approved_names &&
            listItem.stats.rh_approved_names.length > 0 &&
            listItem.stats.rh_approved_names.map((val, i) => <div>{val}</div>)}
        </div>
      </td>
      <td>
        <div>{listItem.stats.rh_rejected}</div>
        <div className="rh_leave_names">
          {listItem.stats.rh_rejected_names &&
            listItem.stats.rh_rejected_names.length > 0 &&
            listItem.stats.rh_rejected_names.map((val, i) => <div>{val}</div>)}
        </div>
      </td>
      <td>{listItem.stats.rh_left}</td>
      <td>
        <div>{listItem.stats.rh_compensation_used}</div>
        <div className="rh_leave_names">
          {listItem.stats.rh_compensation_used_names &&
            listItem.stats.rh_compensation_used_names.length > 0 &&
            listItem.stats.rh_compensation_used_names.map((val, i) => <div>{val}</div>)}
        </div>
      </td>
      <td>{listItem.stats.rh_compensation_pending}</td>
    </>
  );
};

class RHStatList extends React.Component {
  render() {
    const { yearArray } = this.props;
    return (
      <div>
        <div className="rhStatsList">
          <GenericTable
            list={rhStatsList}
            headerData={[
              "Employee ID ",
              "Name",
              "Total RH Available",
              "Approved RH",
              "Cancelled RH",
              "Pending RH",
              "RH Compensation Used",
              "Pending RH Compensation"
            ]}
            tableListData={this.props.rhStatsList}
            isLoading={this.props.isRHLoading}
            widget_mobile={"rh-mobileTable"}
          />
        </div>
      </div>
    );
  }
}

export default RHStatList;
