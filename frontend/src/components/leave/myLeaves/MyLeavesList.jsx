import React, {useEffect,useState} from "react"; 
import PropTypes from "prop-types";
import $ from 'jquery';
import Dropzone from "react-dropzone";
import UploadImageComp from "../../../modules/uploadImageCompressed/UploadImageComp";
const MyLeavesList = ({ leave, handleOpen, cancelLeave, length , ...props}) => {
  const [open, setOpen] = useState(false);

 const handleClose = ()=>{
    setOpen(!open)
  }
  
  let leaveStatusColor =
    leave.leave_type.toLowerCase() === "restricted"
      ? "restricted"
      : leave.leave_type.toLowerCase() === "casual leave"
      ? "casualLeave"
      : leave.leave_type.toLowerCase() === "sick leave"
      ? "sickLeave"
      : null;

  let leaveBorderColor =
    leave.leave_type.toLowerCase() === "restricted"
      ? "restrictedBorder"
      : leave.leave_type.toLowerCase() === "casual leave"
      ? "casualLeaveBorder"
      : leave.leave_type.toLowerCase() === "sick leave"
      ? "sickLeaveBorder"
      : null;

  return (
    <div id="getd" className={`list-item b-l b-l-2x ${leaveBorderColor}`}>
      <div className="list-left">
        <span className={`w-40 avatar ${leaveStatusColor}`}>
          {leave.leave_type.toLowerCase() === "restricted"
            ? "RH"
            : leave.leave_type.toLowerCase() === "casual leave"
            ? "CL"
            : leave.leave_type.toLowerCase() === "sick leave"
            ? "SL"
            : null}
        </span>
      </div>
      <div className="list-body">
        <div className="pull-right text-muted text-xs" />
        <div>
          <h3 className="_500">
            <b>
              {leave.leave_type.toLowerCase() === "restricted"
                ? "Reserved Holiday"
                : leave.leave_type.toLowerCase() === "casual leave"
                ? "Casual Leave"
                : leave.leave_type.toLowerCase() === "sick leave"
                ? "Sick Leave"
                : null}
            </b>
          </h3>
        </div>
        {/* <div>
          <span className="_500">Status : {leave.status}</span>
        </div> */}
        <div>
          <span className="_500">
            From {leave.from_date} to {leave.to_date}
          </span>
          &nbsp;&nbsp;&nbsp;
          <span className="label cyan">{leave.no_of_days} Day</span>
          {leave.status === "Pending" && (
            <span
              className="label cyan m-l-xs"
              id="cancel-cursor"
              onClick={() => cancelLeave(leave.user_Id, leave.from_date)}
            >
              {" "}
              Cancel{" "}
            </span>
          )}
        </div>
        <div>
          <span className="_500">Apply on : {leave.applied_on}</span>
        </div>
        <div className="text-ellipsis text-muted text-sm">
          Status : {leave.status}
        </div>
        <div className="text-ellipsis text-muted text-sm">
          Reason : {leave.reason}
        </div>
        {leave.leave_type !== "" ? (
          <div className="text-ellipsis text-muted text-sm">
            Leave Type : {leave.leave_type}
          </div>
        ) : null}
        {leave.late_reason !== "" ? (
          <div className="text-ellipsis text-muted text-sm">
            Reason For Late Applying : {leave.late_reason}
          </div>
        ) : null}
        {leave.comment !== "" ? (
          <div className="text-ellipsis text-muted text-sm">
            comment : {leave.comment}
          </div>
        ) : null}
      </div>

      <div className="row">
      {/* when admin click notify for document then it will 1 other wise default value is 0 */}
        {leave.doc_require == "0" || leave.doc_require == "1"  ? (
          <div className="col-xs-12 row">
            <button
              onClick={() => {
                handleOpen(leave.id);
                setOpen(!open)
              }}
              className="btn btn-info btn-responsive btn-res col-xs-2 mt-0 p-1 ml-5 mr-2"
            >
              {"Upload Leave Document"}
            </button>
            {leave.doc_link === "" || leave.doc_link === "N/A" ? null : (
              <div>
                <form method="get" target="_blank" action={leave.doc_link}>
                  <button className="btn btn-primary btn-responsive btn-res col-xs-2 p-1">
                    {"View Document"}
                  </button>
                </form>
              </div>
            )}
            <div className="col-xs-12 row">
              <small className="uploadnotic ml-5">
                * Upload file size should be less than 1 MB
              </small>
            </div>
          </div>
        ) : null}
        {open ?
      <div className="m-auto pt-5 w-75">
        <Dropzone onDrop={props.handleFileChange}>
          {({ getRootProps, getInputProps }) => {
            return (
              <section className="container m-0 p-0 col-sm-12">
                <div {...getRootProps({ className: "dropzone" })}>
                  <input {...getInputProps()} />
                  <div className="drag_and_drop align-items-center">
                    {props.file &&
                      props.file.map((file, index) => (
                        <div className="font-weight-bold"> {file.name}</div>
                      ))}
                    {props.imageUrl ? (
                      <img
                        src={props.imageUrl}
                        className="mx-3 img-fluid profile-doc-preview"
                        alt="Preview"
                      />
                    ) : (
                      <p className="uploading_doc">
                        <i className="fi flaticon-upload" />
                      </p>
                    )}

                    <p className="doc_upload_place">
                      Drop a document here or click to select file to upload
                    </p>
                  </div>
                </div>
              </section>
            );
          }}
        </Dropzone>
        <div>
          <UploadImageComp
            validateDocuments={props.callUpdateDocuments}
            fileParams={{
              file: props.file,
              document_type: "leave_doc",
              token: props.user_token,
              type: props.type,
              imageUrl: props.imageUrl,
              leaveid:props.id,
              file_upload_action: 'upload_leave_document'
            }}
            url={"generic_upload_url"}
            onSuccessfulFileUpload={props.successfulFileUpload}
            handleRomoveImage={props.handleRomoveImage}
            handleClose={handleClose}
            showSuccessMessage={props.showSuccessMessage}
          />
        </div>
      </div> :''}
      </div>
    </div>
  );
};

MyLeavesList.propTypes = {
  leave: PropTypes.shape({
    status: PropTypes.string.isRequired,
    applied_on: PropTypes.string.isRequired,
    from_date: PropTypes.string.isRequired,
    to_date: PropTypes.string.isRequired,
    no_of_days: PropTypes.number.isRequired,
    user_Id: PropTypes.number.isRequired,
    reason: PropTypes.string.isRequired,
    leave_type: PropTypes.string.isRequired,
    late_reason: PropTypes.string.isRequired,
    comment: PropTypes.string.isRequired
  }).isRequired,
  handleOpen: PropTypes.func.isRequired,
  cancelLeave: PropTypes.func.isRequired
};

export default MyLeavesList;
