import React, { useState } from "react";
import PropTypes from "prop-types";
import _ from "lodash";
import * as a5 from "../../../static/avatar.png";
import moment from "moment";
import { Collapse } from "reactstrap";

const ListLeaves = ({ listItems, selectedLeave, selectLeave,height }) => {
  const [collapsed, setCollapsed] = useState(false);
  let LeavesList = _.map(listItems, (leave, key) => {
    let leaveStatusColor = "";
    let selectedDivClass = "";

    if (leave.status === "Approved") {
      leaveStatusColor = "success";
    } else if (leave.status === "Pending") {
      leaveStatusColor = "warn";
    } else if (leave.status === "Rejected") {
      leaveStatusColor = "danger";
    }
    const date = leave.applied_on;
    if (leave.id == selectedLeave.id && leave.status === "Approved") {
      selectedDivClass = "lightgreen";
    } else if (leave.id == selectedLeave.id && leave.status === "Pending") {
      selectedDivClass = "lightyellow";
    } else if (leave.id == selectedLeave.id && leave.status === "Rejected") {
      selectedDivClass = "lightred";
    }
    return (
      <div className="bg-white">
        <div
          key={key}
          id={`leaveList${key}`}
          className={`list-item pointer mb-2 b-l b-l-2x  b-${leaveStatusColor} ${selectedDivClass}`}
          onClick={() => {
            selectLeave(leave.id);
            setCollapsed(false);
          }}
        >
          <div className="list-left">
            <span className="w-40 avatar">
              <img
                src={
                  leave.user_profile_image === null
                    ? a5
                    : leave.user_profile_image
                }
                className="img-circle"
              />
            </span>
          </div>
          <div className="list-body">
            <div>
              {leave.leave_type.toLowerCase() === "restricted" && <b>RH</b>}
            </div>
            <div>
              <small>{leave.user_profile_name}</small>
              <div className="time_for_apply">
                <i className="fa fa-clock-o pr-2" />
                {moment(date).format("ll")}
              </div>
            </div>

            <div className="block">
              <small>
                Date :{" "}
                {`${moment(leave.from_date).format(
                  "DD MMM"
                )}, ${moment().format("YYYY")}`}{" "}
                {leave.from_date !== leave.to_date && <>
                {"To"}{" "}
                {`${moment(leave.to_date).format("DD MMM")}, ${moment().format(
                  "YYYY"
                )}`}
                </>}
              </small>
            </div>
            <div className="block d-flex ">
              <small>Reason: </small>
              <small className="block reason-slide pl-2">{leave.reason}</small>
            </div>
          </div>
        </div>
      </div>
    );
  });
  return (
    <div className="row-col">
      <div
        className={`text-center leave-list-collapse hidden-md hidden-sm hidden-lg${
          collapsed ? " collapsed" : ""
        }`}
        onClick={() => setCollapsed(collapse => !collapse)}
      >
        <h6>List Leaves</h6>
        <span className="material-icons expand-icon">
          {collapsed ? "expand_more" : "expand_less"}
        </span>
      </div>
      {window.innerWidth > 767 ? (
        <div id="leavesList" className={"collapse show"}>
          <div className="list inset scroll scroll-leave-list" style={{maxHeight:`${height}px`}}>
            {LeavesList}
          </div>
        </div>
      ) : (
        <Collapse isOpen={collapsed}>
          <div id="leavesList" className={"collapse show"}>
            <div className="list inset scroll scroll-leave-list">
              {LeavesList}
            </div>
          </div>
        </Collapse>
      )}
    </div>
  );
};

ListLeaves.propTypes = {
  listItems: PropTypes.array.isRequired,
  selectedLeave: PropTypes.shape({
    id: PropTypes.string.isRequired
  }).isRequired,
  selectLeave: PropTypes.func.isRequired
};

export default ListLeaves;
