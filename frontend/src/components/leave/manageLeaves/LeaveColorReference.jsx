import React from "react";
import PropTypes from "prop-types";
import { CONFIG } from "../../../config/index";
import { Nav, NavItem, NavLink } from "reactstrap";

const LeaveColorReference = ({ filterLeaveList, selectedTab, userRole, windowInnerwidth }) => {
  if (userRole === CONFIG.ADMIN) {
    return (
      <Nav className="status_header bg-body manage-users-nav-bar d-flex" tabs>
        <NavItem className="flex-fill text-center">
          <NavLink
            className={`${selectedTab === "PendingAdmin" ? "border-warning" : "active"} leave-tab-header`}
            onClick={() => {
              filterLeaveList("PendingAdmin");
            }}
          >
            <span className="leave-status-link text-warning">{windowInnerwidth <768 ? "Pending" : "Pending Leaves"}</span>
          </NavLink>
        </NavItem>
        <NavItem className=" flex-fill text-center">
          <NavLink
            className={`${selectedTab === "Approved" ? "border-success" : "active"} leave-tab-header`}
            onClick={() => {
              filterLeaveList("Approved");
            }}
          >
            <span className="leave-status-link text-success">{windowInnerwidth <768 ? "Approved" : "Approved Leaves"}</span>
          </NavLink>
        </NavItem>
        <NavItem className="flex-fill text-center">
          <NavLink
            className={`${selectedTab === "Rejected" ? "border-danger" : "active"} leave-tab-header`}
            onClick={() => {
              filterLeaveList("Rejected");
            }}
          >
            <span className="leave-status-link text-danger">{windowInnerwidth <768 ? "Rejected" : "Rejected Leaves"}</span>
          </NavLink>
        </NavItem>
      </Nav>
    );
  } else if (userRole === CONFIG.HR || userRole === CONFIG['HR Payroll Manager'] ) {
    return( 
    <Nav className="status_header bg-body manage-users-nav-bar d-flex" tabs>
      <NavItem className="flex-fill text-center">
        <NavLink
          className={`${selectedTab === "Pending" ? "border-warning" : "active"} leave-tab-header`}          
          onClick={() => {
            filterLeaveList("Pending");
          }}
        >
          <span className="leave-status-link text-warning">Pending Leaves</span>
        </NavLink>
      </NavItem>
      <NavItem className="flex-fill text-center">
        <NavLink
          className={`${selectedTab === "ApprovedByHr" ? "border-success" : "active"} leave-tab-header`}          
          onClick={() => {
            filterLeaveList("ApprovedByHr");
          }}
        >
          <span className="leave-status-link text-successs">Approved By HR</span>
        </NavLink>
      </NavItem>
    </Nav>
    );
  }
};

LeaveColorReference.propTypes = {
  filterLeaveList: PropTypes.func.isRequired,
  selectedTab: PropTypes.string.isRequired,
  userRole: PropTypes.string.isRequired
};

export default LeaveColorReference;
