import React, { useState } from "react";
import PropTypes from "prop-types";
import _ from "lodash";
import DeviceCounter from "./DeviceCounter";
import { CSVLink } from "react-csv";
import moment from "moment";
import AddDeviceDialoge from "../../modules/inventory/components/AddDeviceDialoge";

const DeviceCounterTab = ({
  statusList,
  deviceCountList,
  history,
  callAddDevice,
  deviceTypeList,
  onCallDeviceType,
  isMobile,
  handleDeleteSoldDevice
}) => {
  let total = 0;
  let newDeviceCountData = [];
  let headers = [{ label: "Device Name", key: "deviceName" }];
  let datas = [];
  let headerData = [];
  Object.keys(deviceCountList).map(key => {
    Object.keys(deviceCountList[key]).map(keys => {
      headers.map(data => {
        if (data.label !== keys) {
          let modLabel = keys.split("_").join(" ");
          let label = modLabel.charAt(0).toUpperCase() + modLabel.slice(1);
          headerData.push({
            label: label,
            key: keys
          });
        }
      });
    });
  });
  let hdata = _.map(
    _.uniq(
      _.map(headerData, function(obj) {
        return JSON.stringify(obj);
      })
    ),
    function(obj) {
      return JSON.parse(obj);
    }
  );
  let headersData = headers.concat(hdata);
  Object.keys(deviceCountList).map(key => {
    let deriveData = {
      deviceName: key
    };
    Object.keys(deviceCountList[key]).map(keys => {
      deriveData[keys] = deviceCountList[key][keys];
    });
    datas.push(deriveData);
  });

  for (var key in deviceCountList) {
    if (deviceCountList.hasOwnProperty(key)) {
      let deviceData = deviceCountList[key];
      total = total + deviceData.total;
      newDeviceCountData.push(
        <DeviceCounter
          key={key}
          deviceData={deviceData}
          deviceName={key}
          history={history}
          statusList={statusList}
          handleDeleteSoldDevice={handleDeleteSoldDevice}
        />
      );
    }
  }
  const [openAddDeviceDialog, setDialog] = useState(false);
  const handleClose = () => setDialog(false);
  const handleOpen = () => setDialog(true);
  return (
    <div>
      <div className="col-xs-12 col-sm-12 mb-4 inventory-head-container px-0">
        <div className="header_inventory bg-body align-items-center">
          <div className="page-title inventory-page-overview-header flex-column flex-sm-row">
            <div className="device_total">
              Total Devices: <strong>{total}</strong>
            </div>
          </div>
          <div className="inventory-overview-btn d-flex justify-content-sm-end">
            {/* <Link
              className="audit-button btn btn-md ml-6"
              to="/app/audit_inventory_list"
            >
              {isMobile > 650 ? "VIEW AUDIT HISTORY" :"HISTORY" }
            </Link> */}
            <CSVLink
              data={datas}
              headers={headersData}
              className={`text-decoration-none ${isMobile > 650 ? "download-button btn btn-md" : "fa fa-download"}`}
              filename={`device-report-${moment().format("YYYY-MMMM-DD")}.csv`}
            >
                {isMobile > 650 ? "Download Report" :"" }
            </CSVLink>
            <AddDeviceDialoge
              callAddDevice={callAddDevice}
              handleClose={handleClose}
              handleOpen={handleOpen}
              open={openAddDeviceDialog}
              deviceTypeList={deviceTypeList}
              onCallDeviceType={onCallDeviceType}
            />
          </div>
        </div>
      </div>
      <div className="device-detail">
      {newDeviceCountData}
      </div>
      
    </div>
  );
};

DeviceCounterTab.propTypes = {
  statusList: PropTypes.array.isRequired
};

export default DeviceCounterTab;
