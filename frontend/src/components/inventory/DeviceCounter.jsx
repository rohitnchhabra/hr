import React, { Component } from "react";
import PropTypes from "prop-types";
import Widget from "../generic/Widget/Widget";
import Raphael from "raphael";
import "morris.js/morris.css";
import "morris.js/morris.js";
import $ from "jquery";
window.Raphael = Raphael;
const Morris = window.Morris;
class DeviceCounter extends Component {
  constructor(props) {
    super(props);
    this.state = { data: [] };
  }
  componentDidMount() {
    const { deviceData, statusList } = this.props;
    let data = [];
    let color = [];
    Object.keys(deviceData).map((k, idx) => {
      if (
        k === "User_Assign" ||
        k === "User_Not_Assign" ||
        k === "Assigned" ||
        k === "total" /* ||
        k === "Working" */
      ) {
        return;
      } else {
        let stat = statusList.find(
          data => data.status.toLowerCase() === k.toLowerCase()
        );
        data.push({ label: k, value: deviceData[k] });
        if (stat) {
          color.push(stat.color);
        }
      }
    });
    this.initMorrisDonutChart(data, color);
  }
  initMorrisDonutChart = (data, color) => {
    $(this.morrisDonutChart).html("");
    Morris.Donut({
      element: this.morrisDonutChart,
      resize: true,
      data: data,
      colors: color
    });
  };

  renderList = deviceData => {
    Object.keys(deviceData).map((k, idx) => {
      if (k === "User_Assign" || k === "User_Not_Assign" || k === "Assigned") {
        return;
      } else if (k === "total") {
        return (
          <React.Fragment key={k}>
            <div className="stat-item">
              <h6 className="name">Total</h6>
              <p className="value">{deviceData[k]}</p>
            </div>
            {deviceData["Working"] ? (
              <div className="stat-item">
                <h6 className="name">Working</h6>
                <p className="value">{deviceData["Working"]}</p>
              </div>
            ) : null}
          </React.Fragment>
        );
      }
    });
  };
  handleButtonClick = (e, device_name) => {
    e.stopPropagation();
    this.props.handleDeleteSoldDevice(device_name);
  };

  render() {
    const { deviceData, deviceName } = this.props;
    const device_name = (this.props && this.props.deviceName).toUpperCase();
    let list = Object.keys(deviceData).map((k, idx) => {
      if (k === "User_Assign" || k === "User_Not_Assign" || k === "Assigned") {
        return;
      } else if (k === "total") {
        return (
          <React.Fragment key={k}>
            <div className="stat-item d-flex">
              <div className="name border-primary pl-2  mr-2">Total:</div>
              <div className="value">
                <small className="font-weight-bold">{deviceData[k]}</small>
              </div>
            </div>
            {deviceData["Working"] ? (
              <div className="stat-item d-flex p-0 border-none">
                <div className="name border-success mr-2 pl-2">Working:</div>
                <div className="value">
                  <small className="font-weight-bold">
                    {deviceData["Working"]}
                  </small>
                </div>
              </div>
            ) : null}
            {deviceData["User_Not_Assign"] ? (
              <div className="stat-item d-flex p-0 border-none">
                <div className="name border-danger mr-2 pl-2">Unassigned:</div>
                <div className="value">
                  <small className="font-weight-bold">
                    {deviceData["User_Not_Assign"]}
                  </small>
                </div>
              </div>
            ) : null}
          </React.Fragment>
        );
      }
    });
    return (
      <div
        className="col-xs-12 col-sm-12 col-md-6 col-xl-4 deviceinfo px-1"
        onClick={() =>
          this.props.history.push(`/app/inventoryOverviewDetail/${deviceName}`)
        }
      >
        <Widget
          title={<h6>{device_name}</h6>}
          className="deviceCardWidget my-1"
        >
          <div className="deviceCard">
            <div className="stats-row list-content">{list}</div>
            <div className="stats-row text-center chart">
              <div
                className="stat-item"
                style={{ height: "130px", width: "130px" }}
                ref={r => {
                  this.morrisDonutChart = r;
                }}
              />
            </div>
          </div>
        </Widget>
      </div>
    );
  }
}

DeviceCounter.propTypes = {
  deviceData: PropTypes.shape({
    total: PropTypes.number.isRequired,
    User_Assign: PropTypes.number,
    User_Not_Assign: PropTypes.number
  }),
  deviceName: PropTypes.string.isRequired
};

export default DeviceCounter;
