import React, { Component } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { withRouter } from "react-router";
import * as actions from "../../redux/actions";
import * as actionsManageUsers from "../../redux/manageUsers/actions/manageUsers";
import Heading from "../generic/Heading";
import GenricCardList from "../generic/GenricCardList";
import { Alert,Button } from "reactstrap";
import AlertFader from "../../components/generic/AlertFader";
import * as actionsManageDevice from "../../redux/inventory/actions/inventory";

class ViewUserDevice extends Component {
  constructor(props) {
    super(props);
    this.state = {
      defaultUserDisplay: "",
      assigned_userid:"",
      user_assign_machine:[],
      status:""
    };
  }

  componentDidUpdate(prevProps,prevState) {
    if (this.state.defaultUserDisplay === "") {
      if (this.props.usersList.users && this.props.usersList.users.length > 0) {
        let firstUser = this.props.usersList.users[0];
        let defaultUserId = firstUser.user_Id;
        let defaultUserName = firstUser.username;
        this.onUserClick(defaultUserId, defaultUserName);
      }
    }
    if(JSON.stringify(prevProps.manageUsers.user_assign_machine) !== JSON.stringify(this.props.manageUsers.user_assign_machine)){
      let userAssignMachine =this.props.manageUsers.user_assign_machine
      userAssignMachine.forEach((item,i)=>{
        item.check=false
      })
      this.setState({
        user_assign_machine:userAssignMachine
      })
    }
  
  }

  onUserClick = (userid, username) => {
    this.setState({
      defaultUserDisplay: userid
    });
    this.props.onUserProfileDetails(userid, username);
  };

  componentWillMount() {
    this.props.requestUsersList();
    this.props.showHeading("User Inventory Details");
  }

  assignDevice=(data,user_Id)=>{
    this.props.onAssignDevice(data)
    .then(res=>{
      this.props.onUserProfileDetails(user_Id)
      this.setState({
        assigned_userid:"",
        status:{
          err:0,
          message:res
        }
      },()=>this.setState({
        status:""
      }))
    },err=>{
      this.setState({
        status:{
          err:1,
          message:err
        }
      },()=>this.setState({
        status:""
      }))
    })
  }

  onHandleChange=(e,item)=>{
    let userAssignMachine=this.state.user_assign_machine
    userAssignMachine.forEach((val,i)=>{
      val.check=false
      item.check=true
    })
    this.setState({
      user_assign_machine:userAssignMachine,
      assigned_userid:e.target.value
    })
  }

  details = () => {
    return (
      <div className="col-md-9 px-0 scroll my-inventory-list-user">
        <div className="user-inventory-details">
          <div className="bg-white">
            {this.state.user_assign_machine.length == "" ? (
              <Alert
                className="alert-transparent rem-fade rh-section rh-alert"
                color="danger"
              >
                <span>Device not Asssigned !</span>
              </Alert>
            ) : (
              <div className="px-4">
                {this.state.user_assign_machine && this.state.user_assign_machine.map((item, i) => (
                  <>
                    <div className="machineDetailInfo p-t">
                      <h4
                        className="redirect-pointer darkGrey"
                        onClick={() =>
                          this.props.history.push({
                            pathname: `/app/inventoryOverviewDetail/${item.machine_type}`,
                            state: { id: item.id }
                          })
                        }
                      >
                        {item.machine_name}
                      </h4>
                      <h6 className="lightGrey">{item.machine_type}</h6>
                    </div>
                    <div className="d-flex">
                      <div className="machineDetailInfo col-md-3 pl-0">
                        <h6 className="lightGrey">Assign Date</h6>
                        <h6 className="darkGrey">{item.assign_date}</h6>
                      </div>
                      <div className="machineDetailInfo col-md-3 ">
                        <h6 className="lightGrey ">Serial No.</h6>
                        <h6
                          className="redirect-pointer darkGrey"
                          onClick={() =>
                            this.props.history.push({
                              pathname: `/app/inventoryOverviewDetail/${item.machine_type}`,
                              state: { id: item.id }
                            })
                          }
                        >
                          {item.serial_number}
                        </h6>
                      </div>
                      <div className="machineDetailInfo col-md-3 ">
                        <h6 className="lightGrey">Internal serial No</h6>
                        <h6
                          className="redirect-pointer darkGrey"
                          onClick={() =>
                            this.props.history.push({
                              pathname: `/app/inventoryOverviewDetail/${item.machine_type}`,
                              state: { id: item.id }
                            })
                          }
                        >
                          {item.bill_number}
                        </h6>
                      </div>
                      <div className="machineDetailInfo col-md-3">
                      <>
                        <select
                          onChange={e =>
                            this.onHandleChange(e,item)
                          }
                          className="form-control"
                          value={item.check==true ? this.state.assigned_userid:""}
                        >
                        <option value="">--Select User--</option>
                        {this.props.usersList  && this.props.usersList.users && this.props.usersList.users.length>0 && this.props.usersList.users.map((user,i)=>
                          <option value={user.user_Id}>{user.name}</option>
                        )}
                      </select>
                      <Button
                        color="primary"
                        className="mt-1 w-100"
                        size="md"
                        onClick={() =>
                          this.assignDevice({
                            user_id:this.state.assigned_userid,
                            inventory_id:item.id
                          },item.user_Id)
                        }
                      >
                        Assign Inventory
                      </Button>
                  </>
                      </div>
                    </div>
                  </>
                ))}
              </div>
            )}
          </div>
        </div>
      </div>
    );
  };

  render() {
    return (
      <div className="user-inventory-overview-container">
        <AlertFader>
            {this.state.status && (
              <Alert
                fade={false}
                className="alert-transparent"
                color={this.state.status.err==0?this.state.status.message=="Sold status inventory cannnot be assign to any employee"?"info":"success":"danger"}
              >
                {this.state.status.message}
              </Alert>
            )}
          </AlertFader>
        <div className="d-flex flex-wrap flex-lg-nowrap">
          <div className="assign-member scroll col-md-3 p-0 my-inventory-list-user"> 
            {this.props.usersList &&
              this.props.usersList.users &&
              this.props.usersList.users.map((item, index) => {
                return (
                  <>
                    <GenricCardList
                      onCardSelected={() =>
                        this.onUserClick(item.user_Id, item.name)
                      }
                      cardBodyClassName={
                        this.state.defaultUserDisplay === item.user_Id
                          ? "lightGreen"
                          : ""
                      }
                      cardClassname={"border-0 m-b"}
                      heading={item.name}
                      subHeading={item.jobtitle}
                    />
                    {window.screen.availWidth < 768 &&
                      this.state.defaultUserDisplay === item.user_Id &&
                      this.details()}
                  </>
                );
              })}
          </div>
          {/* <h4 className="text-center">Assigned Device Details</h4> */}
          {window.screen.availWidth >= 768 && this.details()}
        </div>
      </div>
    );
  }
}
function mapStateToProps(state) {
  return {
    frontend: state.frontend.toJS(),
    usersList: state.usersList.toJS(),
    manageUsers: state.manageUsers.toJS(),
    loggedUser: state.logged_user.userLogin,
    manageDevice: state.manageDevice.toJS()
  };
}

const mapDispatchToProps = dispatch => {
  return {
    requestUsersList: () => {
      return dispatch(actions.requestUsersList());
    },
    onUserProfileDetails: (userid, username) => {
      return dispatch(
        actionsManageUsers.getUserProfileDetails(userid, username)
      );
    },
    onAssignDevice: assign_device => {
      return dispatch(actionsManageDevice.assignDevice(assign_device,true));
    },
    showHeading: data => {
      return dispatch(actions.showHeading(data));
    },
    
  };
};

ViewUserDevice.propTypes = {
  userAssignMachine: PropTypes.array.isRequired
};

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(ViewUserDevice)
);
