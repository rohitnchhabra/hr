import React, { useState, useEffect } from "react";
import PropTypes from "prop-types";
import moment from "moment";
import { CONFIG } from "../../config/index";
import * as a5 from "../../static/avatar.png";
import map from "lodash/map";

import { Button, Spinner } from "reactstrap";
import isEmpty from "lodash/isEmpty";
import classnames from "classnames";
import GenricCardList from "../generic/GenricCardList";
import FlatButton from "material-ui/FlatButton";
import RaisedButton from "material-ui/RaisedButton";
import Dialog from "material-ui/Dialog";
import TextField from "material-ui/TextField";
import useDomNodeHeight from "../customHooks/useDomNodeHeight";

let path = CONFIG.inventory_images;
const DeviceDetails = ({
  userAssignMachine,
  unassignDevice,
  loggedUser,
  handleAuditClick,
  activeAuditId,
  activeAudits,
  showPending,
  callAddUserComment,
  inventoryAuditComments,
  auditTypeChange,
  audit_comment_type,
  onAuditMsgChange,
  handleAuditSubmit,
  auditMsg,
  activeUnassign,
  isAssignLoading,
  loading,
  userName,
  onUserChange,
  AssignDevice,
  isMobile,
  inputBox,
  handleDropdown,
  handleCardClick
}) => {
  const [comment, setComment] = useState("");
  const [machineDetail, setMachineDetail] = useState({});
  const [showError, setError] = useState(false);
  const [open, handleOpen] = useState(false);
  const [clicked,setClicked] = useState(0);
  const [realPendingAudit,handleRealPendingAudit] = useState([])
  const [deviceDetailsRef, height] = useDomNodeHeight(clicked);
    useEffect(() => {
    if (
      userAssignMachine &&
      userAssignMachine.length &&
      isEmpty(machineDetail)
    ) {
      setMachineDetail(userAssignMachine[0]);
    } else {
      if (
        userAssignMachine &&
        userAssignMachine.length &&
        !isEmpty(machineDetail)
      ) {
        let new_machineDetail = userAssignMachine.filter((val, index) => {
          return val.id == machineDetail.id;
        });
        if (new_machineDetail.length) {
          setMachineDetail(...new_machineDetail);
        } else {
          setMachineDetail(userAssignMachine[0]);
        }
      } 
    }
  }, [userAssignMachine]);

  function handlePendingAudit(id){
    let realPending = realPendingAudit;
    realPending.push(id);
    handleRealPendingAudit(realPending);
  }

  let size = 5;
  let details = function() {
    const actions = [
      <FlatButton
        label="Cancel"
        primary
        onClick={() => handleOpen(false)}
        style={{ marginRight: 5 }}
      />,
      <RaisedButton
        label="Unassign Device"
        primary
        onClick={() => {
          callAddUserComment({
            inventory_id: machineDetail.id,
            comment,
            action:
              loggedUser.data.role === "Admin"
                ? "unassigned_my_inventory"
                : "inventory_unassign_request"
          });
          handleOpen(false);
          setComment("");
        }}
        disabled={!Boolean(comment)}
      />
    ];
    return (
      <div className="col-md-9 p-0 selectedDetail">
        <div
          className={classnames(
            machineDetail.status === "Working"
              ? "workingStatus"
              : "not-workingStatus",
            "overflow-auto px-0",
            " machineDetail",
            "ml-1"
          )}
          ref={deviceDetailsRef}
        >
        {machineDetail && machineDetail.machine_name &&
          <div className="col-lg-8 col-md-12">
            <div className="col-md-6">
              <div className="machineDetailInfo p-t">
                <h4 className="darkGrey">{machineDetail.machine_name}</h4>
                <h6 className="lightGrey">{machineDetail.machine_type}</h6>
              </div>
              <div className="machineDetailInfo">
                <h6 className="lightGrey">Id</h6>
                <h6 className="darkGrey">{machineDetail.id}</h6>
              </div>
              <div className="machineDetailInfo">
                <h6 className="lightGrey">Assign Date</h6>
                <h6 className="darkGrey">{machineDetail.assign_date}</h6>
              </div>

              <div className="machineDetailInfo">
                <h6 className="lightGrey">Serial Number</h6>
                <h6 className="darkGrey">{machineDetail.serial_number}</h6>
              </div>
              <div className="machineDetailInfo">
                <h6 className="lightGrey">Internal Serial No.</h6>
                <h6 className="darkGrey">{machineDetail.bill_number}</h6>
              </div>
            </div>
            <div className="col-md-6">
              <div className="machineDetailInfo p-t">
                <h4 className="darkGrey">
                  {machineDetail.name ? machineDetail.name : "Not Assigned"}
                </h4>
                <h6 className="lightGrey">
                  {machineDetail.designation
                    ? machineDetail.designation
                    : "No Designation Data"}
                </h6>
              </div>

              <div className="machineDetailInfo pt-4">
                {machineDetail.audit_current_month_status &&
                machineDetail.audit_current_month_status.status === false &&
                !machineDetail.hasOwnProperty('is_ownership_change_req_handler')
                ? (
                  <>
                    <select
                      onChange={e => {
                        auditTypeChange(e, machineDetail.id);
                      }}
                      value={audit_comment_type}
                      className="form-control mb-1 mt-1"
                    >
                      <option value="">Select Your Audit Report</option>
                      {map(inventoryAuditComments, (data, key) => {
                        return <option value={key}>{data}</option>;
                      })}
                    </select>
                    {inputBox ? (
                      <div className="d-flex flex-column">
                        <input
                          type="text"
                          className="form-control mb-1 mt-1"
                          value={auditMsg}
                          onChange={e => {
                            setError(false);
                            onAuditMsgChange(e.target.value);
                          }}
                          onKeyUp={e => {
                            if (e.keyCode === 13) {
                              if (auditMsg != "") {
                                handleAuditSubmit(
                                  "large",
                                  machineDetail.id,
                                  auditMsg,
                                  audit_comment_type
                                );
                              }
                            }
                          }}
                          required
                        />
                        {showError ? (
                          <small className="text-right text-danger">
                            Fill the field
                          </small>
                        ) : null}
                      </div>
                    ) : null}
                    {audit_comment_type !== "" ? (
                      <>
                        <Button
                          size="sm"
                          color="success"
                          className="mb-1 float-right"
                          disabled={realPendingAudit.includes(machineDetail.id) ? showPending : false}
                          onClick={
                            audit_comment_type === "all_good"
                              ? () => {
                                  handleAuditSubmit(
                                    "large",
                                    machineDetail.id,
                                    auditMsg,
                                    audit_comment_type
                                  );
                                  handlePendingAudit(machineDetail.id);
                                }
                              : auditMsg !== ""
                              ? () => {
                                  handleAuditSubmit(
                                    "large",
                                    machineDetail.id,
                                    auditMsg,
                                    audit_comment_type
                                  );
                                  handlePendingAudit(machineDetail.id);
                                }
                              : () => {
                                  setError(true);
                                }
                          }
                        >
                          {activeAudits.find((item,i)=>{
                            return machineDetail.id==item.id
                          }) &&
                          showPending ? (
                            <Spinner color="light" size="sm" />
                          ) : (
                            <small>Submit</small>
                          )}
                        </Button>
                      </>
                    ) : null}
                  </>
                ) : (
                  <div>
                    <h6 className="lightGrey">Status</h6>
                    <h6 className="darkGrey">
                      <div>
                        {
                          machineDetail.audit_current_month_status.status
                            .audit_comment
                        }
                        <br />
                        Audited On -{" "}
                        {moment(
                          machineDetail.audit_current_month_status.status
                            .updated_at
                        ).format("Do MMMM YYYY, h:mm:ss a")}
                        <br/>
                        Audited By -{" "}{
                          machineDetail.audit_current_month_status.status
                            .audit_done_by_user_name
                        }
                      </div>
                    </h6>
                  </div>
                )}
              </div>
              <div className="machineDetailInfo">
                {(machineDetail.audit_current_month_status.status || 
                    (machineDetail.hasOwnProperty('is_ownership_change_req_handler') && 
                    machineDetail.is_ownership_change_req_handler ==1)) && (
                  <> {(machineDetail.is_unassign_request === "1") && !(machineDetail.is_unassign_request_handler) && !machineDetail.hasOwnProperty('is_ownership_change_req_handler') ? <strong> You had requested for unassign</strong> : 
                       ((machineDetail.is_unassign_request === "1") &&
                        (machineDetail.is_unassign_request_handler === 1 ) ||
                        (machineDetail.hasOwnProperty('is_ownership_change_req_handler') &&
                        machineDetail.is_ownership_change_req_handler ==1) ? 
                       <>
                       {
                         (machineDetail.hasOwnProperty('is_ownership_change_req_handler') &&
                         machineDetail.is_ownership_change_req_handler ==1) &&
                         <h6 className="darkGrey">
                          <div>
                              Requested by user : {machineDetail.ownership_change_req_by_user} <br/>
                              Requested by user name : {machineDetail.ownership_change_req_by_user_name}
                          </div>
                         </h6>
                       }
                          <select
                            onChange={e =>
                              onUserChange(
                                e.target.value,
                                machineDetail.id
                              )
                            }
                            className="form-control"
                            // value={props.state.user_id}
                          >
                            <option disabled>--Select User--</option>
                            {userName}
                          </select>
                          <Button
                            color="primary"
                            className="mt-1"
                            size="md"
                             onClick={() => AssignDevice() }
                          >
                            Assign Inventory
                          </Button>
                        </>
                       : 
                    <Button
                      id="unAssign-btn"
                      outline
                      size="sm"
                      color="danger"
                      onClick={() => handleOpen(true)}
                      disabled={
                        activeUnassign.includes(machineDetail.id) &&
                        isAssignLoading
                          ? true
                          : false
                      }
                    >
                      {activeUnassign.includes(machineDetail.id) &&
                      isAssignLoading ? (
                        <span className="loading-dot">Unassigning</span>
                      ) : (
                        "Unassign Device"
                      )}
                       </Button> )}
                    <Dialog
                      title={"COMMENT"}
                      titleStyle={{ opacity: "0.56" }}
                      modal={false}
                      actions={actions}
                      open={open}
                      onRequestClose={() => handleOpen(false)}
                      autoScrollBodyContent
                    >
                      <TextField
                        fullWidth
                        // ref="value"
                        floatingLabelText={"Comment"}
                        onChange={e => {
                          setComment(e.target.value);
                        }}
                        value={comment}
                      />
                    </Dialog>
                  </>
                )}
                {/* ) : null} */}
              </div>
            </div>
          </div>}

          <div className="col-lg-4 col-md-12">
            { machineDetail.fileInventoryPhoto &&  <img
              className="w-100"
              src={
                machineDetail.fileInventoryPhoto
                  ? machineDetail.fileInventoryPhoto
                  : null
              }
            ></img>}
            <div className="my-inventory-comment bg-white pt-3">
              <h6> Recent Comments</h6>
              <ul>
                {machineDetail.history &&
                  machineDetail.history.length &&
                  machineDetail.history.slice(0, size).map((val, index) => {
                    return (
                      <li>
                        <span className={`thumb-xs pull-left mr-sm`}>
                          <img className="rounded-circle" src={a5} alt="..." />
                        </span>
                        <div>
                          <h6 className={`commentUser fs-sm fw-semi-bold`}>
                            {val.updated_by_user}
                            <br />
                            <small>
                              {moment(val.updated_at).format(
                                "Do MMM YY, h:mm a"
                              )}
                            </small>
                          </h6>
                          <p>{val.assign_unassign_user_name? val.comment=="Inventory Assigned"?val.comment + " to " + val.assign_unassign_user_name:val.comment + " from " + val.assign_unassign_user_name :val.comment}</p>
                        </div>
                      </li>
                    );
                  })}
              </ul>
            </div>
          </div>
        </div>
      </div>
    );
  };
  return (
    <div>
      <div className="d-flex flex-wrap flex-lg-nowrap">
        {!loading && !userAssignMachine ? (
          <div className="col-md-3 p-0 overflow-auto scroll">
            No inventories assigned
          </div>
        ) : (
          <div
            className="col-md-3 p-0 my-inventory-list-scroll scroll"
          >
            {userAssignMachine
              ? userAssignMachine.map((item, index) => {
                  return (
                    <>
                      <GenricCardList
                        onCardSelected={() => {
                          auditTypeChange(null, item.id); 
                          setMachineDetail(item);
                          setClicked(click => click + 1);
                          handleDropdown(item.id,item.audit_current_month_status.status);
                          handleCardClick(item)
                        }
                      }
                        
                      cardBodyClassName={`${
                        item.id === machineDetail.id ? "lightGreen" : ""
                      }
                        ${
                          item.audit_current_month_status.status
                            ? item.audit_current_month_status.status
                                .comment_type == "all_good"
                              ? "borderGreen"
                              : item.audit_current_month_status.status
                                  .comment_type == "critical_issue"
                              ? "borderRed"
                              : item.audit_current_month_status.status
                                  .comment_type == "issue"
                              ? "borderOrange"
                              : ""
                            : "borderGray"
                        }`}
                        cardClassname={"border-0"}
                        heading={item.machine_name}
                        subHeading={item.machine_type}
                        sec_Heading={item.bill_number}
                        request = {item.is_unassign_request_handler}
                        ownershipRequest={
                          item.hasOwnProperty("is_ownership_change_req_handler") && 
                          item.is_ownership_change_req_handler
                        }
                        forRequest = {item.is_unassign_request}
                      />
                      {window.screen.availWidth < 768 &&
                        item.id === machineDetail.id &&
                        details()}
                    </>
                  );
                })
              : null}
          </div>
        )}

        {Object.keys(machineDetail).length && userAssignMachine 
          ? window.screen.availWidth >= 768 && details()
          : null}
      </div>
    </div>
  );
};

DeviceDetails.propTypes = {
  userAssignMachine: PropTypes.array.isRequired
};

export default DeviceDetails;
