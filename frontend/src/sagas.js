import { takeLatest } from 'redux-saga/effects';
import * as constants from './redux/constants';
import { loginRequest, isAlreadyLoggedIn, forgotPassword, logout } from './redux/auth/actions';
import { holidayList, addHoliday, holidayType, deleteHoliday } from './redux/holidays/actions';
import {
  addAttendanceUploadSettings,
  deleteAttendanceUploadSettings,
  requestResetPasswordSetting,
  requestResetPasswordStatus,
  requestClearResetPasswordData,
  getLoginOption,
  setLoginOption,
  setWebSalaryView,
  setWeeklyHoliday,
  updateConfigPageHeadingsRequest,
  requestUploadAttendance,
  setAttendanceLateLimit,
  setSlackSettings,
  getSlackSettings,
  getSlackChannels,
  testSlackMsg,
  requestAddChannel,
  requestGetSmtpSettings,
  saveSmtpSettings,
  ontestSmtp,
  changePayslipSettings,
  getPayslipSettings,
  changeRhConfigSetting
} from "./redux/settings/actions";
import { getTeamStats, getMonthlyReportAllUsers, getEmployeLifeCycle, setCurrentUserRequest, getEmployeeHours, getEmployeeMonthlyHours, getEmployeePerformance, getUserIdList, getLeastActiveEmp, setUserPayslipMonthUserRequest } from './redux/manageUsers/actions/dashboard';
import { userMonthlyAttendance } from './redux/attendance/actions/monthlyAttendance';
import { getUserDayAttendance, userAttendanceStatus } from './redux/attendance/actions/attendanceRequests';
import { getUserDaySummary, updateUserDaySummary, empUpdateDaySummary } from './redux/attendance/actions/userDaySummary';
import { getUsersList } from './redux/generic/actions/usersList';
import { fetchUserPolicyDocument, submitPolicyDocs, updateReadStatus, fetchPolicyDocument } from './redux/policyDocuments/actions/index';
import { getTeamList, saveTeam, getTeam } from './redux/team/actions/teamList';
import {
  healthStatsRequest,
  healthStatsSecretKeyListRequest,
  healthStatsAddSecretKeyRequest,
  healthStatsDeleteSecretKeyRequest,
  healthStatsRegenerateSecretKeyRequest,
  deleteHealthStats,
  requestStatsHistory,
  requestStatsLeave
} from "./redux/healthStats/actions/healthStats";
import { getShowMenuListRequest, sendSelectedOptionRequest } from "./redux/showMenu/actions/index";

import { userTimeSheetRequest ,sendTimeSheetDataRequest, timeSheetListRequest, 
  approveRejectTimeSheetRequest,
  submitWeeklyTimeSheetRequest,
  requestGetUserTmsReport,
  getSubmittedTimeSheetRequest,
  getTimeSheetPerWeekRequest,
  approveRejectFullTimeSheetRequest
} from './redux/timesheet/actions';


export function* watchActions() {
  yield takeLatest(constants.USER_LOGIN_REQUEST, loginRequest);
  yield takeLatest(constants.IS_ALREADY_LOGGED_IN, isAlreadyLoggedIn);
  yield takeLatest(constants.REQUEST_FORGOT_PASSWORD, forgotPassword);
  yield takeLatest(constants.REQUEST_LOGOUT, logout);

  yield takeLatest(constants.REQUEST_HOLIDAYSLIST, holidayList);
  yield takeLatest(constants.REQUEST_ADDHOLIDAYS, addHoliday);
  yield takeLatest(constants.REQUEST_HOLIDAYTYPE, holidayType);
  yield takeLatest(constants.REQUEST_DELETEHOLIDAYS, deleteHoliday);

  yield takeLatest(constants.REQUEST_USER_ATTENDANCE, userMonthlyAttendance);
  yield takeLatest(constants.REQUEST_USER_DAY_SUMMARY, getUserDaySummary);
  yield takeLatest(constants.REQUEST_UPDATE_USER_DAY_SUMMARY, updateUserDaySummary);

  yield takeLatest(constants.REQUEST_USER_ATTENDANCE_REQUEST, getUserDayAttendance);
  yield takeLatest(constants.REQUEST_USER_ATTENDANCE_STATUS, userAttendanceStatus);
  yield takeLatest(constants.REQUEST_UPDATE_EMP_DAY_SUMMARY, empUpdateDaySummary);

  yield takeLatest(constants.REQUEST_USERSLIST, getUsersList);
  yield takeLatest(constants.REQUEST_HEALTH_STATS, healthStatsRequest);
  yield takeLatest(constants.DELETE_HEALTH_STATS, deleteHealthStats);
  yield takeLatest(constants.REQUEST_STATS_HISTORY, requestStatsHistory)
  yield takeLatest(constants.REQUEST_HEALTH_STATS_SECRET_KEY_LIST, healthStatsSecretKeyListRequest);
  yield takeLatest(constants.REQUEST_HEALTH_STATS_ADD_SECRET_KEY, healthStatsAddSecretKeyRequest);
  yield takeLatest(constants.REQUEST_HEALTH_STATS_DELETE_SECRET_KEY, healthStatsDeleteSecretKeyRequest);
  yield takeLatest(constants.REQUEST_HEALTH_STATS_REGENERATE_SECRET_KEY, healthStatsRegenerateSecretKeyRequest);
  yield takeLatest(constants.REQUEST_STATS_LEAVE_HISTORY, requestStatsLeave);
  yield takeLatest(constants.GET_GENRIC_LOGIN_CONFIGURATION, getLoginOption);
  yield takeLatest(constants.SET_GENRIC_LOGIN_CONFIGURATION, setLoginOption);
  yield takeLatest(constants.SET_WEB_SALARY_VIEW, setWebSalaryView);
  yield takeLatest(constants.SET_SLACK_SETTINGS, setSlackSettings);
  yield takeLatest(constants.GET_SLACK_SETTINGS, getSlackSettings);
  yield takeLatest(constants.GET_SLACK_CHANNELS, getSlackChannels);
  yield takeLatest(constants.TEST_SLACK_MESSAGE, testSlackMsg);
  yield takeLatest(constants.REQUEST_ADD_CHANNEL, requestAddChannel);
  yield takeLatest(constants.REQUEST_GET_SMPTP_SETTING, requestGetSmtpSettings);
  yield takeLatest(constants.SAVE_SMTP_SETTING, saveSmtpSettings);
  yield takeLatest(constants.TEST_SMTP, ontestSmtp);
  yield takeLatest(constants.CHANGE_PAYSLIP_SETTING, changePayslipSettings);
  yield takeLatest(constants.GET_PAYSLIP_SETTING, getPayslipSettings);
  yield takeLatest(constants.CHANGE_RH_CONFIG_SETTING, changeRhConfigSetting);



  yield takeLatest(constants.REQUEST_TEAM_STATS, getTeamStats);
  yield takeLatest(constants.REQUEST_MONTHLY_REPORT_ALL_USERS, getMonthlyReportAllUsers);
  yield takeLatest(constants.REQUEST_EMP_LIFE_CYCLE, getEmployeLifeCycle);
  yield takeLatest(constants.REQUEST_EMP_HOURS, getEmployeeHours);
  yield takeLatest(constants.REQUEST_EMP_MONTHLY_HOURS, getEmployeeMonthlyHours);
  yield takeLatest(constants.REQUEST_EMP_PERFORMANCE, getEmployeePerformance);
  yield takeLatest(constants.REQUEST_USER_LIST, getUserIdList);
  yield takeLatest(constants.REQUEST_LEAST_ACTIVE_EMP, getLeastActiveEmp);

  yield takeLatest(constants.REQUEST_USER_POLICY_DOCUMENT, fetchUserPolicyDocument);

  yield takeLatest(constants.REQUEST_SUBMIT_DOCS, submitPolicyDocs);
  yield takeLatest(constants.REQUEST_UPDATE_READ, updateReadStatus);

  yield takeLatest(constants.REQUEST_POLICY_DOCUMENT, fetchPolicyDocument);
  yield takeLatest(constants.REQUEST_TEAM_LIST, getTeamList);
  yield takeLatest(constants.REQUEST_ADD_TEAM, saveTeam);
  yield takeLatest(constants.REQUEST_GET_TEAM, getTeam);

  yield takeLatest(constants.REQUEST_ADD_ATTANDANCE_UPLOAD_SETTING, addAttendanceUploadSettings);
  yield takeLatest(constants.REQUEST_DELETE_ATTANDANCE_UPLOAD_SETTING, deleteAttendanceUploadSettings);


  yield takeLatest(constants.REQUEST_RESET_PASSWORD_SETTING, requestResetPasswordSetting);
  yield takeLatest(constants.REQUEST_RESET_PASSWORD_STATUS, requestResetPasswordStatus);
  yield takeLatest(constants.REQUEST_CLEAR_RESET_PASSWORD_DATA, requestClearResetPasswordData);

  yield takeLatest(constants.REQUEST_SHOW_MENU_LIST, getShowMenuListRequest);
  yield takeLatest(constants.SEND_SELECTED_OPTION_REQUEST, sendSelectedOptionRequest);
  yield takeLatest(constants.SET_WEEKLY_HOLIDAY, setWeeklyHoliday);

  yield takeLatest(constants.UPDATE_CONFIG_PAGE_HEADINGS_REQUEST, updateConfigPageHeadingsRequest);

  yield takeLatest(constants.UPLOAD_ATTENDANCE, requestUploadAttendance);
  yield takeLatest(constants.SET_ATTENDANCE_LIMIT_REQUEST, setAttendanceLateLimit);
  yield takeLatest(constants.SET_CURRENT_DISABLE_USER_REQUEST, setCurrentUserRequest);
  yield takeLatest(constants.SET_USER_PAYSLIP_MONTH_YEAR_REQUEST, setUserPayslipMonthUserRequest);

  yield takeLatest(constants.USER_TIME_SHEET_REQUEST, userTimeSheetRequest);
  yield takeLatest(constants.TIME_SHEET_LIST_REQUEST, timeSheetListRequest);
  yield takeLatest(constants.APPROVE_REJECT_TIME_SHEET_REQUEST, approveRejectTimeSheetRequest);
  yield takeLatest(constants.SEND_TIME_SHEET_DATA_REQUEST,sendTimeSheetDataRequest)
  yield takeLatest(constants.SUBMIT_WEEKLY_TIMESHEET_REQUEST,submitWeeklyTimeSheetRequest)
  yield takeLatest(constants.REQUEST_GET_USER_TMS_REPORT,requestGetUserTmsReport)
  yield takeLatest(constants.GET_SUBMITTED_TIME_SHEET_REQUEST,getSubmittedTimeSheetRequest)
  yield takeLatest(constants.GET_TIME_SHEET_PER_WEEK_REQUEST,getTimeSheetPerWeekRequest);
  yield takeLatest(constants.APPROVE_REJECT_FULL_TIME_SHEET_REQUEST, approveRejectFullTimeSheetRequest);
}

export default function* rootSaga() {
  yield [
    watchActions()
  ];
}
