import React from 'react';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import {withRouter} from 'react-router';
import * as actions from './redux/actions';

class APP extends React.Component {
  constructor (props) {
    super(props);
  }
  
  render () {
    return (
      <div>
        
        {React.cloneElement(this.props.children, this.props)}
      </div>
    );
  }
}

function mapStateToProps (state) {
  return {
    frontend:    state.frontend.toJS(),
    logged_user: state.logged_user.userLogin
  };
}
function mapDispatchToProps (dispatch) {
  return bindActionCreators(actions, dispatch);
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(APP));
