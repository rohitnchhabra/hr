import {getToken} from '../services/generic';


console.log('Environment :: ' + process.env.NODE_ENV);

const baseUrl = process.env.REACT_APP_BASE_URL;
const notificationBaseUrl = process.env.REACT_APP_NOTIFICATION_PYTHON;
const token = getToken();
// http://dev.hr.excellencetechnologies.in/hr/temp_rh_test_27042019/API_HR/api.php
const CONFIG = {
  BASE_URL:                  window.location.href.split('/#')[0] || baseUrl,
  api_url:                   `${baseUrl}/attendance/API_HR/api.php`,
  other_api_url:             `${baseUrl}/attendance/sal_info/api.php`,
  api_url_salary:            `${baseUrl}/attendance/sal_info`,
  google_login_btn_page_url: `${baseUrl}/attendance/sal_info/google-api/drive_file/index.php?token=${token}`,
  login_page_url:            `${baseUrl}`,
  // upload_url:                `${baseUrl}/attendance/sal_info/upload_file.php`,
  upload_leave_url:          `${baseUrl}/attendance/API_HR/upload_leave_doc.php`,
  upload_attendance_url:     `${baseUrl}/attendance/index.php`,
  pdf_url:                   `${baseUrl}/attendance/sal_info/`,
  upload_email_attachment:   `${baseUrl}/attendance/sal_info/upload_file_attachment.php`,
  transfer_link:             `${baseUrl}/attendance/sal_info/display_user_info.php`,
  expressApiUrl:             `${baseUrl}/attendance/API_HR/express_api_call.php`,
  expressRequestUrl:         'http://176.9.137.77:4000', // this should be http for production 
  generic_upload_url:      `${baseUrl}/attendance/API_HR/generic-file-upload.php`,
  view_inventory_documents:  `${baseUrl}/attendance/uploads/inventories/`,
  inventory_images:          `${baseUrl}/attendance/uploads/inventories/`,
  notificationBaseUrl: notificationBaseUrl
};

CONFIG['ADMIN'] = 'Admin';
CONFIG['HR'] = 'HR';
CONFIG['HR Payroll Manager'] = 'HR Payroll Manager'
CONFIG['GUEST'] = 'Guest';
CONFIG['EMPLOYEE'] = 'Employee';
CONFIG['WORKING_DAY'] = 'WORKING_DAY';
CONFIG['LEAVE_DAY'] = 'LEAVE_DAY';
CONFIG['jwt_secret_key'] = 'HR_APP';
CONFIG['PAGEROLES'] = [];
CONFIG['minimum_months_hr_salary_view'] = 8;

CONFIG['taxConfig'] = {
  educationCessPercentage: 3, // in percentage
  taxRange : [
    {
      min: 0,
      max: 250000,
      rate: 0,
      taxableAmount: 0,
      taxCalculated: 0
    },
    {
      min: 250000,
      max: 500000,
      rate: 5,
      taxableAmount: 0,
      taxCalculated: 0
    },
    {
      min: 500000,
      max: 1000000,
      rate: 20,
      taxableAmount: 0,
      taxCalculated: 0
    },
     {
      min: 1000000,
      max: 10000000,
      rate: 30,
      taxableAmount: 0,
      taxCalculated: 0
    }
  ]
}

export {CONFIG};
