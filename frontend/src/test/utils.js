import React from "react";
import { render as rtlRender } from "@testing-library/react";
import { Provider } from "react-redux";
import store from "../store";
import { Router } from "react-router";
import { createBrowserHistory } from "history";
const appHistory = createBrowserHistory();
process.env.REACT_APP_BASE_URL = "*";

function render(ui, { ...renderOptions } = {}) {
  function Wrapper({ children }) {
    return (
      <Provider store={store}>
        <Router history={appHistory}>{children}</Router>
      </Provider>
    );
  }
  return rtlRender(ui, { wrapper: Wrapper, ...renderOptions });
}

// re-export everything
export * from "@testing-library/react";

// override render method
export { render };
