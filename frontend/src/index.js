import "./styles/main.scss";
import "whatwg-fetch";
// import 'sweetalert';
import "sweetalert/dist/sweetalert.css";
import "jquery";
import "jquery-ui";
// import 'tether';
import "bootstrap";
// import "./themeFlatkit/scripts";
import React, { Suspense, lazy } from "react";
import ReactDOM from "react-dom";
import { Provider } from "react-redux";
import {
  HashRouter as Router,
  Switch,
  Route,
  Link,
  Redirect
} from "react-router-dom";
import createBrowserHistory from "history/createBrowserHistory";
import MuiThemeProvider from "material-ui/styles/MuiThemeProvider";
import Page_Login from "./modules/auth/containers/Login";
import Page_Logout from "./modules/auth/containers/logout";
import Page_ForgotPassword from "./modules/auth/containers/forgotPassword";
import PageManageDashboard from "./modules/manageUsers/containers/manageDashboard";
import LayoutComponent from "./components/Layout/Layout";
import MenuDefault from "./components/menu/LoaderMenuDefault";
import DeniedComponent from "./modules/denied/Denied"
import store from "./store";

import "./styles/theme.scss";
import  {version} from '../package.json';

const appHistory = createBrowserHistory();

ReactDOM.render(
  <MuiThemeProvider>
    <Provider store={store}>
      <Router history={appHistory} queryKey={false}>
          <Switch>
            {/* <Route path="/" exact render={() => <Redirect to="/app/home" />} /> */}
            <Route path="/app/logout" component={Page_Logout} />
            <Route
              path="/app"
              exact
              render={() => <Redirect to="/app/home" />}
            />
            <Route path="/app" component={LayoutComponent} />
            <Route path="/forgot_password" component={Page_ForgotPassword} />
            <Route exact path="/page_dashboard" component={PageManageDashboard} />
            <Route path="/" component={Page_Login} /> 
            <Route  path="*" component={DeniedComponent} />

          </Switch>
      </Router>
    </Provider>
  </MuiThemeProvider>,
  document.querySelector("#root")
);
