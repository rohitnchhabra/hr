export const qualityValue = file => {
  if (file.size >= 4000000) {
    return ( 0.1);
  } else if (file.size < 4000000 && file.size >= 3500000) {
    return (0.3);
  } else if (file.size < 3500000 && file.size >= 3000000) {
    return (0.4);
  } else if (file.size < 3000000) {
    return (0.6);
  }
};

export const dateFormatter = () => {
  const months = [
    "Jan",
    "Feb",
    "Mar",
    "Apr",
    "May",
    "Jun",
    "Jul",
    "Aug",
    "Sep",
    "Oct",
    "Nov",
    "Dec"
  ];
  const fullMonths = [
    "January",
    "February",
    "March",
    "April",
    "May",
    "June",
    "July",
    "August",
    "September",
    "October",
    "November",
    "December"
  ];
  const currentDateISO = new Date();
  const year = currentDateISO.getFullYear();
  const monthNumeric = currentDateISO.getMonth();
  const date = currentDateISO.getDate();
  const monthAlpha = months[monthNumeric];
  const dateObject = {
    date,
    monthNumeric,
    monthAlpha,
    months,
    fullMonths,
    year
  };
  return dateObject;
};

export const getLocationId = pathName => {
  return (
    pathName.includes("apply_emp_leave") ||
    pathName.includes("manage_users") ||
    pathName.includes("home") ||
    pathName.includes("manage_user_working_hours") ||
    pathName.includes("manage_salary") ||
    pathName.includes("/app/manage_roles/") || 
    pathName.includes("/app/manage_payslips") || 
    pathName.includes("/app/disabled_employees") ||
    pathName.includes("/app/all_user_timesheet")
  );
};

export const weekDays = [
  "Sunday",
  "Monday",
  "Tuesday",
  "Wednesday",
  "Thursday",
  "Friday",
  "Saturday"
];

export const docs_list = [
  {
    name: "CV",
    value: "CV"
  },
  {
    name: "PAN Card",
    value: "PAN Card"
  },
  {
    name: "Address Proof",
    value: "Address Proof"
  },
  {
    name: "Photo",
    value: "Photo"
  },
  {
    name: "Offer Letter",
    value: "Offer Letter"
  },
  {
    name: "Appointment Letter",
    value: "Appointment Letter"
  },
  {
    name: "Previous Company Experience Letter",
    value: "Previous Company Experience Letter"
  },
  {
    name: "Previous Company Offer Letter",
    value: "Previous Company Offer Letter"
  },
  {
    name: "Previous Company Salary Slip",
    value: "Previous Company Salary Slip"
  },
  {
    name: "Previous Company Other Documents",
    value: "Previous Company Other Documents"
  },
  {
    name: "Qualification Certificate",
    value: "Qualification Certificate"
  },
  {
    name: "Other Documents",
    value: "Other Documents"
  }
];
export const isMobileDevice = () =>
  /iPhone|iPad|iPod|Android/i.test(navigator.userAgent);