import * as _ from 'lodash';
import {createAction} from 'redux-actions';
import {CONFIG} from '../../../config/index';
import {fireAjax, axios_calls} from '../../../services/index';
import * as constants from '../../../redux/constants';
import {show_loading, hide_loading} from '../../../redux/generic/actions/frontend';
import 'whatwg-fetch';

// --------add new valiable-------

export function success_add_variable (data) {
  return createAction(constants.ACTION_SUCCESS_ADD_VARIABLE)(data);
}
export function error_add_varaible (data) {
  return createAction(constants.ACTION_ERROR_ADD_VARIABLE)(data);
}
export function request_add_generated_preview (data) {
  return createAction(constants.GENERATE_PREVIEW_TEMPLATE_REQUEST)(data);
}
export function success_add_generated_preview (data) {
  return createAction(constants.GENERATE_PREVIEW_TEMPLATE_SUCCESS)(data);
}
export function error_add_generated_preview (data) {
  return createAction(constants.GENERATE_PREVIEW_TEMPLATE_ERROR)(data);
}
export function request_sent_email_preview (data) {
  return createAction(constants.SEND_EMAIL_TEMPLATE_REQUEST)(data);
}
export function success_sent_email_preview (data) {
  return createAction(constants.SEND_EMAIL_TEMPLATE_SUCCESS)(data);
}
export function error_sent_email_preview (data) {
  return createAction(constants.SEND_EMAIL_TEMPLATE_ERROR)(data);
}

export function success_clear_gene_pre_send_email (data) {
  return createAction(constants.CLEAR_GENERATE_PREVIEW_SENT_EMAIL_TEMPLATE_SUCCESS)(data);
}


/*
function async_saveVariable (id, variable) {
  return fireAjax('POST', '', {
    action:        'create_template_variable',
    name:          variable.varCode,
    value:         variable.varValue,
    variable_type: variable.varType
  });
}

function async_editVariable (id, variable) {
  return fireAjax('POST', '', {
    action:        'update_template_variable',
    id:            id,
    name:          variable.varCode,
    value:         variable.varValue,
    variable_type: variable.varType
  });
}

export function saveVariable (id, variable) {
  return (dispatch, getState) => {
    return new Promise((resolve, reject) => {
      dispatch(show_loading());
      if (id == '') {
        async_saveVariable(id, variable).then((json) => {
          dispatch(hide_loading()); // hide loading icon
          if (json.error == 0) {
            dispatch(success_add_variable(json.data));
            dispatch(get_variable());
            resolve(json.data);
          } else {
            dispatch(error_add_varaible(json.error[0]));
          }
        },
        (error) => {
          dispatch(hide_loading()); // hide loading icon
          dispatch(error_add_varaible('error occurs'));
        }
);
      } else {
        async_editVariable(id, variable).then((json) => {
          dispatch(hide_loading()); // hide loading icon
          if (json.error == 0) {
            dispatch(success_add_variable(json.data));
            dispatch(get_variable());
            resolve(json.data);
          } else {
            dispatch(error_add_varaible(json.error[0]));
          }
        },
          (error) => {
            dispatch(hide_loading()); // hide loading icon
            dispatch(error_add_varaible('error occurs'));
          }
        );
      }
    });
  };
}

// ------------Get valiables---------


CODE CAN BE DELETED
function async_get_variable () {
  return fireAjax('POST', '', {
    action: 'get_template_variable'
  });
}

export function success_variable_get (data) {
  return createAction(constants.ACTION_SUCCESS_VARIABLE_GET)(data);
}

export function get_variable () {
  return function (dispatch, getState) {
    return new Promise((resolve, reject) => {
      dispatch(show_loading()); // show loading icon
      async_get_variable().then((json) => {
        dispatch(hide_loading());
        if (typeof json !== 'undefined' && json.length > 0) {
          let data = json;
          dispatch(success_variable_get(data));
          resolve(data);
        } else {
          dispatch(success_variable_get([]));
        }
      }, (error) => {
        dispatch(hide_loading());// hide loading icon
        dispatch(success_variable_get([]));
      }
			);
    });
  };
}


// ----------delete Variables---------------

function async_delete_variable (id) {
  return fireAjax('POST', '', {
    action: 'delete_template_variable',
    id:     id
  });
}

export function deleteVariable (id) {
  return function (dispatch, getState) {
    return new Promise((resolve, reject) => {
      dispatch(show_loading()); // show loading icon
      async_delete_variable(id).then((json) => {
        dispatch(hide_loading()); // hide loading icon
        if (json.error == 0) {
          dispatch(get_variable());
          resolve(json.data.message);
        } else {
          dispatch(get_variable());
          resolve(json.data.message);
        }
      }, (error) => {
        dispatch(hide_loading());// hide loading icon
        reject('error occurs!!');
      }
			);
    });
  };
}

*/

// ----------------get templates -------------

export function success_get_template (data) {
  return createAction(constants.ACTION_SUCCESS_GET_TEMPLATES)(data);
}

export function request_get_template () {
  return createAction(constants.ACTION_REQUEST_GET_TEMPLATES)();
}

function async_get_templates () {
  return axios_calls('GET', '/message/get_email_template/HR');
}
export function get_templates (isUpdate) {
  return function (dispatch, getState) {
    return new Promise((resolve, reject) => {
      dispatch(show_loading());
      if(!isUpdate) {
        dispatch(request_get_template());
      }
      async_get_templates().then((json) => {
        dispatch(hide_loading());
        if (typeof json !== 'undefined' && Object.keys(json).length > 0) {
          let data = json;
          dispatch(success_get_template(data.data));
          resolve(data);
        } else {
          dispatch(success_get_template([]));
        }
      }, (error) => {
        dispatch(hide_loading()); // hide loading icon
        reject('error occurs!!');
      }
			);
    });
  };
}

//--------------------add template--------------
function async_add_templates (formData) {
  return axios_calls('PUT', '/message/get_email_template/HR',formData);
}

export function success_add_template (payload) {
  return createAction(constants.ACTION_SUCCESS_ADD_TEMPLATES)(payload);
}

export function add_templates (formData) {
  return function (dispatch, getState) {
    return new Promise((resolve, reject) => {
      dispatch(show_loading());
      async_add_templates(formData).then((json) => {
        dispatch(hide_loading());
        if(json.status==200){
          resolve(json.data.Message)
          dispatch(success_add_template(json.data.Message));
        }
      }, (error) => {
        dispatch(hide_loading()); // hide loading icon
        reject('error occurs!!');
      }
			);
    });
  };
}

// -------------------save template ------------

// function async_save_template (t_name, t_subject, t_body) {
//   return axios_calls('PUT', '', {
//     action:  'create_email_template',
//     name:    t_name,
//     subject: t_subject,
//     body:    t_body
//   });
// }
function async_update_template (formData) {
  return axios_calls('PUT', '/message/get_email_template/HR',formData);
}
export function save_templates (formData) {
  return function (dispatch, getState) {
    return new Promise((resolve, reject) => {
      dispatch(show_loading());
//       if (_.isEmpty(t_id)) {
//         async_save_template(t_subject,t_key,t_message,t_message_subject,t_doc_type,t_for).then((json) => {
//           dispatch(hide_loading());
//           dispatch(get_templates());
//           resolve(json.data.message);
//         },
// 					(error) => {
//   dispatch(hide_loading()); // hide loading icon
//   reject('error occurs!!');
// });
//       } else {
        async_update_template(formData).then((json) => {
          dispatch(hide_loading());
          resolve(json.data.Message);
        }, (error) => {
          dispatch(hide_loading()); // hide loading icon
          reject('error occurs!!');
        }
				);
      // }
    });
  };
}

/* Generaee email template preview */

function async_generateTemplatePreview (data) {
  return axios_calls('POST', '/notify/preview', data);
}

export function generateTemplatePreview (data) {  
  return function (dispatch, getState) {
    return new Promise((resolve, reject) => {
      dispatch(request_add_generated_preview())
      dispatch(show_loading());
      async_generateTemplatePreview(data).then((json) => {
        dispatch(success_add_generated_preview(json))
        dispatch(hide_loading());
        if (json.error) {
          reject(json.data.message);
        } else {
          
          resolve();
        }
      }, (error) => {
        dispatch(hide_loading());
        dispatch(error_add_generated_preview())
        reject('error occurs!!');
      }
      );
    });
  };
}

/* sent email template */

function async_emailTemplatePreview (data) {
  return axios_calls('POST', '/notify/preview', data)
}
export function emailTemplatePreview (data) {
  return function (dispatch, getState) {
    return new Promise((resolve, reject) => {      
      dispatch(request_sent_email_preview())
      dispatch(show_loading());
      async_emailTemplatePreview(data).then((json) => {
        dispatch(success_sent_email_preview(json))
        dispatch(hide_loading());
        if (json.error) {
          reject(json.data.message);
        } else {
          resolve("Mail Sent");
        }
      }, (error) => {
        dispatch(error_sent_email_preview())
        dispatch(hide_loading());
        reject('error occurs!!');
      }
      );
    });
  };
}
export function clearGenePreSendEmail (data) {
  return function (dispatch, getState) {
    return new Promise((resolve, reject) => {
      dispatch(success_clear_gene_pre_send_email())
    });
  };
}


/* Generae template preview */

// --------------Delete template-------------

function async_delete_template (t_key) {
  return axios_calls('Delete', '/message/get_email_template/HR', {
    message_key:t_key
  });
}

export function delete_template (t_key) {
  return function (dispatch, getState) {
    return new Promise((resolve, reject) => {
      dispatch(show_loading());
      async_delete_template(t_key).then((json) => {
        dispatch(hide_loading());
        if (json.error) {
          reject(json);
        } else {
          dispatch(get_templates());
          resolve(json.data.Message);
        }
      }, (error) => {
        dispatch(hide_loading());
        reject('error occurs!!');
      }
			);
    });
  };
}

// ------------send_mail functionality------------

function async_send_mail (email) {
  return fireAjax('POST', '', {
    action: 'send_employee_email',
    email:  email
  });
}

export function send_mail (email) {
  return function (dispatch, getState) {
    return new Promise((resolve, reject) => {
      dispatch(show_loading());
      async_send_mail(email).then((json) => {
        dispatch(hide_loading());
        if (json.error) {
          reject(json.data.message);
        } else {
          dispatch(get_templates());
          resolve(json.data.message);
        }
      }, (error) => {
        dispatch(hide_loading());
        reject('error occurs!!');
      }
			);
    });
  };
}

// -------------------Download PDF ------------

function async_download_template (template, fileName) {
  return fireAjax('POST', '', {
    action:    'create_pdf',
    template:  template,
    file_name: fileName
  });
}
export function download_pdf (template, fileName) {
  return function (dispatch, getState) {
    return new Promise((resolve, reject) => {
      dispatch(show_loading());
      async_download_template(template, fileName).then((json) => {
        dispatch(hide_loading());
        resolve(json.data);
      }, (error) => {
        dispatch(hide_loading());
        reject('error occurs!!');
      }
				);
    });
  };
}

//------------------------get system and user variables----------------
export function async_get_SystemAndUserVariable(){
  return axios_calls("GET","/message/special_variable");
}

export function success_get_SystemAndUserVariable (data) {
  return createAction(constants.ACTION_SUCCESS_GET_SYSTEM_AND_USER_VARIABLE)(data);
}

export function fetchSystemAndUserVariable () {
  return function (dispatch, getState) {
    return new Promise((resolve, reject) => {
      dispatch(show_loading());
      async_get_SystemAndUserVariable().then((data) => {
        dispatch(hide_loading());
        if (typeof data.data !== 'undefined' && data.data.length > 0) {
          dispatch(success_get_SystemAndUserVariable(data.data));
          resolve(data.data);
        } else {
          dispatch(success_get_SystemAndUserVariable([]));
        }
      }, (error) => {
        dispatch(hide_loading()); // hide loading icon
        reject('error occurs!!');
      }
			);
    });
  };
}
/* export function fetchVariable(){
	return (dispatch,getState)=>{
		return new Promise((resolve,reject)=>{
			Meteor.call('fetchAllVariable',(err,data)=>{
				if(err){
					reject(err)
				}else{
					if(data.length > 0){
						dispatch(success_fetch_variable(data))
						resolve('variable loading completed')
					}else{
						dispatch(success_fetch_variable(data))
						resolve('No variable in database')
					}
				}
			})
		})
	}
}

export function deleteVariable( id ){
	return (dispatch,getState) => {
		return new Promise( (resolve,reject) => {
			Meteor.call('deleteVariable', id , (err, data) => {
				if(err){
					reject(err)
				}else{
					dispatch ( fetchVariable(data) )
					resolve(data)
				}
			})
		})
	}
}

export function success_fetch_variable( data ){
	return createAction( ACTION_SUCCESS_FETCH_VARIABLE )( data )
}*/
