import Immutable from 'immutable';

let initialState = {
  'status_message': '',
  'variable':       [],
  'templates':      [],
  'generated_preview':[],
  'sent_email':[],
  'success':false,
  'sytem_and_user_variables':[],
  'add_template_status':'',
  'isLoading':false
};

export function template (state = Immutable.fromJS(initialState), action) {  
  if (action.type === 'ACTION_SUCCESS_VARIABLE_GET') {
    return state.set('variable', action.payload);
  } else if (action.type === 'ACTION_SUCCESS_ADD_VARIABLE') {
    return state.set('status_message', action.payload);
  } else if (action.type === 'ACTION_ERROR_ADD_VARIABLE') {
    return state.set('status_message', action.payload);
  } else if (action.type === 'ACTION_REQUEST_GET_TEMPLATES') {
    return state.set('success', false);
  }else if (action.type === 'ACTION_SUCCESS_GET_TEMPLATES') {
    return state.set('templates', action.payload).set('success',true).set('add_template_status',"");
  }else if (action.type === 'GENERATE_PREVIEW_TEMPLATE_SUCCESS') {
    return state.merge({'generated_preview': action.payload,'isLoading':false});;
  }else if (action.type === 'GENERATE_PREVIEW_TEMPLATE_REQUEST') {
    return state.set('isLoading',true);
  }else if (action.type === 'GENERATE_PREVIEW_TEMPLATE_ERROR') {
    return state.set('isLoading',false);
  }else if (action.type === 'SEND_EMAIL_TEMPLATE_REQUEST') {
    return state.set('isLoading',false);
  }else if (action.type === 'SEND_EMAIL_TEMPLATE_SUCCESS') {
    return state.merge({'sent_email': action.payload,'isLoading':false});
  }else if (action.type === 'SEND_EMAIL_TEMPLATE_ERROR') {
    return state.set('isLoading',false);
  }else if (action.type === 'CLEAR_GENERATE_PREVIEW_SENT_EMAIL_TEMPLATE_SUCCESS') {
    return state.set('sent_email', []).set('generated_preview',[]);
  } else if (action.type ==='ACTION_SUCCESS_GET_SYSTEM_AND_USER_VARIABLE'){
    return state.set('sytem_and_user_variables',action.payload);
  }
  else if (action.type ==='ACTION_SUCCESS_ADD_TEMPLATES'){
    return state.set('add_template_status',action.payload);
  }
  else {
    return state.set('status_message', '');
  }
}