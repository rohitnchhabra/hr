let initialState = { userId: "", status: true };

export function userListSidebar(state = initialState, action) {
  if (action.type === "UPDATE_SIDEBAR_SELECTED_USER") {
    return { userId: action.payload.userid, status: action.payload.status };
  } else if (action.type === "ACTION_SUCCESS_USERSLIST") {
    if (action.payload) {
      const userId = !state.userId
        ? action.payload && action.payload[0]
          ? action.payload[0].user_Id
          : null
        : state.userId;
      return { userId };
    } else {
      return state;
    }
  } else {
    return state;
  }
}
