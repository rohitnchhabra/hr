import Immutable from "immutable";

let initialState = {
  unapprovedList: [],
  approvedList: [],
  deviceHistory: {},
  device: [],
  status_message: "",
  deviceList: [],
  statusList: [],
  deviceCountList: {},
  comment: [],
  unassignedDeviceList: [],
  editData: {},
  showTab: false,
  auditData: {},
  auditDataStatus:false,
  inventoryDeleteStatus: {
    error: null,
    message: ""
  },
  addedMachine: {},
  isGetData: false,
  internal_no:"",
  deleteStatus:{},
  tempInventory: [],
  tempInventoryMessage: ''
};

export function manageDevice(state = Immutable.fromJS(initialState), action) {
  if (action.type === "ACTION_SUCCESS_DEVICE_LIST") {
    return state.set("device", action.payload).set("isGetData", true);
  } else if (action.type === "ACTION_EMPTY_DEVICE_LIST") {
    return state.set("device", action.payload).set("isGetData", true);
  } else if (action.type === "ACTION_SUCCESS_ADD_NEW_MACHINE") {
    return state.set("addedMachine", action.payload);
  } else if (action.type === "ACTION_SHOW_TAB") {
    return state.set("showTab", action.payload);
  } else if (action.type === "ACTION_NO_TAB") {
    return state.set("showTab", action.payload);
  } else if (action.type === "ACTION_ERROR_ADD_NEW_MACHINE") {
    return state.set("status_message", action.payload);
  } else if (action.type === "ACTION_SUCCESS_UPDATE_DEVICELIST") {
    return state.set("status_message", action.payload);
  } else if (action.type === "ACTION_SUCCESS_DEVICE_TYPE") {
    return state.set("status_message", action.payload);
  } else if (action.type === "ACTION_SUCCESS_ASSIGN_DEVICE") {
    return state.set("status_message", action.payload);
  } else if (action.type === "ACTION_ERRORR_ASSIGN_DEVICE") {
    return state.set("status_message", action.payload);
  } else if (action.type === "ACTION_SUCCESS_DELETE_DEVICELIST") {
    return state.set("inventoryDeleteStatus", {
      error: action.payload.error,
      message: action.payload.message
    });
  } else if (action.type === "ACTION_ERROR_DELETE_DEVICELIST") {
    return state.set("inventoryDeleteStatus", {
      error: action.payload.error,
      message: action.payload.message
    });
  } else if (action.type === "ACTION_SUCCESS_GET_DEVICE_TYPE_LIST") {
    return state.set("deviceList", action.payload.list).set("internal_no", action.payload.Internal_number);
  } else if (action.type === "ACTION_SUCCESS_DEVICE_COUNT") {
    return state.set("deviceCountList", action.payload);
  } else if (action.type === "ACTION_SUCCESS_GET_DEVICE_STATUS_LIST") {
    return state.set("statusList", action.payload);
  } else if (action.type === "ACTION_SUCCESS_ADD_INVENTORY_COMMENT") {
    return state.set("status_message", action.payload);
  } else if (action.type === "ACTION_ERROR_ADD_INVENTORY_COMMENT") {
    return state.set("status_message", action.payload);
  } else if (action.type === "ACTION_SUCCESS_GET_DEVICELIST") {
    return state.set("deviceHistory", action.payload);
  } else if (action.type === "ACTION_SUCCESS_UPDATE_UNAPPROVED_USER") {
    return state.set("unapprovedList", action.payload.data);
  } else if (action.type === "ACTION_SUCCESS_UPDATE_APPROVED_USER") {
    return state.set("approvedList", action.payload);
  } else if (action.type === "ACTION_SUCCESS_ADD_USER_COMMENT") {
    return state.set("comment", action.payload);
  } else if (action.type === "ACTION_ERROR_ADD_USER_COMMENT") {
    return state.set("comment", action.payload);
  } else if (action.type === "ACTION_SUCCESS_UNASSIGNED_DEVICE_LIST") {
    return state.set("unassignedDeviceList", action.payload);
  } else if (action.type === "ACTION_SUCCESS_EDIT_WITHOUT_API") {
    return state.set("editData", action.payload);
  } else if (action.type === "ACTION_SUCCESS_AUDIT_LIST") {
    return state.set("auditData", action.payload).set("auditDataStatus",false);
  } else if(action.type==="SUCCESS_ADD_INVENTORY_AUDIT"){
    return state.set("auditDataStatus",true);
  }else if (action.type === "DELETE_SOLD_DEVICE_SUCCESS") {
    return state.set("deleteStatus", action.payload);
  } else if (action.type === "DELETE_SOLD_DEVICE_ERROR") {
    return state.set("deleteStatus", action.payload);
  } else if (action.type === "GET_TEMP_UPLOADED_INVENTORY_FILES_SUCCESS") {
    return state.set("tempInventory", action.payload);
  } else if (action.type === "GET_TEMP_UPLOADED_INVENTORY_FILES_ERROR") {
    return state.set("tempInventoryMessage", action.payload);
  }
    else {
    return state;
  }
}
