import { fireAjax } from '../../../services/index';
import { call, put } from 'redux-saga/effects';
import * as actions from '../../../redux/actions';
import {show_loading, hide_loading} from '../../../redux/generic/actions/frontend';

export function* getShowMenuListRequest(action) {
    yield put(show_loading());
    try {
      const response = yield call(fireAjax, "POST", "", {
        action: "get_all_pages"
      });
      if (response) {
        yield put(actions.successShowMenuListStatus(response.data));
      } 
      yield put(hide_loading());
    } catch (e) {
      yield put(actions.errorrShowMenuListStatus("Error Occurs !!"));
      yield put(hide_loading());
    }
  }

  export function* sendSelectedOptionRequest(action){
    try{
      const response = yield call(fireAjax, "POST", "", {
        "action": "update_page_status",
        ...action.payload
      });
      if (response) {
        yield put(actions.sendSelectedOptionSuccess(response.data));
      } 
    }catch(e){
      yield put(actions.sendSelectedOptionError("Error Occurs !!"));

    }
  }
  