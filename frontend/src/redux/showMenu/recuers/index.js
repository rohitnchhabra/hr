import { handleActions } from "redux-actions";
import update from "immutability-helper";
import * as constants from "../../../redux/constants";
import "../../../redux/update";

let initialState = {
  showListMenu: {
    data: [],
    isLoading: false,
    isError: false,
    isSuccess: false,
    message: ""
  },
  showStatus:{
    data: [],
    isLoading: false,
    isError: false,
    isSuccess: false,
    message: ""
  }
};

const requestShowMenuList = (state, action) =>
  update(state, {
    showListMenu: {
      isLoading: { $set: true },
      isError: { $set: false },
      isSuccess: { $set: false },
      message: { $set: "" }
    }
  });
const successShowMenuList = (state, action) =>
  update(state, {
    showListMenu: {
      data: { $set: action.payload },
      isLoading: { $set: false },
      isError: { $set: false },
      isSuccess: { $set: true },
      message: { $set: "" }
    }
  });
const errorShowMenuList = (state, action) =>
  update(state, {
    showListMenu: {
      isLoading: { $set: false },
      isError: { $set: true },
      isSuccess: { $set: false },
      message: { $set: action.payload }
    }
  });

  const sendSelectedOptionRequest = (state, action) =>
  update(state, {
    showStatus: {
      isLoading: { $set: true },
      isError: { $set: false },
      isSuccess: { $set: false },
      message: { $set: "" }
    }
  });
const sendSelectedOptionSuccess = (state, action) =>
  update(state, {
    showStatus: {
      data: { $set: action.payload },
      isLoading: { $set: false },
      isError: { $set: false },
      isSuccess: { $set: true },
      message: { $set: "" }
    }
  });
const sendSelectedOptionError = (state, action) =>
  update(state, {
    showStatus: {
      isLoading: { $set: false },
      isError: { $set: true },
      isSuccess: { $set: false },
      message: { $set: action.payload }
    }
  })

export default handleActions(
  {
    [constants.REQUEST_SHOW_MENU_LIST]: requestShowMenuList,
    [constants.SUCCESS_SHOW_MENU_LIST]: successShowMenuList,
    [constants.ERROR_SHOW_MENU_LIST]: errorShowMenuList,
    [constants.SEND_SELECTED_OPTION_REQUEST]: sendSelectedOptionRequest,
    [constants.SEND_SELECTED_OPTION_SUCCESS]: sendSelectedOptionSuccess,
    [constants.SEND_SELECTED_OPTION_ERROR]: sendSelectedOptionError
  },
  initialState
);
