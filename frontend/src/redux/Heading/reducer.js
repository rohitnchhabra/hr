import { handleActions } from "redux-actions";
import update from "immutability-helper";
import * as constants from "../../redux/constants";
import "../../redux/update";
// import { getSlackSettings } from "../actions";

let initialState = {
  heading: ""
};

const showHeading = (state, action) =>
  update(state, {
    heading: { $set: action.payload }
  });

export default handleActions(
  {
    [constants.SHOW_HEADING]: showHeading
  },
  initialState
);
