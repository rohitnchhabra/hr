import { handleActions } from "redux-actions";
import * as constants from "../constants";
import update from "immutability-helper";
// import "../../../redux/update";

const initialState = {
  sidebarOpened: false,
  sidebarStatic: false,
  activeItem: "/app/home"
  // JSON.parse(localStorage.getItem("staticSidebar"))
  //   ? window.location.pathname
  //   : null
};

const handleToggleSidebar = (state, action) =>
  update(state, {
    sidebarStatic: { $set: !state.sidebarStatic }
  });

const handleOpenSidebar = (state, action) =>
  update(state, {
    sidebarOpened: { $set: true }
  });

const handleCloseSidebar = (state, action) =>
  update(state, {
    sidebarOpened: { $set: false }
  });

const handleActiveSidebarItem = (state, action) =>
  update(state, {
    activeItem: { $set: action.payload }
  });

export default handleActions(
  {
    [constants.TOGGLE_SIDEBAR]: handleToggleSidebar,
    [constants.OPEN_SIDEBAR]: handleOpenSidebar,
    [constants.CLOSE_SIDEBAR]: handleCloseSidebar,
    [constants.CHANGE_ACTIVE_SIDEBAR_ITEM]: handleActiveSidebarItem
  },
  initialState
);
