import Immutable from 'immutable';

let initialState = {
  userProfileImage:    '',
  userName:            '',
  userjobtitle:        '',
  userid:              '',
  month:               '',
  year:                '',
  monthName:           '',
  nextMonth:           {},
  previousMonth:       {},
  monthSummary:        {},
  attendance:          [],
  compensationSummary: '',
  isAttendanceLoading: false,
};

export function monthlyAttendance (state = Immutable.fromJS(initialState), action) {
  if (action.type === 'ACTION_SUCCESS_USER_ATTENDANCE') {
    return state.set('userName', action.payload.userName)
        .set('userProfileImage', action.payload.userProfileImage)
        .set('userjobtitle', action.payload.userjobtitle)
        .set('userid', action.payload.userid)
        .set('month', action.payload.month)
        .set('year', action.payload.year)
        .set('monthName', action.payload.monthName)
        .set('monthSummary', action.payload.monthSummary)
        .set('nextMonth', action.payload.nextMonth)
        .set('previousMonth', action.payload.previousMonth)
        .set('compensationSummary', action.payload.compensationSummary)
        .set('attendance', action.payload.attendance)
        .set('isAttendanceLoading',false);
  } else if (action.type === 'ACTION_EMPTY_USER_ATTENDANCE') {
    return state;
  } else if (action.type === 'ACTION_ERROR_USER_ATTENDANCE') {
    return state.set('isAttendanceLoading',false);
  } else if (action.type === 'REQUEST_USER_ATTENDANCE') {
    return state.set('isAttendanceLoading',true);
  }
  else {
    return state;
  }
}
