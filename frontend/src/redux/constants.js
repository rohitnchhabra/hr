// +++LOGIN+++
export const USER_LOGIN_REQUEST = 'USER_LOGIN_REQUEST';
export const USER_LOGIN_SUCCESS = 'ACTION_LOGIN_SUCCESS';
export const USER_LOGIN_FAIL = 'ACTION_LOGIN_FAIL';
export const USER_LOGIN_ERROR = 'ACTION_LOGIN_ERROR';
export const IS_ALREADY_LOGGED_IN = 'IS_ALREADY_LOGGED_IN';

//NAVIGATION
export const TOGGLE_SIDEBAR = 'TOGGLE_SIDEBAR';
export const OPEN_SIDEBAR = 'OPEN_SIDEBAR';
export const CLOSE_SIDEBAR = 'CLOSE_SIDEBAR';
export const CHANGE_ACTIVE_SIDEBAR_ITEM = 'CHANGE_ACTIVE_SIDEBAR_ITEM';

// logout
export const REQUEST_LOGOUT = 'REQUEST_LOGOUT';
export const LOGOUT_SUCCESS = 'LOGOUT_SUCCESS';

// forgot password
export const REQUEST_FORGOT_PASSWORD = 'REQUEST_FORGOT_PASSWORD';
export const SUCCESS_FORGOT_PASSWORD = 'SUCCESS_FORGOT_PASSWORD';
export const ERROR_FORGOT_PASSWORD = 'ERROR_FORGOT_PASSWORD';

// holiday list
export const REQUEST_HOLIDAYSLIST = 'REQUEST_HOLIDAYSLIST';
export const SUCCESS_HOLIDAYSLIST = 'SUCCESS_HOLIDAYSLIST';
export const ERROR_HOLIDAYSLIST = 'ERROR_HOLIDAYSLIST';

//add holiday
export const REQUEST_ADDHOLIDAYS = 'REQUEST_ADDHOLIDAYS';
export const SUCCESS_ADDHOLIDAYS = 'SUCCESS_ADDHOLIDAYS';
export const ERROR_ADDHOLIDAYS = 'ERROR_ADDHOLIDAYS';

//delete holiday
export const REQUEST_DELETEHOLIDAYS = 'REQUEST_DELETEHOLIDAYS';
export const SUCCESS_DELETEHOLIDAYS = 'SUCCESS_DELETEHOLIDAYS';
export const ERROR_DELETEHOLIDAYS = 'ERROR_DELETEHOLIDAYS';

//reset holiday reducer
export const RESET_ADDHOLIDAYREDUCER = 'RESET_ADDHOLIDAYREDUCER';

//holiday type
export const REQUEST_HOLIDAYTYPE = 'REQUEST_HOLIDAYTYPE';
export const SUCCESS_HOLIDAYTYPE = 'SUCCESS_HOLIDAYTYPE';
export const ERROR_HOLIDAYTYPE = 'ERROR_HOLIDAYTYPE';

// monthly attendence
export const REQUEST_USER_ATTENDANCE = 'REQUEST_USER_ATTENDANCE';
export const SUCCESS_USER_ATTENDANCE = 'ACTION_SUCCESS_USER_ATTENDANCE';
export const ERROR_USER_ATTENDANCE = 'ACTION_ERROR_USER_ATTENDANCE';

// attendence REQUEST
export const REQUEST_USER_ATTENDANCE_REQUEST = 'REQUEST_USER_ATTENDANCE_REQUEST';
export const SUCCESS_USER_ATTENDANCE_REQUEST = 'ACTION_SUCCESS_USER_ATTENDANCE_REQUEST';
export const ERROR_USER_ATTENDANCE_REQUEST = 'ACTION_ERROR_USER_ATTENDANCE_REQUEST';
// attendence status
export const REQUEST_USER_ATTENDANCE_STATUS = 'REQUEST_USER_ATTENDANCE_STATUS';
export const SUCCESS_USER_ATTENDANCE_STATUS = 'ACTION_SUCCESS_USER_ATTENDANCE_STATUS';
export const ERROR_USER_ATTENDANCE_STATUS = 'ACTION_ERROR_USER_ATTENDANCE_STATUS';

// get user day summary
export const REQUEST_USER_DAY_SUMMARY = 'REQUEST_USER_DAY_SUMMARY';
export const SUCCESS_USER_DAY_SUMMARY = 'ACTION_SUCCESS_USER_DAY_SUMMARY';
export const ERROR_USER_DAY_SUMMARY = 'ACTION_ERROR_USER_DAY_SUMMARY';

// update user day summary
export const REQUEST_UPDATE_USER_DAY_SUMMARY = 'REQUEST_UPDATE_USER_DAY_SUMMARY';
export const SUCCESS_UPDATE_USER_DAY_SUMMARY = 'ACTION_SUCCESS_UPDATE_USER_DAY_SUMMARY';
export const ERROR_UPDATE_USER_DAY_SUMMARY = 'ACTION_ERROR_UPDATE_USER_DAY_SUMMARY';

// user update day summary --
export const REQUEST_UPDATE_EMP_DAY_SUMMARY = 'REQUEST_UPDATE_EMP_DAY_SUMMARY';
export const SUCCESS_UPDATE_EMP_DAY_SUMMARY = 'ACTION_SUCCESS_UPDATE_EMP_DAY_SUMMARY';
export const ERROR_UPDATE_EMP_DAY_SUMMARY = 'ACTION_ERROR_UPDATE_EMP_DAY_SUMMARY';
// USERS LIST
export const REQUEST_USERSLIST = 'ACTION_EMPTY_USERSLIST';
export const SUCCESS_USERSLIST = 'ACTION_SUCCESS_USERSLIST';
export const ERROR_USERSLIST = 'ACTION_ERROR_USERSLIST';

// POLICY DOCUMENTS
export const REQUEST_POLICY_DOCUMENT = 'REQUEST_POLICY_DOCUMENT';

export const REQUEST_USER_POLICY_DOCUMENT = 'REQUEST_USER_POLICY_DOCUMENT';
export const SUCCESS_POLICY_DOCUMENT = 'SUCCESS_POLICY_DOCUMENT';
export const ERROR_POLICY_DOCUMENT = 'ERROR_POLICY_DOCUMENT';

export const REQUEST_SUBMIT_DOCS = 'REQUEST_SUBMIT_DOCS';
export const SUCCESS_SUBMIT_DOCS = 'SUCCESS_SUBMIT_DOCS';
export const ERROR_SUBMIT_DOCS = 'ERROR_SUBMIT_DOCS';

export const REQUEST_UPDATE_READ = 'REQUEST_UPDATE_READ';
export const SUCCESS_UPDATE_READ = 'SUCCESS_UPDATE_READ';
export const ERROR_UPDATE_READ = 'ERROR_UPDATE_READ';

// TEAM LIST
export const REQUEST_TEAM_LIST = 'REQUEST_TEAM_LIST';
export const SUCCESS_TEAM_LIST = 'SUCCESS_TEAM_LIST';
export const ERROR_TEAM_LIST = 'ERROR_TEAM_LIST';

export const REQUEST_ADD_TEAM = 'REQUEST_ADD_TEAM';
export const SUCCESS_ADD_TEAM = 'SUCCESS_ADD_TEAM';
export const ERROR_ADD_TEAM = 'ERROR_ADD_TEAM';

export const REQUEST_GET_TEAM = 'REQUEST_GET_TEAM';
export const SUCCESS_GET_TEAM = 'SUCCESS_GET_TEAM';
export const ERROR_GET_TEAM = 'ERROR_GET_TEAM';

export const REQUEST_TEAM_STATS = 'REQUEST_TEAM_STATS';
export const SUCCESS_TEAM_STATS = 'SUCCESS_TEAM_STATS';
export const ERROR_TEAM_STATS = 'ERROR_TEAM_STATS';

// dashboard
export const REQUEST_MONTHLY_REPORT_ALL_USERS = 'REQUEST_MONTHLY_REPORT_ALL_USERS';
export const SUCCESS_MONTHLY_REPORT_ALL_USERS = 'SUCCESS_MONTHLY_REPORT_ALL_USERS';
export const ERROR_MONTHLY_REPORT_ALL_USERS = 'ERROR_MONTHLY_REPORT_ALL_USERS';

export const REQUEST_EMP_LIFE_CYCLE = 'REQUEST_EMP_LIFE_CYCLE';
export const SUCCESS_EMP_LIFE_CYCLE = 'SUCCESS_EMP_LIFE_CYCLE';
export const ERROR_EMP_LIFE_CYCLE = 'ERROR_EMP_LIFE_CYCLE';

export const REQUEST_EMP_HOURS = 'REQUEST_EMP_HOURS';
export const SUCCESS_EMP_HOURS = 'SUCCESS_EMP_HOURS';
export const ERROR_EMP_HOURS = 'ERROR_EMP_HOURS';

export const REQUEST_EMP_MONTHLY_HOURS = 'REQUEST_EMP_MONTHLY_HOURS';
export const SUCCESS_EMP_MONTHLY_HOURS = 'SUCCESS_EMP_MONTHLY_HOURS';
export const ERROR_EMP_MONTHLY_HOURS = 'ERROR_EMP_MONTHLY_HOURS';

export const REQUEST_EMP_PERFORMANCE = 'REQUEST_EMP_PERFORMANCE';
export const SUCCESS_EMP_PERFORMANCE = 'SUCCESS_EMP_PERFORMANCE';
export const ERROR_EMP_PERFORMANCE = 'ERROR_EMP_PERFORMANCE';

// USER List
export const REQUEST_USER_LIST = 'REQUEST_USER_LIST';
export const SUCCESS_USER_LIST = 'SUCCESS_USER_LIST';
export const ERROR_USER_LIST = 'ERROR_USER_LIST';

//* **********************************************************************************
// +++""ADMIN""+++

export const ACTION_SUCCESS_DISABLED_USERSLIST = 'ACTION_SUCCESS_DISABLED_USERSLIST';
export const ACTION_ERROR_DISABLED_USERSLIST = 'ACTION_ERROR_DISABLED_USERSLIST';

// 'INVENTORY
export const ACTION_SUCCESS_ADD_NEW_MACHINE = 'ACTION_SUCCESS_ADD_NEW_MACHINE';
export const ACTION_ERROR_ADD_NEW_MACHINE = 'ACTION_ERROR_ADD_NEW_MACHINE';

// Get DEvice List
export const ACTION_SUCCESS_DEVICE_LIST = 'ACTION_SUCCESS_DEVICE_LIST';
export const ACTION_EMPTY_DEVICE_LIST = 'ACTION_EMPTY_DEVICE_LIST';
export const ACTION_ERROR_DEVICE_LIST = 'ACTION_ERROR_DEVICE_LIST';
export const ACTION_SUCCESS_GET_DEVICELIST = 'ACTION_SUCCESS_GET_DEVICELIST';

// Update DEvice List
export const ACTION_SUCCESS_UPDATE_DEVICELIST = 'ACTION_SUCCESS_UPDATE_DEVICELIST';

// Delete Device List
export const ACTION_SUCCESS_DELETE_DEVICELIST = 'ACTION_SUCCESS_DELETE_DEVICELIST';
export const ACTION_ERROR_DELETE_DEVICELIST = 'ACTION_ERROR_DELETE_DEVICELIST';

// Assign Device
export const ACTION_SUCCESS_ASSIGN_DEVICE = 'ACTION_SUCCESS_ASSIGN_DEVICE';
export const ACTION_ERROR_ASSIGN_DEVICE = 'ACTION_ERROR_ASSIGN_DEVICE';

// Device Type
export const ACTION_SUCCESS_DEVICE_TYPE = 'ACTION_SUCCESS_DEVICE_TYPE';
export const ACTION_ERROR_DEVICE_TYPE = 'ACTION_ERROR_DEVICE_TYPE';

// Device Type List
export const ACTION_SUCCESS_GET_DEVICE_TYPE_LIST = 'ACTION_SUCCESS_GET_DEVICE_TYPE_LIST';

// DEvice Status
export const ACTION_SUCCESS_DEVICE_STATUS = 'ACTION_SUCCESS_DEVICE_STATUS';
export const ACTION_ERROR_DEVICE_STATUS = 'ACTION_ERROR_DEVICE_STATUS';
export const ACTION_SUCCESS_GET_DEVICE_STATUS_LIST = 'ACTION_SUCCESS_GET_DEVICE_STATUS_LIST';
export const ACTION_SUCCESS_DELETE_DEVICE_STATUS_LIST = 'ACTION_SUCCESS_DELETE_DEVICE_STATUS_LIST';
export const ACTION_SUCCESS_DEVICE_COUNT = 'ACTION_SUCCESS_DEVICE_COUNT';

// LEAVE SUMMARY
export const ACTION_SUCCESS_LEAVES_SUMMARY = 'ACTION_SUCCESS_LEAVES_SUMMARY';
export const ACTION_EMPTY_LEAVES_SUMMARY = 'ACTION_EMPTY_LEAVES_SUMMARY';
export const ACTION_ERROR_LEAVES_SUMMARY = 'ACTION_ERROR_LEAVES_SUMMARY';

// MANAGE PAYSLIPS
export const ACTION_SUCCESS_USER_MANAGE_PAYSLIPS_DATA = 'ACTION_SUCCESS_USER_MANAGE_PAYSLIPS_DATA';
export const ACTION_ERROR_USER_MANAGE_PAYSLIPS_DATA = 'ACTION_ERROR_USER_MANAGE_PAYSLIPS_DATA';
// export const ACTION_SUCCESS_TEAM_LIST = 'ACTION_SUCCESS_TEAM_LIST'
// export const ACTION_EMPTY_TEAM_LIST = 'ACTION_EMPTY_TEAM_LIST'
// export const ACTION_ERROR_TEAM_LIST = 'ACTION_ERROR_TEAM_LIST'

// create user payslip
export const ACTION_SUCCESS_CREATE_USER_SALARY_SLIP = 'ACTION_SUCCESS_CREATE_USER_SALARY_SLIP';
export const ACTION_ERROR_CREATE_USER_SALARY_SLIP = 'ACTION_ERROR_CREATE_USER_SALARY_SLIP';
// ------email payslips
export const ACTION_SUCCESS_EMAIL_PAYSLIPS = 'ACTION_SUCCESS_EMAIL_PAYSLIPS';
export const ACTION_ERROR_EMAIL_PAYSLIPS = 'ACTION_ERROR_EMAIL_PAYSLIPS';
// ------save google access token
export const ACTION_SUCCESS_SAVE_GOOGLE_ACCESS_TOKEN = 'ACTION_SUCCESS_SAVE_GOOGLE_ACCESS_TOKEN';
export const ACTION_ERROR_SAVE_GOOGLE_ACCESS_TOKEN = 'ACTION_ERROR_SAVE_GOOGLE_ACCESS_TOKEN';

// MANAGE SALARY
export const ACTION_SUCCESS_USER_SALARY_DETAILS = 'ACTION_SUCCESS_USER_SALARY_DETAILS';
export const ACTION_EMPTY_USER_SALARY_DETAILS = 'ACTION_EMPTY_USER_SALARY_DETAILS';

export const ACTION_SUCCESS_ADD_USER_SALARY = 'ACTION_SUCCESS_ADD_USER_SALARY';
export const ACTION_ERROR_ADD_USER_SALARY = 'ACTION_ERROR_ADD_USER_SALARY';

// holding
export const ACTION_SUCCESS_ADD_USER_HOLDING = 'ACTION_SUCCESS_ADD_USER_HOLDING';
export const ACTION_ERROR_ADD_USER_HOLDING = 'ACTION_ERROR_ADD_USER_HOLDING';

export const ACTION_SUCCESS_DELETE_USER_SALARY = 'ACTION_SUCCESS_DELETE_USER_SALARY';
export const ACTION_ERROR_DELETE_USER_SALARY = 'ACTION_ERROR_DELETE_USER_SALARY';

// MANAGE USERS
export const ACTION_REQUEST_UPDATE_USER_PASSWORD='ACTION_REQUEST_UPDATE_USER_PASSWORD'
export const ACTION_SUCCESS_UPDATE_USER_PASSWORD='ACTION_SUCCESS_UPDATE_USER_PASSWORD'
export const ACTION_ERROR_UPDATE_USER_PASSWORD='ACTION_ERROR_UPDATE_USER_PASSWORD'

export const ACTION_SUCCESS_USER_PROFILE = 'ACTION_SUCCESS_USER_PROFILE';
export const ACTION_EMPTY_USER_PROFILE = 'ACTION_EMPTY_USER_PROFILE';
export const ACTION_ERROR_USER_PROFILE = 'ACTION_ERROR_USER_PROFILE';

export const ACTION_SUCCESS_GET_STEPS = 'ACTION_SUCCESS_GET_STEPS';
export const ACTION_ERROR_GET_STEPS = 'ACTION_ERROR_GET_STEPS';

export const ACTION_SUCCESS_EMPLOYEE_STEPS = 'ACTION_SUCCESS_EMPLOYEE_STEPS';
export const ACTION_ERROR_EMPLOYEE_STEPS = 'ACTION_ERROR_EMPLOYEE_STEPS';

// MANAGE ROLES
export const ACTION_SUCCESS_ADD_ROLE = 'ACTION_SUCCESS_ADD_ROLE';
export const ACTION_ERROR_ADD_ROLE = 'ACTION_ERROR_ADD_ROLE';

export const ACTION_SUCCESS_LIST_ROLES = 'ACTION_SUCCESS_LIST_ROLES';
export const ACTION_EMPTY_LIST_ROLES = 'ACTION_EMPTY_LIST_ROLES';
export const ACTION_ERROR_LIST_ROLES = 'ACTION_ERROR_LIST_ROLES';

export const ACTION_SUCCESS_UPDATE_ROLES = 'ACTION_SUCCESS_UPDATE_ROLES';
export const ACTION_ERROR_UPDATE_ROLES = 'ACTION_ERROR_UPDATE_ROLES';

export const ACTION_SUCCESS_UPDATE_USER_ROLES = 'ACTION_SUCCESS_UPDATE_USER_ROLES';
export const ACTION_ERROR_UPDATE_USER_ROLES = 'ACTION_ERROR_UPDATE_USER_ROLES';

// -------update profile details
export const ACTION_REQUEST_UPDATE_USER_PROFILE_DETAILS = 'ACTION_REQUEST_UPDATE_USER_PROFILE_DETAILS';
export const ACTION_SUCCESS_UPDATE_USER_PROFILE_DETAILS = 'ACTION_SUCCESS_UPDATE_USER_PROFILE_DETAILS';
export const ACTION_ERROR_UPDATE_USER_PROFILE_DETAILS = 'ACTION_ERROR_UPDATE_USER_PROFILE_DETAILS';
// -------add New employee
export const ACTION_SUCCESS_ADD_NEW_EMPLOYEE = 'ACTION_SUCCESS_ADD_NEW_EMPLOYEE';
export const ACTION_ERROR_ADD_NEW_EMPLOYEE = 'ACTION_ERROR_ADD_NEW_EMPLOYEE';
// user documents
export const ACTION_SUCCESS_USER_DOCUMENT = 'ACTION_SUCCESS_USER_DOCUMENT';
export const ACTION_ERROR_USER_DOCUMENT = 'ACTION_ERROR_USER_DOCUMENT';

// MANAGE USER WORKING HOURS

export const ACTION_SUCCESS_USER_WORKING_HOURS = 'ACTION_SUCCESS_USER_WORKING_HOURS';
export const ACTION_EMPTY_USER_WORKING_HOURS = 'ACTION_EMPTY_USER_WORKING_HOURS';
export const ACTION_ERROR_USER_WORKING_HOURS = 'ACTION_ERROR_USER_WORKING_HOURS';

export const ACTION_SUCCESS_ADD_USER_WORKING_HOURS = 'ACTION_SUCCESS_ADD_USER_WORKING_HOURS';
export const ACTION_ERROR_ADD_USER_WORKING_HOURS = 'ACTION_ERROR_ADD_USER_WORKING_HOURS';

// MANAGE USER Pending HOURS

export const ACTION_SUCCESS_USER_PENDING_HOURS = 'ACTION_SUCCESS_USER_PENDING_HOURS';
export const ACTION_EMPTY_USER_PENDING_HOURS = 'ACTION_EMPTY_USER_PENDING_HOURS';
export const ACTION_ERROR_USER_PENDING_HOURS = 'ACTION_ERROR_USER_PENDING_HOURS';

export const ACTION_SUCCESS_ADD_USER_PENDING_HOURS = 'ACTION_SUCCESS_ADD_USER_PENDING_HOURS';
export const ACTION_ERROR_ADD_USER_PENDING_HOURS = 'ACTION_ERROR_ADD_USER_PENDING_HOURS';

// pENDING hOUR lEAVE
export const ACTION_PENDING_LEAVE_SUCCESS = 'ACTION_PENDING_LEAVE_SUCCESS';
export const ACTION_PENDING_LEAVE_FAIL = 'ACTION_PENDING_LEAVE_FAIL';
export const ACTION_PENDING_LEAVE_ERROR = ' ACTION_PENDING_LEAVE_ERROR';
// get days between leaves

export const ACTION_DAYS_BETWEEN_PENDING_LEAVES_SUCCESS = 'ACTION_DAYS_BETWEEN_PENDING_LEAVES_SUCCESS';
export const ACTION_DAYS_BETWEEN_PENDING_LEAVES_FAIL = 'ACTION_DAYS_BETWEEN_PENDING_LEAVES_FAIL';
export const ACTION_DAYS_BETWEEN_PENDING_LEAVES_ERROR = 'ACTION_DAYS_BETWEEN_PENDING_LEAVES_ERROR';

// TEMAPLATES

export const ACTION_REQUEST_GET_TEMPLATES = 'ACTION_REQUEST_GET_TEMPLATES';
export const ACTION_SUCCESS_GET_TEMPLATES = 'ACTION_SUCCESS_GET_TEMPLATES';
export const ACTION_SUCCESS_ADD_VARIABLE = 'ACTION_SUCCESS_ADD_VARIABLE';
export const ACTION_ERROR_ADD_VARIABLE = 'ACTION_ERROR_ADD_VARIABLE';
export const ACTION_SUCCESS_VARIABLE_GET = 'ACTION_SUCCESS_VARIABLE_GET';
// export const ACTION_UPDATE_PROGRESS_STATUS = "ACTION_UPDATE_PROGRESS_STATUS"
// export const UPDATE_TAGID = "UPDATE_TAGID"
export const ACTION_SUCCESS_GET_SYSTEM_AND_USER_VARIABLE = 'ACTION_SUCCESS_GET_SYSTEM_AND_USER_VARIABLE';
export const ACTION_SUCCESS_ADD_TEMPLATES="ACTION_SUCCESS_ADD_TEMPLATES"

// WORKING HOURS SUMMARY
export const ACTION_REQUEST_HOURS_SUMMARY = 'ACTION_REQUEST_HOURS_SUMMARY';
export const ACTION_SUCCESS_HOURS_SUMMARY = 'ACTION_SUCCESS_HOURS_SUMMARY';
export const ACTION_LOADING_HOURS_SUMMARY = 'ACTION_LOADING_HOURS_SUMMARY';
export const ACTION_EMPTY_HOURS_SUMMARY = 'ACTION_EMPTY_HOURS_SUMMARY';
export const ACTION_ERROR_HOURS_SUMMARY = 'ACTION_ERROR_HOURS_SUMMARY';
// Update day working time
export const ACTION_EMPTY_UPDATE_HOURS_SUMMARY = 'ACTION_EMPTY_UPDATE_HOURS_SUMMARY';
export const ACTION_ERROR_UPDATE_HOURS_SUMMARY = 'ACTION_ERROR_UPDATE_HOURS_SUMMARY';

// +++GENERIC+++

// front end

export const ACTION_SHOW_LOADING = 'ACTION_SHOW_LOADING';
export const ACTION_HIDE_LOADING = 'ACTION_HIDE_LOADING';

// +++LEAVE+++

// Apply Leave

export const ACTION_LEAVE_SUCCESS = 'ACTION_LEAVE_SUCCESS';
export const ACTION_LEAVE_FAIL = 'ACTION_LEAVE_FAIL';
export const ACTION_LEAVE_ERROR = 'ACTION_LEAVE_ERROR';
// get days between leaves

export const ACTION_DAYS_BETWEEN_LEAVES_SUCCESS = 'ACTION_DAYS_BETWEEN_LEAVES_SUCCESS';
export const ACTION_DAYS_BETWEEN_LEAVES_FAIL = 'ACTION_DAYS_BETWEEN_LEAVES_FAIL';
export const ACTION_DAYS_BETWEEN_LEAVES_ERROR = 'ACTION_DAYS_BETWEEN_LEAVES_ERROR';

// GET_RH_LIST_COMPENSATION

export const SUCCESS_GET_RH_LIST_COMPENSATION="SUCCESS_GET_RH_LIST_COMPENSATION"
export const ERROR_GET_RH_LIST_COMPENSATION="ERROR_GET_RH_LIST_COMPENSATION"

// LIST LEAVES
export const ACTION_LIST_LEAVES_SUCCESS = 'ACTION_LIST_LEAVES_SUCCESS';
export const ACTION_LIST_LEAVES_EMPTY = 'ACTION_LIST_LEAVES_EMPTY';
export const ACTION_LIST_LEAVES_ERROR = 'ACTION_LIST_LEAVES_ERROR';

// Filter leaves
export const ACTION_SELECT_LEAVE = 'ACTION_SELECT_LEAVE';
export const ACTION_LEAVE_FILTER = 'ACTION_LEAVE_FILTER';

// MANAGE LEAVES
export const ACTION_LEAVE_STATUS_CHANGE_SUCCESS = 'ACTION_LEAVE_STATUS_CHANGE_SUCCESS';
export const ACTION_LEAVE_STATUS_CHANGE_FAIL = 'ACTION_LEAVE_STATUS_CHANGE_FAIL';
export const ACTION_LEAVE_STATUS_CHANGE_ERROR = 'ACTION_LEAVE_STATUS_CHANGE_ERROR';
// my leaves

export const ACTION_LIST_MY_LEAVES_SUCCESS = 'ACTION_LIST_MY_LEAVES_SUCCESS';
export const ACTION_LIST_MY_LEAVES_EMPTY = 'ACTION_LIST_MY_LEAVES_EMPTY';
export const ACTION_LIST_MY_LEAVES_ERROR = 'ACTION_LIST_MY_LEAVES_ERROR';

//RH Stats

export const ACTION_GET_RH_STATS_SUCCESS = 'ACTION_GET_RH_STATS_SUCCESS';
export const ACTION_GET_RH_STATS = 'ACTION_GET_RH_STATS';
export const ACTION_GET_RH_STATS_ERROR = 'ACTION_GET_RH_STATS_ERROR';

// export const ACTION_LOGIN_ERROR = "ACTION_LOGIN_ERROR"

// +++SALARY+++

export const ACTION_SUCCESS_SALARY_DETAILS = 'ACTION_SUCCESS_SALARY_DETAILS';
export const ACTION_EMPTY_SALARY_DETAILS = 'ACTION_EMPTY_SALARY_DETAILS';
export const ACTION_ERROR_USER_SALARY_DETAILS = 'ACTION_ERROR_USER_SALARY_DETAILS';

// +++USER+++

// attendence SUMMARY
export const ACTION_SUCCESS_ATTENDANCE_SUMMARY = 'ACTION_SUCCESS_ATTENDANCE_SUMMARY';
export const ACTION_EMPTY_ATTENDANCE_SUMMARY = 'ACTION_EMPTY_ATTENDANCE_SUMMARY';
export const ACTION_ERROR_ATTENDANCE_SUMMARY = 'ACTION_ERROR_ATTENDANCE_SUMMARY';

// my document
export const ACTION_SUCCESS_MY_DOCUMENT = 'ACTION_SUCCESS_MY_DOCUMENT';
export const ACTION_ERROR_MY_DOCUMENT = 'ACTION_ERROR_MY_DOCUMENT';
// my profile
export const ACTION_LOADING_MY_PROFILE = 'ACTION_LOADING_MY_PROFILE';
export const ACTION_SUCCESS_MY_PROFILE = 'ACTION_SUCCESS_MY_PROFILE';
export const ACTION_EMPTY_MY_PROFILE = 'ACTION_EMPTY_MY_PROFILE';
export const ACTION_ERROR_MY_PROFILE = 'ACTION_ERROR_MY_PROFILE';
// update profile details
export const ACTION_SUCCESS_UPDATE_PROFILE_DETAILS = 'ACTION_SUCCESS_UPDATE_PROFILE_DETAILS';
export const ACTION_ERROR_UPDATE_PROFILE_DETAILS = 'ACTION_ERROR_UPDATE_PROFILE_DETAILS';
// update bank details
export const ACTION_SUCCESS_UPDATE_BANK_DETAILS = 'ACTION_SUCCESS_UPDATE_BANK_DETAILS';
export const ACTION_ERROR_UPDATE_BANK_DETAILS = 'ACTION_ERROR_UPDATE_BANK_DETAILS';
// update password
export const ACTION_SUCCESS_UPDATE_PASSWORD = 'ACTION_SUCCESS_UPDATE_PASSWORD';
export const ACTION_ERROR_UPDATE_PASSWORD = 'ACTION_ERROR_UPDATE_PASSWORD';

//add comment on inventory
export const ACTION_REQUEST_ADD_INVENTORY_COMMENT = 'ACTION_REQUEST_ADD_INVENTORY_COMMENT';
export const ACTION_SUCCESS_ADD_INVENTORY_COMMENT = 'ACTION_SUCCESS_ADD_INVENTORY_COMMENT';
export const ACTION_ERROR_ADD_INVENTORY_COMMENT = 'ACTION_ERROR_ADD_INVENTORY_COMMENT';
export const SUCCESS_ADD_INVENTORY_AUDIT = 'SUCCESS_ADD_INVENTORY_AUDIT';

//unapproved user list
export const ACTION_REQUEST_UPDATE_UNAPPROVED_USER = 'ACTION_REQUEST_UPDATE_UNAPPROVED_USER';
export const ACTION_SUCCESS_UPDATE_UNAPPROVED_USER= 'ACTION_SUCCESS_UPDATE_UNAPPROVED_USER';
export const ACTION_ERROR_UPDATE_UNAPPROVED_USER = 'ACTION_ERROR_UPDATE_UNAPPROVED_USER';

//approve user list
export const ACTION_REQUEST_UPDATE_APPROVED_USER = 'ACTION_REQUEST_UPDATE_APPROVED_USER';
export const ACTION_SUCCESS_UPDATE_APPROVED_USER= 'ACTION_SUCCESS_UPDATE_APPROVED_USER';

export const ACTION_ERROR_UPDATE_APPROVED_USER = 'ACTION_ERROR_UPDATE_APPROVED_USER';
//unassigned device list
export const REQUEST_UNASSIGNED_DEVICE_LIST = 'REQUEST_UNASSIGNED_DEVICE_LIST';
export const ACTION_SUCCESS_UNASSIGNED_DEVICE_LIST = 'ACTION_SUCCESS_UNASSIGNED_DEVICE_LIST';
export const ACTION_ERROR_UNASSIGNED_DEVICE_LIST = 'ACTION_ERROR_UNASSIGNED_DEVICE_LIST';
// add user comment to unassign inventory
export const REQUEST_ADD_USER_COMMENT = 'REQUEST_ADD_USER_COMMENT';
export const ACTION_SUCCESS_ADD_USER_COMMENT = 'ACTION_SUCCESS_ADD_USER_COMMENT';
export const ACTION_ERROR_ADD_USER_COMMENT = 'ACTION_ERROR_ADD_USER_COMMENT';

export const ACTION_REQUEST_EDIT_WITHOUT_API='ACTION_REQUEST_EDIT_WITHOUT_API';
export const ACTION_SUCCESS_EDIT_WITHOUT_API='ACTION_SUCCESS_EDIT_WITHOUT_API';
export const ACTION_ERROR_EDIT_WITHOUT_API='ACTION_ERROR_EDIT_WITHOUT_API';

export const REQUEST_ADD_NEW_USER_DETAILS = 'REQUEST_ADD_NEW_USER_DETAILS';
export const ACTION_SUCCESS_ADD_NEW_USER_DETAILS = 'ACTION_SUCCESS_ADD_NEW_USER_DETAILS';
export const ACTION_ERROR_ADD_NEW_USER_DETAILS = 'ACTION_ERROR_ADD_NEW_USER_DETAILS';

// Get least active Emp 
export const REQUEST_LEAST_ACTIVE_EMP = 'REQUEST_LEAST_ACTIVE_EMP';
export const SUCCESS_LEAST_ACTIVE_EMP = 'SUCCESS_LEAST_ACTIVE_EMP';
export const ERROR_LEAST_ACTIVE_EMP = 'ERROR_LEAST_ACTIVE_EMP';

export const ACTION_SUCCESS_AUDIT_LIST = 'ACTION_SUCCESS_AUDIT_LIST';
export const ACTION_ERROR_AUDIT_LIST = 'ACTION_ERROR_AUDIT_LIST';

export const REQUEST_HEALTH_STATS = 'REQUEST_HEALTH_STATS';
export const SUCCESS_HEALTH_STATS = 'SUCCESS_HEALTH_STATS';
export const ERROR_HEALTH_STATS = 'ERROR_HEALTH_STATS';

export const REQUEST_HEALTH_STATS_SECRET_KEY_LIST = 'REQUEST_HEALTH_STATS_SECRET_KEY_LIST';
export const SUCCESS_HEALTH_STATS_SECRET_KEY_LIST = 'SUCCESS_HEALTH_STATS_SECRET_KEY_LIST';
export const ERROR_HEALTH_STATS_SECRET_KEY_LIST = 'ERROR_HEALTH_STATS_SECRET_KEY_LIST';

export const REQUEST_HEALTH_STATS_ADD_SECRET_KEY = 'REQUEST_HEALTH_STATS_ADD_SECRET_KEY';
export const SUCCESS_HEALTH_STATS_ADD_SECRET_KEY = 'SUCCESS_HEALTH_STATS_ADD_SECRET_KEY';
export const ERROR_HEALTH_STATS_ADD_SECRET_KEY = 'ERROR_HEALTH_STATS_ADD_SECRET_KEY';

export const REQUEST_HEALTH_STATS_REGENERATE_SECRET_KEY = 'REQUEST_HEALTH_STATS_REGENERATE_SECRET_KEY';
export const SUCCESS_HEALTH_STATS_REGENERATE_SECRET_KEY = 'SUCCESS_HEALTH_STATS_REGENERATE_SECRET_KEY';
export const ERROR_HEALTH_STATS_REGENERATE_SECRET_KEY = 'ERROR_HEALTH_STATS_REGENERATE_SECRET_KEY';

export const REQUEST_HEALTH_STATS_DELETE_SECRET_KEY = 'REQUEST_HEALTH_STATS_DELETE_SECRET_KEY';
export const SUCCESS_HEALTH_STATS_DELETE_SECRET_KEY = 'SUCCESS_HEALTH_STATS_DELETE_SECRET_KEY';
export const ERROR_HEALTH_STATS_DELETE_SECRET_KEY = 'ERROR_HEALTH_STATS_DELETE_SECRET_KEY';

export const DELETE_HEALTH_STATS = 'DELETE_HEALTH_STATS';
export const SUCCESS_DELETE_HEALTH_STATS = 'SUCCESS_DELETE_HEALTH_STATS';
export const ERROR_DELETE_HEALTH_STATS = 'ERROR_DELETE_HEALTH_STATS';

export const REQUEST_STATS_HISTORY = 'REQUEST_STATS_HISTORY';
export const SUCCESS_STATS_HISTORY = 'SUCCESS_STATS_HISTORY';
export const ERROR_STATS_HISTORY = 'ERROR_STATS_HISTORY';

export const REQUEST_STATS_LEAVE_HISTORY = 'REQUEST_STATS_LEAVE_HISTORY';
export const SUCCESS_STATS_LEAVE_HISTORY = 'SUCCESS_STATS_LEAVE_HISTORY';
export const ERROR_STATS_LEAVE_HISTORY = 'ERROR_STATS_LEAVE_HISTORY';
export const UPDATE_FLAG = 'UPDATE_FLAG';


export const REQUEST_GET_ATTANDANCE_UPLOAD_SETTING = 'REQUEST_GET_ATTANDANCE_UPLOAD_SETTING';
export const SUCCESS_GET_ATTANDANCE_UPLOAD_SETTING = 'SUCCESS_GET_ATTANDANCE_UPLOAD_SETTING';
export const ERROR_GET_ATTANDANCE_UPLOAD_SETTING = 'ERROR_GET_ATTANDANCE_UPLOAD_SETTING';

export const REQUEST_ADD_ATTANDANCE_UPLOAD_SETTING = 'REQUEST_ADD_ATTANDANCE_UPLOAD_SETTING';
export const SUCCESS_ADD_ATTANDANCE_UPLOAD_SETTING = 'SUCCESS_ADD_ATTANDANCE_UPLOAD_SETTING';
export const ERROR_ADD_ATTANDANCE_UPLOAD_SETTING = 'ERROR_ADD_ATTANDANCE_UPLOAD_SETTING';

export const REQUEST_DELETE_ATTANDANCE_UPLOAD_SETTING = 'REQUEST_DELETE_ATTANDANCE_UPLOAD_SETTING';
export const SUCCESS_DELETE_ATTANDANCE_UPLOAD_SETTING = 'SUCCESS_DELETE_ATTANDANCE_UPLOAD_SETTING';
export const ERROR_DELETE_ATTANDANCE_UPLOAD_SETTING = 'ERROR_DELETE_ATTANDANCE_UPLOAD_SETTING';

export const REQUEST_GET_ALL_EMPLOYEE_DETAILS = 'REQUEST_GET_ALL_EMPLOYEE_DETAILS';
export const SUCCESS_GET_ALL_EMPLOYEE_DETAILS = 'SUCCESS_GET_ALL_EMPLOYEE_DETAILS';
export const ERROR_GET_ALL_EMPLOYEE_DETAILS = 'ERROR_GET_ALL_EMPLOYEE_DETAILS';

export const REQUEST_RESET_PASSWORD_SETTING = 'REQUEST_RESET_PASSWORD_SETTING';
export const SUCCESS_RESET_PASSWORD_SETTING = 'SUCCESS_RESET_PASSWORD_SETTING';
export const ERROR_RESET_PASSWORD_SETTING = 'ERROR_RESET_PASSWORD_SETTING';

export const REQUEST_RESET_PASSWORD_STATUS = 'REQUEST_RESET_PASSWORD_STATUS';
export const SUCCESS_RESET_PASSWORD_STATUS = 'SUCCESS_RESET_PASSWORD_STATUS';
export const ERROR_RESET_PASSWORD_STATUS = 'ERROR_RESET_PASSWORD_STATUS';

export const REQUEST_CLEAR_RESET_PASSWORD_DATA = 'REQUEST_CLEAR_RESET_PASSWORD_DATA';
export const SUCCESS_CLEAR_RESET_PASSWORD_DATA = 'SUCCESS_CLEAR_RESET_PASSWORD_DATA';

export const SHOW_INVENTOEY_PENDING="SHOW_INVENTOEY_PENDING"; 

export const REQUEST_RH_LIST_SUCCESS = 'REQUEST_RH_LIST_SUCCESS';
export const REQUEST_RH_LIST_EMPTY = 'REQUEST_RH_LIST_EMPTY';
export const REQUEST_RH_LIST_ERROR = 'REQUEST_RH_LIST_ERROR';

export const REQUEST_RH_STATUS_SUCCESS = 'REQUEST_RH_STATUS_SUCCESS';
export const REQUEST_RH_STATUS_ERROR = 'REQUEST_RH_STATUS_ERROR';

export const REQUEST_SHOW_MENU_LIST = 'REQUEST_SHOW_MENU_LIST';
export const SUCCESS_SHOW_MENU_LIST= 'SUCCESS_SHOW_MENU_LIST';
export const ERROR_SHOW_MENU_LIST = 'ERROR_SHOW_MENU_LIST';


export const SEND_SELECTED_OPTION_REQUEST="SEND_SELECTED_OPTION_REQUEST";
export const SEND_SELECTED_OPTION_SUCCESS="SEND_SELECTED_OPTION_SUCCESS";
export const SEND_SELECTED_OPTION_ERROR="SEND_SELECTED_OPTION_ERROR";

//user list sidebar
export const UPDATE_SIDEBAR_SELECTED_USER = "UPDATE_SIDEBAR_SELECTED_USER";

// export const REMOVE_USER_ROLE_REQUEST = "REMOVE_USER_ROLE_REQUEST";
export const REMOVE_USER_ROLE_SUCCESS = "REMOVE_USER_ROLE_SUCCESS";
export const REMOVE_USER_ROLE_ERROR = "REMOVE_USER_ROLE_ERROR";

export const ACTION_CHANGE_COLLAPSE_SELECTED = "ACTION_CHANGE_COLLAPSE_SELECTED";

export const GET_GENRIC_LOGIN_CONFIGURATION = "GET_GENRIC_LOGIN_CONFIGURATION";
export const GET_GENRIC_LOGIN_CONFIGURATION_SUCCESS = "GET_GENRIC_LOGIN_CONFIGURATION_SUCCESS";
export const GET_GENRIC_LOGIN_CONFIGURATION_ERROR = "GET_GENRIC_LOGIN_CONFIGURATION_ERROR";
export const SET_GENRIC_LOGIN_CONFIGURATION = "SET_GENRIC_LOGIN_CONFIGURATION";
export const SET_GENRIC_LOGIN_CONFIGURATION_SUCCESS = "SET_GENRIC_LOGIN_CONFIGURATION_SUCCESS";
export const SET_GENRIC_LOGIN_CONFIGURATION_ERROR = "SET_GENRIC_LOGIN_CONFIGURATION_ERROR";
export const SET_WEB_SALARY_VIEW = "SET_WEB_SALARY_VIEW";
export const SET_WEEKLY_HOLIDAY = "SET_WEEKLY_HOLIDAY";
export const SET_WEEKLY_HOLIDAY_SUCCESS = "SET_WEEKLY_HOLIDAY_SUCCESS";
export const SET_WEEKLY_HOLIDAY_ERROR = "SET_WEEKLY_HOLIDAY_ERROR";
export const SET_SLACK_SETTINGS = "SET_SLACK_SETTINGS";
export const GET_SLACK_SETTINGS = "GET_SLACK_SETTINGS";
export const GET_SLACK_CHANNELS = "GET_SLACK_CHANNELS";
export const GET_SLACK_CHANNELS_SUCCESS = "GET_SLACK_CHANNELS_SUCCESS";
export const GET_SLACK_SETTINGS_SUCCESS = "GET_SLACK_SETTINGS_SUCCESS";
export const SET_SLACK_SETTINGS_SUCCESS = "SET_SLACK_SETTINGS_SUCCESS";
export const TEST_SLACK_MESSAGE = "TEST_SLACK_MESSAGE";
export const TEST_SLACK_MESSAGE_STATUS=" TEST_SLACK_MESSAGE_STATUS";
export const REQUEST_ADD_CHANNEL="REQUEST_ADD_CHANNEL";
export const REQUEST_ADD_CHANNEL_SUCCESS="REQUEST_ADD_CHANNEL_SUCCESS"
export const REQUEST_HIDE_SLACK_CHANNEL="REQUEST_HIDE_SLACK_CHANNEL"
export const REQUEST_GET_SMPTP_SETTING="REQUEST_GET_SMPTP_SETTING"
export const SUCCESS_GET_SMTP_SETTING="SUCCESS_GET_SMTP_SETTING"
export const ERROR_SMTP_SETTING="ERROR_SMTP_SETTING"
export const SAVE_SMTP_SETTING="SAVE_SMTP_SETTING"
export const TEST_SMTP="TEST_SMTP"
export const SUCCESS_TEST_SMTP="SUCCESS_TEST_SMTP"
export const SUCCESS_SAVE_SMTP_SETTING="SUCCESS_SAVE_SMTP_SETTING"
export const CHANGE_PAYSLIP_SETTING="CHANGE_PAYSLIP_SETTING"
export const GET_PAYSLIP_SETTING="GET_PAYSLIP_SETTING"
export const SUCCESS_GET_PAYSLIP_SETTING="SUCCESS_GET_PAYSLIP_SETTING"
export const CHANGE_RH_CONFIG_SETTING="CHANGE_RH_CONFIG_SETTING"
export const ERROR_CHANGE_RH_CONFIG_SETTING="ERROR_CHANGE_RH_CONFIG_SETTING"
export const SUCCESS_CHANGE_RH_CONFIG_SETTING="SUCCESS_CHANGE_RH_CONFIG_SETTING"


/* update config page headings*/
export const UPDATE_CONFIG_PAGE_HEADINGS_REQUEST = "UPDATE_CONFIG_PAGE_HEADINGS_REQUEST"
export const UPDATE_CONFIG_PAGE_HEADINGS_SUCCESS = "UPDATE_CONFIG_PAGE_HEADINGS_SUCCESS"
export const UPDATE_CONFIG_PAGE_HEADINGS_ERROR = "UPDATE_CONFIG_PAGE_HEADINGS_ERROR"

export const UPLOAD_ATTENDANCE = "UPLOAD_ATTENDANCE";
export const UPLOAD_ATTENDANCE_SUCCESS = "UPLOAD_ATTENDANCE_SUCCESS";
export const UPLOAD_ATTENDANCE_ERROR = "UPLOAD_ATTENDANCE_ERROR";

export const GENERATE_PREVIEW_TEMPLATE_REQUEST = "GENERATE_PREVIEW_TEMPLATE_REQUEST";
export const GENERATE_PREVIEW_TEMPLATE_SUCCESS = "GENERATE_PREVIEW_TEMPLATE_SUCCESS";
export const GENERATE_PREVIEW_TEMPLATE_ERROR = "GENERATE_PREVIEW_TEMPLATE_ERROR";

export const SEND_EMAIL_TEMPLATE_REQUEST = "SEND_EMAIL_TEMPLATE_REQUEST";
export const SEND_EMAIL_TEMPLATE_SUCCESS = "SEND_EMAIL_TEMPLATE_SUCCESS";
export const SEND_EMAIL_TEMPLATE_ERROR = "SEND_EMAIL_TEMPLATE_ERROR";

export const SET_ATTENDANCE_LIMIT_REQUEST = "SET_ATTENDANCE_LIMIT_REQUEST";

export const CLEAR_GENERATE_PREVIEW_SENT_EMAIL_TEMPLATE_SUCCESS = "CLEAR_GENERATE_PREVIEW_SENT_EMAIL_TEMPLATE_SUCCESS";

export const SET_CURRENT_DISABLE_USER_REQUEST = "SET_CURRENT_DISABLE_USER_REQUEST";
export const SET_CURRENT_DISABLE_USER_SUCCESS = "SET_CURRENT_DISABLE_USER_SUCCESS";

export const SET_USER_PAYSLIP_MONTH_YEAR_REQUEST = "SET_USER_PAYSLIP_MONTH_YEAR_REQUEST";
export const SET_USER_PAYSLIP_MONTH_YEAR_SUCCESS = "SET_USER_PAYSLIP_MONTH_YEAR_SUCCESS";

export const SET_USER_RESTORE_ROLE_FAILED = "SET_USER_RESTORE_ROLE_FAILED";
export const SET_USER_RESTORE_ROLE_SUCCESS = "SET_USER_RESTORE_ROLE_SUCCESS";


//delete sold device 

export const DELETE_SOLD_DEVICE_REQUEST = "DELETE_SOLD_DEVICE_REQUEST";
export const DELETE_SOLD_DEVICE_SUCCESS = "DELETE_SOLD_DEVICE_SUCCESS";
export const DELETE_SOLD_DEVICE_ERROR = "DELETE_SOLD_DEVICE_ERROR";

export const GET_TEMP_UPLOADED_INVENTORY_FILES_SUCCESS = "GET_TEMP_UPLOADED_INVENTORY_FILES_SUCCESS";
export const GET_TEMP_UPLOADED_INVENTORY_FILES_ERROR = "GET_TEMP_UPLOADED_INVENTORY_FILES_ERROR";
//to notify user for missing timings
export const ACTION_SUCCESS_NOTIFY_EMP_MISSING_TIMINGS = "ACTION_SUCCESS_NOTIFY_EMP_MISSING_TIMINGS";
export const ACTION_ERROR_NOTIFY_EMP_MISSING_TIMINGS = "ACTION_ERROR_NOTIFY_EMP_MISSING_TIMINGS";
export const ACTION_REQUEST_NOTIFY_EMP_MISSING_TIMINGS = "ACTION_REQUEST_NOTIFY_EMP_MISSING_TIMINGS";

//to update employee allocated leaveas
export const UPDATE_EMPLOYEE_ALLOCATED_LEAVES_REQUEST = "UPDATE_EMPLOYEE_ALLOCATED_LEAVES_REQUEST";
export const UPDATE_EMPLOYEE_ALLOCATED_LEAVES_SUCCESS = "UPDATE_EMPLOYEE_ALLOCATED_LEAVES_SUCCESS";
export const UPDATE_EMPLOYEE_ALLOCATED_LEAVES_ERROR = "UPDATE_EMPLOYEE_ALLOCATED_LEAVES_ERROR";

export const USER_TIME_SHEET_SUCCESS = "USER_TIME_SHEET_SUCCESS";
export const USER_TIME_SHEET_ERROR = "USER_TIME_SHEET_ERROR";
export const USER_TIME_SHEET_REQUEST="USER_TIME_SHEET_REQUEST";

export const TIME_SHEET_LIST_REQUEST = "TIME_SHEET_LIST_REQUEST";
export const TIME_SHEET_LIST_SUCCESS = "TIME_SHEET_LIST_SUCCESS";
export const TIME_SHEET_LIST_ERROR = "TIME_SHEET_LIST_ERROR";

export const APPROVE_REJECT_TIME_SHEET_REQUEST = "APPROVE_REJECT_TIME_SHEET_REQUEST";
export const APPROVE_REJECT_TIME_SHEET_SUCCESS = "APPROVE_REJECT_TIME_SHEET_SUCCESS";
export const APPROVE_REJECT_TIME_SHEET_ERROR = "APPROVE_REJECT_TIME_SHEET_ERROR";

export const SEND_TIME_SHEET_DATA_REQUEST="SEND_TIME_SHEET_DATA_REQUEST";
export const SEND_TIME_SHEET_DATA_SUCCESS="SEND_TIME_SHEET_DATA_SUCCESS";
export const SEND_TIME_SHEET_DATA_ERROR="SEND_TIME_SHEET_DATA_ERROR";

// export const UPLOAD_TIMESHEET_FILE="UPLOAD_TIMESHEET_FILE";
// export const UPLOAD_TIMESHEET_FILE_SUCCESS="UPLOAD_TIMESHEET_FILE_SUCCESS";
// export const UPLOAD_TIMESHEET_FILE_ERROR="UPLOAD_TIMESHEET_FILE_ERROR";
export const SHOW_HEADING="SHOW_HEADING"

export const SUBMIT_WEEKLY_TIMESHEET_REQUEST="SUBMIT_WEEKLY_TIMESHEET_REQUEST"
export const SUBMIT_WEEKLY_TIMESHEET_SUCCESS="SUBMIT_WEEKLY_TIMESHEET_SUCCESS"
export const SUBMIT_WEEKLY_TIMESHEET_ERROR="SUBMIT_WEEKLY_TIMESHEET_ERROR"

export const REQUEST_GET_USER_TMS_REPORT="REQUEST_GET_USER_TMS_REPORT"
export const SUCCESS_GET_USER_TMS_REPORT="SUCCESS_GET_USER_TMS_REPORT"
export const ERROR_GET_USER_TMS_REPORT="ERROR_GET_USER_TMS_REPORT"

export const GET_SUBMITTED_TIME_SHEET_REQUEST="GET_SUBMITTED_TIME_SHEET_REQUEST"
export const GET_SUBMITTED_TIME_SHEET_ERROR="GET_SUBMITTED_TIME_SHEET_ERROR"
export const GET_SUBMITTED_TIME_SHEET_SUCCESS="GET_SUBMITTED_TIME_SHEET_SUCCESS"

export const GET_TIME_SHEET_PER_WEEK_REQUEST="GET_TIME_SHEET_PER_WEEK_REQUEST"
export const GET_TIME_SHEET_PER_WEEK_ERROR="GET_TIME_SHEET_PER_WEEK_ERROR"
export const GET_TIME_SHEET_PER_WEEK_SUCCESS="GET_TIME_SHEET_PER_WEEK_SUCCESS"

export const APPROVE_REJECT_FULL_TIME_SHEET_REQUEST = "APPROVE_REJECT_FULL_TIME_SHEET_REQUEST";
export const APPROVE_REJECT_FULL_TIME_SHEET_SUCCESS = "APPROVE_REJECT_FULL_TIME_SHEET_SUCCESS";
export const APPROVE_REJECT_FULL_TIME_SHEET_ERROR = "APPROVE_REJECT_FULL_TIME_SHEET_ERROR";