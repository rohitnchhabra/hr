import Immutable from 'immutable';

let initialState = {
  status_message: '',
  month:          '',
  year:           '',
  monthName:      '',
  nextMonth:      {},
  previousMonth:  {},
  monthSummary:   [],
  isLoading:      false
};

export function workingHoursSummary (state = Immutable.fromJS(initialState), action) {
  if(action.type === "ACTION_REQUEST_HOURS_SUMMARY"){
    return state
        // .set('year', '')
        // .set('monthName', '')
        // .set('monthSummary', '')
        // .set('nextMonth', {})
        // .set('previousMonth', {})
        // .set('status_message', '')
        .set("workingHoursSummaryLoading",true)
  }
  if (action.type === 'ACTION_SUCCESS_HOURS_SUMMARY') {
    return state.set('month', action.payload.month)
        .set('year', action.payload.year)
        .set('monthName', action.payload.monthName)
        .set('monthSummary', action.payload.monthSummary)
        .set('nextMonth', action.payload.nextMonth)
        .set('previousMonth', action.payload.previousMonth)
        .set('status_message', '')
        .set('isLoading', false);
  } else if (action.type === 'ACTION_EMPTY_HOURS_SUMMARY' || action.type === 'ACTION_EMPTY_UPDATE_HOURS_SUMMARY') {
    return state.set('status_message', action.payload);
  } else if (action.type === 'ACTION_ERROR_HOURS_SUMMARY') {
    return state.set('status_message', '')
                .set('isLoading', false);
  }else if(action.type === 'ACTION_LOADING_HOURS_SUMMARY'){
    return state.set('isLoading', true);
  } else {
    return state.set('status_message', '');
  }
}
