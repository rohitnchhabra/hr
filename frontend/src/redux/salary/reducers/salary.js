import Immutable from "immutable";

let initialState = {
  salary_history: [],
  payslip_history: []
};

export function salary(state = Immutable.fromJS(initialState), action) {
  if (action.type === "ACTION_SUCCESS_SALARY_DETAILS") {
    const sortedSalary = action.payload && action.payload.salary_details && action.payload.salary_details.sort((a, b) => {
      return new Date(b.date) - new Date(a.date);
    });
    return state
      .set("salary_history", sortedSalary)
      .set("payslip_history", action.payload.payslip_history ? action.payload.payslip_history : []);
  } else if (action.type === "ACTION_EMPTY_SALARY_DETAILS") {
    return state.set("salary_history", []);
  } else {
    return state;
  }
}
