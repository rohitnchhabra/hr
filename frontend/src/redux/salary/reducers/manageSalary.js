import Immutable from 'immutable';

let initialState = {
  'status_message':   '',
  'salary_structure': {},
  'updated_employee_allocated_leaves':{},
  'updated_employee_allocated_leaves_loader':false
};

export function manageSalary (state = Immutable.fromJS(initialState), action) {
  if (action.type === 'ACTION_SUCCESS_USER_SALARY_DETAILS') {
    return state.set('salary_structure', action.payload);
  } else if (action.type === 'ACTION_EMPTY_USER_SALARY_DETAILS') {
    return state.set('salary_structure', action.payload);
  } else if (action.type === 'ACTION_SUCCESS_ADD_USER_SALARY') {
    return state.set('status_message', action.payload);
  } else if (action.type === 'ACTION_ERROR_ADD_USER_SALARY') {
    return state.set('status_message', action.payload);
  } else if (action.type === 'ACTION_SUCCESS_ADD_USER_HOLDING') {
    return state.set('status_message', action.payload);
  } else if (action.type === 'ACTION_ERROR_ADD_USER_HOLDING') {
    return state.set('status_message', action.payload);
  }else if(action.type === 'UPDATE_EMPLOYEE_ALLOCATED_LEAVES_REQUEST'){
    return state.set('updated_employee_allocated_leaves', {})
    .set('updated_employee_allocated_leaves_loader',true);
  }else if(action.type === 'UPDATE_EMPLOYEE_ALLOCATED_LEAVES_SUCCESS'){
    return state.set('updated_employee_allocated_leaves', action.payload)
    .set('updated_employee_allocated_leaves_loader',false);
  }else if(action.type === 'UPDATE_EMPLOYEE_ALLOCATED_LEAVES_ERROR'){
    return state.set('updated_employee_allocated_leaves', action.payload)
    .set('updated_employee_allocated_leaves_loader',false);
  }
   else {
    return state.set('status_message', '');
  }
}
