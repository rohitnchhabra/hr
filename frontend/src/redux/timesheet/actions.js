import { fireAjax } from "../../services/index";
import { call, put } from "redux-saga/effects";
import * as actions from "../../redux/actions";
import ClearFix from "material-ui/internal/ClearFix";

export function* userTimeSheetRequest(action) {
  try {
    const response = yield call(fireAjax, "POST", "", {
      action: "get_user_timesheet",
      ...action.payload
    });

    if (response) {
      yield put(actions.userTimeSheetSuccess(response));
    } else {
      yield put(actions.userTimeSheetError(response.data));
    }
  } catch (e) {
    yield put(
      actions.userTimeSheetError({ error: 1, message: "Error Occurs!!" })
    );
  }
}

export function* getTimeSheetPerWeekRequest(action) {
  try {
    const response = yield call(fireAjax, "POST", "", {
      action: "pending_timesheets_per_month",
      ...action.payload
    });

    if (response) {
      yield put(actions.getTimeSheetPerWeekSuccess(response));
    } else {
      yield put(actions.getTimeSheetPerWeekError(response.data));
    }
  } catch (e) {
    yield put(
      actions.getTimeSheetPerWeekError({ error: 1, message: "Error Occurs!!" })
    );
  }
}


export function* timeSheetListRequest(action) {
  try {
    const response = yield call(fireAjax, "POST", "", {
      action: "get_all_user_timesheet",
      ...action.payload
    });
    if (response.error === 0) {
      yield put(actions.userTimeSheetSuccess(response.data));
    } else {
      yield put(actions.userTimeSheetError(response.data));
    }
  } catch (e) {
    yield put(
      actions.userTimeSheetError({ error: 1, message: "Error Occurs!!" })
    );
  }
}

export function* sendTimeSheetDataRequest(action) {
  const {
    date,
    user_id,
    applied_date,
    fileId,
    comments,
    from_date,
    hours
  } = action.payload;
  var data = {
    date,
    user_id,
    applied_on: applied_date,
    fileId: fileId,
    comments,
    hours
  };
  if (!fileId) {
    delete data["fileId"];
  }
  try {
    const response = yield call(fireAjax, "POST", "", {
      action: "user_timesheet_entry",
      ...data
    });
    if (response) {
      yield put(actions.sendTimeSheetDataSuccess(response));
      yield put(actions.userTimeSheetRequest({ from_date, user_id }));
    } else {
      yield put(actions.sendTimeSheetDataError(response.data));
    }
  } catch (e) {
    yield put(
      actions.sendTimeSheetDataError({ error: 1, message: "Error Occurs!!" })
    );
  }
}

export function* approveRejectTimeSheetRequest(action) {
  try {
    const response = yield call(fireAjax, "POST", "", {
      action: "update_user_timesheet_status",
      ...action.payload
    });
    if (response.error === 0) {
      yield put(actions.approveRejectTimeSheetSuccess(response.data));
    } else {
      yield put(actions.approveRejectTimeSheetError(response.data));
    }
  } catch (e) {
    yield put(
      actions.approveRejectTimeSheetError({
        error: 1,
        message: "Error Occurs!!"
      })
    );
  }
}

export function* submitWeeklyTimeSheetRequest(action) {
  try {
    const response = yield call(fireAjax, "POST", "", {
      action: "submit_timesheet",
      ...action.payload
    });
    if (response) {
      yield put(actions.submitWeeklyTimeSheetSuccess(response));
      yield put(
        actions.userTimeSheetRequest({
          from_date: action.payload.from_date,
          user_id: action.payload.user_id
        })
      );
    } else {
      yield put(actions.submitWeeklyTimeSheetError(response.data));
    }
  } catch (e) {
    yield put(
      actions.submitWeeklyTimeSheetError({
        error: 1,
        message: "Error Occurs!!"
      })
    );
  }
}

export function* requestGetUserTmsReport(action) {
  try {
    const response = yield call(fireAjax, "POST", "", {
      action: "get_user_tms_report",
      ...action.payload
    });
    if (response.error == 0) {
      yield put(actions.successGetUserTmsReport(response.data));
    } else {
      yield put(actions.errorGetUserTmsReport(response.data));
    }
  } catch (e) {
    yield put(
      actions.errorGetUserTmsReport({ error: 1, message: "Error Occurs!!" })
    );
  }
}

export function* getSubmittedTimeSheetRequest(action) {
  try {
    const response = yield call(fireAjax, "POST", "", {
      action: "get_user_submitted_timesheet",
      ...action.payload
    });
    if (response.error == 0) {
      yield put(actions.getSubmittedTimeSheetSuccess(response.data));
    } else {
      yield put(actions.getSubmittedTimeSheetError(response));
    }
  } catch (e) {
    yield put(
      actions.getSubmittedTimeSheetError({
        error: 1,
        message: "Error Occurs!!"
      })
    );
  }
}

export function* approveRejectFullTimeSheetRequest(action) {
  try {
    const response = yield call(fireAjax, "POST", "", {
      action: "update_user_full_timesheet_status",
      ...action.payload
    });
    if (response.error === 0) {
      yield put(actions.approveRejectFullTimeSheetSuccess(response.data));
      yield put(actions.getSubmittedTimeSheetRequest({user_id: action.payload.user_id,from_date: action.payload.from_date}));
    } else {
      yield put(actions.approveRejectFullTimeSheetError(response.data));
    }
  } catch (e) {
    yield put(
      actions.approveRejectFullTimeSheetError({
        error: 1,
        message: "Error Occurs!!"
      })
    );
  }
}