import { handleActions } from "redux-actions";
import update from "immutability-helper";
import * as constants from "../../redux/constants";
import "../../redux/update";
// import { getSlackSettings } from "../actions";

let initialState = {
  userTimeSheetGetData: {
    isLoading: false,
    isError: false,
    isSuccess: false,
    message: "",
    data: ""
  },
  userTimeSheetSentData: {
    isLoading: false,
    isError: false,
    isSuccess: false,
    message: "",
    data: ""
  },
  approveRejectTimeSheet: {
    isLoading: false,
    isError: false,
    isSuccess: false,
    message: "",
    data: ""
  },
  submitWeeklyTimeSheet: {
    isLoading: false,
    isError: false,
    isSuccess: false,
    message: "",
    data: ""
  },
  tmsReportData: {
    isLoading: false,
    isError: false,
    isSuccess: false,
    message: "",
    data: ""
  },
  submittedTimeSheetData: {
    isLoading: false,
    isError: false,
    isSuccess: false,
    message: "",
    data: ""
  },
  timeSheetPerWeekData: {
    isLoading: false,
    isError: false,
    isSuccess: false,
    message: "",
    data: ""
  }
};
const getTimeSheetPerWeekSuccess = (state, action) =>
  update(state, {
    timeSheetPerWeekData: {
      isLoading: { $set: false },
      isError: { $set: false },
      isSuccess: { $set: true },
      message: { $set: "" },
      data: { $set: action.payload }
    }
  });
const getTimeSheetPerWeekRequest = (state, action) =>
  update(state, {
    timeSheetPerWeekData: {
      isLoading: { $set: true },
      isError: { $set: false },
      isSuccess: { $set: false },
      message: { $set: "" }
    }
  });
const getTimeSheetPerWeekError = (state, action) =>
  update(state, {
    timeSheetPerWeekData: {
      isLoading: { $set: false },
      isError: { $set: true },
      isSuccess: { $set: false },
      message: { $set: "" }
    }
  });

const userTimeSheetSuccess = (state, action) =>
  update(state, {
    userTimeSheetGetData: {
      isLoading: { $set: false },
      isError: { $set: false },
      isSuccess: { $set: true },
      message: { $set: "" },
      data: { $set: action.payload }
    }
  });
const userTimeSheetRequest = (state, action) =>
  update(state, {
    userTimeSheetGetData: {
      isLoading: { $set: true },
      isError: { $set: false },
      isSuccess: { $set: false },
      message: { $set: "" }
    }
  });
const userTimeSheetError = (state, action) =>
  update(state, {
    userTimeSheetGetData: {
      isLoading: { $set: false },
      isError: { $set: true },
      isSuccess: { $set: false },
      message: { $set: "" }
    }
  });
const sendUserTimeSheetDataSuccess = (state, action) =>
  update(state, {
    userTimeSheetSentData: {
      isLoading: { $set: false },
      isError: { $set: false },
      isSuccess: { $set: true },
      message: { $set: "" },
      data: { $set: action.payload }
    }
  });

const sendUserTimeSheetDataError = (state, action) =>
  update(state, {
    userTimeSheetSentData: {
      isLoading: { $set: false },
      isError: { $set: true },
      isSuccess: { $set: false },
      message: { $set: "" },
      data: { $set: action.payload }
    }
  });

const sendUserTimeSheetDataRequest = (state, action) =>
  update(state, {
    userTimeSheetSentData: {
      isLoading: { $set: true },
      isError: { $set: false },
      isSuccess: { $set: false },
      message: { $set: "" }
    }
  });

const approveRejectTimeSheetSuccess = (state, action) =>
  update(state, {
    approveRejectTimeSheet: {
      isLoading: { $set: false },
      isError: { $set: false },
      isSuccess: { $set: true },
      message: { $set: "" },
      data: { $set: action.payload }
    }
  });

const approveRejectTimeSheetError = (state, action) =>
  update(state, {
    approveRejectTimeSheet: {
      isLoading: { $set: false },
      isError: { $set: true },
      isSuccess: { $set: false },
      message: { $set: "" },
      data: { $set: action.payload }
    }
  });

const approveRejectTimeSheetRequest = (state, action) =>
  update(state, {
    approveRejectTimeSheet: {
      isLoading: { $set: true },
      isError: { $set: false },
      isSuccess: { $set: false },
      message: { $set: "" }
    }
  });

const submitWeeklyTimeSheetSuccess = (state, action) =>
  update(state, {
    submitWeeklyTimeSheet: {
      data: { $set: action.payload },
      isLoading: { $set: false },
      isError: { $set: false },
      isSuccess: { $set: true },
      message: { $set: "" }
    }
  });

const submitWeeklyTimeSheetRequest = (state, action) =>
  update(state, {
    submitWeeklyTimeSheet: {
      isLoading: { $set: true },
      isError: { $set: false },
      isSuccess: { $set: true },
      message: { $set: "" }
    }
  });
const submitWeeklyTimeSheetError = (state, action) =>
  update(state, {
    submitWeeklyTimeSheet: {
      data: { $set: action.payload },
      isLoading: { $set: false },
      isError: { $set: true },
      isSuccess: { $set: false },
      message: { $set: "" }
    }
  });

const successGetUserTmsReport = (state, action) =>
  update(state, {
    tmsReportData: {
      data: { $set: action.payload },
      isLoading: { $set: false },
      isError: { $set: false },
      isSuccess: { $set: true },
      message: { $set: "" }
    }
  });
const errorGetUserTmsReport = (state, action) =>
  update(state, {
    tmsReportData: {
      data: { $set: action.payload },
      isLoading: { $set: false },
      isError: { $set: true },
      isSuccess: { $set: false },
      message: { $set: "" }
    }
  });
const requestGetUserTmsReport = (state, action) =>
  update(state, {
    tmsReportData: {
      data: { $set: "" },
      isLoading: { $set: true },
      isError: { $set: false },
      isSuccess: { $set: false },
      message: { $set: "" }
    }
  });

const getSubmittedTimeSheetSuccess = (state, action) =>
  update(state, {
    submittedTimeSheetData: {
      data: { $set: action.payload },
      isLoading: { $set: false },
      isError: { $set: false },
      isSuccess: { $set: true },
      message: { $set: "" }
    }
  });

const getSubmittedTimeSheetError = (state, action) =>
  update(state, {
    submittedTimeSheetData: {
      data: { $set: action.payload },
      isLoading: { $set: false },
      isError: { $set: true },
      isSuccess: { $set: false },
      message: { $set: "" }
    }
  });

  const getSubmittedTimeSheetRequest = (state, action) =>
  update(state, {
    submittedTimeSheetData: {
      isLoading: { $set: true },
      isError: { $set: false },
      isSuccess: { $set: false },
      message: { $set: "" }
    }
  });

export default handleActions(
  {
    [constants.USER_TIME_SHEET_SUCCESS]: userTimeSheetSuccess,
    [constants.USER_TIME_SHEET_REQUEST]: userTimeSheetRequest,
    [constants.USER_TIME_SHEET_ERROR]: userTimeSheetError,
    [constants.SEND_TIME_SHEET_DATA_SUCCESS]: sendUserTimeSheetDataSuccess,
    [constants.SEND_TIME_SHEET_DATA_ERROR]: sendUserTimeSheetDataError,
    [constants.SEND_TIME_SHEET_DATA_REQUEST]: sendUserTimeSheetDataRequest,
    [constants.APPROVE_REJECT_TIME_SHEET_REQUEST]: approveRejectTimeSheetRequest,
    [constants.APPROVE_REJECT_TIME_SHEET_SUCCESS]: approveRejectTimeSheetSuccess,
    [constants.APPROVE_REJECT_TIME_SHEET_ERROR]: approveRejectTimeSheetError,
    [constants.SUBMIT_WEEKLY_TIMESHEET_SUCCESS]: submitWeeklyTimeSheetSuccess,
    [constants.SUBMIT_WEEKLY_TIMESHEET_REQUEST]: submitWeeklyTimeSheetRequest,
    [constants.SUBMIT_WEEKLY_TIMESHEET_ERROR]: submitWeeklyTimeSheetError,
    [constants.SUCCESS_GET_USER_TMS_REPORT]: successGetUserTmsReport,
    [constants.ERROR_GET_USER_TMS_REPORT]: errorGetUserTmsReport,
    [constants.REQUEST_GET_USER_TMS_REPORT]: requestGetUserTmsReport,
    [constants.GET_SUBMITTED_TIME_SHEET_SUCCESS]: getSubmittedTimeSheetSuccess,
    [constants.GET_SUBMITTED_TIME_SHEET_ERROR]: getSubmittedTimeSheetError,
    [constants.GET_SUBMITTED_TIME_SHEET_REQUEST]: getSubmittedTimeSheetRequest,
    [constants.GET_TIME_SHEET_PER_WEEK_SUCCESS]: getTimeSheetPerWeekSuccess,
    [constants.GET_TIME_SHEET_PER_WEEK_ERROR]: getTimeSheetPerWeekError,
    [constants.GET_TIME_SHEET_PER_WEEK_REQUEST]: getTimeSheetPerWeekRequest
  },
  initialState
);
