import { fireAjax } from "../../../services/index";
import { call, put } from "redux-saga/effects";
import * as actions from "../../../redux/actions";
import {axios_calls} from "../../../services/index"
import {
  show_loading,
  hide_loading
} from "../../../redux/generic/actions/frontend";
import { notify } from "../../../services/notify";

export function* getAttendanceUploadSettings(action) {
  yield put(show_loading());
  try {
    const response = yield call(fireAjax, "POST", "", {
      action: "get_attendance_keys"
    });
    if (response.error === 0) {
      yield put(actions.successGetAttendanceUploadSetting(response.data));
    } else if (response.error === 1) {
      yield put(actions.errorGetAttendanceUploadSetting("API response error."));
    }
    yield put(hide_loading());
  } catch (e) {
    yield put(actions.errorGetAttendanceUploadSetting("Error Occurs !!"));
    yield put(hide_loading());
  }
}

export function* addAttendanceUploadSettings(action) {
  yield put(show_loading());
  try {
    const response = yield call(fireAjax, "POST", "", {
      // action: "add_attendance_keys",
      // ...action.payload
      action: "update_config",
      type: "add_attendance_csv",
      data : {
        ...action.payload
      }
    });
    if (response.error === 0) {
      yield put(actions.getGenricLoginConfig());
    } else if (response.error === 1) {
      yield put(actions.errorAddAttendanceUploadSetting(response.data.message));
    }
    yield put(hide_loading());
  } catch (e) {
    yield put(actions.errorAddAttendanceUploadSetting("Error Occurs !!"));
    yield put(hide_loading());
  }
}

export function* deleteAttendanceUploadSettings(action) {
  yield put(show_loading());
  try {
    const response = yield call(fireAjax, "POST", "", {
      // action: "delete_attendance_keys",
      // ...action.payload
      action: "update_config",
      type: "delete_attendance_csv",
      data: {
        ...action.payload
      }
    });
    if (response.error === 0) {
      yield put(actions.getGenricLoginConfig());
    } else if (response.error === 1) {
      yield put(
        actions.errorDeleteAttendanceUploadSetting(response.data.message)
      );
    }
    yield put(hide_loading());
  } catch (e) {
    yield put(actions.errorDeleteAttendanceUploadSetting("Error Occurs !!"));
    yield put(hide_loading());
  }
}

export function* requestResetPasswordSetting(params) {
  yield put(show_loading());
  const response = yield call(fireAjax, "POST", "", {
    action: "update_config",
    type: "reset_password",
    data: {
      pwd_reset_interval: params.payload.selectedOption.value,
      pwd_reset_status: params.payload.toggleActive
    } 
  });
  yield put(hide_loading());
  if (response.error === 0) {
    yield put(actions.getGenricLoginConfig());
  } else {
    notify("Error", response.data.message, "error");
  }
}
export function* setAttendanceLateLimit(params) {
  yield put(show_loading());
  const response = yield call(fireAjax, "POST", "", {
    action: "update_config",
    type: "attendance_late_days",
    data: params.payload.selectedOption.value
  });
  yield put(hide_loading());
  if (response.error === 0) {
    yield put(actions.getGenricLoginConfig());
  } else {
    notify("Error", response.data.message, "error");
  }
}

export function* requestResetPasswordStatus() {
  try {
    const response = yield call(fireAjax, "POST", "", {
      action: "get_reset_password_config"
    });
    if (response.error === 0) {
      yield put(actions.successResetPasswordStatus(response));
    }
  } catch (e) {
    yield put(actions.errorrResetPasswordStatus());
  }
}

export function* requestClearResetPasswordData() {
  yield put(actions.successClearResetPasswordData());
}

export function* getLoginOption(action) {
  yield put (show_loading());
  try {
    const response = yield call(fireAjax, "POST", "", {
      action: "get_generic_configuration"
    });
    if (response.error) {
      yield put(actions.getGenricLoginConfigError());
      yield put(hide_loading());
    } else {
      yield put(actions.getGenricLoginConfigSuccess(response.data));
      yield put(hide_loading());
    }
  } catch (e) {
    yield put(actions.getGenricLoginConfigError());
    yield put (hide_loading());
    console.warn('Some error found in "loginTypes" action\n', e);
  }
}

export function* setLoginOption(action) {
  try {
    const response = yield call(fireAjax, "POST", "", {
      action: "update_config",
      type: "login_types",
      data: action.payload
    });
    if (response.error == 0) {
      yield put(actions.setGenricLoginConfigSuccess());
      yield put(actions.getGenricLoginConfig());
    } else if (response.error == 1) {
      yield put(actions.setGenricLoginConfigError());
      notify("Error", response.data.message, "error");
      yield put(actions.getGenricLoginConfig());
    }
  } catch (e) {
    yield put(actions.setGenricLoginConfigError());
    console.warn('Some error found in "setLoginTypes" action\n', e);
  }
}

export function* setWebSalaryView(action) {
  try {
    const response = yield call(fireAjax, "POST", "", {
      action: "update_config",
      type: "web_show_salary",
      data: action.payload.status
    });
    if (response.error == 0) {
      yield put(actions.getGenricLoginConfig());
    } else if (response.error == 1) {
      notify("Error", response.data.message, "error");
      yield put(actions.getGenricLoginConfig());
    }
  } catch (e) {
    console.warn('Some error found in "setWebSalaryView" action\n', e);
  }
}

export function* setWeeklyHoliday(action) {
  try {
    const response = yield call(fireAjax, "POST", "", {
      action: "update_config",
      type: "alternate_saturday",
      data: action.payload
    });
    if (response.error == 0) {
      yield put(actions.setWeeklyHolidaySuccess());
      yield put(actions.getGenricLoginConfig());
    } else if (response.error == 1) {
      notify("Error", response.data.message, "error");
      yield put(actions.setWeeklyHolidayError());
    }
  } catch (e) {
    console.warn('Some error found in "setWeeklyHoliday" action\n', e);
  }
}


export function* updateConfigPageHeadingsRequest(params) {
  yield put(show_loading());
  
  const response = yield call(fireAjax, "POST", "", {
    action: "update_config",
    type: "page_headings",
    data: JSON.stringify(params.payload)
  });
  yield put(hide_loading());
  if (response.error === 0) {
    yield put(hide_loading());
    yield put(actions.updateConfigPageHeadingsSuccess());
    yield put(actions.getGenricLoginConfig());
  } else {
    notify("Error", response.data.message, "error");
    yield put(hide_loading());
  }
}


export function* requestUploadAttendance(action) {
  try {
    const response = yield call(fireAjax, "POST", "", {
      action: "upload_attendance",
      attendance: action.payload
    });
    if (response.error == 0) {
      yield put(actions.uploadAttendanceSuccess(response));
      yield put(actions.getGenricLoginConfig());
    } else if (response.error == 1) {
      notify("Error", response.data.message, "error");
      yield put(actions.uploadAttendanceError());
    }
  } catch (e) {
    console.warn('Some error found in "uploadAttendance" action\n', e);
  }
}

export function* requestAddChannel(action) {
  yield put(show_loading());
  try {
    var response = yield call(axios_calls, "POST", "/message/slack_channel_test",
    action.payload);
    if(response.status==200){
      yield put(hide_loading());
      yield put(actions.requestAddChannelSuccess({status:false}));
      yield put(actions.testSlackMsgStatus(response.data));
    }
    else{
      yield put(hide_loading());
    }
  } catch (e) {
    yield put(hide_loading());
    console.warn('Some error found in "Slack Channel" action\n', e);
  }
}

export function* setSlackSettings(action) {
  yield put(show_loading());
  try {
    var response = yield call(axios_calls, "PUT", "/slack/settings",
    action.payload);
    if(response.data.status==true){
      yield put(hide_loading());
      yield put(actions.setSlackSettingsSuccess(response.data));
      yield put(actions.getSlackSettings());
      yield put(actions.getSlackChannels());
    }
    else{
      yield put(hide_loading());
    }
  } catch (e) {
    yield put(hide_loading());
    console.warn('Some error found in "Slack Settings" action\n', e);
  }
}

export function* getSlackSettings(action) {
  yield put(show_loading());
  try {
    const response = yield call(axios_calls, "GET", "/slack/settings");
    if(response.status==200){
      yield put(hide_loading());
      yield put(actions.getSlackSettingsSuccess(response));
    }
    else{
      yield put(hide_loading());
    }
  } catch (e) {
    yield put(hide_loading());
    console.warn('Some error found in "Slack Settings" action\n', e);
  }
}

export function* getSlackChannels(action) {
  yield put(show_loading());
  try {
    const response = yield call(axios_calls, "GET", "/slackchannels");
    if(response.status==200){
      yield put(hide_loading());
      yield put(actions.getSlackChannelsSuccess({data:response.data,status:true}));
    }
    else{
      yield put(hide_loading());
    }
  } catch (e) {
    yield put(hide_loading());
    console.warn('Some error found in "Slack Channel" action\n', e);
  }
}

export function* testSlackMsg(action) {
  yield put(show_loading());
  try {
    const response = yield call(axios_calls, "POST", "/notify/slack_test",action.payload);
    if(response.status==200){
      yield put(hide_loading());
      yield put(actions.testSlackMsgStatus(response.data));
    }
    else{
      yield put(hide_loading());
      yield put(actions.testSlackMsgStatus({status:false,message:"Slack User not exist or invalid token"}));
    }
  } catch (e) {
    yield put(hide_loading());
    yield put(actions.testSlackMsgStatus({status:false,message:"Slack User not exist or invalid token"}));
    console.warn('Some error found in "Test Slack Message" action\n', e);
  }
}


export function* requestGetSmtpSettings(action) {
  yield put(show_loading());
  try {
    const response = yield call(axios_calls, "GET", "/smtp/settings/HR");
    if(response.status==200){
      yield put(hide_loading());
      yield put(actions.successGetSmtpSettings(response.data));
    }
    else{
      yield put(hide_loading());
      yield put(actions.errorSmtpSettings({status:false,message:"Error Occurs"}));
    }
  } catch (e) {
    yield put(hide_loading());
      yield put(actions.errorSmtpSettings({status:false,message:"Error Occurs"}));
    console.warn('Some error found in "Test Slack Message" action\n', e);
  }
}

export function* saveSmtpSettings(action) {
  yield put(show_loading());
  try {
    const response = yield call(axios_calls, "POST", "/smtp/settings/HR",action.payload);
    if(response.status==200){
      yield put(hide_loading());
      yield put(actions.requestGetSmtpSettings());
      yield put(actions.successSaveSmtpSettings({status:true,message:"Smtp data has been updated"}));
    }
    else{
      yield put(hide_loading());
      yield put(actions.errorSmtpSettings({status:false,message:"Error Occurs"}));
    }
  } catch (e) {
    yield put(hide_loading());
    yield put(actions.errorSmtpSettings({status:false,message:e.response.data.message}));
    console.warn('Some error found in "SMTP Settings" action\n', e);
  }
}

export function* ontestSmtp(action){
  yield put(show_loading());
  try {
    const response = yield call(axios_calls, "POST", "/notify/mail_test",action.payload);
    if(response.status==200){
      yield put(hide_loading());
      yield put(actions.successTestSmtp(response.data));
    }
    else{
      yield put(hide_loading());
      yield put(actions.errorSmtpSettings({status:false,message:"Smtp invalid or not working"}));
    }
  } catch (e) {
    yield put(hide_loading());
      yield put(actions.errorSmtpSettings({status:false,message:"Smtp invalid or not working"}));
    console.warn('Some error found in "SMTP Settings" action\n', e);
  }
}

export function* changePayslipSettings(action){
  yield put(show_loading());
  try {
    const response = yield call(axios_calls, "PUT", "/settings",action.payload);
    if(response.status==200){
      yield put(hide_loading());
    }
    else{
      yield put(hide_loading());
    }
  } catch (e) {
    yield put(hide_loading());
  }
}
export function* getPayslipSettings(action){
  yield put(show_loading());
  try {
    const response = yield call(axios_calls, "GET", "/settings");
    if(response.status==200){
      yield put(hide_loading());
      yield put(actions.successGetPayslipSettings(response.data));
    }
    else{
      yield put(hide_loading());
    }
  } catch (e) {
    yield put(hide_loading());
  }
}
export function* changeRhConfigSetting(action){
  yield put(show_loading());
  try{
    const response = yield call(fireAjax, "POST", "", {
      action: "update_config",
      type: "rh_config",
      data: action.payload
    });
    if (response.error === 0) {
      yield put(hide_loading());
      yield put(actions.successChangeRhConfigSetting({message:response.data.message,status:true}))
      yield put(actions.getGenricLoginConfig());
    } else {
      yield put(actions.errorChangeRhConfigSetting({message:"Error Occurs",status:false}))
      yield put(hide_loading());
    }
  }
  catch{
    yield put(actions.errorChangeRhConfigSetting({message:"Error Occurs",status:false}))
    yield put(hide_loading());
  }
}





