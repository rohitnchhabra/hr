import { handleActions } from "redux-actions";
import update from "immutability-helper";
import * as constants from "../../../redux/constants";
import "../../../redux/update";
// import { getSlackSettings } from "../actions";

let initialState = {
  addAttendanceUploadSetting: {
    isLoading: false,
    isError: false,
    isSuccess: false,
    message: ""
  },
  deleteAttendanceUploadSetting: {
    isLoading: false,
    isError: false,
    isSuccess: false,
    message: ""
  },
  resetPasswordStatus: {
    isLoading: false,
    isError: false,
    isSuccess: false,
    message: "",
    data: {}
  },
  loginTypes: {
    isLoading: false,
    isError: false,
    isSuccess: false,
    message: "",
    isSettingLoginOption: false,
    data: {}
  },
  showSalary: {
    webShowSalary: ""
  },
  alternative_saturdays: [],
  uploadAttendance: {
    isLoading: false,
    isSuccess: false,
    isError: false
  },
  reset_password: {
    status: "",
    days: "",
    last_updated: "",
  },
  attendanceUploadSetting: {
    time: [],
    user_id: []
  },
  page_headings: [],
  inventory_audit_comments: {},
  attendance_late_days:null,

  slackSettings:{
    slack_token:"",
    slack_message:"",
  },

  slack_channel:{
    slack_channels:"",
    show_slack_channel:false
   },

   smtp_settings:{
     smtp_settings:"",
     smtp_message:""
   },

   payslip_settings:{} ,
   rh_config:{
    rh_config:"",
    status:""
  },

};

const requestAttendanceUploadSetting = (state, action) =>
  update(state, {
    attendanceUploadSetting: {
      isLoading: { $set: true },
      isError: { $set: false },
      isSuccess: { $set: false },
      message: { $set: "" }
    }
  });
const successGetAttendanceUploadSetting = (state, action) =>
  update(state, {
    attendanceUploadSetting: {
      data: { $set: action.payload },
      isLoading: { $set: false },
      isError: { $set: false },
      isSuccess: { $set: true },
      message: { $set: "" }
    }
  });
const errorAttendanceUploadSetting = (state, action) =>
  update(state, {
    attendanceUploadSetting: {
      isLoading: { $set: false },
      isError: { $set: true },
      isSuccess: { $set: false },
      message: { $set: action.payload }
    }
  });

// const requestAddAttendanceUploadSetting = (state, action) => update(state, {
//   addAttendanceUploadSetting: {
//     isLoading: {$set: true},
//     isError:   {$set: false},
//     isSuccess: {$set: false},
//     message:   {$set: ''}
//   }
// });
const successAttendanceUploadSettingActions = (state, action) =>
  update(state, {
    addAttendanceUploadSetting: {
      isLoading: { $set: false },
      isError: { $set: false },
      isSuccess: { $set: true },
      message: { $set: "" }
    }
  });
// const errorAddAttendanceUploadSetting = (state, action) => update(state, {
//   addAttendanceUploadSetting: {
//     isLoading: {$set: false},
//     isError:   {$set: true},
//     isSuccess: {$set: false},
//     message:   {$set: 'Error !'}
//   }
// });

// const requestDeleteAttendanceUploadSetting = (state, action) => update(state, {
//   deleteAttendanceUploadSetting: {
//     isLoading: {$set: true},
//     isError:   {$set: false},
//     isSuccess: {$set: false},
//     message:   {$set: ''}
//   }
// });
// const successDeleteAttendanceUploadSetting = (state, action) => update(state, {
//   deleteAttendanceUploadSetting: {
//     isLoading: {$set: false},
//     isError:   {$set: false},
//     isSuccess: {$set: true},
//     message:   {$set: ''}
//   }
// });
// const errorDeleteAttendanceUploadSetting = (state, action) => update(state, {
//   deleteAttendanceUploadSetting: {
//     isLoading: {$set: false},
//     isError:   {$set: true},
//     isSuccess: {$set: false},
//     message:   {$set: 'Error !'}
//   }
// });

const successResetPasswordStatus = (state, action) =>
  update(state, {
    resetPasswordStatus: {
      isLoading: { $set: false },
      isError: { $set: false },
      isSuccess: { $set: true },
      message: { $set: "Error !" },
      data: { $set: action.payload.data }
    }
  });

const successClearResetPasswordData = (state, action) =>
  update(state, {
    resetPasswordStatus: {
      isLoading: { $set: false },
      isError: { $set: false },
      isSuccess: { $set: true },
      message: { $set: "Error !" },
      data: { $set: {} }
    }
  });

const getGenericConfiguration = (state, action) =>
  update(state, {
    loginTypes: {
      isLoading: { $set: true },
      isError: { $set: false },
      isSuccess: { $set: false },
      message: { $set: "" },
      data: { $set: [] }
    },
    showSalary: {
      webShowSalary: { $set: "" }
    }
  });

const showStatusRhConfigSetting=(state,action)=>
  update(state, {
    rh_config:{
      status:{$set:action.payload}
    }
  });

const getGenericConfigurationSuccess = (state, action) =>
  update(state, {
    rh_config:{
      rh_config:{$set:action.payload.rh_config}
    },
    loginTypes: {
      isLoading: { $set: false },
      isError: { $set: false },
      isSuccess: { $set: true },
      message: { $set: "" },
      data: { $set: action.payload.login_types }
    },
    showSalary: {
      webShowSalary: {
        $set: action.payload.hasOwnProperty("web_show_salary")
          ? action.payload.web_show_salary
          : ""
      }
    },
    alternative_saturdays: {
      $set: action.payload.alternate_saturday
        ? action.payload.alternate_saturday.reverse()
        : []
    },
    reset_password: {
      status: {
        $set:
          (action.payload.reset_password &&
            action.payload.reset_password.status) ||
          ""
      },
      days: {
        $set:
          (action.payload.reset_password &&
            action.payload.reset_password.days) ||
          ""
      },
      last_updated: {
        $set:
          (action.payload.reset_password &&
            action.payload.reset_password.last_updated) ||
          ""
      }
    },
    attendanceUploadSetting: {
      time: {
        $set:
          (action.payload.attendance_csv &&
            action.payload.attendance_csv.time) ||
          []
      },
      user_id: {
        $set:
          (action.payload.attendance_csv &&
            action.payload.attendance_csv.user_id) ||
          []
      }
    },
    page_headings: { $set: action.payload.page_headings },
    inventory_audit_comments: { $set: action.payload.inventory_audit_comments },
    attendance_late_days: { $set: action.payload.attendance_late_days }
  });

  const testSlackMsgStatus = (state, action) =>
  update(state, {
    slackSettings:{
      slack_message:{$set:action.payload}
    },
  }); 

  const getSlackChannelsSuccess = (state, action) =>
  update(state, {
     slack_channel:{
      slack_channels:{$set:action.payload.data},
      show_slack_channel:{$set:action.payload.status}
     }
  }); 
  


const getGenericConfigurationError = (state, action) =>
  update(state, {
    loginTypes: {
      isLoading: { $set: false },
      isError: { $set: true },
      isSuccess: { $set: false },
      message: { $set: "" },
      data: { $set: [] }
    },
    showSalary: {
      webShowSalary: { $set: "" }
    }
  });
const settingLoginTypes = (state, action) =>
  update(state, {
    loginTypes: {
      isSettingLoginOption: { $set: true }
    }
  });
const settingLoginTypesSuccesss = (state, action) =>
  update(state, {
    loginTypes: {
      isSettingLoginOption: { $set: false }
    }
  });

const settingLoginTypesError = (state, action) =>
  update(state, {
    loginTypes: {
      isSettingLoginOption: { $set: false }
    }
  });

const requestUploadAttendance = (state, action) =>
  update(state, {
    uploadAttendance: {
      isLoading: { $set: true },
      isError: { $set: false },
      isSuccess: { $set: false },
      message: { $set: "" }
    }
  });
const successUploadAttendance = (state, action) =>
  update(state, {
    uploadAttendance: {
      data: { $set: action.payload },
      isLoading: { $set: false },
      isError: { $set: false },
      isSuccess: { $set: true },
      message: { $set: "" }
    }
  });
const errorUploadAttendance = (state, action) =>
  update(state, {
    uploadAttendance: {
      isLoading: { $set: false },
      isError: { $set: true },
      isSuccess: { $set: false },
      message: { $set: action.payload }
    }
  });

  const getSlackSettingsSuccess = (state, action) =>
  update(state, {
    slackSettings: {
      slack_token: { $set: action.payload.data.slack_token },
    }
  });

  const requsetAddChannelSuccess = (state, action) =>
  update(state, {
    slack_channel:{
      show_slack_channel: { $set: action.payload.status }
     }
  });
  const requestHideSlackChannel = (state, action) =>
  update(state, {
    slack_channel:{
      show_slack_channel: { $set: action.payload },
     }
  });

  const successGetSmtpSettings=(state,action)=>
  update(state,{
    smtp_settings:{
      smtp_settings:{$set:action.payload}
    }
  })

  const successTestSmtp = (state, action) =>
  update(state, {
    smtp_settings:{
      smtp_message:{$set:action.payload}}
  }); 
  const successSaveSmtpSetting = (state, action) =>
  update(state, {
    smtp_settings:{
      smtp_message:{$set:action.payload}}
  }); 

  const errorSmtpSetting=(state,action)=>update(state,{
    smtp_settings:{
      smtp_message:{$set:action.payload}
    }
  })

  const successGetPayslipSetting=(state,action)=>update(state,{
    payslip_settings:{$set:action.payload}
  })
  
export default handleActions(
  {
    [constants.REQUEST_GET_ATTANDANCE_UPLOAD_SETTING]: requestAttendanceUploadSetting,
    [constants.SUCCESS_GET_ATTANDANCE_UPLOAD_SETTING]: successGetAttendanceUploadSetting,
    [constants.ERROR_GET_ATTANDANCE_UPLOAD_SETTING]: errorAttendanceUploadSetting,

    [constants.REQUEST_ADD_ATTANDANCE_UPLOAD_SETTING]: requestAttendanceUploadSetting,
    [constants.SUCCESS_ADD_ATTANDANCE_UPLOAD_SETTING]: successAttendanceUploadSettingActions,
    [constants.ERROR_ADD_ATTANDANCE_UPLOAD_SETTING]: errorAttendanceUploadSetting,

    [constants.REQUEST_DELETE_ATTANDANCE_UPLOAD_SETTING]: requestAttendanceUploadSetting,
    [constants.SUCCESS_DELETE_ATTANDANCE_UPLOAD_SETTING]: successAttendanceUploadSettingActions,
    [constants.ERROR_DELETE_ATTANDANCE_UPLOAD_SETTING]: errorAttendanceUploadSetting,
    [constants.SUCCESS_RESET_PASSWORD_STATUS]: successResetPasswordStatus,
    [constants.SUCCESS_CLEAR_RESET_PASSWORD_DATA]: successClearResetPasswordData,
    
    [constants.GET_GENRIC_LOGIN_CONFIGURATION]: getGenericConfiguration,
    [constants.GET_GENRIC_LOGIN_CONFIGURATION_ERROR]: getGenericConfigurationError,
    [constants.GET_GENRIC_LOGIN_CONFIGURATION_SUCCESS]: getGenericConfigurationSuccess,
    
    [constants.SET_GENRIC_LOGIN_CONFIGURATION]: settingLoginTypes,
    [constants.SET_GENRIC_LOGIN_CONFIGURATION_SUCCESS]: settingLoginTypesSuccesss,
    [constants.SET_GENRIC_LOGIN_CONFIGURATION_ERROR]: settingLoginTypesError,
    [constants.GET_SLACK_SETTINGS_SUCCESS]: getSlackSettingsSuccess,
    [constants.GET_SLACK_CHANNELS_SUCCESS]: getSlackChannelsSuccess,
    [constants.TEST_SLACK_MESSAGE_STATUS]: testSlackMsgStatus,
    [constants.UPLOAD_ATTENDANCE]: requestUploadAttendance,
    [constants.UPLOAD_ATTENDANCE_ERROR]: errorUploadAttendance,
    [constants.UPLOAD_ATTENDANCE_SUCCESS]: successUploadAttendance,
    [constants.REQUEST_ADD_CHANNEL_SUCCESS]: requsetAddChannelSuccess,
    [constants.REQUEST_HIDE_SLACK_CHANNEL]: requestHideSlackChannel,
    [constants.SUCCESS_GET_SMTP_SETTING]: successGetSmtpSettings,
    [constants.SUCCESS_TEST_SMTP]: successTestSmtp,
    [constants.SUCCESS_SAVE_SMTP_SETTING]: successSaveSmtpSetting,
    [constants.ERROR_SMTP_SETTING]: errorSmtpSetting,
    [constants.SUCCESS_GET_PAYSLIP_SETTING]:successGetPayslipSetting,
    [constants.SUCCESS_CHANGE_RH_CONFIG_SETTING]:showStatusRhConfigSetting,
    [constants.ERROR_CHANGE_RH_CONFIG_SETTING]:showStatusRhConfigSetting
  },
  initialState
);
