import { createAction } from "redux-actions";
import * as _ from "lodash";
import { fireAjax } from "../../../services/index";
import * as constants from "../../../redux/constants";
import {
  show_loading,
  hide_loading
} from "../../../redux/generic/actions/frontend";

export function leave_sucess(data) {
  return createAction(constants.ACTION_LEAVE_SUCCESS)(data);
}

export function leave_fail(data) {
  return createAction(constants.ACTION_LEAVE_FAIL)("Leave Apply Fail");
}

export function leave_error(err) {
  return createAction(constants.ACTION_LEAVE_ERROR)("Error Occurs !!");
}

function async_apply_leave(
  from_date,
  to_date,
  no_of_days,
  reason,
  day_status,
  leaveType,
  late_reason,
  doc_link,
  rh_dates
) {
  return fireAjax("POST", "", {
    action: "apply_leave",
    from_date: from_date,
    to_date: to_date,
    no_of_days: no_of_days,
    reason: reason,
    day_status: day_status,
    leave_type: leaveType,
    late_reason: late_reason,
    doc_link: doc_link,
    rh_dates: rh_dates
  });
}

function async_apply_employe_leave(
  from_date,
  to_date,
  no_of_days,
  reason,
  userId,
  day_status,
  leaveType,
  late_reason,
  doc_link,
  rh_dates
) {
  return fireAjax("POST", "", {
    action: "admin_user_apply_leave",
    from_date: from_date,
    to_date: to_date,
    no_of_days: no_of_days,
    reason: reason,
    user_id: userId,
    day_status: day_status,
    leave_type: leaveType,
    late_reason: late_reason,
    doc_link: doc_link,
    rh_dates: rh_dates
  });
}

export function apply_leave(
  from_date,
  to_date,
  no_of_days,
  reason,
  userId,
  day_status,
  leaveType,
  late_reason,
  doc_link,
  rh_dates
) {
  return function(dispatch, getState) {
    if (_.isEmpty(from_date)) {
      return Promise.reject("From date is empty");
    }
    if (_.isEmpty(to_date)) {
      return Promise.reject("To date is empty");
    }
    if (no_of_days == "") {
      return Promise.reject("No of days is empty");
    }
    if (_.isEmpty(reason)) {
      return Promise.reject("Reason is empty");
    }
    if(leaveType=="RH Compensation" && no_of_days==0.5){
      return Promise.reject("You cannot Apply half day RH Compensation");
    }

    return new Promise((resolve, reject) => {
      dispatch(show_loading()); // show loading icon
      if (userId == "") {
        async_apply_leave(
          from_date,
          to_date,
          no_of_days,
          reason,
          day_status,
          leaveType,
          late_reason,
          doc_link,
          rh_dates
        ).then(
          json => {
            dispatch(hide_loading()); // hide loading icon
            if (json.error == 0) {
              dispatch(leave_sucess(json.data.message));
              resolve(json.data);
            } else {
              dispatch(leave_fail(json.data.message));
              reject(json.data);
            }
          },
          error => {
            dispatch(hide_loading()); // hide loading icon
            dispatch(leave_error("error occurs"));
          }
        );
      } else {
        async_apply_employe_leave(
          from_date,
          to_date,
          no_of_days,
          reason,
          userId,
          day_status,
          leaveType,
          late_reason,
          doc_link,
          rh_dates
        ).then(
          json => {
            dispatch(hide_loading()); // hide loading icon
            if (json.error == 0) {
              resolve(json.data);
              dispatch(leave_sucess(json.data.message));
            } else {
              dispatch(leave_fail(json.data.message));
              reject(json.data);
            }
          },
          error => {
            dispatch(hide_loading()); // hide loading icon
            dispatch(leave_error("error occurs"));
            reject("error occurs");
          }
        );
      }
    });
  };
}

// ------get days between leaves

export function days_between_leaves_sucess(data) {
  return createAction(constants.ACTION_DAYS_BETWEEN_LEAVES_SUCCESS)(data);
}

export function days_between_leaves_fail(data) {
  return createAction(constants.ACTION_DAYS_BETWEEN_LEAVES_FAIL)(data);
}

export function days_between_leaves_error(data) {
  return createAction(constants.ACTION_DAYS_BETWEEN_LEAVES_ERROR)(data);
}

export function successGetUserRhListCompensation(data) {
  return createAction(constants.SUCCESS_GET_RH_LIST_COMPENSATION)(data);
}
export function errorGetUserRhListCompensation(data){
  return createAction(constants.ERROR_GET_RH_LIST_COMPENSATION)(data);

}
function async_getDaysBetweenLeaves(startDate, endDate) {
  return fireAjax("POST", "", {
    action: "get_days_between_leaves",
    start_date: startDate,
    end_date: endDate
  });
}

function async_getUserRhListCompensation(data) {
  return fireAjax("POST", "", {
    action: "user_rh_list_for_compensation",
    ...data
  });
}

export function getDaysBetweenLeaves(startDate, endDate) {
  return function(dispatch, getState) {
    return new Promise((reslove, reject) => {
      dispatch(show_loading()); // show loading icon
      async_getDaysBetweenLeaves(startDate, endDate).then(
        json => {
          dispatch(hide_loading()); // hide loading icon
          if (json.error == 0) {
            dispatch(days_between_leaves_sucess(json.data));
            reslove(json.data);
          } else {
            dispatch(days_between_leaves_fail(json.data));
          }
        },
        error => {
          dispatch(hide_loading()); // hide loading icon
          dispatch(days_between_leaves_error("error occurs"));
        }
      );
    });
  };
}
export function getUserRhListCompensation(data) {
  return function(dispatch, getState) {
    return new Promise((resolve, reject) => {
      dispatch(show_loading()); // show loading icon
      async_getUserRhListCompensation(data).then(
        json => {
          dispatch(hide_loading()); // hide loading icon
          if (json.error == 0) {
            dispatch(successGetUserRhListCompensation(json.data));
          } else {
            dispatch(errorGetUserRhListCompensation({message:json.message,type:"info"}));
          }
        },
        error => {
          dispatch(errorGetUserRhListCompensation({message:"Error Occurs",type:"danger"}));
          dispatch(hide_loading()); // hide loading icon
        }
      );
    });
  };
}
